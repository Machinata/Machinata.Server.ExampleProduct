# Machinata 

1. [Building](#building)
2. [Adding Products](#adding-products)
3. [Adding Modules](#adding-modules)
4. [Running Products](#running-products-in-iis)
5. [Deploying](#deploying-products-to-remote-iis)
6. [Entities](#entities)
7. [Routes](#routes)
8. [Machinata.Core](#machinatacore)
9. [Machinata.Module.Admin](#machinatamoduleadmin)
10. [Machinata.Module.Finance](#machinatamodulefinance)
11. [Machinata.Module.Shop](#machinatamoduleshop)
12. [Machinata.Module.Ticketing](#machinatamoduleticketing)
13. [Machinata.Module.Mailing](#machinatamodulemailing)
14. [Machinata.Module.Crawler](#machinatamodulecrawler)
15. [Machinata.Module.Services](#machinatamoduleservices)


## Building

Machinata can be built on any .NET system with version 4.6.1 or greater. It is compatible on Linux with MonoDevelop/XamarinStudio and Mono 5.0+, and compatible on Mac with MonoDevelop/XamarinStudio/VisualStudio with Mono 5.0+.


## Adding Products
  - Create a new ASP.net web project called ```Machinata.Product.ProductXYZ``` where ```ProductXYZ``` would be something like ```CocaColaWebsite```. Use the empty template  and make sure to deselect "Use Application Insights"
  - Under Project Settings, make sure the .net version is 4.6.1.
  - Under Project Settings > Web, deselect "Apply server settings to all users"
  - Copy the boiler plate files at Machinata.Product.Template to the project root, replacing any default files from Visual Studio. Make sure in the product project to 'include' the files and set any non-code files to 'Copy if newer'.
  - Change AssemblyInfo.cs version to the following (ensures that bundle caches are properly versioned across all libraries):

```c#
[assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyFileVersion("1.0.0.0")]
```
  - In VS 2019: edit the .csproj and change the following entry to false
```xml
<Deterministic>false</Deterministic>
```

  - Set the product config file (at Config/Machinata.Product.ProductXYZ.config) to 'Always Copy'. This make sure that Visual Studio will always build the new modules into the project bin.
  - Remove reference ```Microsoft.CodeDom.Providers.DotNetCompilerPlatform```
  - Remove reference ```Microsoft.CSharp```
  - If the project created has the section ```EnsureNuGetPackageBuildImports``` or project imports in .csproj, remove it from the Project file
```
  <Target Name="EnsureNuGetPackageBuildImports" BeforeTargets="PrepareForBuild">
    ...
  </Target>
  <Import Project="..\packages\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.1.0.0\build\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.props" Condition="Exists('..\packages\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.1.0.0\build\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.props')" />
  <Import Project="..\packages\Microsoft.Net.Compilers.1.0.0\build\Microsoft.Net.Compilers.props" Condition="Exists('..\packages\Microsoft.Net.Compilers.1.0.0\build\Microsoft.Net.Compilers.props')" />-
```
  - Run the command ```Update-Package -reinstall -ProjectName Machinata.Product.XYZ``` in the package manager console (you might need to restart Visual Studio).
  - Run a Project Find-and-Replace for ```ProductXYZ```, replacing with your own product name.
  - Modify Web.config to include the proper product configurations. Make sure the database tables are setup with a own product table and make sure to create new encryption keys and salts.
  - It is paramount to create a custom encryption keys and salts for your product: [https://nerves.ch/admin/system/tools/key-generator]
    - EncryptionCryptKeyString32
    - EncryptionAuthKeyString32
    - EncryptionHashSalt1
    - EncryptionHashSalt2
  - Under Project Settings > Build Events, add the following pre-build event: ```echo "BuildTime=%DATE% %TIME%;BuildUser=%USERNAME%;BuildMachine=%COMPUTERNAME%" > $(ProjectDir)\Config\$(ProjectName).timestamp```. This is used to identify the exact build.
  - Build the Project, then locate the newly created ```Machinata.Product.ProductXYZ.timestamp``` file in the ```Config``` directory and Right-Click > Include in Project. Then under it's properties, set 'Copy to Output Directory' to 'Copy if newer'.

## Adding Modules
  - Create a new class library called ```Machinata.Module.XYZ```. 
  - Make sure the .net version is 4.6.1.
  - Remove all ApplicationInsights packages (start with the most specific)
  - Remove reference ```Microsoft.CodeDom.Providers.DotNetCompilerPlatform```
  - Remove reference ```Microsoft.CSharp```
  - Add a packages.config file with following: 

```xml
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="EntityFramework" version="6.1.3" targetFramework="net461" />
  <package id="MySql.Data" version="6.9.9" targetFramework="net461" />
  <package id="MySql.Data.Entity" version="6.9.9" targetFramework="net461" />
  <package id="NLog" version="4.4.7" targetFramework="net461" />
  <package id="Newtonsoft.Json" version="9.0.1" targetFramework="net461" />
</packages>
```

  - Run the command ```Update-Package -reinstall -ProjectName Machinata.Module.XYZ``` in the package manager console (you might need to restart Visual Studio).
  - Add a project refence to ```Machinata.Core``` and any other modules you need.
  - Change AssemblyInfo.cs version to the following (ensures that bundle caches are properly versioned across all libraries):

```c#
[assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyFileVersion("1.0.0.0")]
```
  - In VS 2019: edit the .csproj and change the following entry to false
```xml
<Deterministic>false</Deterministic>
```


  - Add a root folder called ```Config``` with the following files:
  - Machinata.Module.XYZ.config:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <appSettings>
    <!-- SSL Settings -->
    <add key="YourModuleConfig" value="xyz" />
  </appSettings>
</configuration>
```

  - Config.cs:

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.XYZ {
    public class Config {
        public static int YourModuleConfig = Core.Config.GetIntSetting("YourModuleConfig", 0);
    }
}
```
  - Under Project Settings > Build Events, add the following pre-build event: ```echo "BuildTime=%DATE% %TIME%;BuildUser=%USERNAME%;BuildMachine=%COMPUTERNAME%" > $(ProjectDir)\Config\$(ProjectName).timestamp```. This is used to identify the exact build.
  - Build the Project, then locate the newly created ```Machinata.Module.ProductXYZ.timestamp``` file in the ```Config``` directory and Right-Click > Include in Project. Then under it's properties, set 'Copy to Output Directory' to 'Copy if newer'.



## Running Products in IIS

  - Running a product is best when using a own domain. You can register a new domain at https://10.0.0.1:10000/ (iron) under Server > BIND DNS Server > office.nerves.ch...
  - Furthermore, there are some required config settings that are not shipped by default:
    - Environment
    - AdminEmail
  - Some additional recommended local configs could be:
    - TestEmail
    - SecurityForceHTTPS
  - In IIS, under URL Rewrite for "Default Web Site", add a blank rule:
    - Matches pattern: ```(.*)```
    - Condition input: ```{HTTP_HOST}``` matches the pattern ```xyz.icasa.office.nerves.ch```
    - Rewrite URL: ```/Machinata.Product.YOURPRODUCT{PATH_INFO}```
    - Stop processing: yes
  - To use a common network file share like 'FileStorageProviderPath' 
    - Map the share to a network drive: e.g. '\\\\10.0.0.1\DevData\'
    - In 'Application Pools' under 'Advanced Settings' 'Identity' set your local user as 'custom account'
    - In 'Application Settings' set the 'FileStorageProviderPath-NervesTest' to the desired path
  - In IIS, make sure under 'Compression' settings 'Enable static content compression' is disabled, as this interferes with Machinata's own compression and streaming support.
  - If the website has tasks that need to run regularly, set the website's Application Pool ```Idle Time-out (minutes)``` to zero in the application pool advanced settings.
  - To access a common FileStorage define the config setting: FileStorageProviderPath 
    - for incstance a network path ```FileStorageProviderPath-NervesTest=\\10.0.0.1\DevData\MachinataStorage\{domain}``` 
  - To give the same access rights to the IIS like to local user has. (e.g. needed to access a network drive), go to the 'Application Pools' select the Application Pool open 'Advanced Settings' and set the desired user under 'Idenity'


## Running Products with XSP Mono Server

  - To run a product with XSP, just use the default settings and update a local config (Web.Local.config) as follows:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <appSettings>
        <add key="Environment" value="NervesTest" />
        <add key="SecurityForceHTTPS" value="false" />
        <add key="AdminEmail" value="youremail@domain.com" />
        <add key="TestEmail" value="youremail@domain.com" />
  </appSettings>
</configuration>
```
    

## Deploying Products to remote IIS

  - On the server, install Web/Management Service from the Windows Server Rols & Services installer
  - On the server, install Web Deploy 3.6 (without dependencies or SQL Server addons)
  - In ISS Management on remote machine, make sure the server under 'Management' has 'Management Service' and is configured to allow remote updates
  - In Visual Studio, right-click product, select 'Publish', then 'Custom' with Publish Method 'Web Deploy'. Destination URL can be left blank. Make sure the name of the publish profile reflects both the target server and website, for example ```Hydrogen events.nerves.ch```.
  - Press Deploy

Note: Make sure to always deploy either the master branch or the specific product branch (ie do not deploy a module or feature branch). For example,
when deploying yourwebsite.com, use the product/yourwebsite branch.

Note: If encountering strange errors after a successful deploy regarding connection strings, make sure the update of connections strings is disabled in the deply config file:

```
<ItemGroup>
    <MSDeployParameterValue Include="$(DeployParameterPrefix)Machinata.Core.Model.ModelContext-Web.config Connection String" >
      <UpdateDestWebConfig>False</UpdateDestWebConfig>
    </MSDeployParameterValue>
    <MSDeployParameterValue Include="$(DeployParameterPrefix)Machinata.Core.Model.ModelHistory-Web.config Connection String" >
      <UpdateDestWebConfig>False</UpdateDestWebConfig>
    </MSDeployParameterValue>
    <MSDeployParameterValue Include="$(DeployParameterPrefix)SQLConnection-NervesLive-Web.config Connection String" >
      <UpdateDestWebConfig>False</UpdateDestWebConfig>
    </MSDeployParameterValue>
    <MSDeployParameterValue Include="$(DeployParameterPrefix)SQLConnection-NervesTest-Web.config Connection String" >
      <UpdateDestWebConfig>False</UpdateDestWebConfig>
    </MSDeployParameterValue>
  </ItemGroup>
```


### Let's Encrypt / HTTPS - Certificates Generation

#### Add new Certificate
  1. Run the letsencrypt script
  2. Menu: select "N: Create new certificate"
  3. Which kind of certificate would you like to create?: "1: Single binding of an IIS site"
  4. Choose site: "xx: Website to add Certificate"
  5. "Do you want to replace the existing task? (y/n): -> "n"


### No Database Connection Strings in Publish Profiles
  - Add Machinata.Product.XXX.wpp.targets to root project dir with the following content:

```xml
<?xml version="1.0" encoding="utf-8"?>
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003"
         ToolsVersion="4.0">
  <PropertyGroup>
    <AutoParameterizationWebConfigConnectionStrings>false</AutoParameterizationWebConfigConnectionStrings>
  </PropertyGroup>
</Project>
```

## Entities


## Routes

Routes provide access to different views, files or APIs for your projects. In the admin backend one
can view a interactive tree of all the active routes.

The following practice is standardized:

### /api/*

This is where all the API calls should be organized under.

### /admin/*

All of the admin backend tools should be organized under this.

### /static/*

Serving static files (files provided with the binary) hould be organized under this.

### /content/*

For all content related files that are stored in the Data center

### Views, API's and Admin Routes

The following best practice is defined, and provides a universal method of ensuring that namespaces are very clean,
both for the frontend and backend, as well as for the API, taking into account the programmers needs as well as SEO needs.

#### Entity
  - Artist
  
#### API's 
  - ```/api/artists/create```
  - ```/api/artists/list```
  - ```/api/artists/artist/{publicId}/edit```
  - ```/api/artists/artist/{publicId}/delete```
  - ```/api/artists/artist/{publicId}/custom-method```

#### Admin Views 
  - ```/admin/artists``` (list)
  - ```/admin/artists/artist/{publicId}``` (view)
  - ```/admin/artists/artist/{publicId}/edit``` (edit)

#### Frontend Views 
  - ```/artists``` (list)
  - ```/artists/by/style``` (list grouped by style)
  - ```/artists/by/country``` (list grouped by country)
  - ```/artists/artist/{artistName}``` (view)

### Binding Routes

All routes are served by a request handler (Handler.cs). Every request handler can be customized
in any way. 

Machinata.Core provides the most common, including:
  - APIHandler
  - PageTemplateHandler
  - TextHandler
  - StaticFileHandler
  - StaticDirectoryHandler

In addition, Machinata.Core also provides some useful handlers:
  - AccountAPIHandler
  - CRUDAPIHandler
  - HeartbeatHandler
  - DebuggingHandler
  - ContentFileHandler
  - BarCodeFileHandler
  - CaptchaFileHandler
  - QRCodeFileHandler

To bind a route to a handler, create a class that inherits from Handler.cs and add a method with
the attribute ```[RequestHandler]``` with its path defined:

```
[RequestHandler("/jobs")]
public void Jobs() {
    ...
}
```

Parameters are automatically parsed and registered:

```
[RequestHandler("/jobs/{jobID}")]
public void Jobs(string jobID) {
    ...
}
```
By default, the access policy ARN is the same as the route path, which means if the route
is ```/jobs```, the required ARN for access will also be ```/jobs```. This can be customized
using additional parameters:

```
[RequestHandler("/jobs", AccessPolicy.PUBLIC_ARN)]
public void Default() {
    ...
}
```

Additionally, through the use of ```[AliasRequestHandler]```, a route alias can be added.
A route alias is useful if you want to bind completely different routes to the same handler,
but want to make sure that, for example, templating systems can still find the original route.
This, for example, is very useful, because it allows the PageTemplateHandler to know that
it should try to find the master route templates, instead of the translated templates.

```
[RequestHandler("/jobs", AccessPolicy.PUBLIC_ARN, "en")]
[AliasRequestHandler("/de/jobs", AccessPolicy.PUBLIC_ARN, "de")]
public void Default() {
    ...
}
```

If your project uses translated routes (through the use of ```routes.txt```), you can use the
```[MultiRouteRequestHandler]```, which will automatically explode the routes into all the
registered language versions:


routes.txt:
```
routes.jobs.en=/jobs
routes.jobs.de=/de/jobs
```

Handler:
```
[MultiRouteRequestHandler("routes.jobs",AccessPolicy.PUBLIC_ARN)]
public void Default() {
    ...
}
```

A route handler can also be appended with multiple features, which any part of the system
can use to cleverly compile data, cross-reference, and more:

```
[MultiRouteRequestHandler("routes.jobs",AccessPolicy.PUBLIC_ARN)]
[SitemapRoute]
[CMSPath("Jobs")]
public void Default() {
    ...
}
```

# Machinata.Core

## Analytics
## API

The API namespace provides common API routines for publicly exposing API's. Any regular API handler
can be integrated into a public API through the use of API Keys. API Keys are backwards compatable
with AuthTokens (or rather, a replacement), with the difference that each request is further validated
for basic replay attacks and author authenticity through the use of timestamps and key secrets.


### API Keys

Access to API's are given through API keys which can be generated for any user. The API key inherits
the same access groups and policies for which user it is assigned to. 

Generation and management of API keys is easily integrated by linking to the module ```Machinata.Module.APIKeys```.

API Keys are integrated directly in the core, however they are disabled by default. To
enable the use of authentication using API Keys one must add the following config, or include the module ```Machinata.Module.APIKeys``` 
in your project:

```
	<!-- API Keys -->
    <add key="APIKeysEnabled" value="true" />
```

### API Auth

An API key has both a 128-char key and secret. To make access to an API via a API key one must know both.

For an API key to be valid, one must fulfill all of the following:

- HTTPS
- Header Valid
- Key ID valid
- Key Enabled
- Key Not Expired
- Timestamp within AUTHENTICATION_TIMEWINDOW_SECONDS
- Signature valid

Each HTTP request must be signed using a generated authentication header for the key id and secret:

```machinata {keyId}:{timestamp}:{digest}```

The digest is the HMAC SHA256 of:

```{method}+{resource}+{timestamp}```

Thus, the full authentication header is:

```Authentication: machinata {keyId}:{timestamp}:hmac({method}+{resource}+{timestamp})```

By default, AUTHENTICATION_TIMEWINDOW_SECONDS is 15 minutes.

### API Call

To simplify access, one can use the APICall class to make requests to a Machinata API endpoint:

```
var data = new API.APICall("/api/admin/sandbox/echo","https://api.yourserver.com")
    .WithAPIKey(API_KEY_ID, API_KEY_SECRET)
    .WithParameter("param1", "val1")
    .Send()
    .Data();
```

An endpoint can be configured by default to make things easier (in any config):

```
<add key="{ENDPOINT_NAME}API-Server" value="https://api.yourserver.com" />
<add key="{ENDPOINT_NAME}API-KeyId" value="YOUR API KEY ID" />
<add key="{ENDPOINT_NAME}API-KeySecret" value="YOUR API KEY SECRET" />
<add key="{ENDPOINT_NAME}API-IgnoreSSLCertificate" value="false" />
```

```
var json = new API.APICall("/api/admin/sandbox/echo")
    .WithAPIKey()
    .Send()
    .JSON();
```

By default, the APICall class uses the endpoint configuration ```Default```. You can change your
endpoint by calling ```WithEndPoint()```:

```
var json = new API.APICall("/api/admin/sandbox/echo")
    .WithEndpoint("MySpecialEndpoint")
    .WithAPIKey()
    .Send()
    .JSON();
```

```
<add key="MySpecialEndpointAPI-Server" value="https://api.myspecialendpoint.com" />
<add key="MySpecialEndpointAPI-KeyId" value="YOUR API KEY ID" />
<add key="MySpecialEndpointAPI-KeySecret" value="YOUR API KEY SECRET" />
```

You can also access non-Machinata APIs easily:

```
var json = new API.APICall("/users","https://jsonplaceholder.typicode.com")
    .Send()
    .JSON();
```


## Bundles
## Caching
## Cards
## Charts
## Config
## Data
## Encryption
## Exceptions
## FormBuilder
## Handler

### API

### Page

### PDF

### Static

#### BarCodeFileHandler
#### CaptchaFileHandler

#### ContentFileHandler

Serves content files saved in the ContentFile storage system. Each file can be stored using different providers (for example on the local filesystem, via a network share, or via external providers such as AWS S3).
Typically, serving content is in conjunction with the CMS system (ContentNodes).

Example URL:

```
/content/{category}/{publicId}/{fullFilename}
```

Images (category == image) may use any of the imaging functionality provied by Core.Imaging (Core.Imaging.ImageFactory.ProcessImageForRequest).

#### QRCodeFileHandler
#### StaticDirectoryHandler
#### StaticFileHandler

### Text

## Ids

### Serial Ids

Reserved Serial ID Type Numbers:

NotImplemented: 0 (MODELOBJECT_NO_TYPENUMBER)
Business: 10
Order: 20
OrderSubscription: 26
Invoice: 30

## Imaging

Provides a full set of image manipulation functions for JPG, BMP, PNG, GIF (non animated).

### ImageFactory

Handles the entire lifecycle for working with images, including caching any of the imaging functions.

#### ImageFactory.ProcessImageForRequest

Automatically processes a image for the current request, returning a new filepath that is cached.
All parameters are optional and can be passed either blank or null.
The processed image is automatically cached to disk for future reference.

Examples when used by the content handler:

Crop the image to the exact size 650x234 (keeping aspect) as a JPG 95%, at 2x retina
```
/content/image/dlBqVQ/Amber.jpg?crop=650x234&format=jpg&quality=95&scale=2
```

Resize image to exactly 100x100 (not keeping aspect) as a PNG
```
/content/image/dlBqVQ/Amber.jpg?size=100x100&format=png
```

Scale (or reduce) image to exact width of 300, keeping the aspect ratio
```
/content/image/dlBqVQ/Amber.jpg?size=300
```

Scale (or reduce) image to exact height of 600, keeping the aspect ratio
```
/content/image/dlBqVQ/Amber.jpg?size=x600
```

Recolor an image while preserving the alpha channel
```
/content/image/zhei83/Logo.png?recolor=%26FF00FF
```

The following context variables (via GET or POST) are supported:

- format: Specifies the image type (either png or jpg). If no type is specified, then the current type of the image is kept.
- quality: The quality of the image type (only applies to JPG, number between 1 and 100).
- size: The size of the image to force in format of 'WxH' where W and H are numbers. This will stretch the image to the given size. If either W or H is '' or '?', then the image aspect ratio is kept.
- crop: If specified, automatically crops the image to the given format of 'WxH' here W and H are numbers. If the original is either smaller or larger than the given crop, then the image is automatically scaled. The resulting crop is always centered both horizontally and vertically.
- scale: The scale of the resulting image in the format of 'Sx' where S is a number greater than or equal to 1. If you specify '2x', for example, you would get a image twice as large as the given size or crop.
- recolor: The color to fill the new image with while preserving the alpha channel.

## JSON
## Lifecycle
## Localization

### Language Files

Language files (.txt) in a Product or Module's Localization folder are automatically loaded
on application startup and mapped by the Localization Text Factory. Such a text file
can contain any number of translations and typically looks like:

```
order.en=Order
order.de=Bestellung

orders.de=Bestellungen

order-costs.en=Costs
order-costs.de=Kosten

order-save.en=Save Order
order-save.de=Speichern Order

```

To translate entity properties and enums, one can use the following pre-defined translation ids 
(listed as priority used in finding the correct translation):

```
{entity.type-name}.{property.name}.{form.id}
{entity.type-name}.{property.name}
{property.name}.{form.id}
{property.name}
```

For example, to translate Order.Status one could set:

```
order.status.de=Status
```

In addition, to translate only a specific property on a specific form, one can use:

```
order.status.edit.de=Change Status
```

To translate the Order.Status enums one could set:

```
order.status-type.confirmed.de=Bestaetigt
```

Translating enums can have the following formats:

```
{class.type-name}.{enum.type-name}.{enum.value}
{enum.type-name}.{enum.value}
enum.{enum.value}
```

### Templates

To include a translation in a template, simply use the ```text``` variable namespace:

```
<span class="button">{text.order-save}</span>
```

A translation can be defined inline:

```
<span class="button">{text.order-save.en=Save Order}</span>
```

## Logging
## Messaging
## Model
## NavigationBuilder
## Properties
## Reflection
## Reporting
## Routes
## Server
## SQL
## Static

### JS

#### machinata-core-bundle.js

Machinata JavaScript Core

##### machinata-core.js

Provides the core functionality and structure of the Machinata JavaScript Core

 - Machinata.onInit(callback): Used by any plugin functionality if you want to bind a feature and add a feature during the core JS init routine.
 - Machinata.ready(callback): Used (just like jQuery) for when the DOM is ready and you want to bind UI clicks et cetera to elements.

 
##### machinata-core-privacy.js

Provides common functionality for handing privacy and terms agreement information, warnings and management.
 - Machinata.Privacy.registerOption(key, source, type, defaultValue, required, link, title, description)
 - Machinata.Privacy.registerAgreement(type, version, date, link): Registers a new agreement of specific type.
 - Machinata.Privacy.currentAgreements(): Returns the current agreements types with their newest versions.

Example usage:
 ```
Machinata.ready(function () {
    Machinata.Privacy.registerOption("functional_cookies", "cookie", "checkbox", true, true, null, "{text.privacy.functional-cookies.title}", "{text.privacy.functional-cookies.description}");
    Machinata.Privacy.registerOption("analytical_cookies", "cookie", "checkbox", true, true, null, "{text.privacy.analytical-cookies.title}", "{text.privacy.analytical-cookies.description}");
    Machinata.Privacy.registerOption("personalization_cookies", "cookie", "checkbox", true, false, null, "{text.privacy.personalization-cookies.title}", "{text.privacy.personalization-cookies.description}");
    Machinata.Privacy.registerOption("tracking_cookies", "cookie","checkbox", false, false, null, "{text.privacy.tracking-cookies.title}", "{text.privacy.tracking-cookies.description}");
    Machinata.Privacy.registerAgreement("terms", 1.0, "2018-01-01", "{text.routes.legal}", "{text.privacy.terms-of-use}");
    Machinata.Privacy.registerAgreement("privacy", 1.1, "2018-05-17", "{text.routes.privacy}", "{text.privacy.privacy-policy}");
    Machinata.Privacy.automaticValidationAndInfo();
});
 ```

## TaskManager
## Templates
## Util 
## Worker

The Worker namespace provides easy and lightweight async worker tasks that run automatically in a worker pool. 

By default, a local worker pool is automatically created and started at startup:

```
Core.Worker.LocalPool
```

To create a new async task (work packet), just implement the class ```Core.Worker.WorkerTask```. For example:

```
private class MyWorkerTask : Core.Worker.WorkerTask {

    private Dictionary<string, string> _data;

    public MyWorkerTask(Dictionary<string,string> data) {
        _data = data;
    }

    public override void Process() {
        // Do something with data...
    }

}

// Execute the task on the pool
Core.Worker.LocalPool.QueueTask(new MyWorkerTask(data));
```

Note: You should make sure to avoid including DB referenced members/parameters in a task. Rather than attaching entity objects directly, pass the id's instead and then let the task re-populat the entity object.



# Machinata.Module.Shop

## Bundles

### machinata-shop.js Bundle

Provides common interface methods for managing a cart with products/items.

#### Automatic UI Binding

If a UI is built up using certain classes and data attributes, then a shop
can be entirely automatically bound to the UI:

Example Product:

```html
<div class="bb-shop-product" data-product-id="dkwux3" data-configuration-id="28adj3">
    <h2>Product Title</h2>
	<div>Quantity: <input class="bb-shop-changequantity" type="number" value="1"></div>
	<button class="bb-shop-addtocart">Add to Cart</button>
</div>
```

Example Cart:

```html
<div class="bb-shop-cart" data-cart-id="##########################" >
    <div class="bb-shop-order-item" data-order-item-id="dkwux3">
		<h2>Product Title</h2>
		<div>Quantity: <input class="bb-shop-changequantity" type="number" value="1"></div>
		<button class="bb-shop-removefromcart">Remove</button>
	</div>
</div>
```

Example Currency Conversion:

```
<h1 class="bb-shop-currency-convert" 
	data-source-currency="chf" 
	data-source-amount="80" 
	data-target-currency="{page.currency}" 
	data-target-rounded="true" 
	data-target-format="{converted.amount}">
</h1>
```

# Machinata.Module.Crawler

Utility module providing common crawler functionality, as well as some specific implementations.

## WebCrawler

Helper class that can be inherited to quickly get a crawler up and running.


```
public class NervesImageWebCrawler : WebCrawler {
        
    public const string SERVER_URL = "https://nerves.ch";

    public const string IMAGE_EXTRACTION_REGEX = "https:\\/\\/nerves\\.ch\\/[0-9A-Za-z-/._]+\\/content\\/file\\/[0-9A-Za-z-/._]+\\.jpg";
        
    public NervesImageWebCrawler() : base(SERVER_URL) {
        SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_IMAGES);
    }

    public List<string> GetJournalPhotos() {
        var images = GetRegexExtractionsFromURL("/journal", IMAGE_EXTRACTION_REGEX);
        return images;
    }
}
```

### Profiles

Per default, this crawler provides an easy method to setup the crawler using default profiles. A
profile will define the crawlers rate, user agent spoofing, et cetera.

```
Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH
Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_IMAGES
```

# Machinata.Module.Services

Provides common online services, such as GeoCoding, GeoIP et cetera.

Each service provides a common interfaces and has one or more implementations.

## GeoCoding

```
var data = Module.Services.GeoCoding.Default.DataForAddress("Zurich, Switzerland");
this.LatLon = data.Lat + "," + data.Lon;
```

## GeoIP

```
var data = Module.Services.GeoIP.Default.DataForIP("83.150.2.227");
this.LatLon = data.Lat + "," + data.Lon;
this.City = data.Address.City;
```











# Machinata.Module.UniversalLoginProvider

This module provides a common interface for hosting a universal login endpoint. 
When this module is referenced, the server automatically exposes the endpoint and allows clients servers which have
the endpoint enabled to login via the hosting universal login server.

## Setup 

### Domain

Universal Login always works on a domain level. The first step is to choose a common domain. For example, if you want
all username@example.com users to be able to login universally, you would select ```example.com``` as the domain.

### Universal Login Provider (Server)

To setup a universal login provider, just reference the module ```Machinata.Module.UniversalLoginProvider```.
This server will contain all the master user lists, and manage all the user access rights.
You will also have to setup any AccessGroups and AccessPolicies that clients may use.

### User (Client)

To login on a client server via the Universal Login Provider, you must configure the client server:

- ```UniversalLoginEnabled```: boolean, must be set to true
- ```UniversalLoginDomains```: string list, contains key value collection of domains mapped to endpoints in the format ```{domain}={endpoint-url}```

Example:

```
<add key="UniversalLoginDomains" value="example.com=https://example.com" />
```

## Mechanics

Univeral Login works through the use of user account syncronization. 
The synchronization is always performed from the Provider (master) to the Client (slave).

- If a login on the Client is a username that matches a universal login domain provided by ```UniversalLoginDomains```,
then the login is initially performed on the remove endpoint (the Provider) via the API call ```/api/universal-login/login```. The Client generates a unique Synckey that the master will save. 
This is used later to verify that the Master is authentic on sync requests.
- This API call will perform the login and create an ```AuthToken``` (on the Provider) while creating an additional ```UniversalAuthToken```, storing the Synckey.
- The Client then creates a AuthToken locally. The Provider never exposes it's (master) ```AuthToken```.
- If needed, the Client will automatically create a local copy of the User.
- The ```UniversalAuthToken``` is for tracking the remote ```AuthToken``` on the Client, and stores the hash of both the original AuthToken on the Provider and the remote AuthToken on the Client. 
- For every login, the user account is synced on the Client, including all AccessGroups and AccessPolicies (which exist on the Client).
- If the account is modified on the Provider, then a request is made to each consumer (Client) that has logged in the user via Universal Login: ```/api/account/request-sync```
- The Client recieves this call, validates that the synckey matches what the Client originally set, and performs the sync on the user account.
- 


test submodule touch 1
test subtree touch 2
change from subtree #2 touch 1
change from gitlab 11.58