using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing {
    public class Config {

        public static string SupportTicketPublicPath = Core.Config.GetStringSetting("SupportTicketPublicPath", "tickets/ticket");

        /// <summary>
        /// Add Admin email to BCC on Ticket emails to customers
        /// </summary>
        public static bool SupportTicketCustomerEmailNotficationBCC = Core.Config.GetBoolSetting("SupportTicketCustomerEmailNotficationBCC");

        /// <summary>
        /// The support ticket customer support response enabled.
        /// Deactivate this if customer doesnt use this or there are no proper template for a response
        /// </summary>
        public static bool SupportTicketCustomerSupportResponseEnabled = Core.Config.GetBoolSetting("SupportTicketCustomerSupportResponseEnabled");
    }
}
