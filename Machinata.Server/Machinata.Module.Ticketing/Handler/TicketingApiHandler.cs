
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Ticketing.Model;
using Machinata.Core.Builder;
using Machinata.Module.Ticketing.Messaging;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Ticketing.Handler {


    public class TicketingApiHandler : APIHandler {

        public override string PackageName
        {
            get
            {
                return "Machinata.Module.Ticketing";
            }
        }
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("tickets");
        }

        #endregion


        [RequestHandler("/api/tickets/create", AccessPolicy.PUBLIC_ARN)]
        public void CreateSupportTicket() {

            try {

                // Create ticket & first message
                var ticket = new SupportTicket();
                ticket.Context = DB;
                ticket.User = User;
                ticket.Category = this.Params.String("category", "General");
                ticket.Type = this.Params.String("type", string.Empty);

                // Agent Infos
                ticket.Infos[SupportTicket.REMOTE_IP_INFOS_KEY] = Core.Util.HTTP.GetRemoteIP(this.Context);
                ticket.Infos[SupportTicket.REMOTE_USER_AGENT_KEY] = Core.Util.HTTP.GetUserAgent(this.Context);
                ticket.Infos[SupportTicket.REMOTE_USER_LANGUAGES_KEY] = Core.Util.HTTP.GetUserLanguagesString(this.Context);

                ticket.Populate(this, new FormBuilder(Forms.Frontend.CREATE));
                ticket.Validate();

                ticket.AddMessageByCustomer(ticket.User, this.Params.String("message"));

                // Populate attached entities
                ticket.Populate(this.Context);

                // Additional logic
                ticket.Process(this);

                this.DB.SupportTickets().Add(ticket);
                this.DB.SaveChanges();


                // Customer Email           
                var userTemplate = ticket.CreateEmail(DB, SupportTicket.Receivers.Customer, this.Language);
                userTemplate.SendTicketEmail(ticket, ticket.Category);

                // Notification Emails -> not admins anymore
                var adminTemplate = ticket.CreateEmail(DB, SupportTicket.Receivers.Admin, Core.Config.LocalizationAdminLanguage);
                adminTemplate.SendNotificationEmail();

                // Success
                this.SendAPIMessage("create-ticket-success", new {
                    Ticket = new {
                        PublicId = ticket.PublicId
                    }
                });
            }
            catch(Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Ticket Create Error",this.PackageName, this.Context, this, e);
                throw new BackendException("ticket-error", "Something went wrong. If the problem persists, contact us via email.", e);
            }

        }

        [RequestHandler("/api/tickets/{publicTicketId}/create-message")]
        public void CreateSupportMessage(string publicTicketId) {
         
            //var email = Params.String("email", null);
            var message = Params.String("message", null);

            var ticket = this.DB.SupportTickets().GetByPublicId(publicTicketId);

            // Create message
            var supportMessage = ticket.AddMessageByCustomer(User, message);

            // Set to awaiting reply
            ticket.ChangeStatus(SupportTicket.TicketStatus.AwaitingReply);
         
            this.DB.SaveChanges();
            var email = ticket.CreateEmail(DB, SupportTicket.Receivers.Customer, this.Language);
            email.SendTicketEmail(ticket, ticket.Category);

            SendAPIMessage("create-message-success", new {
                Message = new {
                    PublicId = supportMessage.PublicId
                }
            });
        }


     





    }
}
