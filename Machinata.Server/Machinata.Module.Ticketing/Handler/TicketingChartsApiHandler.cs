using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Ticketing.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;

namespace Machinata.Module.Ticketing.Handler {
    
    public class TicketingChartsApiHandler : Module.Admin.Handler.AdminAPIHandler {
        

        [RequestHandler("/api/admin/tickets/chart/donut/open/categories")]
        public void DonutTicketsOpenCategories() {
            
            var data = new DonutChartData();
            data.Title = "Open Tickets";
            var tickets = this.DB.SupportTickets().Where(t => t.Status == SupportTicket.TicketStatus.AwaitingReply);
            var cats = tickets.GroupBy(t => t.Category).Select(g => new { Category = g.Key, Count = g.Count() });
            foreach(var cat in cats) {
                data.Items.Add(new DonutChartDataItem() { Name = cat.Category, Title = cat.Category, Value = cat.Count });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }
        

    }
}
