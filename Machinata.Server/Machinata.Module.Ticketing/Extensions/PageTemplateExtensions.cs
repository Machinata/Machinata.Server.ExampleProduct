using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Ticketing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing.Extensions {
    public static class PageTemplateExtensions {
        public static void InsertConversation(this PageTemplate template, string variableName, SupportTicket ticket, IOrderedQueryable<SupportMessage> messages, bool isCustomerView ) {

            // Main Template
            var conversationTemplate = template.LoadTemplate("ticket.conversation");
            conversationTemplate.InsertTemplates(
                variableName: "messages",
                entities: messages.OrderBy(m => m.Id),
                templateName: "ticket.message",
                forEachEntity: delegate (SupportMessage m, PageTemplate t) {
                    var ownMessage = m.CustomerMessage && isCustomerView || isCustomerView == false && m.CustomerMessage == false;
                    t.InsertVariable("is-own-message", ownMessage ? "is-own-message" : string.Empty);
                    t.InsertVariableXMLSafeWithLineBreaks("entity.message-with-breaks", m.Message);
                });

          
            var sendTemplate = template.LoadTemplate("ticket.send");
            sendTemplate.InsertVariable("api-call", $"/api/admin/tickets/{ticket.PublicId}/create-message");


            // Statuses
            var statusTemplates = new List<PageTemplate>();
            foreach (var status in Core.Util.EnumHelper.GetEnumValues<SupportTicket.TicketStatus>(typeof(SupportTicket.TicketStatus))) {
                var optionTemplate = template.LoadTemplate("ticket.send.status-option");
                optionTemplate.InsertVariable("option.key", status);
                optionTemplate.InsertVariable("option.selected", status == SupportTicket.TicketStatus.Processed ? "selected='selected'" : string.Empty);
                optionTemplate.InsertVariable("option.value", status.GetEnumTranslationTextVariable());
                statusTemplates.Add(optionTemplate);
            }
            sendTemplate.InsertTemplates("status.options", statusTemplates);

            // Send Teplate
            conversationTemplate.InsertTemplate("reply-send", sendTemplate);

          


            template.InsertTemplate(variableName, conversationTemplate);
        }
    }
}
