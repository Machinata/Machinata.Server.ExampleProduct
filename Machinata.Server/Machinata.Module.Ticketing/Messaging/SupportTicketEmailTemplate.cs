using Machinata.Core.Templates;
using Machinata.Core.Model;

using System.Linq;
using Machinata.Core.Builder;
using Machinata.Module.Ticketing.Model;
using static Machinata.Module.Ticketing.Model.SupportTicket;
using Machinata.Core.Exceptions;
using Machinata.Module.Ticketing;

namespace Machinata.Module.Shop.Messaging {
    public class SupportTicketEmailTemplate : EmailTemplate {

        public SupportTicket Ticket { get; private set; }
        public Receivers Receiver { get; private set; }

        public SupportTicketEmailTemplate(ModelContext db, SupportTicket ticket, Receivers receiver, string templateName, string extension = ".htm", string language = null) : base(db, templateName, extension, language) {
            Ticket = ticket;
            Receiver = receiver;
        }

        public override void Compile() {
            if (Ticket.Context != null) {
                Ticket.Include(nameof(Ticket.Messages));
            }

            // Init
            bool newTicket = Ticket.Messages.Count == 1;

            // Subject & Introduction
            string subject = null;
            string introduction = null;
            string salutation = null;
            if (Receiver == Receivers.Admin) {
                subject = newTicket ?
                    "{text.support-email-subject-admin-new}"
                  : "{text.support-email-subject-admin}";

                introduction = newTicket ?
                "{text.support-email-introduction-admin-new}"
              : "{text.support-email-introduction-admin}";

                salutation = "{text.support-email-salutation-admin}";

            } else if (Receiver == Receivers.Customer) {
                subject = newTicket ?
                "{text.support-email-subject-customer-new}"
              : "{text.support-email-subject-customer}";

                introduction = newTicket ?
               "{text.support-email-introduction-customer-new}"
             : "{text.support-email-introduction-customer}";

                salutation = "{text.support-email-salutation-customer}";

            } else {
                throw new BackendException("receiver-error", $"{Receiver} is not supported");
            }

            // CMS Content
            this.InsertContent(
                variableName: "support-ticket.cms-content",
                cmsPath: "/General/SupportTicket/EmailText",
                throw404IfNotExists: false,
                db: _db
            );

            // Conclusion
            var conclusion = Ticket.Status >= TicketStatus.Closed ? "{text.support-email-conclusion}" : string.Empty;

            // Template
            this.InsertVariables(introduction, conclusion, salutation);

            // Subject translations
            var subjectTextTemplate = new PageTemplate(subject);
            subjectTextTemplate.Language = Language;
            subjectTextTemplate.InsertTextVariables();
            subjectTextTemplate.InsertVariables("support-ticket", Ticket);
            subject = subjectTextTemplate.Content;

            if (string.IsNullOrEmpty(this.Subject)) {
                this.Subject = subject;
            }

            base.Compile();

        }

        private void InsertVariables(string introduction, string conclusion, string salution) {
            this.InsertTemplateVariables();
            this.InsertTextVariables();

            // Path
            if (this.Receiver == Receivers.Customer) {
                this.InsertVariable("ticket-public-path", Config.SupportTicketPublicPath);
            } else {
                this.InsertVariable("ticket-public-path", "admin/tickets/ticket");
            }
          
            // Ticket infos
            this.InsertPropertyList(
                variableName: "support-ticket",
                entity: Ticket,
                form: new FormBuilder(Forms.Admin.EMAIL),
                showCard: false
            );

            this.InsertVariable("support-ticket.infos", Ticket.Infos.ToString());
            this.InsertVariable("support-ticket.introduction", introduction);
            this.InsertVariable("support-ticket.conclusion", conclusion);
            this.InsertVariable("support-ticket.salution", salution);
            this.DiscoverVariables();
            this.InsertTextVariables();

            this.InsertVariables("support-ticket", Ticket);
        }
    }
}
