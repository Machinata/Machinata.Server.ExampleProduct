using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Ticketing.Model {

    [Serializable()]
    [ModelClass]
    public partial class SupportMessage : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SupportMessage() {
            // Parameterless constructure is required for reflection...

        }

        public override void Validate() {
            if (!string.IsNullOrEmpty(this.Message) && Core.Util.String.CheckForSpamLinks(this.Message, false)) {
                throw new BackendException("links-not-allowed", "Sorry, to avoid spam messages we don't allow links.");
            }
        }



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [ForeignKey("TicketId")]
        [InverseProperty("Messages")]
        [FormBuilder]
        public SupportTicket Ticket { get; set; }

        [Column]
        [FormBuilder]
        public int? TicketId { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public User User { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool CustomerMessage { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Author {
            get
            {
                if (CustomerMessage) {
                    Include(nameof(Ticket));
                    return Ticket.Author;
                }
                return User?.Name;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Override Methods //////////////////////////////////////////////////////////////////
        public override string ToString() {
            return Message;
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSupportMessageExtenions {
        public static DbSet<SupportMessage> SupportMessages(this Core.Model.ModelContext context) {
            return context.Set<SupportMessage>();
        }
        public static IQueryable<SupportMessage> SupportMessages(this Core.Model.User user) {
            return user.Context.SupportMessages().Where(o => o.User.Id == user.Id);
        }
    }

  

    #endregion
}
