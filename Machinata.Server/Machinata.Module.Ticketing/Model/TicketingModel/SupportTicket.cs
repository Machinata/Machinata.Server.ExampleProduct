using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Shop.Messaging;
using System.ComponentModel;
using Machinata.Core.Cards;
using Machinata.Core.Util;
using System.Web;

namespace Machinata.Module.Ticketing.Model {

    [Serializable()]
    [ModelClass]
    public partial class SupportTicket : ModelObject, IPublishedModelObject {

        #region Constants /////////////////////////////////////////////////////////////////////////

        public enum Receivers {
            Admin,
            Customer
        }

        public const string REMOTE_IP_INFOS_KEY = "RemoteIP";
        public const string REMOTE_USER_AGENT_KEY = "UserAgent";
        public const string REMOTE_USER_LANGUAGES_KEY = "UserLanguages";

        #endregion

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SupportTicket() {
            // Parameterless constructure is required for reflection...
            this.Messages = new List<SupportMessage>();
            this.GUID = Guid.NewGuid().ToString();
            this.Status = TicketStatus.AwaitingReply;
            this.Infos = new Properties();
            this.ExtensionData = new Properties();
            
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [Column]
        [Required]
        public string Category { get; set; }
      

        [FormBuilder]
        [Column]
        public string GUID { get; set; }

        /// <summary>
        /// Use to specify the id of a SupportTicketType
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Type { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [ForeignKey("UserId")]
        public User User { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [Column]
        public string Name { get; set; }

        [Column]
        [FormBuilder]
        public int? UserId { get; set; }

        [FormBuilder]
        [Column]
        public string EmailEncrypted { get; set; }

        [FormBuilder]
        [Column]
        public string EmailHash { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder("admin-response")]
        [Column]
        public TicketStatus Status { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public Properties Infos { get; set; }

        /// <summary>
        /// Reserved for the Ticket Extension logic
        /// </summary>
        /// <value>
        /// The extension data.
        /// </value>
        [Column]
        public Properties ExtensionData { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [InverseProperty("Ticket")]
        public ICollection<SupportMessage> Messages { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

       
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [NotMapped]
        [DataType(DataType.EmailAddress)]
        public string Email
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.EmailEncrypted);
            }
            set
            {
                this.EmailHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.EmailEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [NotMapped]
        public DateTime Date
        {
            get
            {
                return this.Created;
            }
        }

        // WARNING: Do not change the values which are already in use!
        public enum TicketStatus : short {
            AwaitingReply = 20,         // A new message has been created by the user
            Processed = 30,             // A reply has been sent by the admin, the user may reply back
            Closed = 40,                // The ticket has been closed by admin, replies are no longer admitted
            Resolved = 41,              // This ticket is resolved (and closed)
            Unresolved = 42            // This ticket is unresolved (and closed)
        }
            
        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Title {
            get {

                var title =  $"{this.Category}: ";
                var msg = this.FirstSupportMessage;
                if (msg?.Message != null) {
                    title += Core.Util.String.CreateSummarizedText(msg.Message, 30, true, true);
                }else {
                    title += this.PublicId;
                }
                return title;
            }
        }

        [NotMapped]
        public SupportMessage FirstSupportMessage {
            get {
                this.Include(nameof(this.Messages));
                if (Messages.Count > 0) {
                    return Messages.OrderBy(sm => sm.Id).First();
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the latest support message object.
        /// </summary>
        /// <value>
        /// The latest support message.
        /// </value>
        [NotMapped]
        [FormBuilder]
        public SupportMessage LatestSupportMessage {
            get {
                if (Messages.Count > 0) {
                    return Messages.OrderByDescending(sm => sm.Id).First();
                }
                return null;
            }
        }



        /// <summary>
        /// Gets the author which is either the username or the email (not all tickets have users).
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        [FormBuilder]
        public string Author
        {
            get
            {
                Include(nameof(User));
                return !string.IsNullOrWhiteSpace(Name) ? Name : User != null ? User.Username : Email;
            }
        }
   

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
  
        public string GetTicketTitle() {
            var title = this.Author;
            LoadFirstLevelNavigationReferences();
            if (LatestSupportMessage?.Message != null) {
                title = Core.Util.String.CreateSummarizedText(LatestSupportMessage.Message, 20, true, true);
            }
            return title;
        }

        public SupportMessage AddMessageByCustomer(User user, string message) {
            // Init
            SupportMessage supportMessage = new SupportMessage();
            supportMessage.Message = message;
            supportMessage.Context = Context;
            supportMessage.User = user;
            supportMessage.CustomerMessage = true;
            supportMessage.Validate();
            this.Messages.Add(supportMessage);
            return supportMessage;
        }

        public SupportMessage AddMessageByAdmin(User user, string message) {
            // Init
            SupportMessage supportMessage = new SupportMessage();
            supportMessage.Message = message;
            supportMessage.Context = Context;
            supportMessage.User = user;
            supportMessage.Validate();
            this.Messages.Add(supportMessage);
            return supportMessage;
        }

        public SupportTicketEmailTemplate CreateEmail(ModelContext db, Receivers receiver, string language) {
            var templateName = this.GetEmailTemplateName(receiver);
            var template = new SupportTicketEmailTemplate(db, this, receiver, templateName, ".htm", language);
            this.InsertEmailVariables(db, template);
            template.Compile();
            return template;
        }
        
        /// <summary>
        /// Gets the summary of the last message
        /// </summary>
        /// <value>
        /// The summary.
        /// </value>
        public string Summary {
            get
            {
                const int maxSubjectLenght = 20;
                return Core.Util.String.CreateSummarizedText(LatestSupportMessage.Message, maxSubjectLenght, true, true);
            }
        }

        public bool Published {
            get {
                return this.Status < TicketStatus.Closed;
            }
        }


        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="newStatus">The new status.</param>
        public void ChangeStatus(TicketStatus? newStatus) {
            if (newStatus.HasValue) {
                this.Status = newStatus.Value;
            }
        }

        /// <summary>
        /// Populates the form values from the context to the corresponding entities according to the SupportTicketType
        /// Format of form keys: SupportTicketTypeId_nameForPropertyKey_keyName
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public void Populate(System.Web.HttpContext context) {
            var type = SupportTicket.GetSupportTicketType(context);

            // No Extension found on this ticket
            if (type == null) {
                return;
            }
            var entities = type.GetEntities();

            foreach (var entityKey in entities) {
                var populateProvider = new SupportTicketPopulateProvider(context,type.GetId(), entityKey.Key);
                entityKey.Value.Populate(populateProvider, type.GetForm(entityKey.Key));
                this.ExtensionData[entityKey.Key] = entityKey.Value;
            }
        }

        public void Process(Core.Handler.Handler handler) {
            var type = SupportTicket.GetSupportTicketType(handler.Context);
            // No Extension found on this ticket
            if (type == null) {
                return;
            }
            type.Process(handler, this);
        }

        public string GetEmailTemplateName(Receivers customer) {
            // todo can we get type from SupportTicket.Category or something?
            var type = this.GetSupportTicketType();
            // No Extension found on this ticket
            if (type == null || type.GetEmailTemplateName(customer) == null) {
                return "support-ticket";
            }
            return type.GetEmailTemplateName(customer);
        }

        public void InsertEmailVariables(ModelContext db, EmailTemplate template) {
            var type = this.GetSupportTicketType();
            // No Extension found on this ticket
            if (type == null) {
                return;
            }
            type.InsertEmailVariables(db, template, this);
        }


        public static SupportTicketType GetSupportTicketType(HttpContext context) {
            var formKeys = context.Request.Form.AllKeys.Where(k => k.Contains("_"));

            // Ids which correspond with the SupportTicketType.GetId()
            var types = formKeys.Select(a => a.Split('_').First()).GroupBy(s => s).Select(s => s.Key);


            // New: Also support request params
            if (types.Count() == 0) {

                
                if (context.Request.Params.AllKeys.Contains("type")) {

                    string type = context.Request.Params["type"];
                    var postKeys = context.Request.Params.AllKeys.Where(k =>k.StartsWith(type) &&  k.Contains("_"));

                    // Ids which correspond with the SupportTicketType.GetId()
                    types = postKeys.Select(a => a.Split('_').First()).GroupBy(s => s).Select(s => s.Key);
                }

              
            }

            if (types.Count() > 1) {
                throw new NotSupportedException("mulitiple SupportTicketTypes are not supported");
            }

            // Nothing todo
            if (types.Any() == false) {
                return null;
            }

            var typeName = types.First();
            return SupportTicketType.GetType(typeName);
        }

        public SupportTicketType GetSupportTicketType() {
            var type = SupportTicketType.GetType(this.Type);

            if (type != null) {
                // Check if all data is available
                foreach (var requiredKey in type.GetEntities().Keys) {
                    if (!this.ExtensionData.Keys.Contains(requiredKey)) {
                        return null;
                    }
                }
                return type;
            }
            return null;
        }


        /// <summary>
        /// Fill json data in properties into ModelObjects according to SupportTicketType
        /// </summary>
        /// <returns></returns>
        public Tuple<SupportTicketType, Dictionary<string, ModelObject>> LoadExtensionData() {
            var dataEntities = new Dictionary<string, ModelObject>();
            var supportTicketType = SupportTicketType.GetType(this.Type);
            if (supportTicketType != null) {
                var entities = supportTicketType.GetEntities();
                foreach (var entity in entities) {
                    var objectData = this.ExtensionData[entity.Key];
                    if(objectData == null) {
                        //TODO @micha (dan says: added this to prevent null exceptions but unsure how you want this handled or if these are mission-critical
                        continue;
                    }
                    var modelObject = Core.JSON.Deserialize(objectData.ToString(), entity.Value.GetType()) as ModelObject;
                    dataEntities[entity.Key] = modelObject;
                }
            }
            return new Tuple<SupportTicketType, Dictionary<string, ModelObject>>(supportTicketType, dataEntities);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

        #region Virtual Methods ////////////////////////////////////////////////////////////

        public override CardBuilder VisualCard() {

            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Author)
                .Subtitle(this.Category)
                .Sub(this.Status)
                .Tag(this.Email)
                .Icon("support-ticket")
                .Wide();
            // Delivery
            if (this.Status < TicketStatus.Processed) {
                    card.BlinkSub();
            }

            // Breadcrumb
            var statuses = new List<string>();
            statuses.Add(TicketStatus.AwaitingReply.GetEnumTranslationTextVariable());
            statuses.Add(TicketStatus.Processed.GetEnumTranslationTextVariable());
            statuses.Add(TicketStatus.Closed.GetEnumTranslationTextVariable());
            // Pseudo status
            if (this.Status < TicketStatus.Resolved) {
                statuses.Add("{text.support-ticket.ticket-status.resolved-unresolved}");
            } else if (this.Status == TicketStatus.Resolved) {
                statuses.Add(TicketStatus.Resolved.GetEnumTranslationTextVariable());
            } else if (this.Status == TicketStatus.Unresolved) {
                statuses.Add(TicketStatus.Unresolved.GetEnumTranslationTextVariable());
            }
            card.Breadcrumb(statuses, this.Status.GetEnumTranslationTextVariable());

            return card;
        }

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSupportTicketExtenions {
        public static DbSet<SupportTicket> SupportTickets(this Core.Model.ModelContext context) {
            return context.Set<SupportTicket>();
        }

        public static T GetByGuid<T>(this IQueryable<T> query, string guid, bool throwExceptions = true) where T : Model.SupportTicket {
            var ret = query.Where(e => e.GUID == guid).SingleOrDefault();
            if (ret == null && throwExceptions) throw new Backend404Exception("ticket-not-found", $"The ticket with guid '{guid}' does not exist.");
            return ret;
        }
    }

    public static class ModelObjectOrderExtenions {
        public static IQueryable<SupportTicket> SupportTickets(this Core.Model.User user) {
            return user.Context.SupportTickets().Where(o => o.User.Id == user.Id);
        }

        /// <summary>
        /// Support Tickets connected to a Business via its users
        /// </summary>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        public static IQueryable<SupportTicket> SupportTickets(this Core.Model.Business business) {
            if (business.Context != null) {
                business.Include(nameof(business.Users));
            }
            var usersIds = business.Users.Select(u => u.Id);
            return business.Context.SupportTickets().Include(nameof(SupportTicket.User)).Where(o =>usersIds.Contains( o.User.Id ));
        }

    }

    #endregion
}
