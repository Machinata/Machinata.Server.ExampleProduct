using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Shop.Messaging;
using System.ComponentModel;
using Machinata.Core.Cards;
using Machinata.Core.Util;
using System.Web;

namespace Machinata.Module.Ticketing.Model {
    
    public abstract class SupportTicketType : ModelObject  {

        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SupportTicketType() {
            
        }

        #endregion



        #region Public Methods ////////////////////////////////////////////////////////////////////

        public virtual bool IsEditable(string id) {
            return false;
        }


        public abstract string GetId();

        /// <summary>
        /// Gets the form to Populate the entity with the given key/name
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual FormBuilder GetForm(string key) {
            return new FormBuilder(Forms.Frontend.CREATE);
        }

        // return map key:ModelObject of (in memory) entities which are automtically included in the form builder
        public abstract Dictionary<string, ModelObject> GetEntities();

        public static IEnumerable<SupportTicketType> GetAllTypes() {
            var types = Core.Reflection.Types.GetMachinataTypes(typeof(SupportTicketType)).Where(t => !t.IsAbstract);
            var instances = types.Select(t => Activator.CreateInstance(t) as SupportTicketType);
            var invalidIds = instances.Where(i => i.GetId().Contains('_'));
            if (invalidIds.Any()) {
                throw new Exception("'_' are not allow for SupportTicketType.GetId(): " + invalidIds.First());
            }
            return instances;
        }

        /// <summary>
        /// Get a SupportTicketType by name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// More than one SupportTicketTypes found please fix
        /// or
        /// No SupportTicketType found please fix
        /// </exception>
        public static SupportTicketType GetType(string name) {
            var types = GetAllTypes().Where(t => t.GetId() == name);
            if (types.Count() > 1) {
                throw new Exception("More than one SupportTicketTypes found please fix");
            }
            return types.FirstOrDefault();
        }

      
        public PageTemplate GetExtensionTemplate(PageTemplate parent, string name, ModelObject modelObject, SupportTicket ticket) {
            var template = parent.LoadTemplate("ticket.extension");
            template.InsertVariable("extension.name", name.TextVar());
            template.InsertVariable("extension.short-url", name);
            template.InsertVariable("extension.editable", this.IsEditable(name));
            this.InsertExtensionView(template, name, ticket, modelObject);
            return template;
        }

        /// <summary>
        /// Admin Page View
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="name">The name.</param>
        /// <param name="modelObject">The model object.</param>
        public virtual void InsertExtensionView(PageTemplate parent, string name, SupportTicket ticket, ModelObject modelObject) {
            parent.InsertPropertyList(variableName: "extension",
                                      entity: modelObject,
                                      showCard: false);
        }

        /// <summary>
        /// Admin Page Edit, default behaviour                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="name">The name.</param>
        /// <param name="entity">The extension entity to edit</param>
        public virtual void InsertExtensionEdit(PageTemplate parent, string name, SupportTicket ticket, ModelObject entity) {
            parent.InsertVariables("entity", entity);
            parent.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/tickets/ticket/{ticket.PublicId}/edit-extension/{name}",
                onSuccess: "{page.navigation.prev-path}"
            );
        }


        /// <summary>
        /// Additional Logic when generating a ticket
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="ticket">The ticket.</param>
        public virtual void Process(Core.Handler.Handler handler, SupportTicket ticket) {

        }

        /// <summary>
        /// Additional templating logic
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="context">The context.</param>
        /// <param name="template">The template.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void InsertEmailVariables(ModelContext db, EmailTemplate template, SupportTicket ticket) {
           
        }

        public virtual string GetEmailTemplateName(SupportTicket.Receivers customer) {
            return null;
        }






        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion



    }

    public class SupportTicketPopulateProvider : IPopulateProvider {

        private HttpContext _context = null;
        private string _type = null;
        private string _entityKey = null;

        public SupportTicketPopulateProvider(HttpContext context, string type, string entityKey) {
            _context = context;
            _type = type;
            _entityKey = entityKey;
        }

        public ModelContext DB
        {
            get
            {
                return null;
            }
        }

        public string GetValue(string key) {
            var all = _context.Request.Form.AllKeys.Where(k => k.Contains("_"));
            var paramsKeys = _context.Request.Params.AllKeys.Where(k => k.Contains("_"));
            var keysPerType = all.Where(a => a.StartsWith(_type + "_" + _entityKey + "_", StringComparison.Ordinal)).Select(kpt => kpt.Split('_').Last());

            if (keysPerType.Count() == 0) {
                keysPerType = paramsKeys.Where(a => a.StartsWith(_type + "_" + _entityKey + "_", StringComparison.Ordinal)).Select(kpt => kpt.Split('_').Last());
            }

            var keyShort = keysPerType.FirstOrDefault(k => k == key);
            if (keyShort == null) {
                return null;
            }
            var longKey = all.FirstOrDefault(a => a.EndsWith(keyShort));
            var longKeyParams = paramsKeys.FirstOrDefault(a => a.EndsWith(keyShort));

            var value =  _context.Request.Form[longKey];

            if (value == null && _context.Request.Params.AllKeys.Contains(longKeyParams)) {
                return _context.Request.Params[longKeyParams];
            }

            return value;
        }
    }


}
