﻿using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.View {
    public static class HandlerExtensions {

        /// <summary>
        /// Populates the data from a list edit call and returns the new created entities
        /// 
        /// the keys have the following pattern  : '{public-id}:{form-name}'
        /// deleted entities keys look like this : 'deleted:{public-id}'
        /// new entities keys                    : '{guid-no-seps}':{form-name}
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<T> PopulateListEdit<T>(this Core.Handler.Handler handler, IQueryable<T> allEntities, Func<T> createNew, FormBuilder form = null, bool enableEntityDeletion = false) where T : ModelObject {

            var separator = ':';
            var publicIdLength = "ouGtXA".Length;
            var deletedMarker = "deleted";
            var guidLength = "1aea2ee2bcf0466abac9423549c973fa".Length;
            var newEntities = new List<T>();

            if (form == null) {
                form = new FormBuilder(Forms.Admin.LISTEDIT);
            }

            // Populate Entities
            {
                IEnumerable<string> ids = GetIds(handler, separator, publicIdLength);
                var entities = allEntities.GetByPublicIds(ids);
                foreach (var entity in entities) {
                    entity.Populate(new HandlerPrefixPopulateProvider(handler, entity.PublicId + separator), form);
                }

            }

            // Deleted Entities
            { 
                if (enableEntityDeletion == true) {
                    var idsToDelete = handler.Params.AllValues().Keys.Where(k => k.Contains(separator)).Where(k => k.StartsWith(deletedMarker)).Select(k => k.Substring((deletedMarker + separator).Length)).Distinct().Where(s => s.Length > 0);
                    var entitiesToDelete = allEntities.GetByPublicIds(idsToDelete);
                    foreach (var toDelete in entitiesToDelete.ToList()) {
                        handler.DB.DeleteEntity(toDelete);
                    }
                }
            }


            // New Entities
            {
                var ids = GetIds(handler, separator, guidLength);
                foreach (var id in ids) {
                    var entity = createNew.Invoke();
                    newEntities.Add(entity);
                    entity.Populate(new HandlerPrefixPopulateProvider(handler, id + separator), new FormBuilder(Forms.Admin.LISTEDIT));
                }
            }

            return newEntities;
        }

        private static IEnumerable<string> GetIds(Core.Handler.Handler handler, char separator, int publicIdLength) {
            return handler.Params.AllValues().Keys.Where(k => k.Contains(separator)).Select(k => k.GetUntilOrEmpty(separator.ToString())).Where(k => k.Length == publicIdLength).Distinct();
        }
    }
}
