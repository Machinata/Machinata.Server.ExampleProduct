using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Machinata.Module.Admin.View {
    public static class PageTemplateExtension {

        /// <summary>
        /// Creates a schedule table with weekly rows. The start date has to correspond with the <para>startOfWeek</para>
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="entities">The entities.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="dateSelector">The date selector.</param>
        /// <param name="getTemplateForDay">A function create a custom template on each day</param>
        /// <param name="startOfWeek">The start of week.</param>
        /// <param name="endOfWeekToShow">The end of week to show.</param>
        /// <param name="selectedSchedule">The selected schedule.</param>
        public static void InsertScheduleTable(
            this PageTemplate template,
            IEnumerable<ModelObject> entities,
            DateTime start,
            DateTime end,
            string templateName,
            string variableName,
            Func<ModelObject, DateTime> dateSelector,
            Func<IEnumerable<ModelObject> ,DateTime, PageTemplate, PageTemplate> getTemplateForDay,
            DayOfWeek startOfWeek = DayOfWeek.Monday,
            DayOfWeek endOfWeekToShow = DayOfWeek.Friday,
            DateRange selectedSchedule = null)
            {

            // Template
            var scheduleTemplate = template.LoadTemplate(templateName);

            // Header
            var headerTemplates = new List<PageTemplate>();
            int daysPerWeek = 0;
            if (endOfWeekToShow < startOfWeek) {
                daysPerWeek = (int)endOfWeekToShow - (int)(startOfWeek) + 8;
            }
            else {
                daysPerWeek = (int)endOfWeekToShow - (int)startOfWeek + 1;
            }
            for (int i = 0; i < daysPerWeek; i++) {
                var headerTemplate = template.LoadTemplate(templateName + ".header.column");
                var columnName = (DayOfWeek)((i + (int)startOfWeek) % 7);
                headerTemplate.InsertVariable("header.column.name", columnName.GetEnumTitle(template.Language));
                headerTemplates.Add(headerTemplate);
            }
            scheduleTemplate.InsertTemplates("header.columns", headerTemplates);

            // Week date ranges in utc
            var weeks = new List<DateRange>();
            for (DateTime i = start; i < end; i = i.AddDays(7)) {
                weeks.Add(new DateRange(i, i.AddDays(7).AddSeconds(-1)));
            }

            // Selected week
            var selectedWeek = weeks.SingleOrDefault(w=>w.Start <= selectedSchedule?.Start && w.End >= selectedSchedule?.Start);
            var rangeTemplates = new List<PageTemplate>();
                      
            foreach (var week in weeks) {
                bool activeWeek = week == selectedWeek;
                var weekTemplate = scheduleTemplate.LoadTemplate(templateName + ".week");
                var weekDefaultTimezone = week.ToDefaultTimezone();
                weekTemplate.InsertVariable("week.week", weekDefaultTimezone.Start.Value.GetIso8601WeekOfYear());
                weekTemplate.InsertVariable("week.schedule", weekDefaultTimezone.ToString(Core.Config.DateFormat, false));
                weekTemplate.InsertVariable("week.selected", activeWeek ? "selected" : string.Empty);
                rangeTemplates.Add(weekTemplate);

                var dayTemplates = new List<PageTemplate>();

                for (int i = 0; i < daysPerWeek ; i++) {
                    var dateDayStartUTC = week.Start.Value.AddDays(i);
                    var dateDayEndUTC = dateDayStartUTC.AddDays(1).AddSeconds(-1);
                    var objectsOnDay = entities.Where(e => dateSelector(e) >= dateDayStartUTC && dateSelector(e) < dateDayEndUTC);
                    var dayTemplate = getTemplateForDay(objectsOnDay,dateDayStartUTC,template);
                    dayTemplate.InsertVariable("day.date", Core.Util.Time.ToDefaultTimezoneDateString(dateDayStartUTC));
                    dayTemplate.InsertVariable("day.selected", activeWeek ? "selected" : string.Empty);
                    dayTemplates.Add(dayTemplate);
                }
                weekTemplate.InsertTemplates("week.days", dayTemplates);
            }

            scheduleTemplate.InsertTemplates("schedule", rangeTemplates);
            scheduleTemplate.InsertVariable("schedule.count", weeks.Count());
            scheduleTemplate.InsertVariable("schedule.has-selection", selectedWeek != null ? "has-selection" : "");

            template.InsertTemplate(variableName, scheduleTemplate);
        }

        
    }
    
}