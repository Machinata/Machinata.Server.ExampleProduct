

Machinata.ready(function () {
    if ($("body").hasClass("timetracking-applet")) {
        Machinata.Timetracking.init();
    }
});

Machinata.Timetracking = {};

Machinata.Timetracking.TIMER_UPDATE_INTERVAL = 1000;
Machinata.Timetracking.AUTORESUME_UI_TIMEOUT = 6000;

Machinata.Timetracking._status = null;
Machinata.Timetracking._currentStart = null;
Machinata.Timetracking._currentPathUI = null;
//Machinata.Timetracking._currentPause = null;

Machinata.Timetracking.elems = {};
Machinata.Timetracking.elems.tools = null;
Machinata.Timetracking.elems.timer = null;
Machinata.Timetracking.elems.timerDays = null;
Machinata.Timetracking.elems.timerD = null;
Machinata.Timetracking.elems.timerHours = null;
Machinata.Timetracking.elems.timerH = null;
Machinata.Timetracking.elems.timerMinutes = null;
Machinata.Timetracking.elems.timerM = null;
Machinata.Timetracking.elems.timerSeconds = null;
Machinata.Timetracking.elems.timerS = null;
Machinata.Timetracking.elems.buttonRestart = null;
Machinata.Timetracking.elems.buttonStop = null;
Machinata.Timetracking.elems.description = null;
Machinata.Timetracking.elems.budget = null;

Machinata.Timetracking.init = function () {
    // Cache some selectors
    Machinata.Timetracking.elems.tools = $(".tools");
    Machinata.Timetracking.elems.timer = $(".timer");
    Machinata.Timetracking.elems.timerDays = $(".timer .dhms .days");
    Machinata.Timetracking.elems.timerD = $(".timer .dhms .d");
    Machinata.Timetracking.elems.timerHours = $(".timer .dhms .hours");
    Machinata.Timetracking.elems.timerH = $(".timer .dhms .h");
    Machinata.Timetracking.elems.timerMinutes = $(".timer .dhms .minutes");
    Machinata.Timetracking.elems.timerM = $(".timer .dhms .m");
    Machinata.Timetracking.elems.timerSeconds = $(".timer .dhms .seconds");
    Machinata.Timetracking.elems.timerS = $(".timer .dhms .s");
    Machinata.Timetracking.elems.buttonPause = $(".applet-tools .button.pause");
    Machinata.Timetracking.elems.buttonRestart = $(".applet-tools .button.restart");
    Machinata.Timetracking.elems.buttonStop = $(".applet-tools .button.stop");
    Machinata.Timetracking.elems.buttonEdit = $(".applet-tools .button.edit");
    Machinata.Timetracking.elems.description = $(".description-input input");;
    Machinata.Timetracking.elems.toolsTitle = $(".applet-tools h2");
    Machinata.Timetracking.elems.budget = $(".budget");
    // Bind buttons
    Machinata.Timetracking.elems.buttonPause.click(function () {
        Machinata.Timetracking.pauseTracking();
    });
    Machinata.Timetracking.elems.buttonRestart.click(function () {
        Machinata.Timetracking.restartTracking();
    });
    Machinata.Timetracking.elems.buttonStop.click(function () {
        Machinata.Timetracking.stopTracking();
    });
    Machinata.Timetracking.elems.buttonEdit.click(function () {
         Machinata.Timetracking.editCost();
    });
    // Bind input events
    Machinata.Timetracking.elems.description.change(function () {
        Machinata.Timetracking.updateDescription();
    });
    // Do initial status update
    var projectToOpen = Machinata.queryParameter("project");
    if (projectToOpen == "") projectToOpen = null;
    Machinata.Timetracking.updateStatusUI(); // hide
    Machinata.Timetracking.listBusinesses(function () {
        Machinata.Timetracking.updateStatus(function () {
            if (Machinata.Timetracking._status != null && Machinata.Timetracking._status["project-short-url-path"] != null) {
                Machinata.Timetracking.resumeProjectFromStatus(function () {
                    Machinata.Timetracking.setupTrackingTimerUI(function () {
                        if (projectToOpen != null && projectToOpen != Machinata.Timetracking._status["project-short-url-path"]) {
                            Machinata.debug("Project to open differs: will autoresume...");
                            Machinata.debug("  projectToOpen: " + projectToOpen);
                            Machinata.debug("  project status: " + Machinata.Timetracking._status["project-short-url-path"]);
                            Machinata.Timetracking.openProjectForPath(projectToOpen);
                            Machinata.Timetracking.autoResumeProjectFromStatusIfTracking();
                        }
                    });
                });
            } else {
                Machinata.Timetracking.openProjectForPath(projectToOpen);
            }
        });
    });
    
};

Machinata.Timetracking.openProjectForPath = function (path, callback) {
    if (path == null || path == "") return;
    Machinata.Timetracking.resumeProject(path, null, function () {
        if (callback) callback();
    });
};

Machinata.Timetracking.resumeProjectFromStatus = function (callback) {
    var projectPath = Machinata.Timetracking._status["project-short-url-path"];
    var workTypeId = Machinata.Timetracking._status["work-type-id"];
    Machinata.Timetracking.resumeProject(projectPath, workTypeId, callback);
};

Machinata.Timetracking._autoResumeTimer = null;
Machinata.Timetracking.autoResumeProjectFromStatusIfTracking = function (callback) {
    if (Machinata.Timetracking._trackingTimer == null) return;
    if (Machinata.Timetracking._autoResumeTimer != null) clearTimeout(Machinata.Timetracking._autoResumeTimer);
    // Start UI timer
    Machinata.Timetracking._autoResumeTimer = setTimeout(function () {
        if (Machinata.Timetracking._trackingTimer == null) return;
        if (Machinata.Timetracking._autoResumeTimer == null) return;
        Machinata.Timetracking.resumeProjectFromStatus();
    }, Machinata.Timetracking.AUTORESUME_UI_TIMEOUT);
};

Machinata.Timetracking.resumeProject = function (projectPath, workTypeId, callback, depth) {
    // Debugging
    Machinata.debug("Resuming tracking for " + projectPath + ":");
    Machinata.debug("  WorkTypeId: " + workTypeId);

    // Init
    var segs = projectPath.split("/");
    if (depth > 20) return;
    var parentId = null;
    var currentPath = null;
    if (depth == null) depth = 0;
    Machinata.debug("  Depth: " + depth);
    var currentPath = segs.slice(0, depth + 1).join("/");
    var businessShortURL = segs[0];
    var businessElem = $(".businesses [data-business-short-url='" + businessShortURL + "']");
    var businessId = businessElem.attr("data-business-id");

    // More debugging
    Machinata.debug("  Business: " + businessShortURL);
    Machinata.debug("  Business ID: " + businessId);
    Machinata.debug("  CurrentPath: " + currentPath);
    Machinata.debug("  ProjectPath: " + projectPath);

    // Validate
    if (businessId == null) {
        Machinata.apiError("invalid-timetracking-resume-project", "{text.error-invalid-timetracking-resume-business=Could not resume your timetracking session because the business could not be found!}");
        return;
    }

    // List businesses or projects...
    if(depth == 0) {
        parentId = null;
        $(".businesses .button").removeClass("selected");
        businessElem.addClass("selected");
    } else {
        Machinata.debug("  CurrentPath: " + currentPath);
        // Select subproject along our current path...
        var projectElem = $(".projects [data-project-short-url-path='" + currentPath + "']");
        if (projectElem.length == 0) {
            Machinata.apiError("invalid-timetracking-resume-project", "{text.error-invalid-timetracking-resume-project=Could not resume your timetracking session because the project could not be found!}");
            return;
        }
        projectElem.closest(".group").find(".button").removeClass("selected");
        projectElem.addClass("selected");
        parentId = projectElem.attr("data-project-id");
    }
    
    Machinata.Timetracking.listProjects(businessId, parentId, function () {
        if (currentPath == projectPath) {
            // Exit if we don't have a work type
            if (workTypeId == null) {
                if (callback) callback();
                return;
            }
            // Select work type
            var workTypeElem = $("[data-work-type-id='" + workTypeId + "']");
            if (workTypeElem.length == 0) {
                Machinata.apiError("invalid-timetracking-resume-worktype", "{text.error-invalid-timetracking-resume-worktype=Could not resume your timetracking session because the project work type could not be found or your account is not attached to the project!}");
                return;
            }
            workTypeElem.addClass("selected");
            if (callback) callback();
        } else {
            Machinata.Timetracking.resumeProject(projectPath, workTypeId, callback, depth + 1);
        }
    });
};

Machinata.Timetracking.updateStatus = function (callback) {
    Machinata.apiCall("/api/admin/projects/timetracking/status")
        .success(function (message) {
            Machinata.Timetracking._status = message.data;
            Machinata.Timetracking._status["timestamp"] = new Date();
            if (callback) callback();
        })
        .genericError()
        .send();
};


Machinata.Timetracking.listBusinesses = function (callback) {
    $(".work-types").hide();
    $(".projects").hide();
    Machinata.apiCall("/api/admin/projects/timetracking/businesses")
        .success(function (message) {
            // Create business buttons
            {
                Machinata.Timetracking._currentPathUI = null;
                var container = $(".businesses .buttons");
                container.empty();
                for (var i = 0; i < message.data["businesses"].length; i++) {
                    var business = message.data["businesses"][i];
                    var buttonElem = $("<input class='button ui-button' type='button'></input>");
                    buttonElem.attr("value", business["name"]);
                    buttonElem.attr("data-business-id", business["public-id"]);
                    buttonElem.attr("data-business-short-url", business["short-url"]);
                    buttonElem.click(function () {
                        $(".businesses .buttons .button").removeClass("selected");
                        $(this).addClass("selected");
                        Machinata.Timetracking.listProjects($(this).attr("data-business-id"));
                        Machinata.Timetracking.autoResumeProjectFromStatusIfTracking();
                    });
                    container.append(buttonElem);
                }
                Machinata.UI.bind(container);
                $(".projects").show();
            }
            if (callback) callback();
        })
        .genericError()
        .send();
};

Machinata.Timetracking.listProjects = function (businessId,parentId,callback) {
    Machinata.apiCall("/api/admin/projects/timetracking/projects")
        .data({
            "business-id": businessId,
            "parent-id": parentId
        })
        .success(function (message) {
            // Create project buttons
            {
                var groupId = businessId + "_"+parentId;
                if (parentId == null) {
                    $(".projects .buttons").empty();
                    groupId = businessId + "_" + "root";
                }
                var container = $(".projects .buttons .group[data-group-id='" + groupId + "']");
                if (container.length == 0 && message.data["projects"].length > 0) {
                    container = $("<div class='group'></div>");
                    container.attr("data-group-id", groupId);
                    if (groupId != "root") container.append($("<div class='clear'></div>"));
                    if (groupId != "root") container.append($("<div class='line'></div>"));
                    $(".projects .buttons").append(container);
                }
                for (var i = 0; i < message.data["projects"].length; i++) {
                    var project = message.data["projects"][i];
                    var buttonElem = $("<input class='button ui-button' type='button'></input>");
                    buttonElem.attr("value", project["name"]);
                    buttonElem.attr("data-project-id", project["public-id"]);
                    buttonElem.attr("data-group-id", groupId);
                    buttonElem.attr("data-business-id", businessId);
                    buttonElem.attr("data-project-short-url", project["short-url"]);
                    buttonElem.attr("data-project-short-url-path", project["short-url-path"]);
                    buttonElem.click(function () {
                        var group = $(this).closest(".group");
                        group.nextAll().remove();
                        group.find(".button").removeClass("selected");
                        $(this).addClass("selected");
                        Machinata.Timetracking.listProjects($(this).attr("data-business-id"), $(this).attr("data-project-id"));
                        Machinata.Timetracking.autoResumeProjectFromStatusIfTracking();
                    });
                    container.append(buttonElem);
                }
                Machinata.UI.bind(container);
            }
            // Create work type buttons
            {
                if (message.data["work-types"].length == 0) {
                    $(".work-types").hide();
                } else {
                    var container = $(".work-types .buttons");
                    container.empty();
                    for (var i = 0; i < message.data["work-types"].length; i++) {
                        var workType = message.data["work-types"][i];
                        var buttonElem = $("<input class='button ui-button' type='button'></input>");
                        buttonElem.attr("value", workType["name"]);
                        buttonElem.attr("data-project-id", parentId);
                        buttonElem.attr("data-work-type-id", workType["public-id"]);
                        buttonElem.click(function () {
                            // Select button
                            container.find(".button").removeClass("selected");
                            $(this).addClass("selected");
                            // Start tracking
                            Machinata.Timetracking.startTracking(
                                $(this).attr("data-project-id"),
                                $(this).attr("data-work-type-id"),
                                null
                                );
                        });
                        container.append(buttonElem);
                    }
                    Machinata.UI.bind(container);
                    $(".work-types").show();
                }
            }
            if (callback) callback();
        })
        .genericError()
        .send();
};

Machinata.Timetracking._trackingTimer = null;

Machinata.Timetracking.stopTracking = function (callback) {
    // Do API call
    Machinata.apiCall("/api/admin/projects/timetracking/stop")
        .success(function (message) {
            // Kill timer
            if (Machinata.Timetracking._trackingTimer != null) {
                clearInterval(Machinata.Timetracking._trackingTimer);
                Machinata.Timetracking._trackingTimer = null;
            }
            // Update UI
            //$(".work-types").find(".button").removeClass("selected");
            Machinata.Timetracking.elems.timer.addClass("stopped");
            Machinata.Timetracking.elems.buttonRestart.show();
            Machinata.Timetracking.elems.buttonEdit.show();
            Machinata.Timetracking.elems.buttonStop.hide();
            Machinata.Timetracking.elems.buttonPause.hide();
            Machinata.Timetracking.updateStatusUI();
            // Callback
            if (callback) callback();
        })
        .genericError()
        .send();
};
Machinata.Timetracking.restartTracking = function (callback) {
    var desc = Machinata.Timetracking.elems.description.val();
    Machinata.Timetracking.resumeProjectFromStatus(function () {
        Machinata.Timetracking.startTracking(
            Machinata.Timetracking._status["project-id"],
            Machinata.Timetracking._status["work-type-id"],
            desc,
            callback
        );
    });
    
};


Machinata.Timetracking.editCost = function (callback) {
    if (Machinata.Timetracking._status != null && Machinata.Timetracking._status["cost-path"] != null && Machinata.Timetracking._status["cost-path"] != "") {
        Machinata.debug(Machinata.Timetracking._status);
        Machinata.openPage( Machinata.Timetracking._status["edit-cost-path"] );
    }
};


Machinata.Timetracking.startTracking = function (projectId, workTypeId, description, callback) {
    // Kill timers
    if (Machinata.Timetracking._trackingTimer != null) {
        clearInterval(Machinata.Timetracking._trackingTimer);
        Machinata.Timetracking._trackingTimer = null;
    }
    if (Machinata.Timetracking._autoResumeTimer != null) {
        clearTimeout(Machinata.Timetracking._autoResumeTimer);
        Machinata.Timetracking._autoResumeTimer = null;
    }

    // Do API call
    Machinata.apiCall("/api/admin/projects/timetracking/start")
        .data({
            "project-id": projectId,
            "work-type-id": workTypeId,
            "description": description
        })
        .success(function (message) {
            // Save state
            Machinata.Timetracking._status = message.data;
            Machinata.Timetracking._status["timestamp"] = new Date();
            // Start tracking in ui...
            Machinata.Timetracking.setupTrackingTimerUI();
            // Callback
            if (callback) callback();
        })
        .genericError()
        .send();
};

Machinata.Timetracking.setupTrackingTimerUI = function (callback) {
    // Kill timer
    if (Machinata.Timetracking._trackingTimer != null) clearInterval(Machinata.Timetracking._trackingTimer);
    // Compile title
    var title = $(".businesses .buttons .button.selected").val();
    $(".projects .buttons .group .button.selected").each(function () {
        title += " / " + $(this).val();
    });
    var worktypeTitle = $(".work-types .buttons .button.selected").val();
    var titleElement = Machinata.Timetracking.elems.timer.find(".project-title");
    titleElement.text(title);

    // Make title clickable
    if (Machinata.Timetracking._status["project-admin-link"] != null) {
        titleElement.click(function () {
            Machinata.openPage(Machinata.Timetracking._status["project-admin-link"]);
        });
    }

    Machinata.Timetracking.elems.timer.find(".worktype-title").text(worktypeTitle);
    Machinata.Timetracking.elems.toolsTitle.text(title + " " + worktypeTitle);
    // Calculate server/user time difference
    var timeLocal = Machinata.Timetracking._status["timestamp"];
    var timeServer = new Date(Machinata.Timetracking._status["server-time"]);
    var timeDiff_ms = timeServer.getTime() - timeLocal.getTime(); // milliseconds
    Machinata.debug("Setting up timer...");
    Machinata.debug("  Time Local: " + timeLocal);
    Machinata.debug("  Time Server: " + timeServer);
    Machinata.debug("  Time Diff: " + timeDiff_ms + " ms");
    // Get start of tracking
    var costStart = Machinata.Timetracking._status["cost-start"];
    if (costStart != null) {
        // Get start time as UTC
        if (!costStart.endsWith("Z")) costStart += "Z"; // server is always UTC, even if the JSON serializer doesnt think so
        Machinata.Timetracking._currentStart = new Date(costStart);
        // Correct for time difference
        Machinata.Timetracking._currentStart.setTime(Machinata.Timetracking._currentStart.getTime() - timeDiff_ms);
    } else {
        Machinata.Timetracking._currentStart = new Date();
    }
    // Set description
    var desc = Machinata.Timetracking._status["description"];
    if (desc != null && desc != "") {
        Machinata.Timetracking.elems.description.val(desc);
    } else {
        Machinata.Timetracking.elems.description.val(null);
    }
    // Update UI
    Machinata.Timetracking.elems.timer.removeClass("stopped");
    Machinata.Timetracking.elems.buttonRestart.hide();
    Machinata.Timetracking.elems.buttonEdit.hide();
    Machinata.Timetracking.elems.buttonStop.show();
    Machinata.Timetracking.elems.buttonPause.show();
    Machinata.Timetracking.updateStatusUI();
    // Start UI timer
    Machinata.Timetracking._trackingTimer = setInterval(function () {
        Machinata.Timetracking.updateStatusUI();
    }, 1000);
    // Callback
    if (callback) callback();
};

Machinata.Timetracking.updateDescription = function (callback) {
    if (Machinata.Timetracking._status == null) return;
    var desc = Machinata.Timetracking.elems.description.val();

    // Do API call
    Machinata.apiCall("/api/admin/projects/timetracking/description/update")
        .data({
            "cost-id": Machinata.Timetracking._status["cost-id"],
            "description": desc
        })
        .success(function (message) {
            // Callback
            if (callback) callback();
        })
        .genericError()
        .send();
};

Machinata.Timetracking.updateStatusUI = function () {
    if (Machinata.Timetracking._status == null) {
        Machinata.Timetracking.elems.timer.hide();
        Machinata.Timetracking.elems.tools.hide();
        return;
    }
    // Get time diff
    var now = new Date();
    var start = Machinata.Timetracking._currentStart;
    var diff = new Date(now - start);
    var dhms = Machinata.daysHoursMinutesSecondsFromDate(diff);
    // Clip (due to server/client time diff)
    if (dhms.seconds < 0) dhms.seconds = 0;
    // Update UI
    Machinata.Timetracking.elems.timerSeconds.text(dhms.seconds);
    if (dhms.days > 0) {
        Machinata.Timetracking.elems.timerDays.text(dhms.days);
        Machinata.Timetracking.elems.timerDays.show();
        Machinata.Timetracking.elems.timerD.show();
    } else {
        Machinata.Timetracking.elems.timerDays.hide();
        Machinata.Timetracking.elems.timerD.hide();
    }
    if (dhms.hours > 0) {
        Machinata.Timetracking.elems.timerHours.text(dhms.hours);
        Machinata.Timetracking.elems.timerHours.show();
        Machinata.Timetracking.elems.timerH.show();
    } else {
        Machinata.Timetracking.elems.timerHours.hide();
        Machinata.Timetracking.elems.timerH.hide();
    }
    if (dhms.minutes > 0) {
        Machinata.Timetracking.elems.timerMinutes.text(dhms.minutes);
        Machinata.Timetracking.elems.timerMinutes.show();
        Machinata.Timetracking.elems.timerM.show();
    } else {
        Machinata.Timetracking.elems.timerMinutes.hide();
        Machinata.Timetracking.elems.timerM.hide();
    }

    // Budget
    //var remainingHours = Machinata.Timetracking._status["project-budget"]["estimated-remaining-hours"];
    Machinata.debug("budget: remaining hours", Machinata.Timetracking._status["project-budget"]);
    if (Machinata.Timetracking._status["project-budget"] != null && Machinata.Timetracking._status["project-budget"]["estimated-remaining-hours"] != null) {
        var remainingHours = Machinata.Timetracking._status["project-budget"]["estimated-remaining-hours"];
        var remainingHoursElem = Machinata.Timetracking.elems.budget.find(".remaining-hours");

        var milliseconds = Math.abs(diff);
        var hours = Math.round(milliseconds / 36e5);
        Machinata.debug("budget: running hours", hours);
        remainingHoursElem.text(remainingHours - hours);
        Machinata.Timetracking.elems.budget.show();
    }
    else {
        Machinata.Timetracking.elems.budget.hide();
    }

    // Show
    Machinata.Timetracking.elems.timer.show();
    Machinata.Timetracking.elems.tools.show();
};


Machinata.Timetracking.createWindow = function (project) {
    var appletPath = '/admin/projects/timetracking/applet';
    if (project != null && project != "") {
        appletPath += '?project=' + project;
    }
    var win = window.open(appletPath, 'machinata_timetracker', 'width=460,height=800,scrollbars=no,toolbar=no,menubar=no');
    win.focus(); // no longer supported by all browsers :(
}