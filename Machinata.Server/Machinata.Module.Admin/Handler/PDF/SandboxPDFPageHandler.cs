
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Builder;

namespace Machinata.Core.Handler  {
    public class SandboxPDFPageHandler : PDFPageTemplateHandler {


        [RequestHandler("/pdf/sandbox/text", "/admin/sandbox")]
        public void Text() {
            var texts = Core.Util.PlaceholderText.GetLoremIpsumTexts(this.Params.Int("parapraphs", 40));
            var data = new StringBuilder();
            foreach(var p in texts) {
                data.Append("<p>"+p + "</p>");
            }
            this.Template.InsertVariableUnsafe("data", data);
        }
    }
}
