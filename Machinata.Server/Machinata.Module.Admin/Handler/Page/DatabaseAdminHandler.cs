using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class DatabaseAdminHandler : AdminPageTemplateHandler {


        [RequestHandler("/admin/data/database")]
        public void Default() {
            // All tables
            var templates = new List<PageTemplate>();
            foreach (var table in this.DB.Sets.OrderBy(t => t.Name)) {
                var template = this.Template.LoadTemplate("table-list.item");
                template.InsertVariable("table.id", table.Name);
                template.InsertVariable("table.name", table.Name);
                template.InsertVariable("table.entity-count", this.DB.GetSetQueryableByName(table.Name).Count());
                templates.Add(template);
            }
            this.Template.InsertTemplates("tables", templates);
            this.Template.InsertPropertyList("database.db-info", new ModelMigration.DBInfoEntity(this.DB));

            // Navigation
            this.Navigation.Add("data","{text.data}");
            this.Navigation.Add("database","{text.database}");
        }

        [RequestHandler("/admin/data/database/table/{table}")]
        public void Table(string table) {
            
            // Dynamically load the context and entities using reflection
            var entities = this.DB.GetSetQueryableByName(table).OrderBy(e => e.Id);
            
            // Paginate and insert list
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder("*"), "/admin/data/database/table/{entity.type-name}/entity/{entity.public-id}", true);
            
            // Insert table id and name
            this.Template.InsertVariable("table.id", table);
            this.Template.InsertVariable("table.name", table);

            // Navigation
            this.Navigation.Add("data","{text.data}");
            this.Navigation.Add("database","{text.database}");
            this.Navigation.Add($"table/{table}",table);
        }

        [RequestHandler("/admin/data/database/table/{table}/entity/{publicId}")]
        public void Entity(string table, string publicId) {
            // Insert table id and name
            this.Template.InsertVariable("table.id", table);
            this.Template.InsertVariable("table.name", table);
            
            // Dynamically load the context and entities using reflection
            var entity = this.DB.GetSetQueryableByName(table).GetByPublicId(publicId);
                   
            // Insert property list using * form
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity.property-list", entity, new FormBuilder(), true, true);

            // Get all model object properties
            var navigationsListTemplates = new List<PageTemplate>();
            foreach(var navProp in entity.GetModelObjectProperties()) {
                // Extract the model object
                var navPropVal = navProp.GetValue(entity);
                if (navPropVal == null) continue;
                var navPropObj = navPropVal as ModelObject;
                var navPropObjList = new List<ModelObject>();
                navPropObjList.Add(navPropObj);
                // Create template
                var template = this.Template.LoadTemplate("entity.navigation-property");
                template.InsertVariable("navigation-property.title",navProp.Name);
                template.InsertEntityList("navigation-property.entities", navPropObjList.AsQueryable(), new FormBuilder(Forms.Admin.WILDCARD), "/admin/data/database/table/{entity.type-name}/entity/{entity.public-id}", true);
                navigationsListTemplates.Add(template);
            }

            // Get all navigation properties
            foreach(var navProp in entity.GetNavigationProperties()) {
                // Extract the list
                var navPropVal = navProp.GetValue(entity);
                if (navPropVal == null) continue;
                var navPropCol = navPropVal as System.Collections.ICollection;
                // Create template
                var template = this.Template.LoadTemplate("entity.navigation-property");
                template.InsertVariable("navigation-property.title",navProp.Name);
                template.InsertEntityList("navigation-property.entities", navPropCol, new FormBuilder(Forms.Admin.WILDCARD), "/admin/data/database/table/{entity.type-name}/entity/{entity.public-id}", true);
                navigationsListTemplates.Add(template);
            }
            this.Template.InsertTemplates("entity.navigations-list", navigationsListTemplates);

            // Navigation
            this.Navigation.Add("data","{text.data}");
            this.Navigation.Add("database","{text.database}");
            this.Navigation.Add($"table/{table}",table);
            this.Navigation.Add($"entity/{publicId}",$"#{publicId}",$"Entity #{publicId}");
        }

        [RequestHandler("/admin/data/database/list-entities")]
        public void ListEntities() {
            var templates = new List<PageTemplate>();
            foreach (var entityType in this.DB.Sets) {
                foreach(var prop in entityType.GetProperties().OrderBy(p=>p.GetSortKey())) {
                    var template = this.Template.LoadTemplate("list-entities.item");
                    template.InsertVariable("entity.name", entityType.Name);
                    template.InsertVariable("entity.property.name", prop.Name);
                    template.InsertVariable("entity.property.description", "");
                    templates.Add(template);
                }
                
            }
            this.Template.InsertTemplates("properties", templates);

            // Navigation
            this.Navigation.Add("data","{text.data}");
            this.Navigation.Add("database","{text.database}");
            this.Navigation.Add("list-entities","{text.list-entities}");
        }
        

    }
}
