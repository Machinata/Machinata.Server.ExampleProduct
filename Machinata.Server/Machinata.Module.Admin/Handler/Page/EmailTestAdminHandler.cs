
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Messaging;

namespace Machinata.Module.Admin.Handler {


    public class EmailTestAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/system/tools/email")]
        public void Default() {

            var emailTemplates = MessageCenter.GetTestTemplates(this.DB);
            var testTemplates = emailTemplates.Select(et => new EmailTestTemplate() { Name = et.Name });

            this.Template.InsertEntityList(
                variableName: "entities",
                entities: testTemplates.AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/test/{entity.public-id}");
            
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("email");
        }


        [RequestHandler("/admin/system/tools/email/test/{name}")]
        public void TestTemplate(string name) {

            this.Template.InsertVariable("entity.name", name);
            this.Template.InsertVariable("current-user.email", this.User.Email);

            // Navigation
            this.Navigation.Add("system", "{text.system}");
            this.Navigation.Add("tools", "{text.tools}");
            this.Navigation.Add("email");
            this.Navigation.Add("test/" + name,"{text.test}: " + name);
        }

        [RequestHandler("/admin/system/tools/email/test/{name}/preview-content")]
        public void TestTemplatePreviewContent(string name) {

            var emailTemplate = MessageCenter.GetTestTemplates(this.DB).FirstOrDefault(t=>t.Name == name);

            emailTemplate.Compile();
            this.Template.InsertVariableXMLDecoded("preview", emailTemplate.Content);
       
         
     
        }


    }

    public class EmailTestTemplate : ModelObject {
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; }

        public override string PublicId
        {
            get
            {
                return this.Name;
            }
        }

    }
}
