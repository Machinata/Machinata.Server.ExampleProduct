using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class ModelAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/data/model")]
        public void Default() {
            // Navigation
            this.Navigation.Add("data", "{text.data}");
            this.Navigation.Add("model", "{text.data-model}");
        }
        
    }
}
