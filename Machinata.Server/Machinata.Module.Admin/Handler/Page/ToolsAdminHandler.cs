
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Admin.Handler {


    public class ToolsAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/system/tools")]
        public void Default() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
        }

        [RequestHandler("/admin/system/tools/id-converter")]
        public void IDConverter() {
            string publicID = "";
            string serialID = "";
            string dbID = "";
            
            if(this.Params.String("db-id") != null) {
                var business = new Business();
                business.Id = this.Params.Int("db-id",0);
                dbID = this.Params.String("db-id");
                publicID = Machinata.Core.Ids.Obfuscator.Default.ObfuscateId(dbID.ToInt());
                serialID = Machinata.Core.Ids.SerialIdGenerator.Default.GetSerialIdForEntity(business);
            }
            if(this.Params.String("public-id") != null) {
                publicID = this.Params.String("public-id");
                dbID = Machinata.Core.Ids.Obfuscator.Default.UnobfuscateId(publicID).ToString();
            }
            
            this.Template.InsertVariable("db-id",dbID);
            this.Template.InsertVariable("public-id",publicID);
            this.Template.InsertVariable("serial-id",serialID);

            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("id-converter","{text.tools-id-converter}");
        }

        [RequestHandler("/admin/system/tools/encryption")]
        public void Encryption() {
            string plainText = "";
            string encryptedText = "";
            
            if(this.Params.String("encrypted-text") != null) {
                encryptedText = this.Params.String("encrypted-text");
                plainText = Machinata.Core.Encryption.DefaultEncryption.DecryptString(encryptedText);
            }
            if(this.Params.String("plain-text") != null) {
                plainText = this.Params.String("plain-text");
                encryptedText = Machinata.Core.Encryption.DefaultEncryption.EncryptString(plainText);
            }
            
            this.Template.InsertVariable("encrypted-text",encryptedText);
            this.Template.InsertVariable("hash-text",Core.Encryption.DefaultHasher.HashString(plainText));
            this.Template.InsertVariable("hash-username",Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(plainText));
            this.Template.InsertVariable("plain-text",plainText);

            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("encryption","{text.tools-encryption}");
        }

        
        [RequestHandler("/admin/system/tools/key-generator")]
        public void KeyGenerator() {
            
            this.Template.InsertVariable("EncryptionCryptKeyString32",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionAuthKeyString32",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionHashSalt1",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionHashSalt2",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));


            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("encryption","{text.tools-encryption}");
        }


        [RequestHandler("/admin/system/tools/link-generator")]
        public void LinkGenerator() {

            var entity = new Campaign();
            entity.URL = Core.Config.PublicURL;

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.CREATE).Button("generate","generate"),
                action: null);


            // Navigation
            this.Navigation.Add("system", "{text.system}");
            this.Navigation.Add("tools", "{text.tools}");
            this.Navigation.Add("link-generator");
        }

        public class Campaign : ModelObject {

            [FormBuilder(Forms.Admin.CREATE)]
            public string URL { get; set; }

            [Placeholder("eg. google, newsletter")]
            [FormBuilder(Forms.Admin.CREATE)]
            [Required]
            public string Source { get; set; }

            [Placeholder("eg. spring_sale, summer20")]
            [FormBuilder(Forms.Admin.CREATE)]
            public string Name { get; set; }

            [Placeholder("eg. cpc, banner, email, sticker")]
            [FormBuilder(Forms.Admin.CREATE)]
            public string Medium { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            public string Term { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            [Placeholder("eg. spring_sale, summer20")]
            public string Content { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            public bool IncludeCoupon { get; set; } = false;

            [FormBuilder(Forms.Admin.CREATE)]
            public string CouponCode { get; set; }
        }

        public class AssemblyTypeInfo: ModelObject {

            [FormBuilder(Forms.Admin.LISTING)]
            public string Assembly { get; set; }

            [FormBuilder(Forms.Admin.LISTING)]
            public string Type { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string AQN { get; set; }
        }

        [RequestHandler("/admin/system/tools/assemblies")]
        public void Assemblies() {
            var types = new List<AssemblyTypeInfo>();
            foreach(var assembly in Core.Reflection.Assemblies.GetMachinataAssemblies()) {
                foreach(var type in assembly.GetTypes()) {
                    if (type.Name.StartsWith("<>")) continue;
                    types.Add(new AssemblyTypeInfo() {
                        Assembly = assembly.GetName().Name,
                        Type = type.Name,
                        AQN = type.AssemblyQualifiedName
                    });
                }
            }
            this.Template.InsertEntityList("types", types, new FormBuilder(Forms.Admin.LISTING));
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("assemblies","{text.tools-assemblies}");
        }



      
    }
}
