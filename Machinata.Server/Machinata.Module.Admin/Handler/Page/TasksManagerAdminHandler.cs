using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Model.ScheduledTask;

namespace Machinata.Module.Admin.Handler {


    public class TasksManagerAdminHandler : AdminPageTemplateHandler {


        [RequestHandler("/admin/system/task-manager")]
        public void Default() {

            var entity = new TaskManagerMeta();
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
     
            var availableTasks = TaskManager.GetAvailableTasks();

            Template.InsertEntityList("running-tasks", TaskManager.ActiveTaskList.OrderByDescending(t=>t.StartTime).AsQueryable(), new FormBuilder("/running/listing"), "/admin/system/task-manager/tasks/task/{entity.public-id}");
            Template.InsertEntityList("finished-tasks", TaskManager.FinishedTaskList.OrderByDescending(t => t.EndTime).AsQueryable(), new FormBuilder("/running/listing"), "/admin/system/task-manager/tasks/task/{entity.public-id}");
            var availables = TaskManager.GetAvailableTasks();
            var instances = new List<Core.TaskManager.Task>();
            foreach (var task in availables) {
                var instance = task.Assembly.CreateInstance(task.FullName);;
                instances.Add(instance as Core.TaskManager.Task);
            }
            Template.InsertEntityList("tasks", instances, new FormBuilder(Forms.Admin.LISTING), "/admin/system/task-manager/start-task/{entity.type-name}");

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
        }


        [RequestHandler("/admin/system/task-manager/scheduled-tasks/task/{publicId}")]
        public void ScheduledTaskView(string publicId) {
            var entity = DB.ScheduledTasks().GetByPublicId(publicId);
            Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("scheduled-tasks");
            this.Navigation.Add($"task/{entity.PublicId}", entity.Title);
        
        }

        [RequestHandler("/admin/system/task-manager/tasks/task/{guid}")]
        public void TaskView(string guid) {
            var entity = TaskManager.GetTaskByGUID(guid);
            if (entity == null) {
                throw new BackendException("task-not-found", "{text.task-not-found=Task doesn't exist anymore}");
            }

            Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
            Template.InsertVariables("entity", entity);
            Template.InsertVariable("task.log", entity.GetLog());


            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add($"tasks/task/{entity.GUID}", "{text.task}:" + entity.Name);
        }

        [RequestHandler("/admin/system/task-manager/start-task/{type}")]
        public void TaskStart(string type) {
            this.RequireWriteARN();

            var task = TaskManager.CreateTaskInstance(type);
            if (task == null) {
                throw new BackendException("task-not-found", "{text.task-not-found=Task doesn't exist anymore}");
            }
         
            this.Template.InsertPropertyList("entity", task, new FormBuilder( "task/start"));
            this.Template.InsertVariables("entity", task);

            this.Template.InsertVariable("type", type);
            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add($"start-task/{type}", "{text.task}: " + type);


        }



        [RequestHandler("/admin/system/task-manager/scheduled-tasks")]
        public void ScheduledTasksView() {

            // Local Tasks
            var scheduledTasks = TaskManager.GetScheduledTasks();
            this.Template.InsertEntityList("scheduled-tasks", scheduledTasks, new FormBuilder(Forms.Admin.LISTING), "/admin/system/task-manager/scheduled-tasks/task/{entity.public-id}");

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("scheduled-tasks");

        }


        [RequestHandler("/admin/system/task-manager/remote-tasks")]
        public void RemoteTasksView() {

            // Known Targets
            this.Template.InsertEntityList(
                    variableName: "targets",
                    entities: RemoteTaskManagerAPI.KnownTargets.AsQueryable().Concat(RemoteTaskManagerAPI.RemoteTaskTargetsConfig.Where(t=>!RemoteTaskManagerAPI.KnownTargets.Select(ktt=>ktt.Name).Contains(t.Name))),
                    form: new FormBuilder(Forms.Admin.LISTING));

            // Remote tasks
            var remoteTasks = this.Template.Paginate(this.DB.RemoteTasks(), this,"Created","desc",1,10);
            this.Template.InsertEntityList(variableName: "remote-tasks",
                                            entities: remoteTasks,
                                            form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(RemoteTask.Created))
                                            .Include(nameof(RemoteTask.PublicId)),
                                            link: "/admin/system/task-manager/remote-tasks/task/{entity.public-id}");


            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("remote-tasks");

        }



        [RequestHandler("/admin/system/task-manager/scheduled-tasks/task/{publicId}/edit")]
        public void ScheduledTaskEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.ScheduledTasks().GetByPublicId(publicId);
            Template.InsertForm("entity", entity, new FormBuilder(Forms.Admin.EDIT),apiCall: "/api/admin/task-manager/scheduled-task/{entity.public-id}/edit",onSuccess: "{page.navigation.prev-path}");
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("scheduled-tasks");
            this.Navigation.Add($"task/{entity.PublicId}", entity.Title);
            this.Navigation.Add("edit");
        }

        [RequestHandler("/admin/system/task-manager/scheduled-tasks/create")]
        public void ScheduledTaskCreate() {
            this.RequireWriteARN();

            var entity = new ScheduledTask();
            entity.Role = Core.Config.Environment + "-" + Core.Config.Role;
            entity.Name = this.Params.String("name");
            entity.State = ScheduledTaskState.Waiting;

            this.Template.InsertForm(variableName: "entity",
                            entity: entity,
                            form: new FormBuilder(Forms.Admin.CREATE),
                            apiCall: "/api/admin/task-manager/scheduled-tasks/create",
                            onSuccess: "{page.navigation.prev-path}"
                           );

            this.Template.InsertVariables("entity", entity);

            var availableTasks = TaskManager.GetAvailableTaskNames();

            this.Template.InsertSelectionList(variableName: "task-types", entities: availableTasks.AsQueryable(), selectedEntity: null);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("scheduled-tasks");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/system/task-manager/remote-tasks/create")]
        public void RemoteTaskCreate() {
            this.RequireWriteARN();

            var entity = new RemoteTask();
            entity.Name = this.Params.String("name");


            this.Template.InsertForm(variableName: "form",
                            entity: entity,
                            form: new FormBuilder(Forms.Admin.CREATE),
                            apiCall: "/api/admin/task-manager/remote-tasks/create",
                            onSuccess: "{page.navigation.prev-path}"
                           );

            this.Template.InsertVariables("entity", entity);

      

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("remote-tasks");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/system/task-manager/remote-tasks/task/{publicId}")]
        public void RemoteTaskView(string publicId) {
            var entity = DB.RemoteTasks().GetByPublicId(publicId);
            Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("remote-tasks");
            this.Navigation.Add($"task/{entity.PublicId}", entity.Title);

        }


        [RequestHandler("/admin/system/task-manager/remote-tasks/task/{publicId}/edit")]
        public void RemoteTaskEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.RemoteTasks().GetByPublicId(publicId);
            Template.InsertForm("entity", entity,
                new FormBuilder(Forms.Admin.EDIT), 
                apiCall: "/api/admin/task-manager/remote-task/{entity.public-id}/edit",
                onSuccess: "{page.navigation.prev-path}");
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("task-manager");
            this.Navigation.Add("remote-tasks");
            this.Navigation.Add($"task/{entity.PublicId}", entity.Title);
            this.Navigation.Add("edit");
        }



    }

   

  
}
