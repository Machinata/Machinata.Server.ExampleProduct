using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SettingsAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        [RequestHandler("/admin/config/settings")]
        public void Default() {
            // List all
            this.Template.InsertEntityList(
                variableName: "entities", 
                entities: this.DB.DynamicConfigs(), 
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/config/settings/setting/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("config");
            this.Navigation.Add("settings");
        }
        
        [RequestHandler("/admin/config/settings/setting/{publicId}")]
        public void View(string publicId) {

            // List all
            var entity = this.DB.DynamicConfigs().GetByPublicId(publicId);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("config");
            this.Navigation.Add("settings");
            this.Navigation.Add($"setting/{publicId}",entity.Key);
        }

        [RequestHandler("/admin/config/settings/setting/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            // List all
            var entity = this.DB.DynamicConfigs().GetByPublicId(publicId);
            this.Template.InsertForm(
                variableName: "form", 
                entity: entity, 
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/config/setting/{entity.PublicId}/edit",
                onSuccess:$"/admin/config/settings/setting/{entity.PublicId}"
            );
            // Navigation
            this.Navigation.Add("config");
            this.Navigation.Add("settings");
            this.Navigation.Add($"setting/{publicId}", entity.Key);
            this.Navigation.Add("edit");
        }
        
    }
}
