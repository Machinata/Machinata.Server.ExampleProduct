
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class BusinessAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("businesses");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "contact",
                Path = "/admin/businesses",
                Title = "{text.businesses}",
                Sort = "500"
            });
        }


        #endregion

        [RequestHandler("/admin/businesses/create")]
        public void BusinessCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new Address(),
               form: new FormBuilder(Forms.Admin.CREATE)
                    .Exclude("Name")
                    .Exclude("Company")
                    .Custom("Contact Name","contactname","text",null,true)
                    .Custom("Business Name","businessname","text",null,true)
                    .Custom("Legal Name","legalname","text",null,true),
               apiCall: "/api/admin/businesses/create",
               onSuccess: "/admin/businesses/business/{business.public-id}"
            );

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/businesses/business/{publicId}/edit")]
        public void BusinessEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            var form = new FormBuilder(Forms.Admin.EDIT);
            var ownerBusiness = this.DB.OwnerBusiness(false);
            if (ownerBusiness != null) {
                form.Exclude(nameof(Business.IsOwner));
            }

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/businesses/business/" + entity.PublicId + "/edit",
               onSuccess:"/admin/businesses/business/" + entity.PublicId
            );
          
            
            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/businesses")]
        public void Businesses() {
            var entities = this.DB.ActiveBusinesses().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/businesses/business/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
        }


        [RequestHandler("/admin/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {
            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);
          
            //Addresses which are not 'Archived'
            this.Template.InsertEntityList("addresses", entity.ActiveAddresses.ToList(), new FormBuilder(Forms.Admin.LISTING).Exclude("Company"), "/admin/businesses/business/"+entity.PublicId+ "/address/{entity.public-id}", true);

            // Users
            this.Template.InsertEntityList("users", entity.Users.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/users/user/{entity.public-id}", true);
            
            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
        }

        [RequestHandler("/admin/businesses/business/{businessId}/address/{publicId}")]
        public void BusinessAddressView(string businessId, string publicId) {
            var business = DB.Businesses().Include("Addresses").GetByPublicId(businessId);
            var entity = business.Addresses.AsQueryable().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);
            

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{businessId}", business.Name);
           
            this.Navigation.Add($"address/{publicId}", entity.Title);
        }

        [RequestHandler("/admin/businesses/business/{businessId}/address/{publicId}/edit")]
        public void BusinessAddressEdit(string businessId, string publicId) {
            this.RequireWriteARN();

            var business = DB.Businesses().Include("Addresses").GetByPublicId(businessId);
            var entity = business.Addresses.AsQueryable().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT)
                    .Custom(nameof(business.DefaultShipmentAddress), "shipping-default", "bool", (business.DefaultShipmentAddress == entity))
                    .Custom(nameof(business.DefaultBillingAddress), "billing-default", "bool", (business.DefaultBillingAddress == entity)),
               
               apiCall: "/api/admin/address/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

         
            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{businessId}", business.Name);
           
            this.Navigation.Add($"address/{publicId}", entity.Title);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/businesses/business/{businessId}/create-address")]
        public void BusinessAddressCreate(string businessId) {
            this.RequireWriteARN();

            var business = DB.Businesses().GetByPublicId(businessId);
            this.Template.InsertForm(
               variableName: "form",
               entity: new Address(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/businesses/business/" + businessId +"/addresses/create",
               onSuccess: "/admin/businesses/business/{business.public-id}"
            );

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{businessId}", business.Name);
            this.Navigation.Add($"create-address", "{text.create-address=Create Address}");
          
        }

        [RequestHandler("/admin/businesses/business/{publicId}/manage-users")]
        public void BusinessManageUser(string publicId) {
            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();

            this.Template.InsertSelectionList("entity-list", DB.Users(), entity.Users, new FormBuilder(Forms.Admin.SELECTION), "/api/admin/businesses/business/" + entity.PublicId + "/users/{entity.public-id}/toggle");
          

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
            this.Navigation.Add($"add-user", "{text.manage-users}");
        }


        [RequestHandler("/admin/businesses/business/{publicId}/add-new-user")]
        public void BusinessAddNewUser(string publicId) {
            var business = DB.Businesses().GetByPublicId(publicId);
            var entity = new User();

            // Create form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.EMPTY)
                .Include(nameof(User.Name))
                .Include(nameof(User.Email)),
                apiCall: $"/api/admin/businesses/business/{publicId}/users/add-new",
                onSuccess: "{page.navigation.prev-path}");

            // Groups
            this.Template.InsertSelectionList(
                variableName: "groups",
                entities: this.DB.AccessGroups(),
                form: new FormBuilder(Forms.Admin.LISTING),
                selectedEntities: entity.AccessGroups);

            // Vars
            this.Template.InsertVariables("business", business);

            // Navigation
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", business.Name);
            this.Navigation.Add($"add-new-user");

        }

    }
}
