using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class DefaultAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return new List<AccessPolicy> {
                new AccessPolicy() { Name = "Admin Root", ARN = "/admin" } // Note: this has no wildcard!
            };
        }

        #endregion

        [RequestHandler("/admin")]
        public void Default() {

            this.Template.InsertNewGitLogs(Core.Config.GitLogKeyword);

        }


        [RequestHandler("/admin/logout", AccessPolicy.PUBLIC_ARN)]
        public void Logout() {

        }


        [RequestHandler("/admin/verify-login", AccessPolicy.PUBLIC_ARN)]
        public void VerifyLogin() {

        }


    }
}
