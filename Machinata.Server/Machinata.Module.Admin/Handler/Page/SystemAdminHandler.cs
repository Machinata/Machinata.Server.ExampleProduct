using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Admin.View;

namespace Machinata.Module.Admin.Handler {


    public class SystemAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("system");
        }

        #endregion

        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "services",
                Path = "/admin/system",
                Title = "{text.system}",
                Sort = "800"
            });
        }

        #endregion

        [RequestHandler("/admin/system")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/system/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("system.menu-items", menuItems);

            // Git Log
            this.Template.InsertGitLogs(Core.Config.GitLogKeyword);





            // Navigation
            this.Navigation.Add("system", "{text.system}");
        }

      

    }
}
