using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public abstract class AdminPageTemplateHandler : Machinata.Module.UIKit.Handler.UIKitPageHandler  {

        public override string PackageName {
            get {
                return "Machinata.Module.Admin";
            }
        }

        public override void SetupForContext(HttpContext context) {
            base.SetupForContext(context);
            // Override language setting?
            if (!string.IsNullOrEmpty(Core.Config.LocalizationAdminLanguage)) {
                this.Language = Core.Config.LocalizationAdminLanguage;
            }
        }

        public override void DefaultNavigation() {
            // Add the default admin root path
            //this.Navigation.TitlePrefix = "{text.machinata-admin}";
            this.Navigation.Add("/admin","{text.admin}", Core.Config.ProjectName + " {text.admin-subtitle}");
        }

        public override string DefaultTheme() {
            return "uikit-admin";
        }

        /*public override string DefaultLanguage() {
            if (!string.IsNullOrEmpty(Core.Config.LocalizationAdminLanguage)) {
                return Core.Config.LocalizationAdminLanguage;
            } else {
                return null;
            }
        }*/

        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();

            // User
            if (this.User != null) {
                this.Template.InsertVariables("user", this.User);
                this.Template.InsertVariable("user.initials", this.User.GetInitials());
            } else {
                this.Template.InsertVariable("user.initials", string.Empty);
            }

            // Menu
            var menuBuilder = MenuBuilder.LoadMenuFromHandlers(this);
            this.Template.InsertMenu("page.menu", menuBuilder, "menu");
            this.Template.InsertMenu("page.menu-tiles", menuBuilder, "menu-tiles");

            // Page settings
            var pageSettings = new List<string>();
            pageSettings.Add("show-menu");
            if (this.User != null) pageSettings.Add("user-logged-in");
            else pageSettings.Add("user-logged-out");
            this.Template.InsertVariable("page.settings", string.Join(" ", pageSettings));

        }

        public override void Finish() {

            //if (Core.Config.GetBoolSetting("PenisAdminMode", false) && Core.Util.HTTP.GetRemoteIP(this.Context) == "83.150.2.227") {
            //    _template.Data.Append("<style>*{cursor:url('/static/file/images/penis-cursor.png'),default;}</style>");
            //}

            base.Finish();
        }

        public override void OnError(BackendException be) {
            base.OnError(be);

            if (Core.Config.AccountUsernameUsesEmail) {
                this.Template.InsertVariable("username-or-email-label","Email");
            } else {
                this.Template.InsertVariable("username-or-email-label","Username");
            }   
        }
    }
}
