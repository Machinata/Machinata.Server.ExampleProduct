using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class RoutesAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("system");
        }

        #endregion

        [RequestHandler("/admin/system/routes")]
        public void Default() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("routes","{text.routes}");
        }

        [RequestHandler("/admin/system/routes/list")]
        public void List() {
            // Init
            var templates = new List<PageTemplate>();
            var routes = Core.Routes.Route.GetRouteMapping();
            // Filter?
            var filter = this.Params.String("filter");
            if (filter == "") filter = null;
            if(filter != null && filter != "" && filter != "all") {
                routes = Core.Routes.Route.GetRouteMappingForFilter(filter);
            }
            // Loop results
            var i = 0;
            foreach (var path in routes.Keys) {
                int priority = i;
                foreach(var keyVal in routes[path]) {
                    var route = keyVal.Value;
                    //string method = del.Value.Item2.Name.ToString();
                    //string handler = del.Value.Item2.DeclaringType.Name;
                    //string verb = del.Key.ToString();
                    // Insert template
                    var template = this.Template.LoadTemplate("route-list.item");
                    template.InsertVariable("route.path", route.Path);
                    template.InsertVariable("route.trimmed-path", route.TrimmedPath);
                    template.InsertVariable("route.uid", route.UID);
                    template.InsertVariable("route.key", route.Key);
                    template.InsertVariable("route.priority", priority.ToString("#0000"));
                    template.InsertVariable("route.verb", route.Verb.ToString());
                    template.InsertVariable("route.handler", route.HandlerName);
                    template.InsertVariable("route.method", route.MethodName);
                    template.InsertVariable("route.language", route.Language);
                    template.InsertVariable("route.alias-for", route.AliasFor);
                    templates.Add(template);
                }
                i++;
            }
            this.Template.InsertTemplates("routes", templates);
            // Filter ui
            var selctedFilter = "all";
            var searchFilter = "";
            if (filter == null || filter == "all") selctedFilter = "all";
            else {
                selctedFilter = "search";
                searchFilter = ": " + filter;
            }
            this.Template.InsertVariable("selected-filter", selctedFilter);
            this.Template.InsertVariable("search-filter", searchFilter);
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("routes","{text.routes}");
            this.Navigation.Add("list","{text.list=List}");
        }



        [RequestHandler("/admin/system/routes/route")]
        public void ViewRoute() {
            // Init
            var uid = this.Params.String("uid");
            var route = Core.Routes.Route.GetRouteForUID(uid);
            this.Template.InsertPropertyList("properties", route, new FormBuilder(Forms.Admin.VIEW));
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("routes","{text.routes}");
            this.Navigation.Add("list","{text.list=List}");
            this.Navigation.Add("route?uid="+uid,uid);
        }

    }
}
