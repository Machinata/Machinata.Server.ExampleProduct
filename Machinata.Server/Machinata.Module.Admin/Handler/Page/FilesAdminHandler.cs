using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class FilesAdminHandler : AdminPageTemplateHandler {


    
        [RequestHandler("/admin/data/files")]
        public void Default() {
            // Paginate and insert list
            var entities = this.Template.Paginate(this.DB.ContentFiles().OrderByDescending(e => e.Created), this);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(ContentFile.Created)),
                link: "/admin/data/files/content-file/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("data", "{text.data}");
            this.Navigation.Add("files", "{text.files}");
        }

        [RequestHandler("/admin/data/files/create")]
        public void Create() {
            this.RequireWriteARN();

            this.Template.InsertForm(
                variableName: "form",
                entity: new ContentFile() { StorageProvider = Core.Config.FileStorageProviderName},
                form: new FormBuilder(Forms.Admin.CREATE).Custom("File","file","file", null),
                apiCall: "/api/admin/content/files/create",
                onSuccess: "/admin/data/files/content-file/{content-file.public-id}"
            );

            // Navigation
            this.Navigation.Add("data", "{text.data}");
            this.Navigation.Add("files", "{text.files}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/data/files/content-file/{publicId}")]
        public void View(string publicId) {
            // Get entity
            var entity = this.DB.ContentFiles().GetByPublicId(publicId);
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList(
                variableName: "entity.property-list",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW).IncludeStandard(),
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );

            // Navigation
            this.Navigation.Add("data", "{text.data}");
            this.Navigation.Add("files", "{text.files}");
            this.Navigation.Add($"content-file/{publicId}",  $"Content File: {entity.FileName}.{entity.FileExtension}");
        }

        [RequestHandler("/admin/data/files/content-file/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();


            var entity = this.DB.ContentFiles().GetByPublicId(publicId);
            this.Template.InsertForm(
              variableName: "form",
              entity: entity,
              form: new FormBuilder(Forms.Admin.EDIT),
              apiCall: "/api/admin/content/files/edit",
              onSuccess: "/admin/data/files/content-file/{content-file.public-id}"
          );

            // Navigation
            this.Navigation.Add("data", "{text.data}");
            this.Navigation.Add("files", "{text.files}");
            this.Navigation.Add($"content-file/{publicId}", $"Content File: {entity.FileName}.{entity.FileExtension}");
            this.Navigation.Add("edit", "{text.edit}");
        }


        [RequestHandler("/admin/data/files/content-file/{publicId}/download",null, null,Verbs.Any,ContentType.StaticFile)]
        public void DownloadContentFile(string publicId) {
            // Get entity
            var entity = this.DB.ContentFiles().GetByPublicId(publicId);
            // Send bytes
            var bytes = DataCenter.DownloadFileContent(entity);
            var fileName = entity.FullFileName;
            HTTP.SendBinaryData(Context,bytes, fileName);
        }

      
    }
}
