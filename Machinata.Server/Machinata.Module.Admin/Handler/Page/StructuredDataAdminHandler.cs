using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class StructuredDataAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("structured-data");
        }

        #endregion

        [RequestHandler("/admin/structured-data")]
        public void Default() {
            // Init
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/structured-data/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("menu-items", menuItems);
        }


        [RequestHandler("/admin/structured-data/configs")]
        public void Configs() {
            // Init
            this.Template.InsertEntityList(
                variableName: "entity-list",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/structured-data/configs/config/{entity.public-id}",
                entities: DB.StructuredDataConfigs()
                );

            // Navigation
            this.Navigation.Add("structured-data");
            this.Navigation.Add("configs");
        }

        [RequestHandler("/admin/structured-data/configs/config/{publicId}")]
        public void Config(string publicId) {
            // Init
            var entity = DB.StructuredDataConfigs().GetByPublicId(publicId);

            // Form
            this.Template.InsertPropertyList(
                variableName: "entity",
                form: new FormBuilder(Forms.Admin.VIEW),
                entity:entity
                );
   
            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("structured-data");
            this.Navigation.Add("config/" + publicId, entity.Name);
        }
        
      
        [RequestHandler("/admin/structured-data/configs/create")]
        public void Create() {
            // Init
            this.RequireWriteARN();


            this.Template.InsertForm(
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                entity: new StructuredDataConfig(),
                apiCall: "/api/admin/structured-data/configs/create",
                onSuccess: "{page.navigation.prev-path}"

                );

            // Navigation
            this.Navigation.Add("structured-data");
            this.Navigation.Add("configs");
            this.Navigation.Add("configs/create","create".TextVar());
        }

        [RequestHandler("/admin/structured-data/config/{publicId}/edit")]
        public void Edit(string publicId) {
            // Init
            this.RequireWriteARN();

            var entity = DB.StructuredDataConfigs().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                entity: entity,
                apiCall: "/api/admin/content/layout/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}"
                );

            // Navigation
            this.Navigation.Add("structured-data");
            this.Navigation.Add("configs");
            this.Navigation.Add("config/" + publicId, entity.Name);
            this.Navigation.Add("edit");
        }


    
    }

    //public class StructuredDataConfig : ModelObject {

    //    [FormBuilder]
    //    [FormBuilder(Forms.Admin.LISTING)]
    //    [FormBuilder(Forms.Admin.VIEW)]
    //    public string Key { get; set; }
    //    [FormBuilder(Forms.Admin.LISTING)]
    //    [FormBuilder(Forms.Admin.VIEW)]
    //    [FormBuilder(Forms.Admin.EDIT)]
    //    public string Value { get; set; }

    //    public override string PublicId
    //    {
    //        get
    //        {
    //            return Key;
    //        }
    //    }
    }
