using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Reporting;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class ExportAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("export");
        }

        #endregion
        
        [RequestHandler("/admin/export")]
        public void Default() {
            // All tables
            var templates = new List<PageTemplate>();
            foreach (var table in this.DB.Sets) {
                var template = this.Template.LoadTemplate("table-list.item");
                template.InsertVariable("table.id", table.Name);
                template.InsertVariable("table.name", table.Name);
                template.InsertVariable("table.entity-count", this.DB.GetSetQueryableByName(table.Name).Count());
                templates.Add(template);
            }
            this.Template.InsertTemplates("tables", templates);

            // Navigation
            this.Navigation.Add("export","{text.export}");
        }

        [RequestHandler("/admin/export/table/{table}")]
        public void Table(string table) {

            // Dynamically load the context and entities using reflection
            var entities = this.DB.GetSetQueryableByName(table).OrderBy(e => e.Id);

            

            var type = DB.GetModelCobjectClassByName(table);
            var dummyInstance = (ModelObject)Activator.CreateInstance(type);


            var propertiesTemplates = new List<PageTemplate>();
            var propertiesPreselected = dummyInstance.GetPropertiesForForm(new FormBuilder(Forms.Admin.WILDCARD));
            var form = new FormBuilder();
            foreach (var prop in dummyInstance.GetPropertiesForForm(form)) {
                var template = this.Template.LoadTemplate("property-selector");
                template.InsertVariable("property.id", prop.GetFormName(form));
                template.InsertVariable("property.name", prop.Name);
                propertiesTemplates.Add(template);
            }
            Template.InsertTemplates("properties", propertiesTemplates);
            Template.InsertVariable("entities.count", entities.Count());

            // Date Range Filter
            if (entities.Any()) {
                this.Template.InsertForm("date-filter", new FormBuilder().Custom("date-start", "date-start", "datetime", entities.Min(m => m.Created)).Custom("date-end", "date-end", "datetime", entities.Max(m => m.Created)));
            }

            // Paginate and insert list
            this.Template.InsertEntityList("entity-list", entities.Take(3), new FormBuilder(Forms.Admin.WILDCARD), "/admin/data/database/table/{entity.type-name}/entity/{entity.public-id}", true);
            
            
            // Insert table id and name
            this.Template.InsertVariable("table.id", table);
            this.Template.InsertVariable("table.name", table);
            this.Template.InsertJSON("preselected.properties", propertiesPreselected.Select(p => p.GetFormName(form)));

            // Navigation
            this.Navigation.Add("export","{text.export}");
            this.Navigation.Add($"table/{table}",table);



        }

        [RequestHandler("/admin/export/table/{table}/entity/{publicId}")]
        public void Entity(string table, string publicId) {

          
            // Insert table id and name
            this.Template.InsertVariable("table.id", table);
            this.Template.InsertVariable("table.name", table);
            
            // Dynamically load the context and entities using reflection
            var entity = this.DB.GetSetQueryableByName(table).GetByPublicId(publicId);
                   
            // Insert property list using * form
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity.property-list", entity, new FormBuilder(), true, true);

            // Get all model object properties
            var navigationsListTemplates = new List<PageTemplate>();
            foreach(var navProp in entity.GetModelObjectProperties()) {
                // Extract the model object
                var navPropVal = navProp.GetValue(entity);
                if (navPropVal == null) continue;
                var navPropObj = navPropVal as ModelObject;
                var navPropObjList = new List<ModelObject>();
                navPropObjList.Add(navPropObj);
                // Create template
                var template = this.Template.LoadTemplate("entity.navigation-property");
                template.InsertVariable("navigation-property.title",navProp.Name);
                template.InsertEntityList("navigation-property.entities", navPropObjList, new FormBuilder(Forms.Admin.WILDCARD), "/admin/database/table/{entity.type-name}/entity/{entity.public-id}", true);
                navigationsListTemplates.Add(template);
            }

            // Get all navigation properties
            foreach(var navProp in entity.GetNavigationProperties()) {
                // Extract the list
                var navPropVal = navProp.GetValue(entity);
                if (navPropVal == null) continue;
                var navPropCol = navPropVal as System.Collections.ICollection;
                // Create template
                var template = this.Template.LoadTemplate("entity.navigation-property");
                template.InsertVariable("navigation-property.title",navProp.Name);
                template.InsertEntityList("navigation-property.entities", navPropCol, new FormBuilder(Forms.Admin.WILDCARD), "/admin/database/table/{entity.type-name}/entity/{entity.public-id}", true);
                navigationsListTemplates.Add(template);
            }
            this.Template.InsertTemplates("entity.navigations-list", navigationsListTemplates);

            // Navigation
            this.Navigation.Add("export","{text.export}");
            this.Navigation.Add($"table/{table}",table);
            this.Navigation.Add($"entity/{publicId}",$"#{publicId}",$"Entity #{publicId}");
        }

        [RequestHandler("/admin/export/all-entities")]
        public void ListEntities() {
            //var templates = new List<PageTemplate>();
            //foreach (var entityType in this.DB.Sets) {
            //    foreach(var prop in entityType.GetProperties().OrderBy(p=>p.GetSortKey())) {
            //        var template = PageTemplate.LoadForTemplateContextAndName(this.Template, "list-entities.item");
            //        template.InsertVariable("entity.name", entityType.Name);
            //        template.InsertVariable("entity.property.name", prop.Name);
            //        template.InsertVariable("entity.property.description", "");
            //        templates.Add(template);
            //    }
                
            //}
            //this.Template.InsertTemplates("properties", templates);

            //// Navigation
            //this.Navigation.Add("export","{text.export}");
            //this.Navigation.Add("list-entities","{text.list-entities}");
        }

        [RequestHandler("/admin/export/table/{table}/download", "*", null, Verbs.Get, ContentType.StaticFile)]
        public void ExportTable(string table) {

            var filterproperties = Params.String("filteredProps", null);
            var filterDateStart = Time.ConvertToUTCTimezone(Params.DateTime("start", DateTime.MinValue));
            var filterDateEnd = Time.ConvertToUTCTimezone(Params.DateTime("end", DateTime.MaxValue));

            // No seconds passed from query
            filterDateEnd = filterDateEnd.AddMinutes(1);

            if (!string.IsNullOrEmpty(filterproperties) && !string.IsNullOrEmpty(table)) {

                // Dynamically load the context and entities using reflection
                var filterPropertyNames = filterproperties.Split(',');

                // Dynamically load the context and entities using reflection
                var classType = this.DB.GetModelCobjectClassByName(table);
                var entities = DB.GetSetQueryableByName(table);

                // Filter Date
                entities = entities.Where(e => e.Created >= filterDateStart && e.Created <= filterDateEnd);

                // Load related properties
                foreach( var entity in entities) {
                    entity.LoadFirstLevelNavigationReferences();
                    entity.LoadFirstLevelObjectReferences();
                }
                var selectedProperties = classType.GetProperties().Where(p => filterPropertyNames.Contains(p.GetFormName(new FormBuilder())));
                var content = ExportService.ExportXLSX(entities, selectedProperties);

                HTTP.SendBinaryData(Context, content, $"{table}.xlsx");

            } else {
                throw new BackendException("export-failed", $"Could not eport data with table={table} and properties={filterproperties} ");
            }

        }


    }
}
