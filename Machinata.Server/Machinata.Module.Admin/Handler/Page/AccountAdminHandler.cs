using Machinata.Core.Handler;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class AccountAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("account");
        }



        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "sign-out",
                ARN = "/admin",
                Path = "/admin/logout",
                Title = "{text.logout}",
                Classes = "requires-login",
                Sort = "999"
            });
            menu.AddSection(new MenuSection {
                Icon = "",
                Path = "/admin",
                Title = "{text.login}",
                Classes = "requires-logout",
                Sort = "999"
            });
        }

        #endregion



        public override void DefaultNavigation() {
            base.DefaultNavigation();

        }

        [RequestHandler("/admin/account")]
        public void Default() {
            // Get entity
            var entity = this.User;
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity.properties", entity);
            // Navigation
            this.Navigation.Add("account");
        }
        

        [RequestHandler("/admin/account/edit")]
        public void Edit() {
            this.RequireWriteARN();

            var entity = this.User;
            this.Template.InsertVariables("entity", entity);
            // Note: form must that in /api/account/edit and /admin/account/edit
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT).Exclude(nameof(User.Name)).Exclude(nameof(User.Username)).Exclude(nameof(User.Enabled)),
                apiCall: $"/api/account/edit",
                onSuccess: $"/admin/account"
            );

            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("edit");
        }
        
        [RequestHandler("/admin/account/activate", AccessPolicy.PUBLIC_ARN)]
        public void ActivateAccout() {

            var user = this.Template.InsertAccountActivationForm(onSuccess: Core.Config.AccountNewUserActivationSuccess);

            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("activate", "{text.activate}", user.Name);

        }

        [RequestHandler("/admin/account/reset-password", AccessPolicy.PUBLIC_ARN)]
        public void ResetPassword() {

            var user = this.Template.InsertResetPasswordForm(onSuccess: "/admin/account/reset-password/success");

            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("reset-password", "{text.reset-password}", user.Name);

        }

        [RequestHandler("/admin/account/reset-password/success", AccessPolicy.PUBLIC_ARN)]
        public void ResetPasswordSucces() {
           
            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("reset-password", "{text.reset-password}");
            this.Navigation.Add("success");

        }

        [RequestHandler("/admin/account/request-password-reset", AccessPolicy.PUBLIC_ARN)]
        public void RequestResetPassword() {

            var form = new FormBuilder("empty").Include(nameof(User.Username));
            if (Core.Config.AccountUsernameUsesEmail) {
                form = new FormBuilder("empty").Include(nameof(User.Email));
            }
            form.Button("button", "{text.request-password-reset}");
            this.Template.InsertForm(
                entity: new User(),
                variableName: "form",
                form: form,
                onSuccess: "/admin/account/request-password-reset/success",
                apiCall: "/api/account/request-password-reset"
            );

            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("request-password-reset", "{text.request-password-reset}");

        }

        [RequestHandler("/admin/account/request-password-reset/success", AccessPolicy.PUBLIC_ARN)]
        public void RequestResetPasswordSucces() {
           
            // Navigation
            this.Navigation.Add("account");
            this.Navigation.Add("request-password-reset", "{text.request-password-reset}");
            this.Navigation.Add("success");

        }


    }
}
