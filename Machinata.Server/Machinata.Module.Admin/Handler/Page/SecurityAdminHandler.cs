using Machinata.Core.Handler;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SecurityAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("security");
        }

        #endregion

        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "lock",
                Path = "/admin/security",
                Title = "{text.security}",
                Sort = "800"
            });
        }


        #endregion

        [RequestHandler("/admin/security")]
        public void Default() {
            // Navigation
            this.Navigation.Add("security","{text.security}");
        }

        #region Groups

        [RequestHandler("/admin/security/groups")]
        public void Groups() {
            // Paginate and insert list
            var entities = this.Template.Paginate(this.DB.AccessGroups(), this);
            this.Template.InsertEntityList(
                variableName: "entity-list", 
                entities: entities, 
                form: new FormBuilder(Forms.Admin.LISTING), 
                link: "/admin/security/groups/group/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("groups","{text.access-groups}");
        }

        [RequestHandler("/admin/security/groups/create")]
        public void GroupsCreate() {
            this.RequireSuperuser();

            this.Template.InsertForm(
                variableName: "form",
                entity: new AccessGroup(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/security/access-group/create",
                onSuccess: "/admin/security/groups/group/{group.public-id}"
            );

            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("groups","{text.access-groups}");
            this.Navigation.Add("create","{text.create}");
        }
        
        [RequestHandler("/admin/security/groups/group/{publicId}")]
        public void GroupsView(string publicId) {
            var entity = this.DB.AccessGroups().Include("AccessPolicies").Include("Users").GetByPublicId(publicId);
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList(
                variableName: "entity.property-list", 
                entity: entity, 
                form: new FormBuilder(Forms.Admin.VIEW).IncludeStandard(), 
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );
            // Insert selection list policies
            {
                var entities = this.Template.AutoSort(this.DB.AccessPolicies(), this);
                this.Template.InsertSelectionList(
                    variableName: "policy-selection",
                    entities: entities,
                    selectedEntities: entity.AccessPolicies.Cast<ModelObject>(),
                    selectionAPICall: "/api/admin/security/access-group/" + entity.PublicId + "/policy/{entity.public-id}/toggle",
                    form: new FormBuilder(Forms.Admin.SELECTION)
                );
            }
            {
                var entities = this.Template.AutoSort(this.DB.Users(), this);
                this.Template.InsertSelectionList(
                    variableName: "user-selection",
                    entities: entities,
                    selectedEntities: entity.Users.Cast<ModelObject>(),
                    selectionAPICall: "/api/admin/security/access-group/" + entity.PublicId + "/user/{entity.public-id}/toggle",
                    form: new FormBuilder(Forms.Admin.SELECTION)
                );
            }
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("groups","{text.access-groups}");
            this.Navigation.Add($"group/{entity.PublicId}",entity.Name);
        }

        [RequestHandler("/admin/security/groups/group/{publicId}/edit")]
        public void GroupsEdit(string publicId) {
            this.RequireSuperuser();

            var entity = this.DB.AccessGroups().GetByPublicId(publicId);
            // Edit form
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/security/access-group/{entity.PublicId}/edit",
                onSuccess: $"/admin/security/groups/group/{entity.PublicId}"
            );
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("groups","{text.access-groups}");
            this.Navigation.Add($"group/{entity.PublicId}",entity.Name);
            this.Navigation.Add("edit","{text.edit}");
        }

        #endregion
        
        #region Policies

        [RequestHandler("/admin/security/policies")]
        public void Policies() {
            // Paginate and insert list
            var entities = this.Template.Paginate(this.DB.AccessPolicies(), this);
            this.Template.InsertEntityList(
                variableName: "entity-list", 
                entities: entities, 
                form: new FormBuilder(Forms.Admin.LISTING), 
                link: "/admin/security/policies/policy/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("policies","{text.access-policies}");
        }

        [RequestHandler("/admin/security/policies/create")]
        public void PoliciesCreate() {
            this.RequireSuperuser();

            this.Template.InsertForm(
                variableName: "form",
                entity: new AccessPolicy(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/security/access-policy/create",
                onSuccess: "/admin/security/policies/policy/{policy.public-id}"
            );

            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("policies","{text.access-policies}");
            this.Navigation.Add("create","{text.create}");
        }
        
        [RequestHandler("/admin/security/policies/policy/{publicId}")]
        public void PoliciesView(string publicId) {
            var entity = this.DB.AccessPolicies().Include("AccessGroups").Include("Users").GetByPublicId(publicId);
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList(
                variableName: "entity.property-list", 
                entity: entity, 
                form: new FormBuilder(Forms.Admin.VIEW).IncludeStandard(), 
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );
            // Insert selection list policies
            {
                var entities = this.Template.AutoSort(this.DB.AccessGroups(), this);
                this.Template.InsertSelectionList(
                    variableName: "group-selection",
                    entities: entities,
                    selectedEntities: entity.AccessGroups.Cast<ModelObject>(),
                    selectionAPICall: "/api/admin/security/access-policy/" + entity.PublicId + "/group/{entity.public-id}/toggle",
                    form: new FormBuilder(Forms.Admin.SELECTION)
                );
            }
            {
                var entities = this.Template.AutoSort(this.DB.Users(), this);
                this.Template.InsertSelectionList(
                    variableName: "user-selection",
                    entities: entities,
                    selectedEntities: entity.Users.Cast<ModelObject>(),
                    selectionAPICall: "/api/admin/security/access-policy/" + entity.PublicId + "/user/{entity.public-id}/toggle",
                    form: new FormBuilder(Forms.Admin.SELECTION)
                );
            }
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("policies","{text.access-policies}");
            this.Navigation.Add($"policy/{entity.PublicId}",entity.Name);
        }

        [RequestHandler("/admin/security/policies/policy/{publicId}/edit")]
        public void PoliciesEdit(string publicId) {
            this.RequireSuperuser();

            var entity = this.DB.AccessPolicies().GetByPublicId(publicId);
            // Edit form
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/security/access-policy/{entity.PublicId}/edit",
                onSuccess: $"/admin/security/policies/policy/{entity.PublicId}"
            );
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("policies","{text.access-policies}");
            this.Navigation.Add($"policy/{entity.PublicId}",entity.Name);
            this.Navigation.Add("edit","{text.edit}");
        }

        #endregion
        
        #region Audit Logs

        [RequestHandler("/admin/security/audit-logs")]
        public void AuditLogs() {
            // Do filter
            var entities = this.DB.AuditLogs().AsQueryable();
            string filterActor = this.Params.String("actor");
            string filterAction = this.Params.String("action");
            string filterTarget = this.Params.String("target");
            string filterTargetType = this.Params.String("target-type");
            string filterTargetPublicID = this.Params.String("target-public-id");
            string filterIP = this.Params.String("ip");
            if(!string.IsNullOrEmpty(filterActor)) {
                entities = entities.Where(al => al.Actor.ToLower().Contains(filterActor.ToLower()));
            }
            if(!string.IsNullOrEmpty(filterAction)) {
                entities = entities.Where(al => al.Action.ToLower().Contains(filterAction.ToLower()));
            }
            if(!string.IsNullOrEmpty(filterTarget)) {
                entities = entities.Where(al => (al.TargetType.ToLower() + " #" + al.TargetPublicID.ToLower()).Contains(filterTarget.ToLower()) );
            }
            if(!string.IsNullOrEmpty(filterTargetType)) {
                entities = entities.Where(al => al.TargetType.ToLower().Contains(filterTargetType.ToLower()));
            }
            if(!string.IsNullOrEmpty(filterTargetPublicID)) {
                entities = entities.Where(al => al.TargetPublicID.ToLower().Contains(filterTargetPublicID.ToLower()));
            }
            if(!string.IsNullOrEmpty(filterIP)) {
                entities = entities.Where(al => al.IP.ToLower().Contains(filterIP.ToLower()));
            }
            // Paginate and insert list
            entities = this.Template.Paginate(entities.OrderByDescending(al => al.Id), this);
            this.Template.InsertEntityList(
                variableName: "entity-list", 
                entities: entities, 
                form: new FormBuilder(Forms.Admin.LISTING).Include("Created"), 
                link: "/admin/security/audit-logs/log/{entity.public-id}"
            );
            // Filter variables
            this.Template.InsertVariable("actor-filter", !string.IsNullOrEmpty(filterActor) ? $": {filterActor}" : "");
            this.Template.InsertVariable("target-filter", !string.IsNullOrEmpty(filterTarget) ? $": {filterTarget}" : "");
            this.Template.InsertVariable("ip-filter", !string.IsNullOrEmpty(filterIP) ? $": {filterIP}" : "");
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("audit-logs","{text.audit-logs}");
        }
        
        [RequestHandler("/admin/security/audit-logs/log/{publicId}")]
        public void AuditLogsView(string publicId) {
            var entity = this.DB.AuditLogs().GetByPublicId(publicId);
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList(
                variableName: "entity.property-list", 
                entity: entity, 
                form: new FormBuilder(Forms.Admin.VIEW).IncludeStandard(), 
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );
            this.Template.InsertEntityList(
                variableName: "entity.changes", 
                entities: entity.Changes, 
                form: new FormBuilder(Forms.Admin.LISTING), 
                link: null
            );
            // Navigation
            this.Navigation.Add("security","{text.security}");
            this.Navigation.Add("audit-logs","{text.audit-logs}");
            this.Navigation.Add($"log/{entity.PublicId}",entity.Title);
        }
        
        #endregion

    }
}
