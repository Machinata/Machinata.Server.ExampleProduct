using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Messaging;
using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SystemMessageAdminHandler : AdminPageTemplateHandler {


        [RequestHandler("/admin/system/messages")]
        public void Default() {

            var entities = DB.SystemMessages().OrderByDescending(sm => sm.Id);

            // Filter UI
            entities = this.Template.Filter(entities, this);
            entities = this.Template.Paginate(entities, this);

            // Entities         
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING), 
                link: "/admin/system/messages/message/{entity.public-id}");

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("messages");
        }


        [RequestHandler("/admin/system/messages/message/{publicId}")]
        public void Message(string publicId) {

            var entity = DB.SystemMessages().GetByPublicId(publicId);

            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("system");
            this.Navigation.Add("messages");
            this.Navigation.Add($"message/{entity.PublicId}", entity.PublicId);
        
        }

        [RequestHandler("/admin/system/messages/message/{publicId}/content")]
        public void Content(string publicId) {
            var entity = DB.SystemMessages().GetByPublicId(publicId);
         
            // Content
            if (entity.Service == nameof(EmailSender)) {

                var content = Core.JSON.ParseJsonAsJObject(entity.Content);
                var template = this.Template.LoadTemplate("content.email");
                var subject = content["subject"].ToString();
                var emailContent = content["content"];
                template.InsertVariable("email.subject", subject);
                template.InsertVariableUnsafe("email.content", emailContent.ToString());
                this.Template.InsertTemplate("content", template);
            } else {
                this.Template.InsertVariable("content", "Content not recognized: " + entity.Service);
            }


        }
    }
}
