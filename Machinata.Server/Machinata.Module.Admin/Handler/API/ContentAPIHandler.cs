
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class ContentAPIHandler : AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        [RequestHandler("/api/admin/content/node/create")]
        public void CreateNode() {
            this.RequireWriteARN();

            // Init
            var parentPath = this.Params.String("parent");
            var parentNode = this.DB.ContentNodes().Include("Children").GetNodeByPath(parentPath);
            // Create
            var newNode = parentNode.CreateSubnode(this.Params.String("name"));

            // Sort
            var siblingNodes = parentNode.Children.Where(c => c.NodeType == ContentNode.NODE_TYPE_NODE);
            if (siblingNodes.Any()) {
                newNode.Sort = siblingNodes.Max(c => c.Sort) + 1;
            }

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("create-success", new {
                Node = new {
                    Name = newNode.Name,
                    Path = newNode.Path,
                    ParentPath = newNode.Parent.Path,
                    PublicId = newNode.PublicId
                }
            });
        }

      

        [RequestHandler("/api/admin/content/node/delete")]
        public void DeleteNode() {
            this.RequireWriteARN();
            // Init
            var path = this.Params.String("path");
            var node = this.DB.ContentNodes().GetNodeByPath(path);
            // Delete
            this.DB.DeleteEntity(node);
            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/content/node/translation/create")]
        public void CreateNodeTranslation() {
            this.RequireWriteARN();

            // Init
            var language = this.Params.String("language");
            var parentPath = this.Params.String("parent");
            var parentNode = this.DB.ContentNodes().Include("Children").GetNodeByPath(parentPath);
            // Create
            var newNode = parentNode.AddTranslation(language);
            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("create-success", new {
                Node = new {
                    Name = newNode.Name,
                    Path = newNode.Path,
                    ParentPath = newNode.Parent.Path,
                    PublicId = newNode.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/content/page/{publicId}/save")]
        public void PageContentSave(string publicId) {
            this.RequireWriteARN();

            // Save using json
            var node = ContentNode.UpdateContentUsingJSON(this.DB, this.Params.String("content"));

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("save-success");
        }

        [RequestHandler("/api/admin/content/page/import/{path}")]
        public void PageContentImport(string path) {
            this.RequireWriteARN();

            // Init
            if (string.IsNullOrEmpty(path)) path = this.Params.String("path");
            if (!path.StartsWith(ContentNode.NODE_PATH_SEP)) path = ContentNode.NODE_PATH_SEP + path;

            // Get json
            Newtonsoft.Json.Linq.JObject json = GetJsonFromContext();

            // Save using json
            var node = ContentNode.ImportContentFromJSON(this.DB, json, path, this.User, true);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("import-success");
        }

      
        [RequestHandler("/api/admin/content/page/import/")]
        public void PageContentImportSection() {
            this.RequireWriteARN();
            this.PageContentImport(null);

        }

        private Newtonsoft.Json.Linq.JObject GetJsonFromContext() {
            var file = this.Request.File();
            if (!file.FileName.EndsWith(".json")) throw new BackendException("invalid-file", "The file must be a JSON (.json) file.");
            byte[] fileData = null;
            using (var binaryReader = new System.IO.BinaryReader(file.InputStream)) {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            var json = Core.JSON.ParseJsonAsJObject(fileData);
            return json;
        }

        [RequestHandler("/api/admin/content/files/search")]
        public void PageContentFilesSearch() {
            // Init
            string category = this.Params.String("category");
            string filter = this.Params.String("filter");
            string source = this.Params.String("source");
            string filetype = this.Params.String("filetype");

            // Do filter
            var results = this.DB.ContentFiles().AsQueryable();
            results = FilterSort(source, category, filter, filetype, results);

            // Return
            SendAPIMessage("content-files", results.ConvertToJObjects(new FormBuilder("*").Exclude(nameof(ContentFile.User))));
        }

        public static IQueryable<ContentFile> FilterSort(string source, string category, string filter, string filetype, IQueryable<ContentFile> results) {
            if (!string.IsNullOrEmpty(source) && source != "*") {
                results = results.Where(cf => cf.Source == source);
            }
            if (!string.IsNullOrEmpty(category) && category != "*") {
                var cat = Core.Util.EnumHelper.ParseEnum<ContentFile.ContentCategory>(category);
                if (category == "video") {
                    // Special exception for video: also include gif images
                    results = results.Where(cf => cf.FileCategory == cat || cf.FileExtension == "gif");
                } else {
                    results = results.Where(cf => cf.FileCategory == cat);
                }
            }
            if (!string.IsNullOrEmpty(filetype)) {
                filetype = filetype.ToLower();
                results = results.Where(cf => cf.FileExtension.ToLower() == filetype.ToLower());
            }
            if (!string.IsNullOrEmpty(filter)) {
                filter = filter.ToLower();
                results = results.Where(cf => cf.FileName.ToLower().Contains(filter) || cf.FileExtension.ToLower().Contains(filter));
            }

            // Order
            results = results.OrderByDescending(cf => cf.Created);
            return results;
        }

        [RequestHandler("/api/admin/content/files/create")]
        public void CreateContentFile() {
            this.RequireWriteARN();

            // Get the source
            var source = this.Params.String("source");
            if (source == null) throw new Exception("No source provided! A source must be provided via the api call.");
            
            var contentFile = Core.Data.DataCenter.UploadFile(this.Request.File(), this.User, source);
            this.DB.ContentFiles().Add(contentFile);
            this.DB.SaveChanges();

            SendAPIMessage("content-file-success", new {
                ContentFile = new {
                    PublicId = contentFile.PublicId,
                    ContentURL = contentFile.ContentURL
                }
            });

        }

        [RequestHandler("/api/admin/content/files/{publicId}/delete")]
        public void DeleteContentFile(string publicId) {
            this.RequireWriteARN();

            var contentFile = DB.ContentFiles().GetByPublicId(publicId);
            Core.Data.DataCenter.DeleteFile(DB, contentFile);
            DB.SaveChanges();
            SendAPIMessage("delete-file-success");
        }

        [RequestHandler("/api/admin/content/node/rename")]
        public void RenameNode() {
            this.RequireWriteARN();
            // Init
            var path = this.Params.String("path");
            var node = this.DB.ContentNodes().GetNodeByPath(path);
            var name = this.Params.String("name");
            // Rename
            var newPath = node.Rename(name);
            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Node = new { Path = newPath } });
        }

        [RequestHandler("/api/admin/content/node/duplicate")]
        public void DuplicateNode() {
            this.RequireWriteARN();
            // Init
            var path = this.Params.String("path");
            var node = this.DB.ContentNodes().Include(nameof(ContentNode.Parent)).GetNodeByPath(path);
            var name = ContentNode.TransformNodeName(this.Params.String("name"));

            var destination = this.Params.String("destination", $"{node.Parent.Path}");


            // New path
            var newPath = $"{destination}/{name}";
            newPath = newPath.Replace("//", "/");
            // Duplicate
            var newNode = node.Duplicate(DB, newPath);
            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Node = new { Path = newNode.Path } });
        }

        

        [RequestHandler("/api/admin/content/templates/create-from-page")] //?template={entity.public-id}&path={node.path}")]
        public void CreatePageFromTemplate() {
            this.RequireWriteARN();
            // Init
            var destinationNodeId = this.Params.String("node-id");
            var destinationNode = this.DB.ContentNodes().Include(nameof(ContentNode.Parent)).GetByPublicId(destinationNodeId);
            var name = ContentNode.TransformNodeName(this.Params.String("name"));
            var templateNodeId = this.Params.String("template");

            var templateNode = DB.ContentNodes().GetByPublicId(templateNodeId);

            // New path
            var newPath = $"{destinationNode.Path}/{name}";
            newPath = newPath.Replace("//", "/");

            // Duplicate
            var newNode = templateNode.Duplicate(DB, newPath);

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Node = new { Path = newNode.Path } });
        }


        [RequestHandler("/api/admin/content/layout/{publicId}/add-option")] //?template={entity.public-id}&path={node.path}")]
        public void LayoutAddOption(string publicId) {
            this.RequireWriteARN();
            // Init
            var layout = this.DB.ContentLayouts().GetByPublicId(publicId);
            var name = this.Params.String("name");

            layout.Options[name] = "true";

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Layout = new { PublicId = publicId} } );
        }

        [RequestHandler("/api/admin/content/layout/{publicId}/delete-option/{option}")] //?template={entity.public-id}&path={node.path}")]
        public void LayoutDeleteOption(string publicId,string option) {
            this.RequireWriteARN();
            // Init
            var layout = this.DB.ContentLayouts().GetByPublicId(publicId);


            layout.Options.Delete(option);

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Layout = new { PublicId = publicId } });
        }


        [RequestHandler("/api/admin/content/layout/{publicId}/option/{key}/edit")] //?template={entity.public-id}&path={node.path}")]
        public void LayoutEditOption(string publicId, string key) {
            this.RequireWriteARN();
            // Init
            var layout = this.DB.ContentLayouts().GetByPublicId(publicId);

            var option = new LayoutOption();
            option.Populate(this, new FormBuilder(Forms.Admin.EDIT));

            layout.Options[key] = option.Value;

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Layout = new { PublicId = publicId } });
        }


        [RequestHandler("/api/admin/content/chart/tree")]
        public void TreeContent() {

            // Init
            var path = this.Params.String("path","/");

            // Get node at path
            var node = this.DB.ContentNodes().Include("Children").Where(n => n.Path == path).SingleOrDefault();

            // Create the root element
            var treeRoot = new Core.Charts.TreeChartDataNode() {
                Name = node.Name,
                ChildrenCall = $"/api/admin/content/chart/tree?path={node.Path}"
            };

            // Add all children
            foreach(var child in node.Children.Where(n => n.NodeType == ContentNode.NODE_TYPE_NODE)) {
                var treeChild = new Core.Charts.TreeChartDataNode() {
                    Name = child.Name,
                    ChildrenCall = $"/api/admin/content/chart/tree?path={child.Path}"
                };
                if(child.NodeType == ContentNode.NODE_TYPE_NODE) {
                    treeChild.Link = $"/admin/content/page{child.Path}";
                }
                treeRoot.Children.Add(treeChild);
            }
            
            SendAPIMessage("tree-data", treeRoot);
        }


        [RequestHandler("/api/admin/content/node/{publicId}/children/sort")]
        public void SortContentNodes(string publicId) {
            this.RequireWriteARN();

            ContentNode entity = null;
            var contentNodes = this.DB.ContentNodes().Include(nameof(ContentNode.Children));
            // Init
            if (publicId == ContentNode.ROOT_NODE_NAME) {
                entity = contentNodes.GetRoot();
            } else {
                entity = contentNodes.GetByPublicId(publicId);
            }
            var nodeChildren = entity.Children.Where(c => c.NodeType == ContentNode.NODE_TYPE_NODE && c.Path != ContentNode.ENTITIES_PATH_NAME && c.IsSystemNode == false);
            var sorts = Params.StringArray("order", new string[] { }).ToList();

            string error = null;

            if (sorts.Count() == nodeChildren.Count()) {
                foreach (var child in nodeChildren) {
                    if (sorts.Contains(child.PublicId)) {
                        child.Sort = sorts.IndexOf(child.PublicId);
                    }
                    else {
                        error = "Node not found in Sort Order";
                    }
                }
            }
            else {
                error = "Wrong number of elements provided";
            }

            if (error!= null) {
                throw new BackendException("sort-error", error);
            }

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { ContentNode = new { PublicId = publicId } });
        }

        [RequestHandler("/api/admin/content/pages/list")]
        public void ListPages() {
            var path = this.Params.String("path");
            var hideSystem = this.Params.Bool("hide-system", true);
            if (path == null) path = "/";
            var pages = this.DB.ContentNodes().GetPagesAtPath(path);
            if (hideSystem) {
                pages = pages.Where(p => p.IsSystemNode == false);
            }
            var result = pages.OrderBy(p=>p.Name).Select(n => n.GetJSON(new FormBuilder(Forms.Admin.EXPORT)));
            // Return
            this.SendAPIMessage("success", new { Path = path, Pages = result });
        }

        [RequestHandler("/api/admin/content/node/{publicId}/move")]
        public void MovePage(string publicId) {
            this.RequireWriteARN();
            var path = this.Params.String("destination-path");

            var node = this.DB.ContentNodes().GetByPublicId(publicId);

            // New path
            var newPath = path + "/" + node.Name;

            // Duplicate
            var newNode = node.Duplicate(DB, newPath);
            newNode.Published = true;

            // Delete old
            this.DB.DeleteEntity(node);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("success", new { Path = path});
        }


        [RequestHandler("/api/admin/content/page/import-translation")]
        public void PageContentImportTranslation() {

            throw new NotImplementedException("This feature is not implemented yet. Needs advanced logic for all the content logic.");
            this.RequireWriteARN();


            var source = this.Params.String("sourceLanguage");
            var target = this.Params.String("targetLanguage");
            var ignoreTypes = this.Params.StringArray("ignoreTypes",new[] { "image"});

            // Get json
            var file = this.Request.File();
            if (!file.FileName.EndsWith(".json", StringComparison.Ordinal)) throw new BackendException("invalid-file", "The file must be a JSON (.json) file.");
            byte[] fileData = null;
            using (var binaryReader = new System.IO.BinaryReader(file.InputStream)) {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            var jobjects = Core.JSON.ParseJsonArrayAsJObjects(fileData);
            var log = new StringBuilder();

            foreach(var jobject in jobjects) {
               var pagePath = jobject["page"]["path"]?.ToString();
                var pageNode = this.DB.ContentNodes().Include("Children.Children").SingleOrDefault(cn => cn.Path == pagePath);
                if(pageNode == null) {
                    log.AppendLine("Not found: " + pagePath);
                    continue;
                }
                log.AppendLine("Importing: " + pagePath);
                int textPos = 0;
                var targetTranslationNode = pageNode.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION).SingleOrDefault(c => c.Value == target);
                if (targetTranslationNode != null) {
                    log.AppendLine("Ignoring text, already has a translation node");
                    continue;
                }

                ContentNode transNode = null;
                              
                foreach (var textNode in jobject["texts"]) {

                    var targetText = textNode["content"]["target"]?.ToString();
                    log.AppendLine("Importing text-: " + textPos++);
                    var nodeType = textNode["type"]?.ToString();
                    if (ignoreTypes.Contains(nodeType)) {
                        continue;
                    }
                    var sourceLanguage = textNode["content"]["sourceLanguage"].ToString();
                    var targetLanguage = textNode["content"]["targetLanguage"].ToString();

                    if (sourceLanguage != source) {
                        log.AppendLine("Ignoring text, wrong source lanuage: " + sourceLanguage);
                        continue;
                    }
                    if (targetLanguage != target) {
                        log.AppendLine("Ignoring text, wrong target lanuage: " + targetLanguage);
                        continue;
                    }

                    log.AppendLine("Creating new translation node with text: " + targetText);
                
                    // Create trans node if no 
                    if (transNode == null) {
                        transNode = pageNode.AddTranslation(target);
                    }

                    ContentNode newNode = new ContentNode();
                    newNode.NodeType = nodeType;
                    newNode.Value = targetText;
                    newNode.Sort = textPos;
                    if (nodeType == ContentNode.NODE_TYPE_TITLE) {
                        newNode.Title = targetText;
                    }

                    transNode.AddChild(newNode);

                }
                
            }  

            Log(log.ToString());
           
           // Save
           this.DB.SaveChanges();

            // Return
            SendAPIMessage("import-success", new { Log = log.ToString()});
        }
    }
}
