using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SandboxAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("sandbox");
        }

        #endregion

        [RequestHandler("/api/admin/sandbox/echo")]
        public void Echo() {
            Dictionary<string, object> req = new Dictionary<string, object>();
            int id = 0;
            foreach(string key in this.Context.Request.Form.Keys) {
                req.Add($"Form_{id++}[{key}]", this.Context.Request.Form[key]);
            }
            foreach(string key in this.Context.Request.QueryString.Keys) {
                req.Add($"QueryString_{id++}[{key}]", this.Context.Request.QueryString[key]);
            }
            foreach(string key in this.Context.Request.Files.Keys) {
                req.Add($"Files_{id++}[{key}]", this.Context.Request.Files[key].FileName + $" ({this.Context.Request.Files[key].ContentLength} bytes)");
            }
            foreach(string key in this.Context.Request.Cookies.Keys) {
                req.Add($"Cookies_{id++}[{key}]", this.Context.Request.Cookies[key].Value + $" ({this.Context.Request.Cookies[key].Path})");
            }
            SendAPIMessage("echo", req);
        }
        
        [RequestHandler("/api/admin/sandbox/delay")]
        public void Delay() {
            System.Threading.Thread.Sleep(2000);
            Echo();
        }


        [RequestHandler("/api/admin/sandbox/errors/api")]
        public void APIError() {
            throw new Exception("This is an unknown test exception.");
            SendAPIMessage("test", "success");
        }

    }
}
