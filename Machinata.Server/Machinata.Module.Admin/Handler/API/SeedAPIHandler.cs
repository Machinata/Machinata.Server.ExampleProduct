using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SeedAPIHandler : AdminAPIHandler {
        

        [RequestHandler("/api/admin/data/seed/dataset/{dataset}")]
        public void SeedDataset(string dataset) {
            this.RequireWriteARN();

            // Call model seed helper method
            Core.Model.ModelSeed.ProjectSeed(this.DB, dataset);
            // Return
            SendAPIMessage("seed-success");
        }

        
        
    }
}
