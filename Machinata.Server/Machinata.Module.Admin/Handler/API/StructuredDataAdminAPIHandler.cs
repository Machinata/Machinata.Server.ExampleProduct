
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Admin.Handler;

namespace Machinata.Module.Admin.Handler {


    public class StructuredDataAdminAPIHandler : Handler.CRUDAdminAPIHandler<StructuredDataConfig> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("structured-data");
        }

        #endregion

        [RequestHandler("/api/admin/structured-data/configs/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/structured-data/configs/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/structured-data/configs/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }





    }
}
