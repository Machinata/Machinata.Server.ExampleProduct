using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class TaskManagerAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("task-manager");
        }

        #endregion

        [RequestHandler("/api/admin/task-manager/scheduled-tasks/create")]
        public void Create() {
            this.RequireWriteARN();

            var entity = DB.ScheduledTasks().Create();
            entity.Context = DB;

            var availableTasks = TaskManager.GetAvailableTasks();

            var selectedName = this.Params.GetSelectedItem(TaskManager.GetAvailableTaskNames().Select(t => t.Name));
            var selectedType = availableTasks.Single(at => at.Name == selectedName);
            var config = ScheduledTask.GetTaskConfig(selectedType);

            entity.Name = selectedType.FullName;
            entity.Role = Core.Config.Environment + "-" + Core.Config.Role;
            entity.IntervalConfig = config.Interval;
            entity.Enabled = config.Enabled;

            this.DB.ScheduledTasks().Add(entity);

            this.DB.SaveChanges();

            SendAPIMessage("create-scheduled-tasks-success", new {
                ScheduledTask = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/task-manager/scheduled-task/{publicId}/edit")]
        public void ScheduledTasksEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var scheduledTask = this.DB.ScheduledTasks().GetByPublicId(publicId);
            scheduledTask.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            scheduledTask.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                ScheduledTask = new {
                    PublicId = scheduledTask.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/task-manager/scheduled-task/{publicId}/delete")]
        public void ScheduledTaskDelete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var scheduledTask = this.DB.ScheduledTasks().GetByPublicId(publicId);
            this.DB.ScheduledTasks().Remove(scheduledTask);
         
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success", new {
                ScheduledTask = new {
                    PublicId = scheduledTask.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/task-manager/start-task/{type}")]
        public void StartTask(string type) {
            this.RequireWriteARN();

            bool testMode = this.Params.Bool("test-mode", false);

            var task = TaskManager.CreateTaskInstance(type);

            // Test Mode?
            task.TestMode = testMode;

            // On test system start the batch task
            if (Core.Config.IsTestEnvironment) {
                task.Initialize();
            }
            task.Setup(null,"admin");
            TaskManager.RunTask(task);

         
            // Return
            this.SendAPIMessage("start-success", new {
                ScheduledTask = new {
                    PublicId = task.PublicId,
                    GUID = task.GUID
                }
            });
        }

        [RequestHandler("/api/admin/task-manager/start")]
        public void Start() {
            this.RequireWriteARN();

            TaskManager.StartSchedular();
            SendAPIMessage("start-success");
        }

        [RequestHandler("/api/admin/task-manager/stop")]
        public void Stop() {
            this.RequireWriteARN();

            TaskManager.StopSchedular();
            SendAPIMessage("stop-success");
        }

        [RequestHandler("/api/admin/task-manager/restart")]
        public void Restart() {
            this.RequireWriteARN();

            TaskManager.RestartSchedular();
            SendAPIMessage("restart-success");
        }

        [RequestHandler("/api/admin/task-manager/scheduled-tasks/auto-generate-missing")]
        public void ScheduledTaskAutoGenerate() {
            this.RequireWriteARN();

            // Existing Tasks
            var existing = this.DB.ScheduledTasks();

            // New Tasks
            var newTasks = ScheduledTask.CreateMissingScheduledTask(this.DB, existing);

            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("delete-success", new {
                ScheduledTasks =  newTasks.Select( t => new { PublicId = t.PublicId, Name = t.Name})
            });
        }

    }
}
