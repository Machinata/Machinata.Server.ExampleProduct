
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;
using Machinata.Core.Messaging;

namespace Machinata.Module.Admin.Handler {


    public class EmailTestAPIHandler : AdminAPIHandler {
      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("system");
        }

        #endregion
        

        [RequestHandler("/api/admin/system/tools/email/{name}/send-test-email")]
        public void SendTestEmail(string name) {
            var emailTemplate = MessageCenter.GetTestTemplates(this.DB).FirstOrDefault(t => t.Name == name);
            emailTemplate.Compile();
            emailTemplate.SendEmail(this.Params.String("email"), MailingUnsubscription.Categories.Testing, name);
            SendAPIMessage("send-success");

        }
        
        
    }
}
