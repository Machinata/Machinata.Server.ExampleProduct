using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class TaskManagerRemoteAPIAdminHandler : APIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("task-manager");
        }

        #endregion

        [RequestHandler("/api/admin/task-manager/remote-task/{publicId}/restart")]
        public void Restart(string publicId) {
            this.RequireWriteARN();

            // Task
            var task = this.DB.RemoteTasks().GetByPublicId(publicId);

            // Reset
            task.State = RemoteTask.RemoteTaskState.Waiting;
            task.Enabled = true;
            task.Started = null;
            task.Finished = null;
            task.StatusInfos = new Properties();
            
            // Save
            this.DB.SaveChanges();

            // Response
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/task-manager/remote-task/{publicId}/cancel")]
        public void Cancel(string publicId) {
            this.RequireWriteARN();

            // Task
            var task = this.DB.RemoteTasks().GetByPublicId(publicId);

            // Reset
            task.State = RemoteTask.RemoteTaskState.Canceling;
        
            // Save
            this.DB.SaveChanges();

            // Response
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/task-manager/remote-task/{publicId}/pause")]
        public void Pause(string publicId) {
            this.RequireWriteARN();

            // Task
            var task = this.DB.RemoteTasks().GetByPublicId(publicId);

            // Reset
            task.State = RemoteTask.RemoteTaskState.Paused;

            // Save
            this.DB.SaveChanges();

            // Response
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/task-manager/remote-task/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var task = this.DB.RemoteTasks().GetByPublicId(publicId);
            task.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            task.Validate();
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                Remote = new {
                    PublicId = task.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/task-manager/remote-task/{publicId}/delete")]
        public void Delete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var task = this.DB.RemoteTasks().GetByPublicId(publicId);
            this.DB.RemoteTasks().Remove(task);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success", new {
                RemoteTask = new {
                    PublicId = task.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/task-manager/remote-tasks/create")]
        public void Create() {
            this.RequireWriteARN();

            var entity = DB.RemoteTasks().Create();
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.Validate();

            this.DB.RemoteTasks().Add(entity);

            this.DB.SaveChanges();

            SendAPIMessage("create-remote-task-success", new {
                RemoteTask = new {
                    PublicId = entity.PublicId
                }
            });
        }



    }
}
