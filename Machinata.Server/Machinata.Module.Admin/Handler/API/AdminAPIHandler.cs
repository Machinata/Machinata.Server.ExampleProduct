
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

using Machinata.Core.Reporting;

using Machinata.Core.Data;


namespace Machinata.Module.Admin.Handler {
    
    public abstract class AdminAPIHandler : Core.Handler.APIHandler {
        
    }
    
    public abstract class CRUDAdminAPIHandler<T> : Core.Handler.CRUDAPIHandler<T> where T : ModelObject, new() {
        
    }
}
