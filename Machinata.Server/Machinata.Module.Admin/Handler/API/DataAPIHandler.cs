
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;

namespace Machinata.Module.Admin.Handler {


    public class DataAPIHandler : AdminAPIHandler {
      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("data");
        }

        #endregion
        

        [RequestHandler("/api/admin/data/chart/graph/model")]
        public void GraphModel() {

            var graph = new GraphChartData();

            var modelTypes = Core.Model.ModelContext.GetModelClassTypes();
            foreach (var type in modelTypes) {
                // Add node
                graph.Nodes.Add(new GraphChartDataNode() {
                    ID = type.Name,
                    Title = type.Name,
                    Star = true
                });
                // Add links
                foreach(var prop in type.GetProperties()) {
                    var propType = prop.PropertyType;
                    if (prop.PropertyType.IsGenericType) propType = prop.PropertyType.GenericTypeArguments.First();
                    if(modelTypes.Contains(propType)) {
                        graph.Nodes.Add(new GraphChartDataNode() {
                            ID = type.Name+"."+prop.Name,
                            Title =  type.Name+"."+prop.Name
                        });
                        graph.Links.Add(new GraphChartDataLink() {
                            Source = type.Name,
                            Target = type.Name+"."+prop.Name,
                            Title = prop.Name
                        });
                        graph.Links.Add(new GraphChartDataLink() {
                            Source = type.Name+"."+prop.Name,
                            Target = propType.Name,
                            Title = type.Name+"."+prop.Name
                        });
                    }
                }
            }
            
            SendAPIMessage("graph-data", graph);
        }
        
        
    }
}
