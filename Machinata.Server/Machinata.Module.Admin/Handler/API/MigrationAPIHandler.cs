
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;

namespace Machinata.Module.Admin.Handler {


    public class MigrationAPIHandler : AdminAPIHandler {
      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("data");
        }

        #endregion
        

        [RequestHandler("/api/admin/data/migration/{publicId}/execute-recommended-sql")]
        public void ExecuteRecommendedSQL(string publicId) {
            
            this.RequireAdminIP();
            this.RequireWriteARN();

            // Analyze
            ModelMigration migration = new ModelMigration(this.DB);
            var migrations = migration.Analyze();
            var entity = migrations.SingleOrDefault(m => m.PublicId == publicId);

            var sql = entity.RecommendedSQL;
            var ret = 0;
            if(sql != null) {
                ret = this.DB.ExecuteSQL(sql);
            }

            SendAPIMessage("success", new { SQL=sql, ExitCode=ret });
        }
        
        
    }
}
