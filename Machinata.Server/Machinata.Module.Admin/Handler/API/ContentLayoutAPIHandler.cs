
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class ContentLayoutAPIHandler : CRUDAdminAPIHandler<ContentLayout> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        [RequestHandler("/api/admin/content/layouts/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/content/layout/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/content/layout/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        

    }
}
