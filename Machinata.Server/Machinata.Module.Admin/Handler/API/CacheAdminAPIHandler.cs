
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;

namespace Machinata.Module.Admin.Handler {


    public class CacheAdminAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("system");
        }

        #endregion

       

        [RequestHandler("/api/admin/system/tools/cache/reset")]
        public void Reset() {
            Core.Caching.CacheReset.FireEvent();

            this.SendAPIMessage("succes");
        }
        
        
    }
}
