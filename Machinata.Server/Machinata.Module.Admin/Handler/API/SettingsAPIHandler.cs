
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class SettingsAPIHandler : CRUDAdminAPIHandler<Core.Model.DynamicConfig> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        #region Dynamic Config Settings CRUD

        [RequestHandler("/api/admin/config/settings/create")]
        public void Create() {
            CRUDCreate();
            Core.Config.Dynamic.ResetCache();
        }

        [RequestHandler("/api/admin/config/setting/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
            Core.Config.Dynamic.ResetCache();
        }
        
        [RequestHandler("/api/admin/config/setting/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
            Core.Config.Dynamic.ResetCache();
        }

        #endregion

        #region Exposed Core Configs

        [RequestHandler("/api/admin/config/core/supported-languages")]
        public void SupportedLanguages() {
            SendAPIMessage("config", new { Key = nameof(Core.Config.LocalizationSupportedLanguages), Value = Core.Config.LocalizationSupportedLanguages });
        }

        #endregion

    }
}
