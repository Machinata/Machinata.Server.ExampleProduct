
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Localization;

namespace Machinata.Module.Admin.Handler {


    public class LocalizationApiHandler : AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        [RequestHandler("/api/admin/config/localization/update")]
        public void Update() {
            this.RequireWriteARN();

            var textIds = this.Params.StringArray("text-id", new string[] { }).ToList();
            var languages = this.Params.StringArray("language", new string[] { }).ToList();
            var packages = this.Params.StringArray("package", new string[] { }).ToList();
            var values = this.Params.StringArray("value", new string[] { }).ToList();

            for (int i = 0; i < textIds.Count(); i++) {

                var language = languages[i];
                var textId = textIds[i];
                var package = packages[i];
                var value = values[i];

                // existing entry
        //        var defaultValue = Text.GetTexts(textId, language).LastOrDefault(t=>t.SourceType != Text.SourceTypes.Database);
                var entity = LocalizationText.FindLocalizationText(this.DB, textId, language);


                if (string.IsNullOrEmpty(value) && entity != null) {
                    this.DB.LocalizationTexts().Remove(entity);

                    // Save
                    this.DB.SaveChanges();

                    // Unregister
                    Text.UnRegisterText(textId, language, Text.SourceTypes.Database);
                } else if (!string.IsNullOrEmpty(value) && entity == null) { 
                
                    // new
                    entity = new LocalizationText();
                    entity.Language = languages[i];
                    entity.TextId = textId;
                    entity.Package = package;
                    entity.Value = value;
                    entity.Validate();
                    this.DB.LocalizationTexts().Add(entity);
                    // Register
                    Text.RegisterText(entity.Source, package, textId, value, language, Text.SourceTypes.Database);
                } else if (entity != null && entity.Value != value){

                    // Update
                    entity.Value = value;

                    // Update/Register
                    var currentDbValue = Text.GetTexts(textId, language).Last(t => t.SourceType == Text.SourceTypes.Database);
                    currentDbValue.Value = value;

                }

                // Save
                this.DB.SaveChanges();

            }

            SendAPIMessage("update-success");
        }



    }
}
