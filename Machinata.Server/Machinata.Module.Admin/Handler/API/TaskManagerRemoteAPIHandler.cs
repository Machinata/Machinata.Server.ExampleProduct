using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class TaskManagerRemoteAPIHandler : APIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("task-manager/remote-tasks");
        }

        #endregion

     
        [RequestHandler("/api/task-manager/remote-tasks/get")]
        public void Get() {

            var target = this.Params.String("target");

            var remoteTasks = RemoteTaskManagerAPI.GetTaskList(this.DB, target);

            var response = remoteTasks.ToList().
                Select(rt =>
                    new {
                        Name = rt.Name,
                        PublicId =  rt.PublicId,
                        Target = rt.Target,
                        State = rt.State.ToString(),
                        Parameters =  rt.Parameters
                    });
            this.SendAPIMessage("get-success", new { Tasks = response });

        }



        [RequestHandler("/api/task-manager/remote-tasks/acquire/{publicId}")]
        public void Acquire(string publicId) {

            var target = this.Params.String("target");
            var role = this.Params.String("role");
            var name = this.Params.String("name");

            var remoteTask = RemoteTaskManagerAPI.AqcuireRemoteTask(this.DB, publicId, target);

            if (remoteTask != null) {

                this.SendAPIMessage("success", new {
                    Task = new {
                        remoteTask.PublicId,
                        remoteTask.Parameters,
                        remoteTask.Name,
                    }
                });
            } else {
                this.SendAPIMessage("no-task");
            }
        }



        [RequestHandler("/api/task-manager/remote-tasks/update/{publicId}")]
        public void Update(string publicId) {

            var target = this.Params.String("target");
            var role = this.Params.String("role");
            var name = this.Params.String("name");
            var state = this.Params.String("state");
            var eta = this.Params.String("eta");
            var message = this.Params.String("message");

            var task = RemoteTaskManagerAPI.UpdateTask(this.DB, publicId, target, role, state, eta, message);
           

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new {
                Task = new {
                    Status = task.State.ToString()
                }
            });

        }
        
    }
}
