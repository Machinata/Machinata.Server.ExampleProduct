
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;

namespace Machinata.Module.Admin.Handler {


    public class RoutesAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("route");
        }

        #endregion
        

        [RequestHandler("/api/admin/route/chart/tree")]
        public void TreeRoutes() {
            
            // Init
            var path = this.Params.String("path");
            var routes = Core.Routes.Route.GetAllRoutes();
            if (path == null) path = "/";

            // Create the root element
            var treeRoot = new TreeChartDataNode() {
                Name = path,
                ChildrenCall = $"/api/admin/route/chart/tree?path={path}"
            };

            // Build a list of all subpaths
            // We don't just use the children because we want to see the full sub-paths
            int pathDepth;
            List<Core.Routes.Route> matchingRoutesAtPath = null;
            if (path == "/") {
                matchingRoutesAtPath = routes;
                pathDepth = 1;
            } else {
                matchingRoutesAtPath = routes.Where(r => r.TrimmedPath.StartsWith(path)).ToList();
                pathDepth = path.Split('/').Length+1;
            }
            List<string> subPaths = new List<string>();
            foreach(var route in matchingRoutesAtPath) {
                // Rebuild the subpath
                var subPath = string.Join("/",route.PathSegs.Take(pathDepth));
                // Add to our subpaths, but only if not already added and not self and not root
                if (!subPaths.Contains(subPath) && subPath != path && route.Path != "/") subPaths.Add(subPath);
            }
            
            // Add all children
            foreach(var subPath in subPaths) {
                var name = subPath.Split('/').Last();
                var matchingRoute = Core.Routes.Route.GetFirstRouteAtTrimmedPath(subPath);
                if (matchingRoute == null) {
                    name = $"({name})";
                }
                var treeChild = new TreeChartDataNode() {
                    Name = name,
                    ChildrenCall = $"/api/admin/route/chart/tree?path={subPath}"
                };
                if(matchingRoute != null) {
                    treeChild.Link = $"/admin/routes/route?uid={matchingRoute.UID}";
                    if(matchingRoute.AliasFor != null) {
                        treeChild.Name += $" > {matchingRoute.AliasFor}";
                    }
                }
                treeRoot.Children.Add(treeChild);
            }
            
            SendAPIMessage("tree-data", treeRoot);
        }
        
    }
}
