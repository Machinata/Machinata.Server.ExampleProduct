
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class AccessGroupAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("security");
        }

        #endregion

        [RequestHandler("/api/admin/security/access-group/{publicId}/delete")]
        public void Delete(string publicId) {
            this.RequireSuperuser();

            this.DB.AccessGroups().RemoveByPublicId(publicId);
            this.DB.SaveChanges();
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/security/access-group/{publicId}/user/{userId}/toggle")]
        public void ToggleUser(string publicId, string userId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.AccessGroups().Include("Users").GetByPublicId(publicId);
            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                user.AddToAccessGroup(entity);
            } else {
                user.RemoveFromAccessGroup(entity);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/security/access-group/{publicId}/policy/{policyId}/toggle")]
        public void TogglePolicy(string publicId, string policyId) {
            this.RequireSuperuser();

            // Init
            var entity = this.DB.AccessGroups().Include("AccessPolicies").GetByPublicId(publicId);
            var policy = this.DB.AccessPolicies().GetByPublicId(policyId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                entity.AddPolicy(policy);
            } else {
                entity.RemovePolicy(policy);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/security/access-group/create")]
        public void Create() {
            this.RequireSuperuser();

            // Create the new user
            var entity = new AccessGroup();
            entity.Populate(this,new FormBuilder(Forms.Admin.CREATE));
            entity.Validate();
            this.DB.AccessGroups().Add(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("create-success", new {
                Group = new {
                    Name = entity.Name,
                    PublicId = entity.PublicId
                }
            });
        }
        
        [RequestHandler("/api/admin/security/access-group/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireSuperuser();

            // Make changes and save
            var entity = this.DB.AccessGroups().GetByPublicId(publicId);
            entity.Populate(this,new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success");
        }
        
    }
}
