
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Machinata.Core.Handler;
//using Machinata.Core.Templates;
//using Machinata.Core.Util;
//using Machinata.Core.Model;
//using Machinata.Core.Exceptions;
//using Machinata.Core.Builder;

//namespace Machinata.Module.Admin.Handler {
    
//    public class ThemeAPIHandler : AdminAPIHandler {
        
//        #region Handler Policies

//        [PolicyProvider]
//        public static List<AccessPolicy> PolicyProvider() {
//            return AccessPolicy.GetDefaultAdminPolicies("theme");
//        }

//        #endregion

//        [RequestHandler("/api/admin/theme/create")]
//        public void CreateTheme() {
//            this.RequireWriteARN();

//            CheckDBThemesEnabled();

//            // Create the new entity
//            var entity = new Theme();
//            //entity.LEGACY_LoadProperties();
//            entity.Populate(this,new FormBuilder(Forms.Admin.CREATE));
//            entity.Validate();
//            // Save
//            this.DB.Themes().Add(entity);
//            this.DB.SaveChanges();
//            // Return
//            SendAPIMessage("create-success", new {
//                Theme = new {
//                    Name = entity.Name,
//                    PublicId = entity.PublicId
//                }
//            });
//        }


//        [RequestHandler("/api/admin/theme/{publicId}/edit")]
//        public void EditTheme(string publicId) {
//            this.RequireWriteARN();

//            CheckDBThemesEnabled();

//            // Make changes and save
//            var entity = this.DB.Themes().GetByPublicId(publicId);
//            entity.Populate(this,new FormBuilder(Forms.Admin.EDIT));
//            entity.Validate();
//            this.DB.SaveChanges();
//            // Reset theme cache
//            Core.Model.Theme.ClearThemeCache();
//            // Return
//            SendAPIMessage("edit-success");
//        }

//        [RequestHandler("/api/admin/theme/{publicId}/delete")]
//        public void DeleteTheme(string publicId) {
//            this.RequireWriteARN();

//            CheckDBThemesEnabled();

//            // Delete and save
//            var entity = this.DB.Themes().GetByPublicId(publicId);
//            this.DB.DeleteEntity(entity);
//            this.DB.SaveChanges();
//            // Reset theme cache
//            Core.Model.Theme.ClearThemeCache();
//            // Return
//            SendAPIMessage("delete-success");
//        }

//        [RequestHandler("/api/admin/theme/{publicId}/set-default")]
//        public void SetDefaultTheme(string publicId) {
//            this.RequireWriteARN();

//            CheckDBThemesEnabled();

//            // Make changes and save
//            var entity = this.DB.Themes().GetByPublicId(publicId);
//            foreach(var theme in this.DB.Themes()) {
//                theme.SetAsDefault = false;
//            }
//            entity.SetAsDefault = true;
//            this.DB.SaveChanges();
//            // Reset theme cache
//            Core.Model.Theme.ClearThemeCache();
//            // Return
//            SendAPIMessage("edit-success");
//        }



//        private void CheckDBThemesEnabled() {
//            //if (Core.Config.ThemesLoadFromDB == false) {
//            //    throw new BackendException("theme-error", "DB Themes are deactivated");
//            //}

//            throw new BackendException("theme-error", "DB Themes are deactivated");
//        }

//    }
//}
