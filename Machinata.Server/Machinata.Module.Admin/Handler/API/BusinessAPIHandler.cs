
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class BusinessApiHandler : AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("business");
        }

        #endregion

        [RequestHandler("/api/admin/businesses/create")]
        public void BusinessCreate() {
            this.RequireWriteARN();


            // Create the new business
            Business business = Business.CreateBusiness(this);
            this.DB.SaveChanges();

            SendAPIMessage("create-business-success", new {
                Business = new {
                    PublicId = business.PublicId
                }
            });
        }

      

        [RequestHandler("/api/admin/businesses/business/{publicId}/edit")]
        public void BusinessEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Businesses().GetByPublicId(publicId);
            var oldName = entity.Name;

            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();

            if (oldName != entity.Name) {
                entity.NameChanged(oldName);
            }

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Business = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/businesses/business/{publicId}/delete")]
        public void BusinessDelete(string publicId) {
            this.RequireWriteARN();

            // Find enitty
            var entity = this.DB.Businesses().GetByPublicId(publicId);

            entity.Archived = true;

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }
        
        

        [RequestHandler("/api/admin/businesses/business/{businessId}/addresses/create")]
        public void BusinessAddressCreate(string businessId) {
            this.RequireWriteARN();
            var business = DB.Businesses().GetByPublicId(businessId);

            var address = new Address();
            address.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            address.Validate();

            business.Addresses.Add(address);
            this.DB.SaveChanges();

            SendAPIMessage("create-address-success", new {
                Business = new { 
                    PublicId = business.PublicId
                },
                Address = new {
                    PublicId = address.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/businesses/business/{publicId}/users/{userId}/toggle")]
        public void BusinessUsersToggle(string publicId,string userId) {
            // Make changes and save
            var entity = this.DB.Businesses().GetByPublicId(publicId);

            // Only admins can do that
            this.ValidateARN("/admin/business");

            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value", false);
            if (enable) {
                entity.AddUser(user);
            } else {
                entity.RemoveUser(user);
            }
            this.DB.SaveChanges();
         
            // Return
            SendAPIMessage("toggle-success");
        }

       
        [RequestHandler("/api/admin/businesses/business/{publicId}/users/add-new")]
        public void AddNewUser( string publicId) {

            this.RequireWriteARN();
            var newUser = new User();
            var business = this.DB.Businesses().Include(nameof(Business.Users)).GetByPublicId(publicId);

            // Username == Email
            newUser.Username = this.Params.String("email");
            newUser.Email = this.Params.String("email");
            newUser.Name = this.Params.String("name");

            // Business
            newUser.Businesses = new List<Business>();
            newUser.Businesses.Add(business);

            this.DB.Users().Add(newUser);

            // Groups
            var groups = this.DB.AccessGroups().GetByPublicIds(this.Params.StringArray("selected", new string[] { }));
            foreach (var group in groups) {
                newUser.AddToAccessGroup(group);
            }

            // Set IP
            newUser.Settings[User.SETTING_KEY_IP] = Core.Util.HTTP.GetRemoteIP(this.Context);

            // Validate
            newUser.Validate();

            this.DB.SaveChanges();

            // User Activation Email
            newUser.SendUserActivationEmail(this.DB);

            // Send Notification Email
            Core.Messaging.MessageCenter.SendMessageToNotificationEmail("User Created", $"User '{newUser.Username}' has been created by '{this.User.Username}'", this.PackageName);

            // Return
            SendAPIMessage("create-success", new {
                Entity = new {
                    Username = newUser.Username,
                    PublicId = newUser.PublicId
                }
            });

            Core.Model.User.FireUserAddedEvent(newUser);

        }


        /// <summary>
        /// A product or module can implement its own business creation logic
        /// if no IBusinessCreator is found the standard method is called
        /// if multiple found an excpetion will be thrown
        /// </summary>
        [RequestHandler("/api/admin/businesses/tools/create")]
        public void ToolsBusinessCreate() {
            this.RequireWriteARN();

            // Create the new business
            var businessCreators = Core.Reflection.Types.GetMachinataTypesImplementingInterface(typeof(IBusinessCreator));

            Business business = null;

            // Undefined if we have more than one
            if (businessCreators.Count > 1) {
                throw new BackendException("business-creator-error", "Found more than one business creator methods please fix");
            } else if (businessCreators.Count == 0) {
                business = Business.CreateBusiness(this);
            } else if (businessCreators.Count == 1) {
                var creator = businessCreators.First();
                var instance = Core.Reflection.Types.CreateInstance<IBusinessCreator>(creator);
                business = instance.CreateBusiness(this);
            }

            this.DB.SaveChanges();

            SendAPIMessage("create-business-success", new {
                Business = new {
                    PublicId = business.PublicId
                }
            });
        }

    }

    public interface IBusinessCreator {
        Business CreateBusiness(Core.Handler.Handler handler);
    }
}
