
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class AddressApiHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("address");
        }

        #endregion

        private Business _getBusinessFromAddress(Address entity) {
            return DB.Businesses().Include("Addresses").FirstOrDefault(b => b.Addresses.Select(a => a.Id).Contains(entity.Id));
        }
        
        [RequestHandler("/api/admin/address/{publicId}/edit")]
        public void AddressEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Addresses().GetByPublicId(publicId);
            Business business = _getBusinessFromAddress(entity);

            EditAddress(this,entity, business,Forms.Admin.EDIT);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Address = new {
                    PublicId = entity.PublicId
                }
            });
        }

        /// <summary>
        /// Reads data from Context to edit address, additionally sets default shipping/billing address
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="business">The business.</param>
        /// <param name="form">The form.</param>
        /// <exception cref="Backend404Exception">address-no-business;This address has no business attached to it</exception>
        public static void EditAddress(APIHandler handler, Address entity, Business business,string form) {
            int? oldDefaultBillingValue = business.DefaultBillingAddress?.Id;
            int? oldDefaultShippingValue = business.DefaultShipmentAddress?.Id;
            entity.Populate(handler, new FormBuilder(form));

            var isShippingDefault = handler.Params.Bool("shipping-default", false);
            var isBillingDefault = handler.Params.Bool("billing-default", false);

            if (business == null) {
                throw new Backend404Exception("address-no-business", "This address has no business attached to it");
            }

            if (isBillingDefault) {
                business.DefaultBillingAddress = entity;
            } else if (oldDefaultBillingValue.HasValue && entity.Id == oldDefaultBillingValue) {
                business.DefaultBillingAddress = null;
            }
            if (isShippingDefault) {
                business.DefaultShipmentAddress = entity;
            } else if (oldDefaultShippingValue.HasValue && entity.Id == oldDefaultShippingValue) {
                business.DefaultShipmentAddress = null;
            }

            entity.Validate();
        }

        [RequestHandler("/api/admin/address/{publicId}/delete")]
        public void AddressDelete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Addresses().GetByPublicId(publicId);
            var business = _getBusinessFromAddress(entity);

            DeleteAddress(entity, business);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        public static void DeleteAddress(Address entity, Business business) {
            business.LoadFirstLevelNavigationReferences();
            business.LoadFirstLevelObjectReferences();

            // Archive the address
            entity.Archived = true;

            // Remove if default shipping or billing
            if (business.DefaultBillingAddress == entity) {
                business.DefaultBillingAddress = null;
            }
            if (business.DefaultShipmentAddress == entity) {
                business.DefaultShipmentAddress = null;
            }
        }


    }
}
