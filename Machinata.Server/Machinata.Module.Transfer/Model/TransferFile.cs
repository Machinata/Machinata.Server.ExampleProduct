using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Machinata.Core.Model;
using Machinata.Core.Builder;

namespace Machinata.Module.Transfer.Model {

    [Serializable()]
    [ModelClass]
    public class TransferFile : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

        public enum TransferTypes : short {
            Upload = 10
        }
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public TransferFile() {
                
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        public string Path { get; set; }
       

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public TransferTypes Type { get; set; }

        [Column]
        public long Size { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Name { get; set; }


        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public Transmission Transmission { get; set; }
    
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string SizeDisplay
        {
            get
            {
                return System.Math.Round(this.Size / 1024d, 1) + " KB";
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


      
        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextTransferFileExtenions {
        [ModelSet]
        public static DbSet<TransferFile> TransferFiles(this Core.Model.ModelContext context) {
            return context.Set<TransferFile>();
        }
    }

    #endregion



}
