using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Transfer.Model {

    [Serializable()]
    [ModelClass]
    public class TransferRecipient : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

      
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public TransferRecipient() {
                
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        public string EmailEncrypted { get; set; }
       

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public Transmission Transmission { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [MinLength(3)]
        [MaxLength(80)]
        [Placeholder("user@domain.com")]
        [DataType(DataType.EmailAddress)]
        [NotMapped]
        public string Email
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.EmailEncrypted);
            }
            set
            {
                this.EmailEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }
        
        public string DisplayName
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Name) ? this.Name : this.Email;
            }
         
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextTransferRecipientExtenions {
        [ModelSet]
        public static DbSet<TransferRecipient> TransferRecipients(this Core.Model.ModelContext context) {
            return context.Set<TransferRecipient>();
        }
    }

    #endregion



}
