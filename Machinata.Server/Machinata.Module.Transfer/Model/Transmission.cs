using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Linq;
using Machinata.Core.Util;
using Machinata.Core.Templates;
using static Machinata.Core.Model.MailingUnsubscription;
using Machinata.Module.Transfer.Logic;
using Machinata.Core.Cards;

namespace Machinata.Module.Transfer.Model {

    [Serializable()]
    [ModelClass]
    [Table("TransferTransmission")]
    public class Transmission : ModelObject, IEnabledModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

        public enum TransmissionStatuses : short {
            Created = 10,
            Initialized = 20,
            Published = 30,
            Deleted = 40, // by user
            Removed = 100 // by system,expiration
        }
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Transmission() {
                
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Title { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Message { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public DateTime? Expires { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public bool Enabled { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Hash { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public TransmissionStatuses Status { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public User Sender { get; set; }
        

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public ICollection<TransferRecipient> Recipients { get; set; }

        [Column]
        public ICollection<TransferFile> Files { get; set; }



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder()]
        public string Name
        {
            get
            {
                return this.PublicId;
            }
        }

        [FormBuilder()]
        [FormBuilder(Forms.System.LISTING)]
        public string ShareId
        {
            get
            {
                return this.Hash;
            }
        }

        [FormBuilder()]
        public string Details
        {
            get
            {
                var details =  $"{this.Files.Count} " + "files".TextVar() + $" {this.TotalSizeDisplay}, " + "transfer.will-delete-warning".TextVar();
                if (this.Expires.HasValue) {
                    details += $" {this.Expires.Value.ToDefaultTimezoneDateString()}";
                }
                return details;
            }
        }

        [FormBuilder()]
        public long TotalSize
        {
            get
            {
                return this.Files.Sum(f => f.Size);
            }
        }

        [FormBuilder()]
        public string TotalSizeDisplay
        {
            get
            {
                return System.Math.Round(this.TotalSize / 1024d, 1) + " KB";
            }
        }

        [FormBuilder()]
        public bool AllFilesCompleted
        {
            get
            {
                return this.Files.All(f => f.Path != null);
            }
        }


        [FormBuilder(Forms.Admin.LISTING)]
        public string RecipientName
        {
            get
            {
                return this.Recipients?.FirstOrDefault()?.Name;
            }
        }
        
        [FormBuilder()]
        public string DownloadPath
        {
            get
            {
                var link = Config.DownloadLinkBase;
                link = link.Replace("{id}", this.Hash);
                return link;
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public string DownloadLink
        {
            get
            {
                var link = Config.DownloadLinkBase;
                link = link.Replace("{id}", this.Hash);
                return Core.Config.PublicURL + link;
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            return this.Title;
        }

        public override CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Subtitle(this.Files.Count() +" "+ "files".TextVar())
                .Sub(this.TotalSizeDisplay);
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        public TransferFile GetFile(string fileName) {
            return this.Files.SingleOrDefault(f => f.Name == fileName);
        }

        public TransferRecipient GetRecipient(string email) {
            return this.Recipients.SingleOrDefault(f => f.Email == email);
        }

        public void SendNotification() {
            foreach (var recipient in this.Recipients) {
                var template = new EmailTemplate(this.Context, "transfer/notification");
                template.InsertVariables("recipient", recipient);
                template.InsertVariables("transmission", this);
                template.InsertVariables("sender", this.Sender);
              
                template.Subject = Core.Localization.Text.GetTranslatedTextById("transfer.notification-subject");
                template.Subject = template.Subject.Replace("{title}", this.Title);
                template.Subject = template.Subject.Replace("{sender}", this.Sender.Name);
                template.SendEmail(
                    email: recipient.Email,
                    unsubsribeType: Categories.Transfer,
                    unsubscribeSubtype: "notification",
                    async: true);
                           
            }
        }

        /// <summary>
        /// Deletes all its TransferFiles and TransferRecipeients
        /// All local fils and directories
        /// </summary>
        public static void Delete(Transmission transmission) {

            TransferFileSystemLogic.DeleteAll(transmission.Hash);

            // Entities
            foreach(var file in transmission.Files.ToList()) {
                transmission.Context.DeleteEntity(file);

            }

            foreach (var recipient in transmission.Recipients.ToList()) {
                transmission.Context.DeleteEntity(recipient);
            }

            transmission.Context.DeleteEntity(transmission);
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextTransmissionExtenions {
        [ModelSet]
        public static DbSet<Transmission> Transmissions(this Core.Model.ModelContext context) {
            return context.Set<Transmission>();
        }

      
        public static Transmission GetByToken<T>(this IQueryable<T> query, string token, bool throwExceptions = true) where T : Transmission {
            var ret = query.Where(e => e.Hash == token).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("entity-not-found", $"The entity {typeof(T).Name} with ID {token} could not be found.");
            return ret;
        }

    }

    #endregion



}
