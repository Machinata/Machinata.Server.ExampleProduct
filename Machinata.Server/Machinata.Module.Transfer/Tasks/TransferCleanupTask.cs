using Machinata.Core.Messaging;
using Machinata.Core.Model;
using Machinata.Module.Transfer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Transfer.Tasks {

    public class TransferCleanupTask : Core.TaskManager.Task {
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        public override void Process() {
            var now = DateTime.UtcNow;
            var expired = this.DB.Transmissions()
                .Include(nameof(Transmission.Files))
                .Include(nameof(Transmission.Recipients))
                .Where(t => t.Expires < now);
            this.Log($"Found {expired.Count()} expired transmissions to delete");
            foreach (var transmission in expired.ToList()) {
                this.Log($"Deleting {transmission.Details} ...");
                try {
                    Transmission.Delete(transmission);
                    this.DB.SaveChanges();
                    this.Log($"Succesfully deleted {transmission.Details} ...");
                }
                catch (Exception e) {
                    LogWithWarning("Could not delete transmission", e);
                }
            }
        }
    }
}
