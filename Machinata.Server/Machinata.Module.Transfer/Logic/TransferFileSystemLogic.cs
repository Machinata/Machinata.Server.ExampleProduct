using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Module.Transfer.Logic {
    public class TransferFileSystemLogic  {

          

        internal static bool ChunkExists(string token, string fileName, int chunkNumber) {
            return (File.Exists(GetChunkFileName(token, fileName, chunkNumber)));
        }

        internal static string GetChunkFileName(string identifier, string fileName, int chunkNumber ) {
            return Path.Combine(Config.TransferFilePath, identifier, fileName  + "_" +  chunkNumber.ToString());
        }

      

        internal static void SaveFile(Stream stream, string token, string fileName, int chunkNumber) {
            // Root directory
            if (!Directory.Exists(Config.TransferFilePath)) {
                Directory.CreateDirectory(Config.TransferFilePath);
            }
            string transmissionDirectory = GetDownloadDirectory(token);
            if (!Directory.Exists(transmissionDirectory)) {
                Directory.CreateDirectory(transmissionDirectory);
            }

            var filePath = GetChunkFileName(token, fileName, chunkNumber);

            using (FileStream fileStream = File.Create(filePath, (int)stream.Length)) {
                // Initialize the bytes array with the stream length and then fill it with data
                byte[] bytesInStream = new byte[stream.Length];
                stream.Read(bytesInStream, 0, bytesInStream.Length);
                // Use write method to write to the file specified above
                fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                //Close the filestream
                fileStream.Close();
            }

        }

        private static string GetDownloadDirectory(string token) {

            // Transmisstion directory
            return Path.Combine(Config.TransferFilePath, token);
        }

        internal static void DeleteChunks(string token, string fileName, int chunkCount) {
            var transmissionDirectory = Path.Combine(Config.TransferFilePath, token);
            if (!Directory.Exists(transmissionDirectory)) {
                return;
            }

            for (int chunkNumber = 1; chunkNumber <= chunkCount; chunkNumber++) {
                if (ChunkExists(token, fileName, chunkNumber)) {
                    var filePath = GetChunkFileName(token, fileName, chunkNumber);
                    File.Delete(filePath);
                }
            }
        }

        internal static string ConsolidateFile(string token, int chunkCount, string fileName) {
            var path = Path.Combine(Config.TransferFilePath, token, fileName);
            using (var destStream = File.Create(path, 15000)) {
                for (int chunkNumber = 1; chunkNumber <= chunkCount; chunkNumber++) {
                    var chunkFileName = GetChunkFileName(token, fileName, chunkNumber);
                    using (var sourceStream = File.OpenRead(chunkFileName)) {
                        sourceStream.CopyTo(destStream);
                    }
                }
                destStream.Close();
            }
            return path;
        }


        internal static bool AllPartsAvailable(string token, string fileName, int chunkCount) {
            for (int chunkNumber = 1; chunkNumber <= chunkCount; chunkNumber++) {
                if (!ChunkExists(token, fileName, chunkNumber)) {
                    return false;
                }
            }
            return true;
        }

        internal static void DeleteAll(string hash) {
            var path = GetDownloadDirectory(hash);
            var directory = new DirectoryInfo(path);
            if (directory.Exists) {
                directory.Delete(true);
            }
        }
    }
}
