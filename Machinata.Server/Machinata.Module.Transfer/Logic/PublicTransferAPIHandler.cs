using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Machinata.Core.Model;
using Machinata.Module.Transfer.Model;
using Machinata.Core.Handler;
using Machinata.Core.Exceptions;
using System.Collections.Concurrent;
using Machinata.Core.Util;

namespace Machinata.Module.Transfer.Logic {
    public abstract class PublicTransferAPIHandler: APIHandler {


        public static Model.Transmission CreateTansmission(ModelContext db, User sender) {

            var transmission = new Transfer.Model.Transmission();
            transmission.Sender = sender;
            transmission.Hash = Guid.NewGuid().ToString();
            transmission.Status = Model.Transmission.TransmissionStatuses.Created;

            db.Transmissions().Add(transmission);

            return transmission;
        }

        public void Upload() {
            var token = this.Params.String("upload_token");
            var chunkNumber = this.Params.Int("resumableChunkNumber", -1);
            var chunkCount = this.Params.Int("resumableTotalChunks", -1);
            var fileName = this.Params.String("resumableFilename", null);


            // Save even if already uploaded
            TransferFileSystemLogic.SaveFile(this.Context.Request.Files[0].InputStream, token, fileName, chunkNumber);

            Transmission transmission = null;

            lock (_dbLock.GetOrAdd(token + fileName, new LockEntry())) {
                if (TransferFileSystemLogic.AllPartsAvailable(token, fileName, chunkCount)) {

                    var path = TransferFileSystemLogic.ConsolidateFile(token, chunkCount, fileName);
                    transmission = this.DB.Transmissions()
                        .Include(nameof(Transmission.Files))
                        .Include(nameof(Transmission.Recipients))
                        .GetByToken(token);
                    var file = transmission.GetFile(fileName);
                    file.Path = path;
                    TransferFileSystemLogic.DeleteChunks(token, fileName, chunkCount);

                    this.DB.SaveChanges();
                }
            }

            lock (_dbLock.GetOrAdd(token, new LockEntry())) {
                if (transmission != null) {
                    FinishTransmission(transmission.PublicId);
                }
            }

            this.SendAPIMessage("success");
        }

        /// <summary>
        /// Finishes the transmission. Needs a new context to check whether alll files are uploaded
        /// </summary>
        /// <param name="transmissionId">The transmission identifier.</param>
        private static void FinishTransmission(string transmissionId) {
            using (var db = ModelContext.GetModelContext(null)) {
                var transmission = db.Transmissions()
                    .Include(nameof(Transmission.Files))
                    .Include(nameof(Transmission.Recipients))
                    .Include(nameof(Transmission.Sender))
                    .GetByPublicId(transmissionId);
                if (transmission != null && transmission.AllFilesCompleted) {
                    transmission.Enabled = true;
                    transmission.Status = Transmission.TransmissionStatuses.Published;
                    transmission.Expires = DateTime.UtcNow + new TimeSpan(Config.DefaultExpirationDays, 0, 0, 0);
                    transmission.SendNotification();
                    db.SaveChanges();
                }
            }
        }

        public void TestUpload() {
            var token = this.Params.String("upload_token");
            var chunkNumber = this.Params.Int("resumableChunkNumber", -1);
            var fileName = this.Params.String("resumableFilename", null);
            var totalSize = this.Params.Long("resumableTotalSize", -1);

            if (!TransferFileSystemLogic.ChunkExists(token, fileName, chunkNumber)) {
                this.Context.Response.StatusCode = 204;
            } else {
                this.Context.Response.StatusCode = 200;
            }

        }

        public void Init() {

            var name = this.Params.String("name");
            var email = this.Params.String("email");
            var message = this.Params.String("message");
            var title = this.Params.String("title");

            if (string.IsNullOrWhiteSpace(email)) {
                throw new BackendException("email-error", "Please provide an email address");
            }

            var transmission = PublicTransferAPIHandler.CreateTansmission(this.DB, this.User);

            // New
            transmission.Files = new List<TransferFile>();
            transmission.Recipients = new List<TransferRecipient>();
            transmission.Message = message;
            transmission.Title = title;
            transmission.Status = Transmission.TransmissionStatuses.Initialized;

            // Add Recipient
            var recipient = transmission.GetRecipient(email);
            if (recipient == null) {
                recipient = new TransferRecipient();
                recipient.Email = email;
                recipient.Name = name;
                transmission.Recipients.Add(recipient);
            }

            // Add file
            // Todo max number of files
            // todo max size overall

            var fileKeys = this.Context.Request.Form.AllKeys.Where(k => k.StartsWith("file_", StringComparison.InvariantCulture));

            foreach (var filekey in fileKeys) {
                var file = new TransferFile();
                file.Name = this.Context.Request.Form[filekey].Split(';').First();
                file.Size = long.Parse(this.Context.Request.Form[filekey].Split(';').Last());
                file.Type = TransferFile.TransferTypes.Upload;
                transmission.Files.Add(file);
            }

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Token = transmission.Hash });

        }

        private static ConcurrentDictionary<string, LockEntry> _dbLock = new ConcurrentDictionary<string, LockEntry>();
    }

    public class LockEntry {
        /// <summary>
        /// 
        /// </summary>
        public LockEntry() {

        }

    }
}
