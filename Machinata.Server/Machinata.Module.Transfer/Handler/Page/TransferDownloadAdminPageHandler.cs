using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Transfer.Logic;
using Machinata.Module.Transfer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Machinata.Module.Transfer.Handler.Page {

    public class TransferDownloadAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {

        


        [RequestHandler("/admin/transfer/download/{id}", AccessPolicy.PUBLIC_ARN)]
        public void Default(string id) {

            var entities = this.DB.Transmissions()
                            .Include(nameof(Model.Transmission.Files))
                            .Include(nameof(Model.Transmission.Recipients))
                            .AsQueryable();

            var transmission = entities.GetByToken(id);

            this.Template.InsertPropertyList(
                variableName: "transfer",
                entity: transmission
                );

            // Files
            this.Template.InsertEntityList(
                variableName: "files",
                entities: transmission.Files.AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/transfer/" + id + "/file/{entity.public-id}");
          
            // Navigation
            this.Navigation.Add("transfer");
            this.Navigation.Add("transmissions/transmission/" + transmission.PublicId, transmission.ToString());
            this.Navigation.Add("download");
        }


        [RequestHandler("/transfer/{id}/file/{publicId}", AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.Binary)]
        public void DownloadFile(string id, string publicId) {

            var entities = this.DB.Transmissions()
                            .Include(nameof(Model.Transmission.Files))
                            .Include(nameof(Model.Transmission.Recipients))
                            .AsQueryable();
            var transmission = entities.GetByToken(id);
            var file = transmission.Files.AsQueryable().GetByPublicId(publicId);
            // TODO Stream?
            var data = File.ReadAllBytes(file.Path);
            this.SendBinary(data, file.Name);
        }

      


    }
}
