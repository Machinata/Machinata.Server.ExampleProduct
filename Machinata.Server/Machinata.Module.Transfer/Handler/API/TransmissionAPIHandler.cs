
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using System.Net.Http;
using System.Net;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Transfer.Logic;
using System.IO;
using System.Net.Http.Headers;
using Machinata.Module.Transfer.Model;
using System.Collections.Concurrent;

namespace Machinata.Module.Transfer.Handler {


    public class TransmissionAPIHandler : PublicTransferAPIHandler {

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("transfer");
        }

        [RequestHandler("/api/transfer/upload", AccessPolicy.PUBLIC_ARN, null, Verbs.Post)]
        public void UploadChunk() {
            base.Upload();
        }

        [RequestHandler("/api/transfer/upload", AccessPolicy.PUBLIC_ARN, null, Verbs.Get)]
        public void TestChunkUpload() {
            this.TestUpload();
        }

        [RequestHandler("/api/transfer/init", AccessPolicy.PUBLIC_ARN)]
        public void InitTransfer() {
            base.Init();
        }


        [RequestHandler("api/transfer/{id}/download/{fileId}", AccessPolicy.PUBLIC_ARN)]
        public void Download(string id, string fileId) {

            var entities = this.DB.Transmissions()
                            .Include(nameof(Model.Transmission.Files))
                            .Include(nameof(Model.Transmission.Recipients))
                            .AsQueryable();

            var transmission = entities.GetByToken(id);

            var file = transmission.Files.AsQueryable().GetByPublicId(id);

            this.SendFile(file.Path);

        }


    }


   

}
