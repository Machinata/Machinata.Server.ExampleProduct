
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Transfer.Model;

namespace Machinata.Module.Transfer.Handler {


    public class TransmissionAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Transfer.Model.Transmission> {

        [RequestHandler("/api/admin/transfer/transmission/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }

        //[RequestHandler("/api/admin/mailing/mailings/create")]
        //public void Create() {
        //    this.CRUDCreate();
        //}

        [RequestHandler("/api/admin/transfer/transmission/{publicId}/delete")]
        public void Delete(string publicId) {
            var entity = this.DB.Transmissions()
             .Include(nameof(Transmission.Files))
             .Include(nameof(Transmission.Recipients))
             .GetByPublicId(publicId);

            Transmission.Delete(entity);
            this.DB.SaveChanges();

            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/admin/transfer/transmission/{publicId}/resend")]
        public void Resend(string publicId) {
            var entity = this.DB.Transmissions()
             .Include(nameof(Transmission.Files))
             .Include(nameof(Transmission.Recipients))
             .Include(nameof(Transmission.Sender))
             .GetByPublicId(publicId);

            entity.SendNotification();

            this.SendAPIMessage("success");
        }
    }

}
