using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Transfer {
    public class Config {

        public static string TransferFilePath = Core.Config.GetPathSetting("TransferFilePath");
       // public static string PublicTransferPath = Core.Config.GetPathSetting("PublicTransferPath");
        public static readonly int DefaultExpirationDays = Core.Config.GetIntSetting("DefaultExpirationDays");

        public static string DownloadLinkBase = Core.Config.GetStringSetting("DownloadLinkBase");

    }
}
