using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;
using Machinata.Module.Mailing.Model;
using Machinata.Core.Builder;
using Machinata.Module.Events.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Events.Mailing {
    public class EventMailingContactProvider : Module.Mailing.ContactProvider.MailingContactProvider {
      

        protected override IQueryable<MailingContact> GetContactsImpl(ModelContext db) {

            // Event
            var entity = GetEvent(db);
            if (entity != null) {
                // Guests         
                var guests = entity.Guests.AsQueryable();

                // Only Not Responded
                if (OnlyNotResponed()) {
                    guests = guests.Where(g => g.Accepted == null);
                }

                // Only Accepted
                if (OnlyAccepted()) {
                    guests = guests.Where(g => g.Accepted.HasValue && g.Accepted == true);
                }


                var contacts = new List<MailingContact>();
                foreach (var guest in guests) {

                    // TODO MORE Fields?
                    var properties = new Properties();
                    properties["CheckinCode"] = guest.CheckinCode;


                    var contact = new MailingContact();

                    // Set the fake id from the Guest.PublicId
                    contact.Id = guest.Id;
                    contact.Email = guest.Email;
                    contact.Name = guest.Name;
                    contact.Properties = properties;
                    contacts.Add(contact);
                }
                return contacts.AsQueryable();
            }
            return new List<MailingContact>().AsQueryable();
            
        }

        private Event GetEvent(ModelContext db) {
            string eventId = GetEventId();
            if (eventId != null) {
                var entity = db.Events()
                    .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
                    .GetByPublicId(eventId);
                return entity;
            }
            return null;
        }

        private string GetEventId() {
            if (string.IsNullOrEmpty( this.List?.ContactProviderSettings?[nameof(ContactProviderConfig.Event)]?.ToString())) {
                return null;
            }
            return this.List?.ContactProviderSettings?[nameof(ContactProviderConfig.Event)].ToString();
        }

        private bool OnlyNotResponed() {
            bool onlyNotResponded = false;
            bool.TryParse(this.List.ContactProviderSettings?[nameof(ContactProviderConfig.OnlyNotResponded)]?.ToString(), out onlyNotResponded);
            return onlyNotResponded;
        }

        private bool OnlyAccepted() {
            bool onlyAccepted = false;
            bool.TryParse(this.List.ContactProviderSettings?[nameof(ContactProviderConfig.OnlyAccepted)]?.ToString(), out onlyAccepted);
            return onlyAccepted;
        }

        public override string GetContactAdminLink() {
            return "/admin/events/event/" + GetEventId() + "/guests/guest/{entity.public-id}";
        }

        public override IDictionary<string, string> GetDefaultSettings() {
            var settings = new Dictionary<string, string>();
            settings[nameof(ContactProviderConfig.Event)] = null;
            settings[nameof(ContactProviderConfig.OnlyNotResponded)] = null;
            settings[nameof(ContactProviderConfig.OnlyAccepted)] = null;
            return settings;
        }
    }

    
    public class ContactProviderConfig : ModelObject {
        public Events.Model.Event Event { get; set; }
        public bool OnlyNotResponded { get; set; }
        public bool OnlyAccepted { get; set; }
    }
}
