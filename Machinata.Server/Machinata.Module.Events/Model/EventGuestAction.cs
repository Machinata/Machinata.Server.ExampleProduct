using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Events.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("EventsGuestActions")]
    public partial class EventGuestAction : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public EventGuestAction() {

        }

        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

        public enum EventGuestActionTypes {
            Acceptance = 10,
            Cancellation = 20,
            Checkin = 100,
            Checkout = 110
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public EventGuestActionTypes Type { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Notes { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////




        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextEventGuestActionExtenions {
        public static DbSet<EventGuestAction> EventGuestActions(this Core.Model.ModelContext context) {
            return context.Set<EventGuestAction>();
        }
    }

    #endregion
}
