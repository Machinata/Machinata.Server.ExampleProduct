using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;
using System.IO;
using Newtonsoft.Json;

namespace Machinata.Module.Events.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("EventsEvents")]
    public partial class Event : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Event() {
            if (string.IsNullOrEmpty(this.GUID)) {
                this.GUID = Guid.NewGuid().ToString();
            }
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Title { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Column]
        [Index(IsUnique = true)]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }

        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public DateTime Date { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.ImageUrl)]
        public string BackgroundImage { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.ImageUrl)]
        public string LogoImage { get; set; } 

        [Column]
        [Required]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string GUID { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public DateTime? RSVPOpening { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public DateTime? RSVPClosing { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public int? MaxGuests { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Published { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Business Business { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public User User { get; set; }

        [Column]
        public ICollection<EventGuest> Guests { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder(Forms.Frontend.JSON)]
        public string PublicURL
        {
            get
            {
                var route = Core.Localization.Text.GetTranslatedTextById("routes.events.event");
                if (route == null) return null;
                else return route.Replace("{publicId}", this.PublicId);
            }
        }
        
        [NotMapped]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool HasPublicURL
        {
            get
            {
                return PublicURL != null;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Validates the code for a event and returns the corresponding guest.
        /// Note: The guest will have the address and actions pre-loaded
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        /// <exception cref="LocalizedException">rsvp-invalid-code</exception>
        public EventGuest ValidateCodeAndGetGuest(Core.Handler.Handler handler, string code) {
            // Validate code
            var guest = handler.DB.EventGuests()
                .Include(nameof(EventGuest.Address))
                .Include(nameof(EventGuest.Actions))
                .SingleOrDefault(e => e.CheckinCode == code);
            if(guest == null || code == null || code == "" || guest.Event != this) throw new LocalizedException(handler,"rsvp-invalid-code", this.Title, code);
            return guest;
        }

        public bool IsRSVPOpen(ModelContext db) {
            // Opened?
            if (this.RSVPOpening != null) {
                if (this.RSVPOpening > DateTime.UtcNow) return false;
            }

            // Closed?
            if (this.RSVPClosing != null) {
                if (this.RSVPClosing < DateTime.UtcNow) return false;
            }

            // Maxed out?
            if (this.MaxGuests != null) {
                var accepted = db.EventGuests().Where(g => g.Event.Id == this.Id && g.Accepted == true).Count();
                if (accepted > this.MaxGuests.Value) return false;
            }

            return true;
        }

        public List<EventStat> GetEventStats() {
            var ret = new List<EventStat>();
            ret.Add(new EventStat() {
                Status = "Invited",
                Guests = this.Guests.Count(),
                Entourage = this.Guests.Sum(g => g.Entourage)
            });
            ret.Add(new EventStat() {
                Status = "Accepted",
                Guests = this.Guests.Where(g => g.Accepted == true).Count(),
                Entourage = this.Guests.Where(g => g.Accepted == true).Sum(g => g.Entourage)
            });
            ret.Add(new EventStat() {
                Status = "Not Accepted",
                Guests = this.Guests.Where(g => g.Accepted == false).Count(),
                Entourage = this.Guests.Where(g => g.Accepted == false).Sum(g => g.Entourage)
            });
            ret.Add(new EventStat() {
                Status = "Unknown",
                Guests = this.Guests.Where(g => g.Accepted == null).Count(),
                Entourage = this.Guests.Where(g => g.Accepted == null).Sum(g => g.Entourage)
            });
            return ret;
        }
    
        public JObject StatusData() {

            var form = new FormBuilder(Forms.Frontend.JSON);
            var ret = this.GetJSON(form);
            ret["guests"] = new JArray( this.Guests.ToList().Select(g => g.StatusData(form)));
            return ret;
        }

        public string ToJson() {
            System.IO.StringWriter sw = new StringWriter();
            JsonTextWriter writer = new JsonTextWriter(sw);

            writer.WriteStartObject();

            writer.WritePropertyName("title");
            writer.WriteValue(this.Title);

            writer.WritePropertyName("short-url");
            writer.WriteValue(this.ShortURL);

            writer.WritePropertyName("date");
            writer.WriteValue(this.Date);

            writer.WritePropertyName("guid");
            writer.WriteValue(this.GUID);

            writer.WritePropertyName("guests");
            writer.WriteStartArray();
            foreach (var guest in this.Guests) {
                writer.WriteRawValue(guest.ToJson());
            }
            writer.WriteEndArray();

      
            writer.WriteEndObject();

            return sw.ToString();
        }


        public void CheckAlreadyRegistered(string source) {
            // Check already registered
            if (source != null && this.Guests.Any(g => g.Source == source)) {
                throw new BackendException("rsvp-error", "Invitation link has already been used.");
            }
        }


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override CardBuilder VisualCard() {
            this.Include(nameof(Event.Business));
            this.Include(nameof(Event.Guests));
            var card = new Core.Cards.CardBuilder(this)
                  .Title(this.Title)
                  .Subtitle(this.Business?.Name)
                  .Sub(this.Guests.Count + " {text.guests}")
                  .Tag(this.Date.ToString(Core.Config.DateFormat))
                  .Icon("flash")
               ;

            return card;
        }

        #endregion

    }

    public class EventStat : ModelObject {
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Status { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Guests { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Entourage { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Total { get { return Guests + Entourage; } }
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextEventExtenions {
        public static DbSet<Event> Events(this Core.Model.ModelContext context) {
            return context.Set<Event>();
        }
    }

    #endregion
}
