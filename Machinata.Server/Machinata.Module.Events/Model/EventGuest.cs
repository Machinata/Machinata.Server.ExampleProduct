using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using System.IO;

namespace Machinata.Module.Events.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("EventsGuests")]
    public partial class EventGuest : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public EventGuest() {
            //this.Address = new Address();
        }

        public static EventGuest CreateGuest(string firstname, string lastname, int entourage, string company, string email, string source) {
            var guest = new EventGuest();
            guest.Actions = new List<EventGuestAction>();
            guest.Address = new Address();
            guest.FirstName = firstname;
            guest.LastName = lastname;
            guest.Entourage = entourage;
            guest.Company = company;
            guest.Source = source;

            // Address
            if (!string.IsNullOrWhiteSpace(email)) {
                guest.Address.Email = email;
            }

            guest.Address.Name = guest.Name;
            guest.Address.Company = company;
            return guest;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string Salutation { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string FirstName { get; set; }
  
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string LastName { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Company { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string Notes { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public int Entourage { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public bool? Accepted  { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public DateTime? CheckedIn  { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public DateTime? CheckedOut  { get; set; }
        
        /// <summary>
        /// The unique checking code for this guest.
        /// This can be used in the frontend to ensure that a guest has
        /// access to register etc...
        /// It is automatically generated, but up to the frontend to integrate
        /// it.
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string CheckinCode { get; set; }

        /// <summary>
        /// e.g. MailingContact.PublicId its based on
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string Source { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public Address Address { get; set; }
        
        [Column]
        public ICollection<EventGuestAction> Actions { get; set; }

        [Column]
        public Event Event { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Frontend.JSON)]
        [NotMapped]
        public string Name
        {
            get
            {
                return $"{this.FirstName} {this.LastName}";
            }
            set
            {
                var splits = value.Split(new char[] { ' ' }, 2);
                var hasLastName = false;
                if (splits.Count() > 1) {
                    hasLastName = true;
                }
                this.FirstName = splits.First();
                this.LastName = hasLastName ? splits[1] : string.Empty;
            }
        }

        [FormBuilder(Forms.Frontend.JSON)]
        [NotMapped]
        public string SearchText
        {
            get
            {
                return Core.Util.String.RemoveDiacritics(this.Name+ " " + this.Company).Trim().ToLower() + " " + this.Name+ " " + this.Company;
            }
          
        }

        [FormBuilder(Forms.Frontend.JSON)]
        [NotMapped]
        public string SortText
        {
            get
            {
                //return Core.Util.String.RemoveDiacritics(this.LastName+ ","+ this.FirstName+"," + this.Company).Trim().ToLower();
                return (this.LastName+ ","+ this.FirstName+"," + this.Company).Trim().ToLower();
            }
          
        }

        [FormBuilder(Forms.Frontend.JSON)]
        [NotMapped]
        public string Group
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.LastName)){
                    return Core.Util.String.RemoveDiacritics(this.LastName.ToUpperInvariant().First().ToString());
                }
                if (!string.IsNullOrWhiteSpace(this.FirstName)){
                    return Core.Util.String.RemoveDiacritics(this.FirstName.ToUpperInvariant().First().ToString());
                }
                if (!string.IsNullOrWhiteSpace(this.Company)){
                    return Core.Util.String.RemoveDiacritics(this.Company.ToUpperInvariant().First().ToString());
                }
                return "Z"; // safe fallback
            }

        }

        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public string Status
        {
            get
            {
                if (this.Actions != null) {
                    return string.Join(",", this.Actions.Select(a => a.Type.ToString()?.ToLowerInvariant()));
                }
                return null;
            }
        }

        [FormBuilder()]
        [NotMapped]
        public string Email
        {
            get
            {
                return this.Address?.Email;
            }
        }


        public JObject StatusData(FormBuilder form) {
            if (form == null) {
                form = new FormBuilder(Forms.Frontend.JSON);
            }
            var ret = this.GetJSON(form);
            ret["publicid"] = this.PublicId;
            return ret;
        }

        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public string Street
        {
            get
            {
                return this.Address?.Address1;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string GenerateCheckinCode() {
            return Core.Ids.Code.Default.GenerateCodeForId(this.Id);
        }

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            if(string.IsNullOrEmpty(this.CheckinCode)) {
                if (this.Id != 0) {
                    this.CheckinCode = GenerateCheckinCode();
                }
            }
        }

        public void Accept() {
            this.AddAction(EventGuestAction.EventGuestActionTypes.Acceptance);
            this.Accepted = true;
        }

        public void Cancel() {
            this.AddAction(EventGuestAction.EventGuestActionTypes.Cancellation);
            this.Accepted = false;
        }

        public void Checkin() {
            this.AddAction(EventGuestAction.EventGuestActionTypes.Checkin);
            this.CheckedIn = DateTime.UtcNow;
        }

        public void UndoCheckin() {
            this.RemoveLastAction(EventGuestAction.EventGuestActionTypes.Checkin);
            this.CheckedIn = null;
        }

        public void Checkout() {
            this.AddAction(EventGuestAction.EventGuestActionTypes.Checkout);
            this.CheckedOut = DateTime.UtcNow;
        }

        public void UndoCheckout() {
            this.RemoveLastAction(EventGuestAction.EventGuestActionTypes.Checkout);
            this.CheckedOut = null;
        }
        
        private EventGuestAction RemoveLastAction(EventGuestAction.EventGuestActionTypes type, bool throwException = false) {
            var action = this.Actions.OrderByDescending(a=>a.Id).FirstOrDefault(a => a.Type == type);
            if (action != null) {
                this.Actions.Remove(action);
                action.Context.DeleteEntity(action);
                return action;
            }
            if (action == null && throwException) {
                throw new BackendException("error", $"Action not found {type}");
            }
            return null;
        }

        private void AddAction(EventGuestAction.EventGuestActionTypes type, bool overwrite = true) {
            var action = this.Actions.FirstOrDefault(a => a.Type == type);
            if (action != null && overwrite == false) {
                throw new BackendException("error", $"Already did {type}");
            } else if (action != null) {
                action.Created = DateTime.UtcNow;
            } else {
                this.Actions.Add(new EventGuestAction() { Type = type });
            }
        }

        public string ToJson() {
            System.IO.StringWriter sw = new StringWriter();
            JsonTextWriter writer = new JsonTextWriter(sw);

          
            writer.WriteStartObject();

         
            writer.WritePropertyName("first-name");
            writer.WriteValue(this.FirstName);

            writer.WritePropertyName("last-name");
            writer.WriteValue(this.LastName);

            writer.WritePropertyName("notes");
            writer.WriteValue(this.Notes);

            writer.WritePropertyName("entourage");
            writer.WriteValue(this.Entourage);

            writer.WritePropertyName("accepted");
            writer.WriteValue(this.Accepted);

            writer.WritePropertyName("checked-in");
            writer.WriteValue(this.CheckedIn);

            writer.WritePropertyName("checked-out");
            writer.WriteValue(this.CheckedOut);

            writer.WritePropertyName("checkin-code");
            writer.WriteValue(this.CheckinCode);

            writer.WritePropertyName("salutation");
            writer.WriteValue(this.Salutation);

            writer.WritePropertyName("company");
            writer.WriteValue(this.Company);

            writer.WritePropertyName("name");
            writer.WriteValue(this.Name);

            writer.WritePropertyName("email");
            writer.WriteValue(this.Email);

            writer.WritePropertyName("search-text");
            writer.WriteValue(this.SearchText);

            writer.WritePropertyName("sort-text");
            writer.WriteValue(this.SortText);

            writer.WritePropertyName("group");
            writer.WriteValue(this.Group);

            writer.WritePropertyName("publicid");
            writer.WriteValue(this.PublicId);


           
            writer.WriteEndObject();

            return sw.ToString();
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextEventGuestExtenions {
        public static DbSet<EventGuest> EventGuests(this Core.Model.ModelContext context) {
            return context.Set<EventGuest>();
        }
    }

    #endregion
}
