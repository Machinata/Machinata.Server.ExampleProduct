using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Events.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Events.Logic {

    [Serializable]
    public abstract class GuestImportLogic {

        public abstract void ImportRow(EventGuest guest, ModelContext db, Dictionary<string, string> data, int rowNumber);

        public abstract string Name { get; }

        public virtual FormBuilder UpdateForm(FormBuilder form) {
            return form;
        }

        public string Type
        {
            get
            {
                return this.GetType().Name;
            }
        }

        public static IEnumerable<GuestImportLogic> GetAllTypes() {
            var types = Core.Reflection.Types.GetMachinataTypes(typeof(GuestImportLogic)).Where(t => !t.IsAbstract);
            var instances = types.Select(t => Activator.CreateInstance(t) as GuestImportLogic);
            return instances.OrderBy(i=>i.Name);
        }

        public static GuestImportLogic GetImportLogicByTypeName(string name = "GuestImportLogicDefault") {
            var allTypes = GetAllTypes();
            var importLogic = allTypes.FirstOrDefault(t => t.GetType().Name == name);
            return importLogic;
        }
    }


    /// <summary>
    /// Salutation	FirstName	LastName	Notes	Entourage	Accepted
    /// </summary>
    /// <seealso cref="Machinata.Module.Events.Logic.GuestImportLogic" />
    public class GuestImportLogicDefault : GuestImportLogic {

        public override void ImportRow(EventGuest guest, ModelContext db, Dictionary<string, string> data, int rowNumber) {
            guest.Address = new Address();
            guest.Address.Name = guest.Name;
            guest.Address.Company = guest.Company;
        }
             
        public override string Name {
            get
            {
                return "Default";
            }
        }

     
    }

    /// <summary>
    /// FirstName	LastName
    /// </summary>
    /// <seealso cref="Machinata.Module.Events.Logic.GuestImportLogic" />
    public class GuestImportLogicMinimal : GuestImportLogic {

        public override FormBuilder UpdateForm(FormBuilder form) {
            form.Exclude(nameof(EventGuest.Salutation));
            form.Exclude(nameof(EventGuest.Notes));
            form.Exclude(nameof(EventGuest.Entourage));
            form.Exclude(nameof(EventGuest.Accepted));
            return form;
        }

        public override void ImportRow(EventGuest guest, ModelContext db, Dictionary<string, string> data, int rowNumber) {
            guest.Address = new Address();
            guest.Address.Name = guest.Name;
            guest.Address.Company = guest.Company;
        }

        public override string Name
        {
            get
            {
                return "Minimal";
            }
        }


    }

    /// <summary>
    /// Salutation	Name    Notes	Entourage	Accepted
    /// </summary>
    /// <seealso cref="Machinata.Module.Events.Logic.GuestImportLogic" />
    public class GuestImportLogicOneName : GuestImportLogicDefault {
        public override string Name {
            get
            {
                return "One Name";
            }
        }

        public override FormBuilder UpdateForm(FormBuilder form) {
            form.Exclude(nameof(EventGuest.FirstName));
            form.Exclude(nameof(EventGuest.LastName));
            form.Include(nameof(EventGuest.Name));
            return form;
        }

       
    }
}
