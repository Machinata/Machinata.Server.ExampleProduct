using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Events.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Machinata.Module.Events.Logic {
    public static class EventsLogic {

         /// <summary>
        /// Gets the events for user
        /// Business and user have to be loaded otherwise its a security flaw
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static IQueryable<Event> GetEvents(ModelContext db, User user, bool includeGuestActions, bool includeGuestAddresses = false) {

            var events = db.Events()
             .Include(nameof(Event.Business) + "." + nameof(Business.Users) + "." + nameof(User.Businesses))
             .Include(nameof(Event.User));

            if (includeGuestActions || includeGuestAddresses) {
                if(includeGuestActions) events = events.Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions));
                if(includeGuestAddresses) events = events.Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address));
            } else {
                events = events.Include(nameof(Event.Guests));
            }

            var businesses = events.Where(e => e.Business.Users.Select(u=>u.Id).Contains(user.Id)).Select(e => e.Business.Id);
           
            var userEvents = events
                .Where(e => e.User.Id == user.Id 
                    || businesses.Contains(e.Business.Id)
                );

            return userEvents;

        }

        /// <summary>
        /// Gets the event for a user via direct relationship or via business
        /// Does validation of ownership
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="user">The user.</param>
        /// <param name="publicId">The public identifier.</param>
        /// <param name="includeGuestActions">if set to <c>true</c> [include guest actions].</param>
        /// <returns></returns>
        public static Event GetEvent(ModelContext db, User user, string publicId, bool includeGuestActions, bool includeGuestAddresses = false) {
            var events = GetEvents(db, user, includeGuestActions, includeGuestAddresses);
            var entity = events.GetByPublicId(publicId);
            entity.ValidateOwnership(events);
            return entity;
        }


        public static  void ValidateOwnership(this Event eventEntity, IEnumerable<Event> events) {
            if (!events.Contains(eventEntity)) {
                throw new BackendException("event-error", "event not found");
            }
        }


      

    }
}