using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Module.Events.Model;
using Machinata.Core.Reporting;
using Machinata.Module.Events.Logic;

namespace Machinata.Product.NervesEvents.Handler {

    public class AdminEventPageHandler : Module.Admin.Handler.AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("events");
        }

        #endregion


        #region Menu Item
              
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "event",
                Path = "/admin/events",
                Title = "{text.events}",
                Sort = "500"
            });
        }

        #endregion


        [RequestHandler("/admin/events")]
        public void Default() {

            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/events/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("data.menu-items", menuItems);


            var entities = this.Template.Paginate(this.DB.Events(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/event/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("events");
        }

        
        [RequestHandler("/admin/events/create")]
        public void Create() {
            this.RequireWriteARN();

            var entity = new Event();
            entity.User = this.User;
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/events/create",
               onSuccess: "/admin/events/event/{event.public-id}"
            );

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/events/event/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Business))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/events/event/" + entity.PublicId + "/edit",
               onSuccess: "/admin/events/event/" + entity.PublicId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add($"edit");
        }



        [RequestHandler("/admin/events/event/{publicId}")]
        public void View(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
                .Include(nameof(Event.Business))
                .Include(nameof(Event.User))
                .GetByPublicId(publicId);

            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );

            // Dates
            this.Template.InsertVariable("start", Core.Util.Time.GetUTCMillisecondsFromDateTime(Time.ToDefaultTimezone( entity.Date).Date));
            this.Template.InsertVariable("end", Core.Util.Time.GetUTCMillisecondsFromDateTime(Time.ToDefaultTimezone( entity.Date).Date.EndOfDay()));

            // Stats
            this.Template.InsertEntityList("entity.stats", entity.GetEventStats());
                 
            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);

        }


        [RequestHandler("/admin/events/event/{publicId}/guests")]
        public void Guests(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
                .Include(nameof(Event.Business))
                .Include(nameof(Event.User))
                .GetByPublicId(publicId);

            var count = entity.Guests.Count;
                   

            // All formats for import
            var allFormats = GuestImportLogic.GetAllTypes();
            this.Template.InsertVariableUnsafe("import-formats", Core.JSON.Serialize(allFormats));

            // Guests
            var guests = this.Template.Filter(entity.Guests.AsQueryable(), this, nameof(EventGuest.SearchText));
            guests = this.Template.Paginate(guests, this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: guests,
                link: "/admin/events/event/" + publicId + "/guests/guest/{entity.public-id}");

            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariable("guests.count", count);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add("guests");

        }

        [RequestHandler("/admin/events/event/{publicId}/guests/guest/{guestId}")]
        public void Guest(string publicId, string guestId) {

            var entity = DB.Events()
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
                .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);

            // Guest
            this.Template.InsertPropertyList(
               variableName: "form",
               entity: guest,
               form: new FormBuilder(Forms.Admin.VIEW)
            );

            // Address

            if (guest.Address != null) {
                this.Template.InsertPropertyList(
                  variableName: "address",
                  entity: guest.Address,
                  form: new FormBuilder(Forms.Admin.VIEW),
                  showCard: false
               );
            } else {
                Template.InsertVariable("address", "No address");
            }

            // Actions
            this.Template.InsertEntityList(
                entities: guest.Actions.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(EventGuestAction.Created)));

            // Variables
            Template.InsertVariables("event", entity);
            Template.InsertVariables("guest", guest);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add("guests");
            this.Navigation.Add($"guest/{guestId}", guest.Name);
          
        }

        [RequestHandler("/admin/events/event/{publicId}/guests/guest/{guestId}/edit")]
        public void GuestEdit(string publicId, string guestId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Guests))
                .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);

            this.Template.InsertForm(
               variableName: "form",
               entity: guest,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: $"/api/admin/events/event/{publicId}/guest/" + guest.PublicId + "/edit",
               onSuccess: $"/admin/events/event/{publicId}/guests/guest/" + guest.PublicId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add("guests");
            this.Navigation.Add($"guest/{guestId}", guest.Name);
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/events/event/{publicId}/guests/guest/{guestId}/edit-address")]
        public void GuestAddressEdit(string publicId, string guestId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
                .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            var address = guest.Address;

            this.Template.InsertForm(
               variableName: "form",
               entity: address,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: $"/api/admin/events/event/{publicId}/guest/" + guest.PublicId + "/edit-address",
               onSuccess: $"/admin/events/event/{publicId}/guests/guest/" + guest.PublicId
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add("guests");
            this.Navigation.Add($"guest/{guestId}", guest.Name);
            this.Navigation.Add("edit-address", "{text.address}", "{text.edit}");

        }


        [RequestHandler("/admin/events/event/{publicId}/guests/export", null, null, Verbs.Get, ContentType.Binary)]
        public void Export(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Events()
                .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EXPORT);
            form.Include(nameof(EventGuest.CheckedIn));
            form.Include(nameof(EventGuest.CheckedOut));
            form.Include(nameof(EventGuest.Address));

            var content = ExportService.ExportXLSX(entity.Guests, form);
            this.SendBinary(content, $"{entity.Title}.xlsx");

            // Navigation
            this.Navigation.Add("events");
            this.Navigation.Add($"event/{publicId}", entity.Title);
            this.Navigation.Add("guests");
            this.Navigation.Add("export");

        }


      
    }


}