using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Module.Events.Handler.Page;

namespace Machinata.Module.Events.Handler {

    public abstract class EventPageHandler : PageHandler {

        public void ShowEvent(string publicId) {

            this.RequireLogin();
            // Get screen and validate
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId, false);
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            // Navigation
            this.Navigation.Add("event/" + publicId, entity.Title);
        }


     
        public void ShowEvents() {

            this.RequireLogin();

            var entities = Module.Events.Logic.EventsLogic.GetEvents(this.DB, this.User, false);

            if (entities.Any()) {
                this.Template.InsertTemplates(
                    variableName: "events",
                    entities: entities,
                    templateName: "default.event"
                );
            } else {
                this.Template.InsertVariable("events", "No screens available.");
            }

            // Misc
            this.Template.InsertVariable("{user.name}", this.User.Name);
        }

        public void ResetPasswordConfirmImpl() {
            // Validate the request
            User user = this.DB.Users().GetByUsername(this.Params.String("username"));
            var code = this.Params.String("code");
            var resetCode = this.Params.String("reset-code");
            user.CheckResetCode(resetCode, code);

            this.Template.InsertVariable("code", code);
            this.Template.InsertVariable("reset-code", resetCode);
            this.Template.InsertVariable("username", user.Email);
        }

        public void ActivateUser() {
            // only to get user and check codes
            User user = this.DB.Users().GetByUsername(this.Params.String("username"));
            var code = this.Params.String("code");
            var resetCode = this.Params.String("reset-code");
            user.CheckResetCode(resetCode, code);

            this.Template.InsertVariable("code", code);
            this.Template.InsertVariable("reset-code", resetCode);

            this.Template.InsertVariable("name", !string.IsNullOrEmpty(user.Name) ? user.Name : user.Username);
            this.Template.InsertVariable("username", user.Email);
        }
    }
}