using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
 
namespace Machinata.Module.Events.Handler.Page {
         

    public abstract class PageHandler : Core.Handler.PageTemplateHandler {


        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("events");
        }
     
        public const string EVENTS_ARN = "/events/*";

        public string Section;

        public override void DefaultNavigation() {
            this.Meta.UseMetaTitleForPageTitle = true;
            this.Navigation.TitleSeparator = " / ";
            if(this.Language == "de") {
                // DE
                this.Meta.TitleTrailer = " / Nerves Events";
                this.Navigation.Add("/", "Nerves Events");
            } else {
                // EN
                this.Meta.TitleTrailer = " / Nerves Events";
                this.Navigation.Add("/", "Nerves Events");
            }
        }

        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();

            if (this.Section == null) Section = this.Navigation.PageTitle;
            this.Template.InsertVariable("page.section", this.Section);
        }
    }
}