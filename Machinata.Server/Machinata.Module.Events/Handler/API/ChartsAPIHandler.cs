using System.Linq;
using System;
using System.Collections.Generic;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;

using Machinata.Module.Events.Model;

namespace Machinata.Module.Events.Handler {

    public class ChartsAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("events");
        }

        #endregion


        [RequestHandler("/api/admin/events/chart/donut/event/{eventId}/statuses")]
        public void DonutGuestsStatuses(string eventId) {
            var data = new DonutChartData();
            data.Title = "Statuses";
            var entities = this.DB.Events()
                .Include(nameof(Event.Guests));
            var entity = entities.GetByPublicId(eventId);

            var groups = entity.Guests.GroupBy(g => g.Accepted);
            foreach (var g in groups) {
                string name = "";
                if (g.Key == null) {
                    name = "Unknown";
                } else if (g.Key == true) {
                    name = "Accepted";
                } else if (g.Key == false) {
                    name = "Not Accepted";
                }

                data.Items.Add(new DonutChartDataItem() {  Name = name, Title = name, Value =  g.Count()  });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/events/chart/donut/event/{eventId}/checkedin")]
        public void DonutGuestsCheckedIn(string eventId) {
            var data = new DonutChartData();
            var includedEntourage = this.Params.Bool("entourage", false);
            data.Title = "Checked In";

            if (includedEntourage) {
                data.Title += " +1";
            }
            var entities = this.DB.Events()
                .Include(nameof(Event.Guests));
          
            
            var entity = entities.GetByPublicId(eventId);

            var groups = entity.Guests.Where(g=>g.Accepted.HasValue).Select(g => new { Guest = g, CheckedIn = g.CheckedIn.HasValue }).GroupBy(g=>g.CheckedIn);
            foreach(var g in groups) {
                data.Items.Add(new DonutChartDataItem() { Name = g.Key ? "Checked In" : "Not Checked In", Title = g.Key ? "Checked In" : "Not Checked In", Value = includedEntourage? g.Sum(x=>x.Guest.Entourage) + g.Count() : g.Count() });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/events/chart/bar/event/{eventId}/info")]
        public void BarGuestsInfos(string eventId) {
            var data = new TimeSeriesChartData();
           // data.TimeFormat = "hh:mm";
            data.XAxisIsTime = false;

            var series = new TimeSeriesChartDataSeries();
            series.Title = "Guests";
            var entity = this.DB.Events()
             .Include(nameof(Event.Guests)).GetByPublicId(eventId);
            var acceptedGuests = entity.Guests.Where(g => g.Accepted == true);
            series.DataPoints.Add(new TimeSeriesChartDataPoint() { Label = "Accepted", Value = entity.Guests.Count(g=>g.Accepted == true) });
            series.DataPoints.Add(new TimeSeriesChartDataPoint() { Label = "Denied", Value = entity.Guests.Count(g =>! g.Accepted == true) });
            series.DataPoints.Add(new TimeSeriesChartDataPoint() { Label = "Accepted +1", Value = acceptedGuests.Sum(g =>g.Entourage) + acceptedGuests.Count() });
            data.Series.Add(series);
            this.SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/events/chart/timeseries/event/{eventId}/checkedin")]
        public void CheckedInGuestsOverTime(string eventId) {
            var entity = this.DB.Events()
                .Include(nameof(Event.Guests))
                .GetByPublicId(eventId);
            
            var data = new TimeSeriesChartData();
            data.TimeFormat = "HH:mm";
            data.XAxisIsTime = false; //BUG: @dan this doesnt work with the current constalation of settings
            
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<EventGuest>(
                handler: this,
                entities: entity.Guests.Where(g => g.CheckedIn.HasValue).AsQueryable() ,
                entitiesForRange: delegate (IQueryable<EventGuest> guests, DateTime start, DateTime end) {
                    return guests.Where(e => e.CheckedIn.Value > start && e.CheckedIn.Value < end); 
                },
                valueForEntities: delegate (IQueryable<EventGuest> guests) {
                    return guests.ToList().Count; //TODO: why is ToList required?
                },
                title: "Check Ins"
            ));
            SendAPIMessage("chart-data", data);
        }
    }
}
