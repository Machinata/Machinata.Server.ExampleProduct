using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Machinata.Module.Events.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.Events.Logic;

namespace Machinata.Product.NervesEvents.Handler.API {
    public class EventsAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Events.Model.Event> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("events");
        }

        #endregion


        [RequestHandler("/api/admin/events/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/events/event/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/events/event/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/events/event/{publicId}/add-guest")]
        public void AddGuest(string publicId) {
            var entity = this.DB.Events()
                .Include(nameof(Event.Guests))
                .GetByPublicId(publicId);
            
            // Init guest
            var guest = new EventGuest();
            guest.Name = this.Params.String("name");

            // TODO: read default country from config
            guest.Address = new Address();
            guest.Address.CountryCode = "ch";
            //this.DB.Addresses().Add(guest.Address);
            entity.Guests.Add(guest);
            this.DB.SaveChanges();

            // Generate code
            if(string.IsNullOrEmpty(guest.CheckinCode)) {
                guest.CheckinCode = guest.GenerateCheckinCode();
                this.DB.SaveChanges();
            }

            this.SendAPIMessage("success", new { Guest = new { PublicId = guest.PublicId } });

        }

       
        [RequestHandler("/api/admin/events/event/{publicId}/guest/{guestId}/edit")]
        public void EditGuest(string publicId, string guestId) {
            var entity = DB.Events()
               .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
               .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);

            guest.Populate(this, new Core.Builder.FormBuilder(Forms.Admin.EDIT));
            guest.Validate();

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Guest = new { PublicId = guest.PublicId } });

        }


        [RequestHandler("/api/admin/events/event/{publicId}/guest/{guestId}/edit-address")]
        public void EditGuestAddress(string publicId, string guestId) {
            var entity = DB.Events()
               .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
               .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Address))
               .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            var address = guest.Address;

            address.Populate(this, new Core.Builder.FormBuilder(Forms.Admin.EDIT));
            address.Validate();

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Guest = new { PublicId = guest.PublicId } });

        }

        [RequestHandler("/api/admin/events/event/{publicId}/guest/{guestId}/remove")]
        public void DeleteGuest(string publicId, string guestId) {
            var entity = DB.Events()
               .Include(nameof(Event.Guests) + "." + nameof(EventGuest.Actions))
               .GetByPublicId(publicId);

            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);

            entity.Guests.Remove(guest);

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Guest = new { PublicId = guest.PublicId } });

        }

        protected override void CreatePopulate(Event entity) {
            base.CreatePopulate(entity);
            entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Title);
        }

        [RequestHandler("/api/admin/events/event/{publicId}/import")]
        public void ImportGuests(string publicId) {
            this.RequireWriteARN();

            var importLogicName = this.Params.String("import-format", "GuestImportLogicDefault") ;
            var resetList = this.Params.Bool("reset", false);

            var importLogic = GuestImportLogic.GetImportLogicByTypeName(importLogicName);

            var form = new FormBuilder(Forms.Admin.IMPORT);
            importLogic.UpdateForm(form);


            var eventEntity = this.DB.Events()
                .Include(nameof(Event.Guests))
                .GetByPublicId(publicId);

            // Get xlsx
            byte[] fileData = this.Request.ReadFileFromRequest("xlsx");

            int saveBatchSize = 50;

            var log = new StringBuilder();
            var guests = Core.Reporting.ExportService.ImportXLSX<EventGuest>(this.DB, fileData, form, importLogic.ImportRow, log,true, saveBatchSize);

            // Fresh list? delete old
            if (resetList) {
                int removeCount = 0;
                foreach (var guest in eventEntity.Guests.ToList()) {
                    eventEntity.Guests.Remove(guest);
                    if (removeCount++ > 0 && removeCount % saveBatchSize == 0) {
                        this.DB.SaveChanges();
                    }
                }
            }

            // Add guests
            int addCount = 0;
            foreach (var guest in guests) {
                eventEntity.Guests.Add(guest);
                if (addCount++ > 0 && addCount % saveBatchSize == 0) {
                    this.DB.SaveChanges();
                }
            }

            // Save
            this.DB.SaveChanges();

            // Create all guest codoes
            foreach (var guest in guests) {
                // Generate code
                if(string.IsNullOrEmpty(guest.CheckinCode)) {
                    guest.CheckinCode = guest.GenerateCheckinCode();
                    this.DB.SaveChanges();
                }
            }

            // Re-Save
            this.DB.SaveChanges();
            

            // Return
            SendAPIMessage("import-success", new { Log = log.ToString() });
        }

      


       

    }
}