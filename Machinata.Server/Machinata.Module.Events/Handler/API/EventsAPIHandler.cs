using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Events.Extensions;
using Machinata.Module.Events.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Machinata.Product.NervesInteractive.Handler.API {
    public class EventsAPIHandler : APIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("events");
        }

        #endregion
         
        [RequestHandler("/api/events/event/{publicId}")]
        public void Get(string publicId) {
            // Init
            var dbtime = Stopwatch.StartNew();
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId, false, true);
            dbtime.Stop();
            var serializetime = Stopwatch.StartNew();
            var data = entity.ToJson();
            serializetime.Stop();
            //data["time-db"] = dbtime.ElapsedMilliseconds;
            //data["time-serialized"] = serializetime.ElapsedMilliseconds;

            // Send
            this.SendRawAPIMessage("event", data, false);
        }
        

        [RequestHandler("/api/events/event/{publicId}/guest/{guestId}/checkin")]
        public void Checkin(string publicId, string guestId) {
            // Init
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId,true);
            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            guest.Checkin();
            this.DB.SaveChanges();
            // Send
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/events/event/{publicId}/guest/{guestId}/checkin/undo")]
        public void CheckinUndo(string publicId, string guestId) {
            // Init
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId,true);
            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            guest.UndoCheckin();
            this.DB.SaveChanges();
            // Send
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/events/event/{publicId}/guest/{guestId}/checkout")]
        public void Checkout(string publicId, string guestId) {
            // Init
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId,true);
            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            guest.Checkout();
            this.DB.SaveChanges();
            // Send
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/events/event/{publicId}/guest/{guestId}/checkout/undo")]
        public void CheckoutUndo(string publicId, string guestId) {
            // Init
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId,true);
            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            guest.UndoCheckout();
            this.DB.SaveChanges();
            // Send
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/events/event/{publicId}/guest/{guestId}/update")]
        public void Update(string publicId, string guestId) {
            // Init
            var entity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId, true, true);
            var guest = entity.Guests.AsQueryable().GetByPublicId(guestId);
            // Update properties
            var checkin = this.Params.Bool("checkin", false);
            guest.FirstName = this.Params.String("firstname",guest.FirstName);
            guest.LastName = this.Params.String("lastname",guest.LastName);
            guest.Entourage = this.Params.Int("entourage", guest.Entourage);
            guest.Company = this.Params.String("company", guest.Company);
            var newEmail = this.Params.String("email", null);
            if(newEmail != null) {
                if (newEmail == "") newEmail = null;
                guest.Address.Email = newEmail;
            }
            //guest.Source = this.Params.String("source", guest.Source);
            if(checkin == true) {
                guest.Checkin();
            } else {
                guest.UndoCheckin();
            }
            // Save
            this.DB.SaveChanges();
            // Send
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/events/event/{publicId}/guests/add")]
        public void AddGuest(string publicId) {


            // Load event, whit security check
            var eventEntity = Module.Events.Logic.EventsLogic.GetEvent(this.DB, this.User, publicId, true);

            // Add guest from request
            EventGuest guest = this.AddGuestFromRequest(eventEntity: eventEntity, allowAutoCheckin: true);

            // Save
            this.DB.SaveChanges();

            // Generate code
            if(string.IsNullOrEmpty(guest.CheckinCode)) {
                guest.CheckinCode = guest.GenerateCheckinCode();
                this.DB.SaveChanges();
            }

            // Send
            this.SendAPIMessage("success", new { Guest = new { PublicId = guest.PublicId } });
        }

       
    }
}