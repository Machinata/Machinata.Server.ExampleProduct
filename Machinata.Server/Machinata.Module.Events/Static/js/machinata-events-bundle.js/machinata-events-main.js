
Machinata.Events = {}
Machinata.Events.GuestList = {}

/* =============================================================================
*  ================ CONSTANTS ==================================================
*  ============================================================================= 
*/
Machinata.Events.GuestList.UPDATE_INTERVAL_MS = 15000;

/* =============================================================================
*  ================ HELPER FUNCTIONS ==================================================
*  ============================================================================= 
*/





/* =============================================================================
*  ================  CLASS ============================================
*  ============================================================================= 
*/

Machinata.Events.GuestList._eventID = null;
Machinata.Events.GuestList._data = null;
Machinata.Events.GuestList._isUpdating = false;
Machinata.Events.GuestList._updateTimer = null;


Machinata.Events.GuestList.NormalizeText = function (text) {
    var weird = 'öüóőúéáàűíÖÜÓŐÚÉÁÀŰÍçÇ!@£$%^&*()_+?/*."\'';
    var normalized = 'ouooueaauiOUOOUEAAUIcC                  ';
    var idoff = -1, new_text = '';
    var lentext = text.toString().length - 1;
    for (i = 0; i <= lentext; i++) {
        idoff = weird.search(text.charAt(i));
        if (idoff == -1) {
            new_text = new_text + text.charAt(i);
        } else {
            new_text = new_text + normalized.charAt(idoff);
        }
    }
    return new_text;
};


Machinata.Events.GuestList.initAndRun = function (elem) {


    Machinata.Events.GuestList._eventID = elem.attr("data-event-id");
    Machinata.Events.GuestList._data = null;
    Machinata.Events.GuestList._isUpdating = false;

    $(".search-input #search").on("change keyup", function () {
        Machinata.Events.GuestList.search($("#search").val());
    });
    $(".search-input .event-icon-x").click(function () {
        $("#search").val("");
        Machinata.Events.GuestList.search(null)
    });

    $(".group").each(function () {
        var groupElem = $(this);
        groupElem.data("selector", $(".group-selector[data-group-id='" + groupElem.attr("data-group-id") + "']"));
    });

    $("#guestdetails .event-icon-x").click(function () {
        $("#guestdetails").hide();
    });

    $("#addguestbutton").click(function () {
        Machinata.Events.GuestList.showAddGuest();
    });

    $("#addguest .event-icon-x").click(function () {
        $("#addguest").hide();
    });
    $("#addguest .button.checkin").click(function () {
        $(this).toggleClass("checked");
    });

    $("#editguest .event-icon-x").click(function () {
        $("#editguest").hide();
    });
    $("#editguest .button.checkin").click(function () {
        $(this).toggleClass("checked");
    });

    $("#addguest .button.add-guest").click(function () {
        // Do API Call
        Machinata.apiCall("/api/events/event/" + Machinata.Events.GuestList._eventID + "/guests/add")
            .data({
                checkin: $("#addguest .button.checkin").hasClass("checked"),
                firstname: $("#addguest input.firstname").val(),
                lastname: $("#addguest input.lastname").val(),
                entourage: $("#addguest input.entourage").val(),
                company: $("#addguest input.company").val(),
                email: $("#addguest input.email").val(),
            })
            .genericError()
            .success(function (message) {
                // Close and update list
                $("#addguest").hide();
                Machinata.Events.GuestList.update();
            })
            .send();
    });

    $("#editguest .button.save-guest").click(function () {
        
        var buttonElem = $(this);
        var publicid = $("#guestdetails").data("guest-publicid");
        var guest = Machinata.Events.GuestList.getGuestForPublicID(publicid);
        if (buttonElem.hasClass("loading")) return;

        // Do API Call
        buttonElem.addClass("loading");
        Machinata.apiCall("/api/events/event/" + Machinata.Events.GuestList._eventID + "/guest/" + guest["publicid"] + "/update")
            .data({
                checkin: $("#editguest .button.checkin").hasClass("checked"),
                firstname: $("#editguest input.firstname").val(),
                lastname: $("#editguest input.lastname").val(),
                entourage: $("#editguest input.entourage").val(),
                company: $("#editguest input.company").val(),
                email: $("#editguest input.email").val(),
            })
            .genericError()
            .success(function (message) {
                // Close and update list
                buttonElem.removeClass("loading");
                $("#editguest").hide();
                Machinata.Events.GuestList.update();
            })
            .error(function (message) {
                buttonElem.removeClass("loading");
            })
            .send();
    });

    $("#guestdetails .button.edit").click(function () {

        var buttonElem = $(this);
        var publicid = $("#guestdetails").data("guest-publicid");
        var guest = Machinata.Events.GuestList.getGuestForPublicID(publicid);
        if (buttonElem.hasClass("loading")) return;

        $("#guestdetails").hide();
        Machinata.Events.GuestList.showEditGuest(publicid);
    });

    $("#guestdetails .button.checkin").click(function () {

        var buttonElem = $(this);
        var publicid = $("#guestdetails").data("guest-publicid");
        var guest = Machinata.Events.GuestList.getGuestForPublicID(publicid);
        if (buttonElem.hasClass("loading")) return;

        if (buttonElem.hasClass("checked")) {
            // UNDO Checkin...

            // Update UI
            buttonElem.removeClass("checked");
            guest["elem"].removeClass("checked-in");
            // Update model
            guest["checked-in"] = null;
            // Do API Call
            buttonElem.addClass("loading");
            Machinata.apiCall("/api/events/event/" + Machinata.Events.GuestList._eventID + "/guest/" + guest["publicid"] + "/checkin/undo")
                .genericError()
                .success(function (message) {
                    // Nothing todo
                    $("#guestdetails").hide();
                    buttonElem.removeClass("loading");
                })
                .error(function (message) {
                    // Undo UI
                    buttonElem.addClass("checked");
                    guest["elem"].addClass("checked-in");
                    buttonElem.removeClass("loading");
                })
                .send();
        } else {
            // Checkin...

            // Update UI
            buttonElem.addClass("checked");
            guest["elem"].addClass("checked-in");
            // Update model
            guest["checked-in"] = new Date();
            // Do API Call
            buttonElem.addClass("loading");
            Machinata.apiCall("/api/events/event/" + Machinata.Events.GuestList._eventID + "/guest/" + guest["publicid"] + "/checkin")
                .genericError()
                .success(function (message) {
                    // Nothing todo
                    $("#guestdetails").hide();
                    buttonElem.removeClass("loading");
                })
                .error(function (message) {
                    // Undo UI
                    buttonElem.removeClass("checked");
                    guest["elem"].removeClass("checked-in");
                    buttonElem.removeClass("loading");
                })
                .send();
        }
    });

    $(".guestlist-navigator td").click(function () {
        var elem = $(this);
        var groupdId = elem.attr("data-group-id");
        var target = $(".group[data-group-id='" + groupdId + "']");
        var settings = { offset: -$(".event-guests").offset().top };
        Machinata.UI.scrollTo(target, 0, settings);
    });

    Machinata.Events.GuestList.update();
    Machinata.Events.GuestList._updateTimer = setInterval(function () {
        if (Machinata.Events.GuestList.shouldUpdateGuestList() == true) {
            Machinata.Events.GuestList.update();
        }
    }, Machinata.Events.GuestList.UPDATE_INTERVAL_MS)
};


Machinata.Events.GuestList.shouldUpdateGuestList = function () {
    if ($("body").hasClass("loading")) return false; // don't update while doing other api calls
    if ($("#guestdetails").is(":visible")) return false; // don't update while details is open
    if ($("#addguest").is(":visible")) return false; // don't update while add ui is open

    return true;
};


Machinata.Events.GuestList.getGuestForPublicID = function (publicid) {
    // Loop all dtaa
    if (Machinata.Events.GuestList._data == null) return null;
    var guests = Machinata.Events.GuestList._data["guests"];
    if (guests == null) return null;
    for (var i = 0; i < guests.length; i++) {
        if (guests[i]["publicid"] == publicid) return guests[i];
    }
    return null;
};

Machinata.Events.GuestList.search = function (val) {
    if (Machinata.Events.GuestList._data == null) return;

    if (val == null || val == "") {
        $(".group").removeClass("no-match");
        $(".guest").removeClass("no-match");
        $(".group-selector").removeClass("no-match");
        $(".guest").removeClass("is-match");
        return;
    }

    // Do search via javascript...

    // Parse query
    val = Machinata.Events.GuestList.NormalizeText(val.toLowerCase());
    var queries = val.split(" ");
    Machinata.debug("Searching for: '" + val + "'");
    for (var ii = 0; ii < queries.length; ii++) {
        Machinata.debug("  '" + queries[ii] + "'");
    }

    // Loop all dtaa
    var guests = Machinata.Events.GuestList._data["guests"];
    for (var i = 0; i < guests.length; i++) {
        var guest = guests[i];
        var guestSearchText = guest["search-text"];
        var matchesAll = true;
        for (var ii = 0; ii < queries.length; ii++) {
            var query = queries[ii];
            if (guestSearchText.indexOf(query) < 0) {
                matchesAll = false;
                break;
            }
        }
        if (matchesAll == false) {
            guest["elem"].addClass("no-match");
            guest["elem"].removeClass("is-match");
        } else {
            guest["elem"].removeClass("no-match");
            guest["elem"].addClass("is-match");
        }
    }
    // Update groups
    $(".group").each(function () {
        var groupElem = $(this);
        var matches = groupElem.children(".guest.is-match").length;
        if (matches > 0 || groupElem.hasClass("no-filtering")) {
            groupElem.removeClass("no-match");
            groupElem.data("selector").removeClass("no-match");
        } else {
            groupElem.addClass("no-match");
            groupElem.data("selector").addClass("no-match");
        }
    });
};

Machinata.Events.GuestList.showGuestDetails = function (publicid) {
    var guest = Machinata.Events.GuestList.getGuestForPublicID(publicid);
    if (guest == null) alert("COULD NOT GET GUEST!");

    // Register guest
    var overlayElem = $("#guestdetails");
    overlayElem.data("guest-publicid", publicid);
    // Set all properties
    overlayElem.find(".firstname").text(guest["first-name"]);
    overlayElem.find(".lastname").text(guest["last-name"]);
    overlayElem.find(".company").text(guest["company"]).parent().toggle(guest["company"] != null && guest["company"] != "");
    overlayElem.find(".email").text(guest["email"]).parent().toggle(guest["email"] != null && guest["email"] != "");
    overlayElem.find(".entourage").text(guest["entourage"]).parent().toggle(guest["entourage"] != null && guest["entourage"] != "");
    overlayElem.find(".notes").text(guest["notes"]).parent().toggle(guest["notes"] != null && guest["notes"] != "");
    if (guest["checked-in"] != null) overlayElem.find(".button.checkin").addClass("checked");
    else overlayElem.find(".button.checkin").removeClass("checked");
    if (guest["checked-out"] != null) overlayElem.find(".button.checkout").addClass("checked");
    else overlayElem.find(".button.checkout").removeClass("checked");
    // Show UI
    overlayElem.find(".button").removeClass("loading");
    overlayElem.show();
};


Machinata.Events.GuestList.showEditGuest = function (publicid) {
    var guest = Machinata.Events.GuestList.getGuestForPublicID(publicid);
    if (guest == null) alert("COULD NOT GET GUEST!");

    // Register guest
    var overlayElem = $("#editguest");
    overlayElem.find("input").val("");
    overlayElem.data("guest-publicid", publicid);
    // Set all properties
    overlayElem.find(".firstname").val(guest["first-name"]);
    overlayElem.find(".lastname").val(guest["last-name"]);
    overlayElem.find(".company").val(guest["company"]);
    overlayElem.find(".email").val(guest["email"]);
    overlayElem.find(".entourage").val(guest["entourage"]);
    overlayElem.find(".notes").val(guest["notes"]);
    if (guest["checked-in"] != null) overlayElem.find(".button.checkin").addClass("checked");
    else overlayElem.find(".button.checkin").removeClass("checked");
    if (guest["checked-out"] != null) overlayElem.find(".button.checkout").addClass("checked");
    else overlayElem.find(".button.checkout").removeClass("checked");

    // Show UI
    overlayElem.show();
};

Machinata.Events.GuestList.showAddGuest = function () {
    var overlayElem = $("#addguest");
    overlayElem.find("input").val("");
    $("#addguest .button.checkin").removeClass("checked");
    overlayElem.show();
};

Machinata.Events.GuestList.update = function () {
    if (Machinata.Events.GuestList._isUpdating == true) return;

    Machinata.Events.GuestList._isUpdating = true;

    // Make API call to get screen data
    Machinata.apiCall("/api/events/event/" + Machinata.Events.GuestList._eventID)
        .genericError()
        .success(function (message) {
            // Update and validate update-state
            Machinata.Events.GuestList._isUpdating = false;
            if (Machinata.Events.GuestList.shouldUpdateGuestList() == false) return;
         
            // Register data
            Machinata.Events.GuestList._data = message["data"];
            var guests = Machinata.Events.GuestList._data["guests"];
            //console.log("guests: " + guests.length );
            // Reset groups
            $(".group").removeClass("added-guest");
            // Update guests
            for (var i = 0; i < guests.length; i++) {
                var guest = guests[i];
                //console.log(guest["search-text"]);
                // Update elem
                var guestElem = $(".guest[data-public-id='" + guest["publicid"] + "']");
                if (guestElem.length == 0) {
                    // New guest!
                    guestElem = $("<div></div>");
                    guestElem.addClass("guest");
                    guestElem.attr("data-public-id", guest["publicid"]);
                    guestElem.data("sort-text", guest["sort-text"]);
                    guestElem.data("firstname", guest["first-name"]);
                    guestElem.data("lastname", guest["last-name"]);
                    guestElem.data("company", guest["company"]);
                    guestElem.append($("<span class='lastname'></span>").text(guest["last-name"]));
                    guestElem.append($("<span class='firstname'></span>").text(guest["first-name"]));
                    guestElem.append($("<span class='company'></span>").text(guest["company"]));
                    guestElem.append($("<span class='event-icon-check-1'></span>"));
                    guestElem.click(function () {
                        Machinata.Events.GuestList.showGuestDetails($(this).attr("data-public-id"));
                    });
                    // Add to group
                    var groupElem = $(".group[data-group-id='" + guest["group"] + "']");
                    if (groupElem.length == 0) {
                        console.log("NO GROUP for " + guest["search-text"] + "!");
                        groupElem = $(".group[data-group-id='?']");
                    }
                    groupElem.append(guestElem);
                    groupElem.addClass("added-guest");
                } else {
                    // Existing guest!
                    // Update if changed
                    if (guestElem.data("sort-text") != guest["sort-text"]) {
                        // not sure what to do here
                        guestElem.data("sort-text", guest["sort-text"]);
                    }
                    if (guestElem.data("firstname") != guest["first-name"]) {
                        guestElem.find(".firstname").text(guest["first-name"]);
                        guestElem.data("firstname", guest["first-name"]);
                    }
                    if (guestElem.data("lastname") != guest["last-name"]) {
                        guestElem.find(".lastname").text(guest["last-name"]);
                        guestElem.data("lastname", guest["last-name"]);
                    }
                    if (guestElem.data("company") != guest["company"]) {
                        guestElem.find(".company").text(guest["company"]);
                        guestElem.data("company", guest["company"]);
                    }
                }
                if (guest["accepted"] == true) {
                    guestElem.addClass("accepted");
                    guestElem.removeClass("not-accepted");
                } else {
                    guestElem.removeClass("accepted");
                    guestElem.addClass("not-accepted");
                }
                if (guest["checked-in"] != null) guestElem.addClass("checked-in");
                else guestElem.removeClass("checked-in");
                if (guest["checked-out"] != null) guestElem.addClass("checked-out");
                else guestElem.removeClass("checked-out");
                guestElem.data("data", guest);
                guest["elem"] = guestElem;
            }
            // Resort
            $(".group.added-guest").each(function () {
                var groupElem = $(this);
                var groupGuests = groupElem.children(".guest").sort(function (a, b) {
                    var vA = $(a).data("sort-text");
                    var vB = $(b).data("sort-text");
                    return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
                });
                groupElem.append(groupGuests);
            });
        })
        .error(function (message) {
            Machinata.Events.GuestList._isUpdating = false;
        })
        .send();
};








$(document).ready(function () {
    if ($("body").hasClass("event-guestlist-app")) {
        Machinata.Events.GuestList.initAndRun($("body"));
    }
});