using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Documentation.Model;
using Machinata.Core.Documentation;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Module.Documentation.Tests {
    [TestClass]
    public class ParsingTests {
        [TestMethod]
        public void Documenation_Parse() {

            // Discover Machinata Dir
            var possibleMachinataDirs = new List<string> { @"C:\Machinata", @"C:\MachinataServer", @"D:\Code\MachinataServer" };
            possibleMachinataDirs.Add(System.IO.Directory.GetCurrentDirectory() + "\\..\\..\\.."); // this is something like D:\Code\Websites\Cinnamon\MachinataServer\Machinata.Module.Documentation.Tests\bin\Debug
            var machinataDir = possibleMachinataDirs.First();
            foreach (var dir in possibleMachinataDirs) {
                if (System.IO.Directory.Exists(dir)) machinataDir = dir;
            }

            // Discover Documentation Output Dir
            var possibleDocsDirs = new List<string> { @"C:\MachinataDocumentation\", @"D:\Misc\MachinataDocumentation\" };
            var docDir = possibleDocsDirs.First();
            foreach (var dir in possibleDocsDirs) {
                if (System.IO.Directory.Exists(dir)) docDir = dir;
            }

            var sampleCss = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Module.Documentation.Tests\Static\css\machinata-documentation-test-bundle.css",
                ".css",
                "machinata-",
                generateMissingNamespaceItems: false);

            var reporting = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Module.Reporting\Static\js\machinata-reporting-bundle.js",
                ".js",
                "machinata-reporting");


            //***************** JS CORE ************************ //

            var core = DocumentationPackage.ParseFromDirectory(
               machinataDir + @"\Machinata.Core\Static\js\machinata-core-bundle.js",
               ".js",
               "machinata-core");

            var ar = DocumentationPackage.ParseFromDirectory(
              machinataDir + @"\Machinata.Core\Static\js\machinata-ar-bundle.js",
              ".js",
              "machinata-ar");

            var carousel = DocumentationPackage.ParseFromDirectory(
              machinataDir + @"\Machinata.Core\Static\js\machinata-carousel-bundle.js",
              ".js",
              "machinata-carousel");

            var filter = DocumentationPackage.ParseFromDirectory(
              machinataDir + @"\Machinata.Core\Static\js\machinata-filter-bundle.js",
              ".js",
              "machinata-filter");

            var maps = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Core\Static\js\machinata-maps-bundle.js",
                ".js",
                "machinata-maps");

            var masonry = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Core\Static\js\machinata-masonry-bundle.js",
                ".js",
                "machinata-masonry");


            var presentation = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Core\Static\js\machinata-presentation-bundle.js",
                ".js",
                "machinata-presentation");


            var scrollkit = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Core\Static\js\machinata-scrollkit-bundle.js",
                ".js",
                "machinata-scrollkit");


            var threed = DocumentationPackage.ParseFromDirectory(
                machinataDir + @"\Machinata.Core\Static\js\machinata-threed-bundle.js",
                ".js",
                "machinata-threed");


            //***************** End core JS *****************

            var coreCS = DocumentationPackage.ParseFromDirectory(
              machinataDir + @"\Machinata.Core\JSON",
              ".cs",
              "");

            var shopCS = DocumentationPackage.ParseFromDirectory(
             machinataDir + @"\Machinata.Module.Shop",
             ".cs",
             "",
             generateMissingNamespaceItems: true);

            var coreCs = DocumentationPackage.ParseFromDirectory(
               machinataDir + @"\Machinata.Core",
               ".cs",
               "",
               generateMissingNamespaceItems: true);

          

            DocumentationPackage.WriteDocumentations(
                new List<DocumentationPackage>() { reporting, core, coreCS, shopCS, coreCs, sampleCss , ar, carousel, filter, maps, masonry, presentation, scrollkit, threed},
                docDir,
                true, machinataDir
            );
        }
    }
}
