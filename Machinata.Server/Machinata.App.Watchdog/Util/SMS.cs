using Machinata.Watchdog.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Watchdog.Util
{
    public class SMS
    {

        public static void SendSMS(string phone, string message) {
            // Spaces
            string phoneOrig = phone;
            phone = phone.Replace(" ", "");
            phone = phone.Replace(" ", ""); // wierd space from ios
            
            // Other chars
            phone = phone.Replace("-", "");
            phone = phone.Replace("(", "");
            phone = phone.Replace(")", "");

            // Do we have a full phone number?
            phone = phone.Replace("+", "");

            // URL
            string endpoint = Config.GetProgramSettingValue("SMSEndpoint");
            string username = Config.GetProgramSettingValue("SMSUsername");
            string password = Config.GetProgramSettingValue("SMSPassword");
            string apiCall = endpoint;
            apiCall = apiCall.Replace("{username}", username);
            apiCall = apiCall.Replace("{password}", password);
            apiCall = apiCall.Replace("{message}", HttpUtility.UrlEncode(message));
            apiCall = apiCall.Replace("{phone}", HttpUtility.UrlEncode(phone));

            // Make request to gateway
            HttpWebRequest request = WebRequest.CreateHttp(apiCall);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string responseData = reader.ReadToEnd();
            reader.Close();
            response.Close();

            // Error?
            if (!responseData.StartsWith("OK:")) {
                // Inject error information
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 11")) responseData = "Error 11 (upstream error)";
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 402")) responseData = "Error 402 (bad username/password or system error)";
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 88")) responseData = "Error 88 (no credits)";
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 102")) responseData = "Error 102 (system timeout)";
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 8")) responseData = "Error 8 (source or destination is too short)";
                if(responseData.Trim().ToUpper().StartsWith("ERROR: 13")) responseData = "Error 13 (unable to contact carrier)";
                // Send email and backend error
                throw new Exception("There was a problem sending the SMS: "+responseData+". Converted number: " + phoneOrig + " >> " + phone + ".");
            }
        }
    }
}
