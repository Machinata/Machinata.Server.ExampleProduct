using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Machinata.Watchdog.Util {

    class Config {

        public static string GetProgramSettingValue(string name, string defaultValue = null) {
            var val = System.Configuration.ConfigurationManager.AppSettings[name];
            if (val != null) return val;
            else return defaultValue;
        }

        /// <summary>
        /// Gets the configuration string setting value.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetConfigurationStringSettingValue(string configurationString, string name, string defaultValue = null) {
            // Configuration strings look like:
            // name=Web HTTP;interval=20s;sms=true;email=true;enabled=true;url=http://boldomatic.com/heartbeat
            try {
                var keyvals = configurationString.Split(';');
                foreach (var keyval in keyvals) {
                    var key = keyval.Split('=').First();
                    var val = keyval.Split(new char[]{'='},2).Skip(1).First();
                    if (!string.IsNullOrEmpty(val)&& val.StartsWith("'")&& val.EndsWith("'")){
                        val = val.Trim('\'');
                    }
                    if (key == name) return val;
                }
            } catch (Exception e) {
                throw new Exception("There was an error reading the configuration string: "+configurationString,e);
            }
            return defaultValue;
        }

        public static int GetConfigurationIntSettingValue(string configurationString, string name, int defaultValue) {
            var ret = GetConfigurationStringSettingValue(configurationString, name, null);
            if (ret == null) return defaultValue;
            return int.Parse(ret);
        }

        public static bool GetConfigurationBoolSettingValue(string configurationString, string name, bool defaultValue) {
            var ret = GetConfigurationStringSettingValue(configurationString, name, null);
            if (ret == null) return defaultValue;
            return bool.Parse(ret);
        }

        public static TimeSpan GetConfigurationTimespanSettingValue(string configurationString, string name, TimeSpan defaultValue) {
            var ret = GetConfigurationStringSettingValue(configurationString, name, null);
            if (ret == null) return defaultValue;
            if (ret.EndsWith("d")) {
                ret = ret.Replace("d", "");
                return new TimeSpan(int.Parse(ret), 0, 0, 0); // d,h,m,s
            }
            if (ret.EndsWith("h")) {
                ret = ret.Replace("h", "");
                return new TimeSpan(int.Parse(ret), 0, 0); // h,m,s
            }
            if (ret.EndsWith("m")) {
                ret = ret.Replace("m", "");
                return new TimeSpan(0, int.Parse(ret), 0); // h,m,s
            }
            if (ret.EndsWith("s")) {
                ret = ret.Replace("s", "");
                return new TimeSpan(0, 0, int.Parse(ret)); // h,m,s
            }
            throw new Exception("Unknown interval value: "+ret);
        }
        
    }
}
