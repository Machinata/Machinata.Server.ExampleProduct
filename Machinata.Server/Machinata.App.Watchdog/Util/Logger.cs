using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Watchdog.Util {
    public static class FileLogger {
       

        public static void Initialize(IEnumerable<string> targets) {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            foreach (var target in targets) {
                // Step 2. Create targets and add them to the configuration 
                var fileTarget = new FileTarget();
                config.AddTarget(target, fileTarget);

                var logDestination = "${basedir}"; // default
                var configLogDestination = System.Configuration.ConfigurationManager.AppSettings["LoggingDestination"];
                if (!string.IsNullOrEmpty(configLogDestination)) {
                    logDestination = configLogDestination;
                }
                fileTarget.FileName = logDestination + "/Watchdog_Healhreport_" + target + ".log";
                fileTarget.Name = target;
                fileTarget.MaxArchiveFiles = 30;
                fileTarget.ArchiveNumbering = ArchiveNumberingMode.Date;
                fileTarget.ArchiveEvery = FileArchivePeriod.Day;
              

                // Step 4. Define rules
                var rule1 = new LoggingRule(target, LogLevel.Debug, fileTarget);
                config.LoggingRules.Add(rule1);
            }
         
            // Step 5. Activate the configuration
            LogManager.Configuration = config;

            
        }

        public static ILogger GetLogger(string target){
          
            return LogManager.GetLogger(target);
           

        }
    }
}
