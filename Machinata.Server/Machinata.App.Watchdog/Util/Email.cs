using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.IO;

namespace Machinata.Watchdog.Util {

    public class Email
    {
    

        public static void SendEmail(string to, string cc, string bcc, string subject, string message, bool html) {
            
            // Init
            string server = Config.GetProgramSettingValue("EmailServer");
            string port = Config.GetProgramSettingValue("EmailPort");
            string username = Config.GetProgramSettingValue("EmailUsername");
            string senderAddress = Config.GetProgramSettingValue("EmailSenderAddress");
            string senderName = Config.GetProgramSettingValue("EmailSenderName");
            string password = Config.GetProgramSettingValue("EmailPassword");
            string overrideAddress = Config.GetProgramSettingValue("EmailOverrideAddress");
            
            // Filters
            subject = subject.Replace("\r", " ").Replace("\n", " ");

			// Create message
			System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.IsBodyHtml = html;
            mail.From = new System.Net.Mail.MailAddress(senderAddress, senderName);
            if (overrideAddress == "") {
                if (to == null || to == "") throw new Exception("No <to> email address provided.");
                foreach (string toAddress in to.Split(',')) mail.To.Add(toAddress);
                if (cc != null) mail.CC.Add(cc);
                if (bcc != null) mail.Bcc.Add(bcc);
            } else {
                mail.To.Add(overrideAddress);
            }
			mail.Subject = subject;
			mail.Body = message;

			// Send it!
            try {
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient(server);
                SmtpServer.Port = System.Convert.ToInt32(port);
                SmtpServer.Credentials = new System.Net.NetworkCredential(username, password);
                SmtpServer.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };//TODO: this should be removed at some point
                SmtpServer.Send(mail);
            } catch (Exception e) {
                throw new Exception("STMP mail send failed for "+username+"@"+server+". "+e.Message, e);
            }
        }
    }
}