using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Watchdog.Util {
    public class Ping {

        public long Number;
        public DateTime Timestamp;
        public long ResponseTime;
        public HttpStatusCode StatusCode;
        public bool Healthy = false;
       public long Bytes = -1;
    //    public long ContentLength = -1;
        public string UnhealthyReason = null;
        public string HealthReport;
        public string MachineId {
            get {
               try{
                   var healthReport = JObject.Parse(HealthReport);
                   return healthReport["Data"]["MachineID"].ToString();
               }
                catch{
                    return null;
                }
            }
        }

        public string CompileLog(bool verbose) {
            StringBuilder log = new StringBuilder();
            string subject = "#" + Number;
            if (!Healthy) {
                subject += " UNHEALTHY";
            }
            if (verbose) {
                subject = "Ping " + subject;
                log.AppendLine(subject);
                if (!Healthy) log.AppendLine("   " + "UnhealthyReason: "+UnhealthyReason);
                log.AppendLine("   " + "Healthy: "+Healthy.ToString());
                log.AppendLine("   " + "Timestamp: "+Timestamp);
                log.AppendLine("   " + "StatusCode: "+(int)StatusCode + " " + StatusCode);
                log.AppendLine("   " + "ResponseTime: "+ResponseTime);
                log.AppendLine("   " + "Length: "+this.Bytes);
            } else {
                subject = "Ping " + subject;
                log.AppendLine(subject);
                if (!Healthy) log.AppendLine(UnhealthyReason);
            }
            string ret = log.ToString();
            if (!verbose) ret = ret.Trim();
            return ret;
        }
    }

    
}
