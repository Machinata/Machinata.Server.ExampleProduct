using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace Machinata.Watchdog.Util {

    public class Endpoint {
        
        public Logic.Watchdog Watchdog;
        public string Name;
        public string URL;
        public TimeSpan Interval;
        public int PingsToKeep = 50; // default
        public int UnhealthyThreshold = 4; // number of fails to cause unhealthy state
        public int HealthyThreshold = 2; // number of success to cause healthy state
        public bool SendSMSWarnings = true;
        public bool SendEmailWarnings = true;
        public int Timeout = 15000; // ms
        public long HealthyMinContentLength = 100; // bytes
        public long HealthyMaxResponseTime = 4000; // ms
        public HttpStatusCode HealthyStatusCode = HttpStatusCode.OK;
        public int Pings = 0;
        public string Id;
        public bool HealthReport; // endpoint provides a healthreport json

        private bool _isHealthy = true;
        private DateTime _lastCheck;
        private List<Ping> _pings;

     

        public Endpoint(string name, string url, TimeSpan interval) {
            this.Name = name;
            this.URL = url;
            this.Interval = interval;
            _commonInit();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Endpoint"/> class.
        /// </summary>
        /// <param name="configurationString">The configuration string. These look like name=Web HTTP;interval=20s;sms=true;email=true;enabled=true;url=http://boldomatic.com/heartbeat</param>
        public Endpoint(string key, string configurationString) {
            this.Id = key;
            this.Name = Util.Config.GetConfigurationStringSettingValue(configurationString,"name");
            this.URL = Util.Config.GetConfigurationStringSettingValue(configurationString, "url");
            this.Interval = Util.Config.GetConfigurationTimespanSettingValue(configurationString, "interval", new TimeSpan(0,0,20));
            this.PingsToKeep = Util.Config.GetConfigurationIntSettingValue(configurationString, "pingstokeep", 50);
            this.UnhealthyThreshold = Util.Config.GetConfigurationIntSettingValue(configurationString, "unhealthythreshold", 4);
            this.HealthyThreshold = Util.Config.GetConfigurationIntSettingValue(configurationString, "healthythreshold", 2);
            this.HealthyMinContentLength = Util.Config.GetConfigurationIntSettingValue(configurationString, "healthyminbytes", 100);
            this.HealthyMaxResponseTime = Util.Config.GetConfigurationIntSettingValue(configurationString, "healthymaxresponsetime", 4000);
            this.Timeout = Util.Config.GetConfigurationIntSettingValue(configurationString, "timeout", 15000);
            this.SendSMSWarnings = Util.Config.GetConfigurationBoolSettingValue(configurationString, "sms", true);
            this.SendEmailWarnings = Util.Config.GetConfigurationBoolSettingValue(configurationString, "email", true);
            this.HealthReport = Util.Config.GetConfigurationBoolSettingValue(configurationString, "healthreport", false);
            _commonInit();
        }

        private void _commonInit() {
            _lastCheck = new DateTime(0);
            _pings = new List<Ping>(PingsToKeep);
        }

        public bool ShouldCheckEndpoint() {
            var ticksSinceLastCheck = DateTime.UtcNow.Ticks - _lastCheck.Ticks;
            return (ticksSinceLastCheck >= this.Interval.Ticks);
        }

        public bool IsHealthy() {
            return _isHealthy;
        }

        public void CheckEndpoint() {
            bool outputEndpointLog = false;
            if (Watchdog.Verbose) {
                outputEndpointLog = true;
                Console.WriteLine("Checking Endpoint " + this.URL + "...");
            }
            
            // Init
            Pings++;
            Ping ping = new Ping();
            ping.Number = Pings;
            ping.StatusCode = HttpStatusCode.Unused;
            Stopwatch timer = Stopwatch.StartNew();
            ping.Timestamp = DateTime.UtcNow;
            ping.Healthy = true;

            // Log to file
            var logger = FileLogger.GetLogger(Id);

            // Make request
            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.URL);
                request.AllowAutoRedirect = false;
                request.Method = "GET";
                request.Timeout = this.Timeout; //ms
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                ping.StatusCode = response.StatusCode;
            
                
                //Read response
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string strResponse = reader.ReadToEnd();

                //Bytes
                if (reader.CurrentEncoding != null) {
                    ping.Bytes = reader.CurrentEncoding.GetByteCount(strResponse);
                }

                // Healthreport
                if (this.HealthReport) {
                    ping.HealthReport = strResponse.Replace("\r\n  ", string.Empty);
                }


                if(Watchdog.Verbose) Console.WriteLine("   " + (int)response.StatusCode + " " + response.StatusDescription);
                if(Watchdog.Verbose) Console.WriteLine("   " + response.ContentLength + " bytes");


                response.Close();

              

                if (this.HealthReport) {
                    logger.Log(LogLevel.Info, string.Format("{0}: {1}", ping.MachineId, ping.HealthReport));
                }
                else {
                    logger.Log(LogLevel.Info, string.Format("Response Time: {0}ms, Content: {1}bytes", timer.ElapsedMilliseconds, ping.Bytes));
                }

            } catch (System.Net.WebException e) {
                if (!outputEndpointLog) { Console.WriteLine("Checking Endpoint " + this.URL + "..."); outputEndpointLog = true;  }
                Console.WriteLine("   ***WARNING: " + e.Message);
                ping.StatusCode = HttpStatusCode.ServiceUnavailable;
                ping.Healthy = false;
                ping.UnhealthyReason = e.Message;
            } catch (Exception e) {
                if (!outputEndpointLog) { Console.WriteLine("Checking Endpoint " + this.URL + "..."); outputEndpointLog = true;  }
                Console.WriteLine("   ***WARNING: " + e.Message);
                ping.StatusCode = HttpStatusCode.ServiceUnavailable;
                ping.Healthy = false;
                ping.UnhealthyReason = e.Message;
            } finally {
                // Bookeeping
                timer.Stop();
                ping.ResponseTime = timer.ElapsedMilliseconds;
                if(Watchdog.Verbose) Console.WriteLine("   " + timer.ElapsedMilliseconds + " ms");
            }

            // Healthy?
            if (ping.Healthy && ping.StatusCode != this.HealthyStatusCode && ping.StatusCode != HttpStatusCode.ServiceUnavailable) {
                ping.Healthy = false;
                ping.UnhealthyReason = "invalid status code ("+(int)ping.StatusCode+" "+ping.StatusCode+")";
                if (!outputEndpointLog) { Console.WriteLine("Checking Endpoint " + this.URL + "..."); outputEndpointLog = true;  }
                var message = "   ***WARNING: " + ping.UnhealthyReason;
                Console.WriteLine(message);
                logger.Log(LogLevel.Warn, message);
            }
            if (ping.Healthy && ping.Bytes < this.HealthyMinContentLength) {
                ping.Healthy = false;
                ping.UnhealthyReason = "too few characters ("+ping.Bytes+" bytes)";
                if (!outputEndpointLog) { Console.WriteLine("Checking Endpoint " + this.URL + "..."); outputEndpointLog = true;  }
                var message = "   ***WARNING: " + ping.UnhealthyReason;
                Console.WriteLine(message);
                logger.Log(LogLevel.Warn, message);
            }
            if (ping.Healthy && ping.ResponseTime > this.HealthyMaxResponseTime) {
                ping.Healthy = false;
                ping.UnhealthyReason = "response too slow ("+ping.ResponseTime+" ms)";
                if (!outputEndpointLog) { Console.WriteLine("Checking Endpoint " + this.URL + "..."); outputEndpointLog = true;  }
                var message = "   ***WARNING: " + ping.UnhealthyReason;
                Console.WriteLine(message);
                logger.Log(LogLevel.Warn, message);
            }

          
       
            // Register metrics
            _lastCheck = DateTime.UtcNow;
            AddPing(ping);
        }

        /// <summary>
        /// Returns a list of recent pings based on the FailThreshold count.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Ping> GetRecentPings() {
            int recent = Math.Max(UnhealthyThreshold, HealthyThreshold);
            return _pings.Skip(_pings.Count - recent).Take(recent);;
        }

        /// <summary>
        /// Returns all failed pings.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Ping> GetFailedPings() {
            return _pings.Where(p => p.Healthy == false);
        }

        /// <summary>
        /// Validates the endpoint.
        /// </summary>
        /// <returns>True if a transition occured.</returns>
        public bool ValidateEndpoint() {
            // Check the last x pings
            var recentPings = GetRecentPings();
            int numPings = recentPings.Count();
            int numHealthy = recentPings.Where(p => p.Healthy == true).Count();
            int numUnhealthy = recentPings.Where(p => p.Healthy == false).Count();
            // Validate
            bool previousState = _isHealthy;
            if (numUnhealthy >= UnhealthyThreshold) {
                _isHealthy = false;
            } else if (numHealthy >= HealthyThreshold) {
                _isHealthy = true;
            }
            return previousState != _isHealthy;
        }

        public void AddPing(Ping ping) {
            // Register
            _pings.Add(ping);
            // Prune
            if (_pings.Count > PingsToKeep) {
                _pings.RemoveRange(0, _pings.Count - PingsToKeep);
            }
        }

        public string CompileLog(bool verbose) {
            // Init
            StringBuilder log = new StringBuilder();
            
            // Settings
            if (verbose) {
                log.AppendLine("Name: " + Name);
                log.AppendLine("URL: " + URL);
                log.AppendLine("Interval: " + Interval);
                log.AppendLine("PingsToKeep: " + PingsToKeep);
                log.AppendLine("UnhealthyThreshold: " + UnhealthyThreshold);
                log.AppendLine("HealthyThreshold: " + HealthyThreshold);
                log.AppendLine("SendSMSWarnings: " + SendSMSWarnings);
                log.AppendLine("SendEmailWarnings: " + SendEmailWarnings);
                log.AppendLine("Timeout: " + Timeout);
                log.AppendLine("HealthyMinBytes: " + HealthyMinContentLength);
                log.AppendLine("HealthyMaxResponseTime: " + HealthyMaxResponseTime);
                log.AppendLine("HealthyStatusCode: " + HealthyStatusCode);
                log.AppendLine("Pings: " + Pings);
                log.AppendLine("");
            } else {
                log.AppendLine(URL);
            }

            // Pings
            var pings = this.GetRecentPings();
            if (verbose) pings = _pings;
            pings = pings.OrderByDescending(p => p.Number);
            foreach (var ping in pings) {
                log.AppendLine(ping.CompileLog(verbose));
            }
            return log.ToString();
        }


        
    }
}
