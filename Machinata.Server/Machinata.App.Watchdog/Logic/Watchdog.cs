using Machinata.Watchdog.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Watchdog.Logic {
    public class Watchdog {

        public int MaxWarningsPerHour = 6; //TODO
        public bool Verbose = Util.Config.GetProgramSettingValue("Verbose","false") == "true";

        private List<Endpoint> _endpoints = new List<Endpoint>();
        private List<string> _contacts = new List<string>();
        private static object _logger = new object();

        public IEnumerable<Endpoint> Endpoints {
            get {
                return _endpoints;
            }
        }

        public Watchdog() {

        }

        public void RegisterContact(string emailOrPhone) {
            _contacts.Add(emailOrPhone);
        }

        public void RegisterEndpoint(Endpoint endpoint) {
            Console.WriteLine("Registering endpoint "+endpoint.Name);
            Console.WriteLine("   "+endpoint.URL);
            endpoint.Watchdog = this;
            _endpoints.Add(endpoint);
        }

        public void Monitor() {
            // Init
            string machineName = Environment.MachineName;
            Console.WriteLine("Starting monitor with " + _endpoints.Count + " endpoints on " + machineName);
            Console.WriteLine("Sending startup notification...");

            // Send startup notifs
            foreach (string contact in _contacts) {
                string message = DateTime.UtcNow.ToLongDateString();
                message += "\n";
                message += "\n" + "Registered "+_contacts.Count + " contacts";
                message += "\n";
                foreach (var contact2 in _contacts) {
                    message += "\n" + contact2;
                }
                message += "\n";
                message += "\n" + "Starting monitor with "+_endpoints.Count + " endpoints";
                message += "\n";
                foreach (var endpoint in _endpoints) {
                    message += "\n" + ("Endpoint "+endpoint.Name);
                    message += "\n" + endpoint.CompileLog(true);
                }
                if (contact.Contains('@')) {
                    // Email
                    try {
                        if(Verbose) Console.WriteLine("   "+"Sending Email to "+contact);
                        Email.SendEmail(contact, null, null, "BoldomaticWatchdog: " + "Started on " + machineName, message, false);
                    } catch (Exception e) {
                        Console.WriteLine("***WARNING: Could not send Email to "+contact+": "+e.Message);
                    }
                }
            }
            Console.WriteLine("Done.");

            // Loop forever
            while (true) {
                // Loop endpoints
                List<Endpoint> transitionedEndpoints = new List<Endpoint>();
                foreach (var endpoint in _endpoints) {
                    if (endpoint.ShouldCheckEndpoint()) {
                        endpoint.CheckEndpoint();
                        bool didTransition = endpoint.ValidateEndpoint();
                        if (didTransition) transitionedEndpoints.Add(endpoint);
                        
                    }
                }

                // Compile report of all transitions
                foreach(var endpoint in transitionedEndpoints) {
                    string subject = "";
                    if (endpoint.IsHealthy()) {
                        Console.WriteLine("TRANSITION: " + endpoint.Name + " is HEALTHY");
                        subject = "INFO: " + endpoint.Name + " is " + "HEALTHY";
                    } else {
                        Console.WriteLine("TRANSITION: " + endpoint.Name + " is UNHEALTHY");
                        subject = "WARNING: " + endpoint.Name + " is " + "UNHEALTHY";
                    }
                    string emailData = endpoint.CompileLog(true);
                    string smsData = endpoint.CompileLog(false);
                    if (smsData.Length > 155) smsData = smsData.Substring(0, 155);
                    //smsData = smsData.Substring(0,155);
                    foreach (string contact in _contacts) {
                        if (contact.Contains('@') && endpoint.SendEmailWarnings) {
                            // Email
                            try {
                                if(Verbose) Console.WriteLine("   "+"Sending Email to "+contact);
                                Util.Email.SendEmail(contact, null, null, "BoldomaticWatchdog: " + subject, emailData, false);
                            } catch (Exception e) {
                                Console.WriteLine("***WARNING: Could not send Email to "+contact+": "+e.Message);
                            }
                        } 
                        if(!contact.Contains('@') && endpoint.SendSMSWarnings) {
                            // SMS
                            try {
                                if(Verbose) Console.WriteLine("   "+"Sending SMS to "+contact);
                                SMS.SendSMS(contact, subject+"\n"+smsData);
                            } catch (Exception e) {
                                Console.WriteLine("***WARNING: Could not send SMS to "+contact+": "+e.Message);
                            }
                        }
                    }
                }

                // Show state
                if (Verbose) {
                    Console.WriteLine("Endpoints:");
                    foreach (var endpoint in _endpoints) {
                        Console.WriteLine("   "+endpoint.Name + ": " + endpoint.IsHealthy().ToString());
                    }
                }

                // Sleep
                if (Verbose) Console.WriteLine("Sleeping...");
                System.Threading.Thread.Sleep(1000);
            }
        }

    }
}
