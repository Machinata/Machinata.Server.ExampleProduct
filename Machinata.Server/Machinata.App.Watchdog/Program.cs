using Machinata.Watchdog.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Watchdog.Logic;
using Machinata.Watchdog.Util;

namespace Machinata.App.Watchdog {
    class Program {

        static void Main(string[] args) {
            // Configure
            Console.WriteLine("Starting Watchdog on " + Environment.MachineName + "...");
            // Loop forever
            while (true) {
                try {
                    // Start fail/retry safe
                    StartWatchdogWithConfiguration();
                } catch (Exception e) {
                    // Send warning
                    try {
                        Email.SendEmail("support@boldomatic.com", null, null, "BoldomaticWatchdog: WARNING: Watchdog Crashed on " + Environment.MachineName, e.Message, false);
                    } catch (Exception emailException) { }
                    // Wait a while
                    System.Threading.Thread.Sleep(5000);
                }
            }
        }

        public static void StartWatchdogWithConfiguration() {

            //TODO: SSL doesnt seem to work on Mono/Linux

            // Defaults
            TimeSpan defaultInterval = new TimeSpan(0, 0, 20); // h,m,s

            // Create watchdog
            Machinata.Watchdog.Logic.Watchdog watchdog = new Machinata.Watchdog.Logic.Watchdog();

            // Contacts
            foreach (var key in System.Configuration.ConfigurationManager.AppSettings.AllKeys) {
                if (key.StartsWith("Contact-")) {
                    watchdog.RegisterContact(System.Configuration.ConfigurationManager.AppSettings[key]);
                }
            }

            // Register endpoints
            foreach (var key in System.Configuration.ConfigurationManager.AppSettings.AllKeys) {
                if (key.StartsWith("Endpoint-")) {
                    try {
                        Endpoint endpoint = new Endpoint(key, System.Configuration.ConfigurationManager.AppSettings[key]);
                        watchdog.RegisterEndpoint(endpoint);
                    } catch (Exception e) {
                        Email.SendEmail("support@boldomatic.com", null, null, "BoldomaticWatchdog: WARNING: Could not load configuration for " + key, e.Message, false);
                    }
                }
            }

            // Initialize Loggers
            FileLogger.Initialize(watchdog.Endpoints.Select(e => e.Id));


            // Start monitor
            watchdog.Monitor();
        }
    }

    
}
