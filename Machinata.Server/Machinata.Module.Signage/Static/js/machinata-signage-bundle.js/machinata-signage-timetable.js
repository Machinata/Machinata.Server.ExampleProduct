
Machinata.Signage.Timetable = {};

Machinata.Signage.Timetable.updateRelativeTimes = function() {
    $(".relative-time").each(function () {
        var elem = $(this);
        var dateStr = elem.attr("data-date"); // 23.10.19
        var timeStr = elem.attr("data-time"); // 15:42
        var now = new Date();
        var time = new Date();
        time.setFullYear(       2000+parseInt(dateStr.split('.')[2])    );
        time.setMonth(          parseInt(dateStr.split('.')[1])-1       );
        time.setDate(           parseInt(dateStr.split('.')[0])         );
        time.setHours(          parseInt(timeStr.split(':')[0])         );
        time.setMinutes(        parseInt(timeStr.split(':')[1])         );
        time.setSeconds(        0                                       );
        time.setMilliseconds(   0                                       );
        var text = Machinata.Time.humanReadableTimestamp(time);
        elem.text(text);
        if (time < now) {
            elem.closest(".connection").addClass("is-past");
        } else {
            elem.closest(".connection").removeClass("is-past");
        }
    });
}

Machinata.Signage.Timetable.init = function () {
    Machinata.Signage.init();

    Machinata.Signage.Timetable.updateRelativeTimes();
    setInterval(function () {
        Machinata.Signage.Timetable.updateRelativeTimes();
    }, 1000);
};