
Machinata.Signage = {};

Machinata.Signage.init = function () {
    Machinata.UI.enableAutoReload();
    if (Machinata.queryParameter("fullscreen") == "true") {
        $("body").addClass("fullscreen");
    }
    if (Machinata.queryParameter("debug") == "true") {
        $("body").addClass("debug");
    }
    if (Machinata.queryParameter("header") == "false") {
        $("body").addClass("no-header");
    }
    if (Machinata.queryParameter("footer") == "false") {
        $("body").addClass("no-footer");
    }
};