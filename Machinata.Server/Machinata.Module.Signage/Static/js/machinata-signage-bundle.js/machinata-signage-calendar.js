
Machinata.Signage.Calendar = {};

Machinata.Signage.Calendar.getTime = function () {
    var now = new Date();
    var timeStr = Machinata.queryParameter("time");
    if (timeStr != null && timeStr != "") {
        var dateStr = Machinata.queryParameter("date");
        if (dateStr != null && dateStr != "") {
            var dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI);
            now = Machinata.dateFromString(dateStr, dateFormat);
        }
        now.setHours(timeStr.split(':')[0]);
        now.setMinutes(timeStr.split(':')[1]);
        now.setSeconds(0);
        now.setMilliseconds(0);
    }
    return now;
};

Machinata.Signage.Calendar.updateSchedule = function () {
    var slots = $(".ui-schedule-slot");
    var items = $(".ui-schedule-item");
    var dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(Machinata.DEFAULT_DATETIME_FORMAT_JAVASCRIPT_UI);
    var now = Machinata.Signage.Calendar.getTime();
    var isToday = $(".outlookcalendar-sign").attr("data-is-today") == "true";
    Machinata.debug("now:" + now);
    Machinata.debug("isToday:" + isToday);

    // Pre-pass
    // Update all the slots
    Machinata.debug("Prepass slots...");
    var slotMinStart = null;
    var slotMaxEnd = null;
    slots.each(function () {
        var slotElem = $(this);
        var slotStart = Machinata.dateFromString(slotElem.attr("data-start"), dateFormat);
        var slotEnd = Machinata.dateFromString(slotElem.attr("data-end"), dateFormat);
        slotElem.data("slot-start", slotStart);
        slotElem.data("slot-end", slotEnd);
        if (slotMinStart == null || slotStart < slotMinStart) slotMinStart = slotStart;
        if (slotMaxEnd == null || slotEnd > slotMaxEnd) slotMaxEnd = slotEnd;
        if (slotEnd < now) {
            slotElem.addClass("in-past");
        } else {
            slotElem.removeClass("in-past");
        }
        if (now >= slotStart && now <= slotEnd) {
            slotElem.addClass("is-now");
        } else {
            slotElem.removeClass("is-now");
        }
    });
    Machinata.debug("  slotMinStart: " + slotMinStart);
    Machinata.debug("  slotMaxEnd: " + slotMaxEnd);



    // Assign all items to their slots
    Machinata.debug("Assigning items to slots...");
    var currentItem = null;
    var currentItemEnd = null;
    slots.addClass("no-items");
    items.each(function () {
        var itemElem = $(this);
        Machinata.debug("Item:" + itemElem.attr("data-subject"));
        var itemStart = Machinata.dateFromString(itemElem.attr("data-start"), dateFormat);
        var itemEnd = Machinata.dateFromString(itemElem.attr("data-end"), dateFormat);
        itemElem.data("item-start", itemStart);
        itemElem.data("item-end", itemEnd);
        Machinata.debug("  Start:" + itemElem.attr("data-start"));
        Machinata.debug("  End:" + itemElem.attr("data-end"));
        if (itemEnd < now) {
            itemElem.addClass("in-past");
        } else {
            itemElem.removeClass("in-past");
        }
        if (now >= itemStart && now <= itemEnd && currentItem == null) {
            Machinata.debug("  Current Item");
            currentItem = itemElem;
            currentItemEnd = Machinata.formattedDate(itemEnd, "HH:mm");
        }
        var matchFound = false;
        slots.each(function () {
            var slotElem = $(this);
            var slotStart = slotElem.data("slot-start");
            var slotEnd = slotElem.data("slot-end");
            if (itemStart >= slotStart && itemStart <= slotEnd) {
                matchFound = true;
                slotElem.removeClass("no-items");
                return;
            }
        });
        if (matchFound == false) itemElem.remove();
    });

    // Determine status free/busy
    // Note: this is a brute force method that probes the schedule minute by minute. It seems the most efficient if you have random schedule items with random overlapping timeslots.
    Machinata.debug("Finding busy timeframe...");
    var busyStart = null;
    var busyEnd = null;
    var freeStart = null;
    var freeEnd = null;
    var slotWindowFromNowInMS = slotMaxEnd.getTime() - now.getTime();
    var slotWindowFromNowInMin = Math.floor(slotWindowFromNowInMS / 1000 / 60);
    Machinata.debug("  " + slotWindowFromNowInMin + "min");
    var cc = 0;
    for (var m = 0; m <= slotWindowFromNowInMin; m++) {
        var mDate = new Date(now.getTime() + (m * 60 * 1000));
        mDate.setSeconds(0);
        mDate.setMilliseconds(0);
        var matchingItem = null;
        items.each(function () {
            cc++;
            var itemElem = $(this);
            var itemStart = itemElem.data("item-start");
            var itemEnd = itemElem.data("item-end");
            if (mDate >= itemStart && mDate <= itemEnd) {
                matchingItem = itemElem;
                return;
            }
        });
        if (m == 0) {
            // Now
            if (matchingItem != null) {
                busyStart = matchingItem.data("item-start");
            } else {
                freeStart = new Date(mDate.getTime());
            }
        } else {
            // Future
            if (busyStart != null) {
                // We were busy, is it now free?
                if (matchingItem == null) {
                    busyEnd = new Date(mDate.getTime() - (60*1000)); // roll back one minute
                    break;
                }
            } else {
                // We are free, is it busy now?
                if (matchingItem != null) {
                    freeEnd = new Date(mDate.getTime()); 
                    break;
                }
            }
        }
    }
    Machinata.debug("  cc:" + cc);
    Machinata.debug("  busyStart:" + busyStart);
    Machinata.debug("  busyEnd:" + busyEnd);
    Machinata.debug("  freeStart:" + freeStart);
    Machinata.debug("  freeEnd:" + freeEnd);

    // Busy/free?
    if (isToday == false && Machinata.DebugEnabled == false) {
        $(".ui-listing-flag.status-busy").hide();
        $(".ui-listing-flag.status-free").hide();
    }else if (busyStart != null) {
        // Busy
        if (busyEnd != null) {
            $(".ui-listing-flag.status-busy .status").text("{text.calendar-busy-until} " + Machinata.formattedTimeFromDate(busyEnd));
        } else {
            $(".ui-listing-flag.status-busy .status").text("{text.calendar-busy}");
        }
        $(".ui-listing-flag.status-busy").show();
        $(".ui-listing-flag.status-free").hide();
    } else {
        // Free
        if (freeEnd != null) {
            $(".ui-listing-flag.status-free .status").text("{text.calendar-free-until} " + Machinata.formattedTimeFromDate(freeEnd));
        } else {
            $(".ui-listing-flag.status-free .status").text("{text.calendar-free}");
        }
        $(".ui-listing-flag.status-busy").hide();
        $(".ui-listing-flag.status-free").show();

    }

    // Position all elements
    Machinata.debug("Positioning items...");
    function getYPositionForTime(time) {
        for (var i = 0; i < slots.length; i++) {
            var slotElem = $(slots[i]);
            var slotStart = Machinata.dateFromString(slotElem.attr("data-start"), dateFormat);
            var slotEnd = Machinata.dateFromString(slotElem.attr("data-end"), dateFormat);
            if (time >= slotStart && time <= slotEnd) {
                var hh = slotElem.height();
                var dy = time.getMinutes() / 60.0 * hh;
                return slotElem.position().top + dy;
            }
        };
        return -1;
    }
    items.each(function () {
        var itemElem = $(this);
        Machinata.debug("Item:" + itemElem.attr("data-subject"));
        var itemStart = Machinata.dateFromString(itemElem.attr("data-start"), dateFormat);
        var itemEnd = Machinata.dateFromString(itemElem.attr("data-end"), dateFormat);
        var itemMinutes = (itemEnd - itemStart) / 1000 / 60;
        if (itemMinutes < 30) {
            itemElem.addClass("small-slot");
        } else {
            itemElem.removeClass("small-slot");
        }
        var y1 = getYPositionForTime(itemStart);
        var y2 = getYPositionForTime(itemEnd);
        itemElem.css("top", y1 + "px");
        itemElem.css("height", (y2 - y1) - 1 + "px");
        Machinata.debug("  y1: " + y1);
        Machinata.debug("  y2: " + y2);
    });
};


Machinata.Signage.Calendar.updateRelativeTimes = function() {
    $(".relative-time.is-today").each(function () {
        var elem = $(this);
        var timeStr = elem.attr("data-time");
        var time = new Date();
        time.setHours(timeStr.split(':')[0]);
        time.setMinutes(timeStr.split(':')[1]);
        time.setSeconds(0);
        time.setMilliseconds(0);
        var text = Machinata.Time.humanReadableTimestamp(time, Machinata.Signage.Calendar.getTime());
        elem.text(text);
    });
}

Machinata.Signage.Calendar.init = function () {
    Machinata.Signage.init();

    // Assign all items to slots
    Machinata.Signage.Calendar.updateSchedule();

    // Start updating relative times
    Machinata.Signage.Calendar.updateRelativeTimes();
    setInterval(function () {
        Machinata.Signage.Calendar.updateRelativeTimes();
    }, 1000);
};