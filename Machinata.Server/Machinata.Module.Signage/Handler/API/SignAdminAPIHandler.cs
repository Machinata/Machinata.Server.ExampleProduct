using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Machinata.Module.Signage.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.Signage.Logic;

namespace Machinata.Product.NervesEvents.Handler.API {
    public class SignAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Signage.Model.Sign> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("signage");
        }

        #endregion


        [RequestHandler("/api/admin/signage/sign/create")]
        public void Create() {
            // Rights
            this.RequireGenericAdminRights();
            CRUDCreate();
        }

       


        [RequestHandler("/api/admin/signage/sign/{publicId}/delete")]
        public void Delete(string publicId) {
            // Rights
            this.RequireGenericAdminRights();
            CRUDDelete(publicId);
          
        }

        [RequestHandler("/api/admin/signage/sign/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        protected override void EditPopulate(Sign entity) {
            base.EditPopulate(entity);
            CheckRights(entity);
        }

        private void CheckRights(Sign entity) {
            this.User.Include(nameof(User.Businesses));
            var businessIds = this.User.Businesses.Select(b => b.Id);

            if (this.IsReadWriteAdmin() == false && businessIds.Contains(entity.Business.Id) == false) {
                throw new Backend403Exception("authorization-required", $"You do not have sufficient privelages", "/admin/signage/*");
            }
        }

        protected override void CreatePopulate(Sign entity) {
            base.CreatePopulate(entity);

            CheckRights(entity);

            entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Title);
        }
        


    }
}