using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Module.Signage.Model;
using Machinata.Core.Reporting;
using Machinata.Module.Signage.Logic;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Signage.Handler.Page.Admin {

    public class AdminSignageHandler : Module.Admin.Handler.AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("signage");
        }

        #endregion


        public override void SetupForContentType() {
            base.SetupForContentType();

            // Load AccesPolicies
            if (this.User != null) {
                var forSideeffect = this.User.AllAccessPolicies;
            }
            this.Template.InsertVariable("user.show-feature", this.IsRestrictedAdmin() ? "hidden-content" : string.Empty);
        }

       
       
    }


}