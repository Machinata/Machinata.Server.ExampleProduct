using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Module.Signage.Model;
using Machinata.Core.Reporting;
using Machinata.Module.Signage.Logic;

namespace Machinata.Module.Signage.Handler.Page.Admin {

    public class AdminSignPageHandler : AdminSignageHandler {

        #region Menu Item

       
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "monitor",
                Path = "/admin/signage",
                Title = "{text.signage}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/signage")]
        public void Default() {

            var isRestrictedAdmin = this.IsRestrictedAdmin();

            // Menu items
            if (isRestrictedAdmin == false) {
                var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/signage/menu/menu.item.", this.TemplateExtension);
                this.Template.InsertTemplates("signage.menu-items", menuItems);
            }else {
                this.Template.InsertVariable("signage.menu-items", string.Empty);
            }

          
            // Listing
            var entities = this.Template.Paginate(
                this.GetSignsForUser().Where(e => e.Enabled == true), 
                this
            );

            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/signage/signs/sign/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("signage");

        }

        
        [RequestHandler("/admin/signage/signs")]
        public void ListSigns() {
            
            // Listing
            var entities = this.Template.Paginate(this.GetSignsForUser(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/signage/signs/sign/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("signs");


        }


        [RequestHandler("/admin/signage/signs/create")]
        public void Create() {
            this.RequireWriteARN();
            this.RequireGenericAdminRights();

            var entity = new Sign();
            entity.User = this.User;
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/signage/sign/create",
               onSuccess: "/admin/signage/signs/sign/{sign.public-id}"
            );

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("signs");
            this.Navigation.Add("create");
        }


        [RequestHandler("/admin/signage/signs/sign/{publicId}")]
        public void View(string publicId) {
            this.RequireWriteARN();

            // Init
            var entity = this.GetSignsForUser()
                .GetByPublicId(publicId);
            var impl = entity.GetImplementation();


            // QR Codes
            if (impl == null) {
                this.Template.InsertVariable("qrcodes","");
            } else {
                var codes = impl.GetQRCodes();
                this.Template.InsertTemplatesForObjects(
                    variableName: "qrcodes",
                    entities: codes,
                    templateName: "qrcode",
                    forEachEntity: new Action<KeyValuePair<string, string>, PageTemplate>(delegate (KeyValuePair<string, string> qrcode, PageTemplate t) {
                        t.InsertVariable("qrcode.title", qrcode.Key);
                        t.InsertVariable("qrcode.link", qrcode.Value);
                        t.InsertVariableURISafe("qrcode.title-uri-safe", qrcode.Key);
                        t.InsertVariableURISafe("qrcode.link-uri-safe", qrcode.Value);
                    })
                );
            }
                 
            // Variables
            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("signs");
            this.Navigation.Add($"sign/{publicId}", entity.Title);

        }
        
        [RequestHandler("/admin/signage/signs/sign/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = this.GetSignsForUser()
                .GetByPublicId(publicId);

            var impl = entity.GetImplementation();
            if(impl != null) impl.LoadDefaultSettingsAndMerge();

            var form = new FormBuilder(Forms.Admin.EDIT);
            form.ContentSource = "Signage/" + entity.Business.PublicId; // make sure each business has own content source namespace
            var select = new CustomFormBuilderProperty(form);
            select.Name = "SignType";
            select.FormName = "signtype";
            select.FormType = "select";
            select.FormValue = entity.SignType;
            select.FormOptions = new Dictionary<string, string>();
            foreach(var type in Signage.Logic.SignImplementation.GetImplementations()) {
                var name = type.Name.Replace("SignImplementation","");
                select.FormOptions.Add(name, name);
            }
            form.Custom(select);

           
            //var isReadWriteAdmin = this.IsReadWriteAdmin();
            //if (isReadWriteAdmin == false) {
            //    form.Exclude(nameof(Sign.User));
            //    form.Exclude(nameof(Sign.Business));
            //    entity.Business = this.User.Businesses.FirstOrDefault();
            //    entity.Business = this.User.Businesses.FirstOrDefault();
            //}

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/signage/sign/" + entity.PublicId + "/edit",
               onSuccess: "/admin/signage/signs/sign/" + entity.PublicId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("signs");
            this.Navigation.Add($"sign/{publicId}", entity.Title);
            this.Navigation.Add($"edit");
        }


        
        [RequestHandler("/admin/signage/signs/sign/{publicId}/customize")]
        public void Customize(string publicId) {
            
            // Init
            var entity = this.GetSignsForUser()
                .GetByPublicId(publicId);
            var impl = entity.GetImplementation();
            
            
            // Variables
            Template.InsertVariables("entity", entity);
            Template.InsertVariableURISafe("data", this.Params.String("url"));
            Template.InsertVariable("url", this.Params.String("url"));
            Template.InsertVariable("title", this.Params.String("title"));

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("signs");
            this.Navigation.Add($"sign/{publicId}", entity.Title);
            this.Navigation.Add($"customize-qrcode");
        }


      
    }


}