
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Signage.Model;
using Machinata.Module.Signage.Handler.Page.Admin;
using Machinata.Module.Signage.Logic;

namespace Machinata.Module.Finance.Handler {


    public class BusinessSignageAdminHandler : AdminSignageHandler {
        

        [RequestHandler("/admin/signage/businesses")]
        public void Businesses() {

            // Rights
            this.RequireGenericAdminRights();

            var entities = this.DB.ActiveBusinesses().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                "entity-list", 
                entities, 
                new FormBuilder(Forms.Admin.LISTING), "/admin/signage/businesses/business/{entity.public-id}", 
                true
            );

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("businesses");
        }


        [RequestHandler("/admin/signage/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {

            // Rights
            this.RequireGenericAdminRights();


            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Branding
            var branding = new BusinessBranding(entity);
            this.Template.InsertPropertyList("entity.branding", branding, new FormBuilder(Forms.Admin.VIEW));


            // Signs
            var entities = this.DB.Signs().Where(e => e.Business.Id == entity.Id);
            this.Template.InsertEntityList(
                "entity.signs", 
                entities, 
                new FormBuilder(Forms.Admin.LISTING), "/admin/signage/signs/sign/{entity.public-id}", 
                true
            );

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("businesses");
            this.Navigation.Add($"business/{publicId}", entity.Name);
        }
        
        
        [RequestHandler("/admin/signage/businesses/business/{publicId}/edit")]
        public void BusinessEdit(string publicId) {

            // Rights
            this.RequireGenericAdminRights();
            
            var entity = DB.Businesses().GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);

            // Branding
            var branding = new BusinessBranding(entity);
            this.Template.InsertForm(
               variableName: "form",
               entity: branding,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: $"/api/admin/signage/businesses/business/{entity.PublicId}/edit",
               onSuccess: $"/admin/signage/businesses/business/{entity.PublicId}"
            );
            

            // Navigation
            this.Navigation.Add("signage");
            this.Navigation.Add("businesses");
            this.Navigation.Add($"business/{publicId}", entity.Name);
            this.Navigation.Add("edit");
        }

    }
}
