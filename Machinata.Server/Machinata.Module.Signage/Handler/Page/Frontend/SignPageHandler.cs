using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Signage.Model;
using Machinata.Module.Signage.Logic;
 
namespace Machinata.Module.Signage.Handler.Page {
         

    public class SignPageHandler : Core.Handler.PageTemplateHandler {


        public override void SetupForContext(HttpContext context) {
            base.SetupForContext(context);

            // Auto-detect language
            this.SetLanguageFromContext();
            
        }

        [RequestHandler("/sign/{publicId}", AccessPolicy.PUBLIC_ARN)]
        public void SignView(string publicId) {

            // Make sure to catch all error messages
            try {
                // Load sign
                var sign = this.DB.Signs()
                    .Include(nameof(Sign.Business))
                    .Enabled()
                    .GetByPublicId(publicId);
                var branding = new BusinessBranding(sign.Business);
                var impl = sign.GetImplementation();
                // Change template
                this.Template.ChangeTemplate("sign." + sign.SignType.ToLower());
                this.Template.InsertTemplate("theme.head", "theme." + branding.Theme + ".head");
                if (this.Params.Bool("header", true)) {
                    this.Template.InsertTemplate("theme.header", "theme." + branding.Theme + ".header");
                } else {
                    this.Template.InsertVariable("theme.header", "");
                }
                if (this.Params.Bool("footer", true)) {
                    this.Template.InsertTemplate("theme.footer", "theme." + branding.Theme + ".footer");
                } else {
                    this.Template.InsertVariable("theme.footer", "");
                }
                // Insert variables
                this.Template.InsertVariables("branding", branding);
                this.Template.InsertVariables("business", sign.Business);
                this.Template.InsertVariables("sign", sign);
                if (impl != null) impl.InsertFrontendVariables(this, this.Template);

            }catch(Exception e) {

                if(Config.SignageSendErrorMessagesViaEmail) {
                    Core.EmailLogger.SendMessageToAdminEmail("Sign Error", "Machinata.Module.Signage", this.Context, this, e);
                }
                
                throw e;
            }
        }
        
     
    }
}