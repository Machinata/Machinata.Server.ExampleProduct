using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Signage.Model {
    
    [Serializable()]
    public partial class BusinessBranding : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        #region Constants //////////////////////////////////////////////////////////////////////
        
        public const string BUSINESS_SETTING_KEY_SIGNAGE_LOGO = "SignageLogo";
        public const string BUSINESS_SETTING_KEY_SIGNAGE_THEME = "SignageTheme";
        public const string BUSINESS_SETTING_KEY_SIGNAGE_CUSTOMURL = "SignageCustomURL";

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public BusinessBranding(Business business) {
            this.Business = business;
            this.Logo = this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_LOGO] as string;
            this.Theme = this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_THEME] as string;
            this.CustomURL = this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_CUSTOMURL] as string;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string Theme { get; set; } 
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string CustomURL { get; set; } 
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////
        
        public Business Business { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);
            
            this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_LOGO] = this.Logo;
            this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_THEME] = this.Theme;
            this.Business.Settings[BUSINESS_SETTING_KEY_SIGNAGE_CUSTOMURL] = this.CustomURL;
        }

        #endregion

    }
    

    
}
