using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Signage.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("SignageSign")]
    public partial class Sign : ModelObject, IEnabledModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Sign() {
            this.SignSettings = new Properties();
            if (string.IsNullOrEmpty(this.GUID)) {
                this.GUID = Guid.NewGuid().ToString();
            }
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Title { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Enabled { get; set; } 

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Column]
        [Index(IsUnique = true)]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string GUID { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [MaxLength(256)]
        public string SignType { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Properties SignSettings { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [ContentType(ContentNode.NODE_TYPE_HTML+","+ContentNode.NODE_TYPE_IMAGE)]
        public ContentNode Content { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Business Business { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public User User { get; set; }
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        public string PublicLink {
            get {
                var branding = new BusinessBranding(this.Business);
                if (!string.IsNullOrEmpty(branding.CustomURL)) {
                    return branding.CustomURL + "/sign/" + PublicId;
                } else {
                    return Core.Config.PublicURL + "/sign/" + PublicId;
                }
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        public string ScreenLink {
            get {
                return this.PublicLink + "?fullscreen=true&auto-reload=30000";
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        public Logic.SignImplementation GetImplementation() {
            if (this.SignType == null || this.SignType == "") return null;
            var type = Logic.SignImplementation.GetImplementations().SingleOrDefault(e => e.Name == this.SignType + "SignImplementation");
            var obj = Core.Reflection.Types.CreateInstance<Logic.SignImplementation>(type, this);
            return obj;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        public bool Published {
            get {
                return this.Enabled;
            }
        }

        public override CardBuilder VisualCard() {
            this.Include(nameof(Sign.Business));
            var card = new Core.Cards.CardBuilder(this)
                  .Title(this.Title)
                  .Subtitle(this.Business?.Name)
                  .Icon("directions");
            return card;
        }

        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSignExtenions {

        public static DbSet<Sign> Signs(this Core.Model.ModelContext context) {
            return context.Set<Sign>();
        }
    }

    #endregion
}
