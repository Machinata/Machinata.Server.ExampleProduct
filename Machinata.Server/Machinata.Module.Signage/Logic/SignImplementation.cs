using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public abstract class SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        private Model.Sign _sign;

        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public SignImplementation(Model.Sign sign) {
            _sign = sign;
        }

        #endregion

        public Model.Sign Sign {
            get {
                return _sign;
            }
        }
        
        public abstract Dictionary<string, string> GetDefaultSettings();

        public abstract void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template);
        
        public virtual Dictionary<string, string> GetQRCodes() {
            var ret = new Dictionary<string, string>();
            ret.Add("Web Applet", this.Sign.PublicLink);
            return ret;
        }

        public virtual string GetNotes() {
            return "";
        }

        public virtual void LoadDefaultSettingsAndMerge() {
            // Make sure all defaults are added
            var defaults = GetDefaultSettings();
            foreach(var keyval in defaults) {
                if(!this.Sign.SignSettings.Keys.Contains(keyval.Key)) {
                    this.Sign.SignSettings[keyval.Key] = keyval.Value;
                }
            }
            // Remove any not of this type...
            var toRemove = new List<string>();
            foreach(var key in this.Sign.SignSettings.Keys) {
                if(!defaults.Keys.Contains(key)) {
                    toRemove.Add(key);
                }
            }
            foreach (var key in toRemove) this.Sign.SignSettings.Delete(key);
        }


        public static List<Type> GetImplementations() {
            return Core.Reflection.Types.GetMachinataTypes(typeof(SignImplementation))
                .Where(e => e.IsAbstract == false)
                .ToList();
        }
        
    }
    

    
}
