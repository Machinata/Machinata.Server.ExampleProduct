using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class WeblinkSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public WeblinkSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion
        

        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {
            template.InsertVariableUnsafe("sign.weblink", this.Sign.SignSettings["WebLink"] as string);
        }
        

        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("WebLink", "");
            return ret;
        }
        
    }
    

    
}
