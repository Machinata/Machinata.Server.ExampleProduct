using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class UberSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public UberSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion

        public string GetDeepLink() {
            // See https://developer.uber.com/docs/riders/ride-requests/tutorials/deep-links/introduction#ride-requests
            // Example: https://m.uber.com/ul/?client_id=<CLIENT_ID>&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383
            var DEEPLINK_URL = "https://m.uber.com/ul/?action=setPickup&pickup[latitude]={latitude}&pickup[longitude]={longitude}&pickup[nickname]={nickname}&pickup[formatted_address]={address}";
            var url = DEEPLINK_URL;
            url = url.Replace("{latitude}", Sign.SignSettings["PickupLatitude"]?.ToString());
            url = url.Replace("{longitude}", Sign.SignSettings["PickupLongitude"]?.ToString());
            url = url.Replace("{nickname}", Uri.EscapeUriString(Sign.SignSettings["PickupNickname"]?.ToString()));
            url = url.Replace("{address}", Uri.EscapeUriString(Sign.SignSettings["PickupAddress"]?.ToString()));
            return url;
        }

        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {
            template.InsertVariableUnsafe("sign.deeplink", GetDeepLink());
        }
        

        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("PickupLatitude", "");
            ret.Add("PickupLongitude", "");
            ret.Add("PickupNickname", "");
            ret.Add("PickupAddress", "");
            return ret;
        }
        
    }
    

    
}
