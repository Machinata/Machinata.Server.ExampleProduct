using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Module.Crawler.Model;
using Machinata.Module.Crawler;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class OTDTimetableSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OTDTimetableSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion

        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {

            var connectionsToShow = handler.Params.Int("connections", int.Parse(this.Sign.SignSettings["ConnectionsToShow"].ToString()));

            // A timeTableMeta can consist of multiple didoks/StopPointRefs in coma separated manner.
            // If mulitple it will make a request for each one and take the number of 'connectionsToShow' of the closest times
            Timetable timeTableMeta = null;
            var crawler = new OpenTransportDataTimetableWebCrawler();
            try {

                string stopPointRefs = null;
                if (this.Sign.SignSettings["StopPointRefs"] != null) {
                    stopPointRefs = this.Sign.SignSettings["StopPointRefs"].ToString();
                }

                var stopPointRefSplits = stopPointRefs.Split(',');
                var allConnections = new List<TimetableConnection>();
                int stopPointRefOrder = 0;
                Exception stopPointException = null;
                foreach (var stopPointRefSplit in stopPointRefSplits) {

                    try
                    {
                        var timetable = crawler.GetTimetableForStation(
                            stopPointRefSplit,
                            DateTime.Now,
                            connectionsToShow
                        );

                        // Keep order of the didoks
                        foreach (var connection in timetable.Connections)
                        {
                            connection.StopPointRefOrder = stopPointRefOrder;
                        }
                        stopPointRefOrder++;

                        allConnections.AddRange(timetable.Connections);

                        // Take the first one in list
                        if (timeTableMeta == null)
                        {
                            timeTableMeta = timetable;
                        }
                    }
                    catch(Exception sPException)
                    {
                        // Dont fail all if one didok/stopref has an exception
                        // E.g Opfikon (8590742) at night: STOPEVENT_LOCATIONUNSERVED
                        _logger.Log(NLog.LogLevel.Error, sPException, $"OTD Error for stop point/didok'{stopPointRefSplit}': " + sPException.Message);
                        stopPointException = sPException;
                    }
                }

                // No results from OTD, re-throw the last exception
                if (allConnections.Count == 0 && stopPointException != null)
                {
                    throw stopPointException;
                }

                var connectionsMeta = allConnections.OrderBy(t => t.LocationDateTime).ThenBy(t => t.StopPointRefOrder).Take(connectionsToShow);
                timeTableMeta.Connections = connectionsMeta.ToList();
                template.InsertVariable("copyright", crawler.GetCopyright());
                template.InsertVariables("timetable", timeTableMeta);
                template.InsertTemplatesForObjects(
                    variableName: "connections",
                    entities: timeTableMeta.Connections.OrderBy(e => e.LocationDateTime),
                    templateName: "sign.otdtimetable.connection",
                    forEachEntity: new Action<TimetableConnection, PageTemplate>(delegate (TimetableConnection connection, PageTemplate t) {
                        try {
                            t.InsertVariables("connection", connection);
                        } catch (Exception e) {
                            throw new BackendException("connection-error", "Could not parse connection data: " + connection.ToString(), e);
                        }
                    })
                );
              
            } catch (Exception e) {
                // Silent for now
                //throw new BackendException("crawler-error", "Could not crawl OTD timetable: " + e.Message, e);
                template.InsertVariable("copyright",string.Empty);
                template.InsertVariable("timetable", string.Empty);
                template.InsertVariable("connections", "No connection to SBB Open Transport Data");
                _logger.Log(NLog.LogLevel.Error, e, "Could not process OTD timetable: " + e.Message);
            } finally {
                template.InsertVariable("station-distance-walk", "{text.station-distance-walk}");
            }
        }

        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("StationName", "");
            ret.Add("StopPointRefs", "");
            ret.Add("StationDistance", "");
            ret.Add("ConnectionsToShow", "8");
            return ret;
        }
        
        
    }
    

    
}
