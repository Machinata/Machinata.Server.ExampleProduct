using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Module.Crawler.Model;
using Machinata.Module.Crawler;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class SplitScreenSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public SplitScreenSignImplementation(Model.Sign sign) : base(sign) {
           
        }

        #endregion
        
        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {
            var layout = Sign.SignSettings["Layout"]?.ToString();
            var screenA = Sign.SignSettings["ScreenA"]?.ToString();
            var screenB = Sign.SignSettings["ScreenB"]?.ToString();
            var screenARatio = Sign.SignSettings["ScreenAAspectRatio"]?.ToString();

            template.InsertVariable("screen-a-url", screenA);
            template.InsertVariable("screen-b-url", screenB);
            template.InsertVariable("layout", layout);
            template.InsertVariable("screena-a-aspect-ratio", screenARatio);
        }
                
        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("Layout", "DIN-476");
            ret.Add("ScreenA", "");
            ret.Add("ScreenAAspectRatio", "0.5625");
            ret.Add("ScreenB", "");
            return ret;
        }
        
        
    }
    

    
}
