using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Signage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Signage.Logic {
    public static class SignLogic {

        /// <summary>
        /// Gets the signs for an admin user.
        /// 
        /// </summary>

        /// <returns>If only admin/signage arn is assigned , then only the signs of the corresponding business</returns>
        public static IQueryable<Sign> GetSignsForUser(this Machinata.Core.Handler.Handler handler) {

            var arn = "/admin/signage/*";
            var signs = handler.DB.Signs()
                .Include(nameof(Sign.Business))
                .Include(nameof(Sign.User))
                .Include(nameof(Sign.Content));

            handler.User?.Include(nameof(User.Businesses));
            bool isGenericAdmin = IsReadWriteAdmin(handler);

            if (isGenericAdmin == false) {

                // Check business
                var businessIds = handler.User.Businesses.Select(b => b.Id);
                if (handler.User.Businesses == null) {
                    throw new Backend403Exception("authorization-required", $"You do not have sufficient privelages to access this page (URL={handler.Uri})", arn);
                }
                return signs.Where(s => businessIds.Contains(s.Business.Id));
            }

            return signs;

        }


        /// <summary>
        /// Determines whether user is Member of the "Read/Write Administrators"
        /// </summary>
        /// <returns></returns>
        public static bool IsReadWriteAdmin(this Core.Handler.Handler handler) {
            if (handler.User == null) {
                return false;
            }
            return handler.User.AccessGroups.ToList().Any(ag => ag.Name == "Read/Write Administrators");
        }


        /// <summary>
        /// Determines whether the user has only a specific admin arn and is not in the "Read/Write Administrators" group
        /// </summary>
        /// <returns></returns>
        public static bool IsRestrictedAdmin(this Core.Handler.Handler handler) {
            return handler.IsReadWriteAdmin() == false && handler.User?.IsAdminUser == true;
        }


        public static void RequireGenericAdminRights(this Core.Handler.Handler handler) {
            if (handler.IsReadWriteAdmin() == false) {
                throw new Backend403Exception("authorization-required", $"You do not have sufficient privelages to access this page (URL={handler.Uri})", "/admin/signage");
            }
        }



    }
}
