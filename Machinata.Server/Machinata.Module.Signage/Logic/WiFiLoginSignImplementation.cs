using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class WiFiLoginSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public WiFiLoginSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion

        public string GetDirectLoginLink() {
            var WIFI_LOGIN_URL = "WIFI:S:{ssid};T:{encryption};P:{password};;";
            var loginURL = WIFI_LOGIN_URL;
            loginURL = loginURL.Replace("{ssid}", Sign.SignSettings["WifiName"]?.ToString());
            loginURL = loginURL.Replace("{encryption}", Sign.SignSettings["WifiEncryption"]?.ToString());
            loginURL = loginURL.Replace("{password}", Sign.SignSettings["WifiPassword"]?.ToString());
            return loginURL;
        }

        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {
            
        }

        public override string GetNotes() {
            return "Please enter either 'WPA' or 'WEP' for encryption, or leave it blank if the WiFi has no encryption.";
        }

        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("WifiName", "");
            ret.Add("WifiPassword", "");
            ret.Add("WifiEncryption", "WPA");
            return ret;
        }

        public override Dictionary<string, string> GetQRCodes() {
            var ret = base.GetQRCodes();
            ret.Add("Direct Login", GetDirectLoginLink());
            return ret;
        }
        
    }
    

    
}
