using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Module.Crawler.Model;
using Machinata.Module.Crawler;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class OutlookCalendarSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OutlookCalendarSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion
        
        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {
            // Get the day
            var date = handler.Params.DateTime("date", DateTime.Today).ToUniversalTime();
            var today = DateTime.Now.Date.ToUniversalTime();

            var crawler = new OutlookCalendarWebCrawler();
            var schedule = crawler.GetCalendarEntries(
                this.Sign.SignSettings["CalendarURL"].ToString(),
                Core.Util.Time.ConvertToDefaultTimezone(date)
            );

            // Get min/max hour
            var minHour = 8;
            var maxHour = 20;
            if (schedule.Items != null && schedule.Items.Count > 0) {
                var minHourSchedule = Core.Util.Time.ConvertToDefaultTimezone(schedule.Items.Min(e => e.Start)).Hour;
                var maxHourSchedule = Core.Util.Time.ConvertToDefaultTimezone(schedule.Items.Max(e => e.End)).Hour;
                minHour = Math.Min(minHour, minHourSchedule);
                maxHour = Math.Max(maxHour, maxHourSchedule);
            }
            var slots = Schedule.CreateHourlySchedule(date, minHour, maxHour);
            
            template.InsertVariable("sign.is-today", date.Date == today.Date);
            template.InsertVariable("sign.day", Core.Util.Time.ConvertToDefaultTimezone(date).DayOfWeek.ToString().ToLower());
            template.InsertVariable("sign.date", Core.Util.Time.ConvertToDefaultTimezone(date).ToString("dd.MM.yyyy"));
            template.InsertVariable("sign.prev-date", Core.Util.Time.ConvertToDefaultTimezone(date).AddDays(-1).ToString("yyyy.MM.dd"));
            template.InsertVariable("sign.next-date", Core.Util.Time.ConvertToDefaultTimezone(date).AddDays(1).ToString("yyyy.MM.dd"));
            template.InsertVariables("schedule", schedule);
            template.InsertTemplatesForObjects(
                variableName: "slots",
                entities: slots.Items,
                templateName: "sign.outlookcalendar.slot",
                forEachEntity: new Action<ScheduleItem, PageTemplate>(delegate (ScheduleItem item, PageTemplate t) {
                    t.InsertVariables("slot", item);
                    t.InsertVariable("slot.is-today", item.Start.Date == DateTime.Now.Date ? "is-today" : "");
                })
            );
            template.InsertTemplatesForObjects(
                variableName: "items",
                entities: schedule.Items,
                templateName: "sign.outlookcalendar.item",
                forEachEntity: new Action<ScheduleItem, PageTemplate>(delegate (ScheduleItem item, PageTemplate t) {
                    t.InsertVariables("item", item);
                    t.InsertVariable("item.subject-translated", item.Subject?.Replace("Busy","{text.calendar-busy}"));
                    t.InsertVariable("item.is-today", item.Start.Date == DateTime.Now.Date ? "is-today" : "");
                    t.InsertVariable("item.timerange", $"{Core.Util.Time.ConvertToDefaultTimezone(item.Start).ToShortTimeString()} - {Core.Util.Time.ConvertToDefaultTimezone(item.End).ToShortTimeString()}");
                    t.InsertVariable("item.start-time", Core.Util.Time.ConvertToDefaultTimezone(item.Start).ToShortTimeString());
                    
                })
            );
        }
                
        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("CalendarURL", "");
            ret.Add("CalendarTitle", "");
            return ret;
        }
        
        
    }
    

    
}
