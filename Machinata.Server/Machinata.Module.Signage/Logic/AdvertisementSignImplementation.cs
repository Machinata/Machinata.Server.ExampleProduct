using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Module.Crawler.Model;
using Machinata.Module.Crawler;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class AdvertisementSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public AdvertisementSignImplementation(Model.Sign sign) : base(sign) {
           
        }

        #endregion
        
        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {

            // Load Content
            this.Sign.Include(nameof(Sign.Content));
            this.Sign.Content?.IncludeContent();
            var translation = this.Sign.Content?.TranslationForLanguage(template.Language);

            if (translation == null) {
                template.InsertVariable("content", string.Empty);
                return;
            }

            var supportedNodes = translation.Children.Where(n => n.NodeType == ContentNode.NODE_TYPE_IMAGE || n.NodeType == ContentNode.NODE_TYPE_HTML);

            var templates = new List<PageTemplate>();

            foreach (var node in supportedNodes) {

                var contentTemplate = template.LoadTemplate("sign.advertisement." + node.NodeType);
                if (node.NodeType == ContentNode.NODE_TYPE_IMAGE) {
                    contentTemplate.InsertVariable("image.src", node.Value);

                    if (string.IsNullOrEmpty(this.Sign.SignSettings["ImagePixelWidth"]?.ToString())== false){
                        contentTemplate.InsertVariable("image.params", "?size=" + this.Sign.SignSettings["ImagePixelWidth"]?.ToString());
                    } else {
                        contentTemplate.InsertVariable("image.params", "");
                    }

                }
                else if (node.NodeType == ContentNode.NODE_TYPE_HTML) {
                    contentTemplate.InsertVariableUnsafe("content", node.Value);
                }

                templates.Add(contentTemplate);

            }


            template.InsertTemplates("content", templates);
       

            //// Images & texts
            //{
               
            //    template.InsertTemplates(
            //        variableName: "images",
            //        entities: images != null ? images : new List<ContentNode>(),
            //        templateName: "sign.advertisement.image",
            //        forEachEntity: new Action<ContentNode, PageTemplate>(delegate (ContentNode n, PageTemplate t) {
            //            t.InsertVariable("image.src", n.Value);
            //        })
            //    );
            //}

            //// Text
            //{
            //    var texts = translation?.ChildrenForType(ContentNode.NODE_TYPE_HTML);
            //    template.InsertTemplates(
            //        variableName: "texts",
            //        entities: texts != null ? texts : new List<ContentNode>(),
            //        templateName: "sign.advertisement.text",
            //        forEachEntity: new Action<ContentNode, PageTemplate>(delegate (ContentNode n, PageTemplate t) {
            //            t.InsertVariableUnsafe("content",n.Value);
            //        })
            //    );
            //}

            // TODO future: support different types
        }
                
        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("RotationSeconds", "30");
            ret.Add("ImagePixelWidth", "");
            return ret;
        }
        
         
    }
    

    
}
