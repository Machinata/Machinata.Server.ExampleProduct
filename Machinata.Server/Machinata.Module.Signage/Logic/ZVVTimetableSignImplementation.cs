using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Module.Crawler.Model;
using Machinata.Module.Crawler;
using Machinata.Core.Templates;

namespace Machinata.Module.Signage.Logic {
    
    public class ZVVTimetableSignImplementation : SignImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public ZVVTimetableSignImplementation(Model.Sign sign) : base(sign) {
            
        }

        #endregion
        
        public override void InsertFrontendVariables(Core.Handler.Handler handler, PageTemplate template) {

            var connectoinToShow = handler.Params.Int("connections", int.Parse(this.Sign.SignSettings["ConnectionsToShow"].ToString()));

            Timetable timetable = null;
            var crawler = new ZVVTimetableWebCrawler();
            try {
                string stationS0ID = null;
                if(this.Sign.SignSettings["StationS0ID"] != null) {
                    stationS0ID = this.Sign.SignSettings["StationS0ID"].ToString();
                }
                timetable = crawler.GetTimetableForStation(
                    this.Sign.SignSettings["StationName"].ToString(),
                    stationS0ID,
                    DateTime.Now,
                    connectoinToShow
                );
            } catch(Exception e) {
                throw new BackendException("crawler-error", "Could not crawl ZVV timetable: " + e.Message, e);
            }
            template.InsertVariable("copyright", crawler.GetCopyright());
            template.InsertVariables("timetable", timetable);
            template.InsertTemplatesForObjects(
                variableName: "connections",
                entities: timetable.Connections.OrderBy(e => e.LocationDate).ThenBy(e => e.LocationTime),
                templateName: "sign.zvvtimetable.connection",
                forEachEntity: new Action<TimetableConnection, PageTemplate>(delegate (TimetableConnection connection, PageTemplate t) {
                    try {
                        t.InsertVariables("connection", connection);
                    } catch(Exception e) {
                        throw new BackendException("connection-error", "Could not parse connection data: " + connection.ToString(), e);
                    }
                })
            );
            template.InsertVariable("station-distance-walk", "{text.station-distance-walk}");
        }
                
        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            ret.Add("StationName", "");
            ret.Add("StationS0ID", "");
            ret.Add("StationDistance", "");
            ret.Add("ConnectionsToShow", "8");
            return ret;
        }
        
        
    }
    

    
}
