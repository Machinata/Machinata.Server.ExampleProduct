
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Guides.Extensions;

namespace Machinata.Module.Finance.Handler {


    public class GuidesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("guides");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "document",
                Path = "/admin/guides",
                Title = "{text.guides}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/guides")]
        public void List() {
            this.Template.InsertGuides(this.DB, "guides");
     
            // Navigation
            this.AddNavigationForPath("/");

        }

      
        [RequestHandler("/admin/guides/guide/{path}")]
        public void Guid(string path) {

            // Insert Guid
            var node = this.Template.InsertGuide(this.DB, path);
            
            var navPath = node.Path.Replace(Module.Guides.Config.GuidesCMSPath, string.Empty);

            // Navigation
            this.AddNavigationForPath(navPath);
        }


        public void AddNavigationForPath(string path) {
            this.Template.InsertVariable("node.path", path);
            this.Navigation.Add("guides", "{text.guides}");
            if (path != null && path != ContentNode.NODE_PATH_SEP) {
                path = path.TrimStart(ContentNode.NODE_PATH_SEP[0]);
                var segs = path.Split(ContentNode.NODE_PATH_SEP[0]);
                for (int i = 0; i < segs.Length; i++) {
                    var seg = segs[i];
                    this.Navigation.Add((i == 0 ? "guide/" : "") + seg, seg);
                }
            }
        }

    }
}
