using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machinata.Module.Shop.Model;
using System.Net;
using Machinata.Core.Util;
using System.Xml.Linq;

using Machinata.Core.Model;
using System.Text.RegularExpressions;

//Response example for 212.119 on  2.6.2020 -> is Declaration_Swiss faulty?
//<? xml version="1.0" encoding="utf-8"?>
//<ws_GetRecipeForDisplay>
//  <Recipe Code = "33605" Number="212.119">
//    <Main>
//      <Name>Chicken Salad Niçoise</Name>
//      <SubName />
//      <Remark />
//      <Description />
//      <Yield1>1.00</Yield1>
//      <Yield1_Unit>Portionen</Yield1_Unit>
//      <Yield2>0.00</Yield2>
//      <Yield2_Unit />
//      <PictureDefault />
//    </Main>
//    <Video>
//      <URL />
//      <Caption />
//    </Video>
//    <Ingredients>
//      <Ingredient>
//        <Sequence>1</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>1211</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Gurken Würfel 10mm</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>2</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>1716804</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Bohnen geschnitten 2x2.5kg</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>3</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>70020</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Artischocken Herzen 2x2.5kg TK</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>4</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>5641</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Oliven schwarz entst. 1kg Btl</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>5</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>3052</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Zwiebeln Würfel 10mm</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>6</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>5415</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Kartoffeln Würfel 10mm</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>58.2</Quantity_Net>
//        <Quantity_Gross>58.2</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>7</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber />
//        <Complement />
//        <Prefix />
//        <Name>Peperoni grün Würfel 10 mm</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>23.3</Quantity_Net>
//        <Quantity_Gross>23.3</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>8</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>369</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Thymian kg</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>1.2</Quantity_Net>
//        <Quantity_Gross>1.2</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>9</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>663</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Majoran kg</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>1.2</Quantity_Net>
//        <Quantity_Gross>1.2</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>10</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>191</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Basilikum Kg</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>1.2</Quantity_Net>
//        <Quantity_Gross>1.2</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>11</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>Art.Nr. 511092</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Kapern</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>1289</BrandCode>
//        <BrandName>Economy</BrandName>
//        <Quantity_Net>11.2</Quantity_Net>
//        <Quantity_Gross>11.2</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>12</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>30729</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Poulet Brust ohne Haut assortiert CH</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>70.85</Quantity_Net>
//        <Quantity_Gross>70.85</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>13</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber />
//        <Complement />
//        <Prefix />
//        <Name>Olivenöl</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric>Bibox</Unit_Metric>
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>10</Quantity_Net>
//        <Quantity_Gross>10</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>14</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>590107</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Quality Sonnenblumenöl</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>20</Quantity_Net>
//        <Quantity_Gross>20</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>15</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>6220255</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Weisser Balsamico</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>10</Quantity_Net>
//        <Quantity_Gross>10</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>16</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>560290</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>JuraSel Speisesalz mit Jod</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>0.25</Quantity_Net>
//        <Quantity_Gross>0.25</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//      <Ingredient>
//        <Sequence>17</Sequence>
//        <IngredientType>Merchandise</IngredientType>
//        <IngredientNumber>512050</IngredientNumber>
//        <Complement />
//        <Prefix />
//        <Name>Quality Pfeffer weiss ge.2,5kg</Name>
//        <AlternativeIngredient />
//        <Preparation />
//        <Quantity_Imperial />
//        <Unit_Imperial />
//        <Quantity_Metric>0.00</Quantity_Metric>
//        <Unit_Metric />
//        <BrandCode>0</BrandCode>
//        <BrandName>- Not Defined</BrandName>
//        <Quantity_Net>0.1</Quantity_Net>
//        <Quantity_Gross>0.1</Quantity_Gross>
//        <Unit>g</Unit>
//      </Ingredient>
//    </Ingredients>
//    <Procedure>
//      <Notes />
//      <AdditionalNotes />
//      <Step>
//        <Sequence>1</Sequence>
//        <Note />
//      </Step>
//    </Procedure>
//    <Times />
//    <Nutrition>
//      <PortionSize />
//      <NutritionBasis />
//      <Nutrients>
//        <Nutrient>
//          <DisplayPosition>1</DisplayPosition>
//          <NutKey>Energie</NutKey>
//          <DisplayName>Energie</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>2038.80</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>629.26</ValuePer100g>
//          <Unit>kJ</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>1</DisplayPosition>
//          <NutKey>Energie</NutKey>
//          <DisplayName>Energie</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>487.29</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>150.40</ValuePer100g>
//          <Unit>kcal</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>2</DisplayPosition>
//          <NutKey>Eiweiss</NutKey>
//          <DisplayName>Eiweiss</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>19.65</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>6.06</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>3</DisplayPosition>
//          <NutKey>Fett</NutKey>
//          <DisplayName>Fett</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>19.70</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>6.08</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>4</DisplayPosition>
//          <NutKey>Kohlenhydrate</NutKey>
//          <DisplayName>Kohlenhydrate</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>15.98</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>4.93</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>5</DisplayPosition>
//          <NutKey>Gesättigte Fettsäuren </NutKey>
//          <DisplayName>Gesättigte Fettsäuren </DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>8.24</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>2.54</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>6</DisplayPosition>
//          <NutKey>Zucker</NutKey>
//          <DisplayName>Zucker</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>4.54</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>1.40</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.00</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//        <Nutrient>
//          <DisplayPosition>7</DisplayPosition>
//          <NutKey>Natrium</NutKey>
//          <DisplayName>Salz</DisplayName>
//          <ValueImposed>0.00</ValueImposed>
//          <Value>1.70</Value>
//          <ValueImposedPer100g>0.00</ValueImposedPer100g>
//          <ValuePer100g>0.53</ValuePer100g>
//          <Unit>g</Unit>
//          <Format>#,##0.0</Format>
//          <GDA>0.00</GDA>
//        </Nutrient>
//      </Nutrients>
//    </Nutrition>
//    <Allergens>
//      <Allergen>
//        <Code>12</Code>
//        <Name>Schwefeldioxid und Sulphite</Name>
//      </Allergen>
//    </Allergens>
//    <Declaration>
//      <DeclarationName>Chicken Salad Niçoise</DeclarationName>
//      <SpecificDetermination>Chicken Salad Niçoise</SpecificDetermination>
//      <Number />
//      <Barcode>7640191950208</Barcode>
//      <DaysConsumption>4</DaysConsumption>
//      <DaysSales>3</DaysSales>
//      <Price>0</Price>
//      <PriceForUnitWEightVolume>0</PriceForUnitWEightVolume>
//      <WeightVolume>325</WeightVolume>
//      <Unit>g</Unit>
//      <Calculated>0</Calculated>
//      <AdditionalInformation1 />
//      <AdditionalInformation2 />
//      <AdditionalInformation3 />
//      <Certification />
//      <CountryOfProduction />
//      <PackagingMethod />
//      <StorageCondition>Bei max. 5°C aufbewahren</StorageCondition>
//      <Declaration_EU>Poulet CH 20%, Kartoffeln 18%, Gurke 7%, grüne Bohnen 7%, Oliven 7%, Zwiebeln 7%, Peperoni 7%, Artischocken 7%, Sonnenblumenöl, Kapern, Olivenöl, Balsamico Bianco (Weinessig, Traubenmostkonzentrat, Antioxidationsmittel: SCHWEFELdioxid), Thymian, Majoran, Basilikum, Salz, Pfeffer</Declaration_EU>
//      <Declaration_Swiss>Poulet frisch[Pouletfleisch], Kartoffeln Würfel 10mm, Oliven schwarz entst. 1kg Btl, Peperoni grün Würfel 10 mm, Zwiebeln Würfel 10mm, Gurken Würfel 10mm, Bohnen geschnitten 2x2.5kg, Artischocken Herzen 2x2.5kg TK, Quality Sonnenblumenöl, Kapern[Kapern, Wasser, Branntweinessig, Kochsalz] , Olivenöl, Weisser Balsamico[Weinessig, Antioxidationsmitte E224(Schwefeldioxid)], Basilikum Kg, Majoran kg, Thymian kg, JuraSel Speisesalz mit Jod[Salz, Jod], Quality Pfeffer weiss ge.2,5kg</Declaration_Swiss>
//    </Declaration>
//  </Recipe>
//</ws_GetRecipeForDisplay>


namespace Machinata.Module.Shop.Services.Implementation {
    public class CalcMenuProductUpdater : ProductUpdater {

        public const string NutritionKey = "gram100";
        public const string GET_RECIPE_DETAILS_NAME = "GetRecipeDetails";

        public override StringBuilder Update(Product product, ProductConfiguration productConfiguration) {

            var log = new StringBuilder();

            try {
                // Call CalcMenu Webservice with defaut config
                var configuration = product.GetDefaultConfiguration();
                this.GetProductFromCalcMenu(log, product, productConfiguration);
            }
            catch (Exception e) {
                log.AppendLine("Error while updating the product");
                log.AppendLine(e.Message);
                log.AppendLine(e.StackTrace);
            }
            return log;


        }

        private void GetProductFromCalcMenu(StringBuilder log, Product product, ProductConfiguration pc) {
            var id = Config.ShopProductUpaters.CalcMenu.ServiceAccount;
            var pswd = Config.ShopProductUpaters.CalcMenu.ServicePassword;
            var kiosk = Config.ShopProductUpaters.CalcMenu.DefaultKiosk;
            var db = pc.Context;

            var codeNutrientSet = 0; // Default
            var language = "de";
            var languageForCMS = language == Core.Config.LocalizationDefaultLanguage ? "default" : language;

            var categoryGross = db.Categories().FirstOrDefault(c => c.CategoryType == Category.CategoryTypes.Main && c.Name == "Gross");
            if (categoryGross == null) {
                categoryGross = Category.FindOrCreateMainCategory(db, "Gross");
            }

            var categoryKlein = db.Categories().FirstOrDefault(c => c.CategoryType == Category.CategoryTypes.Main && c.Name == "Klein");
            if (categoryKlein == null) {
                categoryKlein = Category.FindOrCreateMainCategory(db, "Klein");
            }

            // DE = 10
            var codeTranslation = new string[] { "10" }; // TODO: only german at the moment
            var recipeNumber = pc.SKU;

            var parameters = new Dictionary<string, string>();
            parameters["ID"] = id;
            parameters["PSWD"] = pswd;
            parameters["Kiosk"] = kiosk;
            parameters["CodeTranslation"] = codeTranslation.First();
            parameters["RecipeNumber"] = pc.SKU;
            parameters["CodeNutrientSet"] = codeNutrientSet.ToString();

            log.AppendLine("------------  Updating Product  ----------------");

            var data = Call(GET_RECIPE_DETAILS_NAME, parameters);

            var details = XElement.Parse(data);

            if (Core.Config.IsTestEnvironment) {
                //details.Save("c://test//" + pc.SKU +$"_{Guid.NewGuid().ToString()}.xml");
            }

            var recipe = details.Element("Recipe");
            var nameElement = recipe.Element("Main").Element("Name");

            // Portions
            log.LogSection("Portions");
            var oldPortions = product.Details[Shop.Model.PackagingDesign.DETAILS_KEY_PORTIONS]?.ToString();
            var newPortions = recipe.Element("Main")?.Element("Yield1")?.Value;
            if (oldPortions != newPortions) {
                log.LogChange(oldPortions, newPortions);
                product.Details[Shop.Model.PackagingDesign.DETAILS_KEY_PORTIONS] = newPortions;
            } else {
                log.LogNoChanges();
            }

            // Declarations
            var declaration = recipe.Element("Declaration");
            UpdateDetails(log, product, declaration, Shop.Model.PackagingDesign.DETAILS_KEY_CONSUME_BY_DAYS, "DaysConsumption");
            UpdateDetails(log, product, declaration, Shop.Model.PackagingDesign.DETAILS_KEY_SELL_BY_DAYS, "DaysSales");
            UpdateWeight(log, product, declaration, Shop.Model.PackagingDesign.DETAILS_KEY_WEIGHT, "WeightVolume", "Unit");

            // Customer Price-> Ignore: thomas.staub email 8.7.2019 15:14
            // UpdatePrice(log, pc, declaration);

            // Nutritions
            var nutritions = recipe.Element("Nutrition")?.Element("Nutrients");
            UpdateNutritions(log, product, nutritions);


            // Declarations/Ingredients
            UpdateDeclarations(log, product, languageForCMS, declaration);

            // Store At
            {
                // StoreAt: we get whole string from calcmenue
                log.LogSection("StoreAt");
                var oldStoreAt = product.Details[Shop.Model.PackagingDesign.DETAILS_KEY_STORE_AT]?.ToString();
                var newStoreAt = declaration.Element("StorageCondition")?.Value;
                if (oldStoreAt != newStoreAt) {
                    log.LogChange(oldStoreAt, newStoreAt);
                    product.Details[Shop.Model.PackagingDesign.DETAILS_KEY_STORE_AT] = newStoreAt;
                } else {
                    log.LogNoChanges();
                }
            }


            // Name
            {
                log.LogSection("Name");
                var oldName = product.Name;
                var newName = declaration.Element("DeclarationName")?.Value;
                if (oldName != newName) {
                    log.LogChange(oldName, newName);
                    product.Name = newName;
                } else {
                    log.LogNoChanges();
                }

            }

         

            // Title
            {
                log.LogSection("Title");
                var oldTitle = pc.Title;
                var newTitle = declaration.Element("DeclarationName")?.Value;
                if (oldTitle != newTitle) {
                    log.LogChange(oldTitle, newTitle);
                    pc.Title = newTitle;
                } else {
                    log.LogNoChanges();
                }
            }

            // Barcode
            {
                log.LogSection("Barcode");
                var old = pc.BarCode;
                var @new = declaration.Element("Barcode")?.Value;
                if (old != @new) {
                    log.LogChange(old, @new);
                    pc.BarCode = @new;
                } else {
                    log.LogNoChanges();
                }

            }


            // Category (gross vs klein)
            {
                log.LogSection("Category");
                Category newCategory = null;
                if (nameElement.Value.Contains("GROSS")) {
                    newCategory = categoryGross;
                }
                if (nameElement.Value.Contains("KLEIN")) {
                    newCategory = categoryKlein;
                }
                var hasCategory = product.Categories.Contains(newCategory);
                if (hasCategory == false && newCategory != null) {
                    log.LogChange("None", newCategory.Name);
                    product.Categories.Add(newCategory);
                } else {
                    log.LogNoChanges();
                }
            }


            log.AppendLine("\n------------  Finished Updating Product  ------------");

        }

        [Obsolete("Ignore the price from calcmenu, since the prices are managed on the backend manually")]
        private static void UpdatePrice(StringBuilder log, ProductConfiguration pc, XElement declaration) {
            log.LogSection("Price");
            var oldPrice = pc.CustomerPrice;
            var newPriceRaw = declaration.Element("Price")?.Value;
            Price newPrice = new Price(0);
            if (string.IsNullOrEmpty(newPriceRaw) == false) {
                newPrice = new Price(newPriceRaw);
            }
            if (oldPrice.Value != newPrice.Value) {
                log.LogChange(oldPrice?.ToString(), newPrice?.ToString());
                pc.CustomerPrice = newPrice.Clone();
            } else {
                log.LogNoChanges();
            }
        }

        private static void UpdateDeclarations(StringBuilder log, Product product, string languageForCMS, XElement declarationParent) {
            log.LogSection("Declaration");
            var declaration = declarationParent.Element("Declaration_Swiss").Value;
            
           

            declaration = FormatAllergens(declaration);

            //Clean divs
            declaration = declaration.Replace("<div>", "<p>");
            declaration = declaration.Replace("</div>", "</p>");

            var newNode = false;
            if (product.Ingredients == null) {
                /// new product here
                var trans = ContentNode.CreateContentNodeForEntityProperty(product.Context, product, nameof(Product.Ingredients), declaration);
                product.Ingredients = trans;
                newNode = true;
            } else {
                product.Ingredients?.IncludeContent(); /// Existing product
            }

            var oldIngredients = product.Ingredients?.GetExactTranslationForLanguage(languageForCMS);
            var oldIngredient = oldIngredients?.ChildrenForType(ContentNode.NODE_TYPE_HTML).FirstOrDefault();
            if (oldIngredient?.Value != declaration || newNode) {
                log.LogChange(newNode ? "" : oldIngredient?.Value, declaration);
                if (oldIngredient == null) {
                    /// Delete children if some danling cms content
                    foreach(var child in product.Ingredients.Children.ToList()) {
                        product.Ingredients.Children.Remove(child);
                        product.Context.DeleteEntity(child);
                    }
                    var trans = product.Ingredients.AddTranslation(languageForCMS);
                    var htmlNode = new ContentNode();
                    htmlNode.NodeType = ContentNode.NODE_TYPE_HTML;
                    htmlNode.Value = declaration;
                    trans.AddChild(htmlNode);

                } else {
                    oldIngredient.Value = declaration; /// new product here /// existing product new value
                }
            } else {
                log.LogNoChanges();
            }
        }


        /// <summary>
        /// Formats the allergens
        /// FROM 
        /// Venere Reis, Mango, Apfel, CASHEWS, Limettensaft, Zwiebeln, Chilli, PfefferMINZE, Kokosflocken, Reisessig, Zucker, SESAMÖL, Koriander, Salz
        /// TO
        /// Venere Reis, Mango, Apfel, <b>Cashews</b>, Limettensaft, Zwiebeln, Chilli, Pfeffer<b>minze</b>, Kokosflocken, Reisessig, Zucker, <b>Sesamöl</b>, Koriander, Salz
        /// </summary>
        /// <param name="ingredients">The ingredients.</param>
        /// <returns></returns>
        private static string FormatAllergens(string ingredients) {
            var ingredientsFormatted = ingredients;

            // Find min two CAPS words except CH
            const string regex = "(?!(CH))([A-ZÖÜÄ]){2,}";
            var matches = Regex.Matches(ingredientsFormatted, regex);

            while (matches.Count > 0) {
                var match = matches[0];
                var allergen = match.ToString();

                var allergenFormatted = allergen;
                // Is start or new word
                if (match.Index == 0 || (match.Index > 0 && ShouldKeepCase(ingredientsFormatted, match))) {
                    allergenFormatted = allergen.First() + allergen.Substring(1, allergen.Length - 1).ToLowerInvariant();
                } else {
                    // in word
                    allergenFormatted = allergen.ToLowerInvariant();
                }

                ingredientsFormatted = ingredientsFormatted.ReplaceAt(match.Index, match.Length, $"<b>{allergenFormatted}</b>");

                matches = Regex.Matches(ingredientsFormatted, regex);
            }
            ingredientsFormatted = "<p>" + ingredientsFormatted + "</p>";
            return ingredientsFormatted;
        }

        // if before is whitspace or '(' keep the case
        private static bool ShouldKeepCase(string ingredientsFormatted, Match match) {
            var character = ingredientsFormatted[match.Index - 1];
            return  Char.IsWhiteSpace(character) || character== '(' ;
        }

        private static void UpdateNutritions(StringBuilder log, Product product, XElement nutritions) {

            if (nutritions == null) return;

            var oldInfos = product.Information.GetData();

            var groups = new List<ProductInfoGroup>();

            var nutrientsNotEnergy = nutritions.Elements("Nutrient").Where(n=>n.Element("NutKey")?.Value != "Energie").ToList();
            var nutrientsJouls = nutritions.Elements("Nutrient").FirstOrDefault(n => n.Element("NutKey")?.Value == "Energie" && n.Element("Unit")?.Value == "kJ");
            var nutrientsCals = nutritions.Elements("Nutrient").FirstOrDefault(n => n.Element("NutKey")?.Value == "Energie" && n.Element("Unit")?.Value == "kcal");


            // Energy
            {

                var name = nutrientsCals.Element("DisplayName")?.Value;
                log.LogSection(name);
                var key = nutrientsCals.Element("NutKey")?.Value;
                var cals = nutrientsCals.Element("ValuePer100g")?.Value;
                var jouls = nutrientsJouls.Element("ValuePer100g")?.Value;

                // Rounding
                var calsParsed = decimal.Parse(cals);
                var joulsParsed = decimal.Parse(jouls);

                cals = System.Math.Round(calsParsed).ToString();
                jouls = System.Math.Round(joulsParsed).ToString();


                var item = oldInfos.SelectMany(g => g.Items).FirstOrDefault(i => i.Name == name);

                var group = oldInfos.FirstOrDefault(oi => oi.Items.Contains(item));
                if (group == null) {
                    group = new ProductInfoGroup(name);
                }
           
                if (item == null) {
                    string energyItem = GetEnergyItemNew(name, jouls, cals, true);
                    item = group.AddItem(energyItem);
                    log.LogChange("", item.Values[NutritionKey]);

                } else {
                    var oldNutritionValue = GetNutritionValue(item);
                    var newNutritionValue = GetEnergyItemNew(name, jouls, cals, false);

                    // Check difference and save
                    if (newNutritionValue != oldNutritionValue) {
                        log.LogChange(oldNutritionValue, newNutritionValue);
                        item.Values[NutritionKey] = newNutritionValue;
                    } else {
                        log.LogNoChanges();
                    }
                }
                groups.Add(group);
            }

            // only these (to ignore gesättige fettsäuren etc)
            var nutrients = new List<string> { "Fett", "Kohlenhydrate", "Eiweiss", "Salz", "Natrium" };

            foreach (var nutrient in nutrientsNotEnergy) {

                var name = nutrient.Element("DisplayName")?.Value;

                if (nutrients.Contains(name) == false) {
                    continue;
                }

                log.LogSection(name);

                var key = nutrient.Element("NutKey")?.Value;
                var value = nutrient.Element("ValuePer100g")?.Value;
                var unit = nutrient.Element("Unit")?.Value;
                var format = nutrient.Element("Format")?.Value;

                var item = oldInfos.SelectMany(g => g.Items).FirstOrDefault(i => i.Name == name);

                var group = oldInfos.FirstOrDefault(oi => oi.Items.Contains(item));
                if (group == null) {
                    group = new ProductInfoGroup(name);
                }

                if (item == null) {
                    var newNutritionValue = GetNutritionItemNew(name, value);
                    item = group.AddItem(newNutritionValue);
                    log.LogChange("", item.Values[NutritionKey]);

                } else {
                    var newNutritionValue = GetNutritionItemNew(name, value, false);
                    var oldNutritionValue = GetNutritionValue(item);
                    // Check difference and save
                    if (newNutritionValue != oldNutritionValue) {
                        log.LogChange(oldNutritionValue, newNutritionValue);
                        item.Values[NutritionKey] = newNutritionValue;
                    } else {
                        log.LogNoChanges();
                    }
                }
                groups.Add(group);
            }
            product.Information.SetData(groups);
        }

        /// <summary>
        /// Gets the nutrition value for 100g
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private static string GetNutritionValue(ProductInfoItem item) {
            string oldGram100 = null;
            if (item.Values.ContainsKey(NutritionKey)) {
                oldGram100 = item.Values[NutritionKey];
            }
            return oldGram100;
        }

        //private static string GetNutritionItem(decimal factor100, string name, decimal value, bool includeKey = true) {
        //    var gramsPer100 = decimal.Round(value * factor100, 0);
        //    var gramsPer100Display = string.Format("{0:0.##}", gramsPer100);

        //    var item = $"{gramsPer100Display} g";
        //    if (includeKey) {
        //        item = $"{name},{NutritionKey}:" + item;
        //    }
        //    return item;
        //}

        private static string GetNutritionItemNew(string name, string value, bool includeKey = true) {
            var item = $"{value} g";
            if (includeKey) {
                item = $"{name},{NutritionKey}:" + item;
            }
            return item;
        }

        private static string GetEnergyItemNew(string name, string joulesPer100Display, string calsPer100Display,  bool includeKey = true) {
        
            var energyItem = $"{joulesPer100Display} kJ / {calsPer100Display} kcal";
            if (includeKey) {
                energyItem = $"{name},{NutritionKey}:" + energyItem;
            }
            return energyItem;
        }

        private static string GetEnergyItem(decimal factor100, decimal calsToJoules, string name, decimal value, bool includeKey = true) {
            var calsPer100 = decimal.Round(value * factor100, 0);
            var joulesPer100 = decimal.Round(calsPer100 * calsToJoules);

            var calsPer100Display = string.Format("{0:0.##}", calsPer100);
            var joulesPer100Display = string.Format("{0:0.##}", joulesPer100);

            var energyItem = $"{joulesPer100Display} kJ / {calsPer100Display} kcal";
            if (includeKey) {
                energyItem = $"{name},{NutritionKey}:" + energyItem;
            }
            return energyItem;
        }

        private static void UpdateDetails(StringBuilder log, Product product, XElement declaration, string detailsKey, string xmlKey) {
            log.LogSection(detailsKey);
            var newValue = declaration.Element(xmlKey)?.Value;
            var oldValue = product.Details[detailsKey]?.ToString();
            if (oldValue != newValue) {
                log.LogChange(oldValue, newValue);
                product.Details[detailsKey] = newValue;
            } else {
                log.LogNoChanges();
            }
        }

        private static void UpdateWeight(StringBuilder log, Product product, XElement declaration, string detailsKey, string weightKey, string unitKey) {
            log.LogSection(detailsKey);
            var newValue = declaration.Element(weightKey)?.Value + " " + declaration.Element(unitKey)?.Value;
            var oldValue = product.Details[detailsKey]?.ToString();
            if (oldValue != newValue) {
                log.LogChange(oldValue, newValue);
                product.Details[detailsKey] = newValue;
            } else {
                log.LogNoChanges();
            }
        }

        /// <summary>
        /// Calls the specified webservice method with arguments
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        public string Call(string method, Dictionary<string,string> arguments) {
            var uri = new Uri(Config.ShopProductUpaters.CalcMenu.ServiceURL +"/"+ method); //http://localhost:8081/Test.asmx
            var wcClient = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
            var nvcKeys = arguments.ToNameValueCollection();
            wcClient.Headers.Add(nvcKeys);
            var bytes = wcClient.UploadValues(uri.ToString(),nvcKeys);
            var result = wcClient.Encoding.GetString(bytes);
            return result;
        }
    
    }

    internal static class StringBuilderExtensions {

        internal static void LogChange(this StringBuilder log, string oldValue, string newValue) {
            log.AppendLine($"\tOld:" + oldValue);
            log.AppendLine($"\tNew:" + newValue);
        }

        internal static void LogNoChanges(this StringBuilder log) {
            log.AppendLine("\tNo changes");
        }

        internal static void LogSection(this StringBuilder log, string section) {
            log.AppendLine($"\nUpdating {section}:");
        }
    }
}
