using Machinata.Core.Util;
using Machinata.Module.Shop.Model;
using Machinata.Module.Shop.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.Services {
    public abstract class ProductUpdater {


        public abstract StringBuilder Update(Product product, ProductConfiguration productConfiguration);

        public virtual string Name {
            get{
                return this.GetType().Name.Replace("ProductUpdater", "").ToSentence();
            }
        }


        public static ProductUpdater Updater { get
            {
                var updates = GetProductUpdaters();
                if (!string.IsNullOrEmpty(Shop.Config.ShopProductUpdater) && updates.ContainsKey(Config.ShopProductUpdater)) {
                    return updates[Config.ShopProductUpdater];
                }
                return null;
            }         
        } 


        public static Dictionary<string, ProductUpdater> GetProductUpdaters() {
            var result = new Dictionary<string, ProductUpdater>();
            var types = Core.Reflection.Types.GetMachinataTypes(typeof(ProductUpdater)).Where(t => !t.IsAbstract);
            foreach (var type in types) {
                result[type.Name] = Core.Reflection.Types.CreateInstance<ProductUpdater>(type);
            }
            return result;
        }

}
}
