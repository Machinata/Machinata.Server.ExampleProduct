using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Module.Shop.Model;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using System.Linq;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using System.Collections.Generic;

namespace Machinata.Module.Shop.Handler {


    public class ShopPDFHandler : PDFPageTemplateHandler {
        

        /// <summary>
        /// Currently P&L seems to have to different delivery slips.
        ///  -B2B with no prices (Vermerk: Rechnung folgt)
        ///  -B2C with prices (same as invoice?) Vermerk: Online mit Kreditkarte bezahlt
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/pdf/shop/order/{orderId}/shipment/{shipmentId}/delivery-slip", AccessPolicy.PUBLIC_ARN)]
        public void OrderDeliverySlip(string orderId, string shipmentId) {
            // Init
            var order = DB.Orders()
                .Include(nameof(Order.BillingAddress))
                .Include(nameof(Order.ShippingAddress))
                .Include(nameof(Order.OriginAddress))
                .Include(nameof(Order.Shipments))
                .Include(nameof(Order.User))
                .Include(nameof(Order.Business))
                .GetByPublicId(orderId);

            // Check security
            var adminArn = "/admin/finance";
            var isAdmin = this.ValidateARN(adminArn, false);
            if (isAdmin == false) {
                this.RequireAdminARNOrBusinessOrUserAssociation(adminArn, order.Business, order.User);
            }

         

            // Shipment
            var shipment = order.Shipments.AsQueryable().GetByPublicId(shipmentId);
            shipment.LoadFirstLevelNavigationReferences();
            shipment.LoadFirstLevelObjectReferences();


            // Items
            FormBuilder itemsForm = null;

            if (Config.ShopDeliverySlipShowPrices == true) {
                itemsForm = new FormBuilder(Forms.Admin.EMPTY);
                itemsForm.Include(nameof(OrderItem.Title));
                itemsForm.Include(nameof(OrderItem.SKU));
                itemsForm.Include(nameof(OrderItem.Quantity));
                itemsForm.Include(nameof(OrderItem.CustomerPrice));
                itemsForm.Include(nameof(OrderItem.CustomerPriceSum));
            } else {
                // Default 
                itemsForm = new FormBuilder(Forms.Admin.PDF);
            }
            Template.InsertEntityList(
                variableName: "shipment.order-items",
                entities: shipment.OrderItems.AsQueryable().OrderBy(oi=>oi.SKU), 
                form: itemsForm,
                total: new FormBuilder(Forms.Admin.TOTAL).Custom("Title",null,null,"{text.total}")
            );
            this.Template.InsertContent(
                variableName: "shipment.cms-content",
                cmsPath:"/General/Order/DeliverySlipPDFText", 
                throw404IfNotExists: false
            );

            // Insert entity properties 
            this.Template.InsertVariables("shipment", shipment);
            this.Template.InsertVariables("order", order);
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.billing-address-breaked", order.BillingAddress.ToString().Replace(", ","\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.shipping-address-breaked", order.ShippingAddress.ToString().Replace(", ", "\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.sender-address-breaked", order.OriginAddress.ToString().Replace(", ","\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.sender-address.city", order.OriginAddress.City);
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.sender-address.phone", order.OriginAddress.Phone);
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.sender-address.email", order.OriginAddress.Email);

            if (DB.OwnerBusiness() != null) {
                this.Template.InsertVariables("sender", this.DB.OwnerBusiness());
            }


            var website = Core.Config.PublicURL.Replace("https://", string.Empty).Replace("http://", string.Empty);
            this.Template.InsertVariableXMLSafeWithLineBreaks("shipment.sender-address.website", website);


            // Set footer
            this.FooterLeft = "{text.delivery-slip} "+ order.SerialId;

            // Set filename
            FileName = shipment.GetPDFFileName();
            

        }

        [RequestHandler("/pdf/shop/orders/summary/{date}", AccessPolicy.PUBLIC_ARN)]
        public void OrderSummary(string date) {
            // Check security
            this.RequireARN("/admin/shop");

            // Status
            var statusFilter = this.Params.Enums<Order.StatusType>("status",new List<Order.StatusType>() { Order.StatusType.Confirmed });
           
            // Orders
            var orders = OrderShopAdminHandler.GetOrdersForSummary(this.DB, date, statusFilter);

            // Items
            Template.InsertEntityList(
                variableName: "orders",
                entities: orders,
                form: new FormBuilder(Forms.Admin.PDF),
                total: new FormBuilder(Forms.Admin.TOTAL).Custom("Title", null, null, "{text.total}")
            );

            // Order items
            var items = OrderShopAdminHandler.CreateOrderSummaryItems(orders);
            // Items
            Template.InsertEntityList(
                variableName: "order-items",
                entities: items,
                form: new FormBuilder(Forms.Admin.LISTING),
                total: new FormBuilder(Forms.Admin.TOTAL).Custom("Title", null, null, "{text.total}")
            );

            // Set footer
            this.FooterLeft = "{text.order-summary}: " + date;

            // Set filename
            FileName = "Order_Summary_" + date;


        }

    }
}
