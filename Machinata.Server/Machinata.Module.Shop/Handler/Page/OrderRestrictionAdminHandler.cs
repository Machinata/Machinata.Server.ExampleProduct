
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class OrderRestrictionAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/order-restrictions")]
        public void Restrictions() {
            // Navigation
            var entities = this.DB.OrderRestrictions().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/order-restrictions/restriction/{entity.public-id}", true);
            this.Navigation.Add("shop");
            this.Navigation.Add("order-restrictions");
        }

        [RequestHandler("/admin/shop/order-restrictions/create")]
        public void Create() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new OrderRestriction(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/order-restrictions/create"
               //onSuccess: "/admin/shop/order-restrictions/order-restriction/{order-restriction.public-id}"
            );
            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("order-restrictions");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/shop/order-restrictions/restriction/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderRestrictions().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            this.Template.InsertVariables("entity", entity);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/order-restriction/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("order-restrictions");
            this.Navigation.Add($"restriction/{entity.PublicId}", entity.Name);
            this.Navigation.Add("edit");
        }

        [RequestHandler("/admin/shop/order-restrictions/restriction/{publicId}")]
        public void View(string publicId) {
            var entity = DB.OrderRestrictions().Include(nameof(OrderRestriction.BusinessOrderRestrictions)).GetByPublicId(publicId);
        
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Insert selection list
            if (!entity.AllBusinesses) {
                this.Template.InsertSelectionList(
                    variableName: "entity-businesses",
                    entities: this.DB.CustomerBusinesses().OrderBy(b => b.Name),
                    selectedEntities: entity.GetBusinesses(),
                    selectionAPICall: "/api/admin/shop/order-restriction/" + entity.PublicId + "/toggle-business/{entity.public-id}",
                    form: new FormBuilder(Forms.Admin.SELECTION)
                );
            }

            this.Template.InsertVariable("show-business-selection", entity.AllBusinesses);
          

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("order-restrictions");
            this.Navigation.Add($"restriction/{entity.PublicId}", entity.Name);
        }


    }
}
