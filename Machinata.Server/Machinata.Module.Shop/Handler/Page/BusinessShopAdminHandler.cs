
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class BusinessShopAdminHandler : AdminPageTemplateHandler {
        

        [RequestHandler("/admin/shop/businesses")]
        public void Businesses() {
            var entities = this.DB.CustomerBusinesses().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/businesses/business/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("businesses", "{text.businesses}");
        }


        [RequestHandler("/admin/shop/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {
            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);
          
            // Orders
            var orders = DB.Orders().Include("Business").Where(o => o.Business.Id == entity.Id);
            this.Template.InsertEntityList("entity.orders", orders, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}", true);

            // Catalog Groups
            var groups = DB.GetCatalogGroupsForBusiness(entity);
            this.Template.InsertEntityList("entity.group-catalogs", groups, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/catalogs/group/{entity.public-id}", true);

            // Products Preview
            var productsIds = Catalog.GetProductsForBusiness(this.DB, entity).Select(e => e.Id);
            var products = this.DB.Products()
                .Include(nameof(Product.Configurations))
                .Include(nameof(Product.Categories))
                .Where(e => productsIds.Contains(e.Id));
            this.Template.InsertEntityList("entity.products", products, new FormBuilder(Forms.Admin.LISTING), "{page.navigation.current-path}/product/{entity.public-id}", false);
            //this.Template.InsertTemplates(
            //    variableName: "entity.products-preview",
            //    entities: products,
            //    templateName: "print.label",
            //    forEachEntity: new Action<Product, PageTemplate>(delegate (Product n, PageTemplate t) {
            //        t.InsertVariables("business", entity);
            //    }));

            this.Template.InsertVariable("packaging-enabled", Shop.Config.ShopPackagingEnabled);

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
        }

        [RequestHandler("/admin/shop/businesses/business/{publicId}/product/{productId}")]
        public void ProductView(string publicId, string productId) {
            var business = DB.Businesses().GetByPublicId(publicId);

            // Products Preview
            var products = Catalog.GetProductsForBusiness(this.DB, business);

            // Product
            var product = products.GetByPublicId(productId);

            // Business Configuration
            var configuration = product.GetMergedBusinessConfiguration(business);

            // Default Configuration
            var defaultConfiguration = product.GetDefaultConfiguration();



            // Variables
            this.Template.InsertVariables("business", business);
            this.Template.InsertVariables("product", product);
            this.Template.InsertVariables("default-configuration", defaultConfiguration);
            this.Template.InsertVariables("product.configuration", configuration);
            this.Template.InsertVariable("date", DateTime.UtcNow.ToString(Core.Config.DateFormat));

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", business.Name);
            this.Navigation.Add($"product/{productId}", product.Name);
        }


        [RequestHandler("/admin/shop/businesses/generate-invoices")]
        public void InvoicesCreate() {
            this.RequireWriteARN();

            // Open Orders
            var ordersNoInvoices = Module.Shop.Logic.InvoiceGeneration.GetOrdersToGenerateInvoices(DB);
            this.Template.InsertSelectionList(
                variableName: "no-invoices-orders",
                entities: ordersNoInvoices,
                selectedEntities: ordersNoInvoices,
                form: new FormBuilder(Forms.Admin.LISTING),
                selectionAPICall: null
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add("create", "{text.generate-invoices}");

        }

    }
}
