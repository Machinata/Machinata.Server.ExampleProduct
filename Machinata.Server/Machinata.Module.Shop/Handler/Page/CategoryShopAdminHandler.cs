
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class ShopCategoryAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/categories")]
        public void Categories() {
            // Navigation
            var entities = this.DB.Categories().AsQueryable();
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/categories/category/{entity.public-id}", true);
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("categories", "{text.categories}");
        }

        [RequestHandler("/admin/shop/categories/create")]
        public void CategoryCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new Category(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/categories/create",
               onSuccess: "/admin/shop/categories/category/{product.public-id}"
            );
            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("categories", "{text.categories}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/shop/categories/category/{publicId}/edit")]
        public void CategoryEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Categories().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            this.Template.InsertVariables("entity", entity);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/category/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("categories", "{text.categories}");
            this.Navigation.Add($"category/{entity.PublicId}", entity.Name);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/shop/categories/category/{publicId}")]
        public void CategoryView(string publicId) {
            var entity = DB.Categories().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Products with this category
            entity.Include(nameof(entity.Products));
            var products = Template.Paginate(entity.Products.AsQueryable(), this);
            this.Template.InsertEntityList("entity.products", products, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/products/product/{entity.public-id}");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("categories", "{text.categories}");
            this.Navigation.Add($"category/{publicId}", entity.Name);
        }


    }
}
