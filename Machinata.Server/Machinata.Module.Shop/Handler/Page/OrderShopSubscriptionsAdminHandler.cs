
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class OrderSubscriptionShopAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/subscriptions")]
        public void OrderSubscriptions() {
            var entities = this.DB.OrderSubscriptions().AsQueryable();
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/subscriptions/subscription/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
        }

        [RequestHandler("/admin/shop/subscriptions/create")]
        public void OrderSubscriptionCreate() {
            this.RequireWriteARN();
            
            this.Template.InsertForm(
               variableName: "form",
               entity: new OrderSubscription() { User = User },
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/subscriptions/create",
               onSuccess: "/admin/shop/subscriptions/subscription/{subscription.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/shop/subscriptions/subscription/{publicId}/edit")]
        public void OrderSubscriptionEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptions().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/subscription/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

                     
            // Entity variables
            Template.InsertVariables("entity", entity);
          
            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add($"subscription/{entity.PublicId}", entity.SerialId);
            this.Navigation.Add("edit", "{text.edit}", "{text.edit} " + "{text.order-subscription} " + entity.SerialId);
        }
        

      

        [RequestHandler("/admin/shop/subscriptions/subscription/{subscriptionId}/item/{publicId}")]
        public void OrderSubscriptionItemView(string subscriptionId, string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptionItems().GetByPublicId(publicId);
            var subscription = DB.OrderSubscriptions().GetByPublicId(subscriptionId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            var selectedMainCategory = entity.Categories.FirstOrDefault(c => c.CategoryType == Category.CategoryTypes.Main);
            var selectedSubCategories = entity.Categories.Where(c => c.CategoryType == Category.CategoryTypes.Sub);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT)
                    .RadioList("MainCategories", "main-categories", GetMainCategories(),selectedMainCategory.Name.ToLower(),true)
                    .CheckboxList("SubCategories", "sub-categories", GetSubCategories(),string.Join(",",selectedSubCategories.Select(s=>s.Name.ToLower())),false),
               apiCall: "/api/admin/shop/subscription-item/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add($"subscription/{subscription.PublicId}", subscription.SerialId);
            this.Navigation.Add($"item/{entity.PublicId}", entity.PublicId, "{text.order-subscription-item}: " + entity.PublicId);
        }
        
        /*
        [RequestHandler("/admin/shop/subscriptions/subscription/{subscriptionId}/item/{publicId}/edit")]
        public void OrderSubscriptionItemEdit(string subscriptionId, string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptionItems().GetByPublicId(publicId);
            var subscription = DB.OrderSubscriptions().GetByPublicId(subscriptionId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            var selectedMainCategory = entity.Categories.FirstOrDefault(c => c.CategoryType == Category.CategoryTypes.Main);
            var selectedSubCategories = entity.Categories.Where(c => c.CategoryType == Category.CategoryTypes.Sub);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT)
                    .RadioList("MainCategories", "main-categories", GetMainCategories(),selectedMainCategory.Name.ToLower(),true)
                    .CheckboxList("SubCategories", "sub-categories", GetSubCategories(),string.Join(",",selectedSubCategories.Select(s=>s.Name.ToLower())),false),
               apiCall: "/api/admin/shop/subscription-item/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add($"subscription/{subscription.PublicId}", subscription.SerialId);
            this.Navigation.Add($"item/{entity.PublicId}", entity.PublicId, "{text.order-subscription-item} " + entity.PublicId);
            this.Navigation.Add($"edit");
        }*/



        [RequestHandler("/admin/shop/subscriptions/subscription/{publicId}")]
        public void OrderSubscriptionView(string publicId) {
            var entity = DB.OrderSubscriptions().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));

            entity.SubscriptionItems.ToList().ForEach(si => si.Include(nameof(si.Categories)));

            // Items
            var items = entity.SubscriptionItems.AsQueryable();
            items = this.Template.Paginate(items, this);
            this.Template.InsertEntityList("entity.items", items, new FormBuilder(Forms.Admin.LISTING), "{page.navigation.current-path}/item/{entity.public-id}", true, true);
            this.Template.InsertVariable("items.count", items.Count());

            // Orders
            this.Template.InsertEntityList("orders", entity.Orders.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}");

            // Upcoming orders
            this.Template.InsertEntityList(
                variableName: "upcoming-orders",
                entities: entity.GetUpcomingDeliveryDates()
            );

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add($"subscription/{entity.PublicId}", "{text.order-subscription} " + entity.SerialId);

        }

        [RequestHandler("/admin/shop/subscriptions/subscription/{subscriptionId}/create-item")]
        public void OrderSubscriptionCreateItem(string subscriptionId) {
            this.RequireWriteARN();


            var entity = DB.OrderSubscriptions().GetByPublicId(subscriptionId);
            this.Template.InsertForm(
               variableName: "form",
               entity: new OrderSubscriptionItem(),
               form: new FormBuilder(Forms.Admin.CREATE).RadioList("MainCategories", "main-categories", GetMainCategories()).CheckboxList("SubCategories", "sub-categories", GetSubCategories()),
               apiCall: "/api/admin/shop/subscription/" + subscriptionId + "/items/create",
               onSuccess: "/admin/shop/subscriptions/subscription/" + subscriptionId
            );

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("subscriptions");
            this.Navigation.Add($"subscription/{entity.PublicId}", entity.SerialId);
            this.Navigation.Add($"create-item", "{text.create-order-subscription-item=Create Order Subscription Item}");

        }

        private Dictionary<string, string> GetSubCategories() {
            return DB.Categories().Where(c => c.CategoryType == Category.CategoryTypes.Sub).ToDictionary(x => x.Name, c => c.Name.ToLower());
        }

        private Dictionary<string, string> GetMainCategories() {
            return DB.Categories().Where(c => c.CategoryType == Category.CategoryTypes.Main).ToDictionary(x => x.Name, c => c.Name.ToLower());
        }

    


    }
}
