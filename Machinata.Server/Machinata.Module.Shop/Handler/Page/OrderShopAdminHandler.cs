
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Module.Shop.View;
using Machinata.Core.Exceptions;
using Machinata.Module.Admin.View;

namespace Machinata.Module.Admin.Handler {


    public class OrderShopAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/orders")]
        public void Orders() {

            // Insert filtered list
            var entities = this.DB.Orders().ConfirmedWithDates().OrderByDescending(Order.OrderDateExpression);
            var filteredEntities = this.Template.Filter(entities, this, nameof(Order.SerialId));
            var entitiesPaged = this.Template.Paginate(filteredEntities, this);
            this.Template.InsertEntityList("entity-list", entitiesPaged, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}", true);

            // Insert schedule
            if (true) {
                var start = Time.StartOfDefaultTimezoneWeekInUtc(DateTime.UtcNow).AddDays(-7).ToUniversalTime();
                var orderDates = entities.Select(Order.OrderDateExpression).Select(o => o.Value).ToList();
                var end = start.EndOfWeek();
                if (orderDates.Count > 0) {
                    end = Time.StartOfDefaultTimezoneWeekInUtc(orderDates.Max()).EndOfWeek();
                }

                this.Template.InsertScheduleTable(
                    entities: entities,
                    start: start,
                    end: end,
                    variableName: "order-schedule",
                    templateName: "schedule-table",
                    dateSelector: (e) =>  (e as Order).OrderDate.Value,
                    getTemplateForDay: (orders, date,template) => GetTemplateForDay(orders, date,this.Template),
                    startOfWeek: DayOfWeek.Monday,
                    endOfWeekToShow: DayOfWeek.Sunday
                );
            }

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");

        }

        [RequestHandler("/admin/shop/orders/all")]
        public void All() {

            // Insert filtered list
            var entities = this.DB.Orders().OrderByDescending(o => o.Created);
            var filteredEntities = this.Template.Filter(entities, this, nameof(Order.SerialId));
            var entitiesPaged = this.Template.Paginate(filteredEntities, this);
            this.Template.InsertEntityList("entity-list", entitiesPaged, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}", true);


            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add("all");

        }

        private static  PageTemplate GetTemplateForDay(IEnumerable<ModelObject> orders, DateTime date, PageTemplate template) {
            var dayTemplate = template.LoadTemplate("order-schedule.week.day");

            // Orders
            dayTemplate.InsertTemplates("orders", orders, "order-schedule.week.day.order");

            // Restrictions
            var orderRestrictions = template.Handler.DB.OrderRestrictions().ToList();
            var orderRestrictionsOnDay = orderRestrictions.Where(or => !or.IsValid(date,null));
            dayTemplate.InsertTemplates("restrictions", orderRestrictionsOnDay, "order-schedule.week.day.restriction");
            dayTemplate.InsertTemplates("orders", orders, "order-schedule.week.day.order");

            // Tool
            dayTemplate.InsertTemplate("tool", "order-schedule.week.day.tool");

            dayTemplate.InsertVariable("date", date.ToString(Core.Config.DateFormat));

            return dayTemplate;
        }

        [RequestHandler("/admin/shop/orders/create")]
        public void OrderCreate() {
            this.RequireWriteARN();

            DateTime? deliveryDate = null;
            if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                deliveryDate = DateTime.UtcNow.AddDays(7);
            }

            this.Template.InsertForm(
               variableName: "form",
               entity: new Order() { Status = Order.StatusType.Draft, DeliveryDate = deliveryDate },
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/orders/create",
               onSuccess: "/admin/shop/orders/order/{order.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/shop/orders/order/{publicId}/edit")]
        public void OrderEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/orders/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Status
            var statusTemplateOverview = Template.LoadTemplate("order.status-overview");
            var statuses = new List<PageTemplate>();
            foreach(var status in entity.StatusFlow) {
                var statusTemplate = Template.LoadTemplate("order.status-overview.status");
                statusTemplate.InsertVariable("status.name", status);
                statusTemplate.InsertVariable("status.active", status == entity.Status ? "selected": string.Empty);
                statuses.Add(statusTemplate);
            }
            statusTemplateOverview.InsertTemplates("statuses", statuses);
            Template.InsertTemplate("order.status-overview", statusTemplateOverview);
            
            // Entity variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{entity.PublicId}", entity.SerialId);
            this.Navigation.Add("edit", "{text.edit}", "{text.edit} " + "{text.order} " + entity.SerialId);
        }
        

        [RequestHandler("/admin/shop/orders/order/{publicId}/status")]
        public void OrderStatus(string publicId) {
            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();
            
            // Status
            var statusTemplateOverview = Template.LoadTemplate("order.status-overview");
            var statuses = new List<PageTemplate>();
            foreach(var status in entity.StatusFlow) {
                var statusTemplate = Template.LoadTemplate("order.status-overview.status");
                statusTemplate.InsertVariable("status.name", status);
                statusTemplate.InsertVariable("status.active", status == entity.Status ? "selected": string.Empty);
                statuses.Add(statusTemplate);
            }
            statusTemplateOverview.InsertTemplates("statuses", statuses);
            Template.InsertTemplate("order.status-overview", statusTemplateOverview);
            
            // Entity variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{entity.PublicId}", entity.SerialId);
            this.Navigation.Add("status", "{text.status}");
        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/item/{publicId}")]
        public void OrderItemView(string orderId, string publicId) {
            var order = DB.Orders().GetByPublicId(orderId);
            var orderItem = DB.OrderItems()
            .Include(nameof(OrderItem.Product) + "." + nameof(Shop.Model.Product.Photos))
            .GetByPublicId(publicId);

            orderItem.LoadFirstLevelNavigationReferences();
            orderItem.LoadFirstLevelObjectReferences();

            if (!order.OrderItems.Contains(orderItem)) {
                throw new BackendException("wrong-order", "This order does not contain the given order item");
            }

            // Rejected Tool
            if (!orderItem.Rejection) {
                Template.InsertTemplate("edit.add-rejected-button", Template.LoadTemplate("edit.add-rejected-button"));
            } else {
                Template.InsertVariableEmpty("edit.add-rejected-button");
            }

            Template.InsertVariables("product", orderItem.Product);
            Template.InsertPropertyList("entity", orderItem, new FormBuilder(Forms.Admin.EMPTY).Include(nameof(OrderItem.Title)).Include(nameof(OrderItem.Quantity)));
            Template.InsertVariables("entity", orderItem);

            var template = this.Template.GetItemDetailTemplate( orderItem);
            this.Template.InsertTemplate("details", template);


            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{orderItem.Order.PublicId}", orderItem.Order.SerialId);
            this.Navigation.Add($"item/{orderItem.PublicId}", orderItem.Title, "{text.order-item=Order Item}: " + orderItem.Title);
        }

      

        [RequestHandler("/admin/shop/orders/order/{orderId}/item/{publicId}/edit")]
        public void OrderItemEdit(string publicId) {
            this.RequireWriteARN();

            var orderItem = DB.OrderItems().GetByPublicId(publicId);
            orderItem.LoadFirstLevelNavigationReferences();
            orderItem.LoadFirstLevelObjectReferences();
            
            this.Template.InsertForm(
               variableName: "form",
               entity: orderItem,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/order-item/" + orderItem.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );
            
            Template.InsertVariables("entity", orderItem);


            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{orderItem.Order.PublicId}", orderItem.Order.SerialId);
            this.Navigation.Add($"item/{orderItem.PublicId}", orderItem.Title, "{text.order-item=Order Item}: " + orderItem.Title);
            this.Navigation.Add($"edit", "{text.edit}");
        }



        [RequestHandler("/admin/shop/orders/order/{publicId}")]
        public void OrderView(string publicId) {

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));

            // Cards
            this.Template.InsertCards("entity.cost-cards", entity.VisualCardsForCosts());
            this.Template.InsertCards("entity.address-cards", entity.VisualCardsForShippingBilling());

            // Order Items
            this.Template.InsertEntityList("order-items", entity.OrderItems.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "{page.navigation.current-path}/item/{entity.public-id}", true, true);
            
            // Origin destination address
            this.Template.InsertVariables("entity.origin", entity.OriginAddress);
            if (entity.ShippingAddress != null) {
                this.Template.InsertVariables("entity.destination", entity.ShippingAddress);
            }

            // Shipments
            this.Template.InsertEntityList("order-shipments", entity.Shipments.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "{page.navigation.current-path}/shipment/{entity.public-id}", true, true);
            this.Template.InsertVariable("order-shipments.count", entity.Shipments.Count);
            this.Template.InsertVariable("shipment.public-id", entity.Shipments.Any()? entity.Shipments.First().PublicId:string.Empty);

            // Coupon
            this.Template.InsertEntityList("order-coupons", entity.OrderCoupons.AsQueryable(), new FormBuilder(Forms.Admin.LISTING));

            // Shipping/Billing Address
            this.Template.InsertVariable("order-addresses.editable", entity.IsStatusBefore(Order.StatusType.Confirmed));
            
            // Invoice
            this.Template.InsertEntityList(variableName: "order-invoice",
                                           entity: entity.Invoice,
                                           form:  new FormBuilder(Forms.Admin.LISTING),
                                           link: "/admin/finance/invoices/invoice/{entity.public-id}",
                                           loadFirstLevelReferences:  true);

            this.Template.InsertVariable("invoice.count", entity.Invoice!= null ? "1" : "0");
            this.Template.InsertVariable("invoice.public-id", entity.Invoice != null ? entity.Invoice.PublicId :"");

            // User
            this.Template.InsertVariable("order.user.public-id", entity.User != null ? entity.User.PublicId : "");
            this.Template.InsertVariable("order.user.count", entity.User != null ? "1" : "0");

            // Cancel
            this.Template.InsertVariable("order.allow-cancel", entity.Status != Order.StatusType.Canceled);

            // Confimed?
            this.Template.InsertVariable("order.after-confirmed", !entity.IsStatusBefore(Order.StatusType.Confirmed));

            // Statuses
            this.Template.InsertEntityList("statuses", entity.OrderHistories.OrderByDescending(os => os.Id).AsQueryable(), new FormBuilder(Forms.Admin.LISTING));

            // Labels?
            this.Template.InsertVariable("packaging-enabled", Shop.Config.ShopPackagingEnabled);

          
            // Print Mode
            if (Shop.Config.ShopPackagingPrintMode == "remote") {
                this.Template.InsertVariableUnsafe("print-url", "/admin/shop/packaging/labels/print-remote");
            } else {
                this.Template.InsertVariableUnsafe("print-url", "/admin/shop/packaging/labels/print");
            }
          
            this.Template.InsertVariable("production-date", entity.DeliveryDate?.ToString(Core.Config.DateFormat));

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{entity.PublicId}", entity.SerialId, "{text.order} " + entity.SerialId);

        }


        [RequestHandler("/admin/shop/orders/order/{publicId}/items")]
        public void OrderAddItemsNormalShops(string publicId) {
            this.RequireWriteARN();

      

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.Include(nameof(entity.Business));
            this.Template.InsertVariables("entity", entity);

            // Product selection
            var products = this.DB.Products()
                   .Include(nameof(Shop.Model.Product.Configurations) + "." + nameof(ProductConfiguration.Business))
                   .Include(nameof(Shop.Model.Product.Configurations) + "." + nameof(ProductConfiguration.Product));




            IQueryable<ProductConfiguration> configurations = null;

            // if shop is has a business try to load the specific Business Configuration
            if (entity.Business != null) {
                configurations = Shop.Model.Product.GetMergedBusinessConfigurationsForProducts(entity.Business, products).AsQueryable();
            } else {
                configurations = products.SelectMany(p => p.Configurations);
            }


            this.Template.InsertQuantityList(
                variableName: "configurations",
                entities: configurations.AsQueryable(),
                quantityEntities: entity.OrderItems.Where(oi => oi.Rejection == false),
                form: new FormBuilder(Forms.Admin.SELECTION)
                .Exclude(nameof(ProductConfiguration.RetailPrice))
                .Exclude(nameof(ProductConfiguration.Business))
                );
           

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{entity.PublicId}", entity.SerialId, "{text.order} " + entity.SerialId);
            this.Navigation.Add("items", "{text.order-items}");
        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/shipment/{publicId}")]
        public void OrderShipmentView(string orderId, string publicId) {
            var order = DB.Orders().GetByPublicId(orderId);
            var shipment = DB.OrderShipments().GetByPublicId(publicId);

            shipment.LoadFirstLevelNavigationReferences();
            shipment.LoadFirstLevelObjectReferences();

            if (!order.Shipments.Contains(shipment)) {
                throw new BackendException("wrong-order", "This order does not contain the given shipment");
            }

            Template.InsertPropertyList("entity", shipment, new FormBuilder(Forms.Admin.VIEW));
            Template.InsertVariables("entity", shipment);
            Template.InsertVariable("order.public-id", orderId);

            // Items
            Template.InsertEntityList("order-items", shipment.OrderItems.AsQueryable(), new FormBuilder(Forms.Admin.LISTING));


            // Navigation  
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{orderId}", shipment.Order.SerialId);
            this.Navigation.Add($"shipment/{shipment.PublicId}", shipment.PublicId, "{text.shipment}: " + shipment.PublicId);
          
        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/shipment/{publicId}/edit")]
        public void OrderShipmentEdit(string publicId) {
            this.RequireWriteARN();

            var shipment = DB.OrderShipments().GetByPublicId(publicId);
            shipment.LoadFirstLevelNavigationReferences();
            shipment.LoadFirstLevelObjectReferences();

            this.Template.InsertForm(
               variableName: "form",
               entity: shipment,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/shipment/" + shipment.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            Template.InsertVariables("entity", shipment);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{shipment.Order.PublicId}", shipment.Order.SerialId);
            this.Navigation.Add($"shipment/{shipment.PublicId}", shipment.PublicId, "{text.shipment=Shipment}: " + shipment.PublicId);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/shipping-address")]
        public void OrderShippingAddress(string orderId) {
            var order = DB.Orders().Include("Business.Addresses").GetByPublicId(orderId);
            order.LoadFirstLevelNavigationReferences();
            order.LoadFirstLevelObjectReferences();
            IQueryable<Core.Model.Address> addresses = null; 
            if (order.Business != null) {
                addresses = order.Business.Addresses.Where(a => !a.Archived).AsQueryable();
            }
            else {
                addresses = this.DB.Addresses();
            }
            addresses = Template.Paginate(addresses, this);
            this.Template.InsertSelectionList("entity-list", 
                addresses,
                order.ShippingAddress, 
                new FormBuilder(Forms.Admin.SELECTION),
                "/api/admin/shop/order/" + order.PublicId + "/shipping-address/{entity.public-id}/toggle"
                );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{order.PublicId}", order.SerialId);
            this.Navigation.Add($"shipping-address/{order.PublicId}", order.PublicId, "{text.shipping-address}: " + order.PublicId);
          
        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/billing-address")]
        public void OrderBillingAddress(string orderId) {
            var order = DB.Orders().Include("Business.Addresses").GetByPublicId(orderId);
            order.LoadFirstLevelNavigationReferences();
            order.LoadFirstLevelObjectReferences();
            IQueryable<Core.Model.Address> addresses = null;
            if (order.Business != null) {
                addresses = order.Business.Addresses.Where(a => !a.Archived).AsQueryable();
            } else {
                addresses = this.DB.Addresses();
            }
            addresses = Template.Paginate(addresses, this);
            this.Template.InsertSelectionList("entity-list", addresses,
            order.BillingAddress, new FormBuilder(Forms.Admin.SELECTION), "/api/admin/shop/order/" + order.PublicId + "/billing-address/{entity.public-id}/toggle");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{order.PublicId}", order.SerialId);
            this.Navigation.Add($"billing-address/{order.PublicId}", order.PublicId, "{text.billing-address}: " + order.PublicId);

        }

        [RequestHandler("/admin/shop/orders/summary/{date}")]
        public void OrderSummary(string date) {


            // Filter
            var statusFilter = this.Params.Enums<Order.StatusType>("status", new List<Order.StatusType>() { Order.StatusType.Confirmed});
            var filterTemplates = new List<PageTemplate>();
            foreach (var status in EnumHelper.GetEnumValues<Order.StatusType>(typeof(Order.StatusType))) {
                var filterTemplate = this.Template.LoadTemplate("summary.filter");
                filterTemplate.InsertVariable("filter.key", status);
                filterTemplate.InsertVariable("filter.name", status);
                filterTemplate.InsertVariable("filter.selected", statusFilter.Contains(status));
                filterTemplate.InsertVariable("filter.class-selected", statusFilter.Contains(status)?"selected" : "");
                filterTemplates.Add(filterTemplate);
            }
            this.Template.InsertTemplates("filter", filterTemplates);
            this.Template.InsertVariable("current-selection", string.Join(",", statusFilter));

            // Orders
            List<Order> orders = GetOrdersForSummary(this.DB, date, statusFilter);

            this.Template.InsertEntityList(
                variableName: "orders",
                entities: orders,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/shop/orders/order/{entity.public-id}"
                );

            // Order Items/Products
            List<OrderSummaryItem> summaryItems = CreateOrderSummaryItems(orders);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: summaryItems,
                form: new FormBuilder(Forms.Admin.LISTING)
                );


            // Date
            this.Template.InsertVariable("date", date);

            // Orders
            this.Template.InsertVariable("order-ids", string.Join(",", orders.Select(o => o.PublicId)));


            // Printing

            // Labels Enabled
            this.Template.InsertVariable("packaging-enabled", Shop.Config.ShopPackagingEnabled ?  string.Empty : "hidden-content");

            // Print Mode
            if (Shop.Config.ShopPackagingPrintMode == "remote") {
                this.Template.InsertVariableUnsafe("print-url", "/admin/shop/packaging/labels/print-remote");
            } else {
                this.Template.InsertVariableUnsafe("print-url", "/admin/shop/packaging/labels/print");
            }

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("orders");
            this.Navigation.Add("summary", "{text.summary=Summary}: " + date);


        }

        [RequestHandler("/admin/shop/orders/order/{orderId}/overview-items")]
        public void OrderItemsDetails(string orderId) {
            var order = DB.Orders().Include(nameof(Order.OrderItems)+"."+ nameof(OrderItem.Product) + "." + nameof(Shop.Model.Product.Photos)).GetByPublicId(orderId);
            order.LoadFirstLevelNavigationReferences();

            var templates = new List<PageTemplate>();
            foreach(var item in order.OrderItems.OrderBy(oi=>oi.SKU)) {
                templates.Add(this.Template.GetItemDetailTemplate(item));
            }

            // Items
            this.Template.InsertTemplates("order-items", templates);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("orders", "{text.orders}");
            this.Navigation.Add($"order/{order.PublicId}", order.SerialId);
            this.Navigation.Add("overview-items");

        }

        public static List<Order> GetOrdersForSummary(ModelContext db, string date,IEnumerable<Order.StatusType> statusFilter) {
            // var orderDate = DateTime.Parse(date).ToUniversalTime();

            var orderDate = Core.Util.Time.ParseDate(date, DateTime.MinValue, true);
            var orderEndDate = orderDate.AddDays(1).AddTicks(-1);
            var orders = db.Orders().Include($"{nameof(Order.OrderItems)}.{nameof(OrderItem.Product)}").Where(o => statusFilter.Contains(o.Status)).ToList()
                .Where(o => o.OrderDate >= orderDate
                       && o.OrderDate <= orderEndDate).ToList();
                    
            return orders;
        }

        public static List<OrderSummaryItem> CreateOrderSummaryItems(List<Order> orders) {
            var summaryItems = new List<OrderSummaryItem>();
            var orderItems = orders.SelectMany(o => o.OrderItems);
            foreach (var orderItem in orderItems) {
                var item = summaryItems.SingleOrDefault(si => si.ProductId == orderItem.Product.Id);
                if (item == null) {
                    summaryItems.Add(new OrderSummaryItem() { Quantity = orderItem.Quantity, ProductId = orderItem.Product.Id, Title = orderItem.Title });
                } else {
                    item.Quantity += orderItem.Quantity;
                }
            }

            return summaryItems;
        }

        public class OrderSummaryItem : ModelObject{
            public int ProductId { get; set; }
            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string Title { get; set; }
            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public int Quantity { get; set; }
        }

    }
}
