
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class CatalogShopAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/catalogs")]
        public void Catalogs() {
            var entities = this.DB.CatalogGroups().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);

            this.Template.InsertCards("entity-cards", entities, "/admin/shop/catalogs/group/{entity.public-id}");
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/catalogs/group/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
        }


        [RequestHandler("/admin/shop/catalogs/create")]
        public void CatalogCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new Catalog(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/catalogs/create",
               onSuccess: "/admin/shop/catalogs/catalog/{catalog.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{groupId}/catalog/{publicId}/edit")]
        public void CatalogEdit(string groupId, string publicId) {
            this.RequireWriteARN();

            var group = DB.CatalogGroups()
                .Include($"{nameof(CatalogGroup.Catalogs)}.{nameof(Catalog.Description)}")
                .Include($"{nameof(CatalogGroup.Catalogs)}.{nameof(Catalog.Photos)}")
                .Include($"{nameof(CatalogGroup.Catalogs)}.{nameof(Catalog.Thumbnails)}")
                .GetByPublicId(groupId);
           // group.Include(nameof(group.Catalogs));
            var entity = group.Catalogs.AsQueryable().GetByPublicId(publicId);
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/catalog/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Entity
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add("group/" + groupId,  group.Name);
            this.Navigation.Add("catalog/" + publicId,  entity.Name);
            this.Navigation.Add("edit", "{text.edit}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{groupId}/catalog/{publicId}/products")]
        public void CatalogProductsEdit(string groupId, string publicId) {
            this.RequireWriteARN();

            var entity = DB.Catalogs().GetByPublicId(publicId);
            var group = DB.CatalogGroups().GetByPublicId(groupId);
            entity.Include(nameof(entity.Products));
            var products = entity.Products;

            // Entity
            Template.InsertVariables("entity", entity);

            //Products
            var entities = this.DB.Products().Include(nameof(Product.Categories)).OrderByDescending(e => e.Created);
            //entities = this.Template.Paginate(entities, this); -> sort table now on ui
            this.Template.InsertSelectionList(
                variableName: "entity-list", 
                entities: entities,
                selectedEntities: products.Cast<ModelObject>(),
                form: new FormBuilder(Forms.Admin.LISTING),
                selectionAPICall: "/api/admin/shop/catalog/" + entity.PublicId + "/products/{entity.public-id}/toggle",
                link: "/admin/shop/products/product/{entity.public-id}");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add("group/" + groupId, group.Name);
            this.Navigation.Add("catalog/" + publicId, entity.Name);
            this.Navigation.Add("products", "{text.products}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{groupId}/catalog/{publicId}")]
        public void CatalogView(string groupId, string publicId) {
           var group = DB.CatalogGroups().GetByPublicId(groupId);
            group.Include(nameof(group.Catalogs));
            var entity = group.Catalogs.AsQueryable().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            //Products in catalog
            var products = entity.Products.AsQueryable();
            products = Template.Paginate(products,this, nameof(Product.DefaultSKU), "asc");
            products.ToList().ForEach(p => p.Include("Categories"));
            this.Template.InsertEntityList("catalog.products", products, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/products/product/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add($"group/{groupId}", group.Name);
            this.Navigation.Add($"catalog/{publicId}", entity.Name);
        }

        [RequestHandler("/admin/shop/catalogs/group/{publicId}")]
        public void CatalogGroupView(string publicId) {
            var entity = DB.CatalogGroupsWithBusinesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW));

            // List Catalogs
            this.Template.InsertEntityList("entity.catalogs", entity.Catalogs.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/shop/catalogs/group/"+publicId+"/catalog/{entity.public-id}");

            // List Businesses
           this.Template.InsertEntityList("entity.businesses", entity.Businesses.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/shop/businesses/business/{entity.public-id}");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add($"group/{publicId}", entity.Name);
        }

        [RequestHandler("/admin/shop/business-catalogs")]
        public void BusinessCatalogs() {
            var entities = this.DB.CatalogGroups().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/business-catalogs/business-catalog/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("business-catalogs", "{text.business-catalogs}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{publicId}/edit")]
        public void CatalogGroupEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.CatalogGroups().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/catalog-group/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            var catalogs = entity.Catalogs;

            //Catalogs
            var entities = this.DB.Catalogs().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertSelectionList("catalogs", entities, catalogs.Cast<ModelObject>(), new FormBuilder(Forms.Admin.SELECTION), "/api/admin/shop/business-catalog/" + entity.PublicId + "/catalogs/{entity.public-id}/toggle");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add($"group/{publicId}", entity.Name);
            this.Navigation.Add("edit", "{text.edit}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{publicId}/businesses")]
        public void CatalogGroupBusinessesEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.CatalogGroups().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
         
            //Businesses
            var businesses = this.DB.CustomerBusinesses().OrderByDescending(e => e.Created);
            businesses = this.Template.Paginate(businesses, this);
            this.Template.InsertSelectionList("entity-list", businesses, entity.Businesses, new FormBuilder(Forms.Admin.SELECTION), "/api/admin/shop/business/{entity.public-id}/business-catalogs/" + entity.PublicId + "/toggle");
            
            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add($"group/{publicId}", entity.Name);
            this.Navigation.Add("businesses", "{text.businesses}");
        }

        [RequestHandler("/admin/shop/business-catalogs/create")]
        public void BusinessCatalogsCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new CatalogGroup(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/business-catalogs/create",
               onSuccess: "/admin/shop/business-catalogs/business-catalog/{business-catalog.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("business-catalogs", "{text.business-catalogs}");
            this.Navigation.Add($"create", "{text.create}");
        }


        [RequestHandler("/admin/shop/catalogs/group/{groupId}/catalog/{publicId}/purchased")]
        public void CatalogPurchasedProducts(string groupId, string publicId) {
            var group = DB.CatalogGroups()
            .Include(nameof(CatalogGroup.Catalogs) + "." + nameof(Catalog.Products) + "." + nameof(Product.Configurations))
            .GetByPublicId(groupId);

            var entity = group.Catalogs.AsQueryable().GetByPublicId(publicId);

            IQueryable<PurchasedProduct> purchasedProducts = GetPurchasedProductsForCatalog(entity);
            this.Template.InsertEntityList("purchased-configurations", purchasedProducts, new FormBuilder(Forms.Admin.LISTING), null /* "/admin/shop/products/product/{entity.public-id}"*/, true);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("catalogs", "{text.catalogs}");
            this.Navigation.Add($"group/{groupId}", group.Name);
            this.Navigation.Add($"catalog/{publicId}", entity.Name);
            this.Navigation.Add($"purchased", "{text.purchased-products}");
        }

        [RequestHandler("/admin/shop/catalogs/group/{groupId}/catalog/{publicId}/export-purchased",null, null, Verbs.Get, ContentType.Binary)]
        public void CatalogExportPurchasedProducts(string groupId, string publicId) {
            var group = DB.CatalogGroups()
            .Include(nameof(CatalogGroup.Catalogs) + "." + nameof(Catalog.Products) + "." + nameof(Product.Configurations))
            .GetByPublicId(groupId);

            var entity = group.Catalogs.AsQueryable().GetByPublicId(publicId);

            IQueryable<PurchasedProduct> purchasedProducts = GetPurchasedProductsForCatalog(entity);
            var xlsx = Core.Reporting.ExportService.ExportXLSX<PurchasedProduct>(purchasedProducts.ToList(), new FormBuilder(Forms.Admin.LISTING));
            var filename = Core.Config.ProjectID + "_" + entity.Name +  "_Purchased_Products.xlsx";
            SendBinary(xlsx, filename);

        }

        private IQueryable<PurchasedProduct> GetPurchasedProductsForCatalog(Catalog entity) {
            //Products in catalog
            var products = entity.Products.Select(p => p.Id);

            // All confirmed order items
            var confirmedOrderItems = this.DB.Orders().Include(nameof(Order.OrderItems) + "." + nameof(OrderItem.Configuration)).Where(o => o.Status >= Order.StatusType.Confirmed).
            SelectMany(o => o.OrderItems);

            // Only items in catalog
            var catalogOrderItems = confirmedOrderItems.Where(oi => products.Contains(oi.Product.Id)).GroupBy(oi => oi.Configuration);

            // Fill into structure
            var purchasedProducts = catalogOrderItems.Select(coi => new PurchasedProduct() { SKU = coi.Key.SKU, Quantity = coi.Sum(s => s.Quantity) });
            return purchasedProducts.OrderBy(c=>c.SKU);
        }
    }

    public class PurchasedProduct : ModelObject {
        [FormBuilder(Forms.Admin.LISTING)]
        public int Quantity { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        public string SKU { get; set; }
    }
}
