
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler {


    public class CatalogShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
       
        [RequestHandler("/api/admin/shop/catalogs/group/{groupId}/create")]
        public void CatalogCreate(string groupId) {
            this.RequireWriteARN();


            var group = DB.CatalogGroups().GetByPublicId(groupId);

            var catalog = new Catalog();
            catalog.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            catalog.Validate();

            group.Catalogs.Add(catalog);

            this.DB.SaveChanges();

            SendAPIMessage("create-catalog-success", new {
                Catalog = new {
                    PublicId = catalog.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/catalogs/group/{groupId}/create-weekly")]
        public void CatalogCreateWeekly(string groupId) {
            this.RequireWriteARN();

            var group = DB.CatalogGroups().Include("Catalogs").GetByPublicId(groupId);
            var latestCatalog = group.Catalogs.OrderByDescending(c => c.Schedule.End).FirstOrDefault();

            // default time zone start of week in utc
            var startDate = Time.StartOfDefaultTimezoneWeekInUtc(DateTime.UtcNow);

            int number = Params.Int("number", 1);
           
            if (latestCatalog != null && latestCatalog.Schedule?.End != null) {
                startDate = latestCatalog.Schedule.End.Value.StartOfDefaultTimezoneWeekInUtc().AddDays(7);
            }
            var catalogs = Catalog.PrepareEmptyWeeklyCatalogs(startDate, group.Name, number);
            foreach (var catalog in catalogs) {
                DB.Catalogs().Add(catalog);
                DB.SaveChanges();
                group.Catalogs.Add(catalog);
            }

            this.DB.SaveChanges();

            SendAPIMessage("create-catalogs-success");
        }

        [RequestHandler("/api/admin/shop/catalogs/group/{groupId}/create-quarterly")]
        public void CatalogCreateQuarterly(string groupId) {
            this.RequireWriteARN();

            var group = DB.CatalogGroups().Include("Catalogs").GetByPublicId(groupId);
            var startDate = Core.Util.Time.GetDefaultTimezoneToday().StartOfQuarter();
            var latestCatalog = group.Catalogs.OrderByDescending(c => c.Schedule.End).FirstOrDefault();
            
            if (latestCatalog != null && latestCatalog.Schedule?.End != null) {
                startDate = latestCatalog.Schedule.ToDefaultTimezone().End.Value.StartOfQuarter().AddMonths(3);
            }
            int number = Params.Int("number", 1);
            var catalogs = Catalog.PrepareEmptyQuarterlyCatalogs(startDate, group.Name, number);

            foreach (var catalog in catalogs) {
                DB.Catalogs().Add(catalog);
                DB.SaveChanges();
                group.Catalogs.Add(catalog);
            }

            this.DB.SaveChanges();

            SendAPIMessage("create-catalogs-success");
        }

        [RequestHandler("/api/admin/shop/catalogs/create-group")]
        public void CatalogGroupCreate() {
            this.RequireWriteARN();

            var entity = new CatalogGroup();
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.Validate();

            DB.CatalogGroups().Add(entity);

            this.DB.SaveChanges();

            SendAPIMessage("create-catalog-success", new {
                CatalogGroup = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/catalog/{publicId}/delete")]
        public void CatalogDelete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Catalogs().GetByPublicId(publicId);
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/shop/catalog-group/{publicId}/delete")]
        public void CatalogGroupDelete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.CatalogGroupsWithBusinesses().GetByPublicId(publicId);
      
            var businesses = entity.Businesses.ToList();
            foreach(var business in businesses) {
                entity.RemoveBusiness(business);
            }

            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/shop/catalog/{publicId}/edit")]
        public void CatalogEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Catalogs().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Catalog = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/catalog-group/{publicId}/edit")]
        public void CatalogGroupEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.CatalogGroups().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                CatalogGroup = new {
                    PublicId = entity.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/shop/catalog/{publicId}/products/{productId}/toggle")]
        public void CatalogToggleProduct(string publicId, string productId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Catalogs().GetByPublicId(publicId);
            var product = this.DB.Products().GetByPublicId(productId);
            var enable = this.Params.Bool("value", false);
            if (enable) {
                entity.AddProduct(product);
            } else {
                entity.RemoveProduct(product);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }
        

        [RequestHandler("/api/admin/shop/browse-catalog")]
        public void BrowseCatalog() {
            this.RequireWriteARN();

            var results = new List<Newtonsoft.Json.Linq.JObject>();
            var path = this.Params.String("path");
            var segs = path.Split('/');
            string groupName = null; if (segs.Count() > 1) groupName = segs[1];
            string catalogName = null; if (segs.Count() > 2) catalogName = segs[2];
            if (path == "/") {
                // Catalog groups
                var groups = this.DB.CatalogGroups().ToList();
                foreach(var e in groups) {
                    var obj = new Newtonsoft.Json.Linq.JObject();
                    obj["path"] = "/"+e.Name;
                    obj["name"] = e.Name;
                    obj["summary"] = e.Name;
                    results.Add(obj);
                }
            } else if(segs.Count() == 2) {
                // Catalog
                var group = this.DB.CatalogGroups()
                    .Include(nameof(CatalogGroup.Catalogs))
                    .SingleOrDefault(e => e.Name == groupName);
                var catalogs = group.Catalogs;
                foreach(var e in catalogs) {
                    var obj = new Newtonsoft.Json.Linq.JObject();
                    obj["path"] = "/"+groupName+"/"+e.Name;
                    obj["name"] = e.Name;
                    obj["summary"] = e.Name;
                    results.Add(obj);
                }
            } else if(segs.Count() == 3) {
                // Catalog
                var catalog = this.DB.Catalogs()
                    .Include(nameof(Catalog.Products))
                    .SingleOrDefault(e => e.Name == catalogName);
                var products = catalog.Products;
                foreach(var e in products) {
                    var obj = new Newtonsoft.Json.Linq.JObject();
                    var defaultConfiguration = e.GetDefaultConfiguration();
                    obj["path"] = "/"+groupName+"/"+catalogName+"/"+e.Name;
                    obj["name"] = e.Name;
                    obj["summary"] = e.Name;
                    obj["product-name"] = e.Name;
                    obj["product-public-id"] = e.PublicId;
                    obj["config-public-id"] = defaultConfiguration.PublicId;
                    obj["catalog-name"] = catalogName;
                    obj["catalog-short-url"] = catalog.ShortURL;
                    obj["catalog-public-id"] = catalog.PublicId;
                    results.Add(obj);
                }
            } else {
                throw new Exception("Invalid product catalog browse path: " + path);
            }
            
            // Return
            this.SendAPIMessage("success", new { Path = path, Pages = results });
            
        }

    }
}
