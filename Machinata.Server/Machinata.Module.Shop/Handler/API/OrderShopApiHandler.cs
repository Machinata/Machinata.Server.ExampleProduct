
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using System.Web;

namespace Machinata.Module.Shop.Handler {


    public class OrderShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        [RequestHandler("/api/admin/shop/order/{publicId}/apply-coupon")]
        public void OrderApplyCoupon(string publicId) {
            this.RequireWriteARN();

            var couponCode = this.Params.String("code");

            // Init
            var order = DB.Orders().GetByPublicId(publicId);
            order.LoadFirstLevelObjectReferences();
            order.LoadFirstLevelNavigationReferences();

            // Apply Coupon
            order.ApplyCoupon(
                db: this.DB,
                couponCode: couponCode, 
                throwException: true,
                ignoreStatus: true
                );

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("apply-success");
        }

      

        [RequestHandler("/api/admin/shop/order/{publicId}/remove-coupon")]
        public void OrderRemoveCoupon(string publicId) {
            this.RequireWriteARN();

            // Init
            var order = DB.Orders().GetByPublicId(publicId);
            order.LoadFirstLevelObjectReferences();
            order.LoadFirstLevelNavigationReferences();

            // Remove Coupon Code
            order.RemoveCoupon();

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("remove-success");
        }

     

      
        

        [RequestHandler("/api/admin/shop/order/{publicId}/order-items/add-products")]
        public void OrderAddItem(string publicId) {
            this.RequireWriteARN();

            // Init
            var order = DB.Orders().GetByPublicId(publicId);
            order.LoadFirstLevelObjectReferences();
            order.LoadFirstLevelNavigationReferences();

            // Update order items         
            UdateOrderItemsFromContext(DB, order, Context, User, ignoreStatus: true);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("add-success");
        }

        [RequestHandler("/api/admin/shop/orders/create")]
        public void OrderCreate() {
            this.RequireWriteARN();

            // Create the initial order element
            var order = new Order();
            order.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            order.Validate();

            // Dont validate for admin
            //order.ValidateDeliveryTime(order.DeliveryDate);
            //order.ValidateRestrictions(DB);

            if (order.Business == null) {
                order.InitializeOrder(this.DB, this.Context, this.Currency, this.User);
            } else {
                order.InitializeBusinessOrder(DB, this.Context, this.Currency, User);
            }
        
            this.DB.Orders().Add(order);

            this.DB.SaveChanges();
            SendAPIMessage("create-catalog-success", new {
                Order = new {
                    PublicId = order.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/delete")]
        public void OrderDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();

            // Delete order
            entity.Delete(this.DB);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-order-success");
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/cancel")]
        public void OrderCancel(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();

            // Cancel order
            entity.ChangeStatus(Order.StatusType.Canceled, this.User);

            // TODO: cancel external orders


            this.DB.SaveChanges();
            // Return
            SendAPIMessage("cancel-order-success");
        }



        [RequestHandler("/api/admin/shop/order-item/{publicId}/delete")]
        public void OrderItemDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderItems().GetByPublicId(publicId);

            entity.Include(nameof(entity.Order));
            Order order = entity.Order;
    
            order.Include(nameof(order.OrderItems));
            order.Include(nameof(order.Business));

            var orderItems = order.OrderItems.ToList();
            order.AddHistory("{text.order-history-item-deleted}: " + entity.ToString(), User);
            order.OrderItems.Remove(entity);
            DB.OrderItems().Remove(entity);
            order.UpdateOrder();
            this.DB.SaveChanges();
                     
            // Return
            SendAPIMessage("delete-order-item-success");
        }

        [RequestHandler("/api/admin/shop/orders/{publicId}/edit")]
        public void OrderEdit(string publicId) {
            this.RequireWriteARN();

            // Init entity
            var entity = this.DB.Orders().Include("OrderItems").GetByPublicId(publicId);
            entity.LoadFirstLevelObjectReferences();

            var previousDeliveryTime = entity.DeliveryDate;

            // Apply changes and validate
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();

            string editedKeyValues = entity.GetObjectValueSummary(new FormBuilder(Forms.Admin.EDIT));

            // Add history and save
            entity.AddHistory("{text.order-history-order-edited}: " + editedKeyValues, User);
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                Order = new {
                    PublicId = entity.PublicId
                }
            });
        }

      

        [RequestHandler("/api/admin/shop/order/{publicId}/next-status")]
        public void OrderNextStatus(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Orders().GetByPublicId(publicId);

            // Security
            entity.Include(nameof(entity.Business));

            var nextStatus = entity.GetNextStatus();
            if (nextStatus.HasValue) {
                entity.ChangeStatus(nextStatus.Value, User);
            } else {
                throw new BackendException("no-next-state", "There is no next state defined");
            }

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("next-status-success", new {
                Order = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/previous-status")]
        public void OrderPreviousStatus(string publicId) {
            this.RequireWriteARN();

            // Make changes a nd save
            var entity = this.DB.Orders().GetByPublicId(publicId);

            var nextStatus = entity.GetNextStatus(true);
            if (nextStatus.HasValue) {
                entity.ChangeStatus(nextStatus.Value, User);
            } else {
                throw new BackendException("no-previous-state", "There is no previous state defined");
            }

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("next-status-success", new {
                Order = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/set-status/{status}")]
        public void OrderSetStatus(string publicId, string status) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Orders().GetByPublicId(publicId);

            var nextStatus = entity.GetStatusByName(status);
            if (nextStatus.HasValue) {
                entity.ChangeStatus(nextStatus.Value, User);
            } else {
                throw new BackendException("invalid-status", "This is not a valid status.");
            }

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("next-status-success", new {
                Order = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order-item/{publicId}/edit")]
        public void OrderItemEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.OrderItems().GetByPublicId(publicId);
            entity.Include(nameof(entity.Order));
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
          

            entity.Validate();
            entity.Order.AddHistory("{text.order-history-order-item-edited}: " + $"{entity.Title}: " + entity.GetObjectValueSummary( new FormBuilder(Forms.Admin.EDIT)), User);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                OrderItem = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order-item/{publicId}/add-rejection")]
        public void OrderItemAddRejection(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.OrderItems().GetByPublicId(publicId);
            string quantityString = Params.String("quantity", "0");
            int quantity = 0;

            if (!string.IsNullOrWhiteSpace(quantityString) && quantityString.EndsWith("%")) {
                quantityString =  quantityString.Replace("%", "");
                int percent = 0;
                int.TryParse(quantityString, out percent);
                if (percent > 0) {
                    quantity = (int)System.Math.Ceiling((double)entity.Quantity * (double)percent / 100D);
                }
            }
            else {
                int.TryParse(quantityString, out quantity);
            }

            entity.Include(nameof(entity.Order));
            var order = entity.Order;

            OrderItem orderRejectionItem = order.AddRejectedItem(DB,User,entity,quantity);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("add-rejection-success", new {
                OrderItem = new {
                    PublicId = orderRejectionItem.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/create-shipment")]
        public void OrderCreateShipment(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Orders().GetByPublicId(publicId);
            entity.CreateShipment(this.DB, this.Params.String("tracking-link"),this.User);
          
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("shipment-created-success");
        }
        

        [RequestHandler("/api/admin/shop/shipment/{publicId}/edit")]
        public void OrderShipmentEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.OrderShipments().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();

            entity.Include(nameof(entity.Order));

            entity.Order.AddHistory("{text.order-history-shipment-edited}: " + entity.GetObjectValueSummary(new FormBuilder(Forms.Admin.EDIT)),this.User);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Shipment = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/shipment/{publicId}/delete")]
        public void ShipmentDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderShipments().GetByPublicId(publicId);

            entity.Include(nameof(entity.Order));

            entity.Order.AddHistory("{text.order-history-shipment-deleted}: " + entity.GetObjectValueSummary(new FormBuilder(Forms.Admin.EDIT)), User);
            DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-shipment-success");
        }

        [RequestHandler("/api/admin/shop/order/{orderId}/shipping-address/{addressId}/toggle")]
        public void OrderToggleShippingAddress(string orderId,string addressId) {
            ToggleAddress(this, orderId, addressId,false);
        }

        [RequestHandler("/api/admin/shop/order/{orderId}/billing-address/{addressId}/toggle")]
        public void OrderToggleBillingAddress(string orderId, string addressId) {
            ToggleAddress(this,orderId, addressId);
        }

        public static void ToggleAddress(APIHandler handler, string orderId, string addressId, bool billingAddress = true) {
            handler.RequireWriteARN();

            // Init
            var order = handler.DB.Orders().Include("Business.Addresses").GetByPublicId(orderId);
            Address address = null;
            if (order.Business != null) {
                address = order.Business.Addresses.AsQueryable().GetByPublicId(addressId);
            }
            else {
                address = handler.DB.Addresses().GetByPublicId(addressId);
            }
            var enable = handler.Params.Bool("value", false);
            if (enable) {
                order.LoadFirstLevelObjectReferences();
                order.AddHistory("{text.order-history-"+ (billingAddress?"billing": "shipment") +"-address-changed}: " + address.ToString(), handler.User);
              if(billingAddress) {
                    order.BillingAddress = address;
              } else {
                    order.ShippingAddress = address;
                }
                handler.DB.SaveChanges();
            }
            // Return
            handler.SendAPIMessage("toggle-success");
        }

        public static void UdateOrderItemsFromContext(ModelContext db, Order order, HttpContext context, User user, bool ignoreStatus = false) {
            var configurationIds = context.Request.Form.AllKeys.Where(k => k != "action" && k != "lang");
            var productQuantities = configurationIds.ToDictionary(k => k, v => context.Request.Form[v]);
            if (context.Request.Form["action"] == null || context.Request.Form["action"] != "update-products") throw new BackendException("update-error", "Error in update order items request.");
            foreach (var productQuantity in productQuantities) {

                // Quantity
                int quantity = 0;
                if (!string.IsNullOrEmpty(productQuantity.Value) && !Int32.TryParse(productQuantity.Value, out quantity)) {
                    throw new BackendException("update-error", "Error in update order items request.");
                }

                // Configuration (not merged)
                var configuration = db.ProductConfigurations().Include(nameof(ProductConfiguration.Product)).GetByPublicId(productQuantity.Key);

                // Order Item
                var orderItem = order.UpdateQuantityOrAdd(db, configuration, order.Currency, quantity, ignoreStatus);

                // New or existing?
                if (orderItem != null) {

                    // Save to generate id
                    if (orderItem.Context == null) {
                        db.SaveChanges(); 
                    }

                    // History
                    order.AddHistory("{text.order-history-order-item-added}: " + orderItem.ToString(), user);
                }
            }
        }

        [RequestHandler("/api/admin/shop/order/{publicId}/send-email/{type}")]
        public void SendOrderEmail(string publicId, string type) {
            this.RequireWriteARN();

            var entity = DB.Orders()
                .Include(nameof(Order.Invoice))
                .Include(nameof(Order.Shipments))
                .Include(nameof(Order.User))
                .GetByPublicId(publicId);

            if (type == "confirmation") {
                Shop.Messaging.ShopMessageCenter.SendConfirmedEmail(entity);
            }
            else if(type == "delivery") {
                Shop.Messaging.ShopMessageCenter.SendFulfillmentEmail(entity);
            }

            // Return
            SendAPIMessage("delete-shipment-success");
        }


    }
}
