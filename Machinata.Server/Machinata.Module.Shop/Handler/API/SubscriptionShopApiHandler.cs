using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Builder;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Module.Shop.Handler {

    public class SubscriptionShopApiHandler : Module.Admin.Handler.CRUDAdminAPIHandler<OrderSubscriptionItem> {

        [RequestHandler("/api/admin/shop/subscription/{publicId}/edit")]
        public void SubscriptionEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.OrderSubscriptions().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Subscription = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/subscriptions/create", AccessPolicy.PUBLIC_ARN)]
        public void SubscriptionCreate() {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptions().Create();
            entity.Context = DB;
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.User = User;
            entity.Validate();
            this.DB.OrderSubscriptions().Add(entity);

            this.DB.SaveChanges();

            SendAPIMessage("create-success", new {
                Subscription = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/subscription/{publicId}/delete")]
        public void SubscriptionDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptions().GetByPublicId(publicId);
            entity.Include(nameof(entity.Orders));
            entity.Include(nameof(entity.SubscriptionItems));

            // Delete Items
            foreach (var items in entity.SubscriptionItems.ToList()) {
                DB.DeleteEntity(items); // Why is CascadeDelete not working?
            }

            // Remove orders
            foreach (var order in entity.Orders.ToList()) {
                entity.Orders.Remove(order);
            }
            DB.OrderSubscriptions().Remove(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }


        [RequestHandler("/api/admin/shop/subscription-item/{publicId}/delete")]
        public void SubscriptionItemDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderSubscriptionItems().GetByPublicId(publicId);

            entity.Include(nameof(entity.Categories));

            // remove the the categories first
            foreach(var category in entity.Categories.ToList()) {
                entity.Categories.Remove(category);
            }

            DB.OrderSubscriptionItems().Remove(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/shop/subscription/{subscriptionId}/items/create", AccessPolicy.PUBLIC_ARN)]
        public void SubscriptionItemCreate(string subscriptionId) {
            this.RequireWriteARN();
            IQueryable<Category> categories = GetCategoriesFromContext();

            var subscription = DB.OrderSubscriptions().GetByPublicId(subscriptionId);

            var entity = DB.OrderSubscriptionItems().Create();
                     
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));

            foreach (var category in categories) {
                entity.Categories.Add(category);
            }

            entity.Validate();
            subscription.SubscriptionItems.Add(entity);
            DB.SaveChanges();

            SendAPIMessage("create-success", new {
                SubscriptionItem = new {
                    PublicId = entity.PublicId
                }
            });

        }

        [RequestHandler("/api/admin/shop/subscription/{subscriptionId}/create-orders", AccessPolicy.PUBLIC_ARN)]
        public void SubscriptionCreateOrders(string subscriptionId) {
            this.RequireWriteARN();
         

            var entity = DB.OrderSubscriptions().GetByPublicId(subscriptionId);

            var dates = entity.GetPossibleDeliveryDatesForRange(System.DateTime.UtcNow, System.DateTime.UtcNow.AddDays(14)); //TODO

            entity.GeneratedOrdersForDates(DB, dates.Select(osd => osd.Date));
            
            DB.SaveChanges();

            SendAPIMessage("create-success", new {
                Subscription = new {
                    PublicId = entity.PublicId
                }
            });

        }

        private IQueryable<Category> GetCategoriesFromContext() {
            var mainCategories = Params.StringArray("main-categories", new string[] { });
            var subCategories = Params.StringArray("sub-categories", new string[] { });

            var categories = DB.Categories().Where(c => mainCategories.Contains(c.Name.ToLower()) || subCategories.Contains(c.Name.ToLower()));
            return categories;
        }

        [RequestHandler("/api/admin/shop/subscription-item/{publicId}/edit")]
        public void SubscriptionItemEdit(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.OrderSubscriptionItems().GetByPublicId(publicId);
            entity.Include(nameof(entity.Categories));

            var categoriesFromContext = GetCategoriesFromContext();

            // Clear categories -> apply from context
            entity.Categories.Clear();

            foreach (var category in GetCategoriesFromContext()) {
                entity.Categories.Add(category);
            }

            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                SubscriptionItem = new {
                    PublicId = entity.PublicId
                }
            });
        }


        //[RequestHandler("/api/admin/shop/subscription-item/{publicId}/categories/{categoryId}/toggle")]
        //public void SubscriptionItemToggleCategory(string publicId, string categoryId) {
        //    this.RequireWriteARN();

        //    // Init
        //    var entity = this.DB.OrderSubscriptionItems().GetByPublicId(publicId);
        //    var category = this.DB.Categories().GetByPublicId(categoryId);
        //    entity.Include(nameof(entity.Categories));
        //    var enable = this.Params.Bool("value", false);
        //    if (enable) {
        //        entity.AddCategory(category);
        //    } else {
        //        entity.RemoveCategory(category);
        //    }
        //    this.DB.SaveChanges();
        //    // Return
        //    SendAPIMessage("toggle-success");
        //}
    }
}
