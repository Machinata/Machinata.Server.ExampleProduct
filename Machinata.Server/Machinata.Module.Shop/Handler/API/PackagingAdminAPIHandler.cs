using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;

using Machinata.Module.Admin.Handler;
using System.Collections.Generic;
using Machinata.Module.Shop.Model;
using System;

namespace Machinata.Module.Shop.Handler {


    public class PackagingAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<PackagingDesign> {

        #region Handler Policies
        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }
        #endregion
        

        [RequestHandler("/api/admin/shop/packaging/designs/create")]
        public void CreateDesign() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/shop/packaging/design/{publicId}/delete")]
        public void DeleteDesign(string publicId) {
            CRUDDelete(publicId);
        }
        [RequestHandler("/api/admin/shop/packaging/design/{publicId}/edit")]
        public void EditDesign(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/shop/packaging/labels/print")]
        public void PrintLabels() {

            // Jobs: id_quantiy, id_source
            const string source_suffix = "_source";
            const string quantity_suffix = "_quantity";
            const string preview_suffix = "_preview";
            const string title_suffix = "_title";

            var target = this.Params.GetSelectedItem(Core.TaskManager.RemoteTaskManagerAPI.GetRemoteTaskTargetsConfig().Select(t => t.Name));
            var ids = this.Context.Request.Form.AllKeys.Where(k => k.EndsWith(source_suffix, StringComparison.Ordinal)).Select(k => k.Replace(source_suffix, string.Empty));
            foreach(var id in ids) {
                var entries = this.Context.Request.Form.AllKeys.Where(k => k.StartsWith(id, StringComparison.Ordinal));
                var source = this.Context.Request.Form[entries.First(e => e.EndsWith(source_suffix, StringComparison.Ordinal))];
                var title = this.Context.Request.Form[entries.First(e => e.EndsWith(title_suffix, StringComparison.Ordinal))];
                var previewKey = entries.FirstOrDefault(e => e.EndsWith(preview_suffix, StringComparison.Ordinal));
                string preview = null;
                if(previewKey != null) preview = this.Context.Request.Form[previewKey];
                int quantity = 1;
                var quantityKey = entries.First(e => e.EndsWith(quantity_suffix, StringComparison.Ordinal));
                int.TryParse(this.Context.Request.Form[quantityKey], out quantity);

                // Dont create job if nothing to print
                if (quantity >= 1) {
                    Core.Util.Printing.PrintJob.CreatePrintJob(this.DB, source, preview, title, quantity, target, true);
                }

            }

            this.DB.SaveChanges();


            SendAPIMessage("success");
           
        }



    
    }
}
