
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler {


    public class BusinessShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        private Business GetBusinessFromAddress(Address entity) {
            return DB.Businesses().Include("Addresses").FirstOrDefault(b => b.Addresses.Select(a => a.Id).Contains(entity.Id));
        }
        

        [RequestHandler("/api/admin/shop/business/{publicId}/business-catalogs/{catalogGroupId}/toggle")]
        public void BusinessToggleGroupCatalog(string publicId, string catalogGroupId) {
            this.RequireWriteARN();

            // Init
            var business = this.DB.Businesses().GetByPublicId(publicId);
            var catalogGroup = this.DB.CatalogGroups().GetByPublicId(catalogGroupId);
            var enable = this.Params.Bool("value", false);
            if (enable) {
                catalogGroup.AddBusiness(business);
            } else {
                catalogGroup.RemoveBusiness(business);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }
        

        [RequestHandler("/api/admin/shop/businesses/generate-invoices")]
        public void BusinessesGenerateInvoices() {
            this.RequireWriteARN();


            var allOpenOrders = Module.Shop.Logic.InvoiceGeneration.GetOrdersToGenerateInvoices(DB);
            var orderIds = Params.StringArray("selected",null);
            if (orderIds == null) throw new BackendException("nothing-selected","Nothing was selected.");
            var orders = DB.Orders().GetByPublicIds(orderIds);

            // check all orders are open
            if (!orders.All(oo => allOpenOrders.Contains(oo))) {
                throw new BackendException("order-wrong-status", "Not all orders are ready for invoice creation.");
            }

            // Create all the invoices and save
            var invoices = Module.Shop.Logic.InvoiceGeneration.GenerateInvoicesForOpenOrders(DB, orders.ToList());
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("invoices-created-success");
        }


    }
}
