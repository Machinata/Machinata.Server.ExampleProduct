using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Builder;
using System.Collections.Generic;

namespace Machinata.Module.Shop.Handler {
    
    public class RestrictionApiHander : Module.Admin.Handler.CRUDAdminAPIHandler<OrderRestriction> {


        [RequestHandler("/api/admin/shop/order-restrictions/create")]
        public void Create() {
            CRUDCreate();
        }
        
        [RequestHandler("/api/admin/shop/order-restriction/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/shop/order-restriction/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
   
         [RequestHandler("/api/admin/shop/order-restriction/{publicId}/toggle-business/{toggleId}")]
        public void BusinessToggle(string publicId, string toggleId) {
            // CRUDToggleSelection<Business>(publicId, toggleId, nameof(OrderRestriction.Businesses));
            var entity = this.DB.OrderRestrictions().Include(nameof(OrderRestriction.BusinessOrderRestrictions)+"." + nameof(BusinessOrderRestriction.Business)).GetByPublicId(publicId);
            var business = this.DB.Businesses().GetByPublicId(toggleId);
            var enable = this.Params.Bool("value", false);

            if (enable) {
                entity.Add(business);
            } else {
                entity.Remove(this.DB, business);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }


    }
}
