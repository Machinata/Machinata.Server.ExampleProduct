
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Web;
using Machinata.Module.Shop.Handler;
using Machinata.Module.Finance.Payment;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Machinata.Module.Shop.Logic;
using Newtonsoft.Json.Linq;
using static Machinata.Module.Shop.Model.Order;

namespace  Machinata.Module.Shop.Handler.API.Frontend {


    public abstract class PublicOrderAPIHandler : APIHandler {

        protected void AddToCart(string productId, string configurationId, int quantity, Func<Shop.Model.Product,ProductConfiguration,string> getTitle = null) {
            // Order
            Order order = GetOrderContextOrNew(this);

            // Redraft
            order.Redraft();

            // Product
            var product = this.DB.Products()
                .Include(nameof(Shop.Model.Product.Configurations))
                .Include(nameof(Shop.Model.Product.Title))
                .Include(nameof(Shop.Model.Product.Catalogs))
                .GetByPublicId(productId);

            // Configuration
            var configuration = product.Configurations.AsQueryable().GetByPublicId(configurationId);

            // default title
            var title = $"{product.GetTitle(this.Language)} {configuration.Title}";

            // custom title
            if (getTitle != null) {
                title = getTitle(product, configuration);
            }

            // Item
            var orderItem = order.AddOrderItem(this.DB, product, configuration, this.Currency, quantity, title);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("add-success", new { Order = order.GetJsonForCart() });
        }

        protected void RemoveFromCart(string orderItemId) {

            // Order
            var orders = this.DB.Orders().Include(nameof(Order.OrderItems));
            Order order = GetOrderFromContext(this, orders);

            // Redraft
            order.Redraft();

            // Remove item
            order.RemoveOrderItem(orderItemId);

            // If now empty
            if (order.OrderItems.Count < 1) {
                SetCartEmpty(this, false);
            }

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("remove-success", new { Order = order.GetJsonForCart() });
        }

        protected void UpdateOrderItemQuantity(string orderItemId, int newQuantity) {

            // Order
            var orders = this.DB.Orders().Include(nameof(Order.OrderItems));
            Order order = GetOrderFromContext(this, orders);

            // Redraft
            order.Redraft();

            // Item
            var item = order.OrderItems.AsQueryable().GetByPublicId(orderItemId);

            // Update
            order.UpdateOrderItemQuantity(item, newQuantity);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("update-quantity-success", new { Order = order.GetJsonForCart() });
        }

        protected void UpdateAddresses() {

            // Order
            var orders = this.DB.Orders()
                .Include(nameof(Order.BillingAddress))
                .Include(nameof(Order.ShippingAddress));
            Order order = GetOrderFromContext(this, orders);

            // Redraft
            order.Redraft();

            var sameAddress = this.Params.String("shipping_billing_same") == "on";

            if (order.ShippingAddress == null) {
                order.ShippingAddress = new Address();
            }

            // Billing?
            if (!sameAddress ) {
                if (order.BillingAddress == null || order.BillingAddress == order.ShippingAddress) {
                    order.BillingAddress = new Address();
                }
            } else if (sameAddress) {
                if (order.BillingAddress != null && order.ShippingAddress != order.BillingAddress) {
                    this.DB.Addresses().Remove(order.BillingAddress);
                    order.BillingAddress = null;
                }
                order.BillingAddress = order.ShippingAddress;
            }

            order.ShippingAddress.Name = this.Params.String("shipping_name");
            order.ShippingAddress.Company = this.Params.String("shipping_company");
            order.ShippingAddress.Address1 = this.Params.String("shipping_address1");
            order.ShippingAddress.Address2 = this.Params.String("shipping_address2");
            order.ShippingAddress.ZIP = this.Params.String("shipping_zip");
            order.ShippingAddress.City = this.Params.String("shipping_city");
            order.ShippingAddress.CountryCode = this.Params.String("shipping_country");
            order.ShippingAddress.Phone = this.Params.String("shipping_phone");
            order.ShippingAddress.Email = this.User?.Email;
            var shippingEmailAddress = this.Params.String("shipping_email");
            if (string.IsNullOrEmpty(order.ShippingAddress.Email) && string.IsNullOrEmpty(shippingEmailAddress) == false) {
                order.ShippingAddress.Email = shippingEmailAddress;
            }
            
            if (!sameAddress) {
                order.BillingAddress.Name = this.Params.String("billing_name");
                order.BillingAddress.Company = this.Params.String("billing_company");
                order.BillingAddress.Address1 = this.Params.String("billing_address1");
                order.BillingAddress.Address2 = this.Params.String("billing_address2");
                order.BillingAddress.ZIP = this.Params.String("billing_zip");
                order.BillingAddress.City = this.Params.String("billing_city");
                order.BillingAddress.CountryCode = this.Params.String("billing_country");
                order.BillingAddress.Phone = this.Params.String("billing_phone");
                order.BillingAddress.Email = this.User?.Email;
                var billingEmailAddress = this.Params.String("billing_email");
                if (string.IsNullOrEmpty(order.BillingAddress.Email) && string.IsNullOrEmpty(billingEmailAddress) == false) {
                    order.BillingAddress.Email = billingEmailAddress;
                }
            }

            // Name of User
            if (this.User != null && string.IsNullOrEmpty(this.User.Name)) {
                this.User.Name = order.BillingAddress.Name;
            }

            // General, Guest Email Address
            if (string.IsNullOrEmpty(order.BillingAddress.Email) == true) {
                var orderEmailAddress = this.Params.String("order_email");
                if (string.IsNullOrEmpty(orderEmailAddress) == false) {
                    order.BillingAddress.Email = orderEmailAddress;
                }
            }

            // Update order
            order.UpdateOrder();

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("update-addresses-success", new { Order = order.GetJsonForCart() });
        }
        
        public static Order GetOrderContextOrNew(Core.Handler.Handler handler, DbQuery<Order> orders = null) {
            Order order = GetOrderFromContext(handler, orders, false);
            if (order == null) {
                order = new Order();
                order.User = handler.User;
                handler.DB.Orders().Add(order);
                order.InitializeOrder(handler.DB, handler.Context, handler.Currency, handler.User);
                order.Validate();
                handler.DB.SaveChanges();
            }

            return order;
        }

        /// <summary>
        /// Gets the order representing the cart  from context.(cookie) or via the users last orders
        /// All statuses which are not yet confirmed
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="orders">The orders.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <returns></returns>
        public static Order GetOrderFromContext(Core.Handler.Handler handler, DbQuery<Order> orders = null, bool throwException = true) {
            var orderHash = handler.Context?.Request?.Cookies["OrderHash"]?.Value;
            if (orders == null) {
                orders = handler.DB.Orders();
            }
            var incompleteOrders = orders.Where(o => o.Status == Order.StatusType.Draft
                                         || o.Status == Order.StatusType.Placed
                                         || o.Status == Order.StatusType.Paid
                                     );

            Order order = null;
           
            // By cookie
            if (orderHash != null) {
                order = incompleteOrders.GetByHash(orderHash, false);
            }



            //// By user
            //if (handler.User != null && order == null) {
            //   orders.ToList().ForEach(o => o.Include(nameof(Order.User)));
            //   order = incompleteOrders.Where(o=> o.User.Id == handler.User.Id).OrderByDescending(o => o.Id).FirstOrDefault();
            //}

            // Exception
            if (order == null) {
                SetCartEmpty(handler, throwException);
            } else {
                HandleCouponCookies(handler, order);
            }
            return order;
        }

        private static void HandleCouponCookies(Core.Handler.Handler handler, Order order) {
            // Remove Expired coupons
            if (order.IsStatusBefore(StatusType.Placed)) {
                order.Include(nameof(Order.OrderCoupons));
                // Auto remove expired coupons
                if (order.OrderCoupons.Any(c => c.IsCouponExpired())) {
                    order.RemoveCoupon();
                    handler.DB.SaveChanges();
                }
            }

            // Consume coupon cookie, invalidate afterwards
            if (handler.Context.Request.Cookies["OrderCoupon"] != null) {
                order.Include(nameof(Order.OrderCoupons));
                if (order.OrderCoupon == null) {
                    bool valid = order.ApplyCoupon(handler.DB, handler.Context.Request.Cookies["OrderCoupon"].Value, false);
                    if (valid) {
                        handler.DB.SaveChanges();
                    }
                }
                HTTP.InvalidateCookie(handler.Context, "OrderCoupon");
            }
        }

        private static void SetCartEmpty(Core.Handler.Handler handler, bool throwException) {
            // reset cookie
            Core.Util.HTTP.InvalidateCookie(handler.Context, "OrderHash");
            if (throwException) {
                throw new BackendException("cart-empty", "Your cart is empty");
            }
        }


        protected void OrderApplyCoupon(string couponCode) {
            // Init
            var order = GetOrderFromContext(this, null);
            order.LoadFirstLevelObjectReferences();
            order.LoadFirstLevelNavigationReferences();
            
            // Redraft
            order.Redraft();

            // Apply Coupon
            order.ApplyCoupon(this.DB, couponCode);
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("apply-success", new { Order = order.GetJsonForCart() });
        }


        public void OrderRemoveCoupon() {
            // Init
            var order = GetOrderFromContext(this, null);
            order.LoadFirstLevelObjectReferences();
            order.LoadFirstLevelNavigationReferences();

            // Redraft
            order.Redraft();

            // Remove Coupon Code
            order.RemoveCoupon();

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("remove-success", new { Order = order.GetJsonForCart() });
        }

        /// <summary>
        /// Sets the AutomaticCoupon in the cookie from the Dynamic config
        /// Only if valid and none is already set
        /// </summary>
        /// <param name="handler">The handler.</param>
        public static void ApplyAutomaticCoupon(PageTemplateHandler handler) {

            // Only AutoCoupon configured
            if (string.IsNullOrEmpty(Core.Config.Dynamic.String("AutomaticCoupon", false)) == false) {

                // Nothing if already has coupon
                if (handler.Context.Request.Cookies["OrderCoupon"] != null) {
                    return;
                }

                var coupon = OrderCoupon.GetCoupon(handler.DB, Core.Config.Dynamic.String("AutomaticCoupon", false));
                // Dont do anything if not found or expired
                if (coupon == null || coupon.IsCouponExpired()) {
                    return;
                }

                //Set if none available
                var expiration = coupon.Expires.HasValue ? coupon.Expires.Value : DateTime.UtcNow.AddDays(30);
                Core.Util.HTTP.SetCookie(handler.Context, "OrderCoupon", coupon.Code, expiration);

            }
        }



        /// <summary>
        /// Gets the sitewide coupon from the OrderCoupon-Cookie or Cart-Coupon if compatible
        /// Only if no existing cart
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public static OrderCoupon GetSitewideCoupon(Core.Handler.Handler handler, ModelContext db) {

            // From cart 1st prio 
            var coupon  = GetCouponFromCart(handler);

            // Try from cookie
            if (coupon == null) {
                coupon = GetCouponFromCookie(handler.Context, db);
            }

            // Dont do anything if not found or expired
            if (coupon == null || coupon.IsCouponExpired()) {
                return null;
            }

            // Compatibilty
            //if (coupon.CategoryFilter != null) {
            //    return null;
            //}
            if (coupon.ProductFilter != null) {
                return null;
            }
            if (coupon.NumberOfItems != null) {
                return null;
            }
            if (coupon.OrderCouponType != OrderCouponType.Percentage) {
                return null;
            }
            if (coupon.CountryFilter != null) {
                return null;
            }
            if (coupon.Limit != null) {
                return null;
            }
            if (coupon.MinimumOrderAmount > 0) {
                return null;
            }
            if (coupon.MinimumOrderItems > 0) {
                return null;
            }

            return coupon;

        }

        private static OrderCoupon GetCouponFromCookie(HttpContext context, ModelContext db) {
            OrderCoupon coupon = null;
            if (string.IsNullOrEmpty(context.Request.Cookies["OrderCoupon"]?.Value) == false ) {
                coupon = OrderCoupon.GetCoupon(db, context.Request.Cookies["OrderCoupon"].Value);
            }
            return coupon;
        }

        private static OrderCoupon GetCouponFromCart(Core.Handler.Handler handler) {
            var cart = GetOrderFromContext(handler, handler.DB.Orders().Include(nameof(Order.OrderCoupons)), false);
            if (cart != null && cart.OrderCoupon != null) {
                return cart.OrderCoupon;
            }
            return null;
        }
    }
}
