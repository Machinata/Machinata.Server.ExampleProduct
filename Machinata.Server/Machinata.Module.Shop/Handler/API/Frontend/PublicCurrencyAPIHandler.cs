
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Web;
using Machinata.Module.Shop.Handler;
using Machinata.Module.Finance.Payment;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Machinata.Module.Shop.Logic;
using Newtonsoft.Json.Linq;
using static Machinata.Module.Shop.Model.Order;

namespace  Machinata.Module.Shop.Handler.API.Frontend {


    public class PublicCurrencyAPIHandler : APIHandler {
        
        [RequestHandler("/api/shop/currency/convert", AccessPolicy.PUBLIC_ARN)]
        public void Convert() {
            var sourceCurrency = this.Params.String("source-currency");
            var sourceAmount = this.Params.Decimal("source-amount", 0);
            var targetCurrency = this.Params.String("target-currency");
            var rounded = this.Params.Bool("target-rounded", false);

            var price = new Price(sourceAmount, sourceCurrency);
            Price converted;
            if(rounded) converted = price.ConvertToRoundedCurrency(targetCurrency);
            else converted = price.ConvertToCurrency(targetCurrency);

            SendAPIMessage("currency", new {
                ConvertedAmount = converted.Value,
                ConvertedCurrency = converted.Currency,
                ConvertedIsRounded = rounded,
                Converted = converted.ToString(),
                Original = price.ToString()
            });
        }

    }
}
