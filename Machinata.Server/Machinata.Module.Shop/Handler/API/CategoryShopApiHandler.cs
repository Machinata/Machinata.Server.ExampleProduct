using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Builder;
using System.Collections.Generic;

namespace Machinata.Module.Shop.Handler {
    
    public class CategoryShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
      
        [RequestHandler("/api/admin/shop/category/{publicId}/edit")]
        public void CategoryEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var category = this.DB.Categories().GetByPublicId(publicId);
            category.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            category.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Category = new {
                    PublicId = category.PublicId
                }
            });
        }
        
        [RequestHandler("/api/admin/shop/categories/create")]
        public void CategoryCreate() {
            this.RequireWriteARN();

            var entity = DB.Categories().Create();
            entity.Context = DB;
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.Validate();
            this.DB.Categories().Add(entity);

            this.DB.SaveChanges();

            SendAPIMessage("create-category-success", new {
                Category = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/category/{publicId}/delete")]
        public void CategoryDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Categories().GetByPublicId(publicId);
            DB.Categories().Remove(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-category-success");
        }

    }
}
