
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler {


    public class ShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }

        #endregion
        
        //TODO:@micha Public properties/helpers for 'cart'

    }
}
