using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System.Linq;
using Machinata.Core.Builder;
using System.Collections.Generic;

namespace Machinata.Module.Shop.Messaging {
    public class OrderEmailTemplate : EmailTemplate {

        public Order Order { get; private set; }

      

        public OrderEmailTemplate(ModelContext db, Order order, string templateName, string language = null, string extension = ".htm") : base(db, templateName, extension, language) {
            Order = order;
        }

        public override void Compile() {
            // Order
            this.Order.LoadFirstLevelNavigationReferences();
            this.Order.LoadFirstLevelObjectReferences();

            // Hash or PublicId ? -> must be before InsertVariables
            this.InsertVariable("order-id", Config.OrderPublicId);

            this.InsertVariables("entity", Order);

            // CMS content
            this.InsertContent(
                variableName: "cms-content",
                cmsPath: "/General/Order/EmailText",
                throw404IfNotExists: false, db: _db
            );

            // Contact
            this.InsertVariable("order.contact", this.Order.ShippingAddress.Name);

            // Order url
            this.InsertVariable("order-url", Core.Localization.Text.GetTranslatedTextByIdForLanguage(Config.OrderPublicPath, this.Language, Config.OrderPublicPath).TrimStart('/'));
            
            // Order Items
            this.InsertList(
             variableName: "line-items",
             title: "{text.order-items}",
             entities: this.Order.GetLineItemGroups().FirstOrDefault().LineItems.AsQueryable(),
             form: new FormBuilder(Forms.Admin.PDF),
             totalForm: new FormBuilder(Forms.EMPTY)
                            .Include(nameof(OrderItem.CustomerTotal)));
                            

            // Origin destination address
            this.InsertVariables("entity.origin", this.Order.OriginAddress);
            this.InsertVariables("entity.destination", this.Order.ShippingAddress);

            // Coupons
            this.InsertList("coupons", "{text.coupons}", this.Order.OrderCoupons.AsQueryable());

            // Shipments
            this.InsertList("shipments", "{text.shipments}", this.Order.Shipments.AsQueryable());

            // Shipment
            var shipment = this.Order.GetShipmentForOrder();
            var date = this.Order.DeliveryDate.HasValue ? this.Order.DeliveryDate : this.Order.ShippedDate;
            this.InsertVariable("entity.delivery-shipped-date", date?.ToString(Core.Config.DateFormat));
          
            // Tracking Link
            this.InsertVariableUnsafe("shipment.tracking-link", shipment?.TrackingLink);
            this.InsertVariable("shipment.has-tracking-link", !string.IsNullOrEmpty(shipment?.TrackingLink));
                
            // Billing address
            bool sameAddress = true;
            if (this.Order.BillingAddress != null && this.Order.BillingAddress != this.Order.ShippingAddress) {
                this.InsertAddress("billing-address", "{text.order.billing-address}",Order.BillingAddress);
                sameAddress = false;
            }
            else {
                InsertAddress("billing-address", "{text.order.billing-address}", null);
            }

            // Addresses
            base.InsertAddress("shipping-address",sameAddress ? "{text.order-shipping-and-billing}": "{text.order.shipping-address}",this.Order.ShippingAddress );
            
            // Invoice
            var invoices = new List<ModelObject>();
            if (this.Order.Invoice != null) {
                invoices.Add(this.Order.Invoice);
            }
            base.InsertList("invoices", "{text.invoices}", invoices.AsQueryable());

            // Statuses
            base.InsertList("statuses", "{text.order-history}", this.Order.OrderHistories.OrderByDescending(os => os.Id).AsQueryable());
            base.InsertVariable("order-shipments.count", this.Order.Shipments.Count);
            base.InsertVariable("shipment.public-id", this.Order.Shipments.Any() ? this.Order.Shipments.First().PublicId : string.Empty);

            // Subject
            var subjectTemplate = new PageTemplate(Config.OrderEmailSubject);
            subjectTemplate.Language = this.Language;
            subjectTemplate.DiscoverVariables();
            subjectTemplate.InsertTextVariables();
            subjectTemplate.InsertVariables("order", Order);
            this.Subject = subjectTemplate.Content;

            base.Compile();
        }

     


      


      
    }
}
