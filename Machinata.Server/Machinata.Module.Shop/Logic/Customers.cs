using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Machinata.Module.Shop.Logic {
    public class Customers {

        /// <summary>
        /// Gets the users which have a confirmed order
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public static IQueryable<User> GetUsersWithConfirmedOrders(ModelContext db) {
            return GetConfirmedOrdersWithDates(db).Where(o => o.User != null).Select(o => o.User).Distinct();
        }

        public static IQueryable<Order> GetConfirmedOrdersWithDates(ModelContext db,  params string[] includePaths ) {
            var orders = db.Orders().Include(nameof(Order.User));
            foreach(var includePath in includePaths) {
                orders = orders.Include(includePath);
            }
            return orders.ConfirmedWithDates();
        }
    }
}
