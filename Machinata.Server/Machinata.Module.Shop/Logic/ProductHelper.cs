using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Shop.Logic;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.Model {
    public partial class Product  {

        public static IList<ProductConfiguration> GetMergedBusinessConfigurationsForProducts(Business business, IEnumerable<Product> products) {

            IList<ProductConfiguration> configurations = new List<ProductConfiguration>();

            foreach (var product in products) {
                ProductConfiguration configurationForBusiness = product.GetConfigurationForBusiness(business);
                configurations.Add(configurationForBusiness);
            }

            return configurations;
        }

       
    }
}
