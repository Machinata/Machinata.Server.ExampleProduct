using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;

using Machinata.Module.Finance.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using Machinata.Module.Shop.Model;
using Machinata.Module.Finance.Payment;

namespace Machinata.Module.Shop.Logic {

    public class InvoiceGeneration {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

      
        /// <summary>
        /// Generates the invoices for open orders (payment after invoice orders).
        /// </summary>
        /// <returns>Invoices</returns>
        /// <exception cref="BackendException">
        /// invalid-payment-type;This order isn't intented to be paid by invoice.
        /// or
        /// invalid-order-status;This order is not yet fulfilled.
        /// </exception>
        public static IEnumerable<Invoice> GenerateInvoicesForOpenOrders(ModelContext db, List<Order> orders) {
            _logger.Info("Generating Invoices");

            // Get all orders grouped by business
            foreach(var order in orders) {
                if (order.Context!= null) {
                    order.Include(nameof(order.Business));
                    order.Include(nameof(order.User));
                }
            }
            var businessOrders = orders.GroupBy(o => o.Business);

            // Sender business
            var sender = db.OwnerBusiness().Include<Business>("Addresses");
           // var dueDate = Core.Util.Time.GetDefaultTimezoneToday().AddDays(Config.ShopInvoiceOrderDueDays);

            // Foreach business create an invoice for the open orders
            var invoices = new List<Invoice>();

            // Check order payment types 
            if (orders.Any(o => o.PaymentPointOfTime != Finance.Payment.PaymentTime.After)) {
                throw new BackendException("invalid-payment-type", "This order isn't intented to be paid by invoice.");
            }

            // Check order payment statuses 
            if (orders.Any(o => o.Status != Order.StatusType.Fulfilled)) {
                throw new BackendException("invalid-order-status", "This order is not yet fulfilled.");
            }

            foreach (var group in businessOrders) {
                var business = group.Key;
                var ordersPerBusiness = group.Select(g => g).ToList();
                var currencies = ordersPerBusiness.Select(o => o.Currency).Distinct();
                if (currencies.Count()!=1) {
                    throw new BackendException("invoice-error", "Cannot create invoice with mixed currencies");
                }  
                var invoice = Finance.Invoices.InvoiceGeneration.CreateInvoice(
                    sender: sender,
                    billingAddress: business.GetBillingAddressForBusiness(),
                    invoiceables: ordersPerBusiness,
                    paymentMethod : PaymentMethod.Invoice, 
                    dueDate: null,
                    user: null, 
                    business: business,
                    currency: currencies.First()
                    );
                invoice.Context = db;
                invoices.Add(invoice);
            }
            return invoices;
        }


        public static IQueryable<Order> GetOrderForInvoicesLastMonth(ModelContext db) {
            var ordersWithouInvoices = GetOrdersToGenerateInvoices(db);
            var today = Time.GetDefaultTimezoneToday();
            var month = new DateTime(today.Year, today.Month, 1);
            var lastDate = month.AddDays(-1).EndOfDay().ToUniversalTime();

            return ordersWithouInvoices.Where(o => o.DeliveryDate <= lastDate);
        }


        /// <summary>
        /// Gets the orders to generate invoices from. Only for Orders with PaymentTime=After and PaymentMehod=voice
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public static IQueryable<Order> GetOrdersToGenerateInvoices(ModelContext db) {
            return db.Orders().Include("Invoice").Include("Business").Where(o => o.PaymentPointOfTime == Finance.Payment.PaymentTime.After &&
                                                                       o.PaymentMethod == Finance.Payment.PaymentMethod.Invoice &&
                                                                       o.Status == Order.StatusType.Fulfilled &&
                                                                       o.Invoice == null);
        }

    }
}
