using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System.Collections.Generic;
using static Machinata.Module.Shop.Model.Order;
using System;
using System.Linq;
using Machinata.Core.Builder;

namespace Machinata.Module.Shop.Logic {

    public class OrderProcessLogic {

        public virtual LinkedList<StatusType> StatusFlow { get; }
        protected Order _order;

        #region Constructors /////////////////////////////////////////////////////////////////////

        public OrderProcessLogic(Order order) {
            _order = order;
        }

        public static OrderProcessLogic CreateOrderProcessLogic(Order order, Finance.Payment.PaymentTime paymentTime) {
            if (paymentTime == Finance.Payment.PaymentTime.After) {
                return new PayAfterStrategy(order);
            } else if (paymentTime == Finance.Payment.PaymentTime.Before) {
                return new PaybeforeStrategy(order);
            }
            throw new BackendException("order-process-logic-error", "No OrderProcessLogic found");
        }

        #endregion

        public bool ValidateNextStatus(Order.StatusType status, bool throwException) {
            var nextStatus = _order.GetNextStatus();
            if (!nextStatus.HasValue || nextStatus.Value != status) {
                if (throwException) {
                    throw new BackendException("wrong-status", "Wrong status: " + status);
                }
                return false;
            }
            return true;
        }

        public bool HandleError(bool throwException, string code, string message) {
            if (throwException) {
                throw new BackendException(code, message);
            } else {
                return false;
            }
        }

        public bool AllowDelete() {
            return (_order.Status == StatusType.Draft);
        }

        public Order.StatusType Process(Order.StatusType newStatus, bool validate, User user) {
            // Process depending on implementation
            var resultStatus = this.ProcessStatus(newStatus, validate);

            // Common post process code
            this.PostProcess(newStatus,user);

            return resultStatus;
        }

        protected virtual Order.StatusType ProcessStatus(Order.StatusType newStatus, bool validate) {
            throw new NotImplementedException("The concrete process strategy has to implement this method");
        }

        private void PostProcess(Order.StatusType newStatus, User user) {
            if (newStatus == StatusType.Confirmed) {
                // Stock Quantity decrease
                if (Config.ShopAutomaticallyDecreaseStockQuantity) {
                    if (_order.Context != null) {
                        _order.Include(nameof(Order.OrderItems));
                    }
                    foreach (var orderItem in _order.OrderItems) {
                        if (orderItem.Context != null) {
                            orderItem.Include(nameof(OrderItem.Configuration));
                        }
                        if (orderItem.Configuration.StockQuantity.HasValue) {
                            orderItem.Configuration.StockQuantity -= orderItem.Quantity;
                        }
                    }
                }

                // Set confirmed date
                _order.ConfirmedDate = DateTime.UtcNow;
            } else if (newStatus == StatusType.Fulfilled) {
                // Set shipping date
                _order.ShippedDate = DateTime.UtcNow;
            } else if (newStatus == StatusType.Paid) {
                // Track the sale on default analytics service
                _order.TrackSale();
            }

            // Before shipment? -> delete old ones
            if (_order.Context !=null && _order.IsStatusBefore(StatusType.Fulfilled, newStatus)) {
                _order.Include(nameof(_order.Shipments));
                foreach (var shipment in _order.Shipments.ToList()) {
                    _order.AddHistory("{text.order-history-shipment-deleted}: " + shipment.GetObjectValueSummary(new FormBuilder(Forms.Admin.EDIT)), user);
                    _order.Shipments.Remove(shipment);
                }


            }

        }

        public virtual bool AllowPlaceConfirm() {
            return false;
        }

        public virtual bool AllowPlacePay() {
            return false;
        }

        public bool AllowPlace(bool throwException) {
            if (_order.Context != null) {
                _order.Include(nameof(_order.OrderItems));
            }
            var valid = _order.ValidateOrder(throwException);
            if (valid) {
                if (!_order.IsStatusBefore(StatusType.Confirmed)) {
                    if (throwException) {
                        throw new BackendException("wrong-status", "Order cannot already be confirmend");
                    }
                    return false;
                }
            }
            return valid;
        }

        public virtual bool AllowRedraft(bool throwException) {
            return _order.ValidateRestrictions(_order.Context, throwException);
        }

        public virtual bool AllowPay(bool throwException) {
            return true;
        }
        public virtual bool AllowConfirm(bool throwException) {
            return _order.ValidateRestrictions(_order.Context, throwException);
        }
    }


    public class PaybeforeStrategy : OrderProcessLogic {

        public PaybeforeStrategy(Order order) : base(order) {

        }
        
        public override LinkedList<StatusType> StatusFlow
        {
            get
            {
                var statuses = new LinkedList<StatusType>();
                statuses.AddLast(StatusType.Draft);
                statuses.AddLast(StatusType.Placed);
                statuses.AddLast(StatusType.Paid);
                statuses.AddLast(StatusType.Confirmed);
                statuses.AddLast(StatusType.Fulfilled);
                return statuses;
            }
        }

        public override bool AllowPay(bool throwException) {
            if (base.AllowPay(throwException)) {
                if (_order.Status == StatusType.Placed) {
                    return true;
                } else {
                    return HandleError(throwException, "wrong-status", "Order has to be in Placed Status");
                }
            }
            return false;
        }

        public override bool AllowPlaceConfirm() {
            return false;
        }

        public override bool AllowPlacePay() {
            if (_order.Status == StatusType.Draft) {
                return AllowPlace(false);
            } else if (_order.Status == StatusType.Placed) {
                return AllowPlace(false) && AllowPay(false);
            }
            return false;
        }

        protected override Order.StatusType ProcessStatus(Order.StatusType newStatus, bool validate) {
            if (_order.Context != null) {
                _order.Include(nameof(Order.Business));
                _order.Include(nameof(Order.BillingAddress));
            }

            // Special treat: redraft
            if (newStatus == StatusType.Draft) {
                if (validate) {
                    AllowRedraft(true);
                }
            } else if (validate) {
                ValidateNextStatus(newStatus, true);
            }

            if (newStatus == Order.StatusType.Placed) {
                if (validate) {
                    AllowPlace(true);
                }

                // Create invoice now
                var ownerBusiness = _order.Context.OwnerBusiness();
               
                Finance.Invoices.InvoiceGeneration.CreateInvoice(ownerBusiness, _order.BillingAddress, new List<Order>() { _order }.AsEnumerable(), _order.PaymentMethod, null, _order.User, _order.Currency, _order.Business);

            } else if (newStatus == Order.StatusType.Confirmed) {
                if (validate) {
                    AllowConfirm(true);
                }
                if (_order.Business != null) {
                    _order.ArchiveAddressesForBusiness();
                }
                _order.Include(nameof(Order.Invoice));

                if (_order.Invoice != null && _order.Invoice.InvoiceSent.HasValue == false) {
                    _order.Invoice.InvoiceSent = DateTime.UtcNow;
                    _order.Context.SaveChanges(); // save is mandatory ...otherwise the attached invoice hasnt the new date. its downloaded internally in the same request but with other db-context

                }

            } else if (newStatus == Order.StatusType.Paid) {
                if (validate) {
                    AllowPay(true);
                }
            } else if (newStatus == Order.StatusType.Draft) {
                if (validate) {
                    AllowRedraft(true);
                }
                if (_order.Context != null) {
                    _order.Include(nameof(Order.Invoice));
                    if (_order.Invoice != null) {
                        _order.Invoice.Delete();
                    }
                }
            }
            return newStatus;
        }

        public override bool AllowRedraft(bool throwException) {
            if (base.AllowRedraft(throwException)) {
                if (_order.Status == Order.StatusType.Placed) {
                    return true;
                } else {
                    return HandleError(throwException, "wrong-status", "Cannot transition to Status 'Draft'");
                }
            }
            return false;
        }
    }

    public class PayAfterStrategy : OrderProcessLogic {

        public PayAfterStrategy(Order order) : base(order) {

        }
        public override LinkedList<StatusType> StatusFlow
        {
            get
            {
                var statuses = new LinkedList<StatusType>();
                statuses.AddLast(StatusType.Draft);
                statuses.AddLast(StatusType.Placed);
                statuses.AddLast(StatusType.Confirmed);
                statuses.AddLast(StatusType.Fulfilled);
                statuses.AddLast(StatusType.Paid);
                return statuses;
            }
        }

        public override bool AllowPay(bool throwException) {
            return HandleError(throwException, "not-allowed", "Payment not allowed in current status");
        }

        public override bool AllowPlaceConfirm() {
            return AllowPlace(false);
        }

        protected override Order.StatusType ProcessStatus(Order.StatusType newStatus, bool validate = true) {
            if (newStatus == StatusType.Draft) {
                if (validate) {
                    AllowRedraft(true);
                }
            } else if (validate) {
                ValidateNextStatus(newStatus, true);
            }
            if (newStatus == Order.StatusType.Placed) {
                if (validate) {
                    AllowPlace(true);
                }
            } else if (newStatus == Order.StatusType.Confirmed) {
                if (validate) {
                    AllowConfirm(true);
                }
                if (_order.Business != null) {
                    _order.ArchiveAddressesForBusiness();
                }
            }
          
            return newStatus;
        }

        public override bool AllowRedraft(bool throwException) {
            if (base.AllowRedraft(throwException)) {
                if (_order.IsStatusBefore(Order.StatusType.Fulfilled) && _order.Status != StatusType.Draft) {
                    return true;
                }
                return HandleError(throwException, "edit-error", "This order can't be edited anymore.");
            }
            return false;
        }

        public override bool AllowConfirm(bool throwException) {
            if (base.AllowConfirm(throwException)) {
                if (_order.Status != StatusType.Placed) {
                    return HandleError(throwException, "wrong-status", "Order cannot be confirmed in status: " + _order.Status);
                }
                return true;
            }
            return false;
        }
    }
}
