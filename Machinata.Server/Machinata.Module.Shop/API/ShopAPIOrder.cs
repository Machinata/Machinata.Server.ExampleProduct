using Machinata.Core.Model;
using Machinata.Module.Finance.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.API {
    public class ShopAPIOrder : ShopAPI {

        public ShopAPIOrder(APIHandler handler) : base(handler) {

        }

        /// <summary>
        /// Creates and confirms an order for the given date
        /// </summary>
        /// <param name="business">The business.</param>
        /// <param name="date">The date.</param>
        /// <param name="configurationQuantities">The configuration quantities.</param>
        /// <param name="coupons">The coupons.</param>
        public void Create(Business business, DateTime date, IDictionary<string,int> configurationQuantities, IEnumerable<string> coupons, User user) {

            // Products
            var products = Catalog.GetCatalogsForBusiness(this.DB, business, date).SelectMany(c => c.Products).Distinct();

            // Order
            var order = new Order();
            order.Business = business;
            order.OrderDate = date;
            order.InitializeBusinessOrder(this.DB, this.Handler.Context, this.Handler.Currency, this.Handler.User);

            // Items
            foreach (var productQuantity in configurationQuantities) {

                // Configuration (not merged)
                var configuration = this.DB.ProductConfigurations().Include(nameof(ProductConfiguration.Product)).GetByPublicId(productQuantity.Key);

                // Order Item
                var orderItem = order.UpdateQuantityOrAdd(this.DB, configuration, order.Currency, productQuantity.Value);
            }

            // Coupon
            if (coupons != null && coupons.Any()) {
                order.ApplyCoupon(this.DB, coupons.First());
            }

            // Save
            this.DB.Orders().Add(order);
            this.DB.SaveChanges();

            // Check status and place the order
            order.ChangeStatus(Model.Order.StatusType.Placed, user, false, true);

            // Auto Confirm if Invoice
            order.ChangeStatus(Model.Order.StatusType.Confirmed, user, false, true);


            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("order", new {
                Order = order.GetJSONForAPI()
            });

        }

        public void Delete(Business business, string publicId) {

            // Orderer
            var order = GetOrderByBusiness(business, publicId);

            // Can customers can delete orders in Status draft
            if (!order.ProcessLogic.AllowDelete()) {
                throw new BackendException("delete-error", "Order can only deleted in 'draft'");
            }

            // Delete order, items and history
            order.Delete(this.DB);

            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("order", new {
                Order = new { order.PublicId }
            });
        }

        public void GetOrder(Business business, string publicId) {
            var order = GetOrderByBusiness(business, publicId);
            this.SendAPIMessage("order", new {
                Order = order.GetJSONForAPI()
            });
        }

        public void Place(Business business, string publicId) {
            var order = GetOrderByBusiness(business, publicId);
            if (!order.ProcessLogic.AllowPlaceConfirm()) {
                this.SendAPIMessage("error", new {
                    Order = order.GetJSONForAPI()
                });
                return;
            }

            // Check status and place the order
            order.ChangeStatus(Model.Order.StatusType.Placed, this.Handler.User, false, true);

            // Auto Confirm if Invoice
            order.ChangeStatus(Model.Order.StatusType.Confirmed, this.Handler.User, false, true);

            this.SendAPIMessage("order", new {
                Order = order.GetJSONForAPI()
            });

            this.DB.SaveChanges();
        }

        public void GetOrders(Business business) {
            var orders = this.DB.Orders()
                .Include(nameof(Order.OrderItems))
                .Include(nameof(Order.OrderCoupons))
                .Include(nameof(Order.Shipments))
                .Include(nameof(Order.OrderHistories))
                .Where(o => o.Business.Id == business.Id).OrderByDescending(o => o.Id).ToList();

            this.SendAPIMessage("orders", new {
                Orders = orders.Select(o => o.GetJSONForAPI())
            });
        }

        public void GetOrders(User user) {
            var orders = this.DB.Orders().Where(o => o.User.Id == user.Id);
            this.SendAPIMessage("orders", new {
                Orders = orders.Select(o => o.GetJSONForAPI())
            });
        }


        public void GetCatalogs(Business business, DateTime date) {
            var catalogs = Catalog.GetCatalogsForBusiness(this.DB, business, date).ToList();
            foreach (var catalog in catalogs) {
                catalog.Include(nameof(Catalog.Products));
                foreach (var product in catalog.Products) {
                    product.Include(nameof(Product.Categories));
                }
            }
            this.SendAPIMessage("catalogs", new {
                Catalogs = catalogs.Select(o => o.GetJSONForAPI(business))
            });
        }


        public void GetInvoices(Business business) {
            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
                .Include(nameof(Invoice.InvoiceHistories))
                .Where(i => i.Business.Id == business.Id)
                .Where(i => i.Status >= Invoice.InvoiceStatus.Sent)
                .OrderByDescending(o => o.Id).ToList();
            this.SendAPIMessage("invoices", new {
                Invoices = invoices.Select(o => o.GetJSONForAPI())
            });
        }


        private Order GetOrderByBusiness(Business business, string publicId) {
            return  this.DB.Orders()
                .Include(nameof(Order.OrderItems) + "." + nameof(OrderItem.Configuration))
                .Include(nameof(Order.OrderCoupons))
                .Include(nameof(Order.Shipments))
                .Include(nameof(Order.OrderHistories))
                .Where(o => o.Business.Id == business.Id)
                .GetByPublicId(publicId);
        }
    }




}
