using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Finance.Model;
using Machinata.Module.Shop.Logic;
using Machinata.Module.Shop.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.API {

    /// <summary>
    /// Base class for external usage of the API:
    ///  -listing products for catalogs
    ///  -listing invoices
    ///  -creating orders
    /// </summary>
    public abstract class ShopAPI {

        
        public Core.Handler.APIHandler Handler { get; private set; }
        public ModelContext DB { get; private set; }
        public int Page { get; private set; }
        public int Size { get; private set; }
        public string Sort { get; private set; }
        public string Dir { get; private set; }


        public ShopAPI(Machinata.Core.Handler.APIHandler handler) {
            this.Handler = handler;
            this.DB = handler.DB;

            // TODO: PAGING, FILTERING: Not yet implemented
            this.Page = handler.Params.Int("page", 1);
            this.Size = handler.Params.Int("num", 50);
            // Sorting
            this.Sort = handler.Params.String("sort","desc" );
            this.Dir = handler.Params.String("dir", nameof(ModelObject.Created));

        }
  
        protected void SendAPIMessage(string type, object content) {
            var json = Core.JSON.Serialize(content, true, true);

            json = Core.Localization.Text.InsertVariables(json);
            this.Handler.SendRawAPIMessage(type, json, false);
        }

        public IQueryable<T> Paginate<T>(IQueryable<T> entities) where T : ModelObject{
            return entities.Paginate(this.Page, this.Size);
        }

        
    }
    
    public static class ApiExtensions {

        public static JObject GetJSONForAPI(this Order order) {


            var errorList = new List<OrderError>();
            order.ValidateOrder(false, errorList);

            var jsonForm = new FormBuilder(Forms.API.VIEW).Include(nameof(ModelObject.PublicId));
            var orderJson = order.GetJSON(jsonForm);
            var orderItems = order.OrderItems.Select(oi => oi.GetJSON(jsonForm));
            var coupons = order.OrderCoupons.Select(oc => oc.GetJSON(jsonForm));
            var history = order.OrderHistories.Select(h => h.GetJSON(jsonForm));
            var shipments = order.Shipments.Select(s => s.GetJSON(jsonForm));
            var errors = errorList.Select(s => s.GetJSON(jsonForm));

            orderJson["errors"] =  JToken.FromObject(errors);
            orderJson["items"] = JToken.FromObject(orderItems);
            orderJson["coupons"] = JToken.FromObject(coupons);
            orderJson["history"] = JToken.FromObject(history);
            orderJson["shipments"] = JToken.FromObject(shipments);

            return orderJson;
        }


        public static JObject GetJSONForAPI(this Product product, Business business) {
            var jsonForm = new FormBuilder(Forms.API.VIEW).Include(nameof(product.PublicId));
            var json = product.GetJSON(jsonForm);
            json["categories"] = JToken.FromObject(product.Categories.Select(p => p.GetJSON(jsonForm)));
            json["configuration"] = JToken.FromObject(product.GetConfigurationForBusiness(business).GetJSON(jsonForm));
            json["information"] = JToken.FromObject(product.Information.GetData());
            json["details"] = JToken.Parse(product.Details.Data);
            //json["variations"] = null; // TODO
            //json["related"] = null; // TODO
            return json;
        }

        public static JObject GetJSONForAPI(this Catalog catalog, Business business) {
            var jsonForm = new FormBuilder(Forms.API.VIEW).Include(nameof(catalog.PublicId));
            var catalogJson = catalog.GetJSON(jsonForm);
            var products = catalog.Products.Where(p => p.Published == true);
            catalogJson["products"] = JToken.FromObject(products.Select(p=>p.GetJSONForAPI(business)));
            return catalogJson;
        }


        public static JObject GetJSONForAPI(this Invoice invoice) {
            var jsonForm = new FormBuilder(Forms.API.VIEW).Include(nameof(Invoice.PublicId));
            var catalogJson = invoice.GetJSON(jsonForm);
            catalogJson["line-item-groups"] = JToken.FromObject(invoice.LineItemGroups.Select(g => g.GetJSONForAPI()));
            return catalogJson;
        }

        public static JObject GetJSONForAPI(this LineItemGroup lineItemGroup) {
            var jsonForm = new FormBuilder(Forms.API.VIEW).Include(nameof(Invoice.PublicId));
            var json = lineItemGroup.GetJSON(jsonForm);
            json["line-items"] = JToken.FromObject(lineItemGroup.LineItems.Select(g => g.GetJSON(jsonForm)));
            return json;
        }
    }
}
