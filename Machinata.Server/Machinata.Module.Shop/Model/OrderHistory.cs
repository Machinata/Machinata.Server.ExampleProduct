using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class OrderHistory : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderHistory() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Order.StatusType Status { get; set; }
        
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime Timestamp { get { return this.Created; } }
        
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Description { get; set; }

        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string User { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        public Order Order { get; set; }




        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.API.VIEW)]
        public string DescriptionTranslated
        {
            get {
                return Core.Localization.Text.InsertVariables(this.Description);
            }
        }

      

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderStatusExtenions {
        [ModelSet]
        public static DbSet<OrderHistory> OrderHistories(this Core.Model.ModelContext context) {
            return context.Set<OrderHistory>();
        }
    }


    #endregion

}
