using System;
using Machinata.Core.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Machinata.Core.Builder;
using System.Linq;
using Machinata.Core.Util;
using System.ComponentModel;

namespace Machinata.Module.Shop.Model {
  
    [ComplexType]
    public class ProductInformation  {

        public enum ProductInfoType : short {

            [Description("Information")]
            Unspecified = 0,
            [Description("{text.nutrition-table}")]
            NutritionTable = 10,
            [Description("Specifications")]
            Specification = 20
        }

        [Column]
        public string Description { get; set; }

        [Column]
        public ProductInfoType Type { get; set; }

        /// <summary>
        /// Data is as List of ProductInfoGroup serialized to json
        /// Dont get/set this directly
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [Column]
        public string Data { get; set; }

        public void SetData (IEnumerable<ProductInfoGroup> pig) {
            this.Data = Core.JSON.Serialize(pig);
        }

        public IEnumerable<ProductInfoGroup> GetData() {
            if (this.Data != null) {
                return Core.JSON.Deserialize<IEnumerable<ProductInfoGroup>>(this.Data);
            }
            return new List<ProductInfoGroup>();
        }


        public void AddData(params ProductInfoGroup[] groups) {
            var list = new List<ProductInfoGroup>(GetData());
            list.AddRange(groups);
            SetData(list);
        }
    }

    
    public class ProductInfoGroup  {

        public ProductInfoGroup(string name) {
            this.Items = new List<ProductInfoItem>();
            this.Name = name;
        }

        public ProductInfoGroup() {
            this.Items = new List<ProductInfoItem>();
        }

        public string Name { get; set; }

        public IList<ProductInfoItem> Items { get; set; }

        public ProductInfoItem AddItem(string itemString) {
            var splits = new List<string>(itemString.Split(','));
            var item = new ProductInfoItem();
            item.Name = splits.First();
            foreach(var value in splits.Skip(1)) {
                var valuePair = value.Split(':');
                item.Values.Add(valuePair.First(), valuePair.Last());
            }
            this.Items.Add(item);
            return item;
        }
    }

    public class ProductInfoItem {
        public ProductInfoItem() {
            Values = new Dictionary<string, string>();
        }
        public string Name { get; set; }
        public string Id { get; set; }
        public Dictionary<string, string> Values { get; set; }



        public string GetData() {
            return Core.JSON.Serialize(this);
        }
    }

  
}
