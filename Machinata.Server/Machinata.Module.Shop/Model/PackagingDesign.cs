using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class PackagingDesign : ModelObject {

        #region Constants /////////////////////////////////////////////////////////////////////////

        public const string PACKAGING_LOGO_KEY = "PackagingLogo";

        public const string DETAILS_KEY_SERVING_TYPE = "ServingType";
        public const string DETAILS_KEY_SELL_BY_DAYS = "SellByDays";
        public const string DETAILS_KEY_CONSUME_BY_DAYS = "ConsumeByDays";
        public const string DETAILS_KEY_STORE_AT = "StoreAt";


        public const string DETAILS_KEY_PORTIONS = "Portions";
        public const string DETAILS_KEY_WEIGHT = "Weight";
        #endregion

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public PackagingDesign() {
            this.Settings = new Properties();
        }

        public static PackagingDesign GetDesign(ModelContext db, string designId, bool firstIfNone = false) {
            var design = db.PackagingDesigns().FirstOrDefault(pd => pd.ShortURL == designId);
            if (design == null && firstIfNone) {
                return db.PackagingDesigns().FirstOrDefault();
            }
            return design;
        }

        public static string GetLogoByBusiness(ModelContext db, Business business) {
            var configuration = GetConfiguration(db, business);
            return configuration?.Configuration[PACKAGING_LOGO_KEY]?.ToString();
        }

        public static PackagingDesign GetPackagingDesignByBusiness(ModelContext db, Business business) {
            PackagingConfiguration configuration = GetConfiguration(db, business);
            if (configuration != null) {
                return configuration.Design;
            }
            return null;
        }

        //todo what if more than one per business
        public static PackagingConfiguration GetConfiguration(ModelContext db, Business business, bool createNew = true) {
            var configuration = db.PackagingConfigurations()
                .Include(nameof(PackagingConfiguration.Business))
                .Include(nameof(PackagingConfiguration.Design))
                .FirstOrDefault(pc => pc.Business.Id == business.Id);
            if (configuration == null && createNew) {
                configuration = new PackagingConfiguration();
                configuration.Business = business;
                db.PackagingConfigurations().Add(configuration);
                db.SaveChanges();
            }
            return configuration;

        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Title { get; set; }
        

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string ShortURL { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [PropertiesConfigKey("ShopProductPackagingDesignSettingsKeys")]
        [Column]
        public Properties Settings { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        [DataType(DataType.ImageUrl)]
        [CascadeDelete]
        public string MockupImage { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [DataType(DataType.ImageUrl)]
        [CascadeDelete]
        public string Thumbnail { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        #endregion

        #region Override Methods //////////////////////////////////////////////////////////////////

        public override string ToString() {
            return string.IsNullOrEmpty(this.Title) == false ? this.Title : base.ToString();
        }

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextPackagingDesignExtenions {
        [ModelSet]
        public static DbSet<PackagingDesign> PackagingDesigns(this Core.Model.ModelContext context) {
            return context.Set<PackagingDesign>();
        }
    }

   

    #endregion

}
