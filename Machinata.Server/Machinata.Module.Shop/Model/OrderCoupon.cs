using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]

    public enum OrderCouponType : short {
        Percentage = 10,
        FixedValue = 20,
        
        FreeShipping = 30,
        FreeItems = 40

    }
    [ModelClass] 
    public partial class OrderCoupon : ModelObject {
        
      

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderCoupon() {
            AmountFixedValue = new Price();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Required]
        [Index(IsUnique = true)]
        [MinLength(3)]
        [MaxLength(100)]
        public string Code { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Title { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [MaxLength(500)]
        public string Description { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public OrderCouponType OrderCouponType { get; set; }


        /// <summary>
        /// How many times the coupon can be used.
        /// </summary>
        /// <value>
        /// The limit.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public int? Limit { get; set; }


        /// <summary>
        /// FixedValue Coupon: Amount of reduction in absolut value/currency -> Price
        /// </summary>
        /// <value>
        /// The limit.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Price AmountFixedValue { get; set; }

        /// <summary>
        /// Percentage Coupon: Get or sets the amount percentage.
        /// </summary>
        /// <value>
        /// The amount percentage.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public int? AmountPercentage { get; set; }

        /// <summary>
        /// FreeItems Coupon: Sets the number of free items
        /// </summary>
        /// <value>
        /// The number of items.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public int? NumberOfItems { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string CategoryFilter { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string ProductFilter { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string CountryFilter { get; set; }

        [Column]
        public int MinimumOrderAmount { get; set; }

        [Column]
        public int MinimumOrderItems { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.API.VIEW)]
        public bool IncludeShipping { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool Enabled { get; set; } = true;

        [Column]
        public string Notification { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? Expires { get; set; }


        /// <summary>
        /// If this is set and a product configuration has a StockQuantity which is smaller than this the coupon is not valid
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public int? MinimumStockQuantity { get; set; }
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<Order> Orders { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        /// <summary>
        /// Gets  the amount title. e.g. 15%
        /// Returns null if the OrderCouponType doesnt support AmountTitle
        /// </summary>
        /// <value>
        /// The amount title.
        /// </value>
        public string AmountTitle
        {
            get
            {
                if (this.OrderCouponType == OrderCouponType) {
                    return $"-{this.AmountPercentage}%";
                }
                return null;
            }
        }
        #endregion


        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
            if (OrderCouponType == OrderCouponType.FixedValue && (AmountFixedValue == null || !AmountFixedValue.HasValue)) {
                throw new BackendException("coupon-error", "This Fixed Value Coupon needs an AmountFixedValue");
            }
            if (OrderCouponType == OrderCouponType.Percentage && AmountPercentage == null) {
                throw new BackendException("coupon-error", "This Percentag Coupon needs an AmountPercentage");
            }
            if (OrderCouponType == OrderCouponType.FreeItems && NumberOfItems == null) {
                throw new BackendException("coupon-error", "This FreeItems Value Coupon needs an NumberOfItems");
            }
        }
         
        public override string ToString() {
            return "order-coupon".TextVar() + $": {this.Code}";
        }

        #endregion 

        #region Public Methods ////////////////////////////////////////////////////////////////////
        public bool ValidateForOrder(ModelContext db,Order order, bool throwExceptions, IList<OrderError> errors = null) {
            if (errors == null) { errors = new List<OrderError>(); }
            if (order.Context != null) { order.Include(nameof(order.OrderItems)); }
            bool valid = true;

            // Expired?
            if (this.IsCouponExpired()) {
                valid = false;

                var code = "coupon-expired";
                var message = "Sorry, the coupon has expired.";
                errors.Add(new OrderError() { Code = code, Message = message });

                if (throwExceptions) throw new BackendException(code , message);
            }

            // Limit
            if (this.Limit != null) {
                int count = GetCouponUsages(db);
                if (count >= this.Limit) {
                    valid = false;
                    var code = "coupon-used";
                    var message = "Sorry, this coupon has already been used.";
                    errors.Add(new OrderError() { Code = code, Message = message });

                    if (throwExceptions) throw new BackendException(code, message);
                }
            }

            // Category
            if (this.CategoryFilter != null) {
                foreach (var item in order.OrderItems) {

                    item.Include(nameof(item.Product));
                    item.Product.Include(nameof(Product.Categories));

                    if (IsValidForCategoryFilter(item.Product) == false) {
                        valid = false;

                        var code = "coupon-invalid";
                        var message = "Sorry, this coupon is only valid for " + this.CategoryFilter + " products. Remove other products first before applying the coupon.";
                        errors.Add(new OrderError() { Code = code, Message = message });


                        if (throwExceptions) throw new BackendException(code, message);
                    }
                }
            }

            // Product
            if (this.ProductFilter != null) {
                foreach (var item in order.OrderItems) {
                    item.Include(nameof(item.Product));
                    if (item.Product.Name.ToLower() != this.ProductFilter.ToLower()) {
                        valid = false;

                        var code = "coupon-invalid";
                        var message = "Sorry, this coupon is only valid for the product " + this.ProductFilter + " (" + this.Title + "). Remove other products first before applying the coupon.";
                        errors.Add(new OrderError() { Code = code, Message = message });

                        if (throwExceptions) throw new BackendException(code, message);
                    }
                }
            }

            // Country
            if (this.CountryFilter != null) {
                if (order.BillingAddress.CountryCode.ToLower() != this.CountryFilter.ToLower()
                    ||
                    order.ShippingAddress.CountryCode.ToLower() != this.CountryFilter.ToLower()) {
                    valid = false;


                    var code = "coupon-invalid";
                    var message = "Sorry, this coupon is only valid for the country " + this.CountryFilter.ToUpper() + " (" + this.Title + "). Please make sure the shipping and billing address are correct before applying the coupon.";
                    errors.Add(new OrderError() { Code = code, Message = message });

                    if (throwExceptions) throw new BackendException(code, message);
                }
            }

            // Minimum StockQuantity
            if (this.MinimumStockQuantity != null) {
                foreach (var item in order.OrderItems) {

                    item.Include(nameof(item.Product));
                    item.Product.Include(nameof(Product.Configurations));

                    // Check each configuration
                    foreach(var configuration in item.Product.Configurations) {
                        // TODO/HINT: Should the quantity of the order item also be considered? 
                        if (configuration.StockQuantity != null && (configuration.StockQuantity < this.MinimumStockQuantity.Value)) {
                            valid = false;
                             
                            var code = "coupon-invalid";
                            var message = "Sorry, the product is out of stock "  + "(" + item.Title +"). Please remove this product before applying the coupon.";
                            errors.Add(new OrderError() { Code = code, Message = message });

                            if (throwExceptions) throw new BackendException(code, message);
                        }
                    }

                }
            }

            //TODO: all other validations...
            //coupon.MinimumOrderAmount 
            //coupon.MinimumOrderItems 

            return valid;
        }

        private int GetCouponUsages(ModelContext db) {
            // return db.Orders().Where(o => o.Status != Order.StatusType.Draft && o.Coupons == this.Code).Count();
            Include(nameof(Orders));
            return Orders.Count(o => o.Status != Order.StatusType.Draft);
        }

        public bool IsCouponExpired() {
            if (this.Enabled == false) return true; // expired as no longer enabled
            if (this.Expires == null) return false; // not specified
            return (DateTime.UtcNow > this.Expires.Value);
        }

        internal Price CalculateNegativeDiscountForOrder(Order order) {
            // Init
            Price applyAmount = GetOrderAmountToApplyTo(order);
            //  decimal applyAmountDecimal = GetOrderAmountToApplyTo(order);

            return CalculateDiscount(order.Currency, applyAmount, order);
        }

        /// <summary>
        /// Calculates the negative discount for price.
        /// IMPORTANT ONLY FOR FIXED_VALUE AND PERCANTAGE TYPES
        /// </summary>
        /// <param name="price">The price.</param>
        /// <returns></returns>
        public Price CalculateNegativeDiscountForPrice(Price price) {
            // Init
            if (this.OrderCouponType != OrderCouponType.FixedValue && this.OrderCouponType != OrderCouponType.Percentage) {
                throw new BackendException("coupon-type-invalid", "Calculation only possible with an order");
            }

            return CalculateDiscount(price.Currency, price, null);
        }

        // DONT CALL FROM OUTSIDE
        private Price CalculateDiscount(string currency, Price applyAmount, Order order) {
            // Return amount based on the type of coupon
            if (this.OrderCouponType == OrderCouponType.Percentage) {

                // This is a coupon based on the percentage of the amount
                decimal fraction = (decimal)this.AmountPercentage / ((decimal)100);
                decimal amount = applyAmount.Value.Value * fraction;
                //return new Price(-(int)Math.Floor(amount));
                return new Price(-amount, currency); //TODO: @micha do we need to round to a specific digit?


            } else if (this.OrderCouponType == OrderCouponType.FixedValue) {
                var amount = this.AmountFixedValue.ConvertToCurrency(currency);
                // This is a coupon based on the percentage of the amount
                if (amount > applyAmount) {
                    return -applyAmount;
                }
                return -amount;
            } else if (this.OrderCouponType == OrderCouponType.FreeShipping) {
                // This coupon gives the shipping discount
                return -order.ShippingCustomer.Clone();
            } else if (this.OrderCouponType == OrderCouponType.FreeItems) {

                // This coupon gives a number of items for free
                Price shippingDiscount = new Price(0, currency);
                if (this.IncludeShipping && NumberOfItems >= order.OrderItems.Count) {
                    shippingDiscount.Set(order.ShippingCustomer);
                }
                Price itemDiscount = new Price(0, currency);
                var items = order.OrderItems.Take(this.NumberOfItems.Value); // take maximum of Amount items
                foreach (var item in items) {
                    itemDiscount += item.CustomerPrice;
                }
                return -(itemDiscount + shippingDiscount);

            } else {
                throw new BackendException("coupon-type-invalid", "The coupon type is not valid.");
            }
        }

        /// <summary>
        /// Generates the coupon code the DB Id given.
        /// 0 => 5VZNKB
        /// 1 => HL766Z
        /// 2 => TMGSEY
        /// 3 => P28L4W
        /// 4 => EM5EWD
        /// 5 => WIACCZ
        /// 6 => 8DEPDA
        /// 7 => OQE33A
        /// 8 => 4SEQ5A
        /// 9 => AVAXS5
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">invalid-id;The id is not valid for genareting a coupon code.</exception>
        public static string GenerateCouponCodeForId(int id) {
            if (id < 0) throw new BackendException("invalid-id", "The id is not valid for genareting a coupon code.");
            uint idEncrypted = GenerateCouponCodeForIdCrypt((uint)id);
            return GenerateCouponCodeForIdCouponCode(idEncrypted);
        }


        const int COUPON_CODE_BITCOUNT = 30;
        const int COUPON_CODE_BITMASK = (1 << COUPON_CODE_BITCOUNT / 2) - 1;
        const string COUPON_CODE_ALPHABET = "AG8FJLE2WVTCPY5ZH3NRUDBXSMQK7946"; // Original: AG8FOLE2WVTCPY5ZH3NIUDBXSMQK7946, removed O and I

        private static uint GenerateCouponCodeForIdRoundFunction(uint number) {
            return (((number ^ 47894) + 25) << 1) & COUPON_CODE_BITMASK;
        }

        private static uint GenerateCouponCodeForIdCrypt(uint number) {
            uint left = number >> (COUPON_CODE_BITCOUNT / 2);
            uint right = number & COUPON_CODE_BITMASK;
            for (int round = 0; round < 10; ++round) {
                left = left ^ GenerateCouponCodeForIdRoundFunction(right);
                uint temp = left; left = right; right = temp;
            }
            return left | (right << (COUPON_CODE_BITCOUNT / 2));
        }

        private static string GenerateCouponCodeForIdCouponCode(uint number) {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < 6; ++i) {
                b.Append(COUPON_CODE_ALPHABET[(int)number & ((1 << 5) - 1)]);
                number = number >> 5;
            }
            return b.ToString();
        }

        private static uint GenerateCouponCodeForIdCodeFromCoupon(string coupon) {
            uint n = 0;
            for (int i = 0; i < 6; ++i)
                n = n | (((uint)COUPON_CODE_ALPHABET.IndexOf(coupon[i])) << (5 * i));
            return n;
        }


        public static OrderCoupon GetCoupon(ModelContext db, string couponCode) {
            return db.OrderCoupons().SingleOrDefault(c => c.Enabled == true && c.Code.ToLower() == couponCode.ToLower());
        }

        public bool IsValidForCategoryFilter(Product product) {

            // Support one of the comma separated CategoryFilters
            if (this.CategoryFilter != null && this.CategoryFilter.Contains(",")) {
                var categoryFilters = this.CategoryFilter.Split(',');
               foreach ( var categoryFilter in categoryFilters) {
                    if (product.Categories.Select(c => c.Name.ToLower()).Contains(categoryFilter)) {
                        return true;
                    }
               }
                return false;
            }

            // Only One Category
            return product.Categories.Select(c => c.Name.ToLower()).Contains(this.CategoryFilter.ToLower());
        }

        public bool IsCouponValidForCategory(Product product) {
            if (this.CategoryFilter != null) {
                product.Include(nameof(product.Categories));

                return IsValidForCategoryFilter(product);
            }

            return true;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////
        /// <summary>
        /// Gets the order amount to apply the discount on depending on whether or not shipping is to be included or not.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        private Price GetOrderAmountToApplyTo(Order order) {

            var amount = order.SubtotalCustomer.Clone();

            if (this.IncludeShipping) {
                amount = amount + order.ShippingCustomer;
            } 

            if (Config.ShopVATIncluded == false) {
                amount = amount + order.VATCustomer;
            }

            return amount;

        }
        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderCouponExtenions {
        [ModelSet]
        public static DbSet<OrderCoupon> OrderCoupons(this Core.Model.ModelContext context) {
            return context.Set<OrderCoupon>();
        }
    }

    #endregion
}
