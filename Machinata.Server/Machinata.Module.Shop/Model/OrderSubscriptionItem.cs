using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class OrderSubscriptionItem : ModelObject {
        

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderSubscriptionItem() {
            this.Categories = new List<Category>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Range(1,10000)]
        public int Quantity { get; set; }
         


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public ICollection<Category> Categories { get; set; }

        [Column]
        public OrderSubscription OrderSubscription { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        public string Title
        {
            get
            {
                return $"{string.Join(", ", Categories.Select(c => c.Name))}: {Quantity}";
            }
        }


        #endregion

                #region Model Creation ////////////////////////////////////////////////////////////////////

                #endregion

                #region Model Hooks ////////////////////////////////////////////////////////////////////

                #endregion

                #region Public Methods ////////////////////////////////////////////////////////////////////
                //public void RemoveCategory(Category category) {
                //    if (!this.Categories.Contains(category)) throw new BackendException("no-category", "The subscription item does not have this category.");
                //    this.Categories.Remove(category);
                //}

                //public void AddCategory(Category category) {
                //    if (this.Categories.Contains(category)) throw new BackendException("already-has-category", "The subscription item already has this category.");


                //    this.Categories.Add(category);
                //}



                #endregion

                #region Public Override Methods ////////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();

            if (this.Context != null) {
                this.Include(nameof(this.Categories));
            }

            // Check has min one main category 
            if (Categories != null  && !Categories.Any(c=>c.CategoryType == Category.CategoryTypes.Main)) {
                throw new BackendException("missing-main-category", "Please choose a main category for this order subscription item.");
            }
         
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContexOrderSubscriptionItemsExtenions {
        [ModelSet]
        public static DbSet<OrderSubscriptionItem> OrderSubscriptionItems(this Core.Model.ModelContext context) {
            return context.Set<OrderSubscriptionItem>();
        }
    }

    #endregion

}
