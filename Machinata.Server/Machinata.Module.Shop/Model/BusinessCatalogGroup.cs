using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class BusinessCatalogGroup : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

      
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public BusinessCatalogGroup() { 
        }


        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [InverseProperty("_businessCatalogGroup")]
        [ForeignKey("CatalogGroupId")]
        public CatalogGroup CatalogGroup { get; set; }

        [Index("IX_CatalogGroupBusiness", 1, IsUnique = true)]
        public int? CatalogGroupId { get; set; }
        
        [ForeignKey("BusinessId")]
        public Business Business { get; set; }

        [Index("IX_CatalogGroupBusiness", 2, IsUnique = true)]
        public int? BusinessId { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Model Events //////////////////////////////////////////////////////////////////////
        
        [OnModelObjectDeleteFromDB(typeof(Core.Model.Business))]
        public static bool OnModelObjectDeleteFromDB(ModelContext db, ModelObject entity) {
            var resaveRequired = false;
            // remove group catalog connections

            // this will never be called because we only archive businesses.
            var business = entity as Business;
            //var catalogGroups = db.GetCatalogGroupsForBusiness(business).ToList();
            //foreach(var catalogGroup in catalogGroups) {
            //    catalogGroup.RemoveBusiness(business);
            //    resaveRequired = true;
            //}
            return resaveRequired;
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion
        
        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContexBusinessCatalogGroupGroupExtenions {
        [ModelSet]
        public static DbSet<BusinessCatalogGroup> BusinessCatalogGroups(this Core.Model.ModelContext context) {
            return context.Set<BusinessCatalogGroup>();
        }
    }

    #endregion

}
