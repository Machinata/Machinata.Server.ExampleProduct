using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using System.Web;
using System.Data.Entity.Infrastructure;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass]
    public partial class Catalog : ModelObject, IPublishedModelObject, IShortURLModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Catalog() {
            this.Products = new List<Product>();
            this.Schedule = new DateRange();
        }

        public static Catalog FindOrCreateCatalog(ModelContext db, string catalogName) {
          
            var catalog = db.Catalogs().SingleOrDefault(c => c.Name == catalogName);
            if (catalog == null) {
                catalog = new Catalog();
                catalog.Name = catalogName;
                catalog.ShortURL = catalogName;
                db.Catalogs().Add(catalog);
                db.SaveChanges(); // otherwise not in db.Catalogs for next product
            }
            return catalog;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [Index(IsUnique = true)]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public DateRange Schedule { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public bool Published { get; set; }



        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        [InverseProperty("Catalogs")]
        public ICollection<Product> Products { get; set; }

        //[Column]
        //public ICollection<Business> Businesses { get; set; }

        [Column]
        [InverseProperty("Catalogs")]
        public CatalogGroup Group { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Photos { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Thumbnails { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.API.VIEW)]
        public DateTime? Start { get { return (this.Schedule?.Start); } }

        [FormBuilder(Forms.API.VIEW)]
        public DateTime? End { get { return (this.Schedule?.End); } }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {

        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static IQueryable<Catalog> GetCatalogsForBusiness(ModelContext db, Business business, DateTime preferredDeliveryTime) {
            // All catalogs for this business
            var catalogs = db.GetCatalogGroupsForBusiness(business).SelectMany(bc => bc.Catalogs);

            // fake delivery date, because catalogs end is start of the last day , which should be inluded until the the end
            var endOfDay = preferredDeliveryTime.AddDays(1).AddMilliseconds(-1);
            // Check catalogs schedule
            return catalogs.Where(c => (c.Schedule.Start == null && c.Schedule.End == null) || (c.Schedule.Start <= preferredDeliveryTime && c.Schedule.End.Value > endOfDay));
        }

        /// <summary>
        /// Gets the published catalogs for business.
        /// Use this to get the valid catalogs on the frontend
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="business">The business.</param>
        /// <param name="preferredDeliveryTime">The preferred delivery time.</param>
        /// <returns></returns>
        public static IQueryable<Catalog> GetPublishedCatalogsForBusiness(ModelContext db, Business business, DateTime preferredDeliveryTime) {
            return GetCatalogsForBusiness(db, business, preferredDeliveryTime).Where(c => c.Published);
        }

        public static IEnumerable<Product> GetAllProductsForBusinessOnDate(ModelContext db, Business business, DateTime date) {
            var catalogs = GetCatalogsForBusiness(db, business, date);
            return catalogs.SelectMany(c => c.Products).ToList().Distinct();
        }

        public static IEnumerable<Product> GetAllProductsFromPublishedCatalogsForBusinessOnDate(ModelContext db, Business business, DateTime date) {
            var catalogs = GetPublishedCatalogsForBusiness(db, business, date);
            return catalogs.SelectMany(c => c.Products).ToList().Distinct();
        }

        /// <summary>
        /// Gets the products for business. (Only if products from catalogs this business is in)
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        public static IQueryable<Product> GetProductsForBusiness(ModelContext db, Business business) {
            // All catalogs for this business
            var catalogs = db.GetCatalogGroupsForBusiness(business).SelectMany(bc => bc.Catalogs);
            // Check catalogs schedule
            return catalogs.SelectMany(c => c.Products).Distinct();
        }


        public void RemoveProduct(Product product) {
            if (Context != null) {
                this.Include(nameof(Products));
            }
            if (!this.Products.Contains(product)) throw new BackendException("no-product", "The catalog does not have this product.");
            this.Products.Remove(product);
        }

        public void AddProduct(Product product) {
            if (Context != null) {
                this.Include(nameof(Products));
            }
            if (this.Products.Contains(product)) throw new BackendException("already-has-product", "The catalog already has this product.");
            this.Products.Add(product);
        }

        public static IEnumerable<Catalog> PrepareEmptyWeeklyCatalogs(DateTime startDate, string name, int number, string weekname = "KW") {
            var catalogs = new List<Catalog>();
            for (int i = 0; i < number; i++) {
                var startDateShifted = startDate.AddDays(7 * i).AddHours(2); //FIX: compensate for timezone changes on day of week end of previous calendar @micha

                // Set to start of local time week in utc (offset to utc changes in daylight saving )
                var currentStartDateUtc = startDateShifted.StartOfDefaultTimezoneWeekInUtc();
                var currentEndDateUtc = currentStartDateUtc.AddDays(7).AddSeconds(-1);
                var startDateLocalTime = Time.ConvertToDefaultTimezone(currentStartDateUtc);
                var endDateLocalTime = Time.ConvertToDefaultTimezone(currentEndDateUtc);

                var weekNumberIso8601 = Time.GetIso8601WeekOfYear(startDateLocalTime);
                var yearNumber = startDateLocalTime.Year;
                if(startDateLocalTime.Year != endDateLocalTime.Year) {
                    if (weekNumberIso8601 == 1) yearNumber++;
                }
                
                var catalog = new Catalog();
                catalog.Schedule = new DateRange(currentStartDateUtc, currentEndDateUtc);
                catalog.Name = $"{name}: {weekname} {weekNumberIso8601} {yearNumber}";
                catalog.ShortURL = Core.Util.String.ReplaceUmlauts(catalog.Name);
                catalog.ShortURL = Core.Util.String.RemoveDiacritics(catalog.ShortURL);
                catalog.ShortURL = catalog.ShortURL.Replace("  ", " ").Replace(" ", "-").ToLower();
                catalogs.Add(catalog);
            }
            return catalogs;
        }

        public static IEnumerable<Catalog> PrepareEmptyQuarterlyCatalogs(DateTime startDate, string name, int number, string quarterName = "Quartal") {
            var catalogs = new List<Catalog>();
            for (int i = 0; i < number; i++) {
                var startDateShifted = startDate.AddMonths(i * 3);

                // Set to start of local time quarter in utc (offset to utc changes in daylight saving )
                var currentStartDateUtc = startDateShifted.StartOfDefaultTimezoneQuarterInUtc();
                var startDateLocalTime = Time.ConvertToDefaultTimezone(currentStartDateUtc);

                var catalog = new Catalog();
                catalog.Schedule = new DateRange(currentStartDateUtc, currentStartDateUtc.StartOfDefaultTimezoneEndOfQuarterInUtc());
                catalog.Name = $"{name}: {quarterName} {startDateLocalTime.QuarterNumber()} {startDateLocalTime.Year}";
                catalog.ShortURL = Core.Util.String.ReplaceUmlauts(catalog.Name);
                catalog.ShortURL = Core.Util.String.RemoveDiacritics(catalog.ShortURL);
                catalog.ShortURL = catalog.ShortURL.Replace("  ", " ").Replace(" ", "-").ToLower();
                catalogs.Add(catalog);
            }
            return catalogs;
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override Core.Cards.CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Description?.Summary)
                .Tag(this.Schedule.ToString())
                .Sub(this.ShortURL)
                .Icon("news");
        }

        public override void Validate() {
            base.Validate();

            // Schedule Dates
            if (Schedule != null) {

                if (Schedule.HasValue() && (Schedule.Start == null || Schedule.End == null)) {
                    throw new BackendException("invalid-schedule", "A start and and an end date is required.");
                }

                if (Schedule.HasValue() && (Schedule.Start >= Schedule.End)) {
                    throw new BackendException("invalid-schedule", "The start date has to be after the end date.");
                }

            }

            if (HttpUtility.UrlEncode(ShortURL) != ShortURL) {
                throw new BackendException("invalid-short-url", "The short url is invalid");
            }
        }


        public override void Populate(IPopulateProvider provider, FormBuilder form) {
            base.Populate(provider, form);
            this.ShortURL = Core.Util.String.CreateShortURLForName(this.Name);

        }

        #endregion
    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextCatalogExtenions {
        [ModelSet]
        public static DbSet<Catalog> Catalogs(this Core.Model.ModelContext context) {
            return context.Set<Catalog>();
        }


    }

    public static class CatalogsExtenions {
        
        public static DbQuery<Catalog> IncludeProducts(this DbSet<Catalog> entities) {
            return entities
                .Include($"{nameof(Catalog.Products)}")
                .Include($"{nameof(Catalog.Products)}.{nameof(Product.Configurations)}")
                .Include($"{nameof(Catalog.Products)}.{nameof(Product.Thumbnails)}")
                .Include($"{nameof(Catalog.Products)}.{nameof(Product.Photos)}")
                .Include($"{nameof(Catalog.Products)}.{nameof(Product.Title)}")

            ;
        }

       
    }

    #endregion

    public class CatalogScheduleRange : ModelObject {
        
        public IEnumerable<Catalog> Catalogs { get; set; }

        /// <summary>
        /// Gets or sets the range in utc
        /// </summary>
        /// <value>
        /// The range.
        /// </value>
        [FormBuilder]
        public DateRange Range { get; set; }


        /// <summary>
        /// Gets the start  in default timezone
        /// </summary>
        /// <value>
        /// The start.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Start
        {
            get
            {
                return Time.ConvertToDefaultTimezone(Range.Start.Value).ToString(Core.Config.DateFormat);
            }
        }


        /// <summary>
        /// Gets the schedule  in default timezone
        /// </summary>
        /// <value>
        /// The schedule.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Schedule {
            get {
                if (Range.HasValue()) {
                    var dateStart = this.Range.Start.Value;
                    var dateEnd = this.Range.End.Value;
                    dateStart = Core.Util.Time.ConvertToDefaultTimezone(dateStart);
                    dateEnd = Core.Util.Time.ConvertToDefaultTimezone(dateEnd);
                    return dateStart.ToString(Core.Config.DateFormat) + "-" + dateEnd.ToString(Core.Config.DateFormat);
                }
                return "no-schedule";
            }
        }

        /// <summary>
        /// Gets the month eg. {text.january}  in default timezone
        /// </summary>
        /// <value>
        /// The month.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Month
        {
            get
            {
                var range = this.Range.ToDefaultTimezone();
                var s = "{text." + range.Start.Value.ToString("MMMM").ToLower() + "}";
                var e = "{text." + range.End.Value.ToString("MMMM").ToLower() + "}"; ;
                if (s == e) return s;
                else return s + "/" + e;
            }
        }

        /// <summary>
        /// Gets the week. in default timezone
        /// </summary>
        /// <value>
        /// The week.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Week
        {
            get
            {
                return Core.Util.Time.GetIso8601WeekOfYear(this.Range.ToDefaultTimezone().Start.Value);
            }
        }


        /// <summary>
        /// Gets the year  in default timezone
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Year {
            get {
                var range = this.Range.ToDefaultTimezone();
                var s = range.Start.Value.ToString("yyyy");
                var e = range.End.Value.ToString("yyyy");
                if (s == e) return s;
                else return s + "/" + e;
            }
        }
    }

}