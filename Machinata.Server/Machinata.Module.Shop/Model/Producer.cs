using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;

namespace Machinata.Module.Shop.Model {

    /// <summary>
    /// Receives Order, creates deliverables, sends out goods
    /// (PrintProvider)
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    [Serializable()]
    [ModelClass] 
    public partial class Producer : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Producer() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
                
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
       

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        public void AddCatalog(Catalog catalog) {
            throw new NotImplementedException();
        }

        public void RemoveCatalog(Catalog catalog) {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProducerExtenions {
        [ModelSet]
        public static DbSet<Producer> Producers(this Core.Model.ModelContext context) {
            return context.Set<Producer>();
        }
    }

    //public static class ModelObjectOrderExtenions {
    //    public static IQueryable<Producer> Producers(this Core.Model.User user) {
    //        return user.Context.Orders().Where(o => o.User.Id == user.Id);
    //    }
    //}

    #endregion

}
