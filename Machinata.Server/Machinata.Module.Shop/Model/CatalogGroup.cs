using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using System.Data.Entity.Infrastructure;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class CatalogGroup : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

      
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public CatalogGroup() {
            this.Catalogs = new List<Catalog>();
            //this.Businesses = new List<Business>();
            this._businessCatalogGroup = new List<BusinessCatalogGroup>();
        }


        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string Name { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        [Index(IsUnique = true)]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Description { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////
        
        [InverseProperty("Group")]
        public ICollection<Catalog> Catalogs { get; set; }

        [InverseProperty("CatalogGroup")]
        public ICollection<BusinessCatalogGroup> _businessCatalogGroup { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        
        public IEnumerable<Business> Businesses {
            get {
                return this._businessCatalogGroup.Select(bcg => bcg.Business);
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
            if (System.Web.HttpUtility.UrlEncode(ShortURL) != ShortURL) {
                throw new BackendException("invalid-short-url", "The short url is invalid");
            }
        }

        public void RemoveBusiness(Business business) {
            if(this.Context != null) this.LoadFirstLevelNavigationReferences();
            if (!this.Businesses.Contains(business)) throw new BackendException("no-business", "The catalog group does not contain this business.");
            var bcg = this._businessCatalogGroup.SingleOrDefault(b => b.Business == business);
            bcg.Context.DeleteEntity(bcg);
        }

        public void AddBusiness(Business business) {
            if(this.Context != null) this.LoadFirstLevelNavigationReferences();
            if (this.Businesses.Contains(business)) throw new BackendException("already-has-business", "The group catalog already contains this business");
            var newBusinessCatalogGroup = new Model.BusinessCatalogGroup() { Business = business, CatalogGroup = this };
            this._businessCatalogGroup.Add(newBusinessCatalogGroup);
        }

        public void RemoveCatalog(Catalog catalog) {
            this.LoadFirstLevelNavigationReferences();
            if (!this.Catalogs.Contains(catalog)) throw new BackendException("no-catalog", "The group catalog does not contain this catalog.");
            this.Catalogs.Remove(catalog);
        }

        public void AddCatalog(Catalog catalog) {
            this.LoadFirstLevelNavigationReferences();
            if (this.Catalogs.Contains(catalog)) throw new BackendException("already-has-business", "The group catalog already has this catalog.");
            this.Catalogs.Add(catalog);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion
        
        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        public override Core.Cards.CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Description)
                .Sub(this.ShortURL)
                .Grouping()
                .Icon("");
        }

        public override void Populate(IPopulateProvider provider, FormBuilder form) {
            base.Populate(provider, form);
            if (string.IsNullOrWhiteSpace(this.ShortURL)) {
                this.ShortURL = Core.Util.String.CreateShortURLForName(this.Name);
            }
        }

      

        #endregion
    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContexBusinessGroupExtenions {
        [ModelSet]
        public static DbSet<CatalogGroup> CatalogGroups(this Core.Model.ModelContext context) {
            return context.Set<CatalogGroup>();
        }

        public static CatalogGroup CatalogGroupByUrl(this Core.Model.ModelContext context, string shortUrl) {
            return context.Set<CatalogGroup>().SingleOrDefault(cg=>cg.ShortURL == shortUrl);
        }

        public static DbQuery<CatalogGroup> CatalogGroupsWithBusinesses(this Core.Model.ModelContext context) {
            return context.Set<CatalogGroup>().Include("_businessCatalogGroup.Business");
        }

        public static IQueryable<CatalogGroup> GetCatalogGroupsForBusiness(this Core.Model.ModelContext context, Business business) {
            var groups =  context.BusinessCatalogGroups()
                 .Include(nameof(BusinessCatalogGroup.CatalogGroup))
                 .Include(nameof(BusinessCatalogGroup.CatalogGroup)+"."+nameof(CatalogGroup.Catalogs)+"."+nameof(Catalog.Products)+"."+nameof(Product.Configurations))
                 .Include(nameof(BusinessCatalogGroup.Business))
                 .Where(bc => bc.BusinessId == business.Id).Select(bcg => bcg.CatalogGroup);
            
            return groups;
        
        }

       
    }

    //public static class ModelObjectOrderExtenions {
    //    public static IQueryable<Order> Orders(this Core.Model.User user) {
    //        return user.Context.Orders().Where(o => o.User.Id == user.Id);
    //    }
    //}

    #endregion

}
