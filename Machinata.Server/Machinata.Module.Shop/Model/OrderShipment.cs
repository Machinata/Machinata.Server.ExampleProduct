using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Machinata.Core.Model;
using Machinata.Core.Builder;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass] 
    public partial class OrderShipment : ModelObject {

        public enum ShipmentStatus : short{ 
            Created = 10,
            PartiallyShipped = 20,
            Shipped = 30
        }    

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderShipment() {
            OrderItems = new List<OrderItem>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public ShipmentStatus Status { get; set; }

        [Column]
        public string Speed { get; set; }

        [Column]
        public string Infos { get; set; }
       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public DateTime Shipped { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        public string TrackingLink { get; set; }

        [Column]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? EstimatedDelivery { get; set; }
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<OrderItem> OrderItems { get; set; }

        [Column]
        public Order Order { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        public string GetPDFFileName() {
            return $"{Core.Config.ProjectID}_DeliverySlip_{this.Order.SerialId}.pdf";
        }

        public override string ToString() {
            return "{text.shipment}";
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderShipmentExtenions {
        [ModelSet]
        public static DbSet<OrderShipment> OrderShipments(this Core.Model.ModelContext context) {
            return context.Set<OrderShipment>();
        }
    }

    #endregion
}
