using System;
using Machinata.Core.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Machinata.Core.Builder;
using System.Linq;
using Machinata.Core.Util;
using System.ComponentModel;

namespace Machinata.Module.Shop.Model {

   
    public class OrderError : ModelObject {
        [FormBuilder(Forms.API.VIEW)]
        public string Code { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public string Message { get; set; }
    }
}
