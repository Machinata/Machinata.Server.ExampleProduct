using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Machinata.Core.Handler;
using Machinata.Core.Templates;

using Machinata.Module.Shop.Model;

namespace Machinata.Core.Model {



    /// <summary>
    /// Product Content Node
    /// The Admin interface allows a user to select a product from any catalog and add it as a node.
    /// The node value is the product id.
    /// Additional options stored:
    ///     product-name
    ///     product-public-id
    ///     catalog-name
    ///     catalog-short-url
    ///     catalog-public-id
    /// Note: These options are baked, and may become invalid if the product changes on the backend.
    /// 
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ContentNodeTypeMeta" />
    public class ContentNodeTypeProduct : ContentNodeTypeMeta {

        public const string NODE_TYPE_PRODUCT = "product";

        public override string GetID() { return NODE_TYPE_PRODUCT; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }

        public override void InsertAdditionalVariables(ContentNode node, PageTemplate template, string variableName) {
            base.InsertAdditionalVariables(node, template, variableName);

            using (var db = Core.Model.ModelContext.GetModelContext(null)) {

                // Load from DB
                var product = db.Products()
                    .IncludeContent(true)
                    .GetByPublicId(node.Value);
                var configuration = product.GetDefaultConfiguration();

                // First thumbnail, if any// Load data
                if (template.HasVariable(variableName + ".product.thumbnail")) {
                    product.Thumbnails?.IncludeContent();
                    var thumbnail = product.Thumbnails?.TranslationForLanguage(template.Language)?.Images().FirstOrDefault()?.Value;
                    template.InsertVariable(variableName + ".product.thumbnail", thumbnail);
                }

                // Title
                if (template.HasVariable(variableName + ".product.title")) {
                    product.Title?.IncludeContent();
                    var title = product.Title?.TranslationForLanguage(template.Language)?.Title;
                    template.InsertVariable(variableName + ".product.title", title);
                }

                // Generic entity varialbes
                template.InsertVariables(variableName + ".product", product);
                template.InsertVariables(variableName + ".product.configuration", configuration);

            }
        }

        public override string GetIcon() {
            return "product";
        }
    }

}
