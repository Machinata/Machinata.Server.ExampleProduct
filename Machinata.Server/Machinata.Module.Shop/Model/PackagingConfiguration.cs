using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using static Machinata.Module.Shop.View.PageTemplateExtension;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass]
    public class PackagingConfiguration : ModelObject {

      
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public PackagingConfiguration() {
            this.Configuration = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Properties Configuration { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////



        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Business Business { get; set; }

        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public PackagingDesign Design { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
 
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////




        #endregion

        #region Override Methods ///////////////////////////////////////////////////////////////////


        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextPackagingConfigurationExtenions {

        [ModelSet]
        public static DbSet<PackagingConfiguration> PackagingConfigurations(this Core.Model.ModelContext context) {
            return context.Set<PackagingConfiguration>();
        }
    }
    #endregion

    


}

