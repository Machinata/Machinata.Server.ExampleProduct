using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using System.Data.Entity.Infrastructure;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class Category : ModelObject, IShortURLModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        /// <summary>
        /// These are the possible types of a category
        /// </summary>
        public enum CategoryTypes : short {
            /// <summary>
            /// The main
            /// </summary>
            Main = 10,
            /// <summary>
            /// The sub
            /// </summary>
            Sub = 20,
            Tag = 30
        }

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Category() {
        }

        public static Category FindOrCreateMainCategory(ModelContext db, string categoryName) {
          
            var categoriy = db.Categories().SingleOrDefault(c => c.Name == categoryName);
            if (categoriy == null) {
                categoriy = new Category();
                categoriy.Name = categoryName;
                categoriy.ShortURL = Core.Util.String.CreateShortURLForName(categoriy.Name);
                categoriy.CategoryType = Category.CategoryTypes.Main;
                db.Categories().Add(categoriy);
                db.SaveChanges(); // otherwise not in db.Categories for next product
               
            }
            return categoriy;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [MinLength(3)]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        [Required]
        public string Name { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]

        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [Required]
        public CategoryTypes CategoryType { get; set; }
        
        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////
        [Column]
        [InverseProperty("Categories")]
        public ICollection<Product> Products { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ContentNode Photos { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ContentNode Thumbnails { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [Index(IsUnique = true)]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
      


        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override Core.Cards.CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Description?.Summary)
                .Icon("chart-pie");
        }

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }

        public override void Populate(IPopulateProvider provider, FormBuilder form) {
            base.Populate(provider, form);
            this.ShortURL = Core.Util.String.CreateShortURLForName(this.Name);
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextCategoryExtenions {

        [ModelSet]
        public static DbSet<Category> Categories(this Core.Model.ModelContext context) {
            return context.Set<Category>();
        }
        
        public static DbQuery<Category> IncludeProducts(this DbSet<Category> entities) {
            return entities
                .Include($"{nameof(Category.Products)}")
                .Include($"{nameof(Category.Products)}.{nameof(Product.Configurations)}")
                .Include($"{nameof(Category.Products)}.{nameof(Product.Thumbnails)}")
                .Include($"{nameof(Category.Products)}.{nameof(Product.Photos)}")
                .Include($"{nameof(Category.Products)}.{nameof(Product.Title)}")
            ;
        }
    }


    #endregion

}
