using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using NLog;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class OrderSubscription : ModelObject {
        
        public enum SubscriptionCycleTypes : short {
            Daily = 10,
            Weekly = 20,
            BiWeekly = 30,
            Monthly = 40
        }

        [Flags]
        public enum SubscriptionTimeTypes : short {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
        

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderSubscription() {
            this.Orders = new List<Order>();
            this.SubscriptionItems = new List<OrderSubscriptionItem>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        public User User { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Business Business { get; set; }

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Active { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public SubscriptionCycleTypes SubscriptionCycle { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public SubscriptionTimeTypes SubscriptionTime { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string SerialId { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public ICollection<Order> Orders { get; set; }

        [CascadeDelete]
        [Column]
        public ICollection<OrderSubscriptionItem> SubscriptionItems { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public string Title
        {
            get
            {
                return $"{SubscriptionCycle} on {SubscriptionTime}";
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Model Hooks ////////////////////////////////////////////////////////////////////

        //[OnModelObjectAddToDB(typeof(Order))]
        //public static bool OnModelObjectAddToDB(ModelContext db, Order order) {
        //    order.AddHistory("hook method called from p&l package", null);
        //    return true;
        //}

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Gets the possible delivery dates for this OrderSubscription. Checks dates between the given start and end date
        /// Does check the orders already made
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">subscription-cycle-error</exception>
        public List<OrderSubscriptionDate> GetPossibleDeliveryDatesForRange(DateTime start, DateTime end) {
           
            int totalDays = (int) System.Math.Floor((end - start).TotalDays);
            var dates = new List<OrderSubscriptionDate>();

            DateTime? lastDate = null;
            for (int i = 0; i <= totalDays; i++) {
                var newDate = start.AddDays(i);
                if (GetSubscriptionTimeTypeForWeekday(newDate.DayOfWeek) == SubscriptionTime) {
                    if (SubscriptionCycle == SubscriptionCycleTypes.Monthly) {
                        if (lastDate != null && (newDate.Day<=7 && lastDate.Value.IsDateInLaterMonth(newDate))) {
                            dates.Add(new OrderSubscriptionDate() { Date = newDate });
                        }
                        lastDate = newDate;
                    } else if (SubscriptionCycle == SubscriptionCycleTypes.Weekly) {
                        if (lastDate == null || newDate - lastDate.Value > new TimeSpan(6, 0, 0, 0)) {
                            dates.Add(new OrderSubscriptionDate() { Date = newDate });
                            lastDate = newDate;
                        }
                    } else {
                        throw new BackendException("subscription-cycle-error", $"{SubscriptionCycle} is not supported");
                    }
                }
            }

            return dates;
        }

        public List<OrderSubscriptionDate> GetUpcomingDeliveryDates() {
            return this.GetPossibleDeliveryDatesForRange(DateTime.UtcNow.Date, DateTime.UtcNow.Date.AddMonths(6));
        }
       

        /// <summary>
        /// Generateds orders for given dates, if a catalog for the date exists.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="dates">The dates.</param>
        /// <returns></returns>
        public IEnumerable<Order> GeneratedOrdersForDates(ModelContext db, IEnumerable<DateTime> dates, StringBuilder errorReport = null) {
            this.Include(nameof(Business)); 
            var orders = new List<Order>();

            //this.Include(nameof(Orders));
            //var orderDates = Orders.Select(o => o.DeliveryDate.Date);

            //if(errorReport == null) {
            //    errorReport = new StringBuilder();
            //}

            //// TODO: micha, if order creation fails -> add to error report and create the other orders

            //foreach (var date in dates) {
               
            //    //TODO: @micha seems to be a problem somehow
            //    //if (orderDates.Contains(date.Date)) {
            //    //   throw new BackendException("subscription-error", $"Could not create order for date: {Time.ToDefaultTimezoneDateString(date)}. Order at this date already exists");
            //    //}
            //    var catalogs = Catalog.GetCatalogsForBusiness(db, this.Business, date);
            //    if (catalogs.Any()) {
            //        try {
            //            var order = this.GenerateOrder(db, date, catalogs);
            //            orders.Add(order);
            //            // Adding to DB
            //            db.Orders().Add(order);
            //        } catch (BackendException be) {
            //            // todo: if in task send email
            //            // on admin gui show log
            //            _logger.Error($"Could not create an order on date: {Time.ToDefaultTimezoneDateString(date)} for subscription: {SerialId}, date: {date}");
            //            throw be;
            //        }
            //    } else {
            //        errorReport.AppendLine($"Did not find a catalog for {Time.ToDefaultTimezoneDateString(date)}");
            //    }

            //}

            return orders;
        }

        public static SubscriptionTimeTypes GetSubscriptionTimeTypeForWeekday(DayOfWeek dayOfWeek) {
            if (dayOfWeek == DayOfWeek.Monday) return SubscriptionTimeTypes.Monday;
            if (dayOfWeek == DayOfWeek.Tuesday) return SubscriptionTimeTypes.Tuesday;
            if (dayOfWeek == DayOfWeek.Wednesday) return SubscriptionTimeTypes.Wednesday;
            if (dayOfWeek == DayOfWeek.Thursday) return SubscriptionTimeTypes.Thursday;
            if (dayOfWeek == DayOfWeek.Friday) return SubscriptionTimeTypes.Friday;
            if (dayOfWeek == DayOfWeek.Saturday) return SubscriptionTimeTypes.Saturday;
            if (dayOfWeek == DayOfWeek.Sunday) return SubscriptionTimeTypes.Sunday;
            throw new BackendException("day-not-supported", $"{dayOfWeek} is not supported");
        }


        private Order GenerateOrder(ModelContext db, DateTime delivery, IEnumerable<Catalog> catalogs) {
          
            this.Include(nameof(SubscriptionItems));
            
            // Possible Products from catalogs
            var products = new List<Product>();
            foreach(var catalog in catalogs) {
                catalog.Include(nameof(catalog.Products));
                products.AddRange(catalog.Products);
            }

            products.ForEach(p => p.Include(nameof(p.Categories)));

            var order = new Order();
            order.DeliveryDate = delivery;
            order.Business = Business;
          
          // todo  
            //  order.InitializeBusinessOrder(db, ,db.SystemUser());

            // Forward order status until status is confirmed
            // TODO: micha, if credit card we have to have to credit card infos
            do {
                var nextStatus = order.GetNextStatus();
                if (!nextStatus.HasValue) {
                    throw new BackendException("subscription-order-error", "Could not set order " + order.PublicId + " to confirmed");
                }
                order.ChangeStatus(nextStatus.Value, db.Users().SystemUser());
            }
            while (order.Status != Order.StatusType.Confirmed);

            // Items
            foreach (var item in SubscriptionItems) {

                item.Include(nameof(item.Categories));
                // Find a matching product containing all categories of the subscription item
                var possibleProducts = products.Where(p => item.Categories.All(c => p.Categories.Contains(c)));
               
                // Is there a match?
                if (!possibleProducts.Any()) {
                    throw new BackendException("subscription-order-error", $"Did not find a matching product for subscription: '{SerialId}' item: '{item.Title}' for order on: '{Time.ToDefaultTimezoneDateString(delivery)}'");
                }

              //  var orderItem = order.AddOrderItem(db, possibleProducts.FirstOrDefault(), item.Quantity);
            }

            // Keep track of generated orders
            Orders.Add(order);
            
            return order;
        }



        #endregion

        #region Public Override Methods /////////////////////////////////////////////////////////////////

        public override int TypeNumber { get { return 26; } }

       

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContexOrderSubscriptionsExtenions {
        [ModelSet]
        public static DbSet<OrderSubscription> OrderSubscriptions(this Core.Model.ModelContext context) {
            return context.Set<OrderSubscription>();
        }
    }

    //public static class ModelObjectOrderExtenions {
    //    public static IQueryable<Order> Orders(this Core.Model.User user) {
    //        return user.Context.Orders().Where(o => o.User.Id == user.Id);
    //    }
    //}

    #endregion

    public class OrderSubscriptionDate : ModelObject {
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Weekday { get { return this.Date.DayOfWeek.ToString(); } }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Month { get { return this.Date.ToString("MMMM"); } }

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public int DaysUntil {
            get {
                return (int)((this.Date - DateTime.UtcNow).TotalDays);
            }
        }

    }

}
