using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Data.Entity.Infrastructure;
using Machinata.Module.Shop.VAT;
using Machinata.Core.Cards;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass]
    public partial class Product : ModelObject, IPublishedModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Product() {
            this.Categories = new List<Category>();
            this.Catalogs = new List<Catalog>();
            this.Details = new Properties();
            this.Information = new ProductInformation();
            this.Configurations = new List<ProductConfiguration>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public bool Published { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [CascadeDelete]
        [ContentType("title")]
        public ContentNode Title { get; set; }

      
        [Column]
        public ProductInformation Information { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [CascadeDelete]
        public ContentNode Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [CascadeDelete]
        public ContentNode Ingredients { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [CascadeDelete]
        public ContentNode Preparation { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [CascadeDelete]
        public ContentNode NutritionFacts { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [ContentType("image")]
        [CascadeDelete]
        public ContentNode Photos { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [ContentType("image")]
        [CascadeDelete]
        public ContentNode Thumbnails { get; set; }

        [Column]
        public ICollection<Catalog> Catalogs { get; set; }


        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [PropertiesConfigKey("ShopProductDetailsCustomKeys")]
        public Properties Details { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string Notes { get; set; }

        //Todo
        //[Column]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //public int Sort { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public ICollection<Category> Categories { get; set; }

        [Column]
        public Producer Producer { get; set; }


        [Column]
        public ICollection<Product> Related { get; set; }

        [Column]
        public ICollection<Product> Variations { get; set; }

        [CascadeDelete]
        [Column]
        public ICollection<ProductConfiguration> Configurations { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.LISTING)]
        public string DefaultSKU
        {
            get
            {
                var defaultConfiguration = this.GetDefaultConfiguration();
                return defaultConfiguration?.SKU;
            }
        }


        /// <summary>
        /// Gets the merged configuration. (Default Configuration overloaded with Business or NonBusinesses Configuration)
        /// TODO: Get ValueType Configuration by Type
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">To get the product configuration, the Configurations property must be loaded or the defail configruation must exist!</exception>
        public ProductConfiguration GetConfiguration(ProductConfiguration configuration) {
            if (this.Context != null) {
                this.Include(nameof(this.Configurations));
            }
            // Sanity
            if (this.Configurations == null || this.Configurations.Count == 0) throw new Exception("To get the product configuration, the Configurations property must be loaded or the defail configruation must exist!");

            // Get the default values (Business == null) (override if exists)
            ProductConfiguration defaultConfiguration = GetDefaultConfiguration();

            var ret = MergeConfigurations(defaultConfiguration, configuration);
           
            // Return
            return ret;
        }


        private ProductConfiguration MergeConfigurations(ProductConfiguration basicConfiguration, ProductConfiguration overloadConfiguration) {
            var merged = new ProductConfiguration();

            // Retail Price
            if (overloadConfiguration != null && overloadConfiguration.RetailPrice.HasValue && overloadConfiguration.RetailPrice.Value != 0) {
                merged.RetailPrice = overloadConfiguration.RetailPrice.Clone();
            }
            else {
                merged.RetailPrice = basicConfiguration.RetailPrice.Clone();
            }

            // Customer Price
            if (overloadConfiguration != null && overloadConfiguration.CustomerPrice.HasValue && overloadConfiguration.CustomerPrice.Value != 0) {
                merged.CustomerPrice = overloadConfiguration.CustomerPrice.Clone();
            }else {
                merged.CustomerPrice = basicConfiguration.CustomerPrice.Clone();
            }

            // Internal Price
            if (overloadConfiguration != null && overloadConfiguration.InternalPrice.HasValue && overloadConfiguration.InternalPrice.Value != 0) {
                merged.InternalPrice = overloadConfiguration.InternalPrice.Clone();
            }
            else {
                merged.InternalPrice = basicConfiguration.InternalPrice.Clone();
            }

            // Customer VAT
            if (overloadConfiguration != null && overloadConfiguration.CustomerVATRate.HasValue && overloadConfiguration.CustomerPrice.Value != 0) {
                merged.CustomerVATRate = overloadConfiguration.CustomerVATRate;
            }
            else {
                merged.CustomerVATRate = basicConfiguration.CustomerVATRate;
            }

            // Retail VAT
            if (overloadConfiguration != null && overloadConfiguration.RetailVATRate.HasValue && overloadConfiguration.RetailVATRate.Value != 0) {
                merged.RetailVATRate = overloadConfiguration.RetailVATRate;
            } else {
                merged.RetailVATRate = basicConfiguration.RetailVATRate;
            }
            

            // Min order amount
            if (overloadConfiguration != null && overloadConfiguration.MinimumOrderAmount.HasValue) {
                merged.MinimumOrderAmount = overloadConfiguration.MinimumOrderAmount;
            }else {
                merged.MinimumOrderAmount = basicConfiguration.MinimumOrderAmount;
            }

            // Barcode
            if (overloadConfiguration != null && string.IsNullOrEmpty(overloadConfiguration.BarCode) == false) {
                merged.BarCode = overloadConfiguration.BarCode;
            } else {
                merged.BarCode = basicConfiguration.BarCode;
            }

            // Title
            if (overloadConfiguration != null && !string.IsNullOrEmpty(overloadConfiguration.Title)) {
                merged.Title = overloadConfiguration.Title;
            } else if (string.IsNullOrEmpty(basicConfiguration.Title) == false) {
                merged.Title = basicConfiguration.Title;
            } else {
                // Fallback to Product name
                merged.Title = basicConfiguration.Product?.Name;
            }

            // SKU
            if (overloadConfiguration != null && !string.IsNullOrEmpty(overloadConfiguration.SKU)) {
                merged.SKU = overloadConfiguration.SKU;
            } else {
                merged.SKU = basicConfiguration.SKU;
            }

            // Stock Quantity
            if (overloadConfiguration != null && overloadConfiguration.StockQuantity != null) {
                merged.StockQuantity = overloadConfiguration.StockQuantity;
            }else {
                merged.StockQuantity = basicConfiguration.StockQuantity;
            }

            // Origin Public Id
            if (overloadConfiguration != null) {
                merged.OriginPublicId = overloadConfiguration.PublicId;
            } else {
                merged.OriginPublicId = basicConfiguration.PublicId;
            }

            // Product 
            merged.Product = basicConfiguration.Product;

            return merged;
        }

        public ProductConfiguration GetDefaultConfiguration() {
            if (this.Context != null) {
                this.Include(nameof(this.Configurations));
            }
            return this.Configurations.SingleOrDefault(c => c.IsDefault);
        }

        /// <summary>
        /// Gets the business configuration, not merged with default!
        /// </summary>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        public ProductConfiguration GetBusinessConfiguration(Business business) {
            this.Include($"{nameof(this.Configurations)}");
            this.Configurations.ToList().ForEach(c => c.Include(nameof(c.Business)));
            return this.Configurations.SingleOrDefault(c => c.Business != null && c.Business.Id == business.Id);
        }

        /// <summary>
        /// Gets the merged configuration. (Default Configuration overloaded with Business Configuration)
        /// TODO: Get ValueType Configuration by Type
        /// </summary>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">To get the product configuration, the Configurations property must be loaded or the defail configruation must exist!</exception>
        public ProductConfiguration GetMergedBusinessConfiguration(Business business) {
            var businessConfgiration = GetBusinessConfiguration(business);
            return GetConfiguration(businessConfgiration);
        }
        

        /// <summary>
        /// Gets the the merged configuration for the business if available otherwise the default
        /// </summary>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        public ProductConfiguration GetConfigurationForBusiness(Business business) {
            ProductConfiguration configurationForBusiness = null;
            var businessConfiguration = this.GetMergedBusinessConfiguration(business);
            if (businessConfiguration != null) {
                configurationForBusiness = businessConfiguration;
            } else {
                configurationForBusiness = this.GetDefaultConfiguration();
            }
            return configurationForBusiness;
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelCreating]
        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
            modelBuilder.Entity<Product>().HasMany(m => m.Variations)
                .WithMany()
                .Map(
                    p=> {
                        p.ToTable("ProductVariations");
                        p.MapLeftKey("Product_Id");
                        p.MapRightKey("Variation_Id"); }
            );


            modelBuilder.Entity<Product>().HasMany(m => m.Related)
                .WithMany()
                .Map(
                    p => { p.ToTable("ProductRelated");
                        p.MapLeftKey("Product_Id");
                        p.MapRightKey("Related_Id"); }
            );
        }
        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }

        public override Core.Cards.CardBuilder VisualCard() {
            return CreateVisualCard(this.Name, this.Title?.ToString());
        }

        public CardBuilder CreateVisualCard(string title, string subtitle) {
            // Init card
            var card = new Core.Cards.CardBuilder(this)
                .Title(title)
                .Subtitle(subtitle)
                .Wide()
                .Icon("tag");
            // Add categories
            this.LoadFirstLevelNavigationReferences();
            foreach (var cat in this.Categories) {
                card.Sub(cat.Name);
            }
            return card;
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public ProductConfiguration AddConfiguration(bool isDefault) {
            var configuration = new ProductConfiguration();
          
            if (this.Context!= null) {
                this.Include(nameof(Configurations));
                this.Configurations.ToList().ForEach(c => c.Include(nameof(ProductConfiguration.Business)));
            }
         
            this.Configurations.Add(configuration);

            // Leave empty if not default configuration
            if (isDefault) {
                configuration.CustomerVATRate = VATProvider.GetVATProvier().DefaultVATForProducts();
                configuration.RetailVATRate = VATProvider.GetVATProvier().DefaultVATForProducts();
                configuration.CustomerPrice = new Price(0);
                configuration.InternalPrice = new Price(0);
            } else {
                configuration.CustomerPrice = new Price();
                configuration.RetailPrice = new Price();
            }

            // Tyoe
            configuration.IsDefault = isDefault;

            return configuration;
        }

        public void RemoveCategory(Category category) {
            if (!this.Categories.Contains(category)) throw new BackendException("no-category", "The product does not have this category.");
            this.Categories.Remove(category);
        }

        public void AddCategory(Category category) {
            if (this.Categories.Contains(category)) throw new BackendException("already-has-product", "The product already has this category.");
            this.Categories.Add(category);
        }

        public void RemoveVariation(Product variation) {
            if (!this.Variations.Contains(variation)) throw new BackendException("no-variation", "The product does not have this variation.");
            this.Variations.Remove(variation);
        }

        public void AddVariation(Product variation) {
            if (this.Variations.Contains(variation)) throw new BackendException("already-has-variation", "The product already has this variation.");
            this.Variations.Add(variation);
        }


        public void RemoveRelated(Product related) {
            if (!this.Related.Contains(related)) throw new BackendException("no-related", "The product does not have this related product.");
            this.Related.Remove(related);
        }

        public void AddRelated(Product related) {
            if (this.Related.Contains(related)) throw new BackendException("already-has-related", "The product already has this related product.");
            this.Related.Add(related);
        }

        public void ValidateConfigurations() {
            if (this.Context != null) {
                this.Include(nameof(Configurations));
                this.Configurations.Where(c=>c.Context != null).ToList().ForEach(c => c.Include(nameof(ProductConfiguration.Business)));
            }

            // Check default
            if (this.Configurations.Count(c =>c.IsDefault) > 1) {
                throw new BackendException("add-error", "Cannot add another DEFAULT configuration. Please check existing");
            }

            // Check businesses TODO
            //var byBusiness = this.Configurations.Where(c=>c.Business != null).GroupBy(c => c.Business);
            //if (byBusiness.Any(bb=>bb.Count() > 1)) {
            //    throw new BackendException("add-error", "Cannot add another configuration for this business. Please check existing");
            //}

            // Type vs Values: todo
            //var valueConfigurations = this.Configurations.Where(c => c.Type >= ProductConfiguration.ConfigurationTypes.Size).GroupBy(c => c.Type);
            //foreach (var configGroup in valueConfigurations) {
            //    foreach (var config in configGroup) {
            //        if (configGroup.Count(cg => cg.Type == config.Type && cg.Value == config.Value) > 1) {
            //            throw new BackendException("add-error", "Cannot add another configuration with this Type/Value combination. Please check existing");
            //        }
            //    }
            //}

        }

        public string GetTitle(string language) {
            this.Title?.IncludeContent();
            var ret = this.Title?.TranslationForLanguage(language)?.Title;
            return ret;
        }

        public string GetDescription(string language) {
            this.Description?.IncludeContent();
            var ret = this.Description?.TranslationForLanguage(language)?.Summary;
            return ret;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        public override void Validate() {
            base.Validate();
            this.ValidateConfigurations();
        }

      

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProductExtenions {
        [ModelSet]
        public static DbSet<Product> Products(this Core.Model.ModelContext context) {
            return context.Set<Product>();
        }
    }

    public static class ProductExtenions {

        public static DbQuery<Product> IncludeContent(this DbSet<Product> products, bool listingOnly = false) {
            if(listingOnly) {
                return products
                    .Include($"{nameof(Product.Configurations)}")
                    .Include($"{nameof(Product.Thumbnails)}")
                    .Include($"{nameof(Product.Photos)}")
                    .Include($"{nameof(Product.Title)}")
                    //.Include($"{nameof(Product.Title)}.{nameof(ContentNode.Children)}")
                ;
            } else {
                return products
                    .Include($"{nameof(Product.Configurations)}")
                    .Include($"{nameof(Product.Thumbnails)}")
                    .Include($"{nameof(Product.Photos)}")
                    .Include($"{nameof(Product.Title)}")
                    //.Include($"{nameof(Product.Title)}.{nameof(ContentNode.Children)}")
                    .Include($"{nameof(Product.Description)}")
                    //.Include($"{nameof(Product.Description)}.{nameof(ContentNode.Children)}")
                    .Include($"{nameof(Product.Catalogs)}")
                    .Include($"{nameof(Product.Categories)}")
                ;
            }
        }
        

        public static DbQuery<Product> IncludeCatalogs(this DbSet<Product> products) {
            return products
                .Include($"{nameof(Product.Catalogs)}")
            ;
        }

        public static DbQuery<Product> IncludeCatalogs(this DbQuery<Product> products) {
            return products
                .Include($"{nameof(Product.Catalogs)}")
            ;
        }
    }

    #endregion



}
