using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {


    public class ManualShipmentProvider : IShipmentProvider {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ManualShipmentProvider() {

        }

        #endregion



        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string Name { get; } = "manual";

        public Tuple<int, int> CalculateShipmentTime(Order order) {

            throw new NotImplementedException("todo");
        
                // By country
                var configKeyA = $"ShopShippingDaysCountry{order.ShippingAddress.CountryCode}";
                var configRateA = Core.Config.Dynamic.String(configKeyA, false);
                if (configRateA != null) {
                    return GetShippingTime(configRateA.Split(','));
                }
           

                // find a region
                var regions = CountryCodes.GetRegionsForCountry(order.ShippingAddress.CountryCode);
                var configKeyB = $"ShopShippingDaysRegion{order.ShippingAddress.CountryCode}";
                var configRateB = Core.Config.Dynamic.String(configKeyB, false);
                if (regions != null && regions.Any() && configRateB != null) {
                    return GetShippingTime(configRateB.Split(','));
                } else {
                    throw new Exception($"No shipping costs for this country exists ({configKeyA} or {configKeyB})!");
                }
           
        }

        public Price CalculateCustomerShipmentCosts(Order order) {

            // Shipping address not set
            if (order.ShippingAddress == null) { return new Price(0, order.Currency); }

            // By country
            var configKeyA = $"ShopShippingCostsCountry{order.ShippingAddress.CountryCode}";
            var configRateA = Core.Config.Dynamic.Price(configKeyA, false);
            if (configRateA != null) {
                if (configRateA.Value < 0m) throw new Exception($"The shipping costs for {configKeyA} ({configRateA}) are not valid!");
                return configRateA.ConvertToCurrency(order.Currency);
            }

            // find a region
            var regions = CountryCodes.GetRegionsForCountry(order.ShippingAddress.CountryCode);

            foreach (var region in regions) {
                var configKeyB = $"ShopShippingCostsRegion{region}";
                var configRateB = Core.Config.Dynamic.Price(configKeyB, false);
                if (configRateB != null && configRateB.Value >= 0m) {
                    return configRateB.ConvertToCurrency(order.Currency);
                }
            }

            throw new Exception($"No valid shipping cost for this country was found ({order.ShippingAddress.CountryCode} or {string.Join(", ", regions)})!");

        }

        public Price CalculateInternalShipmentCosts(Order order) {
            return new Price(0);
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////
        private static Tuple<int, int> GetShippingTime(IEnumerable<string> configList) {
            var min = configList.FirstOrDefault();
            var max = configList.LastOrDefault();
            int minRes = -1;
            int maxRes = -1;
            int.TryParse(min, out minRes);
            int.TryParse(max, out maxRes);

            if (minRes < 0 || maxRes < 0) {
                throw new Exception("No shipping time defined");
            }

            return new Tuple<int, int>(minRes, maxRes);
        }
        #endregion

    }
}
    
   

