using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;

namespace Machinata.Module.Shop.Model {


    public class FreeShipmentProvider : IShipmentProvider {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public FreeShipmentProvider() {

        }

        #endregion



        #region Public Methods ////////////////////////////////////////////////////////////////////

        public virtual string Name { get; } = "free";

        public virtual Price CalculateCustomerShipmentCosts(Order order) {
            return new Price(0, order.Currency);
        }

        public virtual Price CalculateInternalShipmentCosts(Order order) {
            return new Price(0); // also zero?
        }

        public virtual Tuple<int, int> CalculateShipmentTime(Order order) {
            throw new NotImplementedException();
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
}
    
   

