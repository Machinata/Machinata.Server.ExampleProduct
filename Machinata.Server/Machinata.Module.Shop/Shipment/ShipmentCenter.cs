using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;

namespace Machinata.Module.Shop.Model {


    public class ShipmentCenter {
        
        public static Price CalculateInternalShippingCosts(Order order) {
            return ShipmentProvider.CalculateInternalShipmentCosts(order);
        }
        public static Price CalculateCustomerShippingCosts(Order order) {
            return ShipmentProvider.CalculateCustomerShipmentCosts(order);
        }

        public static Tuple<int,int> CalculateShipmentTime(Order order) {
            return ShipmentProvider.CalculateShipmentTime(order);
        }


        public static IShipmentProvider ShipmentProvider
        {
            get
            {
                if (Config.ShopShipmentProvider == "free") {
                    return new FreeShipmentProvider();
                } else if (Config.ShopShipmentProvider == "manual") {
                    return new ManualShipmentProvider();
                }
                throw new Exception($"{Config.ShopShipmentProvider} is not supported");
            }
        }


    }
    public interface IShipmentProvider {

       
        #region Public Methods ////////////////////////////////////////////////////////////////////

        string Name { get; }
        Price CalculateCustomerShipmentCosts(Order order);
        Price CalculateInternalShipmentCosts(Order order);

        Tuple<int, int> CalculateShipmentTime(Order order);

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
}
    
   

