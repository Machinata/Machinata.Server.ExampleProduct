using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.VAT {

    /// <summary>
    /// A abstract class for providing various implementaions for VAT calculation.
    /// Two methods must be implemented:
    /// Order Total VAT: CalculateVATForOrder
    /// Order Item VAT: CalculateVATRateForOrderItem
    /// </summary>
    public abstract class VATProvider {

        /*
         * Work in progress: system for creating a exact model of the VAT calculation
         * See https://www.gate.estv.admin.ch/mwst-webpublikationen/public/pages/taxInfos/cipherDisplay.xhtml?publicationId=1002536&componentId=1002651&&winid=994247
        public class VATItem {
            public decimal VATRate;
            public Price VATAmount { get { throw new NotImplementedException(); } }
            public Price ItemsTotal;
        }*/

        /// <summary>
        /// Calculates the total VAT for an order (which is used by shopping carts et cetera).
        /// This method must take into account any shipping VATs, item VATs et cetera...
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public abstract Price CalculateVATForOrder(Order order);

        /// <summary>
        /// Calculates the VAT rate for a single order item.
        /// This method must take into account the shipping destination, item rate, et cetera.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public abstract decimal CalculateVATRateForOrderItem(Order order, OrderItem item);

        /// <summary>
        /// Returns a list of unique order item VAT rates.
        /// Helper method for determining a overall order VAT rate (typically an order has only one rate).
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public List<decimal> GetOrderItemVATRates(Order order) {
            var ret = new List<decimal>();
            foreach (var item in order.OrderItems) {
                var itemVATRate = CalculateVATRateForOrderItem(order, item);
                if (!ret.Contains(itemVATRate)) ret.Add(itemVATRate);
            }
            return ret;
        }

        public decimal? DefaultVATForProducts() {
            return Module.Shop.Config.ShopDefaultVATForProducts;
        }

        /// <summary>
        /// Gets the vat calculator for the current project
        /// </summary>
        /// <value>
        /// The vat calculator.
        /// </value>
        public static VATProvider GetVATProvier() {
            return new Providers.SwissVATProvider();
        }
    }


}
