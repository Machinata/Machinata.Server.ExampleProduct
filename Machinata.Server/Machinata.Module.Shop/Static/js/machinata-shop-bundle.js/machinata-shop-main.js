


/* ======== Machinata Shop ======== */

/*
{
  "type": "add-success",
  "data": {
    "order": {
      "serial-id": "20.18.00012",
      "hash": "bea191f4569d59b95613f147b148b952e59500428060432ccea878103dce0fa3",
      "delivery-date": "0001-01-01T00:00:00",
      "total-customer": 2952.00000000,
      "shipping-customer": 0.0,
      "subtotal-customer": 2880.00,
      "discount": 0.0,
      "vat-customer": 72.00000000,
      "items": [
        {
          "title": "JFY-RIN-QG-70-1.1-W52 JFY-RIN-QG-70-1.1-W52",
          "sku": "JFY-RIN-QG-70-1.1-W52",
          "quantity": 9,
          "customer-price": 320.00,
          "customer-vat-rate": 0.025000
        }
      ]
    }
  }
}
*/

Machinata.Shop = {};

Machinata.Shop.URLs = {};
Machinata.Shop.URLs.CART = "/cart";
Machinata.Shop.URLs.CHECKOUT = "/checkout";

Machinata.Shop.ORDER_EXPIRES_DAYS = 30;

Machinata.Shop.cart = function () {
    //TODO
    return $.cookie("OrderHash");
};

Machinata.Shop.addProductToCart = function (productId, configurationId, quantity, callback) {
    Machinata.debug("addToCart(productId=" + productId + ",configurationId=" + configurationId + ",quantity=" + quantity + ")");
   
    var data = {};
    data["productId"] = productId;
    data["configurationId"] = configurationId;
    data["quantity"] = quantity;
    var call = Machinata.apiCall("/api/shop/cart/add" );
    call.data(data);
    // Prepare api call
    call.success(function (message) {
        Machinata.debug("order hash :" + message.data["order"]);
        // Register cookie
        $.cookie("OrderHash", message.data["order"]["hash"], { expires: Machinata.Shop.ORDER_EXPIRES_DAYS, path: '/' });
        // Call bindings
        for (var i = 0; i < Machinata.Shop.UI._onItemAddedToCart.length; i++) Machinata.Shop.UI._onItemAddedToCart[i](message);
        // Callback on success
        if (callback != null) callback(message);
    })
    call.error(function (message) {
        
    });
    call.genericError();
    // Send
    call.send();
   
};

Machinata.Shop.removeItemFromCart = function (itemId,callback) {
    Machinata.debug("removeItemFromCart(itemId=" + itemId +")");
    var data = {};
    data["itemId"] = itemId;
    var call = Machinata.apiCall("/api/shop/cart/remove");
    call.data(data);
    // Prepare api call
    call.success(function (message) {
      
        // Callback on success
        if (callback != null) callback(message);
    })
    call.error(function (message) {

    });
    call.genericError();
    // Send
    call.send();
};

Machinata.Shop.updateItemQuantity = function (itemId, quantity, callback) {
    Machinata.debug("updateItemQuantity(itemId=" + itemId + ",quantity=" + quantity +")");
    var data = {};
    data["itemId"] = itemId;
    data["quantity"] = quantity;
    var call = Machinata.apiCall("/api/shop/cart/update-quantity");
    call.data(data);
    // Prepare api call
    call.success(function (message) {
        // Callback on success
        if (callback != null) callback(message);
    });
    call.genericError();
    // Send
    call.send();
};

Machinata.Shop.applyCoupon = function (code, callback) {
    Machinata.debug("applyCoupon(code=" + code +")");
    var data = {};
    data["code"] = code;
  
    var call = Machinata.apiCall("/api/shop/cart/apply-coupon");
    call.data(data);
    // Prepare api call
    call.success(function (message) {
        // Callback on success
        if (callback != null) callback(message);
    });
    call.genericError();
    // Send
    call.send();
};

Machinata.Shop.removeCoupon = function ( callback) {
    Machinata.debug("removeCoupon()");
    var data = {};
    var call = Machinata.apiCall("/api/shop/cart/remove-coupon");
    call.data(data);
    // Prepare api call
    call.success(function (message) {
        // Callback on success
        if (callback != null) callback(message);
    });
    call.genericError();
    // Send
    call.send();
};






/* ======== Machinata Shop UI ======== */

Machinata.Shop.UI = {};



Machinata.Shop.UI._onUpdateCart = [];
Machinata.Shop.UI.onUpdateCart = function (fn) {
    Machinata.Shop.UI._onUpdateCart.push(fn);
};

Machinata.Shop.UI._onItemAddedToCart = [];
Machinata.Shop.UI.onItemAddedToCart = function (fn) {
    Machinata.Shop.UI._onItemAddedToCart.push(fn);
};

Machinata.Shop.UI.updateCart = function (message) {
    // Order item count
    var cartItems = 0;
    cartItems = message.data.order["quantity"];
    var cartItemsText = "";
    if (cartItems > 0) cartItemsText = cartItems;
    $(".bb-shop-cart-item-count").text(cartItems).attr("data-count", cartItems);
    $(".bb-shop-cart-item-count-data-only").attr("data-count", cartItems);
    $(".bb-shop-cart-item-count-text").text(cartItemsText);
    // Order
    $(".bb-shop-cart-delivery-date").text(message.data.order["delivery-date"]);
    $(".bb-shop-cart-total-customer").text(message.data.order["total-customer"]);
    $(".bb-shop-cart-shipping-customer").text(message.data.order["shipping-customer"]);
    $(".bb-shop-cart-subtotal-customer").text(message.data.order["subtotal-customer"]);
    $(".bb-shop-cart-total-customer-excl-vat").text(message.data.order["total-customer-excl-vat"]);
    $(".bb-shop-cart-discount").text(message.data.order["discount"]);
    $(".bb-shop-cart-vat-customer").text(message.data.order["vat-customer"]);
    // Items
    var cartElem = $(".bb-shop-cart");
    if(cartElem.length > 0) {
        for (var i = 0; i < message.data.order.items.length; i++) {
            var item = message.data.order.items[i];
            var itemElem = cartElem.find(".bb-shop-order-item[data-order-item-id='" + item["public-id"] + "']");
            itemElem.find(".bb-shop-order-item-quantity").text(item["quantity"]).attr("data-count", item["quantity"]);
            itemElem.find(".bb-shop-order-item-quantity-data-only").attr("data-count", item["quantity"]);
        }
    }
    // Call bindings
    for (var i = 0; i < Machinata.Shop.UI._onUpdateCart.length; i++) Machinata.Shop.UI._onUpdateCart[i](message);
}

Machinata.Shop.UI.bind = function () {
    
    // Bind stripe checkout form
    //$("form.stripe-payment-form").submit(function (e) {
    //    Machinata.showLoading(true);
    //    this.submit();
    //});

    // Bind products
    $(".bb-shop-product").each(function () {
        var elem = $(this);
        elem.find(".bb-shop-addtocart").click(function () {
            var productId = elem.attr("data-product-id");
            var configurationId = elem.attr("data-configuration-id");
            var quantity = 1;
            if (elem.find(".bb-shop-quantity").length > 0) quantity = elem.find(".bb-shop-quantity").val();
            Machinata.Shop.addProductToCart(productId, configurationId, quantity, function (message) {
                Machinata.Shop.UI.updateCart(message);
            });
        });
        elem.find(".bb-shop-quantity-minus").click(function () {
            if (elem.find(".bb-shop-quantity").length > 0) {
                var quantity = parseInt(elem.find(".bb-shop-quantity").val());
                if (quantity > 1) quantity--;
                elem.find(".bb-shop-quantity").val(quantity).trigger("change");
            }
        });
        elem.find(".bb-shop-quantity-plus").click(function () {
            if (elem.find(".bb-shop-quantity").length > 0) {
                var quantity = parseInt(elem.find(".bb-shop-quantity").val());
                quantity++;
                elem.find(".bb-shop-quantity").val(quantity).trigger("change");
            }
        });
    });

    // Bind cart
    $(".bb-shop-cart").each(function () {
        var cartElem = $(this);
        // Remove
        cartElem.find(".bb-shop-removefromcart").click(function () {
            var itemElem = $(this).closest(".bb-shop-order-item");
            var itemId = itemElem.attr("data-order-item-id");
            Machinata.Shop.removeItemFromCart(itemId, function (message) {
                itemElem.remove();
                Machinata.Shop.UI.updateCart(message);
            });
        });
        // Quantity
        cartElem.find(".bb-shop-quantity").change(function () {
            var itemElem = $(this).closest(".bb-shop-order-item");
            var itemId = itemElem.attr("data-order-item-id");
            var quantity = $(this).val();
            Machinata.Shop.updateItemQuantity(itemId, quantity, function (message) {
                Machinata.Shop.UI.updateCart(message);
            });
        });
        cartElem.find(".bb-shop-quantity-minus").click(function () {
            var itemElem = $(this).closest(".bb-shop-order-item");
            if (itemElem.find(".bb-shop-quantity").length > 0) {
                var quantity = parseInt(itemElem.find(".bb-shop-quantity").val());
                if (quantity > 1) quantity--;
                itemElem.find(".bb-shop-quantity").val(quantity).trigger("change");
            }
        });
        cartElem.find(".bb-shop-quantity-plus").click(function () {
            var itemElem = $(this).closest(".bb-shop-order-item");
            if (itemElem.find(".bb-shop-quantity").length > 0) {
                var quantity = parseInt(itemElem.find(".bb-shop-quantity").val());
                quantity++;
                itemElem.find(".bb-shop-quantity").val(quantity).trigger("change");
            }
        });
        // Coupon dialog
        cartElem.find("a.bb-shop-enter-coupon").click(function () {
            Machinata.inputDialog("{text.order-coupon}", "{text.apply-order-coupon-description}", null, "{text.order-coupon-code}")
                .input(function (val,button) {
                    if (!String.isEmpty(val)) {
                        Machinata.Shop.applyCoupon(val, function (message) {
                            Machinata.reload();
                        });
                    }
                })
                .show();
        });
    });
    // Bind shipping-billing
    $(".bb-shop-shipping-billing-form").each(function () {
        var elem = $(this);
        var toggleCheckbox = elem.find(".bb-shop-toggle-shipping-billing-same");
        var updateForm = function () {
            if (toggleCheckbox.length > 0) {
                if (toggleCheckbox.is(":checked")) {
                    // Shipping/billing is the same
                    elem.find(".bb-shop-billing-address input[required]").each(function () {
                        $(this).attr("data-required", "true");
                        $(this).attr("required", null);
                    });
                    elem.find(".bb-shop-billing-address").hide();
                } else {
                    elem.find(".bb-shop-billing-address input[data-required]").each(function () {
                        if ($(this).attr("data-required") == "true") {
                            $(this).attr("required", "required");
                        } else {
                            $(this).attr("required", null);
                        }
                    });
                    elem.find(".bb-shop-billing-address").show();
                }
            }
        };
        updateForm();
        if (toggleCheckbox.length > 0) {
            toggleCheckbox.on("change", updateForm);
        }
    });
    
    // Bind shipping-billing
    $(".bb-shop-currency-convert").each(function () {
        var elem = $(this);
        var sourceCurrency = elem.attr("data-source-currency");
        var sourceAmount = elem.attr("data-source-amount");
        var targetCurrency = elem.attr("data-target-currency");
        var targetRounded = elem.attr("data-target-rounded");
        var targetFormat = elem.attr("data-target-format");
        if (targetFormat == null || targetFormat == "") targetFormat = "{converted}";

        Machinata.apiCall("/api/shop/currency/convert").data({
            "source-currency": sourceCurrency,
            "source-amount": sourceAmount,
            "target-currency": targetCurrency,
            "target-rounded": targetRounded
        }).success(function (message) {
            targetFormat = targetFormat.replace("{converted}", message.data["converted"]);
            targetFormat = targetFormat.replace("{converted.amount}", message.data["converted-amount"]);
            targetFormat = targetFormat.replace("{converted.currency}", message.data["converted-currency"]);
            elem.text(targetFormat);
        }).send();
    });
};





Machinata.ready(function () {
    Machinata.Shop.UI.bind();
});

Machinata.loaded(function () {
   
    // Incoming coupon?
    if (Machinata.queryParameter("coupon") != null && Machinata.queryParameter("coupon") != "") {
        // Register cookie
        $.cookie("OrderCoupon", Machinata.queryParameter("coupon"), { expires: Machinata.Shop.ORDER_EXPIRES_DAYS, path: '/' });
    }
});