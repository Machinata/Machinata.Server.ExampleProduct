using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop {
    public class Config {

        public static decimal? ShopDefaultVATForProducts = Core.Config.GetNullableDecimalSetting("ShopDefaultVATForProducts", null);

        public static string ShopOrderDeliveryTimeWindow = Core.Config.GetStringSetting("ShopOrderDeliveryTimeWindow", null);
        public static string ShopOrderDeliveryTimeValidDays = Core.Config.GetStringSetting("ShopOrderDeliveryTimeValidDays", null);
        public static int ShopInvoiceOrderDueDays = Core.Config.GetIntSetting("ShopInvoiceOrderDueDays", 14);

        // Round Foreign Currency Prices for Order Items
        public static bool ShopRoundForeignCurrencyPrices = Core.Config.GetBoolSetting("ShopRoundForeignCurrencyPrices");

        
        public static int ShopMinQuantityStock = Core.Config.GetIntSetting("ShopMinQuantityStock");

       
        public static string ShopShipmentProvider = Core.Config.GetStringSetting("ShopShipmentProvider");

        public static bool ShopOrderRequiresDeliveryDate = Core.Config.GetBoolSetting("ShopOrderRequiresDeliveryDate");

        public static bool ShopAutomaticallyDecreaseStockQuantity = Core.Config.GetBoolSetting("ShopAutomaticallyDecreaseStockQuantity");

        // Analytics Ecommerce
        public static bool ShopEnableSalesTracking = Core.Config.GetBoolSetting("ShopEnableSalesTracking");

        // Order Public Path
        public static string OrderPublicPath = Core.Config.GetStringSetting("OrderPublicPath", null);
        public static string OrderPublicId = Core.Config.GetStringSetting("OrderPublicId", null);

        // Default catalog group
        public static string DefaultCatalogGroupUrl = Core.Config.GetStringSetting("DefaultCatalogGroupUrl", null);

        // Email
        public static string OrderEmailSubject = Core.Config.GetStringSetting("OrderEmailSubject", "{text.order}: {order.serial-id} - {order.status}");
        public static bool ShopSendFulfillmentEmail = Core.Config.GetBoolSetting("ShopSendFulfillmentEmail");

        // Labels
        public static bool ShopPackagingEnabled = Core.Config.GetBoolSetting("ShopPackagingEnabled");

        // DeliverySlip
        public static bool ShopDeliverySlipShowPrices = Core.Config.GetBoolSetting("ShopDeliverySlipShowPrices");

        // Modes to print labels 
        // local -> printer is directly connected
        // remote -> send print commands to remote Machinata server 
        public static string ShopPackagingPrintMode = Core.Config.GetStringSetting("ShopPackagingPrintMode");

        // Shipping
        public static List<string> ShopShippingDaysFast = Core.Config.GetStringListSetting("ShopShippingDaysFast");
        public static List<string> ShopShippingDaysSlow = Core.Config.GetStringListSetting("ShopShippingDaysSlow");

        // Product Updater
        public static string ShopProductUpdater = Core.Config.GetStringSetting("ShopProductUpdater");
        public static class ShopProductUpaters {
            public static class CalcMenu {
                public static string ServiceURL = Core.Config.GetStringSetting("ShopCalcMenuServiceURL");
                public static string ServiceAccount = Core.Config.GetStringSetting("ShopCalcMenuServiceAccount");
                public static string ServicePassword = Core.Config.GetStringSetting("ShopCalcMenuServicePassword");
                public static string DefaultKiosk = Core.Config.GetStringSetting("ShopCalcMenuDefaultKiosk");
            }
        }
       

        // VAT included in OrderItems
        public static bool ShopVATIncluded = Core.Config.GetBoolSetting("ShopVATIncluded");

        // Use Merged Product Configration Title as OrderItem Title 
        public static bool ShopUseMergedConfigurationTitleForOrderItem = Core.Config.GetBoolSetting("ShopUseMergedConfigurationTitleForOrderItem");

    }
}
