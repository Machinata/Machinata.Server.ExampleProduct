using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Shop.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Machinata.Module.Shop.View {
    public static class PageTemplateExtension {



        public static PageTemplate GetItemDetailTemplate(this PageTemplate parentTemplate, OrderItem orderItem) {

            var template = parentTemplate.LoadTemplate("overview-item.detail");

            // Title
            template.InsertVariable("title", orderItem.Quantity + " x " + orderItem.Title);
            ProductConfiguration configuration = orderItem.UsedProductConfiguration;
            template.InsertPropertyList("configuration", configuration, new FormBuilder(Forms.Admin.VIEW).Exclude(nameof(ProductConfiguration.Infos)));

            // Images
            var product = orderItem.Product;
            product.Photos?.IncludeContent();
            var photos = product.Photos?.TranslationForLanguage(parentTemplate.Language)?.Images();
            template.InsertTemplates(
                variableName: "images",
                entities: photos != null ? photos : new List<ContentNode>(),
                templateName: "overview-item.detail.image",
                forEachEntity: new Action<ContentNode, PageTemplate>(delegate (ContentNode n, PageTemplate t) {
                    t.InsertImagesForContentNode(n);
                })
            );

            // Product LInk
            template.InsertVariables("product", product);

            template.InsertVariable("images.count", photos != null ? photos.Count().ToString() : 0.ToString());
            return template;
        }



        private static void InsertImagesForContentNode(this PageTemplate template, ContentNode node) {
            template.InsertVariable("image.src", node.Value);
        }

    }
    
}