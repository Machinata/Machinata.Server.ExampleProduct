using Machinata.Core.Messaging;
using Machinata.Core.Model;
using Machinata.Module.Finance.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.Tasks {

    public class MonthlyOrderInvoiceTask : Core.TaskManager.Task {
        public override void Process() {

            var orders = new List<Order>();
            var newInvoices = new List<Invoice>();
            var invoicesToSend = new List<Invoice>();
            try {
                
                // Orders
                orders = Logic.InvoiceGeneration.GetOrderForInvoicesLastMonth(DB).ToList();
                Log($"Found {orders.Count()} to create invoices from");

                // New Invoices
                newInvoices = Logic.InvoiceGeneration.GenerateInvoicesForOpenOrders(DB, orders.ToList()).ToList();
                Log($"Generated {newInvoices.Count()} invoices for the following businesses: ");
                foreach (var invoice in newInvoices) {
                    Log($"Business: {invoice.Business}");
                }
                DB.SaveChanges();

                // Invoices To send (maybe some are generated but never sent)
                invoicesToSend = Invoice.GetInvoicesToSend(DB).ToList();

                // Send invoices
                Finance.Model.Invoice.SendInvoices(invoicesToSend, DB.Users().SystemUser());

                Log($"Sent an email for each business/invoice");

                DB.SaveChanges();

            } catch (Exception e)
            {
                var report = new StringBuilder();
                report.AppendLine($"Orders to Invoice: {string.Join(",", orders.Select(o => o.PublicId))}");
                report.AppendLine($"Invoices To Send: {string.Join(",", invoicesToSend.Select(o => o.PublicId))}");
                report.AppendLine("Exception: "+ Core.EmailLogger.CompileFullErrorReport(this.Context, null, e));
                MessageCenter.SendMessageToAdminEmail("Invoice Task Error", report.ToString(), "Machinata.Module.Shop");
            }

        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1d";
            return config;
        }
    }
}
