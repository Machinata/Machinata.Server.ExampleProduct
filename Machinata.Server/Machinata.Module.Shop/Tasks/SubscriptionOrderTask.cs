using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Module.Shop.Tasks {

    public class SubscriptionOrderTask : Core.TaskManager.Task {
        public override void Process() {

            var subscriptions = DB.OrderSubscriptions().Where(os => os.Active);
            Log($"Found {subscriptions.Count()} active order subscriptions");

            foreach (var subscription in subscriptions) {
                try {
                    Log($"Processing Order Subscription: {subscription}");
                    var possibleDates = subscription.GetPossibleDeliveryDatesForRange(DateTime.UtcNow, DateTime.UtcNow.AddMonths(3)); // TODO: micha, what range
                    subscription.GeneratedOrdersForDates(DB, possibleDates.Select(osd => osd.Date));
                } catch (Exception e) {

                }
            }

           // Log($"Generated {subscriptionOrders.Count()} subscription orders");

           

            DB.SaveChanges();

        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1d";
            return config;
        }
    }
}
