﻿
###### Emails #######################################################

email-order-text.de=Vielen Dank für Ihre Bestellung.

email-order-view-order-online.de=Bestellung online anzeigen

email-order-show-tracking-online.en=Show Tracking Online
email-order-show-tracking-online.de=Tracking online anzeigen

email-shipment-text.en=Your order has been shipped.
email-shipment-text.de=Ihre Bestellung wurde versendet.

email-order-salutation.en=Hi {order.contact},
email-order-salutation.de=Guten Tag, {order.contact}


email-refund-text.en=Your order has been refunded. You should receive your refund from the payment provider shortly. 
email-refund-text.de=Ihre Bestellung wurde zurückerstattet. Der Betrag sollte in Kürze vom entsprechenden Anbieter zurückerstattet werden.