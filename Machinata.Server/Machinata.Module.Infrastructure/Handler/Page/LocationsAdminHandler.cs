
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Infrastructure.Model;

namespace Machinata.Module.Finance.Handler {


    public class LocationsAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

     
        
        [RequestHandler("/admin/infrastructure/locations")]
        public void List() {

            var locations = this.Template.Filter(this.DB.Locations(), this, nameof(Location.LocationName));
            locations = this.Template.Paginate(locations, this);

            // Entities
            this.Template.InsertEntityList(
                    variableName: "entities",
                    entities: locations,
                    link: "{page.navigation.current-path}/location/{entity.public-id}",
                    loadFirstLevelReferences: true);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
        }

        [RequestHandler("/admin/infrastructure/locations/location/{publicId}")]
        public void View(string publicId) {

            // Entity
            var entity = this.DB.Locations()
                .Include(nameof(Location.Address))
                .Include(nameof(Location.Devices))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.VIEW);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: form,
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);

            // Address
            this.Template.InsertEntityList(
                variableName: "address",
                entity: entity.Address,
                link: "{page.navigation.current-path}/address");

            // Devices
            this.Template.InsertEntityList(
                variableName: "devices",
                entities: entity.Devices.AsQueryable(),
                link: "/admin/infrastructure/devices/device/{entity.public-id}");


            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
            this.Navigation.Add("location/" + publicId, "{text.location}" + ": " + entity.LocationName);

        }

        [RequestHandler("/admin/infrastructure/locations/create")]
        public void Create() {
            this.RequireWriteARN();

            // Entity
            var entity = new Location();

            //// Device?
            //var device = this.DB.Devices().GetByPublicId(this.Params.String("device"), false);
            //entity.Device = device;

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/infrastructure/locations/create",
                  onSuccess: "/admin/infrastructure/locations/location/{location.public-id}"
                  );

            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
            this.Navigation.Add("create");

        }

       

        [RequestHandler("/admin/infrastructure/locations/location/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Locations()
             //   .Include(nameof(Location.Address))
                .GetByPublicId(publicId);


            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/infrastructure/location/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertForm(
              variableName: "address-form",
              entity: entity,
              form: new FormBuilder(Forms.Admin.EDIT),
              apiCall: "/api/admin/infrastructure/location/" + publicId + "/edit",
              onSuccess: "{page.navigation.prev-path}");

            // Variables
            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
            this.Navigation.Add("location/" + publicId, "{text.location}" + ": " + entity.LocationName);
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/infrastructure/locations/location/{publicId}/address")]
        public void Address(string publicId) {

            // Entity
            var entity = this.DB.Locations()
                .Include(nameof(Location.Address))
                .GetByPublicId(publicId);
                 

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity.Address,
                    form: new FormBuilder(Forms.Admin.VIEW),
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);

        

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
            this.Navigation.Add("location/" + publicId, "{text.location}" + ": " + entity.LocationName);
            this.Navigation.Add("address", "{text.address}" + ": " + entity.Address.Title);

        }


        [RequestHandler("/admin/infrastructure/locations/location/{publicId}/address/edit")]
        public void AddressEdit(string publicId) {

            // Entity
            var entity = this.DB.Locations()
                .Include(nameof(Location.Address))
                .GetByPublicId(publicId);


            // Variables
            this.Template.InsertForm(
                    variableName: "form",
                    entity: entity.Address,
                    form: new FormBuilder(Forms.Admin.EDIT),
                    apiCall: "/api/admin/infrastructure/location/"+ publicId + "/address/edit",
                    onSuccess: "{page.navigation.prev-path}"
                    );


       
            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("locations");
            this.Navigation.Add("location/" + publicId, "{text.location}" + ": " + entity.LocationName);
            this.Navigation.Add("address", "{text.address}" + ": " + entity.Address.Title);
            this.Navigation.Add("edit");
        }


    }
}
