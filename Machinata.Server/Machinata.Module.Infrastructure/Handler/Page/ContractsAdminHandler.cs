
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Infrastructure.Model;

namespace Machinata.Module.Finance.Handler {


    public class ContractsAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

     
        [RequestHandler("/admin/infrastructure/contracts")]
        public void Contracts() {
            // Servers
            this.Template.InsertEntityList(
                    variableName: "contracts",
                    entities: this.DB.Contracts().AsQueryable(),
                    link: "{page.navigation.current-path}/contract/{entity.public-id}",
                    loadFirstLevelReferences: true);

           
            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("contracts");
        }

        [RequestHandler("/admin/infrastructure/contracts/contract/{publicId}")]
        public void Contract(string publicId) {

            // Entity
            var entity = this.DB.Contracts()
                .Include(nameof(Infrastructure.Model.Contract.Devices))
                .Include(nameof(Infrastructure.Model.Contract.Invoices))
                .GetByPublicId(publicId);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: new FormBuilder(Forms.Admin.VIEW),
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);

            // Invoices
            this.Template.InsertEntityList(
                variableName: "invoices",
                entities: entity.Invoices.AsQueryable(),
                link: "/admin/finance/invoices/invoice/{entity.public-id}"
            );
            
            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("contracts");
            this.Navigation.Add("contract/" + publicId, "{text.contract}" + ": " + entity.Title);

        }

        [RequestHandler("/admin/infrastructure/contracts/create")]
        public void Create() {
            this.RequireWriteARN();

            // Course
            var entity = new Contract();
            entity.Interval = Infrastructure.Model.Contract.Intervals.Monthly;
            entity.InternalPrice = new Price(0);
            entity.ExternalPrice = new Price(0);

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/infrastructure/contracts/create",
                  onSuccess: "/admin/infrastructure/contracts/contract/{contract.public-id}"
                  );

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("contracts");
            this.Navigation.Add("create");

        }

        [RequestHandler("/admin/infrastructure/contracts/contract/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Contracts()
                .Include(nameof(Infrastructure.Model.Contract.Business))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/infrastructure/contract/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("contracts");
            this.Navigation.Add("contract/" + publicId, "{text.contract}" + ": " + entity.Title);
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/infrastructure/contracts/contract/{publicId}/generate-invoice")]
        public void GenerateInvoice(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Contracts()
                .Include(nameof(Infrastructure.Model.Contract.Business))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.EMPTY)
                        .Custom("start","start","date",null,true)
                        .Custom("end", "end", "date", null, true),
                apiCall: "/api/admin/infrastructure/contract/" + publicId + "/generate-invoice",
                onSuccess: "/admin/finance/invoices/invoice/{invoice.public-id}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("contracts");
            this.Navigation.Add("contract/" + publicId, "{text.contract}" + ": " + entity.Title);
            this.Navigation.Add("generate-invoice");

        }


    }
}
