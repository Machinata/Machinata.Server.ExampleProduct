
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Admin.Handler {


    public class ContractsAPIHandler : CRUDAdminAPIHandler<Contract> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        [RequestHandler("/api/admin/infrastructure/contracts/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/infrastructure/contract/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/infrastructure/contract/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/infrastructure/contract/{publicId}/generate-invoice")]
        public void GenerateInvoice(string publicId) {

            var entity = DB.Contracts()
              .Include(nameof(Infrastructure.Model.Contract.Business) + "." + nameof(Core.Model.Business.DefaultBillingAddress))
              .Include(nameof(Infrastructure.Model.Contract.Invoices))
              .GetByPublicId(publicId);

            var start = this.Params.DateTime("start", DateTime.UtcNow);
            var end = this.Params.DateTime("end", DateTime.UtcNow);

            var startLocalTime = start.ToDefaultTimezone();
            var endLocalTime = end.ToDefaultTimezone();

            var invoice = entity.CreateInvoice (
                sender: this.DB.OwnerBusiness(),
                billingAddress: entity.Business.DefaultBillingAddress,
                method: Finance.Payment.PaymentMethod.Invoice,
                user: this.User, 
                currency: Core.Config.CurrencyDefault,
                dateRange: new DateRange(startLocalTime, endLocalTime),
                business: entity.Business
            );

            entity.Invoices.Add(invoice);

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Invoice = new { invoice.PublicId } });
        }


    }
}
