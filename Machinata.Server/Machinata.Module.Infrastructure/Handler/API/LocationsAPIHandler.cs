
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;

namespace Machinata.Module.Admin.Handler {


    public class LocationsAPIHandler : CRUDAdminAPIHandler<Location> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        [RequestHandler("/api/admin/infrastructure/locations/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/infrastructure/location/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/infrastructure/location/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/infrastructure/location/{publicId}/address/edit")]
        public void EditAddress(string publicId) {
            var address = this.DB.Locations()
                .Include(nameof(Location.Address))
                .GetByPublicId(publicId).Address;

            address.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            // Post Populate

            address.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success");


        }

        protected override void CreatePopulate(Location entity) {
            base.CreatePopulate(entity);
            entity.Address = new Address();
            entity.Address.CountryCode = "ch";
        }

        protected override void PreDelete(Location entity) {
            entity.Include(nameof(Location.Address));
            if (entity.Address != null) {
                entity.Context.Addresses().Remove(entity.Address);
                entity.Address = null;
            }
        }



    }
}
