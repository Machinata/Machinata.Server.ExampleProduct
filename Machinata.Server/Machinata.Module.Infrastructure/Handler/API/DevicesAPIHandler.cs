
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Infrastructure.Extensions;

namespace Machinata.Module.Admin.Handler {


    public class DevicesAPIHandler : CRUDAdminAPIHandler<Device> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        [RequestHandler("/api/admin/infrastructure/devices/create")]
        public void Create() {
         
            CRUDCreate();
        }

        [RequestHandler("/api/admin/infrastructure/device/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/infrastructure/device/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/infrastructure/device/{publicId}/toggle-account/{accountId}")]
        public void ToggleAccount(string publicId, string accountId) {
            CRUDToggleSelection<Account>(publicId, accountId, nameof(Device.Accounts));
        }


        [RequestHandler("/api/admin/infrastructure/device/{publicId}/add-account")]
        public void AddAccount(string publicId) {
            var device = this.DB.Devices()
                .Include(nameof(Device.Accounts))
                .GetByPublicId(publicId);

            var account = new Account();
            account.Device = device;
            account.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            account.Validate();
            device.Accounts.Add(account);

            this.DB.Accounts().Add(account);

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Account = new { account.PublicId } });

        }
     

        [RequestHandler("/api/admin/infrastructure/device/{publicId}/account/{accountId}/edit")]
        public void EditAccount(string publicId, string accountId) {
            var device = this.DB.Devices()
                .Include(nameof(Device.Accounts))
                .GetByPublicId(publicId);

            var account = device.Accounts.AsQueryable().GetByPublicId(accountId);
            var oldValueSensitive = account.IsSensitive;

            var form = new FormBuilder(Forms.Admin.EDIT).Exclude(nameof(Account.Device));
            form.MakeSensitiveAdjustments(account, this.User);

            account.Populate(this, form);

            this.User.CheckIsSensitiveChange(account, oldValueSensitive);

            account.Validate();
            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Account = new { account.PublicId } });

        }

   

        [RequestHandler("/api/admin/infrastructure/device/{publicId}/account/{accountId}/delete")]
        public void DeleteAccount(string publicId, string accountId) {
            var device = this.DB.Devices()
                .Include(nameof(Device.Accounts))
                .GetByPublicId(publicId);

            var account = device.Accounts.AsQueryable().GetByPublicId(accountId);
            device.Accounts.Remove(account);

            DB.DeleteEntity(account);

            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Account = new { account.PublicId } });

        }

    }
}
