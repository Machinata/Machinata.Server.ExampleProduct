
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Infrastructure.Extensions;

namespace Machinata.Module.Admin.Handler {


    public class AccountsAPIHandler : CRUDAdminAPIHandler<Account> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        [PolicyProvider]
        public static List<AccessPolicy> SpecialPolicyProvider() {
            return new List<AccessPolicy>() {
                new AccessPolicy() { Name = $"Special Sensitive Accounts", ARN = AccessPolicy.GetDefaultRootAccessARN(UserExtensions.SENSITIVE_ARN.TrimStart("/"))}
            };
        }


        #endregion

        [RequestHandler("/api/admin/infrastructure/accounts/create", skipRequestValidation: true)]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/infrastructure/account/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/infrastructure/account/{publicId}/edit", skipRequestValidation: true)]
        public void Edit(string publicId) {
            var account = this.DB.Accounts().GetByPublicId(publicId);
            //var form = new FormBuilder(Forms.Admin.EDIT);
            //form.MakeSensitiveAdjustments(account, this.User, readOnly: false);


            var oldValueSensitive = account.IsSensitive;

            var form = new FormBuilder(Forms.Admin.EDIT).Exclude(nameof(Account.Device));
            form.MakeSensitiveAdjustments(account, this.User);

            account.Populate(this, form);

            this.User.CheckIsSensitiveChange(account, oldValueSensitive);

            account.Validate();
            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Account = new { account.PublicId } });

        }

      


    }
}
