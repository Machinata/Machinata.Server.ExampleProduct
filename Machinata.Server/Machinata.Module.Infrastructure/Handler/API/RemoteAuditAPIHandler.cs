
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;

namespace Machinata.Module.Admin.Handler {


    /// <summary>
    /// A useful handler for easily triggering audit event emails via remote hardware/machines.
    /// </summary>
    public class RemoteAuditAPIHandler : APIHandler {
        

        [RequestHandler("/api/admin/infrastructure/remote-audit/log", AccessPolicy.PUBLIC_ARN)]
        public void Log() {
            // Validate key
            if(string.IsNullOrEmpty(Machinata.Module.Infrastructure.Config.InfrastructureRemoteAuditAcessKey)) {
                throw new Backend403Exception("access-key-invalid","A access key has not been configured.", AccessPolicy.PUBLIC_ARN);
            }
            if (this.Params.String("key") != Machinata.Module.Infrastructure.Config.InfrastructureRemoteAuditAcessKey) {
                throw new Backend403Exception("access-key-invalid", "Access key is not valid.", AccessPolicy.PUBLIC_ARN);
            }
            if (string.IsNullOrEmpty(this.Params.String("source"))) {
                throw new BackendException("no-source", "No source provided");
            }
            if (string.IsNullOrEmpty(this.Params.String("subject"))) {
                throw new BackendException("no-subject", "No subject provided");
            }

            // Compile email
            var msg = "";
            foreach(var param in this.Context.Request.QueryString.AllKeys) {
                if (param == "key") continue;
                msg += $"{param.ToSentence()}: {this.Context.Request.QueryString[param]}\n";
            }
            var subject = this.Params.String("source") + ": Infrastructure Remote Audit Log: " + this.Params.String("subject");

            // Send
            Core.Messaging.MessageCenter.SendMessageToAdminEmail(subject, msg, "Machinata.Module.Infrastructure");

            // Success
            SendAPIMessage("success");
        }

        


    }
}
