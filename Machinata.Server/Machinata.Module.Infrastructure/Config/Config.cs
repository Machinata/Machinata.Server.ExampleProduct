using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Infrastructure {
    public class Config {
        public static double InfrastructureContractDefaultVatRate = Core.Config.GetDoubleSetting("InfrastructureContractDefaultVatRate");
        public static string InfrastructureRemoteAuditAcessKey = Core.Config.GetStringSetting("InfrastructureRemoteAuditAcessKey", "");
    }
}
