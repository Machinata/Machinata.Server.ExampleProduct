using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureDeviceEvents")]
    public partial class DeviceEvent : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public DeviceEvent() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }
        
       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Description { get; set; }
        
           
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public DateRange TimeRange { get; set; }

        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public Device Device { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////




        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
          
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Subtitle(this.Description)
                .Sub(this.TimeRange?.ToString())
                .Icon("flash-outline")
                .Wide();
            return card;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextEventExtensions {

        [ModelSet]
        public static DbSet<DeviceEvent> Events(this Core.Model.ModelContext context) {
            return context.Set<DeviceEvent>();
        }
       
    }

    #endregion

}
