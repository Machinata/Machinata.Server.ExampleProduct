using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureAccounts")]
    public partial class Account : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum AccountCategories : short {
            Service = 10,
            Login = 20,
            WebsiteAdmin = 30,
            Computer = 40
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Account() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }
        
       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public AccountCategories AccountCategory { get; set; }
        
        [Column]
        public string UsernameEncrypted { get; set; }

        [Column]
        public string PasswordEncrypted { get; set; }

        [Column]
        public string NotesEncrypted { get; set; }


        [FormBuilder()]
        [Column]
        public bool IsSensitive { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        



        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public Device Device { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Business Business { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [NotMapped]
        public string Username
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.UsernameEncrypted);
            }
            set
            {
                this.UsernameEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [NotMapped]
        public string Password
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.PasswordEncrypted);
            }
            set
            {
                this.PasswordEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.MultilineText)]
        [NotMapped]
        public string Notes
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.NotesEncrypted);
            }
            set
            {
                this.NotesEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
          
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Notes)
                .Icon("user")
                .Wide();

            if (this.Device != null) {
                card.Sub(this.Device.Name);
            }
            return card;
        }

     

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextAccountExtensions {

        [ModelSet]
        public static DbSet<Account> Accounts(this Core.Model.ModelContext context) {
            return context.Set<Account>();
        }
       
    }

    #endregion

}
