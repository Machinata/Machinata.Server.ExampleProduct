using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Invoices;

namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureContracts")]
    public partial class Contract : ModelObject{
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum Intervals : short {
            Monthly = 10
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Contract() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.MultilineText)]
        [Column]
        public string Description { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public bool Enabled { get; set; }
        
           
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public Price ExternalPrice { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public Price InternalPrice { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Intervals Interval { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Business Business { get; set; }


        [Column]
        public ICollection<Device> Devices { get; set; }


        [Column]
        public ICollection<Invoice> Invoices { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public string GetIntervalDescription(bool singular) {
            if (this.Interval == Intervals.Monthly) {
                return singular ? "month" : "months".TextVar();
            }
            return null;
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public Invoice CreateInvoice(
            Business sender,
            Address billingAddress,
            Finance.Payment.PaymentMethod method,
            User user,
            string currency,
            DateRange dateRange,
            Business business

            ) {

            if (dateRange.Start > dateRange.End) {
                throw new Exception("End date has to be after the start date");
            }

            // Create the invoice and setup details
            var invoice = new Invoice();
            invoice.User = user;
            invoice.Business = business;
            invoice.BillingAddress = billingAddress;
            invoice.BillingName = billingAddress?.Name;
            invoice.BillingEmails = billingAddress?.Email;
            invoice.Sender = sender;
            invoice.SenderAddress = sender.GetBillingAddressForBusiness();
            invoice.Status = Invoice.InvoiceStatus.Created;
            invoice.PaymentMethod = method;

            // Prices /Currencies
            invoice.SubtotalCustomer = new Price(0, currency);
            invoice.TotalCustomer = new Price(0, currency);
            invoice.VATCustomer = new Price(0, currency);
            invoice.Currency = currency;

            // Title
            invoice.Title = business.Name + " " + this.Title + $" {dateRange.Start.Value.ToMonthString()} - {dateRange.End.Value.ToMonthString()}";

            var lineItemsGroups = this.GenerateLineItemGroups(dateRange.Start.Value, dateRange.End.Value);
            foreach (var lineItemGroup in lineItemsGroups) {
                invoice.LineItemGroups.Add(lineItemGroup);
            }
         
            // Update totals
            invoice.UpdateInvoice();


            return invoice;
        }

        /// <summary>
        /// Generates the line item groups. Currently only Interva.== Monthly is supported
        /// Dates have to be passed in the local time
        /// </summary>
        /// <param name="start">The start: only the month,year part is used.</param>
        /// <param name="end">The end: only the month,year part is used.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public IList<Finance.Model.LineItemGroup> GenerateLineItemGroups(DateTime start, DateTime end) {

            if (this.Interval != Intervals.Monthly) {
                throw new Exception($"{this.Interval} is not supported.");
            }

            var months = Core.Util.Time.GetMonths(start, end);

            this.LoadFirstLevelNavigationReferences();
            this.LoadFirstLevelObjectReferences();
            var billedDate = DateTime.UtcNow;
            var groups = new List<Finance.Model.LineItemGroup>();
            {
                // Order group
                var group = new Finance.Model.LineItemGroup();
                group.Title = this.Title + $" {start.ToMonthString()} - {end.ToMonthString()}";
                group.Description = this.Description;
                group.Category = "{text.contract}";
                group.TimeRange = new DateRange(start, end);

                this.LoadFirstLevelNavigationReferences();
                this.LoadFirstLevelObjectReferences();
                var lineItem = new Finance.Model.LineItem();

                // General
                lineItem.CustomerItemPrice = this.ExternalPrice.Clone();
                lineItem.InternalItemPrice = this.InternalPrice.Clone();
                lineItem.Category = "";
                lineItem.Quantity = months.Count();
                lineItem.TimeRange = new DateRange(start, end);
                lineItem.CustomerVATRate = (decimal?)(Config.InfrastructureContractDefaultVatRate / 100);

                // Calc Totals and VAT
                lineItem.CustomerVAT = lineItem.CustomerItemPrice * lineItem.CustomerVATRate;
                lineItem.CustomerTotal = lineItem.CustomerSubtotal + lineItem.CustomerVAT * lineItem.Quantity;

                lineItem.Description = this.Description;

                group.LineItems.Add(lineItem);
                groups.Add(group);
            }
            return groups;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
          
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Subtitle(this.Interval)
                .Icon("document-text")
                .Tag(this.ExternalPrice?.Value?.ToString())
                .Wide();
            return card;
        }

        public List<LineItemGroup> GetLineItemGroups() {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextContractExtensions {
        [ModelSet]
        public static DbSet<Contract> Contracts(this Core.Model.ModelContext context) {
            return context.Set<Contract>();
        }
       
    }

    #endregion

}
