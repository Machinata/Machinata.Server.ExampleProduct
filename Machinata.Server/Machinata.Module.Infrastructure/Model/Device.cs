using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureDevices")]
    public partial class Device : ModelObject, IPublishedModelObject{
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum DeviceStatuses : short {
            Online = 10,
            Maintanance = 20,
            Offline = 30
        }

        public enum DeviceCategories : short {
            Server = 10,
            Workstation = 20,
            Device = 30
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Device() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public DeviceCategories Category { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public DeviceStatuses Status { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }
        
       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public bool Enabled { get; set; }
        
           
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string LocationPath { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string IP { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Platform { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.MultilineText)]
        public string Software { get; set; }

      

        [Column]
        public string NotesEncrypted { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<DeviceEvent> Events { get; set; } // later


        [Column]
        public ICollection<Contract> Contracts { get; set; }


        [Column]
        public ICollection<Account> Accounts { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public Location Location { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Business Business { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.MultilineText)]
        [NotMapped]
        public string Notes
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.NotesEncrypted);
            }
            set
            {
                this.NotesEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

      

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
          
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Category)
                .Sub(this.Status.ToString())
                .Icon("device-desktop")
                .Tag(this.IP)
                .Wide();
            return card;
        }

        public override string ToString() {
            return $"{this.Name} - {this.Category}";
        }

        public bool Published
        {
            get
            {
                return this.Enabled && this.Status == DeviceStatuses.Online;
            }
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextAbsenceExtensions {
        [ModelSet]
        public static DbSet<Device> Devices(this Core.Model.ModelContext context) {
            return context.Set<Device>();
        }
       
    }

    #endregion

}
