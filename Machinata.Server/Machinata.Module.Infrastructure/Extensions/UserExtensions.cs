﻿using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Infrastructure.Extensions {
    public static class UserExtensions {
        public const string SENSITIVE_ARN = "/sensitive/rights";

        public static bool HasSensitiveRights(this User user) {
            if (user.HasMatchingARN(SENSITIVE_ARN) == true) {
                return true;
            }
            
            return false;
        }

        public static void CheckIsSensitiveChange(this User user, Account account, bool oldValueSensitive ) {
            if (account.IsSensitive == false && oldValueSensitive == true && user.HasSensitiveRights() == false) {
                throw new BackendException("account-restriction", "Changing IsSensitive is not allowed");
            }
        }
    }
}
