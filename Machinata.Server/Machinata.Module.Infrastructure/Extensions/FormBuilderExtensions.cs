﻿using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Infrastructure.Extensions {
    public static class FormBuilderExtensions {

        //public static void MakeSensitiveAdjustments(this FormBuilder form, Account account, User user, bool readOnly) {
        //    if (user.HasSensitiveRights() == false && account.IsSensitive == true) {
        //        form = form.Exclude(nameof(Account.Password));
        //    }
        //    if (user.HasSensitiveRights() == true || readOnly == true) {
        //        form = form.Include(nameof(Account.IsSensitive));
        //    }

        //}


        public static void MakeSensitiveAdjustments(this FormBuilder form, Account account, User user) {
            if (user.HasSensitiveRights() == false && account.IsSensitive == true) {
                form = form.Exclude(nameof(Account.Password));
            }
            form = form.Include(nameof(Account.IsSensitive));
        }
    }

}
