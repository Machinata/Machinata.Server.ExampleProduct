
# Dry Run:
#searchanddestroy --dry-run --use-git-move --dir "D:\\Code\\MachinataServer" --replace Blackbox Machinata blackbox machinata --include \*.\* --exclude .\*

# Live all 1/2:
#searchanddestroy --use-git-move --dir "D:\\Code\\MachinataServer" --replace Blackbox Machinata blackbox machinata --include \*.\* --exclude .\*


# Live text pass 2/2:
searchanddestroy --use-git-move --binary true --dir "D:\\Code\\MachinataServer" --replace Blackbox Machinata blackbox machinata --include \*.cs \*.js \*.htm \*.json \*.json --exclude .\*

