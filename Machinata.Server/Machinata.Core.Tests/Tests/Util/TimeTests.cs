using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Core.Tests {
    [TestClass]
    public class TimeTests {
        

        [TestMethod]
        public void Core_Util_Time_WesternBusinessDays() {
            var holidays = new List<DateTime> {
                new DateTime(2018, 06, 22), // Friday
                new DateTime(2018, 06, 23) // Saturday
            };
            var start = new DateTime(2018, 06, 19); // Tuesday
            var end = new DateTime(2018, 07, 01); // Sunday
            var businessDays = Core.Util.Time.GetWesternBusinessDays(start, end, holidays);
            var expected = 8;
            Assert.AreEqual(expected, businessDays);
        }
        
        
        [TestMethod]
        public void Core_Util_Time_WesternWorkingDays() {
            var start = new DateTime(2017, 12, 19);
            var end = new DateTime(2018, 01, 07).EndOfDay(); 
            var businessDays = Core.Util.Time.GetWesternBusinessDays(start, end); // should be 14
            var workingDays = Core.Util.Time.GetWesternWorkingDays(start, end); // should be 10
            var expectedBusinessDays = 14;
            var expectedWorkingDays = 10;
            Assert.AreEqual(expectedBusinessDays, businessDays);
            Assert.AreEqual(expectedWorkingDays, workingDays);
        }
        
        [TestMethod]
        public void Core_Util_Time_VacationDays() {
            var start = new DateTime(2018, 06, 23);
            var end = new DateTime(2018, 06, 28).EndOfDay(); 
            var workingDays = Core.Util.Time.GetWesternWorkingDays(start, end); // should be 10
            var expectedWorkingDays = 4;
            Assert.AreEqual(expectedWorkingDays, workingDays);
        }

        [TestMethod]
        public void Core_Util_Time_Months_1() {
            var start = new DateTime(2018, 06, 1, 0, 0, 0);
            var end = new DateTime(2018, 06, 30, 23, 59, 59);
            var months = Core.Util.Time.GetMonths(start, end);
            var expectedMonts = 1;
            Assert.AreEqual(expectedMonts, months.Count());
            Assert.AreEqual(6, months.First().Item2);
            Assert.AreEqual(2018, months.First().Item1);
        }

        [TestMethod]
        public void Core_Util_Time_Months_2() {
            var start = new DateTime(2018, 06, 1, 0, 0, 0);
            var end = new DateTime(2018, 07, 31, 23, 59, 59);
            var months = Core.Util.Time.GetMonths(start, end);
           
            Assert.AreEqual(2, months.Count());
            Assert.AreEqual(6, months.First().Item2);
            Assert.AreEqual(2018, months.First().Item1);
            Assert.AreEqual(7, months.Second().Item2);
            Assert.AreEqual(2018, months.Second().Item1);
        }

        [TestMethod]
        public void Core_Util_Time_Months_Impossible() {
            var start = new DateTime(2018, 06, 1, 0, 0, 0);
            var end = new DateTime(2018, 05, 31, 23, 59, 59);
            var months = Core.Util.Time.GetMonths(start, end);

            Assert.AreEqual(0, months.Count());
        }

        [TestMethod]
        public void Core_Util_Time_HumanReadableDurationForDays() {
            Assert.AreEqual("0.5 {text.days}", Core.Util.Time.HumanReadableDurationForDays(0.5m));
            Assert.AreEqual("0.7 {text.days}", Core.Util.Time.HumanReadableDurationForDays(0.7m));
            Assert.AreEqual("1 {text.day}", Core.Util.Time.HumanReadableDurationForDays(1m));
            Assert.AreEqual("3 {text.days}", Core.Util.Time.HumanReadableDurationForDays(3));
            Assert.AreEqual("1 {text.week}", Core.Util.Time.HumanReadableDurationForDays(7m));
            Assert.AreEqual("8 {text.days}", Core.Util.Time.HumanReadableDurationForDays(8.6m));
            Assert.AreEqual("2 {text.weeks}", Core.Util.Time.HumanReadableDurationForDays(14m));
            Assert.AreEqual("0.5 {text.months}", Core.Util.Time.HumanReadableDurationForDays(15m));
            Assert.AreEqual("2 {text.months}", Core.Util.Time.HumanReadableDurationForDays(60m));
            Assert.AreEqual("3 {text.months}", Core.Util.Time.HumanReadableDurationForDays(90m));
        }

        [TestMethod]
        public void Core_Util_Time_ParseWindowsConsoleTimestamp() {
            var datetime = new DateTime(2019, 03, 08, 9, 34, 32);
            Assert.AreEqual(
                datetime, 
                Core.Util.Time.ParseWindowsConsoleTimestamp("Fri 03/08/2019  9:34:32.00")
            );
            Assert.AreEqual(
                datetime, 
                Core.Util.Time.ParseWindowsConsoleTimestamp("08.03.2019  9:34:32.00")
            );
            Assert.AreEqual(
               datetime,
               Core.Util.Time.ParseWindowsConsoleTimestamp("08/03/2019 9:34:32,00")
           );
        }

        [TestMethod]
        public void Core_Util_Time_DateTime_StartOfMonth() {
            var datetime = new DateTime(2019, 03, 08, 9, 34, 32);
            var expected = new DateTime(2019, 03, 01, 0, 0, 0);
            Assert.AreEqual(
                expected, 
                datetime.StartOfMonth()
            );
        }

        [TestMethod]
        public void Core_Util_Time_DateTime_EndOfMonth() {
            var datetime = new DateTime(2019, 08, 08, 9, 34, 32);
            var expected = new DateTime(2019, 08, 31, 23, 59, 59);
            Assert.AreEqual(
                expected, 
                datetime.EndOfMonth()
            );
        }

        [TestMethod]
        public void Core_Util_Time_DateTime_StartOfDay() {
            var datetime = new DateTime(2019, 08, 31, 9, 34, 32);
            var expected = new DateTime(2019, 08, 31, 0, 0, 0);
            Assert.AreEqual(
                expected, 
                datetime.StartOfDay()
            );
        }

        [TestMethod]
        public void Core_Util_Time_DateTime_EndOfDay() {
            var datetime = new DateTime(2019, 08, 31, 9, 34, 32);
            var expected = new DateTime(2019, 08, 31, 23, 59, 59);
            Assert.AreEqual(
                expected, 
                datetime.EndOfDay()
            );
        }
    }
}
