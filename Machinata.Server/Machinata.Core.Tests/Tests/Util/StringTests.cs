using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;

namespace Machinata.Core.Tests {
    [TestClass]
    public class StringTests {

        private const string LOREM_IPSUM_LONG = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet sagittis lacus, ut efficitur quam. Aliquam ligula diam, ultricies non nibh at, tempor pulvinar tellus. Aliquam euismod eget orci non sodales. Nulla facilisi. Aenean semper tempor ante, sed interdum augue finibus eu. In et dolor augue. Sed ac suscipit metus. Pellentesque at molestie purus. Pellentesque quam ligula, congue non ante eu, ornare sollicitudin tellus. Donec volutpat malesuada felis, at efficitur erat iaculis et. Sed sollicitudin pretium nunc vitae dictum. Integer ultricies convallis bibendum. Cras lacinia vitae purus id blandit. Suspendisse rhoncus ut diam at vehicula. Quisque fringilla tempor libero non faucibus. Proin ullamcorper in est vel dictum.";
        private const string LOREM_IPSUM_SHORT = "Lorem ipsum dolor sit amet.";

        [TestMethod]
        public void Core_Util_String_RemoveDiacritics() {
            var str =       "Daníelö Krüsi ÖÓÚËÉÅÄÁÏ äåéëüúíóöáïñ";
            var expected =  "Danielo Krusi OOUEEAAAI aaeeuuiooain";
            var converted = Core.Util.String.RemoveDiacritics(str);
            Assert.AreEqual(converted, expected);
        }


       // [TestMethod] -> hangs on gitlab runner?
        public void Core_Util_String_ReplaceUmlauts() {
            var str =       "Daníelö Krüsi ÖÓÚËÉÅÄÁÏ äåéëüúíóöáïñ";
            var expected =  "Daníeloe Kruesi OeÓÚËÉÅAeÁÏ aeåéëueúíóoeáïñ";
            var converted = Core.Util.String.ReplaceUmlauts(str);
            Assert.AreEqual(converted, expected);
        }
        

        [TestMethod]
        public void Core_Util_String_CreateSummarizedText() {
            {
                var str = LOREM_IPSUM_LONG;
                var expected = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eti...";
                var converted = Core.Util.String.CreateSummarizedText(str, 60, true, true, false);
                Assert.AreEqual(expected, converted);
            }
            {
                var str = LOREM_IPSUM_LONG;
                var expected = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam...";
                var converted = Core.Util.String.CreateSummarizedText(str, 60, true, true, true);
                Assert.AreEqual(expected, converted);
            }
            {
                var str = LOREM_IPSUM_LONG;
                var expected = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
                var converted = Core.Util.String.CreateSummarizedText(str, 59, false, true, true);
                Assert.AreEqual(expected, converted);
            }
            {
                var str = LOREM_IPSUM_SHORT;
                var expected = LOREM_IPSUM_SHORT;
                var converted = Core.Util.String.CreateSummarizedText(str, 60, true, true, false);
                Assert.AreEqual(expected, converted);
            }
        }

    }
}
