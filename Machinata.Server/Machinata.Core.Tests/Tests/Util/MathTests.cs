using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;

namespace Machinata.Core.Tests {
    [TestClass]
    public class MathTests {
        

        [TestMethod]
        public void Core_Util_Math_RandomInt() {
            for(var i = 0; i < 1000; i++) {
                var r = Core.Util.Math.RandomInt(0, 10);
                Assert.IsTrue(r >= 0 && r < 10);
                Assert.IsFalse(r == 10);
            }
        }
        

        [TestMethod]
        public void Core_Util_Math_SnapRoundToString() {
            Assert.AreEqual("0", Core.Util.Math.SnapRoundToString(0.0M,1));
            Assert.AreEqual("0.1", Core.Util.Math.SnapRoundToString(0.1M,1));
            Assert.AreEqual("2.6", Core.Util.Math.SnapRoundToString(2.61M,1));
            Assert.AreEqual("0.6", Core.Util.Math.SnapRoundToString(0.55M,1));
            Assert.AreEqual("0.001", Core.Util.Math.SnapRoundToString(0.001M,3));
            Assert.AreEqual("1", Core.Util.Math.SnapRoundToString(1.000M,3));
        }
        

    }
}
