using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Machinata.Core.Tests {
    [TestClass]
    public class JSONTests {

        [TestMethod]
        public void Core_JSON_TestValidJson() {
            string json = @"{
                      CPU: 'Intel',
                      Drives: [
                        'DVD read/writer',
                        '500 gigabyte hard drive'
                      ]
                    }";
            var result = Core.JSON.ParseJsonAsJObject(json);
            Assert.AreEqual("DVD read/writer", result["Drives"][0].ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(JsonReaderException))]
        public void Core_JSON_TestInvalidJson() {
            string json = @"{
                      CPU: 'Intel',
                      Drives:: [
                        'DVD read/writer',
                        '500 gigabyte hard drive'
                      ]
                    }";
            var result = Core.JSON.ParseJsonAsJObject(json);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Core_JSON_TestInvalidNullJson() {
            string json = null;
            var result = Core.JSON.ParseJsonAsJObject(json);
        }

        [TestMethod]
        [ExpectedException(typeof(JsonReaderException))]
        public void Core_JSON_TestInvalidEmptyJson() {
            string json = string.Empty;
            var result = Core.JSON.ParseJsonAsJObject(json);
        }

        [TestMethod]
        public void Core_JSON_TestInValidEmptyJsonWithParam() {
            string json = string.Empty;
            var result = Core.JSON.ParseJsonAsJObject(json, true);
            Assert.IsNull(null);
        }

        [TestMethod]
        public void Core_JSON_TestInValidNullJsonWithParam() {
            string json = null;
            var result = Core.JSON.ParseJsonAsJObject(json, true);
            Assert.IsNull(null);
        }

        [TestMethod]
        [ExpectedException(typeof(JsonReaderException))]
        public void Core_JSON_TestInValidsonWithParam() {
            string json = @"{
                      CPU: 'Intel',
                      Drives:: [
                        'DVD read/writer',
                        '500 gigabyte hard drive'
                      ]
                    }";
            var result = Core.JSON.ParseJsonAsJObject(json);
            Assert.IsNull(result);
        }
    }
}
