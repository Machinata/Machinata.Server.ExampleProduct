using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Machinata.Core.Tests {
    [TestClass]
    public class ImageAnalysisTests {

        //[TestMethod] //Note: disabled for now, as needs some hard coded assets
        public void Core_Imaging_ImageAnalysis_TestDiffer() {
            var results = Machinata.Core.Imaging.ImageAnalysis.WriteImageDiffToFile(
                "C:\\Users\\dankrusi\\Desktop\\testing diff\\a.png",
                "C:\\Users\\dankrusi\\Desktop\\testing diff\\b.png",
                "C:\\Users\\dankrusi\\Desktop\\testing diff\\diff.png"
            );
        }

        
    }
}
