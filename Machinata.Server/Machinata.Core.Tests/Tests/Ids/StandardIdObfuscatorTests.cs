using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Core.Tests {

    [TestClass]
    public class StandardIdObfuscatorTests {
        

        [TestMethod]
        public void Core_Ids_StandardIdObfuscator_ValidateEdgeCases() {
            {
                var id = 1;
                var obfuscated = Core.Ids.Obfuscator.Default.ObfuscateId(id);
                var deobfuscated = Core.Ids.Obfuscator.Default.UnobfuscateId(obfuscated);
                Assert.AreEqual("hTKaaQ", obfuscated);
                Assert.AreEqual(id, deobfuscated);
            }
            {
                var id = 345353;
                var obfuscated = Core.Ids.Obfuscator.Default.ObfuscateId(id);
                var deobfuscated = Core.Ids.Obfuscator.Default.UnobfuscateId(obfuscated);
                Assert.AreEqual("nZKfKA", obfuscated);
                Assert.AreEqual(id, deobfuscated);
            }
            {
                var id = 788678678;
                var obfuscated = Core.Ids.Obfuscator.Default.ObfuscateId(id);
                var deobfuscated = Core.Ids.Obfuscator.Default.UnobfuscateId(obfuscated);
                Assert.AreEqual("piu-Bw", obfuscated);
                Assert.AreEqual(id, deobfuscated);
            }
        }

        private void _Core_Ids_StandardIdObfuscator_TestUniqueness(int start, int end) {
            var collected = new HashSet<string>();
            for(int id = start; id < end; id++) {
                var obfuscated = Core.Ids.Obfuscator.Default.ObfuscateId(id);
                Assert.IsFalse(collected.Contains(obfuscated));
                collected.Add(obfuscated);
                var deobfuscated = Core.Ids.Obfuscator.Default.UnobfuscateId(obfuscated);
                Assert.AreEqual(id, deobfuscated);
            }
        }
        
        [TestMethod]
        public void Core_Ids_StandardIdObfuscator_TestUniqueness_100K() {
            _Core_Ids_StandardIdObfuscator_TestUniqueness(1, 100000);
        }
        
        //[TestMethod]
        //NOTE: This test method needs TONS of memory
        public void Core_Ids_StandardIdObfuscator_TestUniqueness_All() {
            _Core_Ids_StandardIdObfuscator_TestUniqueness(1, int.MaxValue);
        }
        
        
        

    }
}
