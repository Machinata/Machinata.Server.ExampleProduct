using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Core.Tests {

    [TestClass]
    public class StandardCodeGeneratorTests {
        

        [TestMethod]
        public void Core_Ids_StandardCodeGenerator_ValidateEdgeCases() {
            {
                var id = 1;
                var code = Core.Ids.Code.Default.GenerateCodeForId(id);
                Assert.AreEqual("DQF3FUV", code);
            }
        }

        private void _Core_Ids_StandardCodeGenerator_TestUniqueness(int start, int end) {
            var collected = new HashSet<string>();
            for(int id = start; id < end; id++) {
                var code = Core.Ids.Code.Default.GenerateCodeForId(id);
                Assert.IsFalse(collected.Contains(code));
                collected.Add(code);
                Assert.AreEqual(7, code.Length);
            }
        }
        
        [TestMethod]
        public void Core_Ids_StandardCodeGenerator_TestUniqueness_100K() {
            _Core_Ids_StandardCodeGenerator_TestUniqueness(1, 100000);
        }
        
        //[TestMethod]
        //NOTE: This test method needs TONS of memory
        public void Core_Ids_StandardCodeGenerator_TestUniqueness_All() {
            _Core_Ids_StandardCodeGenerator_TestUniqueness(1, int.MaxValue/100);
        }
        
        
        

    }
}
