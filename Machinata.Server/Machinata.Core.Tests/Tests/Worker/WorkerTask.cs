using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Worker;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Machinata.Core.Tests {

    [TestClass]
    public class WorkerTaskTest {
        private WorkerPool _pool;

        private class TestSimpleWorkerTask : Core.Worker.WorkerTask {

            public bool Processed = false;

            public TestSimpleWorkerTask() {
                
            }

            public override void Process() {
                this.Processed = true;
            }

        }

        private class TestFailedWorkerTask : Core.Worker.WorkerTask {
            
            public TestFailedWorkerTask() {
                
            }

            public override void Process() {
                throw new Exception();
            }

        }

        private class TestRetryWorkerTask : Core.Worker.WorkerTask {

            public bool Processed = false;

            public int _maxAttempts;

            public override int MaxAttempts {
                get {
                    return _maxAttempts;
                }
            }

            public TestRetryWorkerTask(int maxAttempts) {
                _maxAttempts = maxAttempts;
            }

            public override void Process() {
                System.Threading.Thread.Sleep(33);
                if (this.Attempts < _maxAttempts) throw new Exception();
                this.Processed = true;
            }

        }

        [TestInitialize()]
        public void Startup() {
            _pool = Core.Worker.LocalPool.GetWorkerPool();
        }

        [TestMethod]
        public void Core_WorkerTask_TestSimpleTaskExec() {
            var task = new TestSimpleWorkerTask();
            _pool.QueueTask(task);
            while(!task.Processed) { System.Threading.Thread.Sleep(50);/* NOP */ }
        }
        
        [TestMethod]
        public void Core_WorkerTask_TestRetryTaskExec1() {
            var maxAttempts = 3;
            var task = new TestRetryWorkerTask(maxAttempts);
            _pool.QueueTask(task);
            while(!task.Processed) {  System.Threading.Thread.Sleep(50); /* NOP */ }
            Assert.IsTrue(task.Attempts == maxAttempts);
        }
        
        [TestMethod]
        public void Core_WorkerTask_TestRetryTaskExec2() {
            var maxAttempts = 1;
            var task = new TestRetryWorkerTask(maxAttempts);
            _pool.QueueTask(task);
            while(!task.Processed) { System.Threading.Thread.Sleep(50); /* NOP */ }
            Assert.IsTrue(task.Attempts == maxAttempts);
        }

        [TestCleanup]
        public void Cleanup() {
             _pool.Shutdown();
        }
    }
}
