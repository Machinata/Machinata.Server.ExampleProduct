using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Text;
using System.Security.Cryptography;

namespace Machinata.Core.Tests {
    [TestClass] 
    public class APIAuthTests {  
        
        private const string API_KEY_ID = "8a1832eebc57409897b150211617c2e1882ba29ab00c4f718bf1f4ec5ff2924d24f82a6565ed4457a638f2486f55cc83aac3c59358a141318185181f3f1fcc21";
        private const string API_KEY_SECRET = "4040d52984d04e60a463504c3556d876ce656f6ef24c4efbb751c1e7fc0bc2cf2f225e988b5c4d8aab281ac53a212fdb7b688d249bde4c89b8fdf763b81eac38";
        
        [TestMethod]
        public void Core_API_APIAuth_Digest() {
            var date = new DateTime(2018, 06, 14, 10, 55, 00, DateTimeKind.Utc);
            var timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(date);
            {
                long expected = 1528973700000;
                long generated = timestamp;
                Assert.AreEqual(expected, generated);
            }
            {
                string expected = "t6DWEJIxxVo2Flrd7nPX4BhNFi4Qo5qa7PlL5L/5WF4p0OrveO1oCmKd+YxEMytw1UBd00hdj3lhNK8O6h4adg==";
                string generated = Core.API.APIAuth.GenerateDigest(API_KEY_ID, API_KEY_SECRET, "GET", "/sandbox/test", timestamp);
                Assert.AreEqual(expected, generated);
            }
        }

        [TestMethod]
        public void Core_API_APIAuth_AuthenticationHeader() {
            var date = new DateTime(2018, 06, 14, 10, 55, 00, DateTimeKind.Utc);
            var timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(date);
            var expected =  "machinata 8a1832eebc57409897b150211617c2e1882ba29ab00c4f718bf1f4ec5ff2924d24f82a6565ed4457a638f2486f55cc83aac3c59358a141318185181f3f1fcc21:1528973700000:t6DWEJIxxVo2Flrd7nPX4BhNFi4Qo5qa7PlL5L/5WF4p0OrveO1oCmKd+YxEMytw1UBd00hdj3lhNK8O6h4adg==";
            var generated = Core.API.APIAuth.GenerateAuthenticationHeader(API_KEY_ID, API_KEY_SECRET, "GET", "/sandbox/test", timestamp);
            Assert.AreEqual(expected, generated);
        }

        [TestMethod]
        public void Core_API_APIAuth_HMACSHA512() {

            // Init
            var secret = "secret";
            var input = "input";
            UnicodeEncoding encoding = new UnicodeEncoding();
            HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(secret));
            hmac.Initialize();

            // Test input encoding
            byte[] bytes = encoding.GetBytes(input);
            {
                string generated = string.Join(",",bytes); 
                string expected = "105,0,110,0,112,0,117,0,116,0";
                Assert.AreEqual(expected, generated);

            }

            // Test hash
            byte[] hashBytes = hmac.ComputeHash(bytes);
            {
                string generated = Convert.ToBase64String(hashBytes);
                string expected = "abOd9yMVLkXNTxKN75Sa9nqfnGDfAyaNoU1UNG/gM+IJPIYmtXk8KzjgOvWjQfH1XM7Ik9Kr3eYV7DARKCHTtw==";
                Assert.AreEqual(expected, generated);
            }

        }
    }
}
