using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Linq;

namespace Machinata.Core.Tests {
    [TestClass]
    public class APICallTests {
        
        private const string API_KEY_ID = "ceb41097c09a4118af59011f129787255025cbef26bf4139a7fdafca80954cee6b224efba0304cb98c0b4574c1c5b61c4e346db78a484a57ac3747b72af87e8d";
        private const string API_KEY_SECRET = "b19836115c5e4ea7839c908227ccc7f91c3168bdab0e48d095bfcaadfefd2c4d853b082adedf4ad1859e8b457c95b402aa8ae37bf9c942b888cb7f27d6f32d86";
        
        // Disabled: these (obviously) only work on a very specific environment
        //[TestMethod]
        public void Core_API_APICall_TestVariable() {

            var call = new API.APICall("/api/admin/sandbox/echo");
            call.WithAPIKey(API_KEY_ID, API_KEY_SECRET);
            call.WithParameter("param1", "val1");
            call.Send();
            var json = call.JSON();

            Assert.AreEqual(json["data"]["form_0[param1]"].ToString(), "val1");
        }

        // Disabled: these (obviously) only work on a very specific environment
        //[TestMethod]
        public void Core_API_APICall_TestChaining() {

            var json = new API.APICall("/api/admin/sandbox/echo")
                .WithAPIKey(API_KEY_ID, API_KEY_SECRET)
                .WithParameter("param1", "val1")
                .Send()
                .JSON();

            Assert.AreEqual(json["data"]["form_0[param1]"].ToString(), "val1");
        }

        // Disabled: this requires a external API which might change
        //[TestMethod]
        public void Core_API_APICall_TestExternal() {

            var json = new API.APICall("/users","https://jsonplaceholder.typicode.com")
                .Send()
                .JSONArray();
            
            Assert.AreEqual(json.Count(), 10);
            Assert.AreEqual(json.First()["name"], "Leanne Graham");
        }

        
        
    }
}
