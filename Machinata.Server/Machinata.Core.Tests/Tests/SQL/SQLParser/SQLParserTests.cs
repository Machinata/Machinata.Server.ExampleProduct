using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Linq;

namespace Machinata.Core.Tests {
    [TestClass]
    public class SQLParserTests {

        private const string SQL_CREATE_COMPLEX =       
@"CREATE TABLE `AccessGroupAccessPolicies` (
  `AccessGroup_Id` int(11) NOT NULL,
  `AccessPolicy_Id` int(11) NOT NULL,
  `Price` double(11,2) NOT NULL,
  `ARN` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`AccessGroup_Id`,`AccessPolicy_Id`),
  UNIQUE KEY `EFIX_Name` (`Name`) USING HASH
  KEY `EFIX_AccessGroup_Id` (`AccessGroup_Id`) USING HASH,
  KEY `EFIX_AccessPolicy_Id` (`AccessPolicy_Id`) USING HASH,
  CONSTRAINT `FK_5bb6a4f357024663960639b487a8cc8b` FOREIGN KEY (`AccessPolicy_Id`) REFERENCES `AccessPolicies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_9597864601dd406288180338c0858978` FOREIGN KEY (`AccessGroup_Id`) REFERENCES `AccessGroups` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1";

        [TestMethod]
        public void Core_Util_SQL_Simplify_RemoveTableSettings() {
            var ret = Core.SQL.Simplify.RemoveTableSettings(SQL_CREATE_COMPLEX);
            Assert.IsTrue(ret.EndsWith(")"));
            Assert.IsFalse(ret.Contains("ENGINE="));
            Assert.IsFalse(ret.Contains("AUTO_INCREMENT="));
            Assert.IsFalse(ret.Contains("DEFAULT CHARSET="));
        }

        [TestMethod]
        public void Core_Util_SQL_SQLParser_CreateTable1() {
            // Parse
            var parser = new Core.SQL.SQLParser.Parser();
            parser.Parse(SQL_CREATE_COMPLEX);
            parser.Reduce();
            var result = parser.GeneratedSQL;
            var statement = parser.Statements.First();
            
            // Basic validations
            Assert.IsTrue(parser.Statements.Count == 1);
            Assert.IsTrue(statement.Columns.Count == 9);

            // Column validations
            foreach(var column in statement.Columns) {
                if(column.ColumnName == "`AccessGroup_Id`") {
                    Assert.IsTrue(column.ColumnType == SQL.SQLParser.SqlColumn.ColumnTypes.Member);
                }
                if (column.ColumnType == SQL.SQLParser.SqlColumn.ColumnTypes.Constraint) {
                    //TODO
                } 
            }
        }

        
        

    }
}
