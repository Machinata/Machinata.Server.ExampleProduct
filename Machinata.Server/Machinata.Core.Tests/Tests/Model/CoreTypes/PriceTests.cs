using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;

namespace Machinata.Core.Tests {
    [TestClass]
    public class PriceTests {

        [TestMethod]
        [ExpectedException(typeof(Exception),"A exception was expected.")]
        public void Core_Model_CoreTypes_Price_CurrencyMix() {
            var p1 = new Price(1.0m, "CHF");
            var p2 = new Price(2.0m, "USD");
            var p3 = p1 + p2;   
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyAdd() {
            var p1 = new Price(1.31m, "CHF");
            var p2 = new Price(32.19m, "CHF");
            var p3 = p1 + p2;
            Assert.AreEqual(p1.Value + p2.Value, p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
            Assert.AreEqual(p2.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyAdd2() {
            var p1 = new Price(-431.33m, "CHF");
            var p2 = new Price(-132.12m, "CHF");
            var p3 = p1 + p2;
            Assert.AreEqual(p1.Value + p2.Value, p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
            Assert.AreEqual(p2.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencySubtract() {
            var p1 = new Price(-431.33m, "CHF");
            var p2 = new Price(-132.12m, "CHF");
            var p3 = p1 - p2;
            Assert.AreEqual(p1.Value - p2.Value, p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
            Assert.AreEqual(p2.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyMultiply() {
            var p1 = new Price(-431.33m, "CHF");
            var p2 = new Price(132.12m, "CHF");
            var p3 = p1 * p2;
            Assert.AreEqual(Math.Round(p1.Value.Value * p2.Value.Value, 2), p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
            Assert.AreEqual(p2.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyMultiplyInt() {
            var p1 = new Price(-431.33m, "CHF");
            int factor = 44;
            var p3 = p1 * factor;
            Assert.AreEqual(p1.Value * factor, p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyDivideInt() {
            var p1 = new Price(-431.33m, "CHF");
            int factor = 44;
            var p3 = p1 / factor;
            Assert.AreEqual(Math.Round(p1.Value.Value / factor, 2), p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_CurrencyMultiplyDecimal() {
            var p1 = new Price(-431.33m, "CHF");
            decimal factor = 44m;
            var p3 = p1 * factor;
            Assert.AreEqual( Math.Round(p1.Value.Value * factor,2),p3.Value);
            Assert.AreEqual(p1.Currency, p3.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_FromString() {
            var p1 = new Price("CHF 4834.33");
            Assert.AreEqual(p1.Value, 4834.33m);
            Assert.AreEqual(p1.Currency, "CHF");

            var p2 = new Price("654834 CHF");
            Assert.AreEqual(p2.Value, 654834m);
            Assert.AreEqual(p2.Currency, "CHF");

            var p3 = new Price(".01 CHF");
            Assert.AreEqual(p3.Value, 0.01m);
            Assert.AreEqual(p3.Currency, "CHF");

            var p4 = new Price("343'344.01 CHF");
            Assert.AreEqual(p4.Value, 343344.01m);
            Assert.AreEqual(p4.Currency, "CHF");
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_ToString() {
            var p1 = new Price("4834.33 CHF");
            Assert.AreEqual(p1.Value, 4834.33m);
            Assert.AreEqual(p1.Currency, "CHF");
            Assert.AreEqual(p1.ToString(), "CHF 4'834.33");

            var p2 = new Price("654834 CHF");
            Assert.AreEqual(p2.Value, 654834m);
            Assert.AreEqual(p2.Currency, "CHF");
            Assert.AreEqual(p2.ToString(), "CHF 654'834.00");

            var p3 = new Price(".01 CHF");
            Assert.AreEqual(p3.Value, 0.01m);
            Assert.AreEqual(p3.Currency, "CHF");
            Assert.AreEqual(p3.ToString(), "CHF 0.01");

            var p4 = new Price("343'344.01 CHF");
            Assert.AreEqual(p4.Value, 343344.01m);
            Assert.AreEqual(p4.Currency, "CHF");
            Assert.AreEqual(p4.ToString(), "CHF 343'344.01");

            var p5 = new Price("0 CHF");
            Assert.AreEqual(p5.Value, 0.0m);
            Assert.AreEqual(p5.Currency, "CHF");
            Assert.AreEqual(p5.ToString(), "CHF 0.00");
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_ToCents() {
            var p1 = new Price("4834.33 CHF");
            Assert.AreEqual(483433, p1.ToCents());
            Assert.AreEqual("CHF", p1.Currency);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_ConvertCHFtoUSD() {
            /*
            var p1 = new Price("4834.33 CHF");
            var p2 = p1.ConvertToCurrency("USD");
            Assert.AreEqual(p2.Currency, "USD");
            Assert.AreEqual(p2.Value, 483433m);
            */
            //TODO: db connections not yet implmented for tests... needed by currency convert
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_ThousandsSeparator() {
            _saveCurrencyConfig();
            Core.Config.CurrencyFormat = "{1} {0:#,0.00}";
            Core.Config.CurrencyThousandsSeparatorChar = "'";

            var p1 = new Price("4834.33 CHF");
            Assert.AreEqual(483433, p1.ToCents());
            Assert.AreEqual("CHF", p1.Currency);
            Assert.AreEqual("CHF 4'834.33", p1.ToString());

            var p2 = new Price("834.33 CHF");
            Assert.AreEqual(83433, p2.ToCents());
            Assert.AreEqual("CHF", p2.Currency);
            Assert.AreEqual("CHF 834.33", p2.ToString());

            var p3 = new Price("1'454'834 CHF");
            Assert.AreEqual(145483400, p3.ToCents());
            Assert.AreEqual("CHF", p3.Currency);
            Assert.AreEqual("CHF 1'454'834.00", p3.ToString());

            _restoreCurrencyConfig();
        }

        [TestMethod]
        public void Core_Model_CoreTypes_Price_ZeroCentsChar() {
            _saveCurrencyConfig();
            Core.Config.CurrencyFormat = "{1} {0:0.00}";
            Core.Config.CurrencyZeroCentsChar = "–";

            var p1 = new Price("CHF 4834");
            Assert.AreEqual(483400, p1.ToCents());
            Assert.AreEqual("CHF", p1.Currency);
            Assert.AreEqual("CHF 4834.–", p1.ToString());

            var p2 = new Price("CHF 834.–");
            Assert.AreEqual(83400, p2.ToCents());
            Assert.AreEqual("CHF", p2.Currency);
            Assert.AreEqual("CHF 834.–", p2.ToString());

            var p3 = new Price("CHF 1454834.0");
            Assert.AreEqual(145483400, p3.ToCents());
            Assert.AreEqual("CHF", p3.Currency);
            Assert.AreEqual("CHF 1454834.–", p3.ToString());

            var p4 = new Price("834.– CHF"); // Reverse currency
            Assert.AreEqual(83400, p4.ToCents());
            Assert.AreEqual("CHF", p4.Currency);
            Assert.AreEqual("CHF 834.–", p4.ToString());

            var p5 = new Price("834.- CHF"); // Missleading minus (users will often type this)
            Assert.AreEqual(83400, p5.ToCents());
            Assert.AreEqual("CHF", p5.Currency);
            Assert.AreEqual("CHF 834.–", p5.ToString());

            var p6 = new Price("-834.- CHF"); // Possible confusing zero cents conversion
            Assert.AreEqual(-83400, p6.ToCents());
            Assert.AreEqual("CHF", p6.Currency);
            Assert.AreEqual("CHF -834.–", p6.ToString());

            _restoreCurrencyConfig();
        }

        #region Helper Methods

        private string _origCurrencyFormat = null;
        private string _origCurrencyThousandsSeparatorChar = null;
        private string _origCurrencyZeroCentsChar = null;
        private decimal? _origCurrencyConversionDefaultRounding = null;
        
        private void _saveCurrencyConfig() {
            _origCurrencyFormat = Core.Config.CurrencyFormat;
            _origCurrencyThousandsSeparatorChar = Core.Config.CurrencyThousandsSeparatorChar;
            _origCurrencyZeroCentsChar = Core.Config.CurrencyZeroCentsChar;
            _origCurrencyConversionDefaultRounding = Core.Config.CurrencyConversionDefaultRounding;
        }

        private void _restoreCurrencyConfig() {
            Core.Config.CurrencyFormat = _origCurrencyFormat ;
            Core.Config.CurrencyThousandsSeparatorChar = _origCurrencyThousandsSeparatorChar;
            Core.Config.CurrencyZeroCentsChar = _origCurrencyZeroCentsChar;
            Core.Config.CurrencyConversionDefaultRounding = _origCurrencyConversionDefaultRounding;
        }

        #endregion

    }
}
