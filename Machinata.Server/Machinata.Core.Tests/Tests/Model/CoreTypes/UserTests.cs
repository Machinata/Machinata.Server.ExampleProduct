using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;

namespace Machinata.Core.Tests {
    [TestClass]
    public class UserTests {

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetInitialsNoName() {
            var user = new User();
            Assert.IsNull(user.GetInitials());

        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetInitialsSpaces() {
            var user = new User();
            user.Name = "     ";
            Assert.IsNull(user.GetInitials());

        }
        [TestMethod]
        public void Core_Model_CoreTypes_User_GetInitialsSpacesAndLeters() {
            var user = new User();
            user.Name = "  d   ";
            Assert.AreEqual("D", user.GetInitials());

        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsThreeNamesSpaces() {
            var user = new User();
            user.Name = "  Hans Rudolf Muster    ";
            Assert.AreEqual("HM", user.GetInitials());
        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsThreeNamesNormal() {
            var user = new User();
            user.Name = "Hans Rudolf Muster";
            Assert.AreEqual("HM", user.GetInitials());
        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsFourNamesNormal() {
            var user = new User();
            user.Name = "Hans Rudolf Muster Koller";
            Assert.AreEqual("HK", user.GetInitials());
        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsOneNameNormal() {
            var user = new User();
            user.Name = "Hans";
            Assert.AreEqual("H", user.GetInitials());
        }

        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsOneNameSpaces() {
            var user = new User();
            user.Name = "    Hans    ";
            Assert.AreEqual("H", user.GetInitials());
        }


        [TestMethod]
        public void Core_Model_CoreTypes_User_GetinitalsEmptyString() {
            var user = new User();
            user.Name = "";
            Assert.IsNull(user.GetInitials());
        }



    }
}
