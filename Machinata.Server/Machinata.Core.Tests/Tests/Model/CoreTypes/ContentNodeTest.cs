using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;
using System.Linq;
using Machinata.Core.Util;

namespace Machinata.Core.Tests {
    [TestClass]
    public class ContentNodeTest {

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML1() {
            var onePath = "<p>&nbsp;test</p><p><img src=\"/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"><br></p>";
            var result = ContentNode.ParseContentPathsFromHTML(onePath);
            Assert.AreEqual(result.First(), "/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg");

        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML2() {
            var twoPaths = "<p>&nbsp;asdfas</p><p><img src=\"/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"></p><p><br></p><p><br></p><p><br></p><p>�lghjhs<br></p><p><br></p><p><img src=\"/content/image/Ir1SCA/Nerves-Slotty-IntroAnimation-v1.gif\"></p><p><br></p>";
            var result = ContentNode.ParseContentPathsFromHTML(twoPaths);
            Assert.AreEqual(result.First(), "/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg");
            Assert.AreEqual(result.Last(), "/content/image/Ir1SCA/Nerves-Slotty-IntroAnimation-v1.gif");

        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML_NoPath1() {
            var twoPaths = "<p>&nbsp;asdfas</p><p><img src=\"/contentimage/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"></p><p><br></p><p><br></p><p><br></p><p>�lghjhs<br></p><p><br></p><p><img src=\"content/image/Ir1SCA/Nerves-Slotty-IntroAnimation-v1.gif\"></p><p><br></p>";
            var result = ContentNode.ParseContentPathsFromHTML(twoPaths);
            Assert.AreEqual(result.Count(), 0);
          

        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML_OnePath_OneBrokenPath() {
            var twoPaths = "<p>&nbsp;asdfas</p><p><img src=\"/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"></p><p><br></p><p><br></p><p><br></p><p>�lghjhs<br></p><p><br></p><p><img src=\"content/image/Ir1SCA/Nerves-Slotty-IntroAnimation-v1.gif></p><p><br></p>";
            var result = ContentNode.ParseContentPathsFromHTML(twoPaths);
            Assert.AreEqual(result.First(), "/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg");
            Assert.AreEqual(result.Count(), 1);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML_Null() {
            var result = ContentNode.ParseContentPathsFromHTML(null);
            Assert.AreEqual(result.Count(), 0);
        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML_Start() {
            var onePath = "\"/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"><br></p>";
            var result = ContentNode.ParseContentPathsFromHTML(onePath);
            Assert.AreEqual(result.First(), "/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg");
        }

        [TestMethod]
        public void Core_Model_CoreTypes_ContentNode_ParsePathFromHTML_End() {
            var onePath = "somecontent src=\"asdfasdf\"\"/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg\"";
            var result = ContentNode.ParseContentPathsFromHTML(onePath);
            Assert.AreEqual(result.First(), "/content/image/D_u3Zw/Nerves-StartPage-Loop-v1.jpg");
        }




    }
}
