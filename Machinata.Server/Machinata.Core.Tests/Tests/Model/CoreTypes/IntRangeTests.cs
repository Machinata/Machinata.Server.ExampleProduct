using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;

namespace Machinata.Core.Tests {
    [TestClass]
    public class IntRangeTests {

        [TestMethod]
        //[ExpectedException(typeof(Exception),"A exception was expected.")]
        public void Core_Model_CoreTypes_IntRange_FromString_Simple() {
            var intRange1 = new IntRange("1 - 10");
            Assert.AreEqual(1, intRange1.A);
            Assert.AreEqual(10, intRange1.B);

            var intRange2 = new IntRange("1-10");
            Assert.AreEqual(1, intRange2.A);
            Assert.AreEqual(10, intRange2.B);

            var intRange3 = new IntRange("1- 10");
            Assert.AreEqual(1, intRange3.A);
            Assert.AreEqual(10, intRange3.B);

            var intRange4 = new IntRange("1 -10");
            Assert.AreEqual(1, intRange4.A);
            Assert.AreEqual(10, intRange4.B);
        }

        [TestMethod]
        //[ExpectedException(typeof(Exception),"A exception was expected.")]
        public void Core_Model_CoreTypes_IntRange_FromString_Negative() {
            var intRange1 = new IntRange("-3 - -11");
            Assert.AreEqual(-3, intRange1.A);
            Assert.AreEqual(-11, intRange1.B);

            var intRange2 = new IntRange("-3- -11");
            Assert.AreEqual(-3, intRange2.A);
            Assert.AreEqual(-11, intRange2.B);

            var intRange3 = new IntRange("-3--11");
            Assert.AreEqual(-3, intRange3.A);
            Assert.AreEqual(-11, intRange3.B);
        }

    }
}
