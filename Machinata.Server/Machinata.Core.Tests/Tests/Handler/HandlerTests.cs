using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using System.Collections.Generic;
using System.Globalization;

namespace Machinata.Core.Tests {
    [TestClass]
    public class HandlerTest {

        // TODO MOVE TO SEPERATE CLASS
        // INITIALIZE TO SET INVATIANT CULTURE
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context) {
            // Set invariant culture to prevent unwanted obscurities
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "A exception was expected.")]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationFailNoArn() {

            var handler = new TestHandlerNoAdmin();
            handler.RequireAdminARNOrBusinessOrUserAssociation(null, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "A exception was expected.")]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationFailNoUser() {

            var handler = new TestHandlerNoAdmin();
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "A exception was expected.")]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationFailWithUser() {

            var handler = new TestHandlerNoAdmin();
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(Backend401Exception), "A exception was expected.")]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationWithWrongUser() {
            var handler = new TestHandlerNoAdmin();
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", null, new User());
        }



        [TestMethod]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationWithUser() {
            var handler = new TestHandlerNoAdmin();
            handler.Route = new Routes.Route("/whatever", Handler.Verbs.Any, null, null);
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", null, handler.User);
        }

        [TestMethod]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationWithAdmin() {
            var handler = new TestHandlerIsAdmin();
            handler.Route = new Routes.Route("/whatever", Handler.Verbs.Any, null, null);
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", null, handler.User);
        }

        [TestMethod]
        [ExpectedException(typeof(Backend401Exception), "A exception was expected.")]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationWithWrongBusiness() {
            var handler = new TestHandlerNoAdmin();
            handler.Route = new Routes.Route("/whatever", Handler.Verbs.Any, null, null);
            var business = new Business();
            handler.User.Businesses = new List<Business>() { new Business() };
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", business, null);
        }

        [TestMethod]
        public void Core_Model_Handler_RequireAdminARNOrBusinessOrUserAssociationWithCorrectBusiness() {
            var handler = new TestHandlerNoAdmin();
            handler.Route = new Routes.Route("/whatever", Handler.Verbs.Any, null, null);
            var business = new Business();
            handler.User.Businesses = new List<Business>() { business };
            handler.RequireAdminARNOrBusinessOrUserAssociation("/whatever", business, handler.User);
        }


        public class TestHandlerNoAdmin : Handler.Handler {

            override public AuthToken GetAuthTokenFromContext() {
                var token = new AuthToken();
                token.User = new User();
                return token;

            }

            public override bool ValidateARN(string requiredARN, bool throwExceptions = true) {
                return false;
            }


        }


    }

    public class TestHandlerIsAdmin : Handler.Handler {

        override public AuthToken GetAuthTokenFromContext() {
            var token = new AuthToken();
            token.User = new User();
            return token;

        }

        public override bool ValidateARN(string requiredARN, bool throwExceptions = true) {
            return true;
        }

    }
}
