using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.API {

    #region APIAuth Plug-n-Play Class

    /// <summary>
    /// Class for validating API Request Authentications
    /// </summary>
    public class APIAuth {
        
        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public const string AUTHENTICATION_HEADER = "Authentication";

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public const string AUTHENTICATION_HEADER_TYPE = "machinata";

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public const long AUTHENTICATION_TIMEWINDOW_SECONDS = 15*60; // 15 min

        /// <summary>
        /// Validates the request for a valid API key and signature in all aspects:
        ///     HTTPS
        ///     Header Valid
        ///     Key ID valid
        ///     Key Enabled
        ///     Key Not Expired
        ///     Timestamp within AUTHENTICATION_TIMEWINDOW_SECONDS
        ///     Signature valid
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">
        /// no-auth-header;No authentication header was provided.
        /// or
        /// invalid-auth-header;The authentication header is not valid. It must be in the format 'Authentication: machinata {api-key}:{request-digest}'
        /// or
        /// invalid-api-key-signature
        /// or
        /// invalid-api-key
        /// or
        /// invalid-api-key
        /// or
        /// invalid-api-key-signature
        /// </exception>
        public static APIKey ValidateRequest(HttpContext context, ModelContext db) {
            // Enabled?
            if(Core.Config.APIKeysEnabled == false) {
                throw new BackendException("api-key-not-enabled","Access with API keys is not enabled.");
            }
            // Init
            var method = context.Request.HttpMethod;
            var resource = context.Request.RawUrl;
            var authHeader = context.Request.Headers[AUTHENTICATION_HEADER];
            if (authHeader == null) throw new BackendException("no-auth-header","No authentication header was provided.");
            // Verify https
            if(Core.Config.APIKeysRequireHTTPS && context.Request.Url.Scheme != "https") {
                throw new BackendException("api-key-no-https","You must connect with HTTPS for this API call.");
            }
            // Look at the auth header
            var authSegs = authHeader.Split(' ', ':');
            if(authSegs.Length < 4 || authSegs[0] != AUTHENTICATION_HEADER_TYPE) throw new BackendException("invalid-auth-header","The authentication header is not valid. It must be in the format 'Authentication: machinata {api-key}:{request-digest}'");
            var keyId = authSegs[1];
            var authTimestampString = authSegs[2];
            var authDigest = authSegs[3];
            long authTimestamp = long.Parse(authTimestampString);
            // Validate the sent timestamp with the current timestamp to ensure
            // we can only do a reply attack within the given window of 
            // AUTHENTICATION_TIMEWINDOW_SECONDS.
            long currentTimestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow);
            long timestampDiff = Math.Abs(authTimestamp - currentTimestamp);
            if(timestampDiff > (AUTHENTICATION_TIMEWINDOW_SECONDS*1000)) {
                throw new BackendException("invalid-api-key-signature",$"The api signature is no longer valid due to the timestamp being to old. Timestamp on server UTC: "+currentTimestamp);
            }
            // Get the key from DB
            APIKey apiKey = db.APIKeys()
                .Include(nameof(APIKey.User))
                .SingleOrDefault(e => e.KeyId == keyId);
            // Validate
            if(apiKey == null) throw new BackendException("invalid-api-key",$"The api key '{keyId}' is not valid.");
            if(apiKey.IsValid == false) throw new BackendException("invalid-api-key",$"The api key '{keyId}' is no longer valid.");
            // Validate the header
            var expectedAuthHeader = GenerateAuthenticationHeader(apiKey.KeyId, apiKey.KeySecret, method, resource, authTimestamp);
            if(expectedAuthHeader != authHeader) {
                throw new BackendException("invalid-api-key-signature",$"The api key signature for key '{keyId}' is not valid for this request.");
            }
            // Return
            return apiKey;
        }

        /// <summary>
        /// Generates the authentication header for the key id and secret.
        /// The header looks like this:
        /// ``` machinata {keyId}:{timestamp}:{digest} ```
        /// The digest is the HMAC SHA512 of:
        /// ``` {method}+{resource}+{timestamp} ```
        /// Thus, the full authentication header is:
        /// ``` machinata {keyId}:{timestamp}:hmac({method}+{resource}+{timestamp}) ```
        /// </summary>
        /// <param name="keyId">The key identifier.</param>
        /// <param name="keySecret">The key secret.</param>
        /// <param name="method">The method.</param>
        /// <param name="resource">The resource.</param>
        /// <param name="timestamp">The timestamp, in UTC milliseconds since 1970 (unix timestamp).</param>
        /// <returns>Returns the generated authentication header</returns>
        public static string GenerateAuthenticationHeader(string keyId, string keySecret, string method, string resource, long? timestamp = null) {
            // Init
            method = method.ToUpper();
            if(timestamp == null) timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow);
            // Generate digest and header
            var digest = GenerateDigest(keyId, keySecret, method, resource, timestamp.Value);
            //                        0                    1         2          3
            var header = $"{AUTHENTICATION_HEADER_TYPE} {keyId}:{timestamp}:{digest}";
            return header;
        }

        /// <summary>
        /// Generates the digest for the key id and secret.
        /// A digest is the HMAC SHA512 of:
        /// ```
        ///     {method}+{resource}+{timestamp}
        /// ```
        /// </summary>
        /// <param name="keyId">The key identifier.</param>
        /// <param name="keySecret">The key secret.</param>
        /// <param name="method">The method.</param>
        /// <param name="resource">The resource.</param>
        /// <param name="timestamp">The timestamp, in UTC milliseconds since 1970 (unix timestamp).</param>
        /// <returns></returns>
        public static string GenerateDigest(string keyId, string keySecret, string method, string resource, long timestamp) {
            var digestUnhashed = $"{method}+{resource}+{timestamp}";
            var digest = _generateHMAC(keySecret, digestUnhashed);
            return digest;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secret"></param>
        /// <param name="input"></param>
        /// <hidden/>
        /// <returns></returns>
        private static string _generateHMAC(string secret, string input) {
            UnicodeEncoding encoding = new UnicodeEncoding();
            HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(secret));
            hmac.Initialize();
            byte[] bytes = encoding.GetBytes(input);
            byte[] hash = hmac.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }

    }
    #endregion

}
