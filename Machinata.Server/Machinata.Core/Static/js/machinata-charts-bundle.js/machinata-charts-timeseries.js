

/* ======== Machinata Charts Timeseries (line, bar) ======== */

Machinata.setupTimeSeriesChart = function (type, chart, svg, data, size, margin) {
    // Register the common class
    chart.addClass("time-series");

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 30 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width - margin.left - margin.right;
    var height = size.height - margin.top - margin.bottom;
    var format = data.format;
    var min = 0;

    // Helper functions
    function safeValue(val) {
        if (val == null) return min; // return min;
        else return val;
    }
    var xIsTime = (data["x-axis-is-time"] == true);
    var timeFormat = data["time-format"];
    var timeFormatDatepicker = null;
    var timeFormatJavascript = null;
    if (timeFormat != null) timeFormatDatepicker = Machinata.convertDotNetTimeFormatToDatepickerFormat(timeFormat);
    if (timeFormat != null) timeFormatJavascript = Machinata.convertDotNetTimeFormatToJavascriptFormat(timeFormat);
    function parseDate(val) {
        var d = Machinata.dateFromString(val, timeFormatDatepicker);
        d = new Date(d + " UTC");
        return d;
    }
    function formattedDate(val) {
        return Machinata.formattedDate(val, timeFormatJavascript);
    }
    function getLineDashArrayForName(name) {
        if (name == "solid") return null;
        else if (name == "dashed") return "4";
        else if (name == "dashed-3") return "3";
        else if (name == "dashed-2") return "2";
        else if (name == "dashed-1") return "1";
        else if (name == "dashed-4-1") return "4 1";
        else return name;
    }

    // Transform series to D3 table
    var tdata = [];
    var tkeys = [];
    var ttitles = [];
    var tdots = [];
    var tlines = [];
    var tlabels = [];
    var tcolors = {};
    for (var i = 0 ; i < data.series.length; i++) {
        var s = data.series[i];
        if (s.name == null) s.name = "Untitled" + (i + 1);
        data[s.name] = s;
        tkeys.push(s.name);
        ttitles[s.name] = s.title;
        tdots[s.name] = s.dots;
        tlines[s.name] = s.line;
        if (s.color != null) tcolors[s.name] = s.color;
        for (var ii = 0 ; ii < s["data-points"].length; ii++) {
            var dp = s["data-points"][ii];
            // Convert labels if we are on time
            // We need to do this at this point to ensure
            // that the labels will matchup since we might to a conversion from UTC
            // to local time
            if (xIsTime) {
                dp.date = parseDate(dp.label);
                dp.label = formattedDate(dp.date);
            }
            // Find maching label
            var row = null;
            for (var tt = 0; tt < tdata.length; tt++) {
                var r = tdata[tt];
                if (r.label == dp.label) {
                    row = r;
                    break;
                }
            }
            // Create new row if not exists
            if (row == null) {
                tlabels.push(dp.label);
                var row = {};
                row.label = dp.label;
                if (xIsTime) {
                    row.date = dp.date;
                    row.xval = dp.date;
                } else {
                    row.xval = row.label;
                }
                tdata.push(row);
            }
            row[s.name] = safeValue(dp.value);
        }
    }
    //Machinata.debug(tdata);

    // Prepare main svg elem
    var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Create scales
    var x0 = d3.scaleBand()
        .rangeRound([0, width])
        .paddingInner(0.1);

    var y = d3.scaleLinear().rangeRound([height, 0]);

    var zDefault = d3.scaleOrdinal(Machinata.getChartScheme(chart));
    var z = function (key) {
        if (tcolors[key] != null) return tcolors[key];
        return zDefault(key);
    }
    var t = function (key) {
        if (ttitles[key] != null) return ttitles[key];
        return key;
    }

    // Get min
    var min = 0;
    if (data["min-value"] == null || data["min-value"] == "auto") {
        min = d3.min(tdata, function (d) { return d3.min(tkeys, function (key) { return safeValue(d[key]); }); });
    } else {
        min = parseFloat(data["min-value"]);
    }

    // Get max
    var max = 0;
    if (data["max-value"] == null || data["max-value"] == "auto") {
        max = d3.max(tdata, function (d) { return d3.max(tkeys, function (key) { return safeValue(d[key]); }); });
    } else {
        max = parseFloat(data["max-value"]);
    }

    // Min/max padding
    var minMaxpadding = 0;
    if (data["min-max-padding"] == null || data["min-max-padding"] != null) {
        minMaxpadding = (max - min) * 0.1;
    }
    if (data["min-max-padding"] != null && data["min-max-padding"] != "") {
        minMaxpadding = parseFloat(data["min-max-padding"]);
    }
    if (minMaxpadding != 0) {
        if(min != 0) min = min - minMaxpadding;
        max = max + minMaxpadding;
    }

    // Map data
    x0.domain(tdata.map(function (d) { return d.xval; }));
    y.domain([
        min,
        max
    ]).nice();

    // Get curve
    var lineCurve = d3.curveCatmullRom;
    if (!String.isEmpty(chart.attr("chart-curve"))) {
        // See https://bl.ocks.org/d3noob/ced1b9b18bd8192d2c898884033b5529
        var strCurve = chart.attr("chart-curve");
        if (strCurve == "curveLinear") return lineCurve = d3.curveLinear;
        if (strCurve == "curveStep") return lineCurve = d3.curveStep;
        if (strCurve == "curveStepBefore") return lineCurve = d3.curveStepBefore;
        if (strCurve == "curveStepAfter") return lineCurve = d3.curveStepAfter;
        if (strCurve == "curveBasis") return lineCurve = d3.curveBasis;
        if (strCurve == "curveCardinal") return lineCurve = d3.curveCardinal;
        if (strCurve == "curveMonotoneX") return lineCurve = d3.curveMonotoneX;
        if (strCurve == "curveCatmullRom") return lineCurve = d3.curveCatmullRom;
    }

    // Area
    if (data["area-min"] != null && data["area-max"] != null) {
        // Get series data keys
        var areaMinKey = data["area-min"];
        var areaMaxKey = data["area-max"];
        // Create area
        var area = d3.area()
            .curve(lineCurve)
            .x(function (d) { return x0(d.xval); })
            .y0(function (d) { return y(safeValue(d[areaMinKey])); })
            .y1(function (d) { return y(safeValue(d[areaMaxKey])); });
        // Get color
        var areaColor = data["area-color"];
        if (areaColor == null) {
            // Get average of the two
            areaColor = d3.scaleLinear()
                .domain([0, 1])
                .range([z(areaMinKey), z(areaMaxKey)])(0.5);
        }
        // Add path
        g.append('path')
            .data([tdata])
            .attr('class', 'area')
            .attr('fill', areaColor)
            .attr('d', area);
    }

    // Zero line
    if (data["zero-line"] != null) {
        var yy = parseFloat(data["zero-line"]);
        var line = d3.line()([[x0.range()[0], y(yy)], [x0.range()[1], y(yy)]]);
        g.append("path")
            .attr("class", "line zero-line")
            .attr("d", line);
    }

    // Segments
    if (data["segments"] != null && data["segments"].length > 0) {
        // Init data
        var segments = data["segments"];
        for (var i = 0; i < segments.length; i++) {
            if (xIsTime) {
                segments[i].x1val = parseDate(segments[i]["label-start"]);
                segments[i].x2val = parseDate(segments[i]["label-end"]);
            } else {
                segments[i].x1val = (segments[i]["label-start"]);
                segments[i].x2val = (segments[i]["label-end"]);
            }
        }
        var seg = g.append("g")
            .attr("class","segments")
            .selectAll("rect")
            .data(data["segments"])
            .enter();
        seg
            .append("rect")
            .attr("class", function (d) { return "mod2-"+d.mod2; })
            .attr("x", function (d) { return x0(d.x1val); })
            .attr("y", function (d) { return (0); })
            .attr("width", function (d) { return x0(d.x2val) - x0(d.x1val); })
            .attr("height", function (d) { return height; })
            .attr("fill", function (d) { return d.color; });
            seg
            .append("text")
            .attr("x", function (d) { return x0(d.x1val)+(x0(d.x2val) - x0(d.x1val)) / 2; })
            .attr("y", 0)
            .attr("dy", "-0.3em")
            .attr("text-anchor", "middle")
            .text(function (d) { return d.label; });
    }

    // Horizontal lines
    if (data["y-axis-lines"] == true) {
        g.append("g")
        .attr("class", "axis-lines")
        .call(
            d3.axisLeft(y)
                .ticks()
                .tickSize(-width)
                .tickFormat("")
        );
    }

    // Create bars
    if (type == "bar") {
        var x1 = d3.scaleBand().padding(0.05);
        x1.domain(tkeys).rangeRound([0, x0.bandwidth()]);
        g.append("g")
            .selectAll("g")
            .data(tdata)
            .enter()
            .append("g")
            .attr("transform", function (d) { return "translate(" + x0(d.xval) + ",0)"; })
            .selectAll("rect")
            .data(function (d) { return tkeys.map(function (key) { return { key: key, value: safeValue(d[key]), label: d.label }; }); })
            .enter()
            .append("rect")
            .attr("class", "show-value-on-hover")
            .attr("x", function (d) { return x1(d.key); })
            .attr("y", function (d) { return y(d.value); })
            .attr("width", x1.bandwidth())
            .attr("height", function (d) { return height - y(d.value); })
            .attr("fill", function (d) { return z(d.key); });
    }

    // Create line graph
    if (type == "line") {
        
        // Create path for each series
        for (var i = 0 ; i < tkeys.length; i++) {
            var key = tkeys[i];
            if (tlines[key] == null) continue;
            var line = d3.line()
                .curve(lineCurve)
                .x(function (d) { return x0(d.xval); })
                .y(function (d) { return y(safeValue(d[key])); });
            g.append("path")
                .data([tdata])
                .attr("class", "line")
                .attr("d", line)
                .attr("stroke", function (d) { return z(key); })
                .attr("stroke-dasharray", function (d) {
                    return getLineDashArrayForName(tlines[key]);
                });
        }
        // Dots
        g.append("g")
            .selectAll("g")
            .data(tdata)
            .enter().append("g")
            .attr("transform", function (d) { return "translate(" + x0(d.xval) + ",0)"; })
            .selectAll("rect")
            .data(function (d) { return tkeys.map(function (key) { return { key: key, value: safeValue(d[key]), label: d.label }; }); })
            .enter().append("circle")
            .attr("opacity", function (d) {
                if (tdots[d.key] == false) return "0.0";
                else return null;
            })
            .attr("class", "show-value-on-hover")
            .attr("cx", function (d) { return 0; })
            .attr("cy", function (d) { return y(d.value); })
            .attr("r", 4)
            .attr("fill", function (d) { return z(d.key); });

    }

    // Bottom axis (x)
    var showLabels = true;
    if (data["x-axis-labels-every"] == 0) showLabels = false;
    //if ((tlabels.length) > 200) showLabels = false; // deprecated because our axis is now smarter...
    if (showLabels) {
        var maxLabelSize = 100;
        var minLabelSize = 10;
        var pixelsPerLabel = width / tlabels.length;
        var labelsEveryX = 1; // 1 = always
        if (data["x-axis-labels-every"] != null) labelsEveryX = data["x-axis-labels-every"];
        if (pixelsPerLabel < minLabelSize) {
            // Autofit
            var targetLabelSize = maxLabelSize / 2;
            var targetLabelCount = Math.floor(width/targetLabelSize);
            labelsEveryX = Math.floor(tlabels.length / targetLabelCount);
        }
        // Calculate label rotation
        var labelRotation = 0; // 0 to -60
        pixelsPerLabel = width / (tlabels.length/labelsEveryX);
        if (pixelsPerLabel < maxLabelSize) {
            var p = pixelsPerLabel/maxLabelSize;
            labelRotation = -(1.0 - p) * 90;
        }
        if (data["x-axis-labels-rotation"] != null) {
            labelRotation = parseFloat(data["x-axis-labels-rotation"]);
        }
        var offsetY = 0;
        if (labelRotation == 0) offsetY = 10;
        //alert("labelRotation:" + labelRotation);
        //alert("pixelsPerLabel:" + pixelsPerLabel);
        //alert("labelsEveryX:" + labelsEveryX);
        var axisX = g.append("g")
            .attr("class", "axis x-axis")
            .attr("transform", "translate(0," + height + ")");
        // Ticks
        if (xIsTime) axisX.call(d3.axisBottom(x0).tickFormat(formattedDate));
        else axisX.call(d3.axisBottom(x0).ticks(null,"s"));
        axisX.selectAll("text")
            .attr("text-anchor", "end")
            .attr("dy", offsetY)
            .attr("x", "-7")
            .attr("class", function (d, i) {
                if (i % labelsEveryX != 0) d3.select(this).remove();
            })
            .attr("transform", "rotate("+labelRotation+")");
    } else {
        Machinata.debug("Too many labels (" + (tlabels.length / tkeys.length) + ") for x-axis, will skip");
    }

    // Left axis (y)
    g.append("g")
        .attr("class", "axis y-axis")
        .call(d3.axisLeft(y).ticks(null, "s"))
        .append("text")
        .attr("x", 2)
        .attr("y", y(y.ticks().pop()) + 0.5)
        .attr("dy", "0.32em")
        //.attr("fill", "#000")
        .attr("text-anchor", "start")
        .text(data.metric);

    // Create legend
    var legendX2 = width;
    if (data["legend-position"] != null) {
        var pos = data["legend-position"];
        if (pos == "center") legendX2 = width * 0.5;
        else if (pos.indexOf("%") != -1) {
            legendX2 = width * (parseFloat(pos.replace("%","")/100.0));
        }
    }
    var legendBoxSize = 19;
    var legendDashedLine = d3.line().x(function (d) { return d.x; }).y(function (d) { return d.y; });
    var legend = g.append("g")
        .attr("class", "legend")
        .attr("text-anchor", "end")
        .selectAll("g")
        .data(tkeys.slice().reverse())
        .enter().append("g")
        .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; });
    // Legend boxes
    legend.append("rect")
        .attr("x", legendX2 - legendBoxSize)
        .attr("width", legendBoxSize)
        .attr("height", legendBoxSize)
        .attr("fill", function (d) {
            if (tlines[d] == null) return z(d);
            return "transparent";
        });
    // Legend box line (for dashed lines)
    legend.append("path")
        .attr("d", function (d) {
            if (tlines[d] == null) return null;
            return legendDashedLine([{ "x": legendX2 - legendBoxSize, "y": legendBoxSize / 2 }, { "x": legendX2, "y": legendBoxSize / 2 }]);
        })
        .attr("stroke-dasharray", function (d) {
            return getLineDashArrayForName(tlines[d]);
        })
        .attr("stroke", z)
        .attr("stroke-width", legendBoxSize + "px")
        .attr("class", function (d) { return "line legend-line " + tlines[d]; })
        .attr("fill", "none");
    // Legend labels
    legend.append("text")
        .attr("x", legendX2 - (legendBoxSize + 5))
        .attr("y", 9.5)
        .attr("dy", "0.32em")
        .attr("fill", z)
        .text(function (d) { return t(d); });

    bindTooltip(g.selectAll(".show-value-on-hover"));

    function bindTooltip(selection) {
        // Create tooltop and it's background
        var tooltipBackground = g.append('rect')
                .attr('class', 'tooltip-background')
                .style("position", "absolute")
                .attr("display", "none")
                .style("pointer-events", "none");
        var tooltip = g.append('text')
                .attr('class', 'tooltip')
                .style("position", "absolute")
                .attr("display", "none")
                .style("pointer-events", "none")
                .html('<tspan class="title" x="0" dy="-0.2em"></tspan><tspan class="subtitle" x="0" dy="+1em"></tspan>')
                .style('alignment-baseline', 'central')
                .style('text-anchor', 'middle');
        // Bind events
        selection.on('mouseenter', function (data) {
            // Update text
            tooltip.selectAll("tspan.title").text(t(data.key));
            tooltip.selectAll("tspan.subtitle").text(data.label + ": " + data.value);
            // Update UI
            tooltip.attr("display", "inline");
            tooltip.each(function () {
                var bbox = this.getBBox();
                tooltipBackground
                    .attr("width", bbox.width + 12)
                    .attr("height", bbox.height + 6);
            });
            tooltipBackground.attr("display", "inline");
        });
        selection.on('mousemove', function (data) {
            var coordinates = d3.mouse(g.node());
            var tooltipOffsetY = -22;
            tooltip.attr("transform", "translate(" + coordinates[0] + "," + (coordinates[1] + tooltipOffsetY) + ")");
            tooltipBackground.attr("transform", "translate(" + (coordinates[0] - tooltipBackground.attr("width") / 2) + "," + ((coordinates[1] + tooltipOffsetY) - tooltipBackground.attr("height") / 2 - 0) + ")");
        });
        selection.on('mouseout', function () {
            tooltip.attr("display", "none");
            tooltipBackground.attr("display", "none");
        });
    }
};




/* ======== Machinata Charts Line ======== */

Machinata.lineChart = function (chart, svg, data, size, margin) {
    Machinata.setupTimeSeriesChart("line", chart, svg, data, size, margin);
};



/* ======== Machinata Charts Bar ======== */

Machinata.barChart = function (chart, svg, data, size, margin) {
    Machinata.setupTimeSeriesChart("bar", chart, svg, data, size, margin);
};