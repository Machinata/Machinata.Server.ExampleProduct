

/* ======== Machinata Charts Tree ======== */

Machinata.timelineChart = function (chart, svg, data, size, margin) {

    // Original from http://bl.ocks.org/TBD/600b23e56545026ae6fda2905efa42ce
    // Highly modified...

    // Set geometry
    var margin = { top: 0, right: 0, bottom: 0, left: 0 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width;
    var height = size.height;


    // Create master groups (one for zoomable timeline and one for fixed legend)

    var svgz = svg.append("g").attr("class", "zoomable");
    var svgl = svg.append("g").attr("class", "legends");

    // Date parser

    var parser = d3.isoParse;
    function parseDate(val) {
        var d = parser(val);
        //TODO @dan figure this out
        //d = new Date(d + " UTC");
        return d;
    }

    // Modify data a bit, cache some parsings

    var maxTrackNameLen = 0;
    for (var i = 0; i < data.tracks.length; i++) {
        if (data.tracks[i].group == null) data.tracks[i].group = "track_" + i;
        if (data.tracks[i].start != null) data.tracks[i].startParsed = parseDate(data.tracks[i].start);
        if (data.tracks[i].end != null) data.tracks[i].endParsed = parseDate(data.tracks[i].end);
        if (data.tracks[i].name.length > maxTrackNameLen) maxTrackNameLen = data.tracks[i].name.length;
        for (var ii = 0; ii < data.tracks[i].events.length; ii++) {
            if (data.tracks[i].events[ii].date != null) data.tracks[i].events[ii].dateParsed = parseDate(data.tracks[i].events[ii].date);

        }
    }
    var trackGroups = d3.nest().key(function (d) {
        return d.group;
    }).entries(data.tracks);
    for (var i = 0; i < trackGroups.length; i++) {
        // Set the track index for track groups
        var trackGroup = trackGroups[i];
        for (var ii = 0; ii < trackGroup.values.length; ii++) {
            trackGroup.values[ii].index = i;
        }
        // Auto stack?
        var stackingEnabled = false;
        if (stackingEnabled == true && trackGroup.values.length > 1) {
            // We have multiple tracks grouped - there might be collisions
            // so we will need to stack them automatically
            for (var ii = 0; ii < trackGroup.values.length; ii++) {
                var track = trackGroup.values[ii];
                track.collisions = 0;
                track.collisionIndex = ii;
                for (var iii = 0; iii < trackGroup.values.length; iii++) {
                    if (ii == iii) continue;
                    var otherTrack = trackGroup.values[iii];
                    // Collide?
                    if( 
                        (otherTrack.startParsed > track.startParsed && otherTrack.startParsed < track.endParsed )
                            ||
                        (track.startParsed > otherTrack.startParsed && track.startParsed < otherTrack.endParsed)
                            ||
                        (otherTrack.endParsed > track.startParsed && otherTrack.endParsed < track.endParsed)
                            ||
                        (track.endParsed > otherTrack.startParsed && track.endParsed < otherTrack.endParsed)
                      ) {
                        track.collisions = track.collisions + 1;
                    }
                }
            }
        }
    }
    var numTracks = trackGroups.length;



    // Initailize helper functions, scales

    var daysToPixels = function (days, scale) {
        var d1 = new Date();
        if (scale == null) scale = x;
        return scale(d3.timeDay.offset(d1, days)) - scale(d1);
    }

    var minX = d3.min(data.tracks, function (d) {
        return d.startParsed;
    });

    var maxX = d3.max(data.tracks, function (d) {
        return d.endParsed;
    });

    var x = d3.scaleTime().domain([minX, maxX]).rangeRound([0, width]);

    var currentDayPixels = daysToPixels(1, x);

    function openLink(link) {
        if (link == null) return;
        if (d3.event.ctrlKey) Machinata.openPage(link);
        else Machinata.goToPage(link, false);
    }

    function openLinkInNewWindow(link) {
        if (link == null) return;
        if (d3.event.button == 1) Machinata.openPage(link);
    }

    // Calculate dimensions

    var paddingH = 4;
    var paddingV = 8;
    var axisHeight = 20;
    var axisTotalHeight = axisHeight * 3;

    var autosize = chart.attr("chart-autosize") == "true";
    if (autosize == true) {
        var targetTrackHeight = 28;
        var targetHeight = axisTotalHeight + (targetTrackHeight * numTracks);
        chart.find("svg").attr("height", targetHeight);
        chart.css("height", targetHeight);
        height = targetHeight;
    }

    var legendWidth = maxTrackNameLen * 6 + 16;

    var tracksHeight = height - axisTotalHeight - paddingV;

    var spanH = (tracksHeight / numTracks) - paddingV;

    var eventX = function (d) {
        return x(d.dateParsed);
    };
    var spanX = function (d) {
        return x(d.startParsed);
    };

    var spanW = function (d) {
        return x(d.endParsed) - x(d.startParsed);
    };

    var spanY = function (d) {
        return d.index * (spanH + paddingV) + axisTotalHeight + paddingV;
    };

    // Background element for grabbing
    svgz.append("rect")
        .attr("fill", "transparent")
        .attr("width", width)
        .attr("height", height);

    // Current week
    var currenWeek = svgz
        .append('g')
        .attr('class', 'axis current-week')
        .append("rect")
        .attr("y", axisHeight)
        .attr("height", height);

    // Create all axises

    var xAxisLabelsDay = d3.axisBottom(x)
        .ticks(d3.timeDay.every(1))
        .tickFormat(function (d) {
            // https://github.com/d3/d3-time-format
            if (currentDayPixels == null || currentDayPixels < 40) {
                return d3.timeFormat('%e')(d);
            } else if (currentDayPixels < 140) {
                return d3.timeFormat('%a %e')(d);
            } else {
                return d3.timeFormat('%a, %B %e')(d);
            }
        }); 

    var xAxisLabelsMonth = d3.axisBottom(x)
        .ticks(d3.timeMonth.every(1))
        .tickFormat(function (d) { return d3.timeFormat('%B %Y')(d); });

    var xAxisGridMonth = d3.axisTop(x)
        .ticks(d3.timeMonth.every(1))
        .tickSize(-height)
        .tickFormat("");

    var xAxisGridWeekend = d3.axisTop(x)
        .ticks(d3.timeSunday.every(1))
        .tickSize(-height)
        .tickFormat("");

    var xAxisGridWeek = d3.axisTop(x)
        .ticks(d3.timeMonday.every(1))
        .tickSize(-axisHeight)
        .tickFormat(function (d) { return d.iso8601CalendarWeek() }); 

    var xAxisGridDay = d3.axisTop(x)
        .ticks(d3.timeDay.every(1))
        .tickSize(-height)
        .tickFormat("");

    var gridWeekend = svgz.append('g').attr('class', 'axis grid-weekend').call(xAxisGridWeekend).attr("transform", "translate(0," + axisHeight * 2 + ")");
    var gridDay = svgz.append('g').attr('class', 'axis grid-day').call(xAxisGridDay).attr("transform", "translate(0," + axisHeight * 2 + ")");
    var gridMonth = svgz.append('g').attr('class', 'axis grid-month').call(xAxisGridMonth);
    var gridWeek = svgz.append('g').attr('class', 'axis grid-week').call(xAxisGridWeek).attr("transform", "translate(0," + axisHeight + ")");
    var labelsDay = svgz.append('g').attr('class', 'axis labels-day').call(xAxisLabelsDay).attr("transform", "translate(0," + axisHeight * 2 + ")");
    var labelsMonth = svgz.append('g').attr('class', 'axis labels-month').call(xAxisLabelsMonth);

    // Main axis lines

    svgz.append("line").attr("class", "line line-1").attr("x1", 0).attr("x2", width).attr("y1", axisHeight).attr("y2", axisHeight);
    svgz.append("line").attr("class", "line line-2").attr("x1", 0).attr("x2", width).attr("y1", axisHeight * 2).attr("y2", axisHeight * 2);


    var updateAxises = function (scale) {
        // Resize axises
        var dw = daysToPixels(1.0, scale);
        var dx = dw / 2.0;
        var ww = dw * 2.0;
        var wdx = (dw * 7.0) / 2.0;
        labelsDay.selectAll("text").attr('transform', 'translate(' + dx + ',0)');
        gridWeek.selectAll("text").attr('transform', 'translate(' + wdx + ',' + (axisHeight - 3) + ')');
        gridWeekend.selectAll("line").attr("stroke-width", ww);
        // Position and size current week
        {
            var d1 = new Date();
            d1.setDate(d1.getDate() - d1.getDay() + 1);
            d1.setHours(0, 0, 0, 0);
            var d2 = new Date();
            d2.setDate(d2.getDate() - d2.getDay() + 1 + 7);
            d2.setHours(0, 0, 0, 0);
            var wx1 = scale(d1);
            var wx2 = scale(d2) + 1;
            currenWeek.attr("width", wx2-wx1).attr("x", wx1);
        }
        
    }

    updateAxises(x);

    // Create events

    var createEvents = function (symbol) {
        if (symbol.events.length == 0) return;
        var track = d3.select(this.parentNode);
        var eventId = symbol.events[0].id;
        var eventClass = "event_" + eventId;
        var radius = spanH / 2 - paddingH;
        var evt = track
            .selectAll('g.' + eventClass)
            .data(symbol.events)
            .enter()
            .append('g')
            .attr("class", "event " + eventClass)
            .attr('cached-y', function (d) {
                return spanY(symbol) + spanH / 2;
            })
            .attr('transform', function (d) {
                var cx = eventX(d);
                var cy = d3.select(this).attr("cached-y");
                return "translate(" + cx + "," + cy + ")";
            });
        evt.append('circle')
            .attr('cx', 0)
            .attr('cy', 0)
            .attr('r', radius)
            .attr('style', function (d) {
                if (d.color != null) return "fill:" + d.color;
                else return null;
            })
            .on('mouseover', function (d) {
                var circleElem = $(this);
                var evtElem = circleElem.parent();
                var siblings = evtElem.siblings(".event");
                var overlappingSiblings = [];
                for (var i = -10; i < 10; i++) {
                    siblings.removeClass("overlap" + i);
                }
                siblings.each(function () {
                    var sibling = $(this);
                    var overlap = Machinata.UI.calculateIfElementsOverlap(sibling, evtElem, true);
                    if (overlap == true) overlappingSiblings.push(sibling);
                });
                for (var i = 0; i < overlappingSiblings.length; i++) {
                    var sibling = overlappingSiblings[i];
                    var ii = i - Math.ceil(overlappingSiblings.length / 2);
                    if (ii > 0) ii++;
                    else if (ii == 0) ii = 1;
                    sibling.addClass("overlap" + ii);
                    console.log("overlap" + ii);
                }
                console.log("siblings: " + siblings.length);
                console.log("overlappingSiblings: " + overlappingSiblings.length);
            })
            .on('mouseout', function (d) {
                var circleElem = $(this);
                var evtElem = circleElem.parent();
                var siblings = evtElem.siblings(".event");
                for (var i = -10; i < 10; i++) {
                    siblings.removeClass("overlap" + i);
                }
            })
            .attr('class', function (d) {
                var classes = "";
                if (d.link != null) classes += "has-link ";
                return classes;
            })
            .on('click', function (d) {
                openLink(d.link);
            })
            .on('mousedown', function (d) {
                openLinkInNewWindow(d.link);
            });
        evt.append('text')
            .attr('dx', radius + paddingH)
            .attr('dy', 4)
            .text(function(d){ return d.title; });
        return evt;
    };

    // Create tracks

    var createTrack = function (symbol) {
        var track = d3.select(this);
        track
            .selectAll('rect')
            .data(symbol.values)
            .enter()
            .append('rect')
            .attr('track-id', function (d) {
                return d.id;
            })
            .attr('x', function (d) {
                return spanX(d);
            })
            .attr('y', function (d) {
                if (d.collisions != null && d.collisions > 0) return spanY(d) + (d.collisionIndex * paddingH);
                else return spanY(d);
            })
            .attr('width', function (d) {
                return spanW(d);
            })
            .attr('height', function (d) {
                if (d.collisions != null && d.collisions > 0) return spanH / (d.collisions + 1) - (paddingH * d.collisions);
                else return spanH;
            })
            .attr('class', function (d) {
                var classes = "";
                if (d.link != null) classes += "has-link ";
                return classes;
            })
            .on('click', function (d) {
                openLink(d.link);
            })
            .on('mousedown', function (d) {
                openLinkInNewWindow(d.link);
            })
            .attr('style', function (d) {
                if (d.color != null) return "fill:" + d.color;
                else return null;
            })
            .each(createEvents);
        return track;
    };
    var allTracks = svgz.selectAll("g.track")
        .data(trackGroups)
        .enter()
        .append("g")
        .attr("class", "track")
        .each(createTrack);

    // Create legends

    var createLegend = function (symbol) {
        var legend = d3.select(this);
        var legendItem = 
            legend.selectAll('text')
                .data(symbol.values)
                .enter();
        legendItem.append('line')
            .attr("class", "line")
            .attr("x1", 0 + 4)
            .attr("y1", function (d) {
                return Math.round(spanY(d) - 4) + 0.5;
            })
            .attr("x2", legendWidth)
            .attr("y2", function (d) {
                return Math.round(spanY(d) - 4)+0.5;
            })
        legendItem.append('text')
            .attr('text-anchor', 'start')
            .attr('x', 0 + 4)
            .attr('y', function (d) {
                return spanY(d) + spanH / 2 + 4;
            })
            .on('click', function (d) {
                openLink(d.link);
            })
            .on('mousedown', function (d) {
                openLinkInNewWindow(d.link);
            })
            .attr('class', function (d) {
                var classes = "";
                if (d.link != null) classes += "has-link ";
                return classes;
            })
            .text(function (d, i) {
                if (i > 0) return null; // only do first text
                return d.title;
            });
            
    };
    svgl.append("rect")
        .attr("class", "back")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", legendWidth)
        .attr("height", height);
    svgl.append("line")
        .attr("class", "line")
        .attr("x1", legendWidth)
        .attr("x2", legendWidth)
        .attr("y1", 0)
        .attr("y2", height);
    var allLegends = svgl.selectAll("g.legend")
        .data(trackGroups)
        .enter()
        .append("g")
        .attr("class", "legend")
        .each(createLegend);

    // Initial zoom state
    if (currentDayPixels < 16) chart.addClass("zoomed-out");
    else chart.removeClass("zoomed-out");
    
    // Figure out zoom scales (this makes the zoom always consistent across different data sets)
    var minZoomPerDay = 1 / 365.0;
    var maxZoomPerDay = 25.0 / 365.0;
    var initialZoomPerDay = 20.0 / 365.0;
    var timeDiff = maxX - minX;
    var timeDiffDays = timeDiff / 1000 / 60 / 60 / 24;
    var minZoom = timeDiffDays * minZoomPerDay; // was 0.2
    var maxZoom = timeDiffDays * maxZoomPerDay; // was 25
    var initialZoom = Machinata.getCache("Machinata.Charts.Timeline.initialZoom");
    if (initialZoom == null) initialZoom = timeDiffDays * initialZoomPerDay;

    // Make zoomable and scrollable
    var lastZoomTransformK = initialZoom;
    var zoom = d3.zoom();
    svgz.call(zoom.scaleExtent([minZoom, maxZoom]).on("zoom", function () {
        // Only zoom on CTRL
        //if (d3.event != null && d3.event.sourceEvent != null && d3.event.sourceEvent.ctrlKey == false) return;

        // Wheel/CTRL pressed?
        var wheelEvent = false;
        var ctrlPressed = false;
        var altPressed = false;
        var shiftPressed = false;
        if (d3.event != null && d3.event.sourceEvent != null && d3.event.sourceEvent.type == "wheel") wheelEvent = true;
        if (d3.event != null && d3.event.sourceEvent != null && d3.event.sourceEvent.ctrlKey == true) ctrlPressed = true;
        if (d3.event != null && d3.event.sourceEvent != null && d3.event.sourceEvent.altKey == true) altPressed = true;
        if (d3.event != null && d3.event.sourceEvent != null && d3.event.sourceEvent.shiftKey == true) shiftPressed = true;

        // Discard a wheel event without CTRL
        if (wheelEvent == true && shiftPressed == false) {
            //console.log("prevent zoom");
            //return;
        }

        // Init
        var transform = d3.event.transform;
        var xRescaled = transform.rescaleX(x);
        currentDayPixels = daysToPixels(1, xRescaled);
        if (currentDayPixels < 16) chart.addClass("zoomed-out");
        else chart.removeClass("zoomed-out");
        // Update axises
        labelsMonth.call(xAxisLabelsMonth.scale(xRescaled));
        labelsDay.call(xAxisLabelsDay.scale(xRescaled));
        gridMonth.call(xAxisGridMonth.scale(xRescaled));
        gridWeekend.call(xAxisGridWeekend.scale(xRescaled));
        gridWeek.call(xAxisGridWeek.scale(xRescaled));
        gridDay.call(xAxisGridDay.scale(xRescaled));
        updateAxises(xRescaled);
        // Update tracks
        allTracks.selectAll('rect').attr('x', function (d) {
            return transform.applyX(spanX(d));
        }).attr('width', function (d) {
            return transform.k * spanW(d);
        });
        allTracks.selectAll('g.event').attr('transform', function (d) {
            var cx = transform.applyX(eventX(d));
            var cy = d3.select(this).attr("cached-y");
            return "translate(" + cx + "," + cy + ")";
        });
        lastZoomTransformK = transform.k;
        Machinata.setCache("Machinata.Charts.Timeline.initialZoom", lastZoomTransformK);
    }));

    // Initial view
    var todayX = -( x( (new Date()).addDays(-5) ) );
    svgz.call(
        zoom.transform,
        d3.zoomIdentity.translate(legendWidth, 0).scale(initialZoom).translate(todayX, 0)
    );
}


