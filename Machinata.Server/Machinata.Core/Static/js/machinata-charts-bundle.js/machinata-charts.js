/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

/* ======== Machinata Charts ======== */

Machinata._chartUID = 0;

Machinata.Charts = {};

Machinata.DEFAULT_CHART_SCHEME = d3.schemeCategory20c;

Machinata.ready(function () {
    // Find all forms that need auto-bind on input-properties
    $("div.chart").each(function () {
        var chart = $(this);
        var chartID = chart.attr("id");
        if (String.isEmpty(chartID)) {
            chartID = "chart_" + Machinata._chartUID++;
            chart.attr("id", chartID);
        }
        var apiCall = chart.attr("api-call");
        var chartType = chart.attr("chart-type");
        chart.addClass(chartType);
        Machinata.debug("Found chart "+chartType);
        // Create D3 Chart
        var svg = d3.select("#" + chartID).append("svg");
        var svgg = svg.append("g");
        // Make zoomable
        if (chart.hasClass("zoomable")) {
            // Bind zoom to the root svg elem
            d3.zoom().on("zoom", function () {
                svgg.attr("transform", d3.event.transform);
            })(svg);
        }
        // Init data to send
        var call = Machinata.apiCall(apiCall);
        var data = {};
        call.data(data);
        // Prepare api call
        call.success(function (message) {
            // Call Function for chart type
            var fn = Machinata.unpackScopedFunctionByName("Machinata." + chartType+"Chart");
            if (typeof fn === 'function') {
                fn(chart, svgg, message.data);
            } else {
                Machinata.warning("Could not initialize chart for ", "Machinata." + chartType + "Chart");
            }
        });
        call.genericError();
        // Send
        call.send();
    });
});


Machinata.setChartMargin = function (chart, margin) {
    var size = { width: chart.width() - margin.left - margin.right, height: chart.height() - margin.top - margin.bottom };
    var svg = chart.find("svg");
    svg.attr("width", size.width + margin.right + margin.left)
       .attr("height", size.height + margin.top + margin.bottom)
       .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    return size;
}

Machinata.getChartScheme = function (chart) {
    var attr = chart.attr("chart-scheme");
    if (String.isEmpty(attr)) return Machinata.DEFAULT_CHART_SCHEME;
    else if (attr == "category10") return d3.schemeCategory10;
    else return Machinata.DEFAULT_CHART_SCHEME;
};

