

/* ======== Machinata Charts Spider ======== */


Machinata.spiderChart = function (chart, svg, data, size, margin) {

    // Via http://bl.ocks.org/nbremer/6506614

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 20 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width - margin.left - margin.right;
    var height = size.height - margin.top - margin.bottom;
    var format = data.format;
    if (format == null) format = ".2f";
    
    // Set data
    var jdata = data["series"];

    // Init helper methods
    var seriesColors = {};
    var defaultColors = d3.scaleOrdinal(Machinata.getChartScheme(chart));
    var getColor = function (key) {
        if (seriesColors[key] != null) return seriesColors[key];
        return defaultColors(key);
    }
    var getFormat = d3.format(format);
    

    // Reverse and reorder so that the incomming data can be clockwise...
    // This is much more logical
    // Also set some defaults
    for (var i = 0; i < jdata.length; i++) {
        // Get series
        var s = jdata[i];
        // Set color and defaults
        seriesColors[s.name] = s.color;
        if (s.title == null) s.title = s.name;
        if (s.name == null) s.name = s.title;
        // Reorder items
        s.items.reverse();
        var last = s.items[s.items.length - 1];
        s.items.pop();
        s.items.splice(0, 0, last);
        // Link each item to series
        for (var ii = 0; ii < s.items.length; ii++) {
            s.items[ii].key = s.name;
        }
    }

    // Defaults
    var cfg = {
        radius: 5,
        w: width,
        h: height,
        factor: 1,
        factorLegend: .85,
        levels: 6,
        maxValue: 0,
        radians: 2 * Math.PI,
        opacityArea: 0.5,
        ToRight: 5,
        color: d3.scaleOrdinal(Machinata.getChartScheme(chart))
    };
    cfg.maxValue = Math.max(cfg.maxValue, d3.max(jdata, function (i) { return d3.max(i.items.map(function (o) { return o.value; })) }));
    var allAxis = (jdata[0].items.map(function (i, j) { return i.name }));
    var total = allAxis.length;
    var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);


    // Prepare main svg elem
    var offsetX = 0;
    var offsetY = 0;
    if (width > height) {
        cfg.w = height;
        cfg.h = height;
        offsetX = width / 2 - cfg.w / 2;
    } else {
        cfg.w = width;
        cfg.h = width;
        offsetY = height / 2 - cfg.h / 2;
    }
    var g = svg.append("g").attr("transform", "translate(" + (margin.left + offsetX) + "," + (margin.top + offsetY) + ")");

    //Circular segments
    for (var j = 0; j < cfg.levels - 1; j++) {
        var levelFactor = cfg.factor * radius * ((j + 1) / cfg.levels);
        g.selectAll(".levels")
         .data(allAxis)
         .enter()
         .append("svg:line")
         .attr("x1", function (d, i) { return levelFactor * (1 - cfg.factor * Math.sin(i * cfg.radians / total)); })
         .attr("y1", function (d, i) { return levelFactor * (1 - cfg.factor * Math.cos(i * cfg.radians / total)); })
         .attr("x2", function (d, i) { return levelFactor * (1 - cfg.factor * Math.sin((i + 1) * cfg.radians / total)); })
         .attr("y2", function (d, i) { return levelFactor * (1 - cfg.factor * Math.cos((i + 1) * cfg.radians / total)); })
         .attr("class", "line spider grid")
         .attr("transform", "translate(" + (cfg.w / 2 - levelFactor) + ", " + (cfg.h / 2 - levelFactor) + ")");
    }

    //Text indicating at what % each level is
    for (var j = 0; j < cfg.levels; j++) {
        var levelFactor = cfg.factor * radius * ((j + 1) / cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function (d) { return levelFactor * (1 - cfg.factor * Math.sin(0)); })
         .attr("y", function (d) { return levelFactor * (1 - cfg.factor * Math.cos(0)); })
         .attr("class", "label")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w / 2 - levelFactor + cfg.ToRight) + ", " + (cfg.h / 2 - levelFactor) + ")")
         //.attr("fill", "#737373")
         //.text(Format((j + 1) * cfg.maxValue / cfg.levels));
         .text(getFormat(((j + 1) / cfg.levels) * cfg.maxValue));
    }

    series = 0;

    var axis = g.selectAll(".axis")
            .data(allAxis)
            .enter()
            .append("g")
            .attr("class", "axis");

    axis.append("line")
        .attr("x1", cfg.w / 2)
        .attr("y1", cfg.h / 2)
        .attr("x2", function (d, i) { return (cfg.w / 2) * (1 - cfg.factor * Math.sin(i * cfg.radians / total)); })
        .attr("y2", function (d, i) { return (cfg.h / 2) * (1 - cfg.factor * Math.cos(i * cfg.radians / total)); })
        .attr("class", "line spider");
        //.style("stroke", "grey")
        //.style("stroke-width", "1px");

    axis.append("text")
        .attr("class", "label")
        .text(function (d) { return d })
        .style("font-family", "sans-serif")
        .style("font-size", "11px")
        .attr("text-anchor", "middle")
        .attr("dy", "1.5em")
        .attr("transform", function (d, i) { return "translate(0, -10)" })
        .attr("x", function (d, i) { return cfg.w / 2 * (1 - cfg.factorLegend * Math.sin(i * cfg.radians / total)) - 60 * Math.sin(i * cfg.radians / total); })
        .attr("y", function (d, i) { return cfg.h / 2 * (1 - Math.cos(i * cfg.radians / total)) - 20 * Math.cos(i * cfg.radians / total); });


    jdata.forEach(function (y, x) {
        dataValues = [];
        g.selectAll(".nodes")
          .data(y.items, function (j, i) {
              dataValues.push([
                cfg.w / 2 * (1 - (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.factor * Math.sin(i * cfg.radians / total)),
                cfg.h / 2 * (1 - (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.factor * Math.cos(i * cfg.radians / total))
              ]);
          });
        dataValues.push(dataValues[0]);
        g.selectAll(".area")
                       .data([dataValues])
                       .enter()
                       .append("polygon")
                       .attr("class", "radar-chart-serie" + series)
                       .style("stroke-width", "2px")
                       .style("stroke", getColor(y.name))
                       .attr("points", function (d) {
                           var str = "";
                           for (var pti = 0; pti < d.length; pti++) {
                               str = str + d[pti][0] + "," + d[pti][1] + " ";
                           }
                           return str;
                       })
                       .style("fill", function (j, i) { return cfg.color(series) })
                       .style("fill-opacity", cfg.opacityArea)
                       .on('mouseover', function (d) {
                           z = "polygon." + d3.select(this).attr("class");
                           g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                           g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                       })
                       .on('mouseout', function () {
                           g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);
                       });
        series++;
    });
    series = 0;


    jdata.forEach(function (y, x) {
        g.selectAll(".nodes")
          .data(y.items).enter()
          .append("svg:circle")
          .attr("class", "radar-chart-" + series + " show-value-on-hover")
          .attr('r', cfg.radius)
          //.attr("alt", function (j) { return Math.max(j.value, 0) })
          .attr("cx", function (j, i) {
              dataValues.push([
                cfg.w / 2 * (1 - (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.factor * Math.sin(i * cfg.radians / total)),
                cfg.h / 2 * (1 - (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.factor * Math.cos(i * cfg.radians / total))
              ]);
              return cfg.w / 2 * (1 - (Math.max(j.value, 0) / cfg.maxValue) * cfg.factor * Math.sin(i * cfg.radians / total));
          })
          .attr("cy", function (j, i) {
              return cfg.h / 2 * (1 - (Math.max(j.value, 0) / cfg.maxValue) * cfg.factor * Math.cos(i * cfg.radians / total));
          })
          .attr("data-id", function (j) { return j.name })
          .style("fill", cfg.color(series)).style("fill-opacity", .9)
          .on('mouseover', function (d) {
              /*newX = parseFloat(d3.select(this).attr('cx')) - 10;
              newY = parseFloat(d3.select(this).attr('cy')) - 5;

              tooltip
                  .attr('x', newX)
                  .attr('y', newY)
                  .text(Format(d.value))
                  .transition(200)
                  .style('opacity', 1);*/

              z = "polygon." + d3.select(this).attr("class");
              g.selectAll("polygon")
                  .transition(200)
                  .style("fill-opacity", 0.1);
              g.selectAll(z)
                  .transition(200)
                  .style("fill-opacity", .7);
          })
          .on('mouseout', function () {
              /*tooltip
                  .transition(200)
                  .style('opacity', 0);*/
              g.selectAll("polygon")
                  .transition(200)
                  .style("fill-opacity", cfg.opacityArea);
          })
          .append("svg:title")
          .text(function (j) { return Math.max(j.value, 0) });

        series++;
    });


    // Create legend
    var legend = g.append("g")
        .attr("class", "legend")
        .attr("transform", "translate(" + (-offsetX) + "," + (-offsetY) + ")")
        .attr("text-anchor", "end")
        .selectAll("g")
        .data(jdata)
        //.data(jdata.slice().reverse())
        .enter().append("g")
        .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; });
    legend.append("rect")
        .attr("x", width - 19)
        .attr("width", 19)
        .attr("height", 19)
        .attr("fill", function (d) { return getColor(d.name); });
    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9.5)
        .attr("dy", "0.32em")
        .text(function (d) { return d.title; });

    bindTooltip(g.selectAll(".show-value-on-hover"));

    function bindTooltip(selection) {
        // Create tooltop and it's background
        var tooltipBackground = g.append('rect')
                .attr('class', 'tooltip-background')
                .style("position", "absolute")
                .attr("display", "none")
                .style("pointer-events", "none");
        var tooltip = g.append('text')
                .attr('class', 'tooltip')
                .style("position", "absolute")
                .attr("display", "none")
                .style("pointer-events", "none")
                .html('<tspan class="title" x="0" dy="-0.2em"></tspan><tspan class="subtitle" x="0" dy="+1em"></tspan>')
                .style('alignment-baseline', 'central')
                .style('text-anchor', 'middle');
        // Bind events
        selection.on('mouseenter', function (data) {
            // Update text
            tooltip.selectAll("tspan.title").text(data.key);
            tooltip.selectAll("tspan.subtitle").text(data.name + ": " + getFormat(data.value));
            // Update UI
            tooltip.attr("display", "inline");
            tooltip.each(function () {
                var bbox = this.getBBox();
                tooltipBackground
                    .attr("width", bbox.width + 12)
                    .attr("height", bbox.height + 6);
            });
            tooltipBackground.attr("display", "inline");
        });
        selection.on('mousemove', function (data) {
            var coordinates = d3.mouse(g.node());
            var tooltipOffsetY = -22;
            tooltip.attr("transform", "translate(" + coordinates[0] + "," + (coordinates[1] + tooltipOffsetY) + ")");
            tooltipBackground.attr("transform", "translate(" + (coordinates[0] - tooltipBackground.attr("width") / 2) + "," + ((coordinates[1] + tooltipOffsetY) - tooltipBackground.attr("height") / 2 - 0) + ")");
        });
        selection.on('mouseout', function () {
            tooltip.attr("display", "none");
            tooltipBackground.attr("display", "none");
        });
    }
};

