

/* ======== Machinata Charts Graph ======== */

Machinata.graphChart = function (chart, svg, data, size, margin) {

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 80 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width;
    var height = size.height;

    // Init property methods
    var colors = d3.scaleOrdinal(Machinata.getChartScheme(chart));
    var radius = function (d) { return d.star == true ? 15 : 5; }

    // Init simulation
    var linkForce = d3.forceLink().id(function (d) { return d.id; });
    var chargeForce = d3.forceManyBody();
    var forceCenter = d3.forceCenter(width / 2, height / 2);
    var simulation = d3.forceSimulation()
        .force("link", linkForce)
        .force("charge", chargeForce)
        .force("center", forceCenter);

    // Create links
    var link = svg.append("g")
        .attr("class", "link")
        .selectAll("line")
        .data(data.links)
        .enter().append("line");

    // Create nodes
    var node = svg.append("g")
        .attr("class", "nodes")
        .selectAll("circle")
        .data(data.nodes)
        .enter().append("circle")
        .attr("r", radius)
        .attr("fill", function (d) { return d.star == true ? colors(1) : colors(0); })
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended)
         );

    // Set node titles
    node.append("title").text(function (d) { return d.title; });

    // Create labels
    var label = svg.append("g")
        .attr("class", "labels")
        .selectAll("text")
        .data(data.nodes)
        .enter().append("g")
        .attr("class", "label");
    // Set label text
    label
        .append("text")
        .text(function (d) { return d.title; })
        .attr("dy", "0.4em")
        .attr("dx", function (d) { return radius(d) + 5; });

    // Bind simulation
    simulation
        .nodes(data.nodes)
        .on("tick", onTick);
    simulation.force("link")
        .links(data.links);

    function onTick() {
        // Update positions
        link.attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });
        node.attr("cx", function (d) { return d.x; })
            .attr("cy", function (d) { return d.y; });
        label.attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });
    }

    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

}