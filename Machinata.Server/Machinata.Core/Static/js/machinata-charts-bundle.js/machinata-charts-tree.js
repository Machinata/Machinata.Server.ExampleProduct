

/* ======== Machinata Charts Tree ======== */

Machinata.treeChart = function (chart, svg, data, size, margin) {

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 80 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width;
    var height = size.height;

    // Init
    var i = 0;
    var duration = 750;
    var root;


    // declares a tree layout and assigns the size
    var treemap = d3.tree().size([height, width]);

    // Assigns parent, children, height, depth
    root = d3.hierarchy(data, function (d) { return d.children; });
    root.x0 = height / 2;
    root.y0 = 0;

    // Mark root as loaded
    root.loaded = true;

    // Collapse after the second level
    root.children.forEach(collapse);
    update(root);

    // Collapse the node and all it's children
    function collapse(d) {
        if (d.children) {
            d._children = d.children
            d._children.forEach(collapse)
            d.children = null
        }
    }

    function update(source) {

        // Assigns the x and y position for the nodes
        var treeData = treemap(root);

        // Compute the new tree layout.
        var nodes = treeData.descendants(),
            links = treeData.descendants().slice(1);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) { d.y = d.depth * 180 });

        // ****************** Nodes section ***************************

        // Update the nodes...
        var node = svg.selectAll('g.node')
            .data(nodes, function (d) { return d.id || (d.id = ++i); });

        // Enter any new modes at the parent's previous position.
        var nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr("transform", function (d) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            });

        // Add Circle for the nodes
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', 1e-6)
            //.style("fill", function (d) {
            //    return d._children ? "lightsteelblue" : "#fff";
            //});
            .on('click', expandChildren);

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr("dy", ".35em")
            .attr("x", function (d) {
                return d.children || d._children ? -13 : 13;
            })
            .attr("text-anchor", function (d) {
                return d.children || d._children ? "end" : "start";
            })
            .attr("class", function (d) {
                var classes = "";
                if(d.data.link != null) classes += "has-link ";
                return classes;
            })
            .text(function (d) { return d.data.name; })
            .on('click', showLinkOrInfos);


        // UPDATE
        var nodeUpdate = nodeEnter.merge(node);

        // Transition to the proper position for the node
        nodeUpdate.transition()
          .duration(duration)
          .attr("transform", function (d) {
              return "translate(" + d.y + "," + d.x + ")";
          });
          

        // Update the node attributes and style
        nodeUpdate.select('circle.node')
          .attr('r', 10)
          //.style("fill", function (d) {
          //    return d._children ? "lightsteelblue" : "#fff";
          //})
          .attr('cursor', 'pointer');


        // Remove any exiting nodes
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")";
            }).style("opacity", function (d) {
                return d._children ? "1.0" : "0.0";
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
          .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
          .style('fill-opacity', 1e-6);

        // ****************** links section ***************************

        // Update the links...
        var link = svg.selectAll('path.link')
            .data(links, function (d) { return d.id; });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert('path', "g")
            .attr("class", "link")
            .attr('d', function (d) {
                var o = { x: source.x0, y: source.y0 }
                return diagonal(o, o)
            });

        // UPDATE
        var linkUpdate = linkEnter.merge(link);

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(duration)
            .attr('d', function (d) { return diagonal(d, d.parent) });

        // Remove any exiting links
        var linkExit = link.exit().transition()
            .duration(duration)
            .attr('d', function (d) {
                var o = { x: source.x, y: source.y }
                return diagonal(o, o)
            }).style("opacity", function (d) {
                return d._children ? "1.0" : "0.0";
            })
            .remove();

        // Store the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

        // Creates a curved (diagonal) path from parent to the child nodes
        function diagonal(s, d) {

            //path = `M ${s.y} ${s.x} C ${(s.y + d.y) / 2} ${s.x}, ${(s.y + d.y) / 2} ${d.x}, ${d.y} ${d.x}`;
            path = "M "+s.y+" "+s.x+" C "+((s.y + d.y) / 2)+" "+s.x+", "+((s.y + d.y) / 2)+" "+d.x+", "+d.y+" "+d.x;

            return path;
        }
        
        // Show link or infos
        function showLinkOrInfos(d) {
            if (d.data.link != null && d.data.link != "") {
                window.open(d.data.link, '_blank');
            }
        }

        // Toggle children on click.
        function expandChildren(d) {
            if (d.children) {
                // Hide
                d._children = d.children;
                d.children = null;
                update(d);
            } else {
                // Show
                if (d.loaded == true) {
                    // We have already loaded, so we can just expand
                    d.children = d._children;
                    d._children = null;
                    update(d);
                } else {
                    // We need to get the data
                    var apiCall = d.data["children-call"];
                    var call = Machinata.apiCall(apiCall);
                    call.success(function (message) {
                        // Set children and mark as loaded
                        d._children = [];
                        var newNodesData = message.data.children;
                        for (var i = 0; i < newNodesData.length; i++) {
                            var newNodeData = newNodesData[i];
                            var newNode = d3.hierarchy(newNodeData);
                            newNode.depth = d.depth + 1;
                            newNode.height = d.height - 1;
                            newNode.parent = d;
                            //newNode.id = Date.now();
                            d._children.push(newNode);
                        }
                        if (newNodesData.length == 0) d._children = null;
                        d.loaded = true;
                        // Show children
                        d.children = d._children;
                        d._children = null;
                        update(d);
                    });
                    call.genericError().send();
                }
            }
        }
    }
}