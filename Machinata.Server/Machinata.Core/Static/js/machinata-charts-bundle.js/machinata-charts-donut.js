

/* ======== Machinata Charts Tree ======== */

Machinata.donutChart = function (chart, svg, data, size, margin) {

    // Original from https://bl.ocks.org/mbhall88/b2504f8f3e384de4ff2b9dfa60f325e2

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 80 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width;
    var height = size.height;
    var radius = Math.min(width, height) / 2;
    console.log("radius=" + radius);

    //var selection = svg;

    //alert(JSON.stringify(data.items));

    var colour = d3.scaleOrdinal(d3.schemeCategory20c), // colour scheme
        variable = 'value', // value in data that will dictate proportions on chart
        category = 'name', // compare data by
        padAngle = 0.015, // effectively dictates the gap between slices
        floatFormat = d3.format('.4r'),
        cornerRadius = 0, // sets how rounded the corners are on each slice
        percentFormat = d3.format(',.0%');


    // creates a new pie generator
    var pie = d3.pie()
        .value(function (d) { return floatFormat(d[variable]); })
        .sort(null);

    // contructs and arc generator. This will be used for the donut. The difference between outer and inner
    // radius will dictate the thickness of the donut
    var arc = d3.arc()
        .outerRadius(radius * 0.8)
        .innerRadius(radius * 0.6)
        .cornerRadius(cornerRadius)
        .padAngle(padAngle);

    // this arc is used for aligning the text labels
    var outerArc = d3.arc()
        .outerRadius(radius * 0.9)
        .innerRadius(radius * 0.9);
    // ===========================================================================================

    // ===========================================================================================
    // append the svg object to the selection
    //var svg = selection.append('svg')
   //     .attr('width', width + margin.left + margin.right)
    //    .attr('height', height + margin.top + margin.bottom)
    //  .append('g')
        svg.attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
    // ===========================================================================================

    // ===========================================================================================
    // g elements to keep elements within svg modular
    svg.append('g').attr('class', 'slices');
    svg.append('g').attr('class', 'labelName');
    svg.append('g').attr('class', 'lines');
    // ===========================================================================================

    // ===========================================================================================
    // add and colour the donut slices
    var path = svg.select('.slices')
        //.datum(data.items).selectAll('path')
        .selectAll('path')
        .data(pie(data.items))
      .enter().append('path')
        .attr('fill', function (d) { return colour(d.data[category]); })
        .attr('d', arc);
    // ===========================================================================================

    // ===========================================================================================
    // add text labels
    var label = svg.select('.labelName').selectAll('text')
        .data(pie(data.items))
      .enter().append('text')
        .attr('dy', '.35em')
        .text(function (d) {
            // add "key: value" for given category. Number inside tspan is bolded in stylesheet.
            //return d.data[category] + ': <tspan>' + percentFormat(d.data[variable]) + '</tspan>';
            return d.data[category];
        })
        .attr('transform', function (d) {

            // effectively computes the centre of the slice.
            // see https://github.com/d3/d3-shape/blob/master/README.md#arc_centroid
            var pos = outerArc.centroid(d);

            // changes the point to be on left or right depending on where label is.
            pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
            return 'translate(' + pos + ')';
        })
        .style('text-anchor', function (d) {
            // if slice centre is on the left, anchor text to start, otherwise anchor to end
            return (midAngle(d)) < Math.PI ? 'start' : 'end';
        });
    // ===========================================================================================

    // ===========================================================================================
    // add lines connecting labels to slice. A polyline creates straight lines connecting several points
    var polyline = svg.select('.lines')
        .selectAll('polyline')
        .data(pie(data.items))
      .enter().append('polyline')
        .attr('points', function (d) {

            // see label transform function for explanations of these three lines.
            var pos = outerArc.centroid(d);
            pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
            return [arc.centroid(d), outerArc.centroid(d), pos]
        });
    // ===========================================================================================

    // ===========================================================================================
    // add tooltip to mouse events on slices and labels
    svg.selectAll('.labelName text, .slices path').call(toolTip);
    // ===========================================================================================

    // ===========================================================================================
    // Functions

    // calculates the angle for the middle of a slice
    function midAngle(d) { return d.startAngle + (d.endAngle - d.startAngle) / 2; }

    // function that creates and adds the tool tip to a selected element
    function toolTip(selection) {

        var tooltip = svg.append('text')
                .attr('class', 'toolCircle')
                .attr('dy', -5) // hard-coded. can adjust this to adjust text vertical alignment in tooltip
                .html('<tspan class="title" x="0" dy="5">' + data.title + '</tspan>') // add text to the circle.
                .style('text-anchor', 'middle'); // centres text in tooltip

        // add tooltip (svg circle element) when mouse enters label or slice
        selection.on('mouseenter', function (data) {
            tooltip.html(toolTipHTML(data));
        });

        // remove the tooltip when mouse leaves the slice/label
        selection.on('mouseout', function () {
            tooltip.html('<tspan class="title" x="0" dy="5">' + data.title + '</tspan>');
        });
    }

    // function to create the HTML string for the tool tip. Loops through each key in data object
    // and returns the html string key: value
    function toolTipHTML(data) {
        var tip = "";
        tip += '<tspan class="title" x="0">' + data.data.title + '</tspan>';
        tip += '<tspan class="meta" x="0" dy="1.6em">' + data.data.value + ', ' + percentFormat(data.data.percent) + '</tspan>';
        return tip;
        /*
        var tip = '',
            i = 0;

        for (var key in data.data) {

            // if value is a number, format it as a percentage
            //var value = (!isNaN(parseFloat(data.data[key]))) ? percentFormat(data.data[key]) : data.data[key];
            var value = data.data[key];

            // leave off 'dy' attr for first tspan so the 'dy' attr on text element works. The 'dy' attr on
            // tspan effectively imitates a line break.
            if (i === 0) tip += '<tspan x="0">' + key + ': ' + value + '</tspan>';
            else tip += '<tspan x="0" dy="1.2em">' + key + ': ' + value + '</tspan>';
            i++;
        }

        return tip;*/
    }
}


