

/* ======== Machinata Charts Map ======== */

Machinata.mapChart = function (chart, svg, data, size, margin) {

    // Set geomet
    var margin = { top: 0, right: 0, bottom: 0, left: 0 };
    var size = Machinata.setChartMargin(chart, margin); 
    var width = size.width;
    var height = size.height;
    
    // We don't need a svg, so we replace it...
    var map = $("<div></div>");
    chart.empty();
    chart.append(map);
    map.css("height", "auto");
    chart.css("height", "auto");

    // Init the map and add all markers from data
    map.addClass("bb-map").addClass("initialized").each(function () {
        var self = $(this);
        // Init id
        Machinata.Maps._mapUIDs++;
        self.attr("id", "map_" + Machinata.Maps._mapUIDs);
        //Machinata.echo(self.attr("id"));
        // Init tiles
        var tiles = Machinata.Maps.getTileLayer();
        // Init options
        var opts = Machinata.Maps.DEFAULT_MAPS_OPTIONS;
        opts["layers"] = [tiles];
        // Init map
        var map = L.map(self.attr("id"), opts);
        //map.setView([0, 0], 1);
        // Add markers
        if (data.markers != null && data.markers.length > 0) {
            Machinata.maps.addMarkersToMap(map, data.markers, data["use-cluster"], data["jitter"]);
        }
        if (data.heat != null && data.heat.length > 0) {
            Machinata.maps.addHeatToMap(map, data.heat);
        }
    });
}


