

/* ======== Machinata Charts Tree ======== */

Machinata.pieChart = function (chart, svg, data, size, margin) {
    alert("WORK IN PROGRESS!");

    // Set geometry
    var margin = { top: 20, right: 20, bottom: 20, left: 80 };
    var size = Machinata.setChartMargin(chart, margin);
    var width = size.width;
    var height = size.height;
    var radius = Math.min(width, height) / 2;
    console.log("radius="+radius);


    var color = d3.scaleOrdinal()
    .range(["{solid-color}", "{highlight-color}"]);

    var arc = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var labelArc = d3.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 40);

    var pie = d3.pie()
        .sort(null)
        .value(function (d) { return d.value; });

    /*
    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height)
      .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");*/

    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g = svg.selectAll(".arc")
        .data(pie(data.items), function (d) { return d.data.title })
      .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", arc)
        .style("fill", function (d) { return color(d.data); });

    g.append("text")
        .attr("transform", function (d) { return "translate(" + labelArc.centroid(d) + ")"; })
        .attr("dy", ".35em")
        .text(function (d) { return d.data.title + ": "+d.data.value; });
}