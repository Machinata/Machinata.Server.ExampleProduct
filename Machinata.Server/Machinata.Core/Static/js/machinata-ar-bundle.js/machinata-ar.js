/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata AR JS Library
/// </summary>
/// <type>namespace</type>
Machinata.AR = {};

/// <summary>
/// 
/// </summary>
Machinata.AR.MODE_AR = "ar";

/// <summary>
/// 
/// </summary>
Machinata.AR.MODE_PREVIEW = "preview";

/// <summary>
/// 
/// </summary>
Machinata.AR.MODE_ERROR = "error";

/// <summary>
/// 
/// </summary>
Machinata.AR.mode = Machinata.AR.MODE_AR;

/// <summary>
/// 
/// </summary>
Machinata.AR.debug = false;

/// <summary>
/// 
/// </summary>
Machinata.AR.noSceneCache = false;

/// <summary>
/// 
/// </summary>
Machinata.AR.debugCompatibility = false;

/// <summary>
/// 
/// </summary>
Machinata.AR.enableStatistics = true;

/// <summary>
/// 
/// </summary>
Machinata.AR.enableLoadingFailMonitor = false;

/// <summary>
/// 
/// </summary>
Machinata.AR.lazyLoadARJS = true;

/// <summary>
/// 
/// </summary>
Machinata.AR.sceneFile = "/static/file/ar/scene.htm";

/// <summary>
/// 
/// </summary>
Machinata.AR.markerFile = "/static/file/ar/marker.patt";

/// <summary>
/// 
/// </summary>
Machinata.AR.aframeARJSLib = "/static/file/js/machinata-ar-bundle.js/aframe-ar.min.js";

/// <summary>
/// 
/// </summary>
Machinata.AR.loadDelay = 1000;

/// <summary>
/// 
/// </summary>
Machinata.AR.elems = {};

/// <summary>
/// 
/// </summary>
Machinata.AR.elems.scene = null;

/// <summary>
/// 
/// </summary>
Machinata.AR.elems.marker = null;

/// <summary>
/// 
/// </summary>
Machinata.AR.errors = [];

/// <summary>
/// 
/// </summary>
Machinata.AR.errorsDescription = null;

/// <summary>
/// 
/// </summary>
Machinata.AR.loadScript = function(src,callback) {
    var USE_JQUERY = false;
    console.log("Loading "+src);
    if(USE_JQUERY == true) {
        $.getScript(src, function(data, textStatus, jqxhr) {
            console.log("Script "+src+" loaded");
            callback();
        });
    } else {
        var script = document.createElement('script');
        script.onload = function () {
            console.log("Script "+src+" loaded");
            callback();
        };
        script.src = src;
        document.head.appendChild(script);
    }
};


/// <summary>
/// 
/// </summary>
Machinata.AR.queryParameter = function (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null) return "";
    else return decodeURIComponent(results[1].replace(/\+/g, " "));
};

/// <summary>
/// 
/// </summary>
Machinata.AR.getIOSVersion = function(){
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
    return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
};

/// <summary>
/// 
/// </summary>
Machinata.AR.getAndroidVersion = function(){
    var ua = (ua || navigator.userAgent).toLowerCase(); 
    var match = ua.match(/android\s([0-9\.]*)/);
    return match ? match[1] : null;
};

/// <summary>
/// 
/// </summary>
Machinata.AR.getAndroidMajorVersion = function(){
    return parseInt(Machinata.AR.getAndroidVersion(),10);
};

/// <summary>
/// 
/// </summary>
Machinata.AR.installMediaDevicesAPI = function() {
    // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    // Older browsers might not implement mediaDevices at all, so we set an empty object first
    if (navigator.mediaDevices === undefined) {
      navigator.mediaDevices = {};
    }

    // Some browsers partially implement mediaDevices. We can't just assign an object
    // with getUserMedia as it would overwrite existing properties.
    // Here, we will just add the getUserMedia property if it's missing.
    if (navigator.mediaDevices.getUserMedia === undefined) {
      navigator.mediaDevices.getUserMedia = function(constraints) {

        // First get ahold of the legacy getUserMedia, if present
        var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;


        // Some browsers just don't implement it - return a rejected promise with an error
        // to keep a consistent interface
        if (!getUserMedia) {
          return Promise.reject(new Error('navigator.mediaDevices.getUserMedia is not implemented in this browser'));
        }

        // Make sure the full set is supported
        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
          return Promise.reject(new Error('navigator.mediaDevices.enumerateDevices is not implemented in this browser'));
        }

        // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
        return new Promise(function(resolve, reject) {
          getUserMedia.call(navigator, constraints, resolve, reject);
        });
      }
    }
}

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.AR._onMarkerDetect = [];

/// <summary>
/// 
/// </summary>
Machinata.AR.onMarkerDetect = function (fn) { // Machinata.AR.onMarkerDetect(function(visible,marker){});
    Machinata.AR._onMarkerDetect.push(fn);
};


/// <summary>
/// 
/// </summary>
Machinata.AR.init = function(callback) {


    // Set loading
    $("body").addClass("loading");

    // Register load component
    AFRAME.registerComponent('onload', {
      schema: {type: 'string'},
      init: function () {
        $("body").removeClass("loading");
        $("body").addClass("loaded");
      }
    });

    // Load query params
    if(Machinata.AR.queryParameter("debug") == "true") Machinata.AR.debug = true;

    // Force HTTPS
    if(Machinata.AR.debug != true) {
        if (location.protocol != 'https:') {
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
            return;
        }
    }

    // Catch all errors
    if(Machinata.AR.debug == true) {
        window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
            alert("Error occured: " + errorMsg); 
            return false;
        }
    }

    // Install the media devices backwards compatability
    Machinata.AR.installMediaDevicesAPI();

    // Force preview?
    if(Machinata.AR.queryParameter("mode") != "" && Machinata.AR.queryParameter("mode") != null) {
        console.log("Machinata.AR.init mode override");
        Machinata.AR.initForMode(Machinata.AR.queryParameter("mode"),callback);
        return;
    }

    setTimeout(function(){



        // Supported device?
        var deviceSupported = false;
        var deviceAgent = navigator.userAgent.toLowerCase();
        var isIOS = false;
        var isIOS11 = false;
        var isAndroid = false;
        if(deviceAgent != null) {
            if(deviceAgent.indexOf("iphone") > -1) isIOS = true;
            if(deviceAgent.indexOf("ipod") > -1) isIOS = true;
            if(deviceAgent.indexOf("ipad") > -1) isIOS = true;
            isAndroid = deviceAgent.indexOf("android") > -1;
        }
        if(Machinata.AR.debugCompatibility == true) alert("deviceAgent: "+deviceAgent);
        if(Machinata.AR.debugCompatibility == true) alert("isIOS: "+isIOS);
        if(Machinata.AR.debugCompatibility == true) alert("isAndroid: "+isAndroid);
        if(isIOS == true) {
            // Version 11?
            if(Machinata.AR.getIOSVersion()[0] >= 11) {
                // All good (unless not using the native safari browser)
                isIOS11 = true;
            } else {
                Machinata.AR.errors.push("Sorry, augmented reality requires iOS 11 to work correctly.");
                Machinata.AR.initForMode(Machinata.AR.MODE_ERROR,callback);
                return;
            }
        }
        if(isAndroid == true) {
            if(Machinata.AR.debugCompatibility == true) alert(Machinata.AR.getAndroidVersion());
            if(Machinata.AR.debugCompatibility == true) alert(Machinata.AR.getAndroidMajorVersion());
            var androidMajorVersion = Machinata.AR.getAndroidMajorVersion();
        }

        // Webcam available?

        // Newer API
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            if(Machinata.AR.debugCompatibility == true) alert("newer api navigator.mediaDevices supported");
            navigator.mediaDevices.getUserMedia({ video: true })
            .then(function(mediaStream) {
                // All good
                // Register monitor for loading fail
                if(Machinata.AR.enableLoadingFailMonitor == true) {
                    setTimeout(function(){
                        if($("body").hasClass("loading")) {
                            Machinata.AR.errors.push("The camera could not be loaded on your device.");
                            if(isIOS11 == true) Machinata.AR.errors.push("If you have iOS 11 installed, please try using the standard Apple Safari browser.");
                            if(isAndroid == true) Machinata.AR.errors.push("If you have Android installed, please try using the Chrome browser.");
                            Machinata.AR.initForMode(Machinata.AR.MODE_ERROR,callback);
                        }
                    },10000);
                }
                // Webcam available
                Machinata.AR.initForMode(Machinata.AR.MODE_AR,callback);
                return;
            })
            ["catch"](function (err2) {
                console.log("navigator.mediaDevices error:" + err2.name + ": " + err2.message);
                if (Machinata.AR.debug == true) alert("navigator.mediaDevices error:" + err2.name + ": " + err2.message);
                if(err2.name == "NotAllowedError" || err2.name == "SecurityError") {
                    Machinata.AR.errors.push("Please allow access to your camera to continue.");
                } else {
                    Machinata.AR.errors.push("A camera could not be found on your device, or you did not allow access to it. Please allow access to your camera to continue.");
                    if(isIOS11 == true) Machinata.AR.errors.push("If you have iOS 11 installed, please try using the standard Apple Safari browser.");
                    if(isAndroid == true) Machinata.AR.errors.push("If you have Android installed, please try using the Chrome browser.");
                }
                Machinata.AR.initForMode(Machinata.AR.MODE_ERROR,callback);
                return;
            }); 
            return;
        }

        /*
        // Older API (deprecated)
        // https://developer.mozilla.org/en-US/docs/Web/API/Navigator/getUserMedia
        navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                               navigator.webkitGetUserMedia ||
                               navigator.mozGetUserMedia ||
                               navigator.msGetUserMedia);
        if(navigator.getMedia) {
            if(Machinata.AR.debugCompatibility == true) alert("older api navigator.getMedia supported");
            alert("older api navigator.getMedia supported");
            navigator.getMedia({video: true}, function() {
                // Webcam available
                Machinata.AR.initForMode(Machinata.AR.MODE_AR,callback);
                return;
            }, function() {
                // No webcam!
                Machinata.AR.errors.push("A camera could not be found on your device, or you did not allow access to it. Please allow access to your camera to continue.");
                Machinata.AR.initForMode(Machinata.AR.MODE_ERROR,callback);
                return;
            });
        } else {
            // No webcam
            Machinata.AR.errors.push("Your browser does not support the camera interface, which is needed for augmented reality.");
            if(isIOS11 == true) Machinata.AR.errors.push("If you have iOS 11 installed, please try using the standard Apple Safari browser.");
            if(isAndroid == true) Machinata.AR.errors.push("If you have Android installed, please try using the Chrome browser.");
            Machinata.AR.initForMode(Machinata.AR.MODE_ERROR,callback);
            return;
        }*/
    

    },Machinata.AR.loadDelay);
    
};

/// <summary>
/// 
/// </summary>
Machinata.AR.initForMode = function(mode,callback) {

    // Register
    Machinata.AR.mode = mode;
    var appendToSceneElem = null;
    console.log("Machinata.AR.initForMode("+Machinata.AR.mode+")");

    // Build error
    Machinata.AR.errorsDescription = "";
    for(var i = 0; i < Machinata.AR.errors.length; i++) {
        if(Machinata.AR.errorsDescription != "") Machinata.AR.errorsDescription += " ";
        Machinata.AR.errorsDescription += Machinata.AR.errors[i];
    }

    // Statistics
    if(Machinata.AR.enableStatistics == true && Machinata.AR.debug == false) {
        try {
            var eventCategory = "MachinataAR_";
            var eventAction = "Mode: "+Machinata.AR.mode;
            if(Machinata.AR.mode == Machinata.AR.MODE_ERROR) {
                eventCategory += "Error";
                eventAction = Machinata.AR.errorsDescription;
            } else {
                eventCategory += "Success";
            }
            var eventLabel = navigator.userAgent;
            ga('send', 'event', eventCategory, eventAction, eventLabel);
        } catch(e) {
            console.log("Could not track statistics");
        }
    }

    if(Machinata.AR.mode == Machinata.AR.MODE_PREVIEW) {
        
        // Create scene
        Machinata.AR.elems.scene = $("<a-scene></a-scene>");
        appendToSceneElem = Machinata.AR.elems.scene;
        // Add camera
        var camera = $("<a-entity position=\"0 3 6\" data-aframe-default-camera camera wasd-controls rotation look-controls visible></a-entity>");
        Machinata.AR.elems.scene.append(camera);
    
    } else if(Machinata.AR.mode == Machinata.AR.MODE_AR) {
        
        // Create scene
        Machinata.AR.elems.scene = $("<a-scene embedded arjs></a-scene>");
        // Add camera
        var camera = $("<a-entity camera></a-entity>");
        Machinata.AR.elems.scene.append(camera);
        // Add marker
        if (Machinata.AR.markerFile != null) {
            var marker = $("<a-marker id=\"marker\" preset=\"file\" url=\"" + Machinata.AR.markerFile + "\"></a-marker>");
            appendToSceneElem = marker;
            Machinata.AR.elems.scene.append(marker);
        } else {
            // Create scene
            appendToSceneElem = Machinata.AR.elems.scene;
        }
    
    } else if(Machinata.AR.mode == Machinata.AR.MODE_ERROR) {
        
        $("body").removeClass("loading");
        $("body").addClass("loaded");
        $("body").addClass("error");
        if(callback) callback();
        return;

    }

    // Load scene
    var sceneVer = 1;
    if (Machinata.AR.debug == true || Machinata.AR.noSceneCache == true) sceneVer = (new Date()).getTime();
    $.get( Machinata.AR.sceneFile+"?v="+sceneVer, function( data ) {
        var loadedScene = $(data);
        loadedScene.find("#assets").appendTo(Machinata.AR.elems.scene);
        loadedScene.find("#scene").appendTo(appendToSceneElem);
        

        var displayScene = function () {
            // Add to body
            $("body").append(Machinata.AR.elems.scene);

            // Find marker
            Machinata.AR.elems.marker = Machinata.AR.elems.scene.find("#marker,.marker");

            // Call callback
            if (callback) callback();

            // Register monitor for marker
            if (Machinata.AR.elems.marker.length > 0) {
                var lastVisibleMarkers = [];
                setInterval(function () {
                    // Don't process if loading...
                    if ($("body").hasClass("loading")) return;
                    // Init
                    var anyVisible = false;
                    var changeDetected = false;
                    for (var i = 0; i < Machinata.AR.elems.marker.length; i++) {
                        // Get marker information
                        var markerElem = Machinata.AR.elems.marker.eq(i);
                        var lastState = markerElem.data("marker-visible");
                        if (lastState == null) lastState = false;
                        var currentState = Machinata.AR.elems.marker[i].object3D.visible == true;
                        // Change?
                        if (currentState != lastState) {
                            changeDetected = true;
                            markerElem.data("marker-visible",currentState);
                            // Call handler
                            for (var i = 0; i < Machinata.AR._onMarkerDetect.length; i++) {
                                Machinata.AR._onMarkerDetect[i](currentState,markerElem);
                            }
                        }
                        if (currentState == true) anyVisible = true;
                    }
                    // Chnage?
                    if (changeDetected) {
                        // Update UI
                        if (anyVisible == true) {
                            $("body").addClass("marker-found");
                        } else {
                            $("body").removeClass("marker-found");
                        }
                    }
                }, 100);
            }
        };

        // Register the scene and begin A-Frame
        if(Machinata.AR.mode == Machinata.AR.MODE_PREVIEW) {
            $("body").append(Machinata.AR.elems.scene);
            if(callback) callback();
        } else {
            if (Machinata.AR.lazyLoadARJS == true) {
                // First load AR.js...
                Machinata.AR.loadScript(Machinata.AR.aframeARJSLib, displayScene);
            } else {
                // Should already be loaded
                displayScene();
            }
        }

    });

    

};




