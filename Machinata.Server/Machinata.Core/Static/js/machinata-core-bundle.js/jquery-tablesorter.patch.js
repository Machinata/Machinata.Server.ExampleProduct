diff --git "a/Machinata.Server/Machinata.Core/Static/js/machinata-core-bundle.js/jquery-tablesorter.js" "b/Machinata.Server/Machinata.Core/Static/js/machinata-core-bundle.js/jquery-tablesorter.js"
index 8c7248d..71523de 100644
--- "a/Machinata.Server/Machinata.Core/Static/js/machinata-core-bundle.js/jquery-tablesorter.js"
+++ "b/Machinata.Server/Machinata.Core/Static/js/machinata-core-bundle.js/jquery-tablesorter.js"
@@ -885,7 +885,7 @@
 				debug = ts.debug(c, 'core');
 			// update tbody variable
 			c.$tbodies = c.$table.children( 'tbody:not(.' + c.cssInfoBlock + ')' );
-			$tbody = typeof $tbodies === 'undefined' ? c.$tbodies : $tbodies,
+			$tbody = typeof $tbodies === 'undefined' ? c.$tbodies : $tbodies;
 			c.cache = {};
 			c.totalRows = 0;
 			// if no parsers found, return - it's an empty table.
