
/// <summary>
/// Machinata Core JS Library
///
/// A extensive Javscript library that provides rapid web development and aligns nicely
/// with the Machinata Server.
///
/// All modules should be mounted on this namespace.
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>



/* ======== Globals ========================================================== */

/// <summary>
/// Main debug switch. Is automatically enabled with ?debug=true
/// Deprecated: use global.MACHINATA_DEBUG_ENABLED instead
/// </summary>
/// <deprecated/>
/// <hidden/>
/// <namespace>Machinata</namespace>
/// <name>DebugEnabled</name>
Machinata.DebugEnabled = MACHINATA_GLOBAL.MACHINATA_DEBUG_ENABLED;

/* ======== Debugging Functions ========================================================== */

/// <summary>
/// Debug output to console, but only if debug is enabled.
/// </summary>
Machinata.debug = function (str1, str2, str3, str4) {
    if (Machinata.DebugEnabled == false) return; // so that it can be compiled away completely...
    if(str4 != null) console.log(str1, str2, str3, str4);
    else if(str3 != null) console.log(str1, str2, str3);
    else if(str2 != null) console.log(str1, str2);
    else console.log(str1);
};

/// <summary>
/// Warning output to console.
/// </summary>
Machinata.warning = function (str1, str2) {
    if (str2 != null) console.log("WARNING: " + str1 + " " + str2);
    else console.log("WARNING: " + str1);
};

/// <summary>
/// Shorthand for debug of object data
/// </summary>
Machinata.echo = function (data) {
    alert(JSON.stringify(data));
};

/// <summary>
/// Util method for a void function that does nothing.
/// </summary>
Machinata.noop = function () {};



/* ======== Helper Functions ========================================================== */

/// <summary>
/// Given a string, a full function name is automatically resolved.
/// For example, Machinata.UI.xyz would return xyz function object, bound to Machinata.UI object.
/// </summary>
Machinata.unpackScopedFunctionByName = function (string) {
    var scope = window;
    var scopeSplit = string.split('.');
    for (var i = 0; i < scopeSplit.length - 1; i++) {
        scope = scope[scopeSplit[i]];
        if (scope == undefined) return;
    }
    return scope[scopeSplit[scopeSplit.length - 1]];
};

/// <summary>
/// Automatically processes a callback string
/// Callback strings can either be paths to resources (such as
/// a page) specified by a starting slash (/),
/// or an actually function
/// Callback strings are automatically parsed for variables
/// contained in the data object. For example, if the data object has
/// node { path = "xyz" } you can use the variable {node.path} or 
/// { data.node.path }.
/// </summary>
Machinata.processCallback = function (callbackString, message) {
    if (callbackString == null) return;
    Machinata.debug("processCallback", callbackString);
    // Function or page?
    if (callbackString.startsWith("/")) {
        // Page
        // Process all variables
        if (message != null && message.data != null) {
            function traverse(key, jsonObj) {
                if (typeof jsonObj == "object") {
                    $.each(jsonObj, function (k, v) {
                        // k is either an array index or object key
                        var newKey = key + (key != "" ? "." : "") + k;
                        if (typeof v != "object") {
                            callbackString = callbackString.replaceAll("{" + newKey + "}", v);
                            callbackString = callbackString.replaceAll("{data." + newKey + "}", v);
                        }
                        traverse(newKey, v);
                    });
                } else {
                    // jsonOb is a number or string
                    //alert(jsonObj);
                }
            }
            traverse("", message.data);
        }
        Machinata.goToPage(callbackString);
    } else {
        // Function
        var fn = Machinata.unpackScopedFunctionByName(callbackString);
        if (typeof fn === 'function') {
            fn(message);
        } else {
            Machinata.warning("Could not processCallback for", callbackString);
        }
    }
};

/* ======== Init Routines ========================================================== */



/// <summary>
/// This is the main init routine for Machinata. It is automatically called and doesn't
/// need to be manually called.
/// </summary>
Machinata.init = function () {
    // Debug?
    if (Machinata.queryParameter("debug") == "true") {
        Machinata.DebugEnabled = true;
    }
    // Run all init routines...
    for (var i = 0; i < Machinata._onInits.length; i++) Machinata._onInits[i]();
    // Run all on ready...
    for (var i = 0; i < Machinata._onReady.length; i++) Machinata._onReady[i]();
};

Machinata._onInits = [];

/// <summary>
/// Bind a handler to the Machinata init routine.
/// Use this method to init things such as modules.
/// </summary>
/// <type>function</type>
Machinata.onInit = function (fn) {
    Machinata._onInits.push(fn);
};


Machinata._onReady = [];

/// <summary>
/// Bind a handler to the Machinata ready routine.
/// Use this method to setup things that rely on other modules and the DOM.
/// </summary>
Machinata.ready = function (fn) {
    Machinata._onReady.push(fn);
};


Machinata._onLoaded = [];

/// <summary>
/// Bind a handler to the window loaded routine.
/// </summary>
Machinata.loaded = function (fn) {
    Machinata._onLoaded.push(fn);
};


Machinata._doOnce = {};

/// <summary>
/// Execute a function only once for the given id.
/// A second call using the same id is ignored.
/// </summary>
Machinata.doOnce = function (id, fn) {
    if (Machinata._doOnce[id] == true) return;
    Machinata._doOnce[id] = true;
    fn();
};


/* ======== API Functions ========================================================== */

/// <summary>
/// Class for handling a standard AJAX API call to a Machinata server.
/// </summary>
/// <type>class</type>
var APICall = function (call, data, method) {
    this.call = call;
    this._data = data;
    this.dataType = "json";
    this.contentType = "application/x-www-form-urlencoded; charset=UTF-8"; // by default we send form encoded key/vals
    this.method = method;
    this._files = null;
    this._onSuccesses = [];
    this._onErrors = [];
    this._onProgresses = [];
};

/// <summary>
/// 
/// </summary>
APICall.prototype.success = function (onSuccessFunction) {
    this._onSuccesses.push(onSuccessFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.error = function (onErrorFunction) {
    this._onErrors.push(onErrorFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.progress = function (onProgressFunction) {
    this._onProgresses.push(onProgressFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.genericError = function () {
    this.error(function (message) {
        Machinata.apiError(message.data.code, message.data.message, message);
    });
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.send = function () {
    return this.process();
};

/// <summary>
/// 
/// </summary>
APICall.prototype.data = function (data) {
    this._data = data;
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.json = function (data) {
    this._data = JSON.stringify(data);
    this.contentType = "application/json"; //TODO: charset?? + "; charset=UTF-8"
    return this;
};

/// <summary>
/// Attaches a file using a native html file input element.
/// </summary>
APICall.prototype.attachFileInput = function (object, name, filename) {
    if (this._files == null) this._files = [];
    var file = {};
    file.object = object;
    file.name = name;
    file.filename = filename;
    this._files.push(file);
    return this;
};

/// <summary>
/// Attaches a file by automatically creating and invoking a file input elem.
/// </summary>
APICall.prototype.attachFileAndSend = function (inputAccept) {
    var self = this;
    if (inputAccept == null) inputAccept = "*";
    var inputElem = $("<input type='file' accept='" + inputAccept + "' style='position:absolute;left:-10000px;top:0px;'/>");
    $("body").append(inputElem);
    inputElem.on("change", function () {
        // Attach file to call and send
        var fileNative = $(this)[0];
        //self.attachFileInput(fileNative.files[0], name, fileNative.files[0].name);
        self.attachFileInput(fileNative.files[0], null, fileNative.files[0].name);
        self.send();
    });
    inputElem.trigger("click");
    return this;
};

/// <summary>
/// 
/// </summary>
APICall.prototype.process = function () {
    Machinata.showLoading(true);

    // Get data type
    if (this.dataType == null) this.dataType = "json"; // default
    if (this.method == null) this.method = "POST"; // default

    // Compile url
    var url = this.call;
    //if (authToken != null && authToken != "") url += "&auth-token=" + authToken;
    Machinata.debug("APICall.process() url=" + url + " method=" + this.method);

    // Set language, if not already
    if (this._data == null) this._data = {};
    if (this._data["lang"] == null) {
        this._data["lang"] = "{language}";
    }
    // Create new data object
    // Per default we use standard jquery processData = true,
    // but if we have a file we create our own HTML FormData object
    var formData = this._data;
    var processData = true;
    var contentType = this.contentType;
    var dataType = this.dataType;
    if (this._files != null) {
        var formData = new FormData();
        for (var key in this._data) {
            formData.append(key, this._data[key]);
        }
        for (var i = 0; i < this._files.length; i++) {
            formData.append(this._files[i].name, this._files[i].object, this._files[i].filename);
        }
        dataType = null;
        processData = false;
        contentType = false;
    }

    // Send off request
    var self = this;
    $.ajax({
        type: self.method,
        url: url,
        data: formData,
        cache: false,
        traditional: true,
        processData: processData,
        contentType: contentType,
        success: function (message) {
            Machinata.showLoading(false);
            if (self.dataType == "html") {
                Machinata.debug("API success");
                for (var i = 0; i < self._onSuccesses.length; i++) self._onSuccesses[i](message);
            } else if (message) {
                Machinata.debug("API success: " + message.type);
                for (var i = 0; i < self._onSuccesses.length; i++) self._onSuccesses[i](message);
            } else {
                Machinata.apiError("response-message-null", "{text.error-response-message-null=No message was recieved from server.}");
            }
        },
        error: function (xhr, status, errorThrown) {
            Machinata.showLoading(false);
            Machinata.debug("API error: " + errorThrown);
            // Server error
            var message;
            try {
                message = JSON.parse(xhr.responseText);
            } catch (jsonError) {
                if (self._onErrors.length > 0) {
                    for (var i = 0; i < self._onErrors.length; i++) self._onErrors[i]({ type: "error", data: { code: "invalid-response", message: "The server did not respond." } });
                } else {
                    Machinata.apiError("invalid-response", "{text.error-invalid-response=The server did not respond.}");
                }
                return self;
            }
            //TODO: what if the server responds with non-JSON error?
            if (Machinata.error401Handler != null && xhr.status == 401) {
                Machinata.error401Handler(message.data.code, message.data.message, message);
            } else {
                if (self._onErrors.length > 0) {
                    for (var i = 0; i < self._onErrors.length; i++) self._onErrors[i](message);
                } else {
                    // Show generic error
                    Machinata.apiError(message.data.code, message.data.message, self);
                }
            }
        },
        dataType: dataType
    }).uploadProgress(function(e) {
        if (e.lengthComputable && self._onProgresses.length > 0) {
            var percentage = Math.round((e.loaded * 100) / e.total);
            Machinata.debug("APICall.uploadProgress " + percentage);
            for (var i = 0; i < self._onProgresses.length; i++) self._onProgresses[i](percentage);
        }
    });
    return this;
};


/// <summary>
/// Shorthand method for creating a API call object.
/// </summary>
Machinata.apiCall = function (call, data, method) {
    Machinata.debug("apiCall(" + call + "," + JSON.stringify(data) + ")");
    var apiCall = new APICall(call, data, method);
    return apiCall;
};


/* ======== API Errors ========================================================== */

/// <summary>
/// Standardized handling of generic API call errors.
/// </summary>
Machinata.apiErrorForGeneric = function (code, message, data) {
    Machinata.apiError(code, message, data);
};

/// <summary>
/// Standardized handling of API call errors.
/// </summary>
Machinata.apiError = function (code, message, data) {
    Machinata.debug("apiError: " + code + ": " + message);
    Machinata.errorHandler(code, message, data);
};

/* ======== Error Handler ========================================================== */

/// <summary>
/// Standardized error message.
/// </summary>
Machinata.genericError = function (code, message) {
    Machinata.errorHandler(code, message, null);
};

/// <summary>
/// Provides a standard and universal out-of-the-box error handler.
/// </summary>
Machinata.errorHandler = function (code, message, data) {
    Machinata.debug("errorHandler: " + code + ": " + message);
    var diag = Machinata.messageDialog("Error", message + " (" + code + ")");
    if (data != null && data.data != null && data.data.stacktrace != null) {
        diag.button("Details");
        diag.button("Okay");
    }
    diag.okay(function (id) {
        if (id == "details") {
            var diag2 = Machinata.messageDialog("Error Details", data.data.stacktrace + "<br/>" + data.data.logtrace)
            diag2.design("wide");
            diag2.show();
        }
    });
    diag.show();
};

/// <summary>
/// Provides a mechanism to handle 401 (unauthorized) errors for a projects.
/// For example: you can implement this method to call up a signup/login form
/// on a 401 error caused by a API call.
/// Parameters: 
///     Machinata.error401Handler = function (code, message, apiCall) { your code };
/// </summary>
Machinata.error401Handler = null;

/* ======== Basic UI ========================================================== */

/// <summary>
/// Make the body register a loading state.
/// Call this any time a routine is causing a UI to wait or block.
/// </summary>
Machinata.showLoading = function (show) {
    if (show == true) $("body").addClass("loading");
    else $("body").removeClass("loading");
};

/// <summary>
/// Navigate to a page. This method handles a loading state automatically for
/// leaving a page via a link (for example) and then navigating back via the back button.
/// </summary>
Machinata.goToPage = function (page, showLoading) {
    if (showLoading != false) {
        if (!$("body").hasClass("bb-window-blur-bound")) {
            $(window).on("blur", function () {
                Machinata.showLoading(false);
            });
            $("body").addClass("bb-window-blur-bound");
        }
        Machinata.showLoading(true);
    }
    window.location = page;
};

/// <summary>
/// Open a page in a new window.
/// </summary>
Machinata.openPage = function (page) {
    window.open(page, '_blank');
};

/// <summary>
/// Navigates back a page
/// </summary>
Machinata.backAPage = function (page) {
    window.history.back();
};

/// <summary>
/// Do something after a delay in milliseconds.
/// </summary>
Machinata.delay = function (ms, fn) {
    var self = this;
    setTimeout(function () {
        if (fn) fn();
        return self;
    }, ms);
};

/// <summary>
/// Shorthand for calling a function safely if it exists.
/// You can pass null without raising an exception.
/// </summary>
Machinata["do"] = function (fn) {  // Note do is reserved Ecma keyword
    if (fn) fn();
    return this;
};

/// <summary>
/// Reloads the current page.
/// </summary>
Machinata.reload = function (soft) {
    if (soft == true) {
        Machinata.showLoading(true);
        Machinata.changeQueryString("reload-guid", Machinata.guid());
    } else {
        Machinata.showLoading(true);
        document.location.reload();
    }
};

/* ========  Functions ========================================================== */

/// <summary>
/// Returns a key value objection with build information.
/// </summary>
/// <example>
/// ```
/// {
///     id: "a18829bfc0feaa0e99aeb24f53d6fb445413598551ef351806e6903cb1ab7019",
///     date: "02.09.2019 13:17 UTC",
///     timestamp: "2019-09-02T13:15:41.1500000Z",
///     user: "username",
///     machine: "MACHINE"
/// }
///  ```
/// </example>
Machinata.buildInfo = function () {
    return {
        id: "{build-id}",
        version: "{build-version}",
        date: "{build-date}",
        timestamp: "{build-timestamp}",
        user: "{build-user}",
        machine: "{build-machine}",
        profile: "{build-profile}"
    };
};


/// <summary>
/// Returns a GUID using Math.random()
/// </summary>
Machinata.guid = function () {
    var lut = []; for (var i = 0; i < 256; i++) { lut[i] = (i < 16 ? '0' : '') + (i).toString(16); }
    var d0 = Math.random() * 0xffffffff | 0;
    var d1 = Math.random() * 0xffffffff | 0;
    var d2 = Math.random() * 0xffffffff | 0;
    var d3 = Math.random() * 0xffffffff | 0;
    return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
      lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
      lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
      lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
};

/// <summary>
/// Retrieves a single query parameter from the query string.
/// </summary>
Machinata.queryParameter = function (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null) return "";
    else return decodeURIComponent(results[1].replace(/\+/g, " "));
};

/// <summary>
/// Returns true if a query parameters is not null or empty string.
/// </summary>
Machinata.hasQueryParameter = function (name) {
    var ret = Machinata.queryParameter(name);
    if (ret != null && ret != "") return true;
};

/// <summary>
/// Retrieves a single query parameter from the a URL.
/// </summary>
Machinata.queryParameterFromURL = function (name,url) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    if (results == null) return "";
    else return decodeURIComponent(results[1].replace(/\+/g, " "));
};

/// <summary>
/// Changes or switches a single parameter in the query string.
/// If no URL is provided, then the current URL is used.
/// The new URL is navigated to.
/// </summary>
Machinata.changeQueryString = function (key, value, url) {
    Machinata.showLoading(true);
    if (!url) url = window.location.href;
    window.location.href = Machinata.updateQueryString(key, value, url);
};

/// <summary>
/// Updates a query string parameter without navigating away from the page.
/// </summary>
Machinata.updateQueryString = function (key, value, url) {
    if (typeof value !== 'undefined' && value !== null) value = encodeURIComponent(value);
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");
    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            var hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    } else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?',
                hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
};

/// <summary>
/// Removes a parameter from a URL.
/// </summary>
Machinata.removeQueryParameter = function (key, url) {
    if (!url) url = window.location.href;
    var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        if (params_arr.length > 0) {
            rtn = rtn + "?" + params_arr.join("&");
        }
    }
    return rtn;
};


/// <summary>
/// Returns a string array list of all query parameters.
/// If no parametes, returns empty array.
/// </summary>
Machinata.getAllQueryParameter = function (url, filter) {
    if (!url) url = window.location.href;
    var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        var ret = [];
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (filter == null || param.startsWith(filter)) {
                ret.push(param);
            }
        }
        return ret;
    }
    return []; // empty
};


/// <summary>
/// Returns only the path portion of the current page URL.
/// </summary>
Machinata.getPagePath = function (url) {
    if (!url) url = window.location.href;
    return url.split('?')[0]; //TODO: is this safe on all browsers?
};


