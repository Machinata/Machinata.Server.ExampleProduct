


/* ======== jQuery Extensions ======== */


jQuery.fn.randomize = function (childElem) {
    function rnd(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    };
    return this.each(function () {
        var $this = $(this);
        var elems = $this.children(childElem);
        elems.each(function () {
            $(this).data("sort", rnd(0, elems.length * 1000));
        });
        elems.sort(function (a, b) { return $(a).data("sort") < $(b).data("sort"); });
        $this.remove(childElem);
        for (var i = 0; i < elems.length; i++) $this.append(elems[i]);
    });
};

jQuery.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
};


/**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */
jQuery.fn.visible = function (partial) {
    if (partial == null) partial = true;
    var $t = $(this),
        $w = $(window),
        viewTop = $w.scrollTop(),
        viewBottom = viewTop + $w.height(),
        _top = $t.offset().top,
        _bottom = _top + $t.height(),
        compareTop = partial === true ? _bottom : _top,
        compareBottom = partial === true ? _top : _bottom;
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};

jQuery.fn.caret = function (begin, end) {
    if (this.length == 0) return;
    if (typeof begin == 'number') {
        end = (typeof end == 'number') ? end : begin;
        return this.each(function () {
            if (this.setSelectionRange) {
                this.setSelectionRange(begin, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', begin);
                try { range.select(); } catch (ex) { }
            }
        });
    } else {
        if (this[0].setSelectionRange) {
            begin = this[0].selectionStart;
            end = this[0].selectionEnd;
        } else if (document.selection && document.selection.createRange) {
            var range = document.selection.createRange();
            begin = 0 - range.duplicate().moveStart('character', -100000);
            end = begin + range.text.length;
        }
        return { begin: begin, end: end };
    }
};


jQuery.fn.press = function (func) {
    throw "jQuery.fn.press via Machinata Core JS is no longer supported."
    /*
    // Cross platform mobile compatible touchend algorithm for quickly detecting 'presses'
    if (!Machinata.Device.isTouchEnabled()) {
        $(this).click(func);
    } else {
        $(this).bind('touchend', function (e) {
            // Get the element for the touch end
            var touch = event.changedTouches[0];
            var offsetX = touch.pageX - document.body.scrollLeft;
            var offsetY = touch.pageY - document.body.scrollTop;
            var elem = document.elementFromPoint(offsetX, offsetY);

            // Does it match?
            if (elem == this) {
                // Call the function via a proxy so that the this object is properly set
                var proxyFunc = $.proxy(func, this);
                _lastTouchEndEvent = new Date().getTime(); // now in ms (ticks)
                proxyFunc();
            } else {
                // Now check close by in a grid manner as last resort
                var offsetCheck = 4;
                for (var x = -1; x <= 1; x++) {
                    for (var y = -1; y <= 1; y++) {
                        if (!(x == 0 && y == 0)) {
                            var offsetX = touch.pageX - document.body.scrollLeft + (x * offsetCheck);
                            var offsetY = touch.pageY - document.body.scrollTop + (y * offsetCheck);
                            var elem = document.elementFromPoint(offsetX, offsetY);
                            if (elem == this) {
                                var proxyFunc = $.proxy(func, this);
                                _lastTouchEndEvent = new Date().getTime(); // now in ms (ticks)
                                proxyFunc();
                                return;
                            }
                        }
                    }
                }
            }
        });
    }*/
    return this;
};


jQuery.eachBack = function (obj, callback) {
    var revKeys = []; $.each(obj, function (rind, rval) { revKeys.push(rind); });
    revKeys.reverse();
    $.each(revKeys, function (kind, i) {
        if (callback.call(obj[i], i, obj[i]) === false) { return false; }
    });
    return obj;
};
jQuery.fn.eachBack = function (callback, args) {
    return jQuery.eachBack(this, callback, args);
};

jQuery.eachReverse = function (obj, callback) {
    var revKeys = []; $.each(obj, function (rind, rval) { revKeys.push(rind); });
    revKeys.reverse();
    $.each(revKeys, function (kind, i) {
        if (callback.call(obj[i], i, obj[i]) === false) { return false; }
    });
    return obj;
};
jQuery.fn.eachReverse = function (callback, args) {
    return jQuery.eachReverse(this, callback, args);
};