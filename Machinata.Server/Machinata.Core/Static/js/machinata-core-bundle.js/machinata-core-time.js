
/* ======== Time Extensions ======== */

Date.prototype.addDays = function (days) {
    return new Date(this.getTime() + (days * 24 * 60 * 60 * 1000));
};

Date.prototype.iso8601CalendarWeek = function () {
    // Via https://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    return Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
};

Date.prototype.isToday = function () {
    var now = new Date();
    if (this.getYear() == now.getYear()
        && this.getMonth() == now.getMonth()
        && this.getDay() == now.getDay()) {
        return true;
    } else {
        return false;
    }
};


/// <summary>
/// Collection of useful time related functions.
/// Many of the functions align with the Machinata Server Core Time utilities.
/// </summary>
Machinata.Time = {};

/// <summary>
/// 
/// </summary>
Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI = "{config.date-format}";

/// <summary>
/// 
/// </summary>
Machinata.DEFAULT_TIME_FORMAT_JAVASCRIPT_UI = "{config.time-format}";

/// <summary>
/// 
/// </summary>
Machinata.DEFAULT_DATETIME_FORMAT_JAVASCRIPT_UI = "{config.date-time-format}";

/// <summary>
/// 
/// </summary>
Machinata.DEFAULT_TIMEZONE = "{config.default-timezone}";

/// <summary>
/// 
/// </summary>
Machinata.DEFAULT_DATERANGE_SEPARATOR = "{config.date-range-separator}";

/// <summary>
///
/// </summary>
/// <hidden/>

Machinata.onInit(function () {
    // Custom time picker
    Machinata.initCustomTimePicker();
});

/// <summary>
/// Converts a .net time string to javascript time string.
/// Supported params: 'HH','mm'
/// Currently this does not do anything.
/// </summary>
Machinata.convertDotNetTimeFormatToJavascriptFormat = function (format) {
    // .net: https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
    // Datepicker: http://api.jqueryui.com/datepicker/#utility-formatDate 
    // Timepicker: http://trentrichardson.com/examples/timepicker/
    return format;
};

/// <summary>
/// Converts a .net time string to datepicker time string to allow for a single
/// format representation.
/// Supported params: 'yyyy','MM','dd','HH','mm','zzz'
/// </summary>
Machinata.convertDotNetTimeFormatToDatepickerFormat = function (format) {
    // .net: https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
    // Datepicker: http://api.jqueryui.com/datepicker/#utility-formatDate 
    // Timepicker: http://trentrichardson.com/examples/timepicker/
    format = format.replace("yyyy", "yy");
    format = format.replace("MM", "mm");
    format = format.replace("dd", "dd"); // Same
    format = format.replace("zzz", "Z");
    return format;
};
Machinata.convertDatepickerTimeFormatToDotNetFormat = function (format) {
    format = format.replace("yy", "yyyy");
    format = format.replace("mm", "MM");
    format = format.replace("dd", "dd"); // Same
    format = format.replace("Z","zzz");
    return format;
    };

/// <summary>
/// Parses a utcms string to a javascript date time object.
/// If you are coming from the backend world, use ```Machinata.Util.Time.GetUTCMillisecondsFromDateTime()```.
/// </summary>
Machinata.dateFromUTCMilliseconds = function (utcmsString) {
    var utcms = parseInt(utcmsString);
    return new Date(utcms);
};

/// <summary>
/// Converts a javascript date time object to a utcms integer.
/// If the input is a string, the date is first automatically converted
/// using the native javascript new Date(str) method.
/// </summary>
Machinata.UTCMillisecondsFromDate = function (date) {
    // https://www.electrictoolbox.com/unix-timestamp-javascript/
    if (typeof date === 'string' || date instanceof String) {
        date = new Date(date);
    }
    return Math.round(date.getTime());
};

/// <summary>
/// Gets a formatted time from a date object.
/// </summary>
Machinata.formattedTimeFromDate = function (date) {
    return Machinata.formattedDate(date, Machinata.DEFAULT_TIME_FORMAT_JAVASCRIPT_UI);
};

/// <summary>
/// Gets a formatted date from a date object.
/// </summary>
Machinata.formattedDateFromDate = function (date) {
    return Machinata.formattedDate(date, Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI);
};

/// <summary>
/// Gets a formatted date and time from a date object, separated by a space.
/// </summary>
Machinata.formattedDateTimeFromDate = function (date) {
    return Machinata.formattedDateFromDate(date) + " " + Machinata.formattedTimeFromDate(date);
};

/// <summary>
/// Returns a string for the date and given format using .net style replacements.
/// This custom implementation ensures that all browsers behave the same way.
/// Supported params: 'yyyy', 'MM', 'dd', 'HH', 'mm'
/// If not format is provided, then the default format DEFAULT_DATETIME_FORMAT_JAVASCRIPT_UI is used.
/// </summary>
Machinata.formattedDate = function (date, format) {
    // Init
    if (format == null) format = Machinata.DEFAULT_DATETIME_FORMAT_JAVASCRIPT_UI;
    var yy = date.getYear().toString();
    var yyyy = date.getFullYear().toString();
    var MM = (date.getMonth() + 1).toString().padLeft(2, '0');
    var dd = date.getDate().toString().padLeft(2, '0');
    var HH = date.getHours().toString().padLeft(2, '0');
    var mm = date.getMinutes().toString().padLeft(2, '0');
    var ss = date.getSeconds().toString().padLeft(2, '0');
    var ret = format;
    // Do replacements
    ret = ret.replace("yyyy", yyyy);
    ret = ret.replace("MM", MM);
    ret = ret.replace("dd", dd);
    ret = ret.replace("HH", HH);
    ret = ret.replace("mm", mm);
    ret = ret.replace("ss", ss);
    //ret = ret.replace("yy", yy);
    return ret;
};


/// <summary>
/// 
/// </summary>
Machinata.dateFromString = function (strDateTime, format, verbose) {
    // Init
    //Machinata.debug("Converting date string to date object: '" + strDateTime + "' with format '" + format + "'");
    if (strDateTime == null || strDateTime == "") return null;
    if (format == null) format = Machinata.DEFAULT_DATETIME_FORMAT_JAVASCRIPT_UI;
    var segs = strDateTime.split(' ');
    var formatSegs = format.split(' ');
    if (segs.length < 0) {
        Machinata.debug("Error parsing date: " +strDateTime);
        return null;
    }
    // Extract the date and time
    var strDate = segs[0];
    var strTime = null; if (segs.length > 1) strTime = segs[1];
    // Extract format
    var formatDate = formatSegs[0];
    var formatTime = null; if (formatSegs.length > 1) formatTime = formatSegs[1];
    if (verbose == true) Machinata.debug("  strDate: " + strDate);
    if (verbose == true) Machinata.debug("  strTime: " + strTime);
    if (verbose == true) Machinata.debug("  formatDate: " + formatDate);
    if (verbose == true) Machinata.debug("  formatTime: " + formatTime);
    // Get the date and time
    var date = $.datepicker.parseDate(formatDate, strDate);
    if (verbose == true) Machinata.debug("  date: " + date);
    if (formatTime != null) {
        var time = $.datepicker.parseTime(formatTime, strTime);
        date.setHours(time.hour);
        date.setMinutes(time.minute);
        date.setSeconds(time.second);
    }
    if (verbose == true) Machinata.debug("  Converted to: " + date);
    return date;
}

/// <summary>
/// 
/// </summary>
Machinata.daysHoursMinutesSecondsFromDate = function (date) {
    var ms = date.getTime();
    var s = Math.floor(ms / 1000);
    var m = Math.floor(s / 60);
    s = s % 60;
    var h = Math.floor(m / 60);
    m = m % 60;
    var d = Math.floor(h / 24);
    h = h % 24;
    return { days: d, hours: h, minutes: m, seconds: s };
};

/// <summary>
/// 
/// </summary>
Machinata.Time.getMonths = function (start, end) {
    var dates = [];
    var current = new Date(start.getFullYear(), start.getMonth(), 1, 0, 0, 0, 0);
    
    var i = 0;
    while (current < end && i < 1000) {
        dates.push(current);
        var currentYear = current.getFullYear();
        var nextYear = currentYear;
        var currentMonth = current.getMonth();
        var nextMonth = currentMonth+1;
        if (nextMonth > 11) {
            nextMonth = 0;
            nextYear++;
        }
        var next = new Date(nextYear, nextMonth, 1, 0, 0, 0, 0);
        //dates.push(next);
        current = next;
        i++;
    }
    return dates;
};

/// <summary>
/// 
/// </summary>
Machinata.Time.getShortMonth = function (date) {
    // Mimicks date.toLocaleString("de-ch", { month: "short" });
    // (due to browser support of toLocalString)
    var m = date.getMonth();
    if (m == 0) return "{text.january-short}";
    else if (m == 1) return "{text.february-short}";
    else if (m == 2) return "{text.march-short}";
    else if (m == 3) return "{text.april-short}";
    else if (m == 4) return "{text.may-short}";
    else if (m == 5) return "{text.june-short}";
    else if (m == 6) return "{text.july-short}";
    else if (m == 7) return "{text.august-short}";
    else if (m == 8) return "{text.september-short}";
    else if (m == 9) return "{text.october-short}";
    else if (m == 10) return "{text.november-short}";
    else if (m == 11) return "{text.december-short}";
    else return null;
};

/// <summary>
/// 
/// </summary>
Machinata.Time.humanReadableTimestamp = function (datetime, now) {
    // Init
    if (now == null) now = new Date();
    var timestamp = datetime;
    var timesince_ms = (now - timestamp);
    var timesince_s = timesince_ms / 1000;
    var timesince_m = timesince_s / 60;
    var timesince_h = timesince_m / 60;
    var timesince_d = timesince_h / 24;
    var today = new Date(now.getTime());
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    var yesterday = new Date(now.getTime());
    yesterday.setHours(0);
    yesterday.setMinutes(0);
    yesterday.setSeconds(0);
    yesterday.setDate(yesterday.getDate() - 1);
    var days = ['{text.day-of-week.sunday}', '{text.day-of-week.monday}', '{text.day-of-week.tuesday}', '{text.day-of-week.wednesday}', '{text.day-of-week.thursday}', '{text.day-of-week.friday}', '{text.day-of-week.saturday}'];

    // Future or past???
    if (timesince_ms < 0) {

        // Future!

        // In less than one minute - show seconds
        if (timesince_m > -1) {
            return "{text.human-readable.in}".replace("{time}", Math.floor(timesince_s * -1) + "s");
        }

        // In less than one hour - show minutes
        else if (timesince_h > -1) {
            return "{text.human-readable.in}".replace("{time}", Math.floor(timesince_m * -1) + "m");
        }

        // Today - show hours
        else  {
            return "{text.human-readable.in}".replace("{time}", Math.floor(timesince_h * -1) + "h");
        }

    } else {

        // Past!

        // Less than one minute - show seconds
        if (timesince_m < 1) {
            if (timesince_ms == 5000) {
                return "{text.human-readable.justnow}";
            } else {
                return "{text.human-readable.ago}".replace("{time}", Math.floor(timesince_s) + "s");
                //return "1m ago"
            }
        }

            // Less than one minute - show minutes
        else if (timesince_h < 1) {
            return "{text.human-readable.ago}".replace("{time}", Math.floor(timesince_m) + "m");
        }

            // Today - show hours
        else if (timestamp > today) {
            return "{text.human-readable.ago}".replace("{time}", Math.floor(timesince_h) + "h");
        }

            // Yesterday - show day and time
        else if (timestamp > yesterday) {
            return "{text.human-readable.yesterday}";
        }

            // One week ago - show day and time
        else if (timesince_d < 6) {
            return days[timestamp.getDay()];
        }

            // More than a week - show date
        else {
            return timestamp.getDate() + "." + (timestamp.getMonth() + 1) + "." + timestamp.getFullYear();
        }

    }


};

/// <summary>
/// 
/// </summary>
Machinata.Time.today = function () {
    return new Date();
};

/// <summary>
/// 
/// </summary>
Machinata.Time.yesterday = function () {
    return (new Date()).addDays(-1);
};

/// <summary>
/// 
/// </summary>
Machinata.Time.tomorrow = function () {
    return (new Date()).addDays(+1);
};


/// <summary>
/// 
/// </summary>
Machinata.Time.humanReadableDuration = function (datetime) {
    // Init
    var timestamp = datetime;
    var timesince_ms = timestamp.getTime();
    var timesince_s = timesince_ms / 1000;
    var timesince_m = timesince_s / 60;
    var timesince_h = timesince_m / 60;
    var timesince_d = timesince_h / 24;
    var timesince_mm = timesince_d / 30;
    var timesince_yy = timesince_mm / 12;
    
    if (timesince_s < 1) {
        return "{text.human-readable.duration.milliseconds}".replace("{time}", Math.floor(timesince_ms));
    } else if (timesince_m < 1) {
        return "{text.human-readable.duration.seconds}".replace("{time}", Math.floor(timesince_s));
    } else if (timesince_h < 1) {
        return "{text.human-readable.duration.minutes}".replace("{time}", Math.floor(timesince_m));
    } else if (timesince_d < 1) {
        return "{text.human-readable.duration.hours}".replace("{time}", Math.floor(timesince_h));
    } else if (timesince_mm < 1) {
        return "{text.human-readable.duration.days}".replace("{time}", Math.floor(timesince_d));
    } else if (timesince_yy < 1) {
        return "{text.human-readable.duration.months}".replace("{time}", Math.floor(timesince_mm));
    } else  {
        return "{text.human-readable.duration.years}".replace("{time}", Math.floor(timesince_yy));
    }

};


/// <summary>
/// Machinata custom time picker
/// Registers the custom time picker control.
/// </summary>
Machinata.initCustomTimePicker = function () {
    // Custom control object
    var customControl = {
        createButton: function (tp_inst, obj, hour, minute) {
            var button = $("<button/>");
            var textHour = hour;
            if (hour < 10) textHour = "0" +textHour;
            var textMinute = minute;
            if (minute < 10) textMinute = "0" +textMinute;
            button.text(textHour + ":" +textMinute);
            button.attr("data-hour", hour);
            button.attr("data-minute", minute);
            button.addClass("hour-"+hour);
            button.addClass("minute-"+minute);
            button.button();
            button.click(function () {
                //button.parent().find("button").removeClass("selected");
                //button.addClass("selected");
                tp_inst.control.value(tp_inst, obj, "hour", hour);
                tp_inst.control.value(tp_inst, obj, "minute", minute);
                tp_inst._onTimeChange();
                tp_inst._onSelectHandler();
                });
                return button;
        },
            create: function (tp_inst, obj, unit, val, min, max, step) {
                //Machinata.debug("CustomTimePicker: create control with '" + val + "' for " + unit);
            var self = this;
            if (unit == "hour") {
                // Register value
                tp_inst.currentHour = val;
            } else if (unit == "minute") {
                // Register value
                tp_inst.currentMinute = val;
                // Fix some ui stuff
                var container = obj.closest(".ui-timepicker-div");
                container.find(".ui_tpicker_hour_label").hide();
                container.find(".ui_tpicker_minute_label").hide();
                tp_inst.inst.dpDiv.find(".ui-datepicker-current").hide();
                tp_inst.inst.dpDiv.find(".ui-datepicker-buttonpane button").button();
                // Generate all options
                var buttons = $("<div class='ui-time-buttons'/>");
                for (var h = 0; h < 24; h++) {
                    for (var m = 0; m < 60; m += step) {
                        var button = self.createButton(tp_inst, obj, h, m);
                        buttons.append(button);
                }
            }
                buttons.appendTo(container);
        }
            return obj;
        },
            options: function (tp_inst, obj, unit, opts, val) {
            return obj;
                //if(typeof(opts) == 'string' && val !== undefined)
                //    return obj.find('.ui-timepicker-input').spinner(opts, val);
                //return obj.find('.ui-timepicker-input').spinner(opts);
        },
            value: function (tp_inst, obj, unit, val) {
            if (val !== undefined) {
                // Set
                //Machinata.debug("CustomTimePicker: set value '" +val +"' for "+unit);
                if (unit == "hour") {
                    tp_inst.currentHour = val;
            }
                if (unit == "minute") {
                    tp_inst.currentMinute = val;
                    var timeSelector = ".hour-" +tp_inst.currentHour + ".minute-" +tp_inst.currentMinute;
                    Machinata.debug("CustomTimePicker: selecting elem " + timeSelector);
                    obj.closest(".ui-timepicker-div").find(".ui-time-buttons button").removeClass("selected");
                    var selected = obj.closest(".ui-timepicker-div").find(".ui-time-buttons button" +timeSelector);
                    if (selected.length > 0) {
                        selected.addClass("selected");
                        var offsetV = -selected.parent().height() / 2 + selected.height() / 2;
                        setTimeout(function () {
                            selected.parent().scrollTo(selected, 0, { offset: offsetV });
                        }, 10);
                        
                }
            }
                return obj;
            } else {
                // Get
                //Machinata.debug("CustomTimePicker: get value for "+unit);
                if (unit == "hour") return parseInt(tp_inst.currentHour);
                if (unit == "minute") return parseInt(tp_inst.currentMinute);
                return 0; // We only support hour/minute
        }
    }
    };
    // Register
    if ($.timepicker) $.timepicker.setDefaults({ controlType: customControl});
};

