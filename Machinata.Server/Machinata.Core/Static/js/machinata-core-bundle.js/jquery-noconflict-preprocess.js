
/// <summary>
/// Detect if an existing version already exists
/// </summary>
/// <hidden/>
if (typeof window !== 'undefined' && (window.jQuery != null || window.$ != null)) {
    MACHINATA_GLOBAL.MACHINATA_JQUERY_EXISTING_VERSION = window.$.fn.jquery
    MACHINATA_GLOBAL.MACHINATA_JQUERY_ALREADY_EXISTS = true;
}