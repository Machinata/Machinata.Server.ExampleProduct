

/// <summary>
/// Library for a generic filter on a set of elements.
/// </summary>
/// <type>namespace</type>
Machinata.Filter = {};


/// <summary>
/// 
/// </summary>
Machinata.Filter.elements = null;


/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    
    $(".bb-data-filter").each(function () {
        // Init
        var elem = $(this);
        // Set settings
        elem.data("filter-url", elem.attr("data-filter-url"));
        elem.data("filter-results-selector", elem.attr("data-filter-results-selector"));
        elem.data("filter-item-selector", elem.attr("data-filter-item-selector"));
        if (elem.data("filter-url") == null) {
            var url = window.location.href;
            Machinata.debug("Machinata.Filter: autodetect URL: " + url);
            elem.data("filter-url", url);
        }
        if (elem.data("filter-item-selector") == null) {
            elem.data("filter-item-selector", ".bb-data-filter-results-item");
        }
        if (elem.data("filter-results-selector") == null) {
            elem.data("filter-results-selector", ".bb-data-filter-results");
        }
        // Bind interactions
        elem.find(".bb-data-filter-toggle").click(function () {
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected")
            } else {
                $(this).addClass("selected")
            }
            Machinata.Filter.update(elem);
        });
        var inputTimer = null;
        elem.find(".bb-data-filter-input").on("keyup", function () {
            if (inputTimer != null) clearTimeout(inputTimer);
            inputTimer = setTimeout(function () {
                Machinata.Filter.update(elem);
            }, 600);
            
        });
        // Initial state
        elem.find(".bb-data-filter-show-if-loading").hide();
        elem.find(".bb-data-filter-show-if-success").hide();
        elem.find(".bb-data-filter-show-if-empty").hide();
        elem.find(".bb-data-filter-show-if-error").hide();
        // Register element
        if (Machinata.Filter.elements == null) Machinata.Filter.elements = [];
        Machinata.Filter.elements.push(elem);
    });
});


/// <summary>
/// 
/// </summary>
Machinata.Filter.countElements = function (elem) {
    if (elem.data("filter-item-selector") != null) {
        return elem.find(elem.data("filter-item-selector")).length;
    } else {
        return elem.children().length;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Filter.update = function (elem) {
    // Validate
    if (elem.data("filter-loading") == true) return;
    // Show loading
    elem.addClass("loading");
    elem.data("filter-loading", true);
    elem.find(".bb-data-filter-show-if-loading").show();
    elem.find(".bb-data-filter-show-if-success").hide();
    elem.find(".bb-data-filter-show-if-empty").hide();
    elem.find(".bb-data-filter-show-if-error").hide();
    Machinata.showLoading(true);
    // Empty
    var resultsContainer = $(elem.data("filter-results-selector"));
    resultsContainer.empty();
    // Build URL
    var filterData = {};
    elem.find(".bb-data-filter-toggle.selected").each(function () {
        var key = $(this).attr("data-key");
        var val = $(this).attr("data-value");
        if (filterData[key] == null) {
            filterData[key] = val;
        } else {
            filterData[key] = filterData[key] + "," + val;
        }
    });
    elem.find(".bb-data-filter-input").each(function () {
        var key = $(this).attr("data-key");
        var val = $(this).val();
        if (val == null || val == "") return;
        if (filterData[key] == null) {
            filterData[key] = val;
        } else {
            filterData[key] = filterData[key] + "," + val;
        }
    });
    var loadString = elem.data("filter-url");
    var keys = Object.keys(filterData);
    for (var i = 0; i <keys.length; i++) {
        var key = keys[i];
        var val = filterData[key];
        loadString = Machinata.updateQueryString(key, val, loadString);
    }
    if (elem.data("filter-results-selector") != null) loadString += " " + elem.data("filter-results-selector");
    // Bookeeping
    var currentElems = Machinata.Filter.countElements(elem);
    Machinata.debug("Machinata.Filter: currentElems: " + currentElems);
    // Make request
    Machinata.debug("Machinata.Filter: load URL: " + loadString);
    var newElemsContainer = $("<div></div>");
    newElemsContainer.load(loadString, null, function (responseText, textStatus, jqXHR) {
        Machinata.showLoading(false);
        elem.removeClass("loading");
        // Error?
        if (textStatus == "error") {
            Machinata.debug("Machinata.Filter: error with URL: " + loadString);
            elem.data("filter-loading", false);
            elem.find(".bb-data-filter-show-if-loading").hide();
            elem.find(".bb-data-filter-show-if-error").show();
        } else {
            elem.data("filter-loading", false);
            elem.find(".bb-data-filter-show-if-loading").hide();
            elem.find(".bb-data-filter-show-if-success").show();
            newElemsContainer.children().appendTo(resultsContainer).addClass("new-item");
        }
        // Did we add elements?
        var newElems = Machinata.Filter.countElements(elem);
        Machinata.debug("Machinata.Filter: newElems: " + newElems);
        if (newElems <= 0) {
            Machinata.debug("Machinata.Filter: no elements added");
            elem.addClass("empty");
            elem.find(".bb-data-filter-show-if-empty").show();
            elem.find(".bb-data-filter-show-if-success").hide();
        } else {
            Machinata.UI.bind(newElemsContainer);
        }
    });
};