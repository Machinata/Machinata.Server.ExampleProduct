/// <summary>
/// Machinata Notebook by Dan Krusi
/// Based on jQuery Notebook 0.5
/// 
/// Copyright (c) 2014
/// Raphael Cruzeiro - http://raphaelcruzeiro.eu/
/// Otávio Soares
/// 
/// Released under the MIT License
/// http://opensource.org/licenses/MIT
/// 
/// Some functions of this plugin were based on Jacob Kelley's Medium.js
/// https://github.com/jakiestfu/Medium.js/
/// 
/// </summary>
/// <hidden/>
(function ($, d, w) {

    /*
     * This module deals with the CSS transforms. As it is not possible to easily
     * combine the transform functions with JavaScript this module abstract those
     * functions and generates a raw transform matrix, combining the new transform
     * with the others that were previously applied to the element.
     */

    var transform = (function () {
        var matrixToArray = function (str) {
            if (!str || str == 'none') {
                return [1, 0, 0, 1, 0, 0];
            }
            return str.match(/(-?[0-9\.]+)/g);
        };

        var getPreviousTransforms = function (elem) {
            return elem.css('-webkit-transform') || elem.css('transform') || elem.css('-moz-transform') ||
                elem.css('-o-transform') || elem.css('-ms-transform');
        };

        var getMatrix = function (elem) {
            var previousTransform = getPreviousTransforms(elem);
            return matrixToArray(previousTransform);
        };

        var applyTransform = function (elem, transform) {
            elem.css('-webkit-transform', transform);
            elem.css('-moz-transform', transform);
            elem.css('-o-transform', transform);
            elem.css('-ms-transform', transform);
            elem.css('transform', transform);
        };

        var buildTransformString = function (matrix) {
            return 'matrix(' + matrix[0] +
                ', ' + matrix[1] +
                ', ' + matrix[2] +
                ', ' + matrix[3] +
                ', ' + matrix[4] +
                ', ' + matrix[5] + ')';
        };

        var getTranslate = function (elem) {
            var matrix = getMatrix(elem);
            return {
                x: parseInt(matrix[4]),
                y: parseInt(matrix[5])
            };
        };

        var scale = function (elem, _scale) {
            var matrix = getMatrix(elem);
            matrix[0] = matrix[3] = _scale;
            var transform = buildTransformString(matrix);
            applyTransform(elem, transform);
        };

        var translate = function (elem, x, y) {
            var matrix = getMatrix(elem);
            matrix[4] = Math.round(x);
            matrix[5] = Math.round(y);
            var transform = buildTransformString(matrix);
            applyTransform(elem, transform);
        };

        var rotate = function (elem, deg) {
            var matrix = getMatrix(elem);
            var rad1 = deg * (Math.PI / 180);
            var rad2 = rad1 * -1;
            matrix[1] = rad1;
            matrix[2] = rad2;
            var transform = buildTransformString(matrix);
            applyTransform(elem, transform);
        };

        return {
            scale: scale,
            translate: translate,
            rotate: rotate,
            getTranslate: getTranslate
        };
    })();

    var isMac = w.navigator.platform == 'MacIntel',
        mouseX = 0,
        mouseY = 0,
        cache = {
            command: false,
            shift: false,
            isSelecting: false
        },
        modifiers = {
            66: 'bold',
            73: 'italic',
            85: 'underline',
            112: 'h1',
            113: 'h2',
            122: 'undo'
        },
        options,
        utils = {
            keyboard: {
                isCommand: function (e, callbackTrue, callbackFalse) {
                    if (isMac && e.metaKey || !isMac && e.ctrlKey) {
                        callbackTrue();
                    } else {
                        callbackFalse();
                    }
                },
                isShift: function (e, callbackTrue, callbackFalse) {
                    if (e.shiftKey) {
                        callbackTrue();
                    } else {
                        callbackFalse();
                    }
                },
                isModifier: function (e, callback) {
                    var key = e.which,
                        cmd = modifiers[key];
                    if (cmd) {
                        callback.call(this, cmd);
                    }
                },
                isEnter: function (e, callback) {
                    if (e.which === 13) {
                        callback();
                    }
                },
                isArrow: function (e, callback) {
                    if (e.which >= 37 || e.which <= 40) {
                        callback();
                    }
                }
            },
            html: {
                addTag: function (elem, tag, focus, editable) {
                    var newElement = $(d.createElement(tag));
                    if(editable==false)newElement.attr('contenteditable', Boolean(editable));
                    newElement.append(' ');
                    elem.append(newElement);
                    if (focus) {
                        cache.focusedElement = elem.children().last();
                        utils.cursor.set(elem, 0, cache.focusedElement);
                    }
                    return newElement;
                }
            },
            cursor: {
                set: function (editor, pos, elem) {
                    var range;
                    if (d.createRange) {
                        range = d.createRange();
                        var selection = w.getSelection(),
                            lastChild = editor.children().last(),
                            length = lastChild.html().length - 1,
                            toModify = elem ? elem[0] : lastChild[0],
                            theLength = typeof pos !== 'undefined' ? pos : length;
                        range.setStart(toModify, theLength);
                        range.collapse(true);
                        selection.removeAllRanges();
                        selection.addRange(range);
                    } else {
                        range = d.body.createTextRange();
                        range.moveToElementText(elem);
                        range.collapse(false);
                        range.select();
                    }
                }
            },
            selection: {
                save: function () {
                    if (w.getSelection) {
                        var sel = w.getSelection();
                        if (sel.rangeCount > 0) {
                            return sel.getRangeAt(0);
                        }
                    } else if (d.selection && d.selection.createRange) { // IE
                        return d.selection.createRange();
                    }
                    return null;
                },
                restore: function (range) {
                    if (range) {
                        if (w.getSelection) {
                            var sel = w.getSelection();
                            sel.removeAllRanges();
                            sel.addRange(range);
                        } else if (d.selection && range.select) { // IE
                            range.select();
                        }
                    }
                },
                getText: function () {
                    var txt = '';
                    if (w.getSelection) {
                        txt = w.getSelection().toString();
                    } else if (d.getSelection) {
                        txt = d.getSelection().toString();
                    } else if (d.selection) {
                        txt = d.selection.createRange().text;
                    }
                    return txt;
                },
                clear: function () {
                    if (window.getSelection) {
                        if (window.getSelection().empty) { // Chrome
                            window.getSelection().empty();
                        } else if (window.getSelection().removeAllRanges) { // Firefox
                            window.getSelection().removeAllRanges();
                        }
                    } else if (document.selection) { // IE?
                        document.selection.empty();
                    }
                },
                getContainer: function (sel) {
                    if (w.getSelection && sel && sel.commonAncestorContainer) {
                        return sel.commonAncestorContainer;
                    } else if (d.selection && sel && sel.parentElement) {
                        return sel.parentElement();
                    }
                    return null;
                },
                getSelection: function () {
                    if (w.getSelection) {
                        return w.getSelection();
                    } else if (d.selection && d.selection.createRange) { // IE
                        return d.selection;
                    }
                    return null;
                }
            },
            validation: {
                isUrl: function (url) {
                    return (/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/).test(url);
                }
            }
        },
        bubble = {
            /*
             * This is called to position the bubble above the selection.
             */
            updatePos: function (editor, elem) {
                var sel = w.getSelection(),
                    range = sel.getRangeAt(0),
                    boundary = range.getBoundingClientRect(),
                    bubbleWidth = elem.width(),
                    bubbleHeight = elem.height(),
                    offset = editor.offset(),
                    position = editor.position();
                var boundary = {
                    left: boundary.left - offset.left,
                    //top: boundary.top - offset.top + $(document).scrollTop(),
                    top: boundary.top - offset.top + $(document).scrollTop(),
                    width: boundary.width,
                    height: boundary.height,
                };
                var pos = {
                    x: (boundary.left + boundary.width / 2) - (bubbleWidth / 2) + position.left,
                    y: boundary.top - bubbleHeight - 20
                };
                console.log(offset,position,boundary,pos);
                transform.translate(elem, pos.x, pos.y);
            },
            /*
             * Updates the bubble to set the active formats for the current selection.
             */
            updateState: function (editor, elem) {
                elem.find('button').removeClass('active');
                var sel = w.getSelection(),
                    formats = [];
                bubble.checkForFormatting(sel.focusNode, formats);
                var formatDict = {
                    'b': 'bold',
                    'i': 'italic',
                    'u': 'underline',
                    'h1': 'h1',
                    'h2': 'h2',
                    'p': 'p',
                    'a': 'anchor',
                    'img': 'image',
                    'strike': 'strike',
                    'ul': 'ul',
                    'ol': 'ol'
                };
                for (var i = 0; i < formats.length; i++) {
                    var format = formats[i];
                    elem.find('button.cmd-' + formatDict[format]).addClass('active');
                }
            },
            /*
             * Recursively navigates upwards in the DOM to find all the format
             * tags enclosing the selection.
             */
            checkForFormatting: function (currentNode, formats) {
                //Machinata.debug("checkForFormatting: " + currentNode.nodeName);
                //Machinata.debug("  html: " + $(currentNode).html());
                var validFormats = ['b', 'i', 'u', 'h1', 'h2', 'ol', 'ul', 'li', 'a', 'img', 'strike', 'p'];
                if (currentNode.nodeName === '#text' ||
                    validFormats.indexOf(currentNode.nodeName.toLowerCase()) != -1) {
                    if (currentNode.nodeName != '#text') {
                        formats.push(currentNode.nodeName.toLowerCase());
                    }
                    //Machinata.debug("  will recurse");
                    bubble.checkForFormatting(currentNode.parentNode, formats);
                } else {
                    // Special handling for some cases where the selction contains just a single sub child
                    // For example, when double-clicking a link on a single line
                    if ($(currentNode).children().length == 1) {
                        if ($(currentNode).children().first().prop("tagName") == "A") {
                            formats.push("a");
                        }
                    }
                }
            },
            getIcon: function (cmd) {
                //Machinata.debug("notebook: getIcon " + cmd);
                for (var i = 0; i < options.icons.length; i++) {
                    //Machinata.debug("icon " + options.icons[i].cmd);
                    if (options.icons[i].cmd == cmd) {
                        return options.icons[i];
                    }
                }
                return {"text":cmd}; // Fallback
            },
            buildMenu: function (editor, elem) {
                // Create main elems
                var helper = utils.html.addTag(elem, 'div', false, false);
                helper.addClass("helper-text");
                helper.text(options.title);
                var ul = utils.html.addTag(elem, 'ul', false, false);
                // Create all buttons
                for (var cmdi in options.modifiers) {
                    var cmd = options.modifiers[cmdi];
                    // Setup button elem
                    var li = utils.html.addTag(ul, 'li', false, false);
                    var btn = utils.html.addTag(li, 'button', false, false);
                    btn.attr('editor-command', cmd);
                    btn.addClass("cmd-" + cmd);
                    // Setup button icon
                    var icon = bubble.getIcon(cmd);
                    if (icon.text != null) {
                        btn.text(icon.text);
                    } else {
                        btn.addClass("icon");
                        btn.addClass("icon-" + icon.icon);
                        if (icon.icon == "link") {
                            btn.append("{icon.link}");
                        } else if (icon.icon == "image") {
                            btn.append("{icon.picture}");
                        } else if (icon.icon == "document") {
                            btn.append("{icon.document}");
                        } else if (icon.icon == "clipboard") {
                            btn.append("{icon.scroll}");
                        } else if (icon.icon == "th-list") {
                            btn.append("{icon.numbered-list}");
                        } else if (icon.icon == "th-menu") {
                            btn.append("{icon.list}");
                        }
                    }
                    if (icon.helper != null) btn.attr("helper-text", icon.helper);
                    if (icon.style != null) btn.attr("style", icon.style);
                }
                elem.find('button').click(function (e) {
                    e.preventDefault();
                    var cmd = $(this).attr('editor-command');
                    events.commands[cmd].call(editor, e);
                });
                elem.find('button[helper-text]').hover(function () {
                    helper.text($(this).attr("helper-text"));
                }, function () {
                    helper.text(options.title);
                });
                var linkArea = utils.html.addTag(elem, 'div', false, false);
                linkArea.addClass('link-area');
                var linkInput = utils.html.addTag(linkArea, 'input', false, false);
                linkInput.attr({
                    type: 'text'
                });
                var closeBtn = utils.html.addTag(linkArea, 'button', false, false);
                closeBtn.click(function (e) {
                    e.preventDefault();
                    var editor = $(this).closest('.editor');
                    $(this).closest('.link-area').hide();
                    $(this).closest('.bubble').find('ul').show();
                });
            },
            show: function () {
                var tag = $(this).parent().find('.bubble');
                if (!tag.length) {
                    tag = utils.html.addTag($(this).parent(), 'div', false, false);
                    tag.addClass('jquery-notebook bubble');
                }
                tag.empty();
                bubble.buildMenu(this, tag);
                tag.show();
                bubble.updateState(this, tag);
                if (!tag.hasClass('active')) {
                    tag.addClass('jump');
                } else {
                    tag.removeClass('jump');
                }
                bubble.updatePos($(this), tag);
                tag.addClass('active');
            },
            update: function () {
                var tag = $(this).parent().find('.bubble');
                bubble.updateState(this, tag);
            },
            clear: function () {
                var elem = $(this).parent().find('.bubble');
                if (!elem.hasClass('active')) return;
                elem.removeClass('active');
                bubble.hideLinkInput.call(this);
                bubble.showButtons.call(this);
                setTimeout(function () {
                    if (elem.hasClass('active')) return;
                    elem.hide();
                }, 500);
            },
            hideButtons: function () {
                $(this).parent().find('.bubble').find('ul').hide();
            },
            showButtons: function () {
                $(this).parent().find('.bubble').find('ul').show();
            },
            showLinkInput: function (selection) {
                // Init
                var editor = this;
                var currentLink = null;
                // Get the selection anchor element
                var anchor = $(utils.selection.getContainer(selection)).closest('a');
                // Special handling if selection is wrapping the entire link and its single parent (for example: <p><a.../></p>)
                if (anchor.length == 0) {
                    var sel = utils.selection.getSelection(selection);
                    var selNode = $(sel.focusNode);
                    if (selNode.children().length == 1) {
                        if (selNode.children().first().prop("tagName") == "A") {
                            anchor = selNode.children().first();
                        }
                    }
                }
                if (anchor.length > 0) {
                    currentLink = anchor.attr('href');
                }
                Machinata.linkDialog(null, null, currentLink)
                    .button("{text.remove}", "remove")
                    .input(function (val, id) {
                        Machinata.debug("notebook: got link input '" + val + "' for id " + id);
                        if (id == "remove") {
                            // Remove
                            var e = {};
                            events.commands.removeLink(e, selection);
                            bubble.update.call(editor);
                            events.change.call(editor);
                        } else if(val != null) {
                            // Create
                            var e = {};
                            e.url = val;
                            events.commands.createLink(e, selection);
                            bubble.update.call(editor);
                            events.change.call(editor);
                        }
                    })
                    .cancel(function (id) {  })
                    .show();
                // Hide the bubble
                bubble.clear.call(editor);
                /*
                bubble.hideButtons.call(this);
                var editor = this;
                var elem = $(this).parent().find('.bubble').find('input[type=text]');
                var hasLink = elem.closest('.jquery-notebook').find('button.anchor').hasClass('active');
                elem.unbind('keydown');
                elem.keydown(function (e) {
                    var elem = $(this);
                    utils.keyboard.isEnter(e, function () {
                        e.preventDefault();
                        var url = elem.val();
                        if (utils.validation.isUrl(url)) {
                            e.url = url;
                            events.commands.createLink(e, selection);
                            bubble.clear.call(editor);
                        } else if (url === '' && hasLink) {
                            events.commands.removeLink(e, selection);
                            bubble.clear.call(editor);
                        }
                    });
                });
                elem.bind('paste', function (e) {
                    var elem = $(this);
                    setTimeout(function () {
                        var text = elem.val();
                        if (/http:\/\/https?:\/\//.test(text)) {
                            text = text.substring(7);
                            elem.val(text);
                        }
                    }, 1);
                });
                var linkText = 'http://';
                if (hasLink) {
                    var anchor = $(utils.selection.getContainer(selection)).closest('a');
                    linkText = anchor.prop('href') || linkText;
                }
                $(this).parent().find('.link-area').show();
                elem.val(linkText).focus();
                */
            },
            hideLinkInput: function () {
                $(this).parent().find('.bubble').find('.link-area').hide();
            },
            showImageInput: function (selection) {
                // Init
                var editor = this;
                var currentImage = null;
                // Get the selection anchor element
                /*var img = $(utils.selection.getContainer(selection)).closest('img');
                // Special handling if selection is wrapping the entire link and its single parent (for example: <p><a.../></p>)
                if (img.length == 0) {
                    var sel = utils.selection.getSelection(selection);
                    var selNode = $(sel.focusNode);
                    if (selNode.children().length == 1) {
                        if (selNode.children().first().prop("tagName") == "IMG") {
                            img = selNode.children().first();
                        }
                    }
                }
                if (img.length > 0) {
                    currentImage = img.prop('src');
                }*/
                var source = $(editor).attr("data-content-source");
                if (source == null || source == "") source = "ContentNode"; // fallback
                Machinata.fileDialog("{text.choose-a-image=Choose a Image}", source, "image") 
                    //.button("{text.remove}", "remove")
                    .okay(function (val, btn, id) {
                        Machinata.debug("notebook: got image input '" + val + "' for id " + id);
                        if (val == "remove") {
                            // Remove
                            var e = {};
                            events.commands.removeImage(e, selection);
                            bubble.update.call(editor);
                            events.change.call(editor);
                        } else if(val != null) {
                            // Create
                            var e = {};
                            e.url = val;
                            events.commands.insertImage(e, selection);
                            bubble.update.call(editor);
                            events.change.call(editor);
                        }
                    })
                    .show();
                // Hide the bubble
                bubble.clear.call(editor);
            }
        },
        actions = {
            bindEvents: function (elem) {
                elem.keydown(rawEvents.keydown);
                elem.keyup(rawEvents.keyup);
                elem.focus(rawEvents.focus);
                elem.bind('paste', events.paste);
                elem.mousedown(rawEvents.mouseClick);
                elem.mouseup(rawEvents.mouseUp);
                elem.mousemove(rawEvents.mouseMove);
                elem.blur(rawEvents.blur);
                $('body').mouseup(function (e) {
                    if (e.target == e.currentTarget && cache.isSelecting) {
                        rawEvents.mouseUp.call(elem, e);
                    }
                });
            },
            setPlaceholder: function (e) {
                var placeholderText = $(this).attr('editor-placeholder');
                if (placeholderText == "" || placeholderText == null) return;
                if (/^\s*$/.test($(this).text())) {
                    $(this).empty();
                    var placeholder = utils.html.addTag($(this), 'p').addClass('placeholder');
                    placeholder.append(placeholderText);
                    utils.html.addTag($(this), 'p', typeof e.focus != 'undefined' ? e.focus : false, true);
                } else {
                    $(this).find('.placeholder').remove();
                }
            },
            enableObjectResizing: function (e) {
                d.execCommand('enableObjectResizing', false, true);
            },
            disableObjectResizing: function (e) {
                d.execCommand('enableObjectResizing', false, false);
            },
            setInitialP: function (e) {
                var p = utils.html.addTag($(this), 'p', typeof e.focus != 'undefined' ? e.focus : false, true);
                p.html("&nbsp;");
            },
            setDefaultP: function (e) {
                d.execCommand("DefaultParagraphSeparator", false, "p");
            },
            removePlaceholder: function (e) {
                var placeholderText = $(this).attr('editor-placeholder');
                if (placeholderText == "" || placeholderText == null) return;
                $(this).find('.placeholder').remove();
            },
            preserveElementFocus: function () {
                var anchorNode = w.getSelection() ? w.getSelection().anchorNode : d.activeElement;
                if (anchorNode) {
                    var current = anchorNode.parentNode,
                        diff = current !== cache.focusedElement,
                        children = this.children,
                        elementIndex = 0;
                    if (current === this) {
                        current = anchorNode;
                    }
                    for (var i = 0; i < children.length; i++) {
                        if (current === children[i]) {
                            elementIndex = i;
                            break;
                        }
                    }
                    if (diff) {
                        cache.focusedElement = current;
                        cache.focusedElementIndex = elementIndex;
                    }
                }
            },
            setContentArea: function (elem) {
                var id = $('body').find('.jquery-notebook.editor').length + 1;
                elem.attr('data-jquery-notebook-id', id);
                var body = $('body');
                var contentArea = $('<textarea class="jquery-notebook-content"></textarea>');
                contentArea.css({
                    position: 'absolute',
                    left: -1000
                });
                contentArea.val(elem.html());
                contentArea.attr('id', 'jquery-notebook-content-' + id);
                body.append(contentArea);
            },
            prepare: function (elem, customOptions) {
                options = customOptions;
                actions.setContentArea(elem);
                elem.attr('editor-mode', options.mode);
                elem.attr('editor-placeholder', options.placeholder);
                elem.attr('contenteditable', true);
                elem.css('position', 'relative');
                elem.addClass('jquery-notebook editor');
                if (elem.html() == "") actions.setInitialP.call(elem, {});
                actions.setDefaultP.call(elem, {});
                actions.setPlaceholder.call(elem, {});
                actions.preserveElementFocus.call(elem);
                actions.disableObjectResizing.call(elem);
                if (options.autoFocus === true) {
                    var firstP = elem.find('p:not(.placeholder)');
                    utils.cursor.set(elem, 0, firstP);
                }
            }
        },
        rawEvents = {
            keydown: function (e) {
                var elem = this;
                if (cache.command && e.which === 65) {
                    setTimeout(function () {
                        bubble.show.call(elem);
                    }, 50);
                }
                utils.keyboard.isCommand(e, function () {
                    cache.command = true;
                }, function () {
                    cache.command = false;
                });
                utils.keyboard.isShift(e, function () {
                    cache.shift = true;
                }, function () {
                    cache.shift = false;
                });
                utils.keyboard.isModifier.call(this, e, function (modifier) {
                    if (cache.command) {
                        events.commands[modifier].call(this, e);
                    }
                });

                if (cache.shift) {
                    utils.keyboard.isArrow.call(this, e, function () {
                        setTimeout(function () {
                            var txt = utils.selection.getText();
                            if (txt !== '') {
                                bubble.show.call(elem);
                            } else {
                                bubble.clear.call(elem);
                            }
                        }, 100);
                    });
                } else {
                    utils.keyboard.isArrow.call(this, e, function () {
                        bubble.clear.call(elem);
                    });
                }

                if (e.which === 13) {
                    events.enterKey.call(this, e);
                }
                if (e.which === 27) {
                    bubble.clear.call(this);
                }
                if (e.which === 86 && cache.command) {
                    //events.paste.call(this, e);
                }
                if (e.which === 90 && cache.command) {
                    events.commands.undo.call(this, e);
                }
            },
            keyup: function (e) {
                utils.keyboard.isCommand(e, function () {
                    cache.command = false;
                }, function () {
                    cache.command = true;
                });
                actions.preserveElementFocus.call(this);
                actions.removePlaceholder.call(this);

                /*
                 * This breaks the undo when the whole text is deleted but so far
                 * it is the only way that I fould to solve the more serious bug
                 * that the editor was losing the p elements after deleting the whole text
                 */
                //console.log($(this).html());
                //if ($(this).html() == "" || $(this).html() == "<br>") {
                //    utils.html.addTag($(this), 'p', true, true);
                //}
                //if (/^\s*$/.test($(this).text())) {
                //    $(this).empty();
                //    utils.html.addTag($(this), 'p', true, true);
                //}
                events.change.call(this);
            },
            focus: function (e) {
                cache.command = false;
                cache.shift = false;
            },
            mouseClick: function (e) {
                var elem = this;
                cache.isSelecting = true;
                if ($(this).parent().find('.bubble:visible').length) {
                    var bubbleTag = $(this).parent().find('.bubble:visible'),
                        bubbleX = bubbleTag.offset().left,
                        bubbleY = bubbleTag.offset().top,
                        bubbleWidth = bubbleTag.width(),
                        bubbleHeight = bubbleTag.height();
                    if (mouseX > bubbleX && mouseX < bubbleX + bubbleWidth &&
                        mouseY > bubbleY && mouseY < bubbleY + bubbleHeight) {
                        return;
                    }
                }
            },
            mouseUp: function (e) {
                var elem = this;
                cache.isSelecting = false;
                setTimeout(function () {
                    var s = utils.selection.save();
                    if (s) {
                        if (s.collapsed) {
                            bubble.clear.call(elem);
                        } else {
                            bubble.show.call(elem);
                            e.preventDefault();
                        }
                    }
                }, 50);
            },
            mouseMove: function (e) {
                mouseX = e.pageX;
                mouseY = e.pageY;
            },
            blur: function (e) {
                actions.setPlaceholder.call(this, {
                    focus: false
                });
            }
        },
        events = {
            // For execCommand see https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand
            commands: {
                bold: function (e) {
                    e.preventDefault();
                    d.execCommand('bold', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                italic: function (e) {
                    e.preventDefault();
                    d.execCommand('italic', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                underline: function (e) {
                    e.preventDefault();
                    d.execCommand('underline', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                strike: function (e) {
                    e.preventDefault();
                    d.execCommand('strikeThrough', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                anchor: function (e) {
                    e.preventDefault();
                    var s = utils.selection.save();
                    bubble.showLinkInput.call(this, s);
                    //events.change.call(this);
                },
                createLink: function (e, s) {
                    Machinata.debug("notebook: creating link '" + e.url + "' for " + s);
                    utils.selection.restore(s);
                    d.execCommand('createLink', false, e.url);
                },
                removeLink: function (e, s) {
                    var el = $(utils.selection.getContainer(s)).closest('a');
                    el.contents().first().unwrap();
                },
                image: function (e) {
                    e.preventDefault();
                    var s = utils.selection.save();
                    bubble.showImageInput.call(this, s);
                },
                insertImage: function (e, s) {
                    Machinata.debug("notebook: creating image '" + e.url + "' for " + s);
                    utils.selection.restore(s);
                    d.execCommand('insertImage', false, e.url);
                },
                removeImage: function (e, s) {
                    var el = $(utils.selection.getContainer(s)).closest('img');
                    el.contents().first().unwrap();
                },
                h1: function (e) {
                    e.preventDefault();
                    if ($(window.getSelection().anchorNode.parentNode).is('h1')) {
                        d.execCommand('formatBlock', false, '<p>');
                    } else {
                        d.execCommand('removeFormat', false);
                        d.execCommand('formatBlock', false, '<h1>');
                    }
                    bubble.update.call(this);
                    events.change.call(this);
                },
                h2: function (e) {
                    e.preventDefault();
                    if ($(window.getSelection().anchorNode.parentNode).is('h2')) {
                        d.execCommand('formatBlock', false, '<p>');
                    } else {
                        d.execCommand('removeFormat', false);
                        d.execCommand('formatBlock', false, '<h2>');
                    }
                    bubble.update.call(this);
                    events.change.call(this);
                },
                p: function (e) {
                    e.preventDefault();
                    if ($(window.getSelection().anchorNode.parentNode).is('p')) {
                        d.execCommand('removeFormat', false);
                    } else {
                        d.execCommand('removeFormat', false);
                        d.execCommand('formatBlock', false, '<p>');
                    }
                    bubble.update.call(this);
                    events.change.call(this);
                },
                ipsum: function (e) {
                    e.preventDefault();
                    var dummyText = [];
                    dummyText.push("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam imperdiet ultricies molestie. Sed at urna quis ligula pretium dignissim. Maecenas ipsum diam, bibendum ut scelerisque eu, ornare vel augue. Duis at sem vitae nunc tempus vehicula quis at enim. Aliquam pharetra lectus sit amet magna lobortis egestas. Nunc consequat feugiat ornare. Aliquam et risus augue.");
                    dummyText.push("Lorem ipsum nam sit amet lacinia turpis. Quisque aliquet, elit sit amet maximus feugiat, orci justo ullamcorper justo, eu dapibus ligula sapien quis libero. Duis massa ex, sollicitudin id commodo sit amet, consectetur id lacus. Vivamus rutrum maximus nunc, ac vulputate felis vehicula in. Suspendisse blandit eu elit at dapibus. Mauris sem dui, efficitur eu velit vel, pulvinar hendrerit sapien. Sed volutpat leo vitae neque bibendum tempus non eget mi. Mauris scelerisque imperdiet odio eget elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed tristique iaculis mi, non pharetra lacus sodales a.");
                    dummyText.push("Lorem ipsum maecenas malesuada, massa sed fermentum accumsan, mi lacus tempus eros, nec ullamcorper erat est nec nibh. Nam sagittis, augue at ullamcorper dignissim, purus urna maximus quam, nec gravida ante mi non velit. Curabitur tincidunt odio arcu, eget aliquam est finibus in. Sed nec nulla eget metus tempus lobortis quis sit amet diam. In faucibus, nibh non mollis interdum, massa ipsum pretium diam, non aliquet risus sem sed risus. Quisque et metus a quam mattis tincidunt. Aliquam scelerisque nisi sit amet ipsum consectetur, eget varius magna venenatis. Quisque hendrerit scelerisque lorem ut efficitur.");
                    dummyText.push("Lorem ipsum nulla vulputate, tellus nec semper aliquam, purus ligula fermentum magna, eu viverra massa diam imperdiet tellus. Aliquam erat volutpat. Phasellus eu porta mauris, at malesuada metus. Mauris finibus enim nisl. Praesent faucibus vestibulum metus ac ullamcorper. Suspendisse vel finibus neque. Curabitur turpis elit, sollicitudin eu tortor non, euismod ultrices sem. Vestibulum elementum erat vitae egestas malesuada. Curabitur et placerat magna, a sollicitudin tortor.");
                    dummyText.push("Lorem ipsum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse a ligula eu nisl vehicula vestibulum. ");
                    dummyText.push("Lorem ipsum maecenas consectetur, massa sit amet pellentesque egestas, augue lectus suscipit dolor, at placerat ipsum ligula vel dui. Quisque eget molestie dolor. ");
                    dummyText.push("Lorem ipsum cras vitae feugiat orci. Praesent in pretium purus. Nullam ullamcorper est eget placerat semper. Morbi condimentum laoreet tempus. ");
                    var text = dummyText[Machinata.Math.rnd(0, dummyText.length)];
                    d.execCommand('insertText', false, text);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                ul: function (e) {
                    e.preventDefault();
                    d.execCommand('insertUnorderedList', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                ol: function (e) {
                    e.preventDefault();
                    d.execCommand('insertOrderedList', false);
                    bubble.update.call(this);
                    events.change.call(this);
                },
                undo: function (e) {
                    e.preventDefault();
                    d.execCommand('undo', false);
                    var sel = w.getSelection(),
                        range = sel.getRangeAt(0),
                        boundary = range.getBoundingClientRect();
                    $(document).scrollTop($(document).scrollTop() + boundary.top);
                    events.change.call(this);
                }
            },
            enterKey: function (e) {
                return; //dankrusi: the default browser behaviour seems to be much more reliable

                if ($(this).attr('editor-mode') === 'inline') {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }

                var sel = utils.selection.getSelection();
                var elem = $(sel.focusNode.parentElement);
                var nextElem = elem.next();
                if (!nextElem.length && elem.prop('tagName') != 'LI') {
                    var tagName = elem.prop('tagName');
                    if (tagName === 'OL' || tagName === 'UL') {
                        var lastLi = elem.children().last();
                        if (lastLi.length && lastLi.text() === '') {
                            lastLi.remove();
                        }
                    }
                    utils.html.addTag($(this), 'p', true, true);
                    e.preventDefault();
                    e.stopPropagation();
                }
                events.change.call(this);
            },
            paste: function (e) {

                // Which mode?
                // Can be "notebook", "rich", "plain"
                var pasteMode = "plain"; 

                if (pasteMode == "plain") {

                    // Plain-text mode

                    //TODO: @dan Do we need to make sure to wrap the content in a p?

                    // Get text representation of clipboard
                    // https://stackoverflow.com/questions/12027137/javascript-trick-for-paste-as-plain-text-in-execcommand
                    e.preventDefault();
                    var text = '';
                    if (e.clipboardData || e.originalEvent.clipboardData) {
                        text = (e.originalEvent || e).clipboardData.getData('text/plain');
                    } else if (window.clipboardData) {
                        text = window.clipboardData.getData('Text');
                    } else {
                        console.log("warning: could not get clipboard data");
                    }
                    if (d.queryCommandSupported('insertText')) {
                        d.execCommand('insertText', false, text);
                    } else {
                        d.execCommand('paste', false, text);
                    }
                    events.change.call(this);

                } else if (pasteMode == "rich") {

                    // Rich mode
                    d.execCommand('paste');

                } else if (pasteMode == "notebook") {
                    
                    // notebook mode
                    // Note: very buggy

                    var elem = $(this),
                        id = 'jqeditor-temparea',
                        range = utils.selection.save(),
                        tempArea = $('#' + id);
                    if (tempArea.length < 1) {
                        var body = $('body');
                        tempArea = $('<textarea></textarea>');
                        tempArea.css({
                            position: 'absolute',
                            left: -1000
                        });
                        tempArea.attr('id', id);
                        body.append(tempArea);
                    }
                    tempArea.focus();

                    setTimeout(function () {
                        var clipboardContent = '',
                            paragraphs = tempArea.val().split('\n');
                        for (var i = 0; i < paragraphs.length; i++) {
                            clipboardContent += ['<p>', paragraphs[i], '</p>'].join('');
                        }
                        tempArea.val('');
                        utils.selection.restore(range);
                        d.execCommand('delete');
                        d.execCommand('insertHTML', false, clipboardContent);
                        events.change.call(this);
                    }, 500);
                }
            },
            change: function (e) {
                Machinata.debug("Notebook: change");
                var contentArea = $('#jquery-notebook-content-' + $(this).attr('data-jquery-notebook-id'));
                if(contentArea.length < 1) Machinata.debug("Notebook: warning change has not content area");
                contentArea.val($(this).html());
                var content = contentArea.val();
                var changeEvent = new CustomEvent('contentChange', { 'detail': { 'content': content } });
                this.dispatchEvent(changeEvent);
            }
        };

    $.fn.notebook = function (options) {
        options = $.extend({}, $.fn.notebook.defaults, options);
        actions.prepare(this, options);
        actions.bindEvents(this);
        return this;
    };

    $.fn.notebook.defaults = {
        autoFocus: false,
        placeholder: '{text.notebook-placeholder}',
        title: '{text.notebook-title}',
        mode: 'multiline',
        modifiers: ['h1', 'h2', 'p', 'sep', 'bold', 'italic', 'underline', 'strike', 'sep', 'anchor', 'image', 'file', 'ipsum', 'ol', 'ul'], //dankrusi: removed table
        icons: [
            { cmd: 'sep', icon: null, text: '|', helper: null, style: null },
            { cmd: 'bold', icon: null, text: 'b', helper: '{text.notebook-bold}', style: 'font-weight:bold;' },
            { cmd: 'italic', icon: null, text: 'i', helper: '{text.notebook-italic}', style: 'font-style:italic;font-weight:bold;' },
            { cmd: 'underline', icon: null, text: 'u', helper: '{text.notebook-underline}', style: 'text-decoration:underline;font-weight:bold;' },
            { cmd: 'strike', icon: null, text: 's', helper: '{text.notebook-strike}', style: 'text-decoration:line-through;font-weight:bold;' },
            { cmd: 'p', icon: null, text: "p", helper: '{text.notebook-p}', style: 'font-weight:nomrmal;' },
            { cmd: 'h1', icon: null, text: "T", helper: '{text.notebook-h1}', style: 'font-weight:bold;' },
            { cmd: 'h2', icon: null, text: "t", helper: '{text.notebook-h2}', style: 'font-weight:bold;' },
            { cmd: 'ol', icon: 'th-list', text: null, helper: '{text.notebook-ol}', style: null },
            { cmd: 'ul', icon: 'th-menu', text: null, helper: '{text.notebook-ul}', style: null },
            { cmd: 'anchor', icon: 'link', text: null, helper: '{text.notebook-link}', style: null },
            { cmd: 'table', icon: 'th-small', text: null, helper: '{text.notebook-table}', style: null },
            { cmd: 'image', icon: 'image', text: null, helper: '{text.notebook-image}', style: null },
            { cmd: 'file', icon: 'document', text: null, helper: '{text.notebook-file}', style: null },
            { cmd: 'ipsum', icon: 'clipboard', text: null, helper: '{text.notebook-ipsum}', style: null },
        ]
    };

})(jQuery, document, window);
