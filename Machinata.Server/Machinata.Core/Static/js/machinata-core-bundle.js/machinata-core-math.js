
/// <summary>
/// Collection of common math routines.
/// </summary>
/// <type>namespace</type>
Machinata.Math = {};



/// <summary>
///
/// </summary>
Machinata.Math.rnd = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
};

/// <summary>
/// Returns true randomly for a probability p, where 1.0 being always true, and 0.0 never true, 0.5 half the time true
/// </summary>
Machinata.Math.prob = function (p) {
    return Math.random() <= p;
};

/// <summary>
/// Each subsequent call to the return function of xmur3 produces a new "random" 32-bit hash value to be used as a seed in a PRNG. 
/// Based on MurmurHash3's mixing function
/// </summary>
Machinata.Math.xmur3 = function(str) {
    for (var i = 0, h = 1779033703 ^ str.length; i < str.length; i++)
        h = Math.imul(h ^ str.charCodeAt(i), 3432918353),
            h = h << 13 | h >>> 19;
    return function () {
        h = Math.imul(h ^ h >>> 16, 2246822507);
        h = Math.imul(h ^ h >>> 13, 3266489909);
        return (h ^= h >>> 16) >>> 0;
    }
}

/// <summary>
/// sfc32 is part of the PractRand random number testing suite (which it passes of course). sfc32 has a 128-bit state and is very fast in JS.
/// </summary>
Machinata.Math.sfc32 = function (a, b, c, d) {
    // See https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
    return function () {
        a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0;
        var t = (a + b) | 0;
        a = b ^ b >>> 9;
        b = c + (c << 3) | 0;
        c = (c << 21 | c >>> 11);
        d = d + 1 | 0;
        t = t + d | 0;
        c = c + t | 0;
        return (t >>> 0) / 4294967296;
    }
}

/// <summary>
/// Mulberry32 is a simple generator with a 32-bit state, but is extremely fast and has good quality (author states it passes all tests of gjrand testing suite and has a full 232 period, but I haven't verified).
/// </summary>
Machinata.Math.mulberry32 = function (a) {
    // See https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
    return function () {
        var t = a += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
}

/// <summary>
///
/// </summary>
Machinata.Math.rndWithSeed = function (seed, min, max) {

    var seed = Machinata.Math.xmur3(""+seed+""); // ensures seed is a string
    var randomGenerator = Machinata.Math.sfc32(seed(), seed(), seed(), seed());
    var randomNumber = randomGenerator();

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(randomNumber * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive

    // Old method: does not integrate nicely because domain is unknown
    /*var mask = 0xffffffff;
    var m_w = (123456789 + seed) & mask;
    var m_z = (987654321 - seed) & mask;

    function getSeededRandom() {
        m_z = (36969 * (m_z & 65535) + (m_z >>> 16)) & mask;
        m_w = (18000 * (m_w & 65535) + (m_w >>> 16)) & mask;

        var result = ((m_z << 16) + (m_w & 65535)) >>> 0;
        result /= 4294967296;
        return result;
    }
    console.log(getSeededRandom());
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(getSeededRandom() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    */
};

/// <summary>
///
/// </summary>
Machinata.Math.isEven = function (n) {
    return n % 2 == 0;
};

/// <summary>
///
/// </summary>
Machinata.Math.isOdd = function (n) {
    return Math.abs(n % 2) == 1;
};

/// <summary>
///
/// </summary>
Machinata.Math.makeEven = function (n) {
    n = Math.floor(n);
    if (Machinata.Math.isEven(n)) return n;
    return n - 1;
};

/// <summary>
///
/// </summary>
Machinata.Math.degToRad = function (degrees) {
    return Math.PI / 180 * degrees;
}
Machinata.Math.rad2Deg = function (rad) {
    return ((rad + Math.PI) / (2 * Math.PI)) * 360;
}

/// <summary>
///
/// </summary>
Machinata.Math.min3 = function (a, b, c) {
    return Math.min(a, b, c);
}
/// <summary>
///
/// </summary>
Machinata.Math.min4 = function (a, b, c, d) {
    return Math.min(a, b, c, d);
}

/// <summary>
///
/// </summary>
Machinata.Math.max3 = function (a, b, c) {
    return Math.max(a, b, c);
}

/// <summary>
///
/// </summary>
Machinata.Math.max4 = function (a, b, c, d) {
    return Math.max(a, b, c, d);
}

/// <summary>
///
/// </summary>
Machinata.Math.vec2Dist = function (x1, y1, x2, y2) {
    var a = x1 - x2;
    var b = y1 - y2;
    var c = Math.sqrt(a * a + b * b);
    return c;
}

/// <summary>
///
/// </summary>
Machinata.Math.xy2Polar = function (x, y) {
    var r = Math.sqrt(x * x + y * y);
    var phi = Math.atan2(y, x);
    return [r, phi];
}

/// <summary>
///
/// </summary>
Machinata.Math.roundUpToNearest = function (number, fraction) {
    var divisor = 1.0 / fraction;
    return (Math.ceil(number * divisor) / divisor).toFixed(2);
};

/// <summary>
///
/// </summary>
Machinata.Math.roundDownToNearest = function (number, fraction) {
    var divisor = 1.0 / fraction;
    return (Math.floor(number * divisor) / divisor).toFixed(2);
};


/// <summary>
///
/// </summary>
Machinata.Math.rbgInvert = function (rgb) {
    return [255 - rgb[0], 255 - rgb[1], 255 - rgb[2]];
};

/// <summary>
///
/// </summary>
Machinata.Math.rbgBrightness = function (rgb) {
    var r = rgb[0] / 255;
    var g = rgb[1] / 255;
    var b = rgb[2] / 255;
    var p = (r + g + b) / 3.0
    return p;
};

/// <summary>
///
/// </summary>
Machinata.Math.rbgGrayscale = function (rgb) {
    // Y = 0.2126 * R + 0.7152 * G + 0.0722 * B
    // https://en.wikipedia.org/wiki/Grayscale
    var y = 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2];
    return [y, y, y];
};

/// <summary>
///
/// </summary>
Machinata.Math.hexToRBG = function (hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
};

/// <summary>
///
/// </summary>
Machinata.Math.rgbToHex = function (rgb) {
    return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
};

/// <summary>
///
/// </summary>
// hue in range [0, 360]
// saturation, value in range [0,1]
// return [r,g,b] each in range [0,255]
// See: https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV
Machinata.Math.hsv2RGB = function (hue, saturation, value, roundValues) {
    var chroma = value * saturation;
    var hue1 = hue / 60;
    var x = chroma * (1 - Math.abs((hue1 % 2) - 1));
    var r1, g1, b1;
    if (hue1 >= 0 && hue1 <= 1) {
        r1 = chroma;
        g1 = x;
        b1 = 0;
    } else if (hue1 >= 1 && hue1 <= 2) {
        r1 = x;
        g1 = chroma;
        b1 = 0;
    } else if (hue1 >= 2 && hue1 <= 3) {
        r1 = 0;
        g1 = chroma;
        b1 = x;
    } else if (hue1 >= 3 && hue1 <= 4) {
        r1 = 0;
        g1 = x;
        b1 = chroma;
    } else if (hue1 >= 4 && hue1 <= 5) {
        r1 = x;
        g1 = 0;
        b1 = chroma;
    } else if (hue1 >= 5 && hue1 <= 6) {
        r1 = chroma;
        g1 = 0;
        b1 = x;
    }

    var m = value - chroma;
    var r = r1 + m;
    var g = g1 + m;
    var b = b1 + m;

    // Change r,g,b values from [0,1] to [0,255]
    if (roundValues == true) {
        return [Math.round(255 * r), Math.round(255 * g), Math.round(255 * b)];
    } else {
        return [255 * r, 255 * g, 255 * b];
    }
};


/// <summary>
///
/// </summary>
Machinata.Math.map = function (val, inputMin, inputMax, outputMin, outputMax) {
    return (val - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
};

/// <summary>
///
/// </summary>
Machinata.Math.clamp = function (val, outputMin, outputMax) {
    return Math.min(Math.max(val, outputMin), outputMax);
};

/// <summary>
///
/// </summary>
Machinata.Math.inchesToPixels = function (inches, dpi) {
    if (dpi == null) dpi = 96;
    var px = dpi * inches;
    return px;
};

/// <summary>
///
/// </summary>
Machinata.Math.pixelsToInches = function (px, dpi) {
    if (dpi == null) dpi = 96;
    var inches = px / dpi;
    return inches;
};

/// <summary>
///
/// </summary>
Machinata.Math.millimetersToInches = function (mm) {
    var inches = mm * 0.0393701;
    return inches;
};

/// <summary>
///
/// </summary>
Machinata.Math.inchesToMillimeters = function (inches) {
    var mm = inches / 0.0393701;
    return mm;
};

/// <summary>
///
/// </summary>
Machinata.Math.millimetersToPixels = function (mm, dpi) {
    var inches = Machinata.Math.millimetersToInches(mm);
    return Machinata.Math.inchesToPixels(inches,dpi);
};

/// <summary>
///
/// </summary>
Machinata.Math.pixelsToMillimeters = function (px, dpi) {
    var inches = Machinata.Math.pixelsToInches(px, dpi);
    return Machinata.Math.inchesToMillimeters(inches);
};