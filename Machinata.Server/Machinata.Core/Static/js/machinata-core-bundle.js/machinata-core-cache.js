


/// <summary>
/// Extensions for automatically caching requests or objects.
/// </summary>


/// <summary>
/// </summary>
Machinata.CACHE_ENABLED = true;


/// <summary>
/// </summary>
Machinata.CACHE_DEBUG_CONSOLE = false;

/// <summary>
/// </summary>
Machinata.resetCache = function (prefix) {
    if (!window.localStorage) return null;
    // Use Web Storage API
    if (prefix == null) {
        // All
        return localStorage.clear();
    } else {
        // Only those with the prefix
        var keysToRemove = [];
        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            if (key.startsWith(prefix)) keysToRemove.push(key);
        }
        for (var i = 0; i < keysToRemove.length; i++) {
            var key = keysToRemove[i];
            localStorage.removeItem(key);
        }
    }
};

/// <summary>
/// Gets a value from the cache using a key.
/// </summary>
Machinata.getCache = function (key) {
    if (!window.localStorage) return null;
    // Use Web Storage API
    return JSON.parse(localStorage.getItem(key));
};

/// <summary>
/// Sets a value in the cache using the key.
/// </summary>
Machinata.setCache = function (key, value) {
    if (!window.localStorage) return null;
    // Use Web Storage API
    return localStorage.setItem(key, JSON.stringify(value));
};

/// <summary>
/// Helper method to automatically either get the cached value by key (if it exists),
/// or generate and set the cache value (if it does not exist).
/// Example:
/// Machinata.getOrSetCacheWithHashedKey("GEOCODE", search, function (setCacheValue) {
///     // Generator
///     var val = calculateSomeComplicatedValue();
///     setCacheValue(val);
/// }, function (val) {
///     // Callback
///     val.doSomething();
/// });
/// </summary>
Machinata.getOrSetCache = function (key, generator, callback) {
    // Use Web Storage API
    if (Machinata.CACHE_DEBUG_CONSOLE) Machinata.debug("Getting cache with key '" + key + "'");
    var ret = Machinata.getCache(key);
    if (ret == null) {
        if (Machinata.CACHE_DEBUG_CONSOLE) Machinata.debug("  Cache-miss! Will generate...");
        generator(function (val) {
            if (Machinata.CACHE_DEBUG_CONSOLE) Machinata.debug("  Got generated value: " + val);
            Machinata.setCache(key, val);
            callback(val);
        });
    } else {
        if (Machinata.CACHE_DEBUG_CONSOLE) Machinata.debug("  Cache-hit!");
        if (Machinata.CACHE_DEBUG_CONSOLE) Machinata.debug("  Got value: " + ret);
        callback(ret);
    }
};

/// <summary>
/// </summary>
Machinata.getOrSetCacheWithHashedKey = function (type, key, generator, callback) {
    Machinata.getOrSetCache(type + "_" + key.hashCode(), generator, callback);
};