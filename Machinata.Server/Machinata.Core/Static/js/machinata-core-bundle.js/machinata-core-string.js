

/// <summary>
/// Useful string-related functions.
/// Many of the functions align with the Machinata Server Core String functions.
/// </summary>
/// <type>namespace</type>
Machinata.String = {};

/// <summary>
/// 
/// </summary>
Machinata.String.createShortURL = function (text) {
    //TODO special characters

    // Lower case
    text = text.toLowerCase();
    // Remove double spaces
    text = text.replace(/\s\s+/g, ' ');
    // Remove spaces
    text = text.replace(/\s/g, '-');
    // Remove slashes
    text = text.replace(/\//g, '-');
    text = text.replace(/\\/g, '-');
    // Remove all special characters
    text = text.replace(/[^a-zA-Z0-9]/g, '-');

    return text.trim();
};

/// <summary>
/// 
/// </summary>
Machinata.String.createSummarizedText = function (text, length, includeTrail, breakAtWordIfPossible) {

    //Note: ported from Core.Util.String (CS)

    if (length == null) length = 60;
    if (includeTrail == null) includeTrail = true;
    if (breakAtWordIfPossible == null) breakAtWordIfPossible = true;

    // Remove special characters
    //TODO: PORT: 
    //if (!allowSpecialCharacters) {
    //    text = ReplaceUmlauts(text);
    //    text = RemoveDiacritics(text);
    //    text = System.Text.RegularExpressions.Regex.Replace(text, @"[^A-Za-z0-9 -]+", " ");
    //}
    // Remove double spaces
    text = text.replace(/\s\s+/g, ' ');
    // Trim to length, if longer
    if (text.length > length) {
        var trimLength = length;
        if(breakAtWordIfPossible == true) {
            // Try to find a length to cutoff at a word
            var tolerance = 12;
            for(var i = 0; i <= tolerance; i++) {
                var c1 = length - i;
                var c2 = length + i;
                if(c1 > 0 && c1 < text.length && text[c1] == ' ') {
                    trimLength = c1;
                    break;
                }
                if(c2 > 0 && c2 < text.length && text[c2] == ' ') {
                    trimLength = c2;
                    break;
                }
            }
        }
        // Cut strimg
        text = text.substring(0, trimLength);
        // Add trail
        if (includeTrail == true) {
            //TODO: PORT: text = text.trim('.');
            text += "...";
        }
    }
    return text.trim();
};



Machinata.String.isHTML = function (str) {
    var a = document.createElement('div');
    a.innerHTML = str;
    for (var c = a.childNodes, i = c.length; i--;) {
        if (c[i].nodeType == 1) return true;
    }
    return false;
};