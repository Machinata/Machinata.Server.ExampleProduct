

/// <summary>
/// Collection of utilities for displaying, sizing and playing HTML5 videos.
/// </summary>
/// <type>namespace</type>
Machinata.Video = {};

/// <summary>
/// 
/// </summary>
Machinata.Video.WATCHDOG_TIMEOUT_MS = 5000;

/// <summary>
/// 
/// </summary>
Machinata.Video.parseVideoSource = function (src) {
    if (src == null) return null;
    if (src == "") return null;
    if (src.startsWith("{") && src.endsWith("}")) return null;
    return src;
};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.UI.onUIBind(function (elements) {
    var videoElements = elements.find(".bb-video");
    if (Machinata.DebugEnabled == true) {
        Machinata.debug("Machinata.UI.onUIBind: video");
        Machinata.debug("  Found " + videoElements.length + " video elements");
    }
    videoElements.addClass("bb-ui-bound").each(function () {
        // Init
        var elem = $(this);
        var src = elem.attr("src");
        if (src == null) src = elem.attr("data-video-source");
        var srcCover = elem.attr("data-video-cover");
        var srcGIF = elem.attr("data-video-gif");
        var srcMP4 = elem.attr("data-video-mp4");
        var srcOGV = elem.attr("data-video-ogv");
        var srcWEBM = elem.attr("data-video-webm");
        var ar = elem.attr("data-video-aspectratio");
        var autoplay = elem.attr("data-video-autoplay");
        var controls = elem.attr("data-video-controls");
        var loop = elem.attr("data-video-loop");
        var muted = elem.attr("data-video-muted");
        var watchdog = elem.attr("data-video-watchdog"); // if not 'false', we assume a watchdog
        // Detect src automatically
        if (src.endsWith(".gif") && srcGIF == null) srcGIF = src;
        else if (src.endsWith(".mp4") && srcMP4 == null) srcMP4 = src;
        else if (src.endsWith(".ogv") && srcOGV == null) srcOGV = src;
        else if (src.endsWith(".webm") && srcWEBM == null) srcWEBM = src;
        if (src.endsWith(".png") && srcCover == null) srcCover = src;
        else if (src.endsWith(".jpg") && srcCover == null) srcCover = src;
        else if (src.endsWith(".gif") && srcCover == null) srcCover = src;
        // Nullize
        srcGIF = Machinata.Video.parseVideoSource(srcGIF);
        srcMP4 = Machinata.Video.parseVideoSource(srcMP4);
        srcOGV = Machinata.Video.parseVideoSource(srcOGV);
        srcWEBM = Machinata.Video.parseVideoSource(srcWEBM);
        srcCover = Machinata.Video.parseVideoSource(srcCover);
        // Video file?
        var hasVideoFile = false;
        if (srcMP4 != null) hasVideoFile = true;
        if (srcOGV != null) hasVideoFile = true;
        if (srcWEBM != null) hasVideoFile = true;
        // Debug
        if (Machinata.DebugEnabled == true) {
            Machinata.debug("Machinata.Video: Found new video");
            Machinata.debug("  src:" + src);
            Machinata.debug("  srcMP4:" + srcMP4);
            Machinata.debug("  srcOGV:" + srcOGV);
            Machinata.debug("  srcWEBM:" + srcWEBM);
            Machinata.debug("  srcGIF:" + srcGIF);
            Machinata.debug("  srcCover:" + srcCover);
            Machinata.debug("  ar:" + ar);
            Machinata.debug("  autoplay:" + autoplay);
            Machinata.debug("  hasVideoFile:" + hasVideoFile);
        }
        // Disable autoplay?
        if ($("body").hasClass("bb-video-no-autoplay-mobile") && ($("body").hasClass("mobile") || $("body").hasClass("tablet"))) {
            autoplay = "false";
            controls = "true";
        }
        if ($("body").hasClass("bb-video-show-controls")) {
            controls = "true";
        }
        // AR fallback
        if (ar == null) ar = elem.width() / elem.height();
        else ar = parseFloat(ar);
        // Strategy
        if (hasVideoFile) {
            // Create new video elem
            var videoAttribs = "";
            if (autoplay == "true") videoAttribs += " autoplay";
            if (controls == "true") videoAttribs += " controls";
            if (loop == "true") videoAttribs += " loop";
            if (muted == "true") videoAttribs += " muted";
            if (srcCover != null) videoAttribs += " poster=\"" + srcCover + "\"";
            videoAttribs += " preload=\"metadata\"";
            videoAttribs += " playsinline";
            videoAttribs += " webkit-playsinline";
            // Elem
            var videoHTML = "<video " + videoAttribs + ">";
            if (srcWEBM != null) videoHTML += "<source src=\""+srcWEBM+"\" type='video/webm'/>";
            if (srcMP4 != null) videoHTML += "<source src=\""+srcMP4+"\" type='video/mp4'/>";
            if (srcOGV != null) videoHTML += "<source src=\""+srcOGV+"\" type='video/ogg'/>";
            videoHTML += "</video>";
            var videoElem = $(videoHTML);
            // Bind events
            videoElem.on("play", function () {
                $(this).data("video-loaded", true);
                $(this).data("video-playing", true);
            });
            // Insert into DOM
            elem.hide();
            elem.parent().addClass("contains-video-element");
            elem.parent().append(videoElem);
            // Special helper for android
            //if (Machinata.Device.isAndroid()) {
            //}
            // Create watchdog
            // If the video doesnt start auto-playing, we add controls as a fallback
            // so the user can play the video
            if (autoplay == "true" && watchdog != "false") {
                setTimeout(function () {
                    if (videoElem.data("video-loaded") != true) {
                        Machinata.debug("bb-video: watchdog fired, will add controls as fallback");
                        videoElem.attr("controls", "controls");
                    }
                }, Machinata.Video.WATCHDOG_TIMEOUT_MS);
            }
        } else {
            // Cover/gif fallback
        }
    });
    
});
