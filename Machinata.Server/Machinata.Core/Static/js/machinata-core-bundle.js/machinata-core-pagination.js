

/// <summary>
/// Library for automatically paginating and creating forever-scrollable content.
/// </summary>
/// <type>namespace</type>
Machinata.Pagination = {};

/// <summary>
/// 
/// </summary>
Machinata.Pagination.elements = null;

/// <summary>
/// 
/// </summary>
Machinata.Pagination.proximityElements = null;

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    
    $(".bb-pagination").each(function () {
        // Init
        var elem = $(this);
        // Set settings
        elem.data("pagination-url", elem.attr("data-pagination-url"));
        elem.data("pagination-page-size", elem.attr("data-pagination-size"));
        elem.data("pagination-current-page", elem.attr("data-pagination-current-page"));
        elem.data("pagination-selector", elem.attr("data-pagination-selector"));
        elem.data("pagination-proximity", elem.attr("data-pagination-proximity"));
        if (elem.data("pagination-url") == null) {
            var url = window.location.href;
            Machinata.debug("Machinata.Pagination: autodetect URL: " + url);
            elem.data("pagination-url", url);
        }
        if (elem.data("pagination-current-page") == null) {
            elem.data("pagination-current-page", 1);
        } else {
            elem.data("pagination-current-page", parseInt(elem.data("pagination-current-page")));
        }
        if (elem.data("pagination-proximity") != null) {
            elem.data("pagination-proximity", parseFloat(elem.data("pagination-proximity")));
            // Register element
            if (Machinata.Pagination.proximityElements == null) Machinata.Pagination.proximityElements = [];
            Machinata.Pagination.proximityElements.push(elem);
        }
        // Register element
        if (Machinata.Pagination.elements == null) Machinata.Pagination.elements = [];
        Machinata.Pagination.elements.push(elem);
    });

    if (Machinata.Pagination.proximityElements != null) {
        Machinata.Pagination.updateProximityElements();
        // Hook on scroll
        $(window).scroll(function () {
            Machinata.Pagination.updateProximityElements();
        });
    }
});


/// <summary>
/// 
/// </summary>
Machinata.Pagination.countElements = function (elem) {
    if (elem.data("pagination-selector") != null) {
        return elem.find(elem.data("pagination-selector")).length;
    } else {
        return elem.children().length;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Pagination.loadNextPage = function (elem) {
    // Validate
    if (elem.data("pagination-loading") == true) return;
    if (elem.data("pagination-finished") == true) return;
    // Show loading
    elem.addClass("loading");
    elem.data("pagination-loading", true);
    Machinata.showLoading(true);
    // Increment
    elem.data("pagination-current-page", elem.data("pagination-current-page")+1);
    // Build URL
    var loadString = elem.data("pagination-url");
    loadString = Machinata.updateQueryString("page", elem.data("pagination-current-page"), loadString);
    if (elem.data("pagination-page-size") != null) loadString = Machinata.updateQueryString("page-size", elem.data("pagination-page-size"), loadString);
    if (elem.data("pagination-selector") != null) loadString += " " + elem.data("pagination-selector");
    // Bookeeping
    var currentElems = Machinata.Pagination.countElements(elem);
    Machinata.debug("Machinata.Pagination: currentElems: " + currentElems);
    // Make request
    Machinata.debug("Machinata.Pagination: load URL: " + loadString);
    var newElemsContainer = $("<div></div>");
    newElemsContainer.load(loadString, null, function (responseText, textStatus, jqXHR) {
        Machinata.showLoading(false);
        elem.removeClass("loading");
        elem.trigger("pagination-updated");
        // Error?
        if (textStatus == "error") {
            Machinata.debug("Machinata.Pagination: error with URL: " + loadString);
            elem.data("pagination-loading", false);
        } else {
            elem.data("pagination-loading", false);
            elem.find(".new-item").removeClass("new-item");
            newElemsContainer.children().appendTo(elem).addClass("new-item");
        }
        // Did we add elements?
        var newElems = elem.find(".new-item");
        var newElemsCount = Machinata.Pagination.countElements(elem);
        Machinata.debug("Machinata.Pagination: newElems: " + newElemsCount);
        if (newElemsCount <= currentElems) {
            Machinata.debug("Machinata.Pagination: no more elements added, will disable");
            elem.data("pagination-finished", true);
            elem.addClass("finished");
        } else {
            Machinata.UI.bind(newElems);
        }
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Pagination.updateProximityElements = function () {
    var scrollBottom = window.scrollY + window.innerWidth;
    for (var i = 0; i < Machinata.Pagination.proximityElements.length; i++) {
        var elem = Machinata.Pagination.proximityElements[i];
        if (elem.data("pagination-loading") == true) return;
        var elemBottom = elem.offset().top + elem.height();
        var p = scrollBottom / elemBottom;
        if (p > elem.data("pagination-proximity")) {
            Machinata.Pagination.loadNextPage(elem);
        }
    }
};