

/// <summary>
/// Shorthand functions for getting common paremeters from the context.
/// This module mimicks the Machinata Server Core Handler Params.
/// </summary>
/// <type>namespace</type>
Machinata.Params = {};

/// <summary>
/// 
/// </summary>
Machinata.Params.string = function (key) {
    return Machinata.queryParameter(key);
};

/// <summary>
/// 
/// </summary>
Machinata.Params.hasString = function (key) {
    return String.isEmpty(Machinata.queryParameter(key)) == false;
};

