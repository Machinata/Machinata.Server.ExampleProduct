


/// <summary>
/// CMS Library
/// </summary>
/// <type>namespace</type>
Machinata.Content = {};


/// <summary>
/// </summary>
Machinata.Content.PREVIEW_MAX_IMAGE_SIZE = 200;


/* ======== Machinata Content Editor Class ======== */

/// <summary>
/// </summary>
var ContentEditor = function (elem) {
    // Init
    this.elem = elem;
    this.contentSource = "ContentNode"; // default
    this.uid = ContentEditor._uid++;
    this.elem.attr("editor-uid", this.uid);
    ContentEditor._editors["editor"+this.uid] = this;
};

ContentEditor._uid = 0;
ContentEditor._editors = {};

/// <summary>
/// </summary>
ContentEditor.prototype.debug = function (msg) {
    Machinata.debug("Editor " + this.uid + ": " + msg);
};


/// <summary>
/// Set published state
/// </summary>
ContentEditor.prototype.setNodePublished = function (val) {
    if (val == true) {
        this.elem.find(".publish-toggle.true").addClass("selected");
        this.elem.find(".publish-toggle.false").removeClass("selected");
        this.elem.attr("node-published", "true");
    } else {
        this.elem.find(".publish-toggle.false").addClass("selected");
        this.elem.find(".publish-toggle.true").removeClass("selected");
        this.elem.attr("node-published", "false");
    }
    this.markDirty();
};

/// <summary>
/// Setup content type
/// </summary>
ContentEditor.prototype.setupContentType = function (contentNode) {
    var self = this;
    if (contentNode.length > 1) alert("DO NOT PASS MULTIPLE TO editor.setupContentType");
    if (contentNode.length == 0) alert("NO CONTENT NODE PASSED");
    var nodeType = contentNode.attr("node-type");
    var nodeId = contentNode.attr("node-id");
    var nodeValue = contentNode.attr("node-value");
    this.debug("Setting up content type " + nodeType + "...");
    this.debug("  type: " + nodeType);
    this.debug("  value: " + nodeValue);
    this.debug("  id: " + nodeId);
    contentNode.addClass("type-" + nodeType);
    // Call all binders to see if one can process the type...
    for (var i = 0; i < Machinata._onSetupContentNode.length; i++) {
        Machinata._onSetupContentNode[i](self, contentNode, nodeType, nodeValue);
    }
    // Bind action buttons
    contentNode.find(".action-delete").click(function () {
        self.debug("Removing content node...");
        var translation = contentNode.closest(".content-translation");
        // Last on page?
        var contentCount = contentNode.closest(".page-contents").find(".content-node").length;
        if (contentCount == 1) {
            self.debug("Last content node, will remove translation...");
            self.removeTranslation(translation);
            return;
        }
        // Remove the node
        contentNode.remove();
        // Update buttons
        self.updateContentTypeButtons(translation);
        self.markDirty();
    });
};

/// <summary>
/// Remove translation page
/// </summary>
ContentEditor.prototype.removeTranslation = function (translation) {
    var langID = translation.attr("translation-lang");
    //if (langID == "default") return;
    this.debug("Removing translation " + langID + "...");
    // Remove all content nodes
    translation.find(".content-node").remove();
    // Mark for deletion
    translation.attr("node-delete", "true");
    // Disable button
    var toggleButton = this.elem.find(".language-toggle.lang-" + langID);
    toggleButton.removeClass("enabled");
    toggleButton.hide();
    // Select default language
    this.elem.find(".content-translation").hide();
    this.elem.find(".language-toggle.enabled").first().not(".lang-add").trigger("click");
    this.markDirty();
    this.updateTranslationButtons();
};

ContentEditor.prototype.updateTranslationButtons = function () {
    this.elem.find(".translation-buttons .language-toggle").removeClass("option-first").removeClass("option-last");
    this.elem.find(".translation-buttons .language-toggle.enabled").first().addClass("option-first");
    this.elem.find(".translation-buttons .language-toggle.enabled").last().addClass("option-last");
};

/// <summary>
/// Update the buttons states for content types
/// </summary>
ContentEditor.prototype.updateContentTypeButtons = function (translation) {
    translation.find(".content-type.add-to-page").removeClass("option-first").removeClass("option-last");
    translation.find(".content-type.add-to-page").not(".disabled").each(function () {
        var button = $(this);
        // Validate
        var allowMultiple = button.attr("node-type-allow-multiple");
        if (allowMultiple == "false") {
            var count = translation.find(".content-node.type-" + button.attr("node-type")).length;
            if (count >= 1) button.hide().addClass("max-items");
            else button.show().removeClass("max-items");
        }
    });
    translation.find(".content-type.add-to-page").not(".disabled,.max-items").first().addClass("option-first");
    translation.find(".content-type.add-to-page").not(".disabled,.max-items").last().addClass("option-last");
};

/// <summary>
/// Setup translation page
/// </summary>
ContentEditor.prototype.setupTranslation = function (translation) {
    var self = this;
    var langID = translation.attr("translation-lang");
    this.debug("Setting up translation " + langID + "...");
    // Toggle button
    var toggleButton = this.elem.find(".language-toggle.lang-" + langID).not(".lang-add");
    toggleButton.click(function (e) {
        var button = $(this);
        self.showTranslation(button.attr("translation-lang"), e.shiftKey);
    });
    toggleButton.addClass("enabled");
    toggleButton.show();
    // Set title
    translation.find(".page-title").text(toggleButton.first().text());
    // Setup all content nodes
    translation.find(".content-node").not(".template").each(function () {
        self.setupContentType($(this));
    });
    // Make the nodes sortable
    translation.find(".page-contents").sortable({
        placeholder: "content-node placeholder",
        handle: ".handle"
    });
    // Bind add-to-page buttons
    translation.find(".content-type.add-to-page").each(function () {
        var button = $(this);
        button.click(function () {
            // See if a onAdd event has been registered...
            var didDoOnAdd = false;
            for (var i = 0; i < Machinata._onAddContentNode.length; i++) {
                var ret = Machinata._onAddContentNode[i](self, translation, button.attr("node-type"), button.attr("node-type-title"));
                if (ret == true) didDoOnAdd = true;
            }
            if (didDoOnAdd == true) {
                // On add is responsible
            } else {
                self.addContentType(translation, button.attr("node-type"), button.attr("node-type-title"));
            }
            self.updateContentTypeButtons(translation);
        });
    });
    // Bind translation actions
    translation.find(".delete-translation").click(function () {
        self.removeTranslation(translation);
    });
    translation.find(".duplicate-translation").click(function () {
        // Compile a dialog and ask for new language
        var title = "{text.content-translation.duplicate}";
        var descr = "{text.content-translation.duplicate.instructions}";
        var diag = Machinata.messageDialog(title, descr)
            .cancelButton()
            .okay(function (id) {
                self.copyTranslation(translation.attr("translation-lang"), id);
            });
        self.elem.find(".language-toggle").not(".enabled").each(function () {
            diag.option($(this).text(), $(this).attr("translation-lang"));
        });
        diag.show();
    });
    // Are we only supporting specific types?
    var supportedTypes = this.elem.attr("supported-content-types");
    
    if (supportedTypes != "" && supportedTypes != null && supportedTypes != "*") {
        translation.find(".content-type.add-to-page").hide().addClass("disabled");
        var types = supportedTypes.split(",");
        for (var i = 0; i < types.length; i++) {
            translation.find(".content-type[node-type='" + types[i] + "']").show().removeClass("disabled");
        }
    }
    self.updateContentTypeButtons(translation);
    self.updateTranslationButtons();
    
};


/// <summary>
/// Add content type
/// </summary>
ContentEditor.prototype.addContentType = function (translation, nodeType, typeTitle, nodeValue, nodeOptions) {
    this.debug("Adding content type " + nodeType + "...");
    // Make sure we support it
    var supportedTypes = this.elem.attr("supported-content-types");
    if (supportedTypes != "" && supportedTypes != null && supportedTypes != "*") {
        var types = supportedTypes.split(",");
        var supported = false;
        for (var i = 0; i < types.length; i++) {
            if (types[i] == nodeType) supported = true;
        }
        if (!supported) return;
    }
    // Copy template
    var newContentNode = this.contentNodeTemplate.clone();
    // Modify
    newContentNode.removeClass("template");
    newContentNode.removeClass("hidden-content");
    newContentNode.find(".content-type .node-type").text(typeTitle);
    newContentNode.attr("node-type", nodeType);
    if (nodeValue != null) newContentNode.attr("node-value", nodeValue);
    if (nodeOptions != null) newContentNode.attr("node-options", nodeOptions);
    // Append
    this.setupContentType(newContentNode);
    translation.find(".page-contents").append(newContentNode);
    translation.find(".page-contents").sortable('refresh');
    this.markDirty();
};


/// <summary>
/// 
/// </summary>
ContentEditor.prototype.copyTranslation = function (currentLangID, newLangID) {
    this.debug("Copying translation " + currentLangID + " to translation " + newLangID + "...");
    var self = this;
    // Snapshot data
    var json = this.compileContentAsJSON();
    // Create new translation node
    var newTranslationNode = this.createTranslation(newLangID);
    this.setupTranslation(newTranslationNode);
    // Create a copy of all the content nodes
    var currentTranslation = this.elem.find(".content-translation.lang-" + currentLangID);
    var currentTranslationData = null;
    for (var i = 0; i < json["translations"].length; i++) {
        if (json["translations"][i]["lang"] == currentLangID) currentTranslationData = json["translations"][i];
    }
    for (var i = 0; i < currentTranslationData["nodes"].length; i++) {
        var node = currentTranslationData["nodes"][i];
        var typeTitle = newTranslationNode.find(".content-type.add-to-page[node-type='" + node["type"] + "']").attr("node-type-title");
        self.addContentType(newTranslationNode, node["type"], typeTitle, node["data"], node["options"]);
        
    }
    // Show
    //this.setupTranslation(newTranslationNode);
    this.showTranslation(newLangID);
    this.updateTranslationButtons();
    this.markDirty();
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.createTranslation = function (langID) {
    this.debug("Creating translation " + langID + "...");
    // Validate...
    // Supported?
    if (this.elem.find(".language-toggle.lang-" + langID).length == 0) {
        Machinata.apiError("invalid-language", "{text.error-invalid-language=This language is not supported on your system.}");
        return;
    }
    // Already exist?
    if (this.elem.find(".content-translation.lang-" + langID).length > 0) {
        var existingTranslation = this.elem.find(".content-translation.lang-" + langID);
        if (existingTranslation.attr("node-delete") == "true") {
            // We previously deleted it, so since we are adding it again we can remove this one and start fresh again
            existingTranslation.remove();
        } else {
            Machinata.apiError("language-already-exists", "{text.error-language-already-exists=The language you entered already exists!}");
            return;
        }
    }
    // Copy template
    var newTranslationNode = this.translationNodeTemplate.clone();
    // Modify
    newTranslationNode.removeClass("lang-add");
    newTranslationNode.addClass("lang-" + langID);
    newTranslationNode.attr("translation-lang", langID);
    // Append
    this.elem.find(".translations").append(newTranslationNode);
    this.updateTranslationButtons();
    return newTranslationNode;
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.addTranslation = function (langID) {
    this.debug("Adding translation " + langID + "...");
    // Create
    var newTranslationNode = this.createTranslation(langID);
    // Show
    this.setupTranslation(newTranslationNode);
    // Default content type?
    if (this.elem.attr("default-content-type") != "") {
        var contentButton = newTranslationNode.find(".content-type.add-to-page[node-type='" + this.elem.attr("default-content-type") + "']");
        contentButton.trigger("click");
    }
    this.showTranslation(langID);
    this.markDirty();
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.showTranslation = function (langID, toggle) {
    this.debug((toggle == true ? "Toggling" : "Showing") + " translation " + langID + "...");
    var translation = this.elem.find(".content-translation.lang-" + langID);
    if (toggle == true) {
        // Toggled mode
        this.elem.find(".content-translation").addClass("toggled");
        if (translation.hasClass("visible")) {
            // Make sure this is not the only one
            if (this.elem.find(".content-translation.visible").length == 1) return;
            this.elem.find(".language-toggle.lang-" + langID).removeClass("selected");
            translation.removeClass("visible");
            translation.hide();
        } else {
            this.elem.find(".language-toggle.lang-" + langID).addClass("selected");
            translation.addClass("visible");
            translation.show();
        }
        // Update padding for visible 
        this.elem.find(".content-translation").removeClass("padded");
        this.elem.find(".content-translation.visible").slice(1).addClass("padded");
    } else {
        // Single mode
        this.elem.find(".content-translation").removeClass("toggled").removeClass("padded").removeClass("visible").not(translation).hide();
        this.elem.find(".language-toggle").removeClass("selected");
        this.elem.find(".language-toggle.lang-" + langID).addClass("selected");
        translation.addClass("visible");
        translation.show();
    }

};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.compileContentAsJSON = function () {
    var self = this;
    // Init data structure
    var data = {};
    var dataDebug = "";
    data.nodeid = self.elem.attr("node-id");
    data.nodepath = self.elem.attr("node-path");
    data.type = "node";
    data["delete"] = self.elem.attr("node-delete"); // Note delete is reserved Ecma keyword
    data.published = self.elem.attr("node-published");
    data.layout = self.elem.attr("node-layout");
    data.options = self.getNodeLayoutOptions();
    data.translations = [];
    // Debug
    dataDebug += "" + "Delete: " + data["delete"] + "\n";
    dataDebug += "" + "Published: " + data.published + "\n";
    // Interate all tranlsations
    self.elem.find(".content-translation").not(".lang-add").each(function () {
        // Init
        var translation = $(this);
        var tranlsationID = translation.attr("node-id");
        var tranlsationLang = translation.attr("translation-lang");
        // Create json data
        var dataTranslation = {};
        dataTranslation.nodeid = tranlsationID;
        dataTranslation.type = "trans";
        dataTranslation.lang = tranlsationLang;
        dataTranslation["delete"] = translation.attr("node-delete");
        dataTranslation.published = translation.attr("node-published");
        dataTranslation.nodes = [];
        // Debug
        Machinata.debug("Comping data for translation " + tranlsationLang + "...");
        dataDebug += "" + "Translation " + tranlsationLang + " (" + tranlsationID + "):\n";
        dataDebug += "  " + "Delete: " + dataTranslation["delete"] + "\n";
        dataDebug += "  " + "Published: " + dataTranslation.published + "\n";
        // Discover all content nodes for this page
        var contentNodes = translation.find(".content-node").not(".lang-add");
        var i = 0;
        contentNodes.each(function () {
            // Init
            var contentNode = $(this);
            var nodeData = contentNode.data("node-value")();
            var nodeSummary = contentNode.data("node-summary")();
            var nodeShortURL = contentNode.data("node-short-url")();
            var nodeTitle = contentNode.data("node-title") ();
            var nodeType = contentNode.attr("node-type");
            var nodeID = contentNode.attr("node-id");
            var nodeOptions = contentNode.attr("node-options");
            // Create json data
            var dataNode = {};
            dataNode.nodeid = nodeID;
            dataNode.type = nodeType;
            dataNode.data = nodeData;
            dataNode.options = nodeOptions;
            dataNode.summary = nodeSummary;
            dataNode.shorturl = nodeShortURL;
            dataNode.title = nodeTitle;
            dataNode.sort = i;
            // Debug
            dataDebug += "  " + "Node " + i + " (" + nodeID + "):\n"
            dataDebug += "  " + "  " + nodeType + "\n";
            dataDebug += "  " + "  " + nodeData + "\n";
            dataDebug += "  " + "  " + nodeSummary + "\n";
            // Register and next
            dataTranslation.nodes.push(dataNode);
            i++;
        });
        // Register
        data.translations.push(dataTranslation);
    });

    //alert(dataDebug);
    //alert(JSON.stringify(data));

    return data;
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.markClean = function () {
    this.dirty = false;
};
/// <summary>
/// 
/// </summary>
ContentEditor.prototype.markDirty = function () {
    this.dirty = true;
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.getNodeLayout = function () {
    return this.elem.attr("node-layout");
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.getNodeLayoutOptions = function () {
    var self = this;
    var layout = self.getNodeLayout();
    var selectedOptions = self.elem.find(".options-buttons[layout-name=\"" + layout + "\"] .option-toggle.selected");
    var ret = {};
    selectedOptions.each(function () {
        ret[$(this).attr("option-name")] = true;
    });
    return ret;
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.setNodeLayout = function (layout) {
    var self = this;
    // Update node
    self.elem.attr("node-layout", layout);
    // Update layout UI
    //self.elem.find(".layout-toggle").removeClass("selected");
    //self.elem.find(".layout-toggle[layout-name=\"" + layout + "\"]").addClass("selected");
    self.elem.find(".layout-buttons").val(layout);
    // Update options UI
    var allOptions = self.elem.find(".options-buttons");
    var currentOptions = self.elem.find(".options-buttons[layout-name=\"" + layout + "\"]");
    allOptions.hide();
    currentOptions.show();
    if (currentOptions.find(".option-toggle").length == 0) {
        self.elem.find(".options-ui").hide();
    } else {
        self.elem.find(".options-ui").show();
    }
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.save = function () {
    var self = this;
    var progress = Machinata.progressDialog("{text.saving}...");
    progress.show();
    progress.updateProgress(10);
    // Get data
    var compiledData = self.compileContentAsJSON();
    progress.updateProgress(30);
    // Make flat
    var data = {};
    data["content"] = JSON.stringify(compiledData);
    progress.updateProgress(50);
    // Do API call to save
    var call = Machinata.apiCall("/api/admin/content/page/" + self.elem.attr("node-id") + "/save");
    call.data(data);
    // Prepare api call
    call.success(function (message) {
        self.markClean();
        progress.updateProgress(100);
        progress.close();
    })
    call.error(function (message) {
        progress.close();
    });
    call.genericError();
    // Send
    call.send();
    progress.updateProgress(70);
    return call;
};

/// <summary>
/// 
/// </summary>
ContentEditor.prototype.init = function () {
    var self = this;

    // Cache some handles
    self.contentNodeTemplate = self.elem.find(".content-node.template");
    self.translationNodeTemplate = self.elem.find(".content-translation.lang-add");

    // Init some settings
    self.boundToInput = false;
    if (self.elem.attr("editor-bind-to-input") != "") self.boundToInput = true;
    if (self.elem.attr("content-source") != null && self.elem.attr("content-source") != "") self.contentSource = self.elem.attr("content-source");

    // Debug
    self.debug("Creating content editor for node id " + self.elem.attr("node-id") + "...");

    // Bind add lang button
    self.elem.find(".language-toggle.lang-add").addClass("enabled").click(function (event) {
        self.debug("Add Language");
        var button = $(this);
        var title = "{text.add-translation=Add Translation}";
        var descr = "{text.add-translation-description=Choose a new translation to add to this page:}";
        var doCopy = event.shiftKey;
        var currentLangID = self.elem.find(".language-toggle.selected").first().attr("translation-lang");
        var currentLangTitle = self.elem.find(".language-toggle.selected").first().text();
        if (doCopy) {
            title = "{text.copy-translation=Copy Translation}";
            descr = "{text.copy-translation-description=Choose a new translation to copy from}" + " " + currentLangTitle + ":";
            if (self.elem.find(".language-toggle.selected").length > 1) {
                Machinata.apiError("invalid-copy", "{text.cannot-copy-multiple=Multiple translations are open. You must have only a single translation open to copy it.}");
                return;
            }
        }
        // Compile a dialog and ask for new language
        var diag = Machinata.messageDialog(title, descr)
                .cancelButton()
                .okay(function (id) {
                    if (doCopy) {
                        self.copyTranslation(currentLangID,id);
                    } else {
                        self.addTranslation(id);
                    }
                });
        self.elem.find(".language-toggle").not(".enabled").each(function () {
            diag.option($(this).text(), $(this).attr("translation-lang"));
        });
        diag.show();
    });

    // Setup all translations
    var transCount = 0;
    var firstTransLangID = null;
    self.elem.find(".content-translation").not(".lang-add").each(function () {
        if (firstTransLangID == null) firstTransLangID = $(this).attr("translation-lang");
        self.setupTranslation($(this));
        transCount++;
    });

    // No translation? Add default
    //if (transCount == 0 && self.boundToInput) {
    //    self.addTranslation("default");
    //    firstTransLangID = "default";
    //}

    // Is the editor bound to a input?
    if (self.boundToInput) {
        self.elem.find(".save-button").remove();
        self.elem.find(".published-buttons").hide();
        self.elem.find(".remove-if-bound-to-input").remove();
    } else {
        self.elem.find(".show-if-bound-to-input").remove();
    }

    // Bind save button
    //self.elem.find(".save-button").click(function () {
    //    Machinata.saveContentEditor(self);
    //});
    self.elem.closest(".ui-card").find(".ui-toolbar .save-button").click(function () {
        Machinata.saveContentEditor(self);
    });

    // Publish toggle
    self.elem.find(".publish-toggle.true").click(function () {
        self.setNodePublished(true);
    });
    self.elem.find(".publish-toggle.false").click(function () {
        self.setNodePublished(false);
    });

    // Layout selection
    self.elem.find(".layout-buttons").on("change",function () {
        //self.setNodeLayout($(this).attr("layout-name"));
        self.setNodeLayout($(this).val());
    });
    self.setNodeLayout(self.elem.attr("node-layout"));

    // Options toggle
    self.elem.find(".option-toggle").click(function () {
        if ($(this).hasClass("selected")) $(this).removeClass("selected");
        else $(this).addClass("selected");
    });

    // Before unload
    self.dirty = false;
    window.onbeforeunload = function () {
        if (self.dirty == true && self.boundToInput == false) return "{text.unsaved-changes=You have unsaved changes. Are you sure you want to leave the page?";
        else return null;
    }

    // Set initial state
    self.elem.find(".content-translation.hidden-content").hide();
    self.setNodePublished(self.elem.attr("node-published") == "true");
    self.elem.find(".language-toggle").not(".enabled").hide();
    // Show first translation from togglebar (not good since order is not always same)
    //self.elem.find(".language-toggle").not(".lang-add").first().trigger("click");
    // Show first translation supported langs (not good since many people dont see the translations)
    //if (firstTransLangID != null) self.showTranslation(firstTransLangID);
    // Show all enabled langs
    self.elem.find(".language-toggle.enabled").not(".lang-add").each(function () {
        self.showTranslation($(this).attr("translation-lang"),true);
    });
    self.updateTranslationButtons();
    self.elem.show();
    self.markClean();
};





/* ======== Machinata Content ======== */

Machinata._onSetupContentNode = [];

/// <summary>
/// 
/// </summary>
Machinata.onSetupContentNode = function (fn) {
    Machinata._onSetupContentNode.push(fn);
};


Machinata._onAddContentNode = [];

/// <summary>
/// 
/// </summary>
Machinata.onAddContentNode = function (fn) {
    Machinata._onAddContentNode.push(fn);
};

/// <summary>
/// 
/// </summary>
Machinata.setNodeOption = function (contentNode, key, val) {
    var json = contentNode.attr("node-options");
    var obj = {};
    if (json != null && json != "") obj = JSON.parse(json);
    obj[key] = val;
    json = JSON.stringify(obj);
    if (json == "{}") json = "";
    contentNode.attr("node-options", json);
};

/// <summary>
/// 
/// </summary>
Machinata.getNodeOption = function (contentNode,key) {
    var json = contentNode.attr("node-options");
    var obj = {};
    if (json != null && json != "") obj = JSON.parse(json);
    return obj[key];
};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    
    // Setup our core content node setups
    Machinata.onSetupContentNode(function (editor, contentNode, nodeType, nodeValue) {
        if (contentNode.length > 1) alert("DO NOT PASS MULTIPLE TO Machinata.onSetupContentNode");
        // Default
        contentNode.data("node-value", function () {
            Machinata.debug("    Get default node value for " + nodeType);
            return contentNode.find(".content-value").text();
        });
        contentNode.data("node-summary", function () {
            Machinata.debug("    Get default node summary for " + nodeType);
            return null;
        });
        contentNode.data("node-title", function () {
            Machinata.debug("    Get default node title for " + nodeType);
            return null;
        });
        contentNode.data("node-short-url", function () {
            Machinata.debug("    Get default node short url for " + nodeType);
            return null;
        });
        // HTML node
        if (nodeType == "html") {
            Machinata.debug("Creating HTML notebook...");
            var notebookWrapper = $("<div class='notebook rich-text'></div>");
            notebookWrapper.attr("data-content-source", editor.contentSource);
            notebookWrapper.html(decodeURIComponent(nodeValue));
            contentNode.find(".content-value").addClass("cursor-text");
            contentNode.find(".content-value").append(notebookWrapper);
            // See https://github.com/raphaelcruzeiro/jquery-notebook
            notebookWrapper.notebook({
                autoFocus: false,
                placeholder: null,
                mode: 'multiline', // multiline or inline
                //modifiers: ['bold', 'italic', 'underline', 'h1', 'h2', 'ol', 'ul', 'anchor','image']
            });
            notebookWrapper.on('contentChange', function () {
                editor.markDirty();
            });
            contentNode.data("node-value", function () {
                Machinata.debug("    Get custom nodeValue for " + nodeType + " (notebook id " + notebookWrapper.attr('data-jquery-notebook-id') + ")");
                // Get the content area html
                var contentArea = $('#jquery-notebook-content-' + notebookWrapper.attr('data-jquery-notebook-id'));
                var html = contentArea.val();
                // Reformat some oddities
                html = html.replaceAll("<p> </p>", "");
                html = html.replaceAll("<h1> </h1>", "");
                html = html.replaceAll("<h2> </h2>", "");
                return encodeURIComponent(html);
            });
            contentNode.data("node-summary", function () {
                if (notebookWrapper.children().length == 0) {
                    return notebookWrapper.text();
                }
                var summary = "";
                notebookWrapper.children().each(function () {
                    //TODO: @dan .text() is not returning line breaks, we probably need to create our own method to do this
                    summary += $(this).text() + "\n";
                });
                return summary;
            });
        } else if (nodeType == "image" || nodeType == "thumb") {
            Machinata.debug("Creating Image/Thumbnail node...");
            var contentValElem = contentNode.find(".content-value");
            var imageElem = $("<img/>");
            imageElem.attr("src", nodeValue);
            contentValElem.addClass("cursor-pointer").addClass("checkered");
            imageElem.click(function () {
                Machinata.fileDialog("{text.choose-a-image=Choose a Image}", editor.contentSource, "image")
                .okay(function (val) {
                    imageElem.attr("src", val);
                    contentNode.attr("node-value", val);
                    editor.markDirty();
                })
                .show();
            });
            contentValElem.append(imageElem);
            contentValElem.append($("<div class='clear'/>"));
            //contentValElem.append($("<div class='label'>{text.content-type-image-description}</div>"));
            contentValElem.append($("<input class='no-ui type-meta description' placeholder=\"{text.description}\"/>"));
            // Load values
            contentNode.find(".description").val(Machinata.getNodeOption(contentNode, "description"));
            contentNode.find(".description").on("change keyup",function () {
                if ($(this).val() != "") $(this).removeClass("faded");
                else $(this).addClass("faded");
            }).trigger("change");
            // Data handler
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "description", contentNode.find(".description").val());
                return contentNode.attr("node-value");
            });
            // Download button
            var downloadButton = $("<button title='{text.content-node.download}' class='ui-button option-small option-translucent option-icon-only node-tools action-download'>{icon.download}</button>");
            downloadButton.click(function () {
                Machinata.openPage(contentNode.attr("node-value") + "?download=true");
            });
            contentNode.find(".content-actions").append(downloadButton);
        } else if (nodeType == "video") {
            Machinata.debug("Creating Video node...");
            // Video image / gif
            var imageElem = $("<img/>");
            imageElem.attr("src", nodeValue);
            imageElem.addClass("cursor-pointer");
            imageElem.click(function () {
                Machinata.fileDialog("{text.choose-a-thumbnail=Choose a Thumbnail}", editor.contentSource, "image")
                .okay(function (val) {
                    imageElem.attr("src", val);
                    contentNode.attr("node-value", val);
                    editor.markDirty();
                })
                .show();
            });
            contentNode.find(".content-value").append(imageElem);
            contentNode.find(".content-value").append($("<div class='clear'></div>"));
            // Video files
            function createVideoFileUI(filetype) {
                var fileMP4 = Machinata.getNodeOption(contentNode, filetype);
                var fileTitle = filetype.toUpperCase();
                var fileElemMP4 = Machinata.createFilePreview(fileMP4 == null ? fileTitle : fileMP4, fileTitle, false);
                fileElemMP4.addClass("cursor-pointer");
                fileElemMP4.click(function () {
                    Machinata.fileDialog("{text.choose-a-video=Choose a Video} (" + filetype + ")", editor.contentSource, "video/" + filetype)
                    .okay(function (val) {
                        if (val == "delete") return;
                        Machinata.updateFilePreview(fileElemMP4, val, fileTitle, false);
                        Machinata.setNodeOption(contentNode, filetype, val);
                        editor.markDirty();
                    })
                    .button("{text.remove}", "delete", function () {
                        Machinata.updateFilePreview(fileElemMP4, fileTitle, fileTitle, false);
                        Machinata.setNodeOption(contentNode, filetype, null);
                        editor.markDirty();
                    })
                    .show();
                });
                contentNode.find(".content-value").append(fileElemMP4);
            }
            createVideoFileUI("mp4");
            createVideoFileUI("webm");
            createVideoFileUI("ogv");
            contentNode.data("node-value", function () {
                // Re-calculate aspect ratio
                var ar = imageElem.width() / imageElem.height();
                Machinata.setNodeOption(contentNode, "aspectratio", ar);
                return contentNode.attr("node-value");
            });
        } else if (nodeType == "file") {
            Machinata.debug("Creating File node...");
            var fileElem = Machinata.createFilePreview(nodeValue, nodeValue, false);
            var contentValElem = contentNode.find(".content-value");
            fileElem.addClass("cursor-pointer");
            fileElem.click(function () {
                Machinata.fileDialog("{text.choose-a-file=Choose a File}", editor.contentSource)
                .okay(function (val) {
                    Machinata.updateFilePreview(fileElem, val, val, false);
                    contentNode.attr("node-value", val);
                    editor.markDirty();
                })
                .show();
            });
            contentValElem.append(fileElem);
            contentValElem.append($("<div class='clear'/>"));
            contentValElem.append($("<input class='no-ui type-meta title' placeholder=\"{text.title}\"/>"));
            // Load values
            contentNode.find("input.title").val(Machinata.getNodeOption(contentNode, "title"));
            contentNode.find("input.title").on("change keyup", function () {
                if ($(this).val() != "") $(this).removeClass("faded");
                else $(this).addClass("faded");
            }).trigger("change");
            // Data handler
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "title", contentNode.find("input.title").val());
                return contentNode.attr("node-value");
            });
            // Download button
            var downloadButton = $("<button title='{text.content-node.download}' class='ui-button option-small option-translucent option-icon-only node-tools action-download'>{icon.download}</button>");
            downloadButton.click(function () {
                Machinata.openPage(contentNode.attr("node-value")+"?download=true");
            });
            contentNode.find(".content-actions").append(downloadButton);
        } else if (nodeType == "title") {
            Machinata.debug("Creating title node...");
            var inputElem = $("<input class='no-ui type-title'/>");
            inputElem.val(nodeValue);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append(inputElem);
            contentNode.data("node-value", function () {
                return contentNode.find(".content-value input").val();
            });
            contentNode.data("node-title", function () {
                return contentNode.find(".content-value input").val();
            });
        } else if (nodeType == "embed") {
            Machinata.debug("Creating embed node...");
            var inputElem = $("<textarea class='no-ui type-embed'/>");
            var embedCode = decodeURIComponent(nodeValue);
            var lines = embedCode.split('\n').length;
            inputElem.attr("rows", Math.max(lines, 4));
            inputElem.val(embedCode);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append(inputElem);
            contentNode.data("node-value", function () {
                var html = contentNode.find(".content-value textarea").val();
                return encodeURIComponent(html);
            });
        } else if (nodeType == "label") {
            Machinata.debug("Creating label node...");
            var inputElem = $("<input class='no-ui type-label'/>");
            inputElem.val(nodeValue);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-label}</div>")).append(inputElem);
            contentNode.data("node-value", function () {
                return contentNode.find(".content-value input").val();
            });
        } else if (nodeType == "shorturl") {
            Machinata.debug("Creating shorturl node...");
            var inputElem = $("<input class='no-ui type-shorturl'/>");
            inputElem.val(nodeValue);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-shorturl}</div>")).append(inputElem);
            contentNode.data("node-value", function () {
                return contentNode.find(".content-value input").val();
            });
            contentNode.data("node-short-url", function () {
                return contentNode.find(".content-value input").val();
            });
        } else if (nodeType == "option") {
            Machinata.debug("Creating option node...");
            var inputElem = $("<input class='no-ui type-option'/>");
            inputElem.val(nodeValue);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-option}</div>"));
            contentNode.find(".content-value").append(inputElem);
            contentNode.data("node-value", function () {
                return contentNode.find(".content-value input").val();
            });
        } else if (nodeType == "meta") {
            Machinata.debug("Creating meta node...");
            // Create all meta attribs
            var contentValElem = contentNode.find(".content-value");
            contentValElem.append($("<div class='label'>{text.content-type-meta-title}</div>")).append($("<input class='no-ui type-meta meta-title'/>"));
            contentValElem.append($("<div class='label'>{text.content-type-meta-description}</div>")).append($("<input class='no-ui type-meta meta-description'/>"));
            contentValElem.append($("<div class='label'>{text.content-type-meta-keywords}</div>")).append($("<input class='no-ui type-meta meta-keywords'/>"));
            contentValElem.append($("<div class='label'>{text.content-type-meta-image}</div>")).append($("<img class='no-ui cursor-pointer checkered type-meta meta-image'/>"));
            // Load values
            contentNode.find(".meta-title").val(Machinata.getNodeOption(contentNode, "meta-title"));
            contentNode.find(".meta-description").val(Machinata.getNodeOption(contentNode, "meta-description"));
            contentNode.find(".meta-keywords").val(Machinata.getNodeOption(contentNode, "meta-keywords"));
            // Bind image
            function updateImage(propName) {
                var imageSRC = Machinata.getNodeOption(contentNode, "meta-"+propName);
                if (imageSRC == null) imageSRC = "";
                console.log(imageSRC);
                contentNode.find(".meta-" + propName).attr("src", imageSRC);
            }
            updateImage("image");
            contentNode.find(".meta-image").click(function () {
                Machinata.fileDialog("{text.choose-a-image}", editor.contentSource, "image")
                .okay(function (val) {
                    Machinata.setNodeOption(contentNode, "meta-image", val);
                    updateImage("image");
                    editor.markDirty();
                })
                .show();
            });
            // Bind changes
            contentNode.find("input").on("change keyup", function () { editor.markDirty(); });
            // Value/option functions
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "meta-title", contentNode.find(".meta-title").val());
                Machinata.setNodeOption(contentNode, "meta-description", contentNode.find(".meta-description").val());
                Machinata.setNodeOption(contentNode, "meta-keywords", contentNode.find(".meta-keywords").val());
                return null;
            });
        } else if (nodeType == "product") {
            Machinata.debug("Creating Product node...");
            // Create preview
            var previewElem = Machinata.createGenericPreview(
                Machinata.getNodeOption(contentNode, "product-name"), 
                Machinata.getNodeOption(contentNode, "catalog-name"),
                null,
                "{text.product}",
                "tag",
                "product"
            );
            var contentValElem = contentNode.find(".content-value");
            previewElem.addClass("cursor-pointer");
            // Show product browser on click
            previewElem.click(function () {
                var diag = Machinata.productDialog("{text.choose-a-product=Choose a Product}");
                diag.input(function (data) {
                    // Product was selected, update node options
                    Machinata.setNodeOption(contentNode, "product-name", data["product-name"]);
                    Machinata.setNodeOption(contentNode, "product-public-id", data["product-public-id"]);
                    Machinata.setNodeOption(contentNode, "config-public-id", data["config-public-id"]);
                    Machinata.setNodeOption(contentNode, "catalog-name", data["catalog-name"]);
                    Machinata.setNodeOption(contentNode, "catalog-short-url", data["catalog-short-url"]);
                    Machinata.setNodeOption(contentNode, "catalog-public-id", data["catalog-public-id"]);
                    // Update preview
                    Machinata.updateGenericPreview(
                        previewElem,
                        data["product-name"],
                        data["catalog-name"]
                    );
                    // Set node value
                    contentNode.attr("node-value", data["product-public-id"]);
                    editor.markDirty();
                });
                diag.show();
            });
            contentValElem.append(previewElem);

            // Data handler
            contentNode.data("node-title", function () {
                return Machinata.getNodeOption(contentNode, "product-name");
            });
            contentNode.data("node-value", function () {
                return contentNode.attr("node-value");
            });
        } else if (nodeType == "link") {
            Machinata.debug("Creating link node...");
            var inputElem = $("<input class='no-ui no-padding link-url'/>");
            inputElem.val(nodeValue);
            contentNode.find(".content-value").append($("<div class='label'>{text.link-url=URL}</div>"));
            contentNode.find(".content-value").append(inputElem);
            contentNode.find(".content-value").append($("<div class='label'>{text.link-title=Title}</div>"));
            contentNode.find(".content-value").append($("<input class='no-ui no-padding link-title'/>"));
            contentNode.find(".content-value input.link-title").val(Machinata.getNodeOption(contentNode, "title"));
            contentNode.find(".content-value input").on("change keyup", function () { editor.markDirty(); });
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "title", contentNode.find("input.link-title").val());
                return contentNode.find(".content-value input.link-url").val();
            });
        } else if (nodeType == "quote") {
            Machinata.debug("Creating quote node...");
            var inputElem = $("<input class='no-ui no-padding quote-text'/>");
            inputElem.val(nodeValue);
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-quote.text}</div>"));
            contentNode.find(".content-value").append(inputElem);
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-quote.author}</div>"));
            contentNode.find(".content-value").append($("<input class='no-ui no-padding quote-author'/>"));
            contentNode.find(".content-value").append($("<div class='label'>{text.content-type-quote.title}</div>"));
            contentNode.find(".content-value").append($("<input class='no-ui no-padding quote-title'/>"));
            contentNode.find(".content-value input.quote-author").val(Machinata.getNodeOption(contentNode, "author"));
            contentNode.find(".content-value input.quote-title").val(Machinata.getNodeOption(contentNode, "title"));
            contentNode.find(".content-value input").on("change keyup", function () { editor.markDirty(); });
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "author", contentNode.find("input.quote-author").val());
                Machinata.setNodeOption(contentNode, "title", contentNode.find("input.quote-title").val());
                return contentNode.find(".content-value input.quote-text").val();
            });
        }  else if (nodeType == "button") {
            Machinata.debug("Creating button node...");
            var inputElem = $("<input class='no-ui no-padding link-url'/>");
            inputElem.val(nodeValue);
            contentNode.find(".content-value").append($("<div class='label'>{text.button-url=Action}</div>"));
            contentNode.find(".content-value").append(inputElem);
            contentNode.find(".content-value").append($("<div class='label'>{text.button-title=Title}</div>"));
            contentNode.find(".content-value").append($("<input class='no-ui no-padding link-title'/>"));
            contentNode.find(".content-value input.link-title").val(Machinata.getNodeOption(contentNode, "title"));
            contentNode.find(".content-value input").on("change keyup", function () { editor.markDirty(); });
            contentNode.data("node-value", function () {
                Machinata.setNodeOption(contentNode, "title", contentNode.find("input.link-title").val());
                return contentNode.find(".content-value input.link-url").val();
            });
        } else if (nodeType == "desc") {
            Machinata.debug("Creating description node...");
            var inputElem = $("<input class='no-ui type-description'/>");
            inputElem.val(nodeValue);
            inputElem.on("change keyup", function () { editor.markDirty(); });
            contentNode.find(".content-value").append(inputElem);
            contentNode.data("node-value", function () {
                return contentNode.find(".content-value input").val();
            });
            }
    });

    // Setup our core content node setups
    Machinata.onAddContentNode(function (editor, translation, nodeType, nodeTitle) {
        if (nodeType == "image" || nodeType == "thumb") {
            Machinata.fileDialog("{text.choose-a-image}", editor.contentSource, "image")
                .okay(function (val) {
                    editor.addContentType(translation, nodeType, nodeTitle, val)
                })
                .show();
            return true;
        } else if (nodeType == "video") {
            Machinata.fileDialog("{text.choose-a-thumbnail}", editor.contentSource, "image")
                .okay(function (val) {
                    editor.addContentType(translation, nodeType, nodeTitle, val)
                })
                .show();
            return true;
        } else if (nodeType == "file") {
            Machinata.fileDialog("{text.choose-a-file}", editor.contentSource)
                .okay(function (val) {
                    editor.addContentType(translation, nodeType, nodeTitle, val)
                })
                .show();
            return true;
        }
        return false;
    });

    // Auto-init all content editor nodes
    $(".ui-content-editor").each(function () {
        var editor = new ContentEditor($(this));
        editor.init();
    });

});


/// <summary>
///
/// </summary>
Machinata.getContentEditor = function (elem) {
    var key = "editor" + elem.attr("editor-uid");
    return ContentEditor._editors[key];
};

/// <summary>
///
/// </summary>
Machinata.saveContentEditor = function (editor) {
    editor.save();
};



/// <summary>
///
/// </summary>
Machinata.createPagePreview = function (path, title, summary) {
    // Sanity
    var icon = "document";
    // Create the elem
    var previewElem = $("<div class='file-preview'/>");
    previewElem.attr("node-path", path);
    var pathElem = $("<div class='path'/>");
    pathElem.text(title);
    previewElem.append(pathElem);
    var iconElem = $("<div class='icon icon-" + icon + "'/>");
    previewElem.append(iconElem);
    previewElem.addClass("category-page");
    previewElem.attr("title", title);
    // Return
    return previewElem;
};

/// <summary>
///
/// </summary>
Machinata.createGenericPreview = function (name, title, subtitle, type, icon, category) {

    // Create the elem
    var previewElem = $("<div class='file-preview ui-file-preview'/>");
    var metaElem = $("<div class='meta'/>");
    metaElem.append($("<div class='meta-name'/>").text(name));
    metaElem.append($("<div class='meta-title'/>").text(title));
    metaElem.append($("<div class='meta-subtitle'/>").text(subtitle));
    previewElem.append(metaElem);
    var iconElem = $("<div class='icon icon-" + icon + "'/>");
    previewElem.append(iconElem);
    var tagElem = $("<div class='tag'/>").text(type);
    previewElem.append(tagElem);
    previewElem.addClass("category-" + category);
    previewElem.attr("title", title);
    // Return
    return previewElem;
};

/// <summary>
///
/// </summary>
Machinata.updateGenericPreview = function (previewElem, name, title, subtitle, type, icon, category) {
    // Update
    if (name != null) previewElem.find(".meta-name").text(name);
    if (title != null) previewElem.find(".meta-title").text(title);
    if (subtitle != null) previewElem.find(".meta-subtitle").text(subtitle);
    // Return
    return previewElem;
};


/// <summary>
///
/// </summary>
Machinata.updateFilePreview = function (previewElem, contentURL, title, autoDetectImages) {
    // Is it an image?
    var isImage = false;
    if (contentURL.endsWith(".png")) isImage = true;
    else if (contentURL.endsWith(".jpg")) isImage = true;
    else if (contentURL.endsWith(".gif")) isImage = true;
    else if (contentURL.endsWith(".tiff")) isImage = true;
    var icon = "document"; // fallback
    if (isImage == true) icon = "image";
    else if (contentURL.endsWith(".pdf")) icon = "document-text";
    else if (contentURL.endsWith(".doc")) icon = "document-text";
    else if (contentURL.endsWith(".docx")) icon = "document-text";
    else if (contentURL.endsWith(".rtf")) icon = "document-text";
    else if (contentURL.endsWith(".txt")) icon = "document-text";
    else if (contentURL.endsWith(".zip")) icon = "briefcase";
    else if (contentURL.endsWith(".ppt")) icon = "device-desktop";
    else if (contentURL.endsWith(".pptx")) icon = "device-desktop";
    else if (contentURL.endsWith(".mp4")) icon = "video-outline";
    else if (contentURL.endsWith(".mov")) icon = "video-outline";
    else if (contentURL.endsWith(".3gp")) icon = "video-outline";
    else if (contentURL.endsWith(".webm")) icon = "video-outline";
    else if (contentURL.endsWith(".avi")) icon = "video-outline";
    // Update
    previewElem.html("");
    previewElem.attr("content-url", contentURL);

    var filename = contentURL.substring(contentURL.lastIndexOf('/') + 1, contentURL.length) || contentURL;
    var ext = contentURL.substring(contentURL.lastIndexOf('.') + 1, contentURL.length) || contentURL;
    var metaElem = $("<div class='meta'/>");
    metaElem.append($("<div class='meta-name'/>").text(filename));
    previewElem.append(metaElem);
    var tagElem = $("<div class='tag'/>").text(ext);
    previewElem.append(tagElem);
    if (isImage && autoDetectImages) {
        var imageElem = $("<div class='image'/>");
        imageElem.css("background-image", "url('" + contentURL + "?size=" + Machinata.Content.PREVIEW_MAX_IMAGE_SIZE + "')");
        previewElem.append(imageElem);
        previewElem.addClass("category-image");
    } else {
        previewElem.addClass("category-other");
    }
    //previewElem.attr("title", title);
    // Return
    return previewElem;
};


/// <summary>
///
/// </summary>
Machinata.createFilePreview = function (contentURL, title, autoDetectImages) {
    // Sanity
    if (contentURL == null) return null;
    // Create the elem
    var previewElem = $("<div class='file-preview ui-file-preview'/>");
    return Machinata.updateFilePreview(previewElem, contentURL, title, autoDetectImages);
};






/* ======== Machinata Content Editor Frontned ======== */

Machinata.FrontendContentEditor = {};


Machinata.FrontendContentEditor.bind = function (elements) {
    var elems = elements.find("[data-node-path]");
    elems.hover(function () {
        // Hover in
        var elem = $(this);
        // Remove all other
        $(".content-node-overlay").remove();
        // Create clone overlay
        var position = elem.offset();
        var useNativePositionCalculation = false;
        if (useNativePositionCalculation) {
            var bodyRect = document.body.getBoundingClientRect();
            var elemRect = elem[0].getBoundingClientRect();
            position.top = elemRect.top - bodyRect.top;
            position.left = elemRect.left - bodyRect.left;
        }
        var overlayElem = $("<div></div>");
        overlayElem.addClass("content-node-overlay");
        overlayElem.css("position", "absolute");
        overlayElem.css("background-color", "rgba(0,0,0,0.2)");
        overlayElem.css("width", elem.outerWidth() + "px");
        overlayElem.css("height", elem.outerHeight() + "px");
        overlayElem.css("pointer-events", "none");
        overlayElem.css("box-sizing", "border-box");
        overlayElem.css("border", "3px dotted black");
        overlayElem.css({ top: position.top, left: position.left });
        // Meta infos and link
        var metaElem = $("<div><a></a></div>");
        var metaElemA = metaElem.find("a");
        metaElemA.text(elem.attr("data-node-path"));
        metaElemA.css("color", "white");
        metaElemA.css("background-color", "rgba(0,0,0,0.6)");
        metaElemA.css("font-weight", "bold");
        metaElemA.css("font-size", "10px");
        metaElemA.css("padding", "3px");
        metaElemA.css("pointer-events", "all");
        metaElemA.attr("href", "/admin/content/page/" + elem.attr("data-node-path"));
        metaElemA.attr("target", "_blank");
        overlayElem.append(metaElem)
        // Register
        elem.data("content-node-overlay", overlayElem);
        $("body").append(overlayElem);
    }, function () {
        /*// Hover out
        var elem = $(this);
        // Remove overlay
        var overlayElem = elem.data("content-node-overlay");
        if(overlayElem != null) overlayElem.remove();*/
    });
};

Machinata.ready(function () {
    if (Machinata.Params.string("edit") == "true") {
        // Bind the frontend editor
        Machinata.FrontendContentEditor.bind($("body"));
        // Do we have a node path to find?
        if (Machinata.Params.string("node-path") != null) {
            var elem = $("[data-node-path='" + Machinata.Params.string("node-path") + "'");
            Machinata.UI.scrollTo(elem);
        }
    }
});