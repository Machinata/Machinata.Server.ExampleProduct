

/// <summary>
/// QR Code integrations
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    $(".bb-qrcode").not(".generated").addClass("generated").each(function () {
        var elem = $(this);
        var imgURL = "{cdn-url}/static/qrcode?background=transparent&color=black&size=400&data=" + encodeURIComponent(elem.attr("data-qr-code"));
        var imgURLLarge = "{cdn-url}/static/qrcode?background=white&color=black&size=800&data=" + encodeURIComponent(elem.attr("data-qr-code"));
        var imgElem = $("<img/>");
        imgElem.attr("src", imgURL);
        imgElem.css("cursor", "pointer");
        imgElem.addClass("qrcode");
        elem.append(imgElem);
        elem.click(function () {
            var diag = Machinata.dialog("{text.scan-code}", "<div class='qrcode'></div>", null);
            diag.design("wide");
            var imgElem2 = $("<img/>");
            imgElem2.attr("src", imgURLLarge);
            diag.elem.find(".qrcode").append(imgElem2);
            var dataElem = $("<h2/>");
            dataElem.text(elem.attr("data-qr-code"));
            diag.elem.find(".qrcode").append(dataElem);
            diag.show();
        });
    });

    $(".bb-barcode").not(".generated").addClass("generated").each(function () {
        var elem = $(this);
        var imgURL = "{cdn-url}/static/barcode?background=transparent&color=black&size=400&data=" + encodeURIComponent(elem.attr("data-bar-code"));
        var imgURLLarge = "{cdn-url}/static/barcode?background=white&color=black&size=800&data=" + encodeURIComponent(elem.attr("data-bar-code"));
        var imgElem = $("<img/>");
        imgElem.attr("src", imgURL);
        imgElem.css("cursor", "pointer");
        imgElem.addClass("qrcode");
        elem.append(imgElem);
        elem.click(function () {
            var diag = Machinata.dialog("{text.bar-code}", "<div class='qrcode'></div>", null);
            diag.design("wide");
            var imgElem2 = $("<img/>");
            imgElem2.attr("src", imgURLLarge);
            diag.elem.find(".qrcode").append(imgElem2);
            var dataElem = $("<h2/>");
            dataElem.text(elem.attr("data-qr-code"));
            diag.elem.find(".qrcode").append(dataElem);
            diag.show();
        });
    });
});



