/// <summary>
/// If a version exists, validate the version differences and perform the necessary actions (such as warnings or calling npoConflict)
/// </summary>
/// <hidden/>
if (MACHINATA_GLOBAL.MACHINATA_JQUERY != null) {
    // Nothing todo here
} else if (MACHINATA_GLOBAL.MACHINATA_JQUERY_ALREADY_EXISTS == true) {
    // Helper function
    function compareVersion(a, b) {
        if (a == b) {
            return 0;
        }
        var asegs = a.split(".");
        var bsegs = b.split(".");
        var len = Math.min(asegs.length, bsegs.length);
        for (var i = 0; i < len; i++) {
            if (parseInt(asegs[i]) > parseInt(bsegs[i])) {
                return 1; // a bigger than b
            }
            if (parseInt(asegs[i]) < parseInt(bsegs[i])) {
                return -1; // b bigger than a
            }
        }
        if (asegs.length > bsegs.length) {
            return 1;
        }
        if (asegs.length < bsegs.length) {
            return -1;
        }
        return 0;
    }
    console.log("Machinata: will $.noConflict() jQuery: using version " + MACHINATA_GLOBAL.MACHINATA_JQUERY_EXISTING_VERSION + " instead of packaged version " + MACHINATA_GLOBAL.MACHINATA_JQUERY_REQUIRED_VERSION);
    if (compareVersion(MACHINATA_GLOBAL.MACHINATA_JQUERY_EXISTING_VERSION, MACHINATA_GLOBAL.MACHINATA_JQUERY_REQUIRED_VERSION) < 0) {
        console.warn("Machinata: warning: existing jQuery version (" + MACHINATA_GLOBAL.MACHINATA_JQUERY_EXISTING_VERSION + ") is less than " + MACHINATA_GLOBAL.MACHINATA_JQUERY_REQUIRED_VERSION + " (see MACHINATA_REQUIRED_JQUERY_VERSION). Please update your jQuery version or use the packaged version by turning off no-conflict mode (see MACHINATA_JQUERY_NO_CONFLICT_MODE).");
    }
    if (MACHINATA_GLOBAL.MACHINATA_JQUERY_NO_CONFLICT_MODE == true) {
        MACHINATA_GLOBAL.MACHINATA_JQUERY = window.$.noConflict(); // See https://api.jquery.com/jquery.noconflict/
    } else {
        MACHINATA_GLOBAL.MACHINATA_JQUERY = window.$;
    }
} else {
    MACHINATA_GLOBAL.MACHINATA_JQUERY = window.$;
}