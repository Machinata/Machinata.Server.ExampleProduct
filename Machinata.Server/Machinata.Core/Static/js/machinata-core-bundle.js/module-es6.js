
/// <summary>
/// Provides ES6 module loading support
/// </summary>
var Machinata = {};
MACHINATA_GLOBAL.MACHINATA = Machinata;
export default Machinata;