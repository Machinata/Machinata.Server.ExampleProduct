

/// <summary>
/// Extensions related to use login and logout.
/// </summary>

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata._onLogins = [];

/// <summary>
/// Bind a handler to a login event.
/// </summary>
Machinata.onLogin = function (fn) {
    Machinata._onLogins.push(fn);
};

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata._onLogouts = [];

/// <summary>
/// Bind a handler to a logout event.
/// </summary>
Machinata.onLogout = function (fn) {
    Machinata._onLogouts.push(fn);
};

/// <summary>
/// Executes the standard login procedure after recieving a login API packet.
/// This can be used on a login form.
/// </summary>
Machinata.doLogin = function (message, redirect) {
    Machinata.registerAuthToken(message);
    if (redirect == null) {
        if (Machinata.queryParameter("redirect") != "") {
            redirect = Machinata.queryParameter("redirect");
        }
    }
    if (redirect == null) window.location.reload();
    else window.location = redirect;
};

/// <summary>
/// </summary>
Machinata.doVerifiedLogin = function (message, redirect, verifyPage) {
    Machinata.registerAuthToken(message);
    if (redirect == null) {
        if (Machinata.queryParameter("redirect") != "") {
            redirect = Machinata.queryParameter("redirect");
        }
    }
    if (redirect == null) redirect = window.location;
    if (verifyPage == null) verifyPage = "/admin/verify-login";

    window.location = verifyPage + "?redirect=" + encodeURIComponent(redirect);
};

/// <summary>
/// 
/// </summary>
Machinata.logout = function (redirect) {
    Machinata.apiCall("/api/logout").success(function (message) {
        Machinata.doLogout(message, redirect)
    }).send();
};

/// <summary>
/// 
/// </summary>
Machinata.doLogout = function (message, redirect) {
    Machinata.unregisterAuthToken();
    if (redirect == null) window.location.reload();
    else window.location = redirect;
};

/// <summary>
/// 
/// </summary>
Machinata.registerAuthToken = function (message) {
    Machinata.debug("registerAuthToken");
    // Set cookies
    var expires = null;
    var expiresStr = message.data["auth-token"]["expires"];
    if (expiresStr != null && expiresStr != "") {
        expires = new Date(expiresStr); // set to datetime obj
    } else {
        // Fallback (backwards compatibility
        var expiresInDaysStr = message.data["auth-token"]["expires-in-days"];
        if (expiresStr != null && expiresStr != "") {
            var expiresInDays = parseInt(expiresInDaysStr);
            expires = expiresInDays; // set to days (number)
        }
    }
    
    $.cookie("AuthTokenHash", message.data["auth-token"]["hash"], { expires: expires, path: '/' });
    $.cookie("AuthTokenCreated", message.data["auth-token"]["created"], { expires: expires, path: '/' });
    $.cookie("AuthTokenExpires", message.data["auth-token"]["expires"], { expires: expires, path: '/' });
    // Trigger events
    for (var i = 0; i < Machinata._onLogins.length; i++) Machinata._onLogins[i]();
};

/// <summary>
/// 
/// </summary>
Machinata.unregisterAuthToken = function () {
    Machinata.debug("unregisterAuthToken");
    $.cookie("AuthTokenHash", null, { path: '/' });
    $.cookie("AuthTokenCreated", null, { path: '/' });
    $.cookie("AuthTokenExpires", null, { path: '/' });
    for (var i = 0; i < Machinata._onLogouts.length; i++) Machinata._onLogouts[i]();
};

