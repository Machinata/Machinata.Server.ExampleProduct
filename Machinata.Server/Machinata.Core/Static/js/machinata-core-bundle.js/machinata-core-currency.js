


/// <summary>
/// Currency related routines for web shops.
/// </summary>
/// <type>namespace</type>
Machinata.Currency = {};

Machinata.Currency.DEFAULT_CURRENCY = "CHF"; //TODO read from config
Machinata.Currency.DEFAULT_LOCALE = "de-CH"; //TODO read from config

Machinata.Currency.getFormattedString = function (val, currency, locale, digits, zeroCents) {
    if (currency == null) currency = Machinata.Currency.DEFAULT_CURRENCY; // default
    if (locale == null) locale = Machinata.Currency.DEFAULT_LOCALE; // default
    if (digits == null) digits = 0; // default

    var formatter = new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: currency,
        minimumFractionDigits: digits,
        maximumFractionDigits: digits
    });
    var ret = formatter.format(val);
    if (zeroCents != null) ret = ret.replace(".00", zeroCents); //TODO: replace only end?
    ret = ret.replaceAll("$", "USD");
    ret = ret.replace(/\u00A0/g, ' '); // replace non-breaking spaces 
    return ret;
};

/// <summary>
///
/// </summary>
Machinata.Currency.getValueFromString = function (str) {
    if (str == null) return null;

    var p = new Price(str);
    return p.value;
};



/// <summary>
/// Price class implemented 1:1 as Machinata Server Price
/// </summary>
function Price(str) {
    this.value = 0;
    this.currency = Machinata.Currency.DEFAULT_CURRENCY;
    if (str != null && str != "") this.parse(str);
}
Price.prototype.parse = function (str) {
    str = str.replaceAll(".-", "");
    str = str.replaceAll(",", "");
    str = str.replaceAll("'", "");
    str = str.replaceAll("’", "");
    var segs = str.split(' ');
    this.value = parseFloat(segs[1]);
    this.currency = segs[0];
};
Price.validateCompatibility = function (a,b) {
    if (a.currency != b.currency) throw "Cannot add two prices with different currencies ("+a.currency+" vs "+b.currency+")!";
};
Price.prototype.add = function (other) {
    // Validate
    Price.validateCompatibility(this, other);
    this.value = this.value + other.value;
};
Price.prototype.equals = function (other) {
    // Validate
    Price.validateCompatibility(this, other);
    return (this.value == other.value);
};
Price.prototype.toString = function () {
    return this.currency + ' ' + this.value.toFixed(2);
};

