


/// <summary>
/// A generic toolkit and infastructure for packaging high-performance interface components for easy use and integration.
/// ## Performance Overview
/// Components provide a very fast data read/write interface with a disjoint UI update lifecycle, allowing for blazing fast
/// and optimised UI updates in the browser with hundreds of instances. This requires a bit more plumbing, but allows for very
/// optimised update calls in the browser graphics stack.
/// </summary>
/// <type>namespace</type>
Machinata.Component = {};


/// <summary>
/// Provides the namespace for all component implementations.
/// For example, if you have created a component called ```SuperFancyWidget```,
/// you can push it to this namespace with the key ```SuperFancyWidget```.
/// </summary>
/// <namespace>Machinata.Component.Library</namespace>
/// <example>
/// ## Component Interface
/// ```
/// Machinata.Component.Library.SuperFancyWidget = {};
/// Machinata.Component.Library.SuperFancyWidget.DefaultConfig = {};
/// Machinata.Component.Library.SuperFancyWidget.DefaultConfig.inputElem = null;
/// Machinata.Component.Library.SuperFancyWidget.DefaultConfig.labelElem = null;
/// Machinata.Component.Library.SuperFancyWidget.DefaultConfig.onChange = null;
/// Machinata.Component.Library.SuperFancyWidget.DefaultData = {};
/// Machinata.Component.Library.SuperFancyWidget.DefaultData.value = "Test";
/// Machinata.Component.Library.SuperFancyWidget.build = function (instance) {
///     // Shortcuts
///     var config = instance.config, data = instance.data, impl = instance.impl, containerElem = instance.config.containerElem;
/// 
///     // Build UI
///     if (config.inputElem != null) {
///         instance.inputElem = config.inputElem;
///     } else {
///         instance.inputElem = $("<input></input>");
///         containerElem.append(instance.inputElem);
///     }
///     instance.inputElem.val(data.value); // set the initial state
/// 
///     // Bind events
///     instance.inputElem.on("keyup", function (event) {
///         instance.update();
///     });
///     instance.inputElem.on("change", function (event) {
///         if(config.onChange != null) config.onChange(event); // trigger change event when the input is changed
///     });
/// };
/// Machinata.Component.Library.SuperFancyWidget.update = function (instance) {
///     // Shortcuts
///     var config = instance.config, data = instance.data, impl = instance.impl, containerElem = instance.config.containerElem;
///
///     // Sync the browser input with our component data
///     var inputVal = instance.inputElem.val();
///     data.value = inputVal;
///
///     // do something...
/// };
/// ```
/// </example>
Machinata.Component.Library = {};

/// <summary>
/// Definees the default global config values that trickle down to all components' config.
/// If a component has their own default config value defined, or if a custom config is passed
/// in the component build routine, then these values are overridden.
/// </summary>
Machinata.Component.DefaultConfig = {};

/// <summary>
/// Each component can have a container element (jQuery selector) defined.
/// If no component is defined, then it is injected directly into the page where the build routine is called.
/// </summary>
Machinata.Component.DefaultConfig.containerElem = null;

/// <summary>
/// Defines where the assets are stored for the server environment.
/// </summary>
Machinata.Component.DefaultConfig.assetPath = "/static/file/";

/// <summary>
/// A universal handler you can hook into for when components are built.
/// Usefull for running some global post-build hooks/updates.
/// </summary>
Machinata.Component.onBuild = null;

/// <summary>
/// Builds a component using the specified implementation.
/// The parameter ```type``` is a string that must match a implementation registered in ```Machinata.Component.Library```.
/// This method with return the component instance which is a new object consisting of:
///  - ```impl```: the component implementation object
///  - ```config```: the component configuration object
///  - ```data```: the component data object
///  - ```update()```: a function call shortcut to call a comonent update
/// Each component implementation can also provide additional instance variables and methods.
/// Note: You must call this build function after the DOM of the page is ready.
/// </summary>
/// <example>
/// ## Simple build call
/// ```
/// Machinata.Component.buildComponent("SuperFancyWidget");
/// ```
/// ## Advanced build call
/// ```
/// var config = {
///     containerElem: $("#my-component-container"), // custom container (or 'location')
///     myCustomConfig: "value"
/// };
/// var data = {
///     myDataPoint1: 100,
///     myDataPoint2: 50
/// };
/// var component = Machinata.Component.buildComponent("SuperFancyWidget");
/// $("#my-other-component").click(function () {
///     component.data.myDataPoint1 = $(this).val();
///     component.update();
/// });
/// ```
/// </example>
Machinata.Component.buildComponent = function (type, config, data, container) {

    // Sanity
    if (config == null) config = {};
    if (data == null) data = {};

    // Create a container on-the-fly if needed
    if (container != null) config.containerElem = container;
    if (config.containerElem == null) {
        var id = "machinata-" + Machinata.guid();
        document.write("<div class='machinata-component-container' id='" + id + "'></div>");
        config.containerElem = $("#" + id);
    } else {
        config.containerElem.addClass("machinata-component-container");
    }

    // Load implementation
    var impl = Machinata.Component.Library[type];
    if (impl == null) throw "The component '" + type + "' could not be found. It must implemented at Machinata.Component.Library." + type + ".";

    // Create merged data
    var mergedData = {};
    Machinata.Util.extend(mergedData, impl.DefaultData);
    Machinata.Util.extend(mergedData, data);


    // Create merged config
    var mergedConfig = {};
    Machinata.Util.extend(mergedConfig, Machinata.Component.DefaultConfig);
    Machinata.Util.extend(mergedConfig, impl.DefaultConfig);
    Machinata.Util.extend(mergedConfig, config);

    // Create component
    var componentElem = $("<div class='machinata-component'></div>");
    componentElem.addClass("machinata-component-" + type);
    componentElem.attr("id", "machinata-" + Machinata.guid());
    if (config.styles != null) {
        for (var i = 0; i < config.styles.length; i++) componentElem.addClass("style-" + config.styles[i]);
    }
    config.containerElem.append(componentElem);

    // Create instance
    var instance = {
        impl: impl,
        config: mergedConfig,
        data: mergedData,
        componentElem: componentElem
    };

    // Bind instance shortcut functions
    instance.update = function () {
        this.impl.update(this);
    };
    instance.resize = function () {
        if (this.impl.resize != null) this.impl.resize(this);
    };

    // Call the implementation build
    impl.build(instance);

    // Responsiveness (only if the implemtation has a resize handler)
    if (impl.resize != null) {
        Machinata.Responsive.onResize(function () {
            impl.resize(instance);
        });
    }

    // onBuild event
    if (Machinata.Component.onBuild != null) Machinata.Component.onBuild(instance);

    return instance;
};