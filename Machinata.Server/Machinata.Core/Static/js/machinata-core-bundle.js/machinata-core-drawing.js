

/// <summary>
/// Collection of powerful drawing tools.
/// </summary>
/// <type>namespace</type>
Machinata.Drawing = {};


/// <summary>
/// Provides a mechanism for automatically rendering a draw loop 
/// using the browsers builtin callbacks.
/// The renderer provides functionality for bookkeeping (fps) as well
/// as start/stop/pause.
/// </summary>
Machinata.Drawing.createRenderer = function (canvasElem, renderCallback) {


    var renderer = {
        enabled: false,
        canvas: canvasElem[0],
        lastRenderTime: new Date().getTime(),
        lastDebugTime: new Date().getTime(),
        fps: 0,
        showStats: false,
        stats: "initializing...",

        start: function () {
            this.enabled = true;
            window.requestAnimationFrame(animate);
        },
        stop: function () {
            this.enabled = false;
        },
        debugStepOnClick: function () {
            this.enabled = false;
            var step = 0;
            canvasElem.click(function () {
                step += 1000 / 60;
                console.log("render step", step);
                render(step); 
            });
            render(step); 
        },
    };

    function render(time) {
        // Get drawing context
        var ctx = renderer.canvas.getContext("2d");
        // Save/resrotre (we provide this to the callback)
        ctx.save(); {
            // Call callback
            renderCallback(ctx, time, renderer);
        } ctx.restore();
        // Stats?
        if (renderer.showStats == true) {
            Machinata.Drawing.debugText(ctx, renderer.stats, 10, 10+3, false);
        }
    }

    function animate(time) {
        // Bookkeeping
        var nowTime = new Date().getTime();
        renderer.deltaRenderTime = nowTime - renderer.lastRenderTime;
        renderer.lastRenderTime = nowTime;
        renderer.fps = Math.round(1000 / renderer.deltaRenderTime);
        if (nowTime - renderer.lastDebugTime > 1000) {
            renderer.lastDebugTime = nowTime;
            renderer.stats = renderer.fps + " fps @ " + renderer.canvas.width+"x"+renderer.canvas.height+" (" + time + ")";
            if (Machinata.DebugEnabled == true) console.log(renderer.stats);
        }
        // Re-render
        if (renderer.enabled == true) {
            render(time);
            window.requestAnimationFrame(animate);
        }
    }
    

    return renderer;
};

/// <summary>
/// 
/// </summary>
Machinata.Drawing.debugText = function (ctx, text, x, y, centered) {
    ctx.save();
    var fontSize = 10;
    var padding = 3;
    var prevComp = ctx.globalCompositeOperation; 
    ctx.globalCompositeOperation = "source-over"; 
    ctx.font = fontSize+"px Arial";
    var metrics = ctx.measureText(text);
    if (x == null) x = 10;
    if (y == null) y = 10;
    if (centered == null) centered = true;
    if (centered == true) x = x - metrics.width / 2;
    // BG
    ctx.fillStyle = "rgba(0,0,0,0.5)";
    ctx.fillRect(
        x - padding,
        y - fontSize ,
        metrics.width + padding + padding,
        fontSize + padding
    );
    // Text
    ctx.fillStyle = "white";
    ctx.fillText(text, x, y);
    ctx.globalCompositeOperation = prevComp; 
    ctx.restore();
};



/// <summary>
/// 
/// </summary>
Machinata.Drawing.cardinalSplinePathForPoints = function (ctx, points, tension, isClosed, numOfSegments) {
    // See https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
    // See https://github.com/gdenisov/cardinal-spline-js/blob/master/src/curve_func.js

    // options or defaults
    tension = (typeof tension === 'number') ? tension : 0.5;
    numOfSegments = numOfSegments ? numOfSegments : 25;

    var pts,									// for cloning point array
        i = 1,
        l = points.length,
        rPos = 0,
        rLen = (l - 2) * numOfSegments + 2 + (isClosed ? 2 * numOfSegments : 0),
        res = new Float32Array(rLen),
        cache = new Float32Array((numOfSegments + 2) * 4),
        cachePtr = 4;

    pts = points.slice(0);

    if (isClosed) {
        pts.unshift(points[l - 1]);				// insert end point as first point
        pts.unshift(points[l - 2]);
        pts.push(points[0], points[1]); 		// first point as last point
    }
    else {
        pts.unshift(points[1]);					// copy 1. point and insert at beginning
        pts.unshift(points[0]);
        pts.push(points[l - 2], points[l - 1]);	// duplicate end-points
    }

    // cache inner-loop calculations as they are based on t alone
    cache[0] = 1;								// 1,0,0,0

    for (; i < numOfSegments; i++) {

        var st = i / numOfSegments,
            st2 = st * st,
            st3 = st2 * st,
            st23 = st3 * 2,
            st32 = st2 * 3;

        cache[cachePtr++] = st23 - st32 + 1;	// c1
        cache[cachePtr++] = st32 - st23;		// c2
        cache[cachePtr++] = st3 - 2 * st2 + st;	// c3
        cache[cachePtr++] = st3 - st2;			// c4
    }

    cache[++cachePtr] = 1;						// 0,1,0,0

    // calc. points
    parse(pts, cache, l);

    if (isClosed) {
        //l = points.length;
        pts = [];
        pts.push(points[l - 4], points[l - 3], points[l - 2], points[l - 1]); // second last and last
        pts.push(points[0], points[1], points[2], points[3]); // first and second
        parse(pts, cache, 4);
    }

    function parse(pts, cache, l) {

        for (var i = 2, t; i < l; i += 2) {

            var pt1 = pts[i],
                pt2 = pts[i + 1],
                pt3 = pts[i + 2],
                pt4 = pts[i + 3],

                t1x = (pt3 - pts[i - 2]) * tension,
                t1y = (pt4 - pts[i - 1]) * tension,
                t2x = (pts[i + 4] - pt1) * tension,
                t2y = (pts[i + 5] - pt2) * tension;

            for (t = 0; t < numOfSegments; t++) {

                var c = t << 2, //t * 4;

                    c1 = cache[c],
                    c2 = cache[c + 1],
                    c3 = cache[c + 2],
                    c4 = cache[c + 3];

                res[rPos++] = c1 * pt1 + c2 * pt3 + c3 * t1x + c4 * t2x;
                res[rPos++] = c1 * pt2 + c2 * pt4 + c3 * t1y + c4 * t2y;
            }
        }
    }

    // add last point
    l = isClosed ? 0 : points.length - 2;
    res[rPos++] = points[l];
    res[rPos] = points[l + 1];

    // add lines to path
    for (i = 0, l = res.length; i < l; i += 2)
        ctx.lineTo(res[i], res[i + 1]);

    return res;
};
