

/// <summary>
/// </summary>
/// <type>namespace</type>
Machinata.Debugging = {};

Machinata.Debugging._start = null;
Machinata.Debugging._timers = null;

/// <summary>
///
/// </summary>
Machinata.Debugging.startTimer = function (id, category) {
    var ms = new Date();
    if (Machinata.Debugging._start == null) Machinata.Debugging._start = ms;
    if (Machinata.Debugging._timers == null) Machinata.Debugging._timers = {};
    var timer = {
        id: id,
        category: category,
        start: ms
    };
    Machinata.Debugging._timers[id] = timer;
    return timer;
};

/// <summary>
///
/// </summary>
Machinata.Debugging.stopTimer = function (id) {
    var timer = Machinata.Debugging._timers[id];
    timer.end = new Date();
    return timer;
};


/// <summary>
///
/// </summary>
Machinata.Debugging.compileTimerStats = function () {
    $.each(Machinata.Debugging._timers, function (key, timer) {
        timer.ellapsed = timer.end - timer.start;
        timer.rstart = timer.start - Machinata.Debugging._start;
        timer.rend = timer.end - Machinata.Debugging._start;
    });
};

/// <summary>
///
/// </summary>
Machinata.Debugging.createTimersDebugPanel = function (title) {
    var data = {};
    var totalEllapsed = 0;
    Machinata.Debugging.compileTimerStats();
    $.each(Machinata.Debugging._timers, function (key, timer) {
        data[key] = [timer.ellapsed + "ms", timer.rstart+"ms >> "+ timer.rend+"ms"];
        totalEllapsed += timer.ellapsed;
    });
    data["Total"] = totalEllapsed + "ms";
    return Machinata.UI.createDebugPanel(title, data);
};


