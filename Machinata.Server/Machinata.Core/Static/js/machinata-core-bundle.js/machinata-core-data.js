


/// <summary>
/// Collection of data-related routines, such as exporting strings as blobs in the browser.
/// </summary>
/// <type>namespace</type>
Machinata.Data = {};


/// <summary>
///
/// </summary>
Machinata.Data.openDataInNewWindow = function (data, mimetype, filename) {

    // Write the bytes of the string to an ArrayBuffer buffer
    var byteString = atob(data);
    var buffer = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(buffer);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // Create download link element
    var downloadNode = document.createElement("a");
    document.body.appendChild(downloadNode);

    // Get blob url
    var blob = new Blob([buffer], { type: mimetype })
    var url = URL.createObjectURL(blob);
    downloadNode.href = url;
    downloadNode.setAttribute("target", "_blank");

    // Setting the file name
    //downloadNode.download = filename;

    // Tricker download
    downloadNode.click();

    // Hide link after some time...
    setTimeout(function () {
        downloadNode.setAttribute("style", "display:none");
    }, 200);

    // Old method: doesnt work on Chrome
    //var url = "data:" + mimetype + ";base64," + encodeURIComponent(data);
    //Machinata.openPage(url);
};

/// <summary>
///
/// </summary>
Machinata.Data.exportBlob = function (blob, mimetype, filename) {
    // Switch on browser support
    if (navigator.msSaveOrOpenBlob) {

        // MS Blob
        blob.type = mimetype;
        navigator.msSaveOrOpenBlob(blob, filename);

    } else if (window.Blob) {

        // Blob API

        blob.type = mimetype;
        

        // Create download link element
        var downloadNode = document.createElement("a");
        document.body.appendChild(downloadNode);

        // Get blob url
        var url = URL.createObjectURL(blob);
        downloadNode.href = url;

        // Setting the file name
        downloadNode.download = filename;

        // Tricker download
        downloadNode.click();

        // Hide link after some time...
        setTimeout(function () {
            downloadNode.setAttribute("style", "display:none");
        }, 200);

    }
};

/// <summary>
///
/// </summary>
Machinata.Data.exportDataAsBlob = function (data, mimetype, filename) {


    // Switch on browser support
    if (navigator.msSaveOrOpenBlob) {

        // MS Blob
        var blob = new Blob(['\ufeff', data], {
            type: mimetype
        });
        navigator.msSaveOrOpenBlob(blob, filename);
        
    } else if(window.Blob) {
    
        // Blob API

        // Write the bytes of the string to an ArrayBuffer buffer
        var byteString = atob(data);
        var buffer = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(buffer);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // Create download link element
        var downloadNode = document.createElement("a");
        document.body.appendChild(downloadNode);

        // Get blob url
        var blob = new Blob([buffer], { type: mimetype })
        var url = URL.createObjectURL(blob);
        downloadNode.href = url;

        // Setting the file name
        downloadNode.download = filename;

        // Tricker download
        downloadNode.click();

        // Hide link after some time...
        setTimeout(function () {
            downloadNode.setAttribute("style", "display:none");
        }, 200);

    } else {

        // Fallback Data URI

        // Create download link element
        var downloadNode = document.createElement("a");
        document.body.appendChild(downloadNode);
        // Create a link to the file
        downloadNode.href = 'data:' + mimetype + ',' + encodeURIComponent(data);
        // Setting the file name
        downloadNode.download = filename;
        // Trigger download
        downloadNode.click();
        // Hide link after some time...
        setTimeout(function () {
            downloadNode.setAttribute("style", "display:none");
        }, 200);
    }

    //TODO: how to cleanup after download?
};


/// <summary>
///
/// </summary>
Machinata.Data.exportObjectAsJSON = function (obj, filename, pretty) {
    // Create JSON string
    var data = null;
    if (pretty == true) data = JSON.stringify(obj, null, 4);
    else data = JSON.stringify(obj);
    // Send blob back to browser
    Machinata.Data.exportDataAsBlob(data, "application/json", filename);
};

Machinata.Data.exportTableAsExcel = function (tableElem, filename) {
    var tableHTML = tableElem[0].outerHTML;
    // Create html XLS
    var data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    data = data + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
    data = data + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    data = data + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
    data = data + tableHTML;
    data = data + '</body></html>';

    // Send to browser
    Machinata.Data.exportDataAsBlob(btoa(data), "application/vnd.ms-excel", filename); //TODO: extend exportDataAsBlob so that the unecessary btoa and then atob conversion doenst happen
};

/// <summary>
///
/// </summary>
Machinata.Data.exportTableAsCSV = function (tableElem, filename) {
    // Init
    var columnSeperator = ",";
    var rowSeperator = "\r\n";
    var stringDelimiter = "\"";
    var useSeperatorMarker = true;
    var data = "";
    // Marker?
    if (useSeperatorMarker == true) {
        data = "sep=" + columnSeperator + rowSeperator;
    }
    // Helper functions
    function addCellToData(cellElem) {
        var insertValue = $(cellElem).text(); //TODO: raw types?
        insertValue = insertValue.replaceAll(stringDelimiter, stringDelimiter + stringDelimiter); // escape
        var useDelimiter = true;
        if (insertValue == null || insertValue == "") useDelimiter = false;
        if (useDelimiter == true) data += stringDelimiter + insertValue + stringDelimiter;
        else data += insertValue;
        // Handle colspan
        if ($(cellElem).attr("colspan") != null) {
            var colspans = parseInt($(cellElem).attr("colspan"));
            for (var i = 1; i < colspans; i++) {
                data += columnSeperator;
            }
        }
    }
    // Create CSV data...
    // Headers
    tableElem.find("thead").each(function (index, theadElem) {
        $(theadElem).find("tr").each(function (index, trElem) {
            $(trElem).find("th").each(function (index, cellElem) {
                if (index != 0) data += columnSeperator;
                addCellToData(cellElem);
            });
            data += rowSeperator;
        });
    });
    // Bodies
    tableElem.find("tbody").each(function (index, tbodyElem) {
        $(tbodyElem).find("tr").each(function (index, trElem) {
            $(trElem).find("td").each(function (index, cellElem) {
                if (index != 0) data += columnSeperator;
                addCellToData(cellElem);
            });
            data += rowSeperator;
        });
    });
    // Send to browser
    Machinata.Data.exportDataAsBlob(btoa(data), "text/csv", filename); //TODO: extend exportDataAsBlob so that the unecessary btoa and then atob conversion doenst happen
};

/// <summary>
/// Clones a javascript object.
/// </summary>
Machinata.Data.deepClone = function (obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = Machinata.Data.deepClone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = Machinata.Data.deepClone(obj[attr]);
        }
        return copy;
    }

    // We dont understand this object, throw error
    throw new Error("Unable to copy obj! Its type isn't supported.");
};