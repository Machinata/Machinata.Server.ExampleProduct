

/// <summary>
/// Common social media routines.
/// </summary>
/// <type>namespace</type>
Machinata.Social = {};

/// <summary>
/// 
/// </summary>
Machinata.Social.getShareURLForService = function (service, url, text, media) {
    if (url == null) {
        url = window.location;
    }
    if (text == null) {
        text = "";
    }

    if (service == "facebook") {
        return "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url);
    }

    if (service == "linkedin") {
        return "https://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(url) + "&title=" + encodeURIComponent(text);
    }

    if (service == "twitter") {
        if (text != null && text != "") return "https://www.twitter.com/share?text=" + encodeURIComponent(text) + "&url=" + encodeURIComponent(url);
        else return "https://www.twitter.com/share?url=" + encodeURIComponent(url);
    }

    if (service == "link") {
        return url;
    }

    if (service == "email") {
        return "mailto:?body=" + encodeURIComponent(text+"\n\n"+url);
    }

    if (service == "pinterest") return "https://pinterest.com/pin/create/button/?url=" + encodeURIComponent(url) + "&description=" + encodeURIComponent(text);
    if (service == "tumblr") return "https://www.tumblr.com/share/photo?source=" + encodeURIComponent(url) + "&click_thru=" + encodeURIComponent(url);
    if (service == "googleplus") return "https://plus.google.com/share?url=" + encodeURIComponent(url);
    if (service == "stumbleupon") return "https://www.stumbleupon.com/submit?url=" + encodeURIComponent(url);
    if (service == "weheartit") return "https://weheartit.com/heart-it/new_entry?via=" + encodeURIComponent(url) + "&encoding=UTF-8&extension_version=3.2.0&hearting_method=heartbutton&href=" + encodeURIComponent(url) + "&original_title=" + encodeURIComponent(text) + "&caption=" + encodeURIComponent(text) + "&popup=1";
    if (service == "reddit") return "https://www.reddit.com/submit?title=" + encodeURIComponent(text) + "&url=" + encodeURIComponent(url);

    return null;
};



/// <summary>
/// 
/// </summary>
Machinata.Social.openShareWindowForService = function(service, url, text, media) {
    // Init
    var w = 700;
    var h = 450;
    if (service == "reddit") {
        w = "auto";
        h = "auto";
    }
    var left = (window.screen.width / 2) - (w / 2);
    var top = (window.screen.height / 2) - (h / 2);
    var shareURL = Machinata.Social.getShareURLForService(service, url, text, media);
    // Open
    if (service == "copylink") {
        Machinata.UI.copyTextToClipboard(shareURL);
    } else if (service == "email") {
        // Open email window
        window.open(shareURL, "_blank");
    } else {
        // Open popup
        window.open(shareURL, "_blank", "menubar=no,location=no,toolbar=no,width=" + w + ",height=" + h + ",status=no,left=" + left + ",top=" + top, false);
    }
}