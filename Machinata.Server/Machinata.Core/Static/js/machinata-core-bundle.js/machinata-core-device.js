


/// <summary>
/// Collection of device-related information and compatibility data.
/// </summary>
/// <type>namespace</type>
Machinata.Device = {};


/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.Device._infoLoaded = false;
Machinata.Device._loadDeviceInfos = function () {
    if (Machinata.Device._infoLoaded == true) return;

    var deviceAgent = navigator.userAgent.toLowerCase();
    Machinata.Device._isIOS = false;
    Machinata.Device._isAndroid = false;
    Machinata.Device._isWindowsPhone = false;
    Machinata.Device._isMSSurface = false;
    Machinata.Device._isMSIE = false;
    Machinata.Device._isEdge = false;
    if (deviceAgent != null) {
        // IOS
        if (deviceAgent.indexOf("iphone") > -1) Machinata.Device._isIOS = true;
        else if (deviceAgent.indexOf("ipod") > -1) Machinata.Device._isIOS = true;
        else if (deviceAgent.indexOf("ipad") > -1) Machinata.Device._isIOS = true;
        // Android
        Machinata.Device._isAndroid = deviceAgent.indexOf("android") > -1;
        // Windows Phone
        Machinata.Device._isWindowsPhone = deviceAgent.indexOf("windows phone") > -1;
        // MS Surface
        //TODO: not very accurate, but also not really a better way to tell
        if (deviceAgent.indexOf("windows nt") > -1 && deviceAgent.indexOf("touch") > -1) {
            Machinata.Device._isMSSurface = true;
        } 
        // MS Internet Explorer
        if (deviceAgent.indexOf("msie ") > -1) {
            Machinata.Device._isMSIE = true;
        } else if (deviceAgent.match(/trident.*rv\:11\./)) {
            Machinata.Device._isMSIE = true;
        }
        // Edge
        if (deviceAgent.indexOf("edge/") > -1) {
            Machinata.Device._isEdge = true;
        }
        // Register
        Machinata.Device._infoLoaded = true;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Device._isTouchEnabled = null;
Machinata.Device.isTouchEnabled = function () {
    // Cached?
    if (Machinata.Device._isTouchEnabled != null) return Machinata.Device._isTouchEnabled;

    // NEW METHOD 2020: (via https://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript)
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function (query) {
        return window.matchMedia(query).matches;
    }
    //if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    //    return true;
    //}
    if (('ontouchstart' in window) || window.DocumentTouch) {
        return true;
    }
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    Machinata.Device._isTouchEnabled = mq(query);
    return Machinata.Device._isTouchEnabled;


    // OLD METHOD: (since browsers started to return touch on all platforms)
    /*
    //TODO: Probably a smarter way
    return Machinata.Device.isMobileOS();
    */
};

/// <summary>
/// Note: Currently no longer works on Chrome browser on Windows
/// </summary>
Machinata.Device._isHighContrast = null;
Machinata.Device.isHighContrast = function () {
    // Cached?
    if (Machinata.Device._isHighContrast != null) return Machinata.Device._isHighContrast;

    //http://jsfiddle.net/karlgroves/XR8Su/6/

    var objDiv, strColor;

    // Create a test div
    objDiv = document.createElement('div');

    //Set its color style to something unusual
    objDiv.style.color = 'rgb(31, 41, 59)';

    // Attach to body so we can inspect it
    document.body.appendChild(objDiv);

    // Read computed color value
    strColor = document.defaultView ? document.defaultView.getComputedStyle(objDiv, null).color : objDiv.currentStyle.color;
    strColor = strColor.replace(/ /g, '');

    // Delete the test DIV
    document.body.removeChild(objDiv);

    // Check if we get the color back that we set. If not, we're in 
    // high contrast mode. 
    if (strColor !== 'rgb(31,41,59)') {
        Machinata.Device._isHighContrast = true;
    } else {
        Machinata.Device._isHighContrast = false;
    }
    return Machinata.Device._isHighContrast;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isIOS = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isIOS;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isAndroid = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isAndroid;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isWindowsPhone = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isWindowsPhone;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isMSSurfacePhone = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isMSSurface;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isMSIE = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isMSIE;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isEdge = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isEdge;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isMobileOS = function () {
    Machinata.Device._loadDeviceInfos();
    return Machinata.Device._isIOS || Machinata.Device._isAndroid || Machinata.Device._isWindowsPhone || Machinata.Device._isMSSurface;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.getIOSVersion = function () {
    if (Machinata.Device.isIOS() == false) return {
        major: 0,
        minor: 0,
        revision: 0
    };
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
    return {
        major: parseInt(v[1], 10),
        minor: parseInt(v[2], 10),
        revision: parseInt(v[3] || 0, 10)
    };
};


/// <summary>
/// 
/// </summary>
Machinata.Device.getAndroidVersion = function () {
    if (Machinata.Device.isAndroid() == false) return {
        major: 0,
        minor: 0,
        revision: 0
    };
    var v = (navigator.userAgent).toLowerCase().match(/android\s([0-9\.]*)/)[1];
    var segs = v.split('.');
    return {
        major: parseInt(segs[0], 10),
        minor: parseInt(segs[1] || 0, 10),
        revision: parseInt(segs[2] || 0, 10)
    };
};

/// <summary>
/// 
/// </summary>
Machinata.Device.isChromeBrowser = function () {
    return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
};


/// <summary>
/// 
/// </summary>
Machinata.Device.supportsThreeD = function () {
    // All IE browsers don't
    if (Machinata.Device.isMSIE() == true) return false;
    // All others that support WebGL...
    return Machinata.Device.supportsWebGL();
};

/// <summary>
/// 
/// </summary>
Machinata.Device.supportsWebGL = function () {
    try {
        var canvas = document.createElement('canvas');
        return !!(window.WebGLRenderingContext && (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));
    } catch (e) {
        return false;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Device.getAllFeatures = function () {
    return window.bowser;
};

/// <summary>
/// 
/// </summary>
Machinata.Device.waitForWebfonts = function(fonts, callback) {
    // From https://stackoverflow.com/questions/4383226/using-jquery-to-know-when-font-face-fonts-are-loaded
    var loadedFonts = 0;
    for (var i = 0, l = fonts.length; i < l; ++i) {
        (function (font) {
            var node = document.createElement('span');
            // Characters that vary significantly among different fonts
            node.innerHTML = 'giItT1WQy@!-/#';
            // Visible - so we can measure it - but not on the screen
            node.style.position = 'absolute';
            node.style.left = '-10000px';
            node.style.top = '-10000px';
            // Large font size makes even subtle changes obvious
            node.style.fontSize = '300px';
            // Reset any font properties
            node.style.fontFamily = 'sans-serif';
            node.style.fontVariant = 'normal';
            node.style.fontStyle = 'normal';
            node.style.fontWeight = 'normal';
            node.style.letterSpacing = '0';
            document.body.appendChild(node);

            // Remember width with no applied web font
            var width = node.offsetWidth;

            node.style.fontFamily = font + ', sans-serif';

            var interval;
            function checkFont() {
                // Compare current width with original width
                if (node && node.offsetWidth != width) {
                    ++loadedFonts;
                    node.parentNode.removeChild(node);
                    node = null;
                }

                // If all fonts have been loaded
                if (loadedFonts >= fonts.length) {
                    if (interval) {
                        clearInterval(interval);
                    }
                    if (loadedFonts == fonts.length) {
                        callback();
                        return true;
                    }
                }
            };

            if (!checkFont()) {
                interval = setInterval(checkFont, 50);
            }
        })(fonts[i]);
    }
};

/// <summary>
/// Shows warning messages for browsers that are no longer supported.
/// No longer supported browsers:
///  - Microsoft Internet Explorer
///    - All Versions
/// </summary>
Machinata.Device.showWarningForOldBrowsers = function () {
    if (Machinata.Device.isMSIE()) {
        Machinata.ready(function () {
            Machinata.messageDialog("{text.device.unsupported.internetexplorer.title}", "{text.device.unsupported.internetexplorer.message}").show();
        });
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Device.getWifiLoginURL = function (ssid, password, encryption) {
    // NOTE: This only generates a URL used for a QR Code. Browsers do not understand these URLs
    // Encryption can be:
    // WPA, WEP, or empty string
    if(encryption == null) encryption = "";
    var WIFI_LOGIN_URL = "WIFI:S:{ssid};T:{encryption};P:{password};;";
    var loginURL = WIFI_LOGIN_URL;
    loginURL = loginURL.replace("{ssid}", ssid);
    loginURL = loginURL.replace("{encryption}", encryption);
    loginURL = loginURL.replace("{password}", password);
    // Does this device support the url?
    if (Machinata.Device.isMobileOS() == false) {
        return false;
    }
    if (Machinata.Device.isIOS() == true) {
        // Only iOS 11+ is supported
        if (Machinata.Device.getIOSVersion().major < 11) {
            //return false;
        }
    }
    if (Machinata.Device.isAndroid() == true) {
        // Only iOS 11+ is supported
        if (Machinata.Device.getAndroidVersion().major < 11) {
            //return false;
        }
    }
    return loginURL;
};