
/// <summary>
/// Very light-weight and fast trigger mechanisim for triggering certain UI classes when
/// DOM elements appear/dissppear.
/// See also machinata-scrollkit-bundle.js as an alternative.
/// </summary>
/// <type>namespace</type>
Machinata.Triggers = {};


/// <summary>
/// 
/// </summary>
Machinata.Triggers.elements = null;


/// <summary>
/// 
/// </summary>
Machinata.Triggers.triggerIds = {};

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    
    // Get a handle on all repsonsive elements/containers
    Machinata.Triggers.elements = [];

    $(".bb-trigger").each(function () {
        // Init
        var elem = $(this);
        // Do we have a ID?
        var triggerId = elem.attr("data-trigger-id");
        if (!String.isEmpty(triggerId)) {
            // Only process the element if it is unqiue id (ie, don't do the id's twice)
            if (Machinata.Triggers.triggerIds[triggerId] != null) return;
            // Register the id
            Machinata.Triggers.triggerIds[triggerId] = elem;
        }
        // Cached data?
        var params = jQuery.data(elem, "trigger-params");
        if (params == null) {
            // Init
            params = {};
            params.event = elem.attr("data-trigger-event");
            params["class"] = elem.attr("data-trigger-class");
            // Get selector
            params.selector = elem.attr("data-trigger-selector");
            if (params.selector == "") params.selector = null;
            // Get elems
            params.elems = null;
            if (params.selector == null) params.elems = elem; //self
            else params.elems = $(params.selector);
            // Update
            jQuery.data(elem, "trigger-params", params);
        }
        // Register elemenet
        Machinata.Triggers.elements.push(elem);
    });

    Machinata.Triggers.updateTriggersElements();
    // Hook on events
    $(window).resize(function () {
        Machinata.Triggers.updateTriggersElements();
    });
    $(window).on('scroll', function () {
        Machinata.Triggers.updateTriggersElements();
    });
});

/// <summary>
/// 
/// </summary>
Machinata.Triggers.updateTriggersElements = function () {
    // Skip if we have none
    if (Machinata.Triggers.elements == null || Machinata.Triggers.elements.length == 0) return;
    // Process all
    for(var i = 0; i < Machinata.Triggers.elements.length; i++) {
        // Init
        var elem = Machinata.Triggers.elements[i];
        // Get cached params
        var params = jQuery.data(elem,"trigger-params");
        // Process each of the elements of the selector
        if (params.event == "appear") {
            if (params.elems.visible()) {
                elem.addClass(params["class"]);
            } else {
                elem.removeClass(params["class"]);
            }
        } else {
            console.error("invalid trigger event: " + params.event);
        }
    }
};