
/// <summary>
/// Common formatting routines for numbers, dates, text and more.
/// </summary>
/// <type>namespace</type>
Machinata.Formatting = {};

/// <summary>
/// 
/// </summary>
Machinata.Formatting.defaultNumberFormatterOptions = {
    minimumFractionDigits: 0,
};


/// <summary>
/// 
/// </summary>
Machinata.Formatting.defaultNumberLocale = 'de-CH';

/// <summary>
/// 
/// </summary>
Machinata.Formatting.defaultNumberFormatter = null;

/// <summary>
/// 
/// </summary>
Machinata.Formatting.setDefaultNumberLocale = function (locale) {
    Machinata.Formatting.defaultNumberFormatter = new Intl.NumberFormat(locale, Machinata.Formatting.defaultNumberFormatterOptions);
}

/// <summary>
/// Formats a number according to the format and locale.
/// </summary>
Machinata.Formatting.formatNumber = function (amount, format, locale, opts) {
    if (opts == null) opts = Machinata.Formatting.defaultNumberFormatterOptions;
    
    if (format == null) {
        // Init default if null (lazy load)
        if (Machinata.Formatting.defaultNumberFormatter == null) Machinata.Formatting.defaultNumberFormatter = new Intl.NumberFormat(Machinata.Formatting.defaultNumberLocale, Machinata.Formatting.defaultNumberFormatterOptions);
        // Set the formatter to the default
        var formatter = Machinata.Formatting.defaultNumberFormatter; // default
        if (locale != null) {
            formatter = new Intl.NumberFormat(locale, opts);
        }
        return formatter.format(amount);
    } else {
        amount = parseFloat(amount);
        if (format == "autoscale") {
            if (amount >= 1000000000) format = "billion";
            else if (amount >= 1000000) format = "million";
            else if (amount >= 1000) format = "thousand";
            opts.maximumFractionDigits = 2;
        }
        var trailer = "";
        if (format == "billion") {
            amount = amount / 1000000000.0;
            trailer = "b"
        } else if (format == "million") {
            amount = amount / 1000000.0;
            trailer = "m"
        } else if (format == "thousand") {
            amount = amount / 1000.0;
            trailer = "k"
        } else if (format == "price") {
            opts.minimumFractionDigits = 2;
            opts.maximumFractionDigits = 2;
        }
        if (locale == null) locale = Machinata.Formatting.defaultNumberLocale;
        var formatter = new Intl.NumberFormat(locale, opts);
        return formatter.format(amount) + trailer;
    }
};




/// <summary>
/// Automatically formats each given element using its attributes:
///     data-format
///     data-locale
/// </summary>
Machinata.Formatting.formatCurrencyElements = function (elements) {
    elements.each(function () {
        var elem = $(this);
        var amount = elem.text();
        var format = elem.attr("data-format");

        elem.text(Machinata.Formatting.formatNumber(amount, format, elem.attr("data-locale")));
        elem.addClass("currency-" + elem.attr("data-currency"));
    });
};



/// <summary>
/// </summary>
Machinata.Formatting.numberToCommasString = function (number, thousandsSeparator) {
    if (thousandsSeparator == null) thousandsSeparator = "'";
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);
};
/// <summary>
/// </summary>
Machinata.Formatting.commaStringToNumber = function (str, thousandsSeparator) {
    if (thousandsSeparator == null) thousandsSeparator = "'";
    str = str.replace(thousandsSeparator, "");
    str = str.replace(/[^\d.-]/g, ''); // remove all non digits (allowing . -)
    var num = parseInt(str);
    return num;
};