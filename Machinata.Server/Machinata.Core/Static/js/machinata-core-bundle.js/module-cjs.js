
/// <summary>
/// Provides CommonJS module loading support
/// </summary>
var Machinata = {};
MACHINATA_GLOBAL.MACHINATA = Machinata;
if (typeof module === 'object' && typeof module.exports === 'object') {
	// CommonJS
	module.exports = Machinata;
}
