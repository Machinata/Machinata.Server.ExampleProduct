

/* ======== Machinata Masonry ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata maps JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Maps = {};
Machinata.maps = Machinata.Maps; // For backwards compatibility, until 2021




/* ======== Machinata Maps ======== */

Machinata.Maps._mapUIDs = 0;

Machinata.Maps.DEFAULT_MAPS_ACCESS_TOKEN = "{config.maps-access-token}";
Machinata.Maps.DEFAULT_MAPS_GEOCODER = null;
Machinata.Maps.DEFAULT_MAPS_OPTIONS = null;

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.ready(function () {
    // Init leaflet
    // Set default geocoder server
    Machinata.Maps.DEFAULT_MAPS_GEOCODER = function (search) {
        // OSM nominatim
        return "https://nominatim.openstreetmap.org/search.php?&format=json&q=" + encodeURIComponent(search);
    }
    Machinata.Maps.DEFAULT_MAPS_OPTIONS = {
        scrollWheelZoom: false
    };
    $(".bb-map").not(".initialized").addClass("initialized").each(function () {
        var elem = $(this);
        var opts = {
            address: elem.attr("data-address")
        };
        if (!String.isEmpty(elem.attr("data-waypoint-1"))) {
            opts.waypoints = [];
            var i = 0;
            while (i < 20) {
                i++;
                var wpnt = {};
                wpnt.address = elem.attr("data-waypoint-" + i);
                if (!String.isEmpty(wpnt.address)) {
                    opts.waypoints.push(wpnt);
                } else {
                    break;
                }
            }
        }
        Machinata.Maps.createMap(elem, opts);
    });
});









/// <summary>
/// 
/// </summary>
Machinata.Maps.createMap = function (elem, opts) {
    // Init
    if (opts == null) opts = {};
    Machinata.Maps._mapUIDs++;
    if (String.isEmpty(elem.attr("id"))) elem.attr("id", "map_" + Machinata.Maps._mapUIDs);
    // Init options
    if (opts.providerOptions == null) opts.providerOptions = {};
    if (opts.doubleClickZoom != null) opts.providerOptions.doubleClickZoom = opts.doubleClickZoom;
    if (opts.scrollWheelZoom != null) opts.providerOptions.scrollWheelZoom = opts.scrollWheelZoom;
    var mapOpts = Machinata.Util.extend(Machinata.Maps.DEFAULT_MAPS_OPTIONS, opts.providerOptions);

    Machinata.debug("Machinata.Maps.createMap: mapOpts", mapOpts);

    // Init map
    elem.addClass("ui-map");
    var map = L.map(elem.attr("id"), mapOpts);
    map.setView([0, 0], 1);
    var layer = Machinata.Maps.getTileLayer();
    layer.addTo(map);
    elem.data("map", map);
    // Helper functions
    function setMarkerViaGeocode(search, callback) {
        Machinata.Maps.geocode(search, function (results) {
            if (results.length > 0) {
                var loc = results[0];
                var latlng = [loc.lat, loc.lng];
                map.setView(latlng, 13);
                // Create new marker
                if (map.marker) map.marker.remove();
                var markerOpts = {};
                markerOpts.draggable = true;
                var marker = L.marker(latlng, markerOpts).addTo(map).on("moveend", function () {
                    // Marker move
                    elem.trigger("markermove", this.getLatLng());
                });
                map.marker = marker;
                if (callback) callback(loc);
            } else {
                //TODO SHOW ERROR
            }
        });
    }
    // Geocode?
    if (opts.address != null && !String.isEmpty(opts.address)) {
        setMarkerViaGeocode(opts.address);
    }
    // Waypoints?
    if (opts.waypoints != null && opts.waypoints.length > 0) {
        Machinata.Maps.addRouteToMap(map, opts.waypoints);
    }
    // Custom events
    elem.on("update", function (event, args) {
        if (args.address != null) {
            setMarkerViaGeocode(args.address, args.callback);
        }
    });

    return map;
};

/// <summary>
/// 
/// </summary>
Machinata.Maps.createBigImageMap = function (id, imgElem, minZoom, maxZoom, projectionZoom, initialZoom, center) {
    // See https://gist.github.com/longhotsummer/ba9c96bb2abb304e4095ce00df17ae2f
    // create the slippy map
    Machinata.debug("Machinata.Maps.createBigImageMap");
    if(minZoom == null) minZoom = 3;
    if (maxZoom == null) maxZoom = 7;
    if (projectionZoom == null) projectionZoom = 8;
    if (initialZoom == null) initialZoom = 5;
    if (center == null) center = [0, 0];
    var map = L.map(id, {
        minZoom: 3, // zoomed out
        maxZoom: 7, // zoomed in
        center: center,
        zoom: initialZoom,
        crs: L.CRS.Simple
    });
    // dimensions of the image
    var w = imgElem.width(),
        h = imgElem.height(),
        url = imgElem.attr("src");
    Machinata.debug("  w: " + w);
    Machinata.debug("  h: " + w);
    Machinata.debug("  url: " + url);
    // calculate the edges of the image, in coordinate space
    var southWest = map.unproject([0, h], projectionZoom - 1);
    var northEast = map.unproject([w, 0], projectionZoom - 1);
    var bounds = new L.LatLngBounds(southWest, northEast);
    // add the image overlay, 
    // so that it covers the entire map
    L.imageOverlay(url, bounds).addTo(map);
    // tell leaflet that the map is exactly as big as the image
    map.setMaxBounds(bounds);
    map.on('move', function (ev) {
        //console.log(map.getCenter()); // ev is an event object (MouseEvent in this case)
    });
    return map;
}

/// <summary>
/// 
/// </summary>
Machinata.Maps.getTileLayer = function () {
    // Mapbox Tiles (2020 onwards)
    return L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
        tileSize: 512,
        maxZoom: 18,
        zoomOffset: -1,
        id: 'mapbox/streets-v11',
        accessToken: Machinata.Maps.DEFAULT_MAPS_ACCESS_TOKEN
    });
    // Mapbox Classic Tiles (deprecated 2020)
    /*
    return L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: Machinata.DEFAULT_MAPS_ACCESS_TOKEN
    });
    */
};

/// <summary>
/// 
/// </summary>
Machinata.Maps.geocode = function (search, callback) {
    // Init
    var segs = search.split(',');
    // Return if it is lat lng
    if (segs.length == 2) {
        if (isNaN(parseFloat(segs[0])) == false && isNaN(parseFloat(segs[1])) == false) {
            callback([{
                lat: parseFloat(segs[0]),
                lng: parseFloat(segs[1])
            }]);
            return; 
        }
    }
    // Clean search...
    var newSegs = [];
    for (var i = 0; i < segs.length; i++) {
        var seg = segs[i].trim();
        if (seg.toLowerCase().startsWith("c/o")) continue;
        if (seg.toLowerCase().startsWith("postfach")) continue;
        if (seg.toLowerCase().startsWith("p.o. box")) continue;
        newSegs.push(seg);
    }
    var cleaned = newSegs.join(", ");
    if(search != cleaned) Machinata.debug("Geocoding cleaned from '" + search + "' to '"+cleaned+"'...");
    // Cached?
    Machinata.getOrSetCacheWithHashedKey("GEOCODE", cleaned, function (setCacheValue) {
        // Generator
        // Build url and do JSON request
        Machinata.debug("Geocoding '" + cleaned + "'...");
        var url = Machinata.Maps.DEFAULT_MAPS_GEOCODER(cleaned);
        //Machinata.debug(url);
        $.getJSON(url)
            .done(function (json) {
                //Machinata.debug("   Got:" + JSON.stringify(json));
                // Alter each to use lng notation
                for (var i = 0; i < json.length; i++) json[i].lng = json[i].lon;
                setCacheValue(json);
            })
            .fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
                //TODO SHOW ERROR
            });
    }, function (val) {
        // Callback
        callback(val);
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Maps.geocodeAll = function (waypoints, callback) {
    Machinata.debug("Geocoding all waypoints...");
    var i = 0; // Work index
    var ret = [];
    function getNextWaypoint() {
        var wpnt = waypoints[i];
     
        if (wpnt["lat-lng"] != null) {
            var strLatlng = wpnt["lat-lng"].split(',');
            wpnt.latlng = L.latLng(parseFloat(strLatlng[0]), parseFloat(strLatlng[1]));
            ret.push(wpnt);
            i++;
            if (i >= waypoints.length) {
                callback(ret);
            } else {
                getNextWaypoint();
            }
        } else {
            Machinata.debug("Geocoding " + (i + 1) + "/" + waypoints.length + ": " + wpnt.address);
            Machinata.Maps.geocode(wpnt.address, function (results) {
                // Use the first result
                if (results.length > 0) {
                    wpnt.latlng = L.latLng(results[0].lat, results[0].lng);
                    ret.push(wpnt);
                }
                i++;
                if (i >= waypoints.length) {
                    callback(ret);
                } else {
                    getNextWaypoint();
                }
            });
        }
    }
    getNextWaypoint();
}

/// <summary>
/// 
/// </summary>
Machinata.Maps.addRouteToMap = function (map, waypoints) {
    Machinata.debug("Building Route...");
    Machinata.Maps.geocodeAll(waypoints, function (results) {
        var latlngs = [];
        for (var i = 0; i < results.length; i++) {
            latlngs.push(results[i].latlng);
        }
        L.Routing.control({
            waypoints: latlngs,
            show: false,
            router: L.Routing.mapbox(Machinata.Maps.DEFAULT_MAPS_ACCESS_TOKEN)
        }).addTo(map);
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Maps.addHeatToMap = function (map, waypoints) {
    Machinata.Maps.geocodeAll(waypoints, function (results) {
        var markers = [];
        var heat = [];
        for (var i = 0; i < results.length; i++) {
            var d = [results[i].latlng.lat, results[i].latlng.lng, 0.5]; // Optional third argument in each LatLng point (altitude) represents point intensity. Unless max option is specified, intensity should range between 0.0 and 1.0.
            heat.push(d);
            var marker = L.marker(results[i].latlng)
            markers.push(marker);
        }
        // See https://github.com/Leaflet/Leaflet.heat
        var heatmap = L.heatLayer(heat, {radius: 50}).addTo(map);
        // Fit to view
        Machinata.debug("Fit to heat bounds...");
        var group = new L.featureGroup(markers);
        map.fitBounds(group.getBounds(), { padding: [50, 50] });
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Maps.addMarkersToMap = function (map, waypoints, useCluster, jitter) {
    
    Machinata.Maps.geocodeAll(waypoints, function (results) {

        // Custom icon
        //TODO
        /*
        var icon = L.divIcon({
            className: 'map-marker ' ,
            iconSize: null,
            html: '<div class="icon" style="' + "" + '">' + "x" + '</div><div class="arrow" />'
        });*/

        var cluster = null;
        if (useCluster == true) {
            //cluster = L.markerClusterGroup({ spiderfyOnMaxZoom: true, showCoverageOnHover: true, zoomToBoundsOnClick: true });
            cluster = L.markerClusterGroup({
                removeOutsideVisibleBounds: false,
                showCoverageOnHover: true,
                spiderfyOnMaxZoom: false,
                animate: false
            });
        }
        var markers = [];

        Machinata.debug("Machinata.Maps.addMarkersToMap: results from geocodeAll:", results);

        for (var i = 0; i < results.length; i++) {
            var markerOpts = {
                title: results[i].name
            };
            var latlng = results[i].latlng;
            if (jitter != null) {
                latlng.lat += (-Math.random() + Math.random()) * (jitter/50000);
                latlng.lng += (-Math.random() + Math.random()) * (jitter/50000);
            }
            var marker = L.marker(latlng, markerOpts);
            if (useCluster == true) {
                cluster.addLayer(marker);
            } else {
                marker.addTo(map);
            }
            marker.link = results[i].link;
            if (!String.isEmpty(marker.link)) {
                marker.on('click', function (e) {
                    window.open(e.target.link, "_blank");
                });
            } 
            markers.push(marker);
        }
        if (useCluster == true) {
            map.addLayer(cluster);
        }
        // Fit to view
        Machinata.debug("Fit to marker bounds...");
        var group = new L.featureGroup(markers);
        map.fitBounds(group.getBounds(), { padding: [50,50] });
    });
};