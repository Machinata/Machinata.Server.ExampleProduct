/// This files belongs to machinata-filter-bundle.js
/// See bundle.info, bundle.license and bundle.credits for more information.

/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

/// <summary>
/// Machinata Filter JS Library
/// Library for displaying and managing a dynamic search and filter panel.
/// </summary>
/// <type>namespace</type>
Machinata.Filter = {};

/// <summary>
/// 
/// </summary>
Machinata.Filter.Config = {};

/// <summary>
/// 
/// </summary>
Machinata.Filter.Config.delayedUpdateDurationMS = 500;

/// <summary>
/// 
/// </summary>
Machinata.Filter.Config.subFiltersInitiallyHidden = true;


/// <summary>
/// 
/// </summary>
Machinata.Filter.cache = {};

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.allResults = null;

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.allResultGroups = null;

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.allResultGroupsHeaders = null;

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.resultLookupTable = {};

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.resultGroupsLookupTable = {};

/// <summary>
/// 
/// </summary>
Machinata.Filter.cache.resultGroupsHeadersLookupTable = {};

/// <summary>
/// </summary>
Machinata.Filter.init = function (filter, onUpdate) {

    // Set some data properties
    filter.data("bb-filter-on-update", onUpdate);

    // Pre cache result keys
    Machinata.Filter.cache.allResults = $(".bb-filter-result");
    Machinata.Filter.cache.allResultGroups = $(".bb-filter-result-group");
    Machinata.Filter.cache.allResultGroupsHeaders = $(".bb-filter-result-group-header");
    Machinata.Filter.cache.allResults.each(function () {
        var elem = $(this);
        var key = elem.attr("data-filter-key");
        var group = elem.attr("data-group-key");
        Machinata.Filter.cache.resultLookupTable[key] = elem;
        Machinata.Filter.cache.resultGroupsLookupTable[key] = Machinata.Filter.cache.allResultGroups.filter(".results-group-"+group);
        Machinata.Filter.cache.resultGroupsHeadersLookupTable[key] = Machinata.Filter.cache.allResultGroupsHeaders.filter(".results-group-"+group);
    });

    // Common tools
    $(".bb-filter-tool-reset-filters").click(function () {
        filter.find(".bb-filter-option").removeClass("selected");
        filter.find(".bb-filter-search-input").val("");
        Machinata.Filter.queueUpdate(filter);
    });
    $(".bb-filter-tool-update").on("click", function () {
        Machinata.Filter.queueUpdate(filter);
        if (filter.attr("data-auto-focus") == "true") Machinata.Filter.focus(filter);
    });
    $("input.bb-filter-category-toggle").on("change", function () {
        var catElem = filter.find(".bb-filter-category.category-" + $(this).attr("data-filter-category"));
        if ($(this).is(":checked")) {
            catElem.show();
        } else {
            catElem.find(".bb-filter-option").removeClass("selected");
            Machinata.Filter.update(filter, false);
            catElem.hide();
        }
    });
    $("input.bb-filter-search-toggle").on("change", function () {
        var searchElem = filter.find(".bb-filter-search");
        if ($(this).is(":checked")) {
            searchElem.show();
        } else {
            searchElem.find("input").val("");
            Machinata.Filter.update(filter, false);
            searchElem.hide();
        }
    });

    // Bind text search
    filter.find(".bb-filter-search-input").on("change", function () {
        Machinata.Filter.queueUpdate(filter);
    });
    filter.find(".bb-filter-search-input").on("keyup", function () {
        Machinata.Filter.queueDelayedUpdate(filter);
    });

    Machinata.Filter.update(filter, true); // presetUseQueryString only on init!
};

/// <summary>
/// 
/// </summary>
Machinata.Filter.queueDelayedUpdate = function (filter) {
    Machinata.UI.doWithDelay("bb-filter-update", Machinata.Filter.Config.delayedUpdateDurationMS, function () {
        Machinata.Filter.queueUpdate(filter);
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Filter.queueUpdate = function (filter) {
    Machinata.Filter.update(filter);
    //TODO: queue on timeout
};

/// <summary>
/// </summary>
Machinata.Filter.update = function (filter, presetUseQueryString) {
    // Init
    var call = filter.attr("data-api-call");
    var filterURL = "?";
    Machinata.debug("Machinata.Filter.update(): updating...");
    // Create data package
    var data = {};
    var hasQueryParams = false;
    if (presetUseQueryString == true) {
        var params = Machinata.getAllQueryParameter();
        for (var i = 0; i < params.length; i++) {
            var key = params[i];
            var val = Machinata.queryParameter(key);
            data[key] = val;
            hasQueryParams = true;
        }
    }
    var selectedOptions = filter.find(".bb-filter-option.selected");
    selectedOptions.each(function () {
        var optionElem = $(this);
        var key = optionElem.attr("data-filter-key");
        if (data[key] == null) data[key] = optionElem.attr("data-option-value");
        else data[key] += "," + optionElem.attr("data-option-value");
    });
    data["search"] = filter.find(".bb-filter-search-input").val();
    // Compile the url
    for (var key in data) {
        var val = data[key];
        if(val == null || val == "") continue;
        filterURL += (filterURL == "?" ? "" : "&") + key + "=" + encodeURIComponent(val);
    }
    // Do API call
    Machinata.apiCall(call)
        .data(data)
        .success(function (message) {
            Machinata.showLoading(true);
            Machinata.debug("Machinata.Filter.update(): got data");
            // Debug
            filter.attr("data-total", message.data.total);
            filter.attr("data-matches", message.data.matches);
            $(".bb-filter-stat-total").text(message.data.total);
            $(".bb-filter-stat-matches").text(message.data.matches);
            filter.attr("data-current-url", filter.attr("data-base-url") + filterURL);
            $(".bb-filter-url").attr("href", filter.attr("data-current-url"));

            // Main Filters
            Machinata.Filter.updateFilters(filter, "main", message.data["filters"], "Filters", true);
            Machinata.Filter.updateFilters(filter, "sub", message.data["sub-filters"], "Sub Filters", !Machinata.Filter.Config.subFiltersInitiallyHidden);
            // Update buttons states of new filter options
            if (presetUseQueryString == true) {
                var params = Machinata.getAllQueryParameter();
                for (var i = 0; i < params.length; i++) {
                    var key = params[i];
                    var val = Machinata.queryParameter(key);
                    // Find matching option
                    filter.find(".bb-filter-group.group-" + key + " .bb-filter-option[data-option-value=\"" + val + "\"]").addClass("selected");
                }
            }
            // Update matches
            Machinata.Filter.cache.allResults.removeClass("matches");
            Machinata.Filter.cache.allResultGroups.removeClass("matches");
            Machinata.Filter.cache.allResultGroupsHeaders.removeClass("matches");
            var results = message.data["results-by-key"];
            var showResults = true;
            if (message.data.total == message.data.matches && filter.attr("data-show-all-results") == "false") showResults = false;
            if (Machinata.Filter.cache.allResults.length == 0) showResults = false;
            if (results != null && showResults == true) {
                for (var i = 0; i < results.length; i++) {
                    var key = results[i];
                    var elem = Machinata.Filter.cache.resultLookupTable[key];
                    var group = Machinata.Filter.cache.resultGroupsLookupTable[key];
                    var groupHeader = Machinata.Filter.cache.resultGroupsHeadersLookupTable[key];
                    elem.addClass("matches");
                    groupHeader.addClass("matches");
                    group.addClass("matches");
                }
            }
            if (showResults == false) {
                $(".bb-filter-results").hide();
                $(".bb-filter-no-results").show();
            } else {
                $(".bb-filter-results").show();
                $(".bb-filter-no-results").hide();
            }
            Machinata.showLoading(false);
            if (hasQueryParams && filter.attr("data-auto-scroll-to-results") == "true") {
                Machinata.Filter.focusResults(filter);
            }
            // On update?
            var onUpdate = filter.data("bb-filter-on-update");
            if (onUpdate) onUpdate(filter, message.data);
        })
        .send();

};

/// <summary>
/// </summary>
Machinata.Filter.focus = function (filter) {
    Machinata.UI.scrollTo(filter, 600, { offset: -$("#header").height() - 60 });
};

/// <summary>
/// </summary>
Machinata.Filter.focusResults = function (filter) {
    Machinata.UI.scrollTo($(".bb-filter-results,.bb-filter-no-results"), 600, { offset: -$("#header").height() - 60 });
};

/// <summary>
/// </summary>
Machinata.Filter.updateFilters = function (filter, category, filters, title, expanded) {
    Machinata.debug("Machinata.Filter.updateFilters(): "+category);
    // Category exist?
    var categoryElem = filter.find(".bb-filter-category.category-" + category);
    if (categoryElem.length == 0) {
        categoryElem = $("<div class='bb-filter-category ui-filter-category'></div>").addClass("category-" + category);
        // Title
        categoryElem.append($("<div class='title'></div>").text(title));
        // Toolbar
        var groupToolbarElem = $("<div class='ui-toolbar bb-filter-category-toolbar option-small ui-gray-bg option-bind-hints option-has-tab'></div>").addClass("toolbar-" + category);
        groupToolbarElem.append($("<div class='tabs'></div>"));
        groupToolbarElem.append($("<label class='title'></label>"));
        groupToolbarElem.append($("<label class='hint'></label>"));
        categoryElem.append(groupToolbarElem);
        // Filters
        categoryElem.append($("<div class='filters'></div>"));
        // Register
        categoryElem.appendTo(filter);
    }
    // Mark all groups for deletions
    categoryElem.find(".bb-filter-group").addClass("mark-for-deletion");
    // Process each filter
    $.each(filters, function (index, item) {
        // Exist?
        var filterElem = filter.find(".bb-filter.filter-" + item.key);
        var filterGroup = filter.find(".bb-filter-group.group-" + item["group-key"]);
        if (filterElem.length == 0) {
            // Find the group it belongs to
            if (filterGroup.length == 0) {
                // Group doesnt exist, we need to completely re-create it...
                filterGroup = $("<div class='bb-filter-group ui-filter-group'></div>").addClass("group-" + item["group-key"]);
                filterGroup.attr("data-filter-group", item["group-key"]);
                // Register in toolbar
                var tabElem = $("<div class='tab'></div>").text(item["group-label"]);
                tabElem.attr("data-filter-group", item["group-key"]);
                tabElem.click(function () {
                    // Tab click
                    var elem = $(this);
                    elem.toggleClass("selected");
                    var filterGroup = filter.find(".bb-filter-group.group-" + elem.attr("data-filter-group"));
                    if (elem.hasClass("selected")) {
                        filterGroup.show();
                    } else {
                        // Unselect any in group
                        filterGroup.find(".bb-filter-option.selected").removeClass("selected");
                        filterGroup.hide();
                        Machinata.Filter.queueUpdate($(this).closest(".bb-filter-panel"));
                    }
                });
                categoryElem.find(".bb-filter-category-toolbar .tabs").append(tabElem);
                // Register
                if (expanded == true) {
                    filterGroup.show();
                    tabElem.addClass("selected");
                } else {
                    filterGroup.hide();
                }
                if (item["priority"] == 0) {
                    filterGroup.prependTo(categoryElem.find(".filters"));
                } else {
                    filterGroup.appendTo(categoryElem.find(".filters"));
                }
            } else {
                // Nothing todo
            }
            filterElem = $("<div class='bb-filter ui-filter'><div class='filter-label'></div><div class='options'></div></div>").addClass("filter-" + item.key);
            filterElem.attr("data-filter-key", item.key);
            filterElem.attr("data-filter-key", item.key);
            filterElem.addClass("style-" + item.style);
            filterElem.find(".filter-label").text(item.label);
            if (item.tooltip != null) filterElem.find(".filter-label").attr("title",item.tooltip);
            filterElem.appendTo(filterGroup);
        }
        if (filterGroup.length > 0) {
            filterGroup.removeClass("mark-for-deletion");
        }
        // Mark all options for deletions
        filterElem.find(".bb-filter-option").addClass("mark-for-deletion");
        // Process each option
        $.each(item.options, function (index, option) {
            // Exist?
            var optionElem = filterElem.find(".bb-filter-option.option-" + option.key);
            if (optionElem.length == 0) {
                optionElem = $("<div class='bb-filter-option ui-filter-option'><div class='option-label'></div><div class='option-matches ui-number'></div><div class='option-progress'></div></div>").addClass("option-" + option.key);
                optionElem.attr("data-filter-key", item.key);
                optionElem.attr("data-option-key", option.key);
                optionElem.attr("data-option-value", option.value);
                optionElem.attr("data-group-key", item["group-key"]);
                optionElem.attr("data-category", category);
                optionElem.find(".option-label").text(option.value);
                optionElem.click(function () {
                    $(this).toggleClass("selected");
                    Machinata.Filter.queueUpdate($(this).closest(".bb-filter-panel"));
                    if (filter.attr("data-auto-focus") == "true" && optionElem.attr("data-category") == "main") Machinata.Filter.focus(filter);
                });
                optionElem.appendTo(filterElem);
            }
            // Update
            optionElem.removeClass("mark-for-deletion");
            optionElem.attr("data-matches",option.matches);
            optionElem.find(".option-matches").text(option.matches);
            var p = Math.ceil(option["matches-percent"] * 100.0);
            optionElem.find(".option-progress").css("width", p + "%");
        });
        // Delete no longer used options
        filterElem.find(".bb-filter-option.mark-for-deletion").remove();
    });
    categoryElem.find(".bb-filter-group.mark-for-deletion").remove();
};