/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata JSShop JS Library
/// </summary>
/// <type>namespace</type>
Machinata.JSShop = {};

Machinata.JSShop.instance = {};
Machinata.JSShop.instance.opts = null;
Machinata.JSShop.instance.elem = null;
Machinata.JSShop.instance.productElems = null;
Machinata.JSShop.instance.cartElem = null;
Machinata.JSShop.instance.paymentProvider = null;




/// <summary>
/// 
/// </summary>
Machinata.JSShop.PaymentProviders = {};


/// <summary>
/// 
/// </summary>
Machinata.JSShop.init = function (elem, opts) {
    Machinata.JSShop.instance = {};
    var inst = Machinata.JSShop.instance;

    // Init
    if (opts == null) opts = {};
    if (opts.paymentProvider == null) opts.paymentProvider = "PayPal";
    if (opts.paymentClientID == null) opts.paymentClientID = "Aaey3Y1ik2tr6sqkDLlJ9CKnpxaUT-kIOPVDQHEV3eqMLGF8MUI-Oq7YtXT2gP6M1EpYuT20yQfbsWpi"; // Nerves Sandbox
    if (opts.persistantCart == null) opts.persistantCart = false;
    if (opts.currency == null) opts.currency = "CHF";
    if (opts.locale == null) opts.locale = "de-CH";
    if (opts.vatRate == null) opts.vatRate = null;
    if (opts.lineItemTemplate == null) opts.lineItemTemplate = "<div class='bb-jsshop-lineitem ui-line-item'><span class='bb-jsshop-lineitem-label label'></span><span class='bb-jsshop-lineitem-price price'></span></div>";
    inst.paymentProvider = Machinata.JSShop.PaymentProviders[opts.paymentProvider];

    // Validate opts
    if (inst.paymentProvider == null) {
        alert("Invalid payment provider!");
        return;
    }
    if (opts.vatRate != null) {
        alert("vatRate is not yet supported due to rounding issues on paypal!");
        return;
    }

    // Register
    inst.elem = elem;
    inst.opts = opts;

    // Init payment provider
    inst.paymentProvider.init();

    // Wait for DOM to be ready
    Machinata.ready(function () {
        // Actions
        $(".bb-jsshop-action-toggle-cart").click(function () {
            var productElem = $(this).closest(".bb-jsshop-product");
            if (productElem.hasClass("bb-jsshop-incart")) {
                Machinata.JSShop.removeProductFromCart(productElem);
            } else {
                Machinata.JSShop.addProductToCart(productElem);
            }
        });
        $(".bb-jsshop-action-increase-quantity,.bb-jsshop-action-decrease-quantity").click(function () {
            var productElem = $(this).closest(".bb-jsshop-product");
            var quantityElem = productElem.find(".bb-jsshop-quantity");
            var min = parseInt(quantityElem.attr("min"));
            var max = parseInt(quantityElem.attr("max"));
            var quantity = parseInt(quantityElem.val());
            if ($(this).hasClass("bb-jsshop-action-increase-quantity")) {
                quantity++;
            } else {
                quantity--;
            }
            if (quantity <= max && quantity >= min) {
                quantityElem.val(quantity);
                Machinata.JSShop.updateCart();
            }
        });
        $(".bb-jsshop-quantity").on("change", function () {
            Machinata.JSShop.updateCart();
        });
        $(".bb-jsshop-shipping input,.bb-jsshop-shipping select,.bb-jsshop-shipping textarea").on("change", function () {
            Machinata.JSShop.updateCart();
        });
        inst.cartElem = $(".bb-jsshop-cart");
        inst.productElems = $(".bb-jsshop-product");

        // Countries
        if (inst.opts.shippingRates != null) {
            var countriesElem = $(".bb-jsshop-country");
            countriesElem.empty();
            Machinata.Util.each(inst.opts.shippingRates,function (index, rate) {
                countriesElem.append($("<option></option>").attr("value", rate.countrycode).text(rate.name));
            });
        }

        // Initial product updates...
        inst.productElems.each(function () {
            Machinata.JSShop.updateProduct($(this),true);
        });

        // Prices
        $(".bb-jsshop-formatted-price").each(function () {
            $(this).text(Machinata.JSShop.getFormattedPrice($(this).text()));
        });

        // Initial cart update
        Machinata.JSShop.updateCart();

    });

    
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.updateProduct = function (productElem, initialUpdate) {
    var inst = Machinata.JSShop.instance;
    if (initialUpdate == null) initialUpdate = false;

    if (initialUpdate == true) {
        // Set quantity
        //productElem.find(".bb-jsshop-quantity").val(inst.opts.initialQuantity); // removed so that page can be refreshed
    }
    
    if (productElem.hasClass("bb-jsshop-incart")) {
        productElem.find(".bb-jsshop-incart-only").show();
        productElem.find(".bb-jsshop-notincart-only").hide();
    } else {
        productElem.find(".bb-jsshop-incart-only").hide();
        productElem.find(".bb-jsshop-notincart-only").show();
    }
    
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.addProductToCart = function (productElem) {
    var inst = Machinata.JSShop.instance;
    productElem.addClass("bb-jsshop-incart");
    Machinata.JSShop.updateProduct(productElem);
    Machinata.JSShop.updateCart();
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.removeProductFromCart = function (productElem) {
    var inst = Machinata.JSShop.instance;
    productElem.removeClass("bb-jsshop-incart");
    Machinata.JSShop.updateProduct(productElem);
    Machinata.JSShop.updateCart();
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.getOrderData = function () {
    var inst = Machinata.JSShop.instance;

    // Order
    var order = {};
    order.currency = inst.opts.currency;

    // Addresses
    order.shippingAddress = {};
    order.shippingAddress.email = $(".bb-jsshop-shipping").find("[name='email']").val();
    order.shippingAddress.firstname = $(".bb-jsshop-shipping").find("[name='firstname']").val();
    order.shippingAddress.lastname = $(".bb-jsshop-shipping").find("[name='lastname']").val();
    order.shippingAddress.address1 = $(".bb-jsshop-shipping").find("[name='address1']").val();
    order.shippingAddress.address2 = $(".bb-jsshop-shipping").find("[name='address2']").val();
    order.shippingAddress.zip = $(".bb-jsshop-shipping").find("[name='zip']").val();
    order.shippingAddress.city = $(".bb-jsshop-shipping").find("[name='city']").val();
    order.shippingAddress.countrycode = $(".bb-jsshop-shipping").find("[name='countrycode']").val();


    // Items
    order.items = [];
    inst.productElems.each(function () {
        var productElem = $(this);
        if (productElem.hasClass("bb-jsshop-incart")) {
            var item = {};
            item.sku = productElem.attr("data-product-sku");
            item.name = productElem.attr("data-product-name");
            if (productElem.find(".bb-jsshop-quantity").val() == null || productElem.find(".bb-jsshop-quantity").val() == "") item.quantity = 1;
            else item.quantity = parseInt(productElem.find(".bb-jsshop-quantity").val());
            if (item.quantity == 0) return;
            item.unitPrice = parseFloat(productElem.attr("data-product-price"));
            item.price = item.unitPrice*item.quantity;
            order.items.push(item);
        }
    });
    // Subtotal
    order.subtotal = 0;
    Machinata.Util.each(order.items, function (index, item) {
        order.subtotal += item.price;
    });
    // VAT
    order.vat = 0;
    if (inst.opts.vatRate != null) {
        order.vat = order.subtotal * inst.opts.vatRate;
    }
    // Shipping
    order.shipping = 0;
    Machinata.Util.each(inst.opts.shippingRates, function (index, rate) {
        if (rate.countrycode == order.shippingAddress.countrycode) order.shipping = rate.shipping;
    });
    // Total
    order.total = order.subtotal + order.vat + order.shipping;


    // Valid?
    order.valid = true;
    if (order.items.length == 0) order.valid = false; // no items
    $(".bb-jsshop-shipping input").each(function () {
        var input = this;
        if (input.validity && input.validity.valid == false) order.valid = false; // input is invalid
    });

    console.log("bb order",order);
    return order;
    
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.getFormattedPrice = function (val) {
    var inst = Machinata.JSShop.instance;
    return Machinata.Currency.getFormattedString(val, inst.opts.currency, inst.opts.locale, 2, ".–");
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.updateCart = function () {
    var inst = Machinata.JSShop.instance;

    var order = Machinata.JSShop.getOrderData();

    // Valid?
    if (order.valid == false) {
        $(".bb-jsshop-checkout-ui").css("pointer-events", "none");
        $(".bb-jsshop-checkout-ui").css("opacity", "0.6");
    } else {
        $(".bb-jsshop-checkout-ui").css("pointer-events", "");
        $(".bb-jsshop-checkout-ui").css("opacity", "");
    }

    // Line items
    inst.cartElem.empty();
    Machinata.Util.each(order.items, function (index,item) {
        var lineItemElem = $(inst.opts.lineItemTemplate);
        var label = item.name;
        if (item.quantity > 1) label = item.quantity+" × " + item.name;
        lineItemElem.find(".bb-jsshop-lineitem-label").text(label);
        lineItemElem.find(".bb-jsshop-lineitem-price").text(Machinata.JSShop.getFormattedPrice(item.price, opts.currency));
        lineItemElem.addClass("type-LineItem");
        inst.cartElem.append(lineItemElem);
    });
    // Subtotal
    {
        var lineItemElem = $(inst.opts.lineItemTemplate);
        lineItemElem.find(".bb-jsshop-lineitem-label").text("{text.subtotal}");
        lineItemElem.find(".bb-jsshop-lineitem-price").text(Machinata.JSShop.getFormattedPrice(order.subtotal));
        lineItemElem.addClass("type-Subtotal");
        inst.cartElem.append(lineItemElem);
    }
    // Shipping
    {
        var lineItemElem = $(inst.opts.lineItemTemplate);
        lineItemElem.find(".bb-jsshop-lineitem-label").text("{text.shipping}");
        lineItemElem.find(".bb-jsshop-lineitem-price").text(Machinata.JSShop.getFormattedPrice(order.shipping));
        lineItemElem.addClass("type-Shipping");
        inst.cartElem.append(lineItemElem);
    }
    // VAT
    {
        var lineItemElem = $(inst.opts.lineItemTemplate);
        if (inst.opts.vatRate != null && inst.opts.vatRate != 0) {
            lineItemElem.find(".bb-jsshop-lineitem-label").text((inst.opts.vatRate * 100) + "% {text.vat}");
        } else {
            lineItemElem.find(".bb-jsshop-lineitem-label").text("{text.vat}");
        }
        lineItemElem.find(".bb-jsshop-lineitem-price").text(Machinata.JSShop.getFormattedPrice(order.vat));
        lineItemElem.addClass("type-VAT");
        inst.cartElem.append(lineItemElem);
    }
    // Total
    {
        var lineItemElem = $(inst.opts.lineItemTemplate);
        lineItemElem.find(".bb-jsshop-lineitem-label").text("{text.total}");
        lineItemElem.find(".bb-jsshop-lineitem-price").text(Machinata.JSShop.getFormattedPrice(order.total));
        lineItemElem.addClass("type-Total");
        inst.cartElem.append(lineItemElem);
    }
};


