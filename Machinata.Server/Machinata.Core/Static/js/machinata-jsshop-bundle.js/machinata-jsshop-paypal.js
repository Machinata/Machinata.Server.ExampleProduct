
/// <summary>
/// 
/// Get PayPal ClientID:
///  - Login to Developer Dashboard with the target account user-account (ie the clients account): https://developer.paypal.com/developer/applications
///  - Switch to "Live"
///  - Add a new App
///  - Copy the ClientID (not secret)
/// 
/// PayPal Documentation:
///  - Integration Guide: https://developer.paypal.com/docs/checkout/integrate/
///  - Manage developer tools on PayPal: https://developer.paypal.com/developer/applications
/// </summary>
Machinata.JSShop.PaymentProviders.PayPal = {};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.PaymentProviders.PayPal.init = function () {
    var inst = Machinata.JSShop.instance;

    var script = document.createElement('script');
    script.onload = function () {
        Machinata.JSShop.PaymentProviders.PayPal.createUI();
    };
    script.src = "https://www.paypal.com/sdk/js?client-id=" + inst.opts.paymentClientID + "&currency="+inst.opts.currency;
    document.head.appendChild(script);


};

Machinata.JSShop.PaymentProviders.PayPal._orderData = null;

/// <summary>
/// 
/// </summary>
Machinata.JSShop.PaymentProviders.PayPal.createUI = function () {

    paypal.Buttons({
        createOrder: function (data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            // See https://developer.paypal.com/docs/api/orders/v2/#orders_create
            Machinata.JSShop.PaymentProviders.PayPal._orderData = Machinata.JSShop.PaymentProviders.PayPal.getOrderData();
            return actions.order.create(Machinata.JSShop.PaymentProviders.PayPal._orderData);
        },
        onApprove: function (data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function (details) {
                // This function shows a transaction success message to your buyer.
                console.log("paypal return data",details);
                console.log("bb order data",Machinata.JSShop.PaymentProviders.PayPal._orderData);
                // The BB version is here:
                // Machinata.JSShop.PaymentProviders.PayPal._orderData
                // Such a paypal details object looks like this:
                /*
                {
                    "create_time": "2020-03-26T12:04:11Z",
                        "update_time": "2020-03-26T12:05:49Z",
                            "id": "1L546937XB7369227",
                                "intent": "CAPTURE",
                                    "status": "COMPLETED",
                                        "payer": {
                        "email_address": "sandbox@nerves.ch",
                            "payer_id": "XT6ZM2PJ5J7SG",
                                "address": {
                            "country_code": "US"
                        },
                        "name": {
                            "given_name": "Sandbox",
                                "surname": "Nerves"
                        }
                    },
                    "purchase_units": [
                        {
                            "description": "Pinzel Nr. 10",
                            "reference_id": "default",
                            "amount": {
                                "value": "135.00",
                                "currency_code": "CHF",
                                "breakdown": {
                                    "item_total": {
                                        "value": "135.00",
                                        "currency_code": "CHF"
                                    },
                                    "shipping": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "handling": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "tax_total": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "insurance": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "shipping_discount": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    }
                                }
                            },
                            "payee": {
                                "email_address": "micha@nerves.ch",
                                "merchant_id": "RYEL7495MDMDY"
                            },
                            "items": [
                                {
                                    "name": "Pinzel Nr. 10",
                                    "unit_amount": {
                                        "value": "45.00",
                                        "currency_code": "CHF"
                                    },
                                    "tax": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "quantity": "1",
                                    "sku": "TODO SKU"
                                },
                                {
                                    "name": "Pinzel Nr. 10",
                                    "unit_amount": {
                                        "value": "45.00",
                                        "currency_code": "CHF"
                                    },
                                    "tax": {
                                        "value": "0.00",
                                        "currency_code": "CHF"
                                    },
                                    "quantity": "2",
                                    "sku": "TODO SKU"
                                }
                            ],
                            "shipping": {
                                "name": {
                                    "full_name": "Sandbox Nerves"
                                },
                                "address": {
                                    "address_line_1": "1 Main St",
                                    "admin_area_2": "San Jose",
                                    "admin_area_1": "CA",
                                    "postal_code": "95131",
                                    "country_code": "US"
                                }
                            },
                            "payments": {
                                "captures": [
                                    {
                                        "status": "PENDING",
                                        "id": "8G817059XJ146760N",
                                        "final_capture": true,
                                        "create_time": "2020-03-26T12:05:49Z",
                                        "update_time": "2020-03-26T12:05:49Z",
                                        "amount": {
                                            "value": "135.00",
                                            "currency_code": "CHF"
                                        },
                                        "seller_protection": {
                                            "status": "ELIGIBLE",
                                            "dispute_categories": [
                                                "ITEM_NOT_RECEIVED",
                                                "UNAUTHORIZED_TRANSACTION"
                                            ]
                                        },
                                        "status_details": {
                                            "reason": "RECEIVING_PREFERENCE_MANDATES_MANUAL_ACTION"
                                        },
                                        "links": [
                                            {
                                                "href": "https://api.sandbox.paypal.com/v2/payments/captures/8G817059XJ146760N",
                                                "rel": "self",
                                                "method": "GET",
                                                "title": "GET"
                                            },
                                            {
                                                "href": "https://api.sandbox.paypal.com/v2/payments/captures/8G817059XJ146760N/refund",
                                                "rel": "refund",
                                                "method": "POST",
                                                "title": "POST"
                                            },
                                            {
                                                "href": "https://api.sandbox.paypal.com/v2/checkout/orders/1L546937XB7369227",
                                                "rel": "up",
                                                "method": "GET",
                                                "title": "GET"
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ],
                        "links": [
                            {
                                "href": "https://api.sandbox.paypal.com/v2/checkout/orders/1L546937XB7369227",
                                "rel": "self",
                                "method": "GET",
                                "title": "GET"
                            }
                        ]
                }*/
                //alert('Transaction completed by ' + details.payer.name.given_name);
                //TODO: @micha create support ticket with order data and paypal data
                //TODO: @micha redirect to success page?

                Machinata.debug(details);
                var inst = Machinata.JSShop.instance;
                if (inst.opts.orderSuccessCallback != null) {
                    inst.opts.orderSuccessCallback(details);
                }

             
            });
        }
    }).render('.bb-jsshop-checkout-ui');
};

/// <summary>
/// 
/// </summary>
Machinata.JSShop.PaymentProviders.PayPal.getOrderData = function () {
    var inst = Machinata.JSShop.instance;
    var order = Machinata.JSShop.getOrderData();

    var TEST = false;
    if (TEST == true) {
        return {
            purchase_units: [{
                items: [{
                    name: "test",
                    quantity: 1,
                    unit_amount: {
                        currency_code: 'CHF',
                        value: 10.00
                    }
                }],
                amount: {
                    currency_code: 'CHF',
                    value: 15.00,
                    breakdown: {
                        item_total: {
                            currency_code: 'CHF',
                            value: 10.00
                        },
                        shipping: {
                            currency_code: 'CHF',
                            value: 5.00
                        }
                    }
                }
            }]
        };
    }

    function paypalAmount(val) {
        return {
            currency_code: order.currency,
            value: val
        };
    }

    // See https://developer.paypal.com/docs/api/orders/v2/#orders_create
    var paypalOrder = {};
    paypalOrder.application_context = {
        shipping_preference: "SET_PROVIDED_ADDRESS"
    };

    // See https://developer.paypal.com/docs/api/orders/v2/#definition-purchase_unit_request
    var purchaseUnit = {};
    paypalOrder.purchase_units = [purchaseUnit];


    // Items
    purchaseUnit.items = [];
    Machinata.Util.each(order.items, function (index, item) {
        // See https://developer.paypal.com/docs/api/orders/v2/#definition-item
        var paypalItem = {};
        paypalItem.name = item.name;
        paypalItem.sku = item.sku;
        paypalItem.quantity = item.quantity;
        paypalItem.unit_amount = paypalAmount(item.unitPrice);
        purchaseUnit.items.push(paypalItem);
    });

    // Amount
    // See https://developer.paypal.com/docs/api/orders/v2/#definition-amount_breakdown
    purchaseUnit.amount = paypalAmount(order.total);
    purchaseUnit.amount.breakdown = {};
    purchaseUnit.amount.breakdown.item_total = paypalAmount(order.subtotal);
    purchaseUnit.amount.breakdown.shipping = paypalAmount(order.shipping);
    purchaseUnit.amount.breakdown.tax_total = paypalAmount(order.vat);

    // Shipping
    purchaseUnit.shipping = {};
    purchaseUnit.shipping.name = {};
    purchaseUnit.shipping.name.full_name = order.shippingAddress.firstname + " "+ order.shippingAddress.lastname;
    purchaseUnit.shipping.address = {}; // see https://developer.paypal.com/docs/api/orders/v2/#definition-shipping_detail.address_portable
    purchaseUnit.shipping.address.address_line_1 = order.shippingAddress.address1;
    purchaseUnit.shipping.address.address_line_2 = order.shippingAddress.address2;
    purchaseUnit.shipping.address.admin_area_1 = null; // The highest level sub-division in a country, which is usually a province, state, or ISO-3166-2 subdivision. Format for postal delivery. For example, CA and not California. Value, by country, is:
    purchaseUnit.shipping.address.admin_area_2 = order.shippingAddress.city; // A city, town, or village. Smaller than admin_area_level_1. 
    purchaseUnit.shipping.address.postal_code = order.shippingAddress.zip;
    purchaseUnit.shipping.address.country_code = order.shippingAddress.countrycode.toUpperCase(); // The two-character ISO 3166-1 code that identifies the country or region. Note: The country code for Great Britain is GB and not UK as used in the top-level domain names for that country. Use the C2 country code for China worldwide for comparable uncontrolled price (CUP) method, bank card, and cross-border transactions.

    console.log("paypalOrder",paypalOrder);
    return paypalOrder;
};
