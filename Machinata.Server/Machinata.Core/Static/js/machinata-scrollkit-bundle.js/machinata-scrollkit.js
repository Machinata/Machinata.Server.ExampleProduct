

/* ======== Machinata Scrollkit ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

/// <summary>
/// Machinata Scrollkit JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Scrollkit = {};

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.Scrollkit._controller = null;

/// <summary>
///
/// </summary>
Machinata.Scrollkit.controller = function () {
    // Init controller, if not already
    if (Machinata.Scrollkit._controller == null) Machinata.Scrollkit._controller = new ScrollMagic.Controller();
    return Machinata.Scrollkit._controller;
};

/// <summary>
///
/// </summary>
Machinata.Scrollkit.createScene = function (opts) {
    // Create the new scene
    var scene = new ScrollMagic.Scene(opts).addTo(Machinata.Scrollkit.controller());
    if (Machinata.DebugEnabled) scene.addIndicators();
    return scene;
};


/// <summary>
///
/// </summary>
Machinata.Scrollkit.createBlockScrollDetector = function (elements, opts) {
    // Init
    if (opts == null) opts = {};

    elements.each(function () {
        var elem = $(this);
        var scene = Machinata.Scrollkit.createScene({ triggerElement: elem, duration: 0, triggerHook: 0 })
            .on("enter leave", function (e) {
                if (e.type == "enter") {
                    Machinata.debug("Machinata.Scrollkit.createBlockScrollDetector: " + e.type + " --> " + elem.attr("data-node-path"));
                    if (opts.onEnter) opts.onEnter(elem);
                } else {
                    Machinata.debug("Machinata.Scrollkit.createBlockScrollDetector: " + e.type + " --> " + elem.prev().attr("data-node-path"));
                    if (opts.onEnter) opts.onEnter(elem.prev()); // when we leave, we assume we enter the next (this is because the duration 100% does not work on responsive designs, so we just always look at the start enter/leave triggers
                }
            });
    });
};

