


/* ======== Machinata Wizard ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (!Machinata) Machinata = {};

/// <summary>
/// Machinata Wizard JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Wizard = {};

Machinata.Wizard.autoStartWizard = false;
Machinata.Wizard.focusDuration = 200;
Machinata.Wizard.focusBoxEnabled = true;
Machinata.Wizard.focusBoxPadding = 8;
Machinata.Wizard.defaultTitle = "{wizard.step}/{wizard.total}";

Machinata.Wizard._activeElems = null;
Machinata.Wizard._activeIndex = null;

/// <summary>
///
/// </summary>
Machinata.Wizard.setup = function () {
    
};

/// <summary>
///
/// </summary>
Machinata.Wizard.next = function () {
    if (Machinata.Wizard._activeIndex >= Machinata.Wizard._activeElems.length) return;
    Machinata.Wizard._activeIndex++;
    Machinata.Wizard.showForElement(Machinata.Wizard._activeElems.eq(Machinata.Wizard._activeIndex));
};

/// <summary>
///
/// </summary>
Machinata.Wizard.prev = function () {
    if (Machinata.Wizard._activeIndex <= 0) return;
    Machinata.Wizard._activeIndex--;
    Machinata.Wizard.showForElement(Machinata.Wizard._activeElems.eq(Machinata.Wizard._activeIndex));
};

/// <summary>
///
/// </summary>
Machinata.Wizard.skip = function () {
    //todo
    Machinata.Wizard._activeIndex = null;
    Machinata.Wizard._activeElems = null;
    Machinata.Wizard.close();
};

/// <summary>
///
/// </summary>
Machinata.Wizard.close = function () {
    //todo
    Machinata.Wizard.getActiveWizard().remove();
};

/// <summary>
///
/// </summary>
Machinata.Wizard.restart = function () {
    Machinata.Wizard.start();
};

/// <summary>
///
/// </summary>
Machinata.Wizard.start = function () {
    var elems = $("[data-wizard-text]");
    elems.each(function () {
        var elem = $(this);
        if (elem.attr("data-wizard-status") == null) {
            elem.attr("data-wizard-status", "new"); //TODO load from cache
        }
    });
    if (elems.length == 0) {
        //TODO
        alert("TODO: no guide content");
        return;
    }

    Machinata.Wizard._activeElems = elems.filter("[data-wizard-status='new']");
    Machinata.Wizard._activeIndex = 0;
    //TODO
    Machinata.Wizard.showForElement(Machinata.Wizard._activeElems.eq(Machinata.Wizard._activeIndex));
};

Machinata.Wizard.getActiveWizard = function () {
    return $(".bb-wizard");
};

Machinata.Wizard.repositionActiveWizard = function () {
    var wizardElem = Machinata.Wizard.getActiveWizard();
    var elem = wizardElem.data("target");

        // Get bounds and create frame around it
    var bounds = Machinata.UI.getElementBoundsInPage(elem);
    var boundsPadded = {
        left: bounds.left - Machinata.Wizard.focusBoxPadding,
        right: bounds.right + Machinata.Wizard.focusBoxPadding,
        top: bounds.top - Machinata.Wizard.focusBoxPadding,
        bottom: bounds.bottom + Machinata.Wizard.focusBoxPadding,
    }

    // Place top or bottom? Left or right?
    var pos = "top";
    if (((bounds.top + bounds.bottom) / 2) < ($(document).height() / 2)) pos = "bottom";

    {
        wizardElem.css("top", "0px");
        wizardElem.css("left", "0px");
        wizardElem.css("width", $(document).width());
        wizardElem.css("height", $(document).height());
    }
    {
        var coverElem = wizardElem.data("cover-top");
        coverElem.css("top", "0px");
        coverElem.css("left", "0px");
        coverElem.css("width", "100%");
        coverElem.css("height", boundsPadded.top + "px");
    }
    {
        var coverElem = wizardElem.data("cover-left");
        coverElem.css("top", boundsPadded.top + "px");
        coverElem.css("left", "0px");
        coverElem.css("width", boundsPadded.left + "px");
        coverElem.css("height", boundsPadded.bottom - boundsPadded.top);
    }
    {
        var coverElem = wizardElem.data("cover-right");
        coverElem.css("top", boundsPadded.top + "px");
        coverElem.css("left", boundsPadded.right + "px");
        coverElem.css("width", ($(document).width() - boundsPadded.right) + "px");
        coverElem.css("height", boundsPadded.bottom - boundsPadded.top);
    }
    {
        var coverElem = wizardElem.data("cover-bottom");
        coverElem.css("top", boundsPadded.bottom + "px");
        coverElem.css("left", "0px");
        coverElem.css("width", "100%");
        coverElem.css("height", ($(document).height() - boundsPadded.bottom) + "px");
    }
    if (pos == "top") {
        var tooltipElem = wizardElem.data("tooltip");
        tooltipElem.find(".arrow").removeClass("top").addClass("bottom");
        tooltipElem.css("top", boundsPadded.top - (tooltipElem.outerHeight() + 6) + "px");
        tooltipElem.css("left", boundsPadded.left + (boundsPadded.right - boundsPadded.left) / 2 - (tooltipElem.width() / 2) + "px");
    } else if (pos == "bottom") {
        var tooltipElem = wizardElem.data("tooltip");
        tooltipElem.find(".arrow").removeClass("bottom").addClass("top");
        tooltipElem.css("top", boundsPadded.bottom + 6 + "px");
        tooltipElem.css("left", boundsPadded.left + (boundsPadded.right - boundsPadded.left) / 2 - (tooltipElem.width() / 2) + "px");
    } else {
        alert("invalid wizard pos");
    }
};


Machinata.Wizard.createUI = function () {
    // Main wizard elem
    var wizardElem = $("<div class='ui-wizard bb-wizard '></div");
    wizardElem.css("position", "absolute");
    wizardElem.css("overflow", "hidden");

    wizardElem.css("z-index", "99999");
    // Top cover
    {
        var coverElem = $("<div class='cover cover-top ui-widget-overlay'></div");
        coverElem.css("position", "absolute");
        wizardElem.append(coverElem);
        wizardElem.data("cover-top", coverElem);
    }
    // Left cover
    {
        var coverElem = $("<div class='cover cover-left ui-widget-overlay'></div");
        coverElem.css("position", "absolute");
        wizardElem.append(coverElem);
        wizardElem.data("cover-left", coverElem);
    }
    // Right cover
    {
        var coverElem = $("<div class='cover cover-right ui-widget-overlay'></div");
        coverElem.css("position", "absolute");
        wizardElem.append(coverElem);
        wizardElem.data("cover-right", coverElem);
    }
    // Bottom cover
    {
        var coverElem = $("<div class='cover cover-bottom ui-widget-overlay'></div");
        coverElem.css("position", "absolute");
        wizardElem.append(coverElem);
        wizardElem.data("cover-bottom", coverElem);
    }
    // Tooltip
    {
        var tooltipElem = $("<div class='ui-tooltip'><div class='arrow'></div><h2></h2><div class='text'></div><div class='buttons ui-controlgroup'></div></div>");
        tooltipElem.css("min-width", "200px");
        tooltipElem.css("max-width", "300px");
        tooltipElem.css("opacity", 0.0);
        tooltipElem.find(".buttons").append("<button class='ui-button prev' onclick='Machinata.Wizard.prev()'>{text.previous}</button>");
        tooltipElem.find(".buttons").append("<button class='ui-button skip' onclick='Machinata.Wizard.skip()'>{text.skip}</button>");
        tooltipElem.find(".buttons").append("<button class='ui-button next' onclick='Machinata.Wizard.next()'>{text.next}</button>");
        tooltipElem.find(".buttons").append("<button class='ui-button close' onclick='Machinata.Wizard.close()'>{text.close}</button>");
        wizardElem.append(tooltipElem);
        wizardElem.data("tooltip", tooltipElem);
    }
    
    // Show
    wizardElem.appendTo($("body"));
    
    return wizardElem;
};

Machinata.Wizard.showForElement = function (elem) {

    var wizardElem = $(".bb-wizard");
    if (wizardElem.length == 0) wizardElem = Machinata.Wizard.createUI();
    wizardElem.data("target", elem);

    wizardElem.find(".ui-tooltip").fadeTo(Machinata.Wizard.focusDuration / 2, 0.0);

    var stickyParent = elem.closest(".ui-sticky-content");
    var hasStickyParent = stickyParent.length > 0;

    // Scroll to elem, if needed
    var focusDelay = 0;
    if (Machinata.UI.isInViewport(elem) == false) {
        focusDelay = Machinata.Wizard.focusDuration * 1.01;
        if (hasStickyParent == false) {
            Machinata.UI.scrollTo(elem, Machinata.Wizard.focusDuration, {
                offset: -$(window).height() * 0.4
            });
        }
    }
    // Wait for scroll
    setTimeout(function () {

        // Update wizard content
        var wizardText = elem.attr("data-wizard-text");
        if (wizardText == null) wizardText = "";
        wizardText = wizardText.replace("{wizard.placeholder}", "TODO: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean imperdiet nisl sit amet vestibulum fringilla.");

        var wizardTitle = elem.attr("data-wizard-title");
        if (wizardTitle == null || wizardTitle == "") {
            if (Machinata.Wizard._activeElems != null) wizardTitle = Machinata.Wizard._activeElems.filter("[data-wizard-title]").first().attr("data-wizard-title");
        }
        if (wizardTitle == null) wizardTitle = Machinata.Wizard.defaultTitle;
        var wizardImage = elem.attr("data-wizard-image");

        if (Machinata.Wizard._activeElems != null) {
            wizardTitle = wizardTitle.replace("{wizard.step}", Machinata.Wizard._activeIndex + 1);
            wizardTitle = wizardTitle.replace("{wizard.total}", Machinata.Wizard._activeElems.length);
        }

        wizardElem.find(".ui-tooltip .text").text(wizardText);
        wizardElem.find(".ui-tooltip h2").text(wizardTitle);

        // Update button states
        if (Machinata.Wizard._activeIndex == null) {
            wizardElem.find(".ui-tooltip .buttons button.close").show();
            wizardElem.find(".ui-tooltip .buttons button.prev").hide();
            wizardElem.find(".ui-tooltip .buttons button.next").hide();
            wizardElem.find(".ui-tooltip .buttons button.skip").hide();
        } else {


            if (Machinata.Wizard._activeIndex > 0) {
                wizardElem.find(".ui-tooltip .buttons button.prev").show();
            } else {
                wizardElem.find(".ui-tooltip .buttons button.prev").hide();
            }

            if (Machinata.Wizard._activeElems != null && Machinata.Wizard._activeIndex < Machinata.Wizard._activeElems.length-1) {
                wizardElem.find(".ui-tooltip .buttons button.next").show();
                wizardElem.find(".ui-tooltip .buttons button.close").hide();
                wizardElem.find(".ui-tooltip .buttons button.skip").show();
            } else {
                wizardElem.find(".ui-tooltip .buttons button.next").hide();
                wizardElem.find(".ui-tooltip .buttons button.close").show();
                wizardElem.find(".ui-tooltip .buttons button.skip").hide();
            }

        }
        Machinata.UI.updateControlGroupClasses(wizardElem.find(".ui-tooltip .buttons"));

        Machinata.Wizard.repositionActiveWizard();
        if (hasStickyParent == false) {
            Machinata.UI.scrollTo(wizardElem.find(".ui-tooltip"), Machinata.Wizard.focusDuration, {
                offset: -$(window).height() * 0.4
            });
        }
        wizardElem.find(".ui-tooltip").fadeTo(Machinata.Wizard.focusDuration / 2, 1.0);
    }, focusDelay);

};

Machinata.Wizard.resetTips = function () {
    Machinata.resetCache("WIZARD_IGNORE_");
    Machinata.messageDialog("{text.wizard.reset-all-tips}", "{text.wizard.reset-all-tips.success}").show();
};

Machinata.Wizard.createTip = function (elem, opts) {
    // Init
    if (elem == null) elem = $("<div/>")
    if (opts == null) opts = {};
    if (opts.text == null) opts.text = elem.attr("data-tip-text");
    if (opts.id == null) opts.id = elem.attr("data-tip-id");

    var infoElem = $("<div class='ui-info ui-has-closeable'><div class='ui-symbol'><div class='panel icon text-icon'>i</div></div><div class='info-text'></div><div class='ui-closeable' title='{text.wizard.dont-show-tip-again}'>{icon.delete}</div><div class='clear'></div></div>");
    infoElem.find(".info-text").text(opts.text);
    infoElem.find(".ui-closeable").click(function () {
        console.log("close tip",opts.id);
        $(this).closest(".ui-wizard-tip").remove();
        Machinata.setCache("WIZARD_IGNORE_" + opts.id,opts.text);
    });
    elem.append(infoElem);

    return elem;
};

Machinata.Wizard.bindTips = function () {
    var tipIdMappings = {}; // If page has multiple tip ids, then automatically increment them
    var tipIndex = 0;
    Machinata.debug("Machinata.Wizard.bindTips()");
    $(".bb-wizard-tip,.ui-wizard-tip").not(".ui-bound").addClass("ui-bound").addClass("ui-wizard-tip").each(function () {
        // Init
        tipIndex++;
        var tipElem = $(this);
        var tipId = tipElem.attr("data-tip-id"); //TODO: 
        if (tipId == null || tipId == "" || tipId == "auto") {
            tipId = window.location.pathname; // fallback
        }
        if (tipIdMappings[tipId] != null) tipId = tipId + tipIndex;
        // Register
        tipIdMappings[tipId] = tipElem;
        Machinata.debug("  "+"Found tip:",tipId);
        // Show?
        var ignore = Machinata.getCache("WIZARD_IGNORE_" + tipId);
        if (ignore != null) {
            tipElem.hide();
            Machinata.debug("  " + "  " +"marked as ignore");
        } else {
            // Create tip and show!
            Machinata.debug("  " + "  " +"will show");
            tipElem.hide();
            Machinata.Wizard.createTip(tipElem, { id: tipId }).show();
        }
    });
};


/// <summary>
/// </summary>
/// <hidden/>
Machinata.ready(function () {
    if (Machinata.Wizard.autoStartWizard == true) {
        Machinata.Wizard.start();
    }

    // Tips
    // We do this on a slight delay to ensure page performance...
    setTimeout(function () {
        Machinata.Wizard.bindTips();
    }, 100);
});
