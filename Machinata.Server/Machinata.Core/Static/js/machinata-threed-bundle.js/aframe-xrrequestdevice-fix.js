﻿// Emergency fix for new Chrome version not allowing access to the following api
// See https://github.com/aframevr/aframe/issues/4354#issuecomment-567052948
// See https://github.com/MozillaReality/WebXR-emulator-extension/issues/100
navigator.xr.requestDevice = navigator.xr.requestDevice || function () {
    return new Promise((resolve, reject) => {
        resolve({
            supportsSession: new Promise((resolve, reject) => {
                resolve({
                    immersive: true,
                    exclusive: true
                });
            })
        });
    });
}; 