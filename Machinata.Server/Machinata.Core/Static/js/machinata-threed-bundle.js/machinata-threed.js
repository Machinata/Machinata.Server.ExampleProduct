


/* ======== Machinata 3D ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata ThreeD JS Library
/// </summary>
/// <type>namespace</type>
Machinata.ThreeD = {};

/// <summary>
/// Helper varialbe to ensure that clicks are not fired twice on touch devices...
/// See https://github.com/aframevr/aframe/issues/3297
/// </summary>
Machinata.ThreeD._clickEventCurrentlyBlocked = false;

/// <summary>
/// Helper function to ensure that clicks are not fired twice on touch devices...
/// Returns true if the event should be ignored.
/// See https://github.com/aframevr/aframe/issues/3297
/// </summary>
Machinata.ThreeD._preventAFrameDoubleEventFire = function () {
    if (Machinata.Device.isTouchEnabled() == false) return false; // only on touch devices

    if (Machinata.ThreeD._clickEventCurrentlyBlocked == true) return true;
    Machinata.ThreeD._clickEventCurrentlyBlocked = true;
    setTimeout(function () {
        Machinata.ThreeD._clickEventCurrentlyBlocked = false;
    }, 2000);
    return false;
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.loadAllScenes = function (delay,selector) {
    // Init
    if (selector == null) selector = "body";
    if (delay == null) delay = 0;
    var elems = $(selector).find(".bb-threed-autoload-scene");
    // Load each
    var i = 0;
    elems.each(function () {
        var elem = $(this);
        elem.addClass("loading");
        // Embedded?
        if (elem.attr("data-scene-mode") == "embedded") {
            //TODO: probably not needed anymore in the newer aframe and patched versions...
            // Add a special css that overrides a-frames annoying behavior of presuming full-screen
            var css = $("<style id='bb-threed-css-overrides'></style");
            css.text(".a-html {bottom: unset !important;left: unset !important;position: unset !important;right: unset !important;top: unset !important;}.a-body {height: unset !important;margin: unset !important;overflow: unset !important;padding: unset !important;width: unset !important;}");
            $("head").append(css);
        }
        // For each scene, wait x*i ms...
        setTimeout(function () {
            // Inject scene into element...
            var page = elem.attr("data-scene-file");
            if (Machinata.DebugEnabled == true) {
                page = page + "?r=" + Math.ceil(Math.random() * 100000000);
            }
            elem.load(page, function () {
                elem.find("a-assets").on('loaded', function () {
                    console.log("assets loaded");
                    elem.find("a-scene").append($("<a-entity bb-loader='bb-loader'/>"));
                });
            });
        }, delay*i);
        i++;
    });
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.updateDynamicTextures = function (elements) {
    Machinata.debug("Machinata.ThreeD.updateDynamicTextures");

    function getDimensionFromAttr(val) {
        var ret = eval(val);
        return ret;
    }
    
    elements.each(function () {
        // Init
        var autoFitOverlay = true;
        var elem = $(this);
        var canvas = this;
        var canvasId = elem.attr("id");
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        Machinata.debug("  ID: " + canvasId);
        // Cache selectors
        var backgroundImg = $(elem.attr("data-canvas-background"));
        var overlayImg = $(elem.attr("data-canvas-overlay"));
        // Draw Background
        Machinata.debug("    drawing bg...");
        ctx.drawImage(backgroundImg[0], 0, 0, canvas.width, canvas.height);
        // Draw overlay
        if (overlayImg.length > 0) {
            Machinata.debug("    drawing overlay...");
            var overlayToUse = overlayImg[0];
            if (elem.attr("data-canvas-overlay-color") != null) overlayToUse = Machinata.Imaging.changeColor(overlayToUse, elem.attr("data-canvas-overlay-color"));
            if (autoFitOverlay == true) {
                Machinata.debug("      drawing image autofit...");
                Machinata.Imaging.drawImageContainFit(
                    ctx, overlayToUse,
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-x")) * canvas.width, // x
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-y")) * canvas.height, // y
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-w")) * canvas.width, // w
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-h")) * canvas.height // h
                );
            } else {
                Machinata.debug("      drawing image directly...");
                ctx.drawImage(
                    overlayToUse,
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-x")) * canvas.width, // x
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-y")) * canvas.height, // y
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-w")) * canvas.width, // w
                    getDimensionFromAttr(elem.attr("data-canvas-overlay-h")) * canvas.height // h
                );
            }
        }

        // Force a-frame to refresh
        // This is a bug on A-Frame 0.9.0
        // Via:
        // https://stackoverflow.com/questions/44851046/use-a-frame-img-assets-inside-javascript
        // https://github.com/aframevr/aframe/blob/master/docs/introduction/developing-with-threejs.md
        // https://github.com/richardanaya/aframe-canvas/blob/master/index.js
        Machinata.debug("  Refreshing THREE materials...");
        var referencingElems = elem.closest("a-scene").find("[src='#" + canvasId + "']");
        // Find all elements that reference this texture
        Machinata.debug("    found " + referencingElems.length);
        // Create texture
        var texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
        referencingElems.each(function () {
            // Save mixin
            //var mixin = $(this).attr("mixin");
            // Update mesh
            //$(this).attr("mixin", null);
            mesh = this.getObject3D('mesh');  // THREE.Mesh
            if (mesh == null) {
                Machinata.debug("    warning: mesh is null for "+this.id);
                return;
            }
            mesh.material = material;
            // Re-apply mixin
            //$(this).attr("mixin", mixin);
        });

    });
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.getSceneElem = function () {
    return $("a-scene");
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.getCameraElem = function (sceneElem) {
    if (sceneElem == null) sceneElem = Machinata.ThreeD.getSceneElem();
    var camElem = sceneElem.find("[camera]");
    if (camElem != null) return camElem;
    camElem = sceneElem.find("[ortho-camera]");
    if (camElem != null) return camElem;
    return null;
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.bindCommonSceneActions = function (sceneElem) {
    $(".bb-threed-action-zoom-in").click(function () {
        Machinata.ThreeD.zoomIn(1.2);
    });
    $(".bb-threed-action-zoom-out").click(function () {
        Machinata.ThreeD.zoomOut(1.2);
    });
    $(".bb-threed-action-toggle-auto-rotate").click(function () {
        if ($(this).hasClass("option-selected")) {
            $(this).removeClass("option-selected");
            Machinata.ThreeD.enableCameraAutoRotate(false);
        } else {
            $(this).addClass("option-selected");
            Machinata.ThreeD.enableCameraAutoRotate(true);
        }
    });
    $(".bb-threed-action-export").click(function () {
        Machinata.ThreeD.exportScene();
    });
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.exportScene = function () {
    // https://github.com/fernandojsg/aframe-gltf-exporter-component/blob/master/examples/basic/index.html
    // https://threejs.org/docs/#examples/en/exporters/GLTFExporter
    var scene = Machinata.ThreeD.getSceneElem();
    var sceneEl = scene[0];
    //sceneEl.systems['gltf-exporter']["export"](null, {});  
    AFRAME.scenes[0].systems['gltf-exporter']["export"]();
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.zoomIn = function (amount) {
    if (amount == null) amount = 1.5;
    var camElem = Machinata.ThreeD.getCameraElem();
    var orbitComponent = camElem[0].components['orbit-controls'];
    orbitComponent.controls.zoomIn(amount);
    //TODO: support if not using orbit controls...
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.zoomOut = function (amount) {
    if (amount == null) amount = 1.5;
    var camElem = Machinata.ThreeD.getCameraElem();
    var orbitComponent = camElem[0].components['orbit-controls'];
    orbitComponent.controls.zoomOut(amount);
    //TODO: support if not using orbit controls...
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.enableRaycaster = function (sceneElem) {
    Machinata.ThreeD.getCameraElem(sceneElem).attr("raycaster", "objects:.selectable; enabled:true");
    return;
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.disableRaycaster = function (sceneElem) {
    Machinata.ThreeD.getCameraElem(sceneElem).attr("raycaster", "objects:.selectable; enabled:false");
    return;
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.setCameraPosition = function (pos, zoom, stopAutoRotate, sceneElem) {
    // Init
    var camElem = Machinata.ThreeD.getCameraElem(sceneElem);

    // Position shorthands
    if (pos == 'left')          pos = { x: -1, y:  0, z:  0 };
    if (pos == 'right')         pos = { x: +1, y:  0, z:  0 };
    if (pos == 'top')           pos = { x:  0, y: +1, z:  0 };
    if (pos == 'bottom')        pos = { x:  0, y: -1, z:  0 };
    if (pos == 'back')          pos = { x:  0, y:  0, z: -1 };
    if (pos == 'perspective') pos = { x: 1, y: 1, z: 1 };

    // Zoom transforms
    if (zoom != null) {
        pos.x = pos.x * zoom;
        pos.y = pos.y * zoom;
        pos.z = pos.z * zoom;
    }

    //TODO: support both orbit controls and plain vanilla cam
    var orbitComponent = camElem[0].components['orbit-controls'];
    // Pause auto rotate
    if (stopAutoRotate != false) {
        orbitComponent.controls.enableDamping = false; // We need to stop damping for immediate stop
        orbitComponent.controls.autoRotate = false;
        orbitComponent.controls.update();
    }
    // Update position
    var camComponent = orbitComponent.controls.object;
    camComponent.position.set(pos.x, pos.y, pos.z);
    if (stopAutoRotate != false) {
        orbitComponent.controls.enableDamping = true; // We need to re-enable damping
    }
    // Final update
    orbitComponent.controls.update();
    orbitComponent.flushToDOM();

};

/// <summary>
///
/// </summary>
Machinata.ThreeD.enableCameraAutoRotate = function (enabled, sceneElem) {
    // https://threejs.org/docs/#examples/en/controls/OrbitControls
    // https://github.com/supermedium/superframe/tree/master/components/orbit-controls
    var camElem = Machinata.ThreeD.getCameraElem(sceneElem);
    var orbitComponent = camElem[0].components['orbit-controls'];
    orbitComponent.controls.autoRotate = enabled;
    orbitComponent.flushToDOM();
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.toggleCameraAutoRotate = function (sceneElem) {
    // https://threejs.org/docs/#examples/en/controls/OrbitControls
    // https://github.com/supermedium/superframe/tree/master/components/orbit-controls
    var camElem = Machinata.ThreeD.getCameraElem(sceneElem);
    var orbitComponent = camElem[0].components['orbit-controls'];
    orbitComponent.controls.autoRotate = !orbitComponent.controls.autoRotate;
    orbitComponent.flushToDOM();
};

/// <summary>
///
/// </summary>
Machinata.ThreeD.enableCameraAutoRotateOnMouseHover = function (timeout, sceneElem) {
    // Init
    if (timeout == null) timeout = 2500; // default
    if (sceneElem == null) sceneElem = Machinata.ThreeD.getSceneElem();
    var mouseTimer = null;
    sceneElem.on("mousemove", function () {
        Machinata.ThreeD.enableCameraAutoRotate(false, sceneElem);
        if (mouseTimer != null) clearTimeout(mouseTimer);
        mouseTimer = setTimeout(function () {
            Machinata.ThreeD.enableCameraAutoRotate(true, sceneElem);
        }, timeout); //TODO
    });
    sceneElem.hover(function () {
        // Nop
    }, function () {
        if (mouseTimer != null) clearTimeout(mouseTimer);
        Machinata.ThreeD.enableCameraAutoRotate(true, sceneElem);
    });
};

Machinata.ThreeD.createHoverableSelectableUI = function (opts, scene) {
    if (scene == null) scene = Machinata.ThreeD.getSceneElem();
    if (opts == null) opts = {};
    if (opts.selector == null) opts.selector = ".selectable";

    var selectableElems = scene.find(opts.selector);

    // Add component for 3d ojbect selection...
    AFRAME.registerComponent('selectable', {
        schema: {},
        init: function () {
            var data = this.data;

            var elem = $(this.el);
            var entityId = elem.attr("entity-id");

            if (opts.selectHandler != null) {
                elem.on("click", function (event) {
                    event.preventDefault();

                    // Make sure click event is not fired twice...
                    if (Machinata.ThreeD._preventAFrameDoubleEventFire()) return;

                    if (opts.allowInteractionHandler != null && opts.allowInteractionHandler() == false) return;

                    if (opts.onlySingleSelection == true) selectableElems.removeClass("selected");
                    if (opts.applySelection != false) elem.addClass("selected");
                    opts.selectHandler(elem);
                });
            }
            

            // Opacity
            if (opts.opacityHover != null) {
                this.el.addEventListener('mouseenter', function () {
                    if (opts.allowInteractionHandler != null && opts.allowInteractionHandler() == false) return;

                    if (opts.opacityOthers != null) selectableElems.attr("opacity", opts.opacityOthers);
                    elem.attr("opacity", opts.opacityHover);
                });
                this.el.addEventListener('mouseleave', function () {
                    selectableElems.attr("opacity", opts.opacityHover);
                });
            }

            // Color
            if (opts.colorHover != null) {
                this.el.addEventListener('mouseenter', function () {
                    if (opts.allowInteractionHandler != null && opts.allowInteractionHandler() == false) return;

                    if (opts.colorOthers != null) selectableElems.attr("color", opts.colorOthers);
                    elem.attr("color-orig", elem.attr("color"));
                    elem.attr("color", opts.colorHover);
                });
                this.el.addEventListener('mouseleave', function () {
                    if (elem.hasClass("selected")) {
                        elem.attr("color", opts.colorHover);
                    } else {
                        elem.attr("color", elem.attr("color-orig"));
                    }
                });
            }

            
        }
    });


};




Machinata.ThreeD.onLoad = function (callback) {
    var scene = Machinata.ThreeD.getSceneElem();
    scene[0].addEventListener('loaded', callback);
};

Machinata.ThreeD.attachObjectsToBoundingBox = function (opts, scene) {
    // Init
    if (scene == null) scene = Machinata.ThreeD.getSceneElem();
    if (opts == null) opts = {};
    if (opts.selector == null) opts.selector = ".selectable";
    if (opts.markerOffsetX == null) opts.markerOffsetX = 0;
    if (opts.markerOffsetY == null) opts.markerOffsetY = 0;
    if (opts.markerOffsetZ == null) opts.markerOffsetZ = 0;
    if (opts.markerHTML == null) opts.markerHTML = "<a-sphere color='yellow' radius='0.5'></a-sphere>";

    // Helper function to read value or get value
    function getValueFromProp(prop,elem) {
        if (Machinata.Util.isFunction(prop)) return prop(elem);
        else return prop;
    }

    // Wait for scene load, then add the marker to x min location of each elem
    scene[0].addEventListener('loaded', function () {
        var elemsToAttachTo = scene.find(opts.selector);
        elemsToAttachTo.each(function () {
            // Init
            var elem = $(this);
            var obj3d = this.object3D;
            // Get bounding box of mesh
            var box = new THREE.Box3().setFromObject(obj3d);
            // Setup the new marker pos
            var markerPos = box.min;
            markerPos.z = (box.min.z + box.max.z) / 2;
            markerPos.y = (box.min.y + box.max.y) / 2;
            markerPos.x += getValueFromProp(opts.markerOffsetX, elem);
            markerPos.y += getValueFromProp(opts.markerOffsetY, elem);
            markerPos.z += getValueFromProp(opts.markerOffsetZ, elem);
            // Create marker
            var markerElem = $(opts.markerHTML);
            markerElem.attr("position", markerPos.x + " " + markerPos.y + " " + markerPos.z);
            elem.append(markerElem);
        });
    });

    
};