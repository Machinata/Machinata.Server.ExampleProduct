﻿
/* ======== A-Frame Extensions ======== */

AFRAME.registerComponent('bb-loader', {
    schema: { type: 'string' },
    init: function () {
        var elem = $(this.el);
        elem.closest(".bb-threed-autoload-scene").removeClass("loading").trigger("loaded"); // legacy
        $(".bb-threed-scene-loading").removeClass("loading").addClass("loaded").trigger("loaded");
    }
});

AFRAME.registerComponent('bb-canvas', {
    schema: {default:""},
    init: function () {
        alert("THIS COMPONENT IS NOT FULLY DEVELOPED");
        this.canvas = $(this.data)[0];
        this.ctx = this.canvas.getContext('2d');
        // Draw on canvas...
        var i = 0;
        var self = this;
        setInterval(function () {
            console.log(i);
            i++;
            self.ctx.fillRect(20 * i, 20 * i, 20, 20);

            // https://stackoverflow.com/questions/44851046/use-a-frame-img-assets-inside-javascript
            // https://github.com/aframevr/aframe/blob/master/docs/introduction/developing-with-threejs.md
            // https://github.com/richardanaya/aframe-canvas/blob/master/index.js
            var texture = new THREE.Texture(self.canvas);
            texture.needsUpdate = true;

            var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
            var el = $("#entity_p1_labeltop")[0];
            var mesh = el.getObject3D('mesh');  // THREE.Mesh
            console.log(mesh.material);
            //mesh.material = material;
            //setTimeout(function () {
                mesh.material = material;
                //texture.needsUpdate = true;
            //}, 0);
            //mesh.material.needsUpdate = true;
        }, 1000);
    }
});


/*
AFRAME.registerComponent('orthographic', {
    init: function () {
        var sceneEl = this.el.sceneEl;
        console.log("orthographic init");
        sceneEl.addEventListener('loaded', () => {
            console.log("orthographic event");
            this.originalCamera = sceneEl.camera;
            this.cameraParent = sceneEl.camera.parent;
            this.orthoCamera = new THREE.OrthographicCamera(-1, 1, 1, -1);
            this.cameraParent.add(this.orthoCamera);
            sceneEl.camera = this.orthoCamera;
            console.log("orthographic created THREE.OrthographicCamera");
        });
    },
    remove: function () {
        this.cameraParent.remove(this.orthoCamera);
        sceneEl.camera = this.originalCamera;
    }
});*/



/**
 * CUSTOM Camera component.
 * Pairs along with camera system to handle tracking the active camera.
 */
delete AFRAME.components['camera'];
AFRAME.registerComponent('camera', {
    schema: {
        active: { default: true },
        far: { default: 10000 },
        fov: { default: 80, min: 0 },
        near: { default: 0.005, min: 0 },
        spectator: { default: false },
        zoom: { default: 1, min: 0 },
        orthographic: { default: false }
    },

    /**
     * Initialize three.js camera and add it to the entity.
     * Add reference from scene to this entity as the camera.
     */
    init: function () {
        var camera;
        var el = this.el;

        // Create camera...
        // Perspective or orthographic?
        if (this.data.orthographic == true) {
            // See https://threejs.org/docs/#api/en/cameras/OrthographicCamera
            camera = this.camera = new THREE.OrthographicCamera(-1, 1, 1, -1); //todo
            //camera = this.camera = new THREE.OrthographicCamera(0, window.innerWidth, -window.innerHeight, 0, -100, 100); //todo
        } else {
            camera = this.camera = new THREE.PerspectiveCamera();
        }
        el.setObject3D('camera', camera);
    },

    /**
     * Update three.js camera.
     */
    update: function (oldData) {
        var data = this.data;
        var camera = this.camera;

        // Update properties.
        camera.aspect = data.aspect || (window.innerWidth / window.innerHeight);
        camera.far = data.far;
        camera.fov = data.fov;
        camera.near = data.near;
        camera.zoom = data.zoom;
        camera.updateProjectionMatrix();

        this.updateActiveCamera(oldData);
        this.updateSpectatorCamera(oldData);
    },

    updateActiveCamera: function (oldData) {
        var data = this.data;
        var el = this.el;
        var system = this.system;
        // Active property did not change.
        if (oldData && oldData.active === data.active || data.spectator) { return; }

        // If `active` property changes, or first update, handle active camera with system.
        if (data.active && system.activeCameraEl !== el) {
            // Camera enabled. Set camera to this camera.
            system.setActiveCamera(el);
        } else if (!data.active && system.activeCameraEl === el) {
            // Camera disabled. Set camera to another camera.
            system.disableActiveCamera();
        }
    },

    updateSpectatorCamera: function (oldData) {
        var data = this.data;
        var el = this.el;
        var system = this.system;
        // spectator property did not change.
        if (oldData && oldData.spectator === data.spectator) { return; }

        // If `spectator` property changes, or first update, handle spectator camera with system.
        if (data.spectator && system.spectatorCameraEl !== el) {
            // Camera enabled. Set camera to this camera.
            system.setSpectatorCamera(el);
        } else if (!data.spectator && system.spectatorCameraEl === el) {
            // Camera disabled. Set camera to another camera.
            system.disableSpectatorCamera();
        }
    },

    /**
     * Remove camera on remove (callback).
     */
    remove: function () {
        this.el.removeObject3D('camera');
    }
});



// This is a very hacky method of binding jQuery tooltips to A-Frame
// By Dan Krusi
AFRAME.registerComponent('tooltip', {
    schema: {
        enabled: { type: 'boolean', default: true }
    },
    init: function () {
        // Init
        var data = this.data;
        var elem = $(this.el);
        var sceneElem = elem.closest("a-scene");
        var _blockTooltip = false; // If true, tooltips are hidden using the opacity prop
        sceneElem.attr("title", " ");

        // Monitor mouse on scene enter/exit
        sceneElem.hover(function () {
            _blockTooltip = true;
            //$(".ui-tooltip").css("opacity", 0.0);
        }, function () {
            _blockTooltip = false;
        });

        // Global event of handling tooltip opens
        // Here we hide the tooltip using opacity if blocked
        $(document).on("tooltipopen", function (event, ui) {
            if (_blockTooltip == true) {
                ui.tooltip.css("opacity", 0.0);
            } else {
                ui.tooltip.css("opacity", 1.0);
            }
        });

        this.el.addEventListener('mouseenter', function () {
            if (data.enabled == false) return;

            _blockTooltip = false;
            $(document).tooltip("option", "content", elem.attr("title"));
            $(".ui-tooltip").css("opacity", 1.0);
        });
        this.el.addEventListener('mouseleave', function () {
            if (data.enabled == false) return;

            _blockTooltip = true;
            $(".ui-tooltip").css("opacity", 0.0);
        });
    }
});



