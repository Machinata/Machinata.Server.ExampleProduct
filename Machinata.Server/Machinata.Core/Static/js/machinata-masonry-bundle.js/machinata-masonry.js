

/* ======== Machinata Masonry ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata Masonry JS library
/// </summary>
/// <type>namespace</type>
Machinata.Masonry = {};


/// <summary>
///
/// </summary>
Machinata.Masonry.createMasonryLayout = function (container, opts) {
    if (container == null || container.length == 0) return null;

    var containerId = Machinata.UI.autoUIDElem(container);

    if (opts == null) opts = {};
    opts.container = "#" + containerId;

    if (opts.breakAt == null) {
        opts.breakAt = {};
        if (opts.columnsDesktop != null) opts.columns = opts.columnsDesktop;
        if (opts.columnsTablet != null) opts.breakAt[Machinata.Responsive.VIEWPORT_MAX_WIDTH_TABLET] = opts.columnsTablet;
        if (opts.columnsMobile != null) opts.breakAt[Machinata.Responsive.VIEWPORT_MAX_WIDTH_MOBILE] = opts.columnsMobile;
    }

    var macy = Macy(opts); // see https://github.com/bigbitecreative/macy.js

    container.on("pagination-updated", function () {
        macy.recalculate();
    });

    return macy;
};