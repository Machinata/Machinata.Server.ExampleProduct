
/* ======== Machinata Carousel ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata Carousel JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Carousel = {};

/// <summary>
/// 
/// </summary>
Machinata.Carousel.create = function (elements, opts) {
    return elements.slick(opts);
};


