using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Worker {

    

    /// <summary>
    /// A very light-weight, simple worker pool utility class for running tasks in a queue model.
    /// Auto-scaling is automatic, where additional worker threads are added as the queue grows,
    /// or removed as the queue size decreases.
    /// This class is thread-safe and can be used in a multi-threaded environment.
    /// </summary>
    public class WorkerPool {
        
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        

        private ConcurrentQueue<WorkerTask> _queue = new ConcurrentQueue<WorkerTask>();
        private List<WorkerThread> _workers = new List<WorkerThread>();
        private object _workersLock = new object();
        private DateTime _lastScale = new DateTime(0);

        public int MinWorkerThreads                     = Core.Config.GetIntSetting("WorkerPoolMinWorkerThreads");
        public int MaxWorkerThreads                     = Core.Config.GetIntSetting("WorkerPoolMaxWorkerThreads");
        public int AutoScalingQueueScaleUpThreshold     = Core.Config.GetIntSetting("WorkerPoolAutoScalingQueueScaleUpThreshold");
        public int AutoScalingQueueScaleDownThreshold   = Core.Config.GetIntSetting("WorkerPoolAutoScalingQueueScaleDownThreshold");
        public int AutoScalingScaleUpIncrement          = Core.Config.GetIntSetting("WorkerPoolAutoScalingScaleUpIncrement");
        public int AutoScalingScaleDownIncrement        = Core.Config.GetIntSetting("WorkerPoolAutoScalingScaleDownIncrement");
        public TimeSpan AutoScalingScaleUpCooldown      = new TimeSpan(0, 0, Core.Config.GetIntSetting("WorkerPoolAutoScalingScaleUpCooldownSeconds")); // h,m,s
        public TimeSpan AutoScalingScaleDownCooldown = new TimeSpan(0, 0, Core.Config.GetIntSetting("WorkerPoolAutoScalingScaleDownCooldownSeconds")); // h,m,s
        public int WorkerThreadTaskProcessTimeoutSeconds = Core.Config.GetIntSetting("WorkerPoolWorkerThreadTaskProcessTimeoutSeconds");

        public WorkerPool() {
            _log("Constructor");
        }

        protected void _log(string message) {
            _logger.Info(message);
        }

        private bool _performAutoScale() {
            // Make sure at least one worker is around
            if (_workers.Count == 0) {
                _log("Auto-Scaling up due to no workers");
                _scale(+1);
                return true;
            }
            // Time to scale?
            bool didScale = false;
            if (_lastScale < DateTime.Now.Subtract(AutoScalingScaleUpCooldown) 
                && _queue.Count > AutoScalingQueueScaleUpThreshold) {
                // Scale up
                    _log("Auto-Scaling up due to large queue (" + _queue.Count + ")");
                _lastScale = DateTime.Now;
                _scale(AutoScalingScaleUpIncrement);
                didScale = true;
            } else if (_workers.Count > 1
                && _lastScale < DateTime.Now.Subtract(AutoScalingScaleDownCooldown)
                && _queue.Count < AutoScalingQueueScaleDownThreshold) {
                // Scale down
                    _log("Auto-Scaling down due to small queue (" + _queue.Count + ")");
                _lastScale = DateTime.Now;
                _scale(-AutoScalingScaleDownIncrement);
                didScale = true;
            }
            return didScale;
        }

        public void QueueTask(WorkerTask task) {
            _log("QueueTask: Queue size " + _queue.Count + ", workers " + _workers.Count);
            _queue.Enqueue(task);
            lock (_workersLock) {
                _checkForLockedWorkerThreads();
                _performAutoScale();
            }
        }

        public void RemoveWorker(WorkerThread worker) {
            lock (_workersLock) {
                _log("Removing worker " + worker.Id());
                _workers.Remove(worker);
            }
        }

        public WorkerTask DequeueTask() {
            //_log("Queue size " + _queue.Count + ", workers " + _workers.Count);
            WorkerTask task;
            _queue.TryDequeue(out task);
            return task;
        }

        /// <summary>
        /// Checks for locked worker threads and removes any which are locked.
        /// A worker thread is considered locked if a task does not finish within
        /// WorkerThreadTaskProcessTimeoutSeconds seconds.
        /// </summary>
        private void _checkForLockedWorkerThreads() {
            for (int i = 0; i < _workers.Count; i++) {
                WorkerThread worker = null;
                try {
                    worker = _workers.ElementAt(i);
                } catch (Exception e) { }
                if (worker == null) break;
                if (worker.CurrentTaskProcessTime() > WorkerThreadTaskProcessTimeoutSeconds * 1000) {
                    _log("Warning: worker thread is locked");
                    _workers.Remove(worker);
                    worker.Abort();
                }
            }
        }

        private void _scale(int count) {
            _log("Scaling " + count);
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    if (_workers.Count >= MaxWorkerThreads) return;
                    WorkerThread worker = new WorkerThread(this);
                    _workers.Add(worker);
                    _log("Starting worker " + worker.Id());
                    worker.Start();
                }
            } else {
                for (int i = 0; i < -count; i++) {
                    if (_workers.Count <= MinWorkerThreads) return;
                    WorkerThread worker = null;
                    try {
                        worker = _workers.ElementAt(i);
                    } catch (Exception e) { }
                    if (worker == null) break;
                    _log("Stopping worker " + worker.Id());
                    worker.Stop();
                }
            }
        }

        public void Scale(int count) {
            lock (_workersLock) {
                _scale(count);
            }
        }

        public void ScaleUp() {
            Scale(1);
        }
        public void ScaleDown() {
            Scale(-1);
        }

        public void ProcessQueue() {
            while (_queue.Count() > 0) {
                Thread.Sleep(1);
            }
        }

        public int QueueSize() {
            return _queue.Count();
        }

        public int PoolSize() {
            return _workers.Count();
        }

        public void Shutdown(bool wait = true) {
            for (int i = 0; i < _workers.Count; i++) {
                WorkerThread worker = null;
                try {
                    worker = _workers.ElementAt(i);
                } catch (Exception e) { }
                if (worker == null) break;
                _log("Stopping worker " + worker.Id() + " due to shutdown");
                worker.Stop();
            }
            if (wait) {
                while (_workers.Count() > 0) {
                    Thread.Sleep(1);
                }
            }
        }

    }
}
