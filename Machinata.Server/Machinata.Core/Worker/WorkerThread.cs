using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Worker {

    /// <summary>
    /// A Worker Thread which belongs to a Worker Pool.
    /// </summary>
    public class WorkerThread {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        private string _id = null;
        private WorkerPool _pool = null;
        private Thread _thread = null;
        private bool _requestStop = false;
        private bool _isProcessingTask = false;

        private DateTime _taskStartTime;
        private DateTime _taskEndTime;

        public WorkerThread(WorkerPool pool) {
            _id = Guid.NewGuid().ToString();
            _pool = pool;
            _thread = new Thread(_doWork);
        }

        public bool IsProcessingTask() {
            return _isProcessingTask;
        }

        /// <summary>
        /// Returns the current task process time in milliseconds.
        /// If no task is running, 0 is returned.
        /// </summary>
        /// <returns></returns>
        public long CurrentTaskProcessTime() {
            if (_isProcessingTask == false) return 0;
            else return (long)((DateTime.UtcNow - _taskStartTime).TotalMilliseconds);
        }

        public void Start() {
            _requestStop = false;
            _thread.Start();
        }

        public string Id() {
            return _id;
        }

        public void Stop() {
            _requestStop = true;
        }

        public void Abort() {
            _thread.Abort(); 
        }

        private void _doWork() {
            while (!_requestStop) {
                WorkerTask task = _pool.DequeueTask();
                if (task != null) {
                    task.AssignedWorkerThread = this;
                    while (task.Attempts < task.MaxAttempts) {
                        try {
                            _isProcessingTask = true;
                            _taskStartTime = DateTime.UtcNow;
                            
                            // Process the task
                            task.RegisterAttempt();
                            task.Process();

                            _isProcessingTask = false;
                            _taskEndTime = DateTime.UtcNow;

                            // Break the while - the process was successful
                            break;

                        } catch (Exception e) {
                            if(task.Attempts < task.MaxAttempts) {
                                // We are still attempting to process this task...
                                // Wait a bit and then retry through the while loop
                                System.Threading.Thread.Sleep(task.ExceptionCooldownTimeMS);
                            } else {
                                // We give up
                                // Mark task as ended and call the Failed() virtual method so that a task
                                // can know if it failed
                                _isProcessingTask = false;
                                _taskEndTime = DateTime.UtcNow;
                                try {
                                    task.Failed(e);
                                } catch (Exception e2) { }
                            }
                            // Log error
                            _logger.Error(e, $"Error processing task {task.GetType().Name} (attempt {task.Attempts} / {task.MaxAttempts})");
                        }
                    }
                    task.AssignedWorkerThread = null;
                } else {
                    // Wait a bit
                    Thread.Sleep(10);
                }
            }
            // Finish
            _pool.RemoveWorker(this);
        }
    }
}
