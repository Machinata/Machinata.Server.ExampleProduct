using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Worker {

    public static class LocalPool {

        private static WorkerPool _pool = new WorkerPool();

        public static void QueueTask(WorkerTask task) {
            _pool.QueueTask(task);
        }

        public static WorkerPool GetWorkerPool() {
            return _pool;
        }
    }
}
