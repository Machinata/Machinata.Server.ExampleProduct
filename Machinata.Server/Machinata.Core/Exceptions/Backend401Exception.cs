using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    /// <summary>
    /// A 401 (Login Required) Backend Exception. 
    /// This indicates that the user is not logged in for the request.
    /// </summary>
    /// <seealso cref="Machinata.Core.Exceptions.BackendException" />
    public class Backend401Exception : BackendException
    {

        public Backend401Exception(string code, string message, string neededARN, Exception innerException = null) : 
            base(code, message, innerException, 401) {

        }
    }
}
