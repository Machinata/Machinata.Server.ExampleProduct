using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Exceptions {

    public class FailSafeMessage {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// Shows a fail safe error message. This occurs when there is no possibility to load a error template,
        /// or there are errors on the error template.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="e">The e.</param>
        public static void Show(HttpContext context, Exception e) {
            // Init
            string message = "";
            string richContent = "";
            int statusCode = 500;
            string footer = "<hr/><p>Nerves Machinata Server</p><p>Build "+Core.Config.BuildID+"</p>";

            if (Core.Config.ErrorUnmaskingEnabled) {
                // Loop errors to get all innner exceptions
                var currentError = e;
                while (currentError != null) {
                    _logger.Error(currentError.Message);
                    message += currentError.Message + "\n";
                    currentError = currentError.InnerException;
                }
            } else {
                message = e.Message;
            }

            // Search for a rich-exception error (this is an error that provides an interface)
            {
                var currentError = e;
                while (currentError != null) {
                    if (currentError.GetType() == typeof(RichException)) {
                        richContent += ((RichException)currentError).HTML;
                    }
                    currentError = currentError.InnerException;
                }
            }

            // Insert trace
            string trace = "";
            if(Core.Config.ErrorLogTraceEnabled) {
                var traceLog = new StringBuilder();
                int numLogs = 20;
                if (Core.Logging.MemoryTarget != null) {
                    foreach (var line in Core.Logging.MemoryTarget.Logs.Skip(Core.Logging.MemoryTarget.Logs.Count - numLogs)) {
                        traceLog.Insert(0, line + "\n");
                    }
                }
                traceLog.Insert(0,"<pre>");
                traceLog.Append("</pre>");
                trace = traceLog.ToString();
            }
            
            // Show super simple worst-case error message
            context.Response.StatusCode = statusCode;
            context.Response.StatusDescription = "Server Error";
            context.Response.Write("<html><body><h1>Server Error</h1><h2>" + message + "</h2>"+richContent+trace+footer+"</body></html>");
            context.Response.Flush();
        }
    }
}
