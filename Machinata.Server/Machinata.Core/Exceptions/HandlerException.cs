using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {
    public class HandlerException : Exception {

        public HandlerException(Handler.Handler handler, string message, Exception innerException)
            : base(message, innerException) {

        }

    }
}
