using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    /// <summary>
    /// Provides a easy-to-use localized BackendException. 
    /// 
    /// The given error code must correspond to a translation in the following format:
    /// 
    ///     error.{error-code}.en=Your error text with {0} embedded {1} arguments.
    ///     
    /// Where {error-code} would be the given error code.
    /// </summary>
    /// <seealso cref="Machinata.Core.Exceptions.BackendException" />
    public class Localized404Exception : LocalizedException
    {
        public Localized404Exception(Handler.Handler handler, string code, params string[] args) 
            : base(handler, code, null, 404, args) {
        }

        public Localized404Exception(Handler.Handler handler, string code, Exception innerException, params string[] args)
            : base(handler, code, innerException, 404, args) {
        }
    }
}
