using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    /// <summary>
    /// Provides a easy-to-use localized BackendException. 
    /// 
    /// The given error code must correspond to a translation in the following format:
    /// 
    ///     error.{error-code}.en=Your error text with {0} embedded {1} arguments.
    ///     
    /// Where {error-code} would be the given error code.
    /// </summary>
    /// <seealso cref="Machinata.Core.Exceptions.BackendException" />
    public class LocalizedException : BackendException
    {
        private static string _getErrorMessage(Handler.Handler handler, string code, params string[] args) {
            string language = Core.Config.LocalizationDefaultLanguage; // fallback
            if (handler != null) language = handler.Language;
            var msg = Core.Localization.Text.GetTranslatedTextByIdForLanguage("error." + code, language, null);
            if (msg == null) msg = code;
            if(args != null && args.Length > 0) {
                msg = String.Format(msg, args);
            }
            return msg;
        }

        public LocalizedException(Handler.Handler handler, string code, params string[] args) 
            : base(code, _getErrorMessage(handler,code,args), null) {
        }

        public LocalizedException(Handler.Handler handler, string code, Exception innerException = null, params string[] args) 
            : base(code, _getErrorMessage(handler,code,args), innerException) {
        }

        public LocalizedException(Handler.Handler handler, string code, Exception innerException = null, int status = 500, params string[] args) 
            : base(code, _getErrorMessage(handler,code,args), innerException, status) {
        }
    }
}
