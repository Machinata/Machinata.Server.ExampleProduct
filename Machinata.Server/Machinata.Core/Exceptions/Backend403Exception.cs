using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    /// <summary>
    /// A 403 (Inssufficient Rights) Backend Exceptions.
    /// This indicates that a user does not have the proper rights (Access Policy) to access the resource.
    /// </summary>
    /// <seealso cref="Machinata.Core.Exceptions.BackendException" />
    public class Backend403Exception : BackendException
    {

        public Backend403Exception(string code, string message, string neededARN, Exception innerException = null) : 
            base(code, message, innerException, 403) {

        }
    }
}
