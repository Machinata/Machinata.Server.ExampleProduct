using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    public class RichException : Exception
    {
        public string HTML;

        public RichException(string message, string html, Exception innerException = null)
            : base(message, innerException) {
            this.HTML = html;
        }

        
    }
}
