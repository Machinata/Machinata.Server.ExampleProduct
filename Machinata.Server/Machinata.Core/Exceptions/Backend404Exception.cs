using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    /// <summary>
    /// A 404 (Not Found) backend exception.
    /// </summary>
    /// <seealso cref="Machinata.Core.Exceptions.BackendException" />
    public class Backend404Exception : BackendException
    {
        public Backend404Exception(Exception innerException = null) : 
            base("not-found", "Sorry, this page or asset does not exist.", innerException, 404) {
        }
        public Backend404Exception(string code, string message, Exception innerException = null) : 
            base(code, message, innerException, 404) {
        }
    }
}
