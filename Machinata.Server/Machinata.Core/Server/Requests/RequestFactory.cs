using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Text;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Server.Requests
{
    /// <summary>
    /// The IHttpHandler that handles all requests for Machinata.  This IHttpHandler forwards
    /// the request to the Machinata Handler factory and provides a fallback for worst-case
    /// error messages.
    /// </summary>
    /// <seealso cref="System.Web.IHttpHandler" />
    /// <seealso cref="System.Web.SessionState.IReadOnlySessionState" />
    public class RequestFactory : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public void ProcessRequest(HttpContext context) {

            // Catch all exceptions that occur at the very deep handler level...
            try {
                
                // Process the request by our handler factory
                Core.Handler.Handler.ProcessRequest(context);
                
            } catch (Exception e1) {

                _logger.Error(e1, e1.GetFullDescriptionIncludingTrace());

                if(e1 is RichException) {
                    Core.Exceptions.FailSafeMessage.Show(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e1));
                    return;
                }

                // An error occured!
                try {
                    
                    // Try to create a request to handle this error...
                    _logger.Trace("Will try to create error page...");
                    Core.Handler.Handler.ProcessException(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e1));

                } catch(Exception e2) {
                    // Last resort
                    _logger.Error(e2);
                    _logger.Trace("Could not create error page!");
                    Core.Exceptions.FailSafeMessage.Show(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e2));
                }
            }
            
        }

        

        public bool IsReusable
        {
            get { return true; }
        }
    }
}