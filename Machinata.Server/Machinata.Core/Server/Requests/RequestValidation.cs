using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Util;

namespace Machinata.Core.Server.Requests {

    /// <summary>
    /// Simple custom validator that just ignores all Form and QueryString 
    /// RequestValidationSource's. These are ignored at this stage because
    /// the HandlerFactory will do it's own validation at a later stage.
    /// </summary>
    /// <seealso cref="System.Web.Util.RequestValidator" />
    public class RequestValidation : System.Web.Util.RequestValidator {

        public RequestValidation() { }

        protected override bool IsValidRequestString(
            HttpContext context, 
            string value,
            RequestValidationSource requestValidationSource, 
            string collectionKey,
            out int validationFailureIndex) {

            //Set a default value for the out parameter.
            validationFailureIndex = -1;

            // For Form and QueryString variables we do the request
            // manually at HandlerFactory._createHandlerForURI
            // We do it at that point, since we want some information about the route. 
            // For example, the route might specifiy no request validation
            if (requestValidationSource == RequestValidationSource.Form
                || requestValidationSource == RequestValidationSource.QueryString) {
                return true;
            }
            
            // All other HTTP input checks are left to the base ASP.NET implementation.
            return base.IsValidRequestString(
                context, 
                value, 
                requestValidationSource,
                collectionKey, 
                out validationFailureIndex
            );
        }
    }
}
