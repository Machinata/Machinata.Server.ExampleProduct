using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Analytics.Providers {

    /// <summary>
    /// A super light-weight Google Analtyics tracking API helper.
    /// See https://developers.google.com/analytics/devguides/collection/protocol/v1/reference
    /// See https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters
    /// </summary>
    public class GoogleAnalyticsTracker : Core.Analytics.AnalyticsTracker {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        

        
        private const string ProtocolVersion = "1";
        private const string EndpointSSL = "https://ssl.google-analytics.com";
        private const string AnonymousClientId = "555";

        
        
        /// <summary>
        /// The tracking identifier
        /// </summary>
        public string TrackingId = null;

        /// <summary>
        /// The tracking domain
        /// </summary>
        public string TrackingDomain = null;
        
        public GoogleAnalyticsTracker() : base() {
            this.TrackingId = Core.Config.AnalyticsId;
            this.TrackingDomain = Core.Config.AnalyticsDomain;
        }

        /// <summary>
        /// Tracks a page view.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="path">The path.</param>
        public override void TrackPageView(string title, string path) {
            // Custom params
            var data = new Dictionary<string, string>();
            data.Add("dt",title);
            data.Add("dp",path);
            // Send data
            TrackData(data, "pageview");
        }

        /// <summary>
        /// Tracks a event.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="action">The action.</param>
        /// <param name="label">The label.</param>
        /// <param name="value">The value.</param>
        public override void TrackEvent(string category, string action, string label, int value = -1) {
            // Validate
            if (label == null) label = "";            
            // Custom params
            var data = new Dictionary<string, string>();
            data.Add("ec",category);
            data.Add("ea",action);
            data.Add("el",label);
            if(value >= 0) data.Add("ev",value.ToString());
            // Send data
            TrackData(data, "event");
        }

        /// <summary>
        /// Tracks a transaction.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="affiliation">The affiliation.</param>
        /// <param name="revenue">The revenue.</param>
        /// <param name="shipping">The shipping.</param>
        /// <param name="tax">The tax.</param>
        /// <param name="coupon">The coupon.</param>
        /// <param name="currency">The currency.</param>
        public override void TrackTransaction(string transactionId, string affiliation, string revenue, string shipping, string tax, string coupon, string currency) {
            // Validate
            if (coupon == null) coupon = "";
            if (tax == null) tax = "";
            if (currency == null) currency = "";
            if (affiliation == null) affiliation = "";
            // Custom params
            var data = new Dictionary<string, string>();
            data.Add("ti",transactionId);
            data.Add("ta",affiliation);
            data.Add("tr",revenue);
            data.Add("ts",shipping);
            data.Add("tt",tax);
            data.Add("tcc",coupon);
            data.Add("cu",currency);
            // Send data
            TrackData(data, "transaction");
        }
        

        /// <summary>
        /// Tracks a transaction item.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="sku">The sku.</param>
        /// <param name="name">The name.</param>
        /// <param name="price">The price.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="variation">The variation.</param>
        /// <param name="currency">The currency.</param>
        public override void TrackTransactionItem(string transactionId, string sku, string name, string price, int quantity, string variation, string currency) {
            // Validate
            if (sku == null) sku = "";
            if (name == null) name = "";
            if (price == null) price = "";
            if (variation == null) variation = "";
            if (currency == null) currency = "";
            // Custom params
            var data = new Dictionary<string, string>();
            data.Add("ti",transactionId);
            data.Add("ic",sku);
            data.Add("in",name);
            data.Add("ip",price);
            data.Add("iq",quantity.ToString());
            data.Add("iv",variation);
            data.Add("cu",currency);
            // Send data
            TrackData(data, "item");
        }
        

        /// <summary>
        /// Gets the analytics client identifier from the cookie.
        /// Only Google Universal Analytics is supported.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public override string GetClientIDFromContext(HttpContext context) {
            // Sanity
            if (context == null || context.Request == null || context.Request.Cookies == null) return null;
            var cookie = context.Request.Cookies["_ga"];
            if (cookie == null || cookie.Value == null) return null;
            // Google Analtyics JavaScript client id's look like this:
            // 308033787.1425999424
            // The cookie value however is set like this:
            // GA1.1.308033787.1425999424
            // Parse the id
            var ids = cookie.Value.Split('.');
            if (ids.Length < 4) return null;
            string clientId = ids[2] + "." + ids[3];
            return clientId;
        }

        /// <summary>
        /// Sends the measurement data to the Google measurement api endpoint.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="hitType">Type of the hit.</param>
        /// <exception cref="System.Exception">Google Measurement API did not return 200 OK</exception>
        protected override void SendMeasurementData(Dictionary<string,string> data, string hitType) {
            // Init
            bool verboseLogging = false;    // Only for testing
            bool errorLogging = false;       // Only for testing
            _logger.Debug("GAT Start "+hitType);
            // Setup clientCore.Util.Console.Debug("GAT Done "+response.StatusCode);
            var client = new RestSharp.RestClient(GoogleAnalyticsTracker.EndpointSSL);
            //client.UserAgent = this.UserAgent;
            // Setup request
            var request = new RestSharp.RestRequest("/collect", RestSharp.Method.GET);
            // Default required params
            request.AddParameter("v",GoogleAnalyticsTracker.ProtocolVersion);
            request.AddParameter("tid",this.TrackingId);
            request.AddParameter("cid",this.ClientId);
            request.AddParameter("ua",this.UserAgent);
            request.AddParameter("t",hitType);
            // Session control
            //if (_requestsSent <= 1) {
            //    // This is the first of the sesssion
            //    // Used to control the session duration. A value of 'start' forces a new session to start with this hit and 'end' forces the current session to end with this hit. All other values are ignored.
            //    request.AddParameter("sc", "start");
            //} else {
            //    request.AddParameter("sc", "end");
            //}
            //request.AddParameter("ni","1"); // Specifies that a hit be considered non-interactive.
            // Additional params
            if(this.IP != null) request.AddParameter("uip",this.IP);
            if(this.UserId != null) request.AddParameter("uid",this.UserId);
            if(this.TrackingDomain != null && this.TrackingDomain != "") data.Add("dh",this.TrackingDomain);
            // Data
            foreach (var key in data.Keys) {
                string val = data[key];
                if(val != null) request.AddParameter(key, val);
            }
            // Verbose logging
            if (verboseLogging) {
                string paramData = "";
                foreach (var p in request.Parameters) {
                    paramData += p.Name + ": " + p.Value + "\n";
                }
            }
            // Make request
            var response = client.Execute((RestSharp.IRestRequest)request);
            _logger.Debug("GAT Done "+response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK) {
                // Error
                throw new Exception("Google Measurement API did not return 200 OK");
            }
            
        }

    }
}
