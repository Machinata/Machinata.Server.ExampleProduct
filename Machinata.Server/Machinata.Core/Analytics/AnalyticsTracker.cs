using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Analytics {


    public abstract class AnalyticsTracker {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Consts ///////////////////////////////////////////////////////////////////
        
        public const string ANALYTICS_PROFILE_FRONTEND = "Frontend";
        public const string ANALYTICS_PROFILE_BACKEND = "Backend";

        #endregion

        /// <summary>
        /// Get a default tracker initialized with the user details from the handler.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        public static AnalyticsTracker DefaultTrackerForHandler(Core.Handler.Handler handler) {
            var tracker = DefaultTrackerForHandler();
            tracker.SetUserDetailsFromHandler(handler);
            return tracker;
        }

        public static AnalyticsTracker DefaultTrackerForHandler() {
            //TODO: configurable via settings
            var tracker = new Core.Analytics.Providers.GoogleAnalyticsTracker();
            return tracker;
        }

        /// <summary>
        /// This anonymously identifies a particular user, device, or browser instance. For the web, this is generally stored as a first-party cookie with a two-year expiration. For mobile apps, this is randomly generated for each particular instance of an application install. The value of this field should be a random UUID (version 4) as described in http://www.ietf.org/rfc/rfc4122.txt
        /// </summary>
        public string ClientId = null;

        /// <summary>
        /// This is intended to be a known identifier for a user provided by the site owner/tracking library user. It may not itself be PII (personally identifiable information). The value should never be persisted in GA cookies or other Analytics provided storage.
        /// </summary>
        public string UserId = null;

        /// <summary>
        /// The User Agent of the browser. Note that Google has libraries to identify real user agents. Hand crafting your own agent could break at any time.
        /// </summary>
        public string UserAgent = null;

        /// <summary>
        /// The IP address of the user. This should be a valid IP address. It will always be anonymized just as though &aip (anonymize IP) had been used.
        /// </summary>
        public string IP = null;


        /// <summary>
        /// Worker Task used internally if AnalyticsUseWorkerPool is true.
        /// </summary>
        /// <seealso cref="Machinata.Core.Worker.WorkerTask" />
        private class AnalyticsTrackerWorkerTask : Core.Worker.WorkerTask {

            private AnalyticsTracker _tracker;
            private string _hitType;
            private Dictionary<string, string> _data;

            public AnalyticsTrackerWorkerTask(AnalyticsTracker tracker, string hitType, Dictionary<string,string> data) {
                _tracker = tracker;
                _hitType = hitType;
                _data = data;
            }

            public override void Process() {
                _tracker.SendMeasurementData(_data, _hitType);
            }

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="AnalyticsTracker"/> class with all the given context.
        /// </summary>
        public AnalyticsTracker() {

        }

        public void SetUserDetailsFromHandler(Core.Handler.Handler handler) {
            // From context...
            this.UserAgent = handler.Request.UserAgent;
            this.IP = handler.Request.IP;
            this.ClientId = this.GetClientIDFromContext(handler.Context);
            if (handler.User != null) this.UserId = handler.User.PublicId;
            // Fallbacks
            if (this.UserAgent == null) {
                this.UserAgent = "Machinata Server"; // default
            }
            if (this.ClientId == null) {
                //TODO: does google work 'smartly' without one?
                //this.ClientId = Guid.NewGuid().ToString();
            }
        }
        
        /// <summary>
        /// Tracks a page view.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="path">The path.</param>
        public abstract void TrackPageView(string title, string path);

        /// <summary>
        /// Tracks a event.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="action">The action.</param>
        /// <param name="label">The label.</param>
        /// <param name="value">The value.</param>
        public abstract void TrackEvent(string category, string action, string label, int value = -1);

        /// <summary>
        /// Tracks a transaction.
        /// Note: Some tracker might require additional settings to enable this. For example, for Google Analytics, go to view settings and enable E-Commerce Features.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="affiliation">The affiliation.</param>
        /// <param name="revenue">The revenue.</param>
        /// <param name="shipping">The shipping.</param>
        /// <param name="tax">The tax.</param>
        /// <param name="coupon">The coupon.</param>
        /// <param name="currency">The currency.</param>
        public abstract void TrackTransaction(string transactionId, string affiliation, string revenue, string shipping, string tax, string coupon, string currency);
        
        /// <summary>
        /// Tracks a transaction item.
        /// Note: Some tracker might require additional settings to enable this. For example, for Google Analytics, go to view settings and enable E-Commerce Features.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="sku">The sku.</param>
        /// <param name="name">The name.</param>
        /// <param name="price">The price.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="variation">The variation.</param>
        /// <param name="currency">The currency.</param>
        public abstract void TrackTransactionItem(string transactionId, string sku, string name, string price, int quantity, string variation, string currency);
        
        /// <summary>
        /// Gets the analytics client identifier from the context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public abstract string GetClientIDFromContext(HttpContext context);
        
        /// <summary>
        /// Sends the measurement data to the tracking server.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="hitType">Type of the hit.</param>
        /// <exception cref="System.Exception">Google Measurement API did not return 200 OK</exception>
        protected abstract void SendMeasurementData(Dictionary<string, string> data, string hitType);
        

        protected void TrackData(Dictionary<string, string> data, string hitType) {
            if (Core.Config.AnalyticsEnabled == false) return;

            if (Core.Config.AnalyticsUseWorkerPool) {
                AnalyticsTrackerWorkerTask task = new AnalyticsTrackerWorkerTask(this, hitType, data);
                Core.Worker.LocalPool.QueueTask(task);
            } else {
                SendMeasurementData(data, hitType);
            }
        }

    }
}
