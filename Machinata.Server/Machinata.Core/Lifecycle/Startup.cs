using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Lifecycle {

    /// <summary>
    /// Specify this attribute on a static void method to be called when the application
    /// this signature is required: public static void OnApplicationStartup()
    /// starts up.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class OnApplicationStartupAttribute : Attribute {
    }

    public class Startup {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// Internal list of called startup routines to ensure that none are called twice.
        /// </summary>
        private static List<string> _routineCalled = new List<string>();

        private static Dictionary<string,long> _routineStartupTime = new Dictionary<string,long>();

        /// <summary>
        /// Calls the lifecycle startup routine 'static void OnApplicationStartup()' for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        private static void _callLifecycleRoutine(Type type) {
            if (_routineCalled.Contains(type.Name)) return;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _logger.Info(Core.Logging.LOG_SEPARATOR);
            _logger.Info("Initializing "+type.Name+"...");
            try {
                // Register
                _routineCalled.Add(type.Name);
                // Invoke 'static void OnApplicationStartup()'
                type.GetMethod("OnApplicationStartup").Invoke(null, null);
            }catch(Exception e) {
                _logger.Error("Could not call OnApplicationStartup for "+type.Name+": "+e.Message );
                _logger.Error(e);
                if(e.InnerException != null) _logger.Error(e.InnerException);
            }
            stopWatch.Stop();
            _logger.Info("Initializing "+type.Name+" Done ("+stopWatch.ElapsedMilliseconds+" ms)");
            _routineStartupTime.Add(type.Name, stopWatch.ElapsedMilliseconds);
        }

        public static void Run() {
            
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            // Set invariant culture to prevent unwanted obscurities
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            // Enable all SSL Versions
            Core.Util.HTTP.EnableAllSSLVersions();

            // We first pre-initialize the logger as a failsafe, even before config is loaded...
            Core.Logging.CaptureStartupLogs();
            _logger.Info("Application Starting...");

            // Now load the config and then re-load the logger (since we now have the config)
            // Also load the handler since most rely on this being initialized...
            _callLifecycleRoutine(typeof(Core.Config));
            _callLifecycleRoutine(typeof(Core.Logging));
            _callLifecycleRoutine(typeof(Core.Model.ModelContext));
            _callLifecycleRoutine(typeof(Core.Localization.Text));
            _callLifecycleRoutine(typeof(Core.Handler.Handler));
            _callLifecycleRoutine(typeof(Core.TaskManager.TaskManager));
            
            // Now load the rest using reflection
            foreach(var type in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnApplicationStartupAttribute))
                      .Select(mi => mi.DeclaringType)) {
                _callLifecycleRoutine(type);
            }

            // Save the startup logs
            foreach(var key in _routineStartupTime.Keys) {
                _logger.Info("Startup Time "+key+": "+_routineStartupTime[key]+" ms");
            }
            stopWatch.Stop();
            _logger.Info("Total Time: "+stopWatch.ElapsedMilliseconds+" ms");
            _logger.Info("Application Started");
            Core.Logging.RegisterStartupLogs();
        }

    }
}
