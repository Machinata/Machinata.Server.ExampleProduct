using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    

    public class DonutChartData {
        public string Title;
        public List<DonutChartDataItem> Items = new List<DonutChartDataItem>();

        public void UpdatePercentages() {
            var total = this.Items.Sum(i => i.Value);
            foreach(var item  in this.Items) {
                if (total == 0) item.Percent = 0;
                else item.Percent = (double)item.Value / (double)total;
            }
        }
    }

    public class DonutChartDataItem {
        public string Name;
        public string Title;
        public string Link;
        public int Value;
        public double Percent;
    }
    

}
