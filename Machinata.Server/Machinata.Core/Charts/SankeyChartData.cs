using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    
    public class SankeyChartData {
        public List<SankeyChartDataNode> Nodes = new List<SankeyChartDataNode>();
        public List<SankeyChartDataLink> Links = new List<SankeyChartDataLink>();
    }

    public class SankeyChartDataNode {
        public string Name;
        public string UID;
        public string Comment;
    }

    public class SankeyChartDataLink {
        public int Source;
        public int Target;
        public double Value;
    }
    
}
