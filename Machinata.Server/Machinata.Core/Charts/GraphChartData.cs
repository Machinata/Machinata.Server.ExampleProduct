using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    
    public class GraphChartData {
        public List<GraphChartDataNode> Nodes = new List<GraphChartDataNode>();
        public List<GraphChartDataLink> Links = new List<GraphChartDataLink>();
    }

    public class GraphChartDataNode {
        public string ID;
        public string Title;
        public string Link;
        public bool Star = false;
    }
        
    public class GraphChartDataLink {
        public string Source;
        public string Target;
        public string Title;
    }
    
}
