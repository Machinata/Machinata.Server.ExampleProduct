using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    
    public class MapChartData {
        public string Name;
        public List<MapChartDataMarker> Markers = new List<MapChartDataMarker>();
        public List<MapChartDataMarker> Heat = new List<MapChartDataMarker>();
        public bool UseCluster = false;
        public double Jitter = 0;
    }

    public class MapChartDataMarker {
        public string Name;
        public string Address;
        public string LatLon; // DEPRACTED IN JS?
        public string Link;
    }

}
