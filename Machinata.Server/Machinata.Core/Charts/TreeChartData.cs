using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    

    public class TreeChartDataNode {
        public string Name;
        public string Title;
        public string ChildrenCall;
        public string Link;
        public List<TreeChartDataNode> Children = new List<TreeChartDataNode>();
    }
    

}
