using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    

    public class SpiderChartData {
        public string Title;
        
        public List<SpiderChartDataSeries> Series = new List<SpiderChartDataSeries>();
    }

    public class SpiderChartDataSeries {
        public string Name;
        public string Title;
        
        public List<SpiderChartDataItem> Items = new List<SpiderChartDataItem>();
    }

    public class SpiderChartDataItem {
        public string Name;
        public float Value;
    }
    

}
