using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    

    public class TimelineChartData {

        public List<TimelineChartDataTrack> Tracks = new List<TimelineChartDataTrack>();

        public void Prune() {
            foreach (var track in this.Tracks) track.Prune();
        }

        public void ConvertToDefaultTimezone() {
            foreach (var track in this.Tracks) track.ConvertToDefaultTimezone();
        }
    }

    public class TimelineChartDataTrack {
        public string Name;
        public string Title;
        public string Id;
        public string Link;
        public string Color;
        public string Classes;
        public string Group;
        public string ParentId;
        public int Level;
        public DateTime Start;
        public DateTime End;

        public List<TimelineChartDataEvent> Events = new List<TimelineChartDataEvent>();

        public void Prune() {
            End = End.AddSeconds(24 * 60 * 60 - 1);
            foreach(var evt in this.Events) {
                if(evt.Date.Hour == 0 && evt.Date.Minute == 0) {
                    evt.Date = evt.Date.AddHours(12);
                }
            }
        }

        public void ConvertToDefaultTimezone() {
            Start = Start.ToDefaultTimezone();
            End = End.ToDefaultTimezone();
            foreach(var evt in this.Events) {
                evt.Date = evt.Date.ToDefaultTimezone();
            }
        }
    }

    public class TimelineChartDataEvent {
        public string Title;
        public string Link;
        public string Color;
        public string Id;
        public DateTime Date;
        public string Classes;
    }
    

}
