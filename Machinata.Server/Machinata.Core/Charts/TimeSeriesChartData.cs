using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    
    public class TimeSeries {


        #region Delegate Definitions
        
        public delegate IQueryable<T> EntitiesForRange<T>(IQueryable<T> entities, DateTime start, DateTime end) where T : ModelObject;
        public delegate double ValueForEntities<T>(IQueryable<T> entities) where T : ModelObject;

        public delegate string LabelForEntity<T>(T entity) where T : ModelObject;
        public delegate float ValueForEntity<T>(T entity) where T : ModelObject;
        
        public delegate bool IsSegmentDifferent(string label1, string label2);
        public delegate string GetSegmentLabel(string label1, string label2);

        #endregion

        #region Delegate Helpers

        public static IQueryable<T> EntitiesForRangeOnCreated<T>(IQueryable<T> entities, DateTime start, DateTime end) where T : ModelObject {
            return entities.Where(e => e.Created >= start && e.Created < end);
        }
        
        public static double ValueForEntitiesOnCount<T>(IQueryable<T> entities) where T : ModelObject {
            return entities.Count();
        }

        #endregion

        /// <summary>
        /// Gets the time series for entities with provided delegates.
        /// This method will automatically read the handler context and configure
        /// the correct timeranges et cetera, and will compile the data using the 
        /// specified delegates.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="entitiesForRange">The entities for range.</param>
        /// <param name="valueForEntities">The value for entities.</param>
        /// <returns></returns>
        public static TimeSeriesChartDataSeries GetTimeSeriesForEntitiesWithDelegates<T>(
            IQueryable<T> entities, 
            Core.Handler.Handler handler,
            EntitiesForRange<T> entitiesForRange,
            ValueForEntities<T> valueForEntities,
            string title = null,
            string name = null,
            string color = null,
            DateRange customTimeRange = null) where T : ModelObject {
            // Init defaults
            var data = new TimeSeriesChartDataSeries();
            data.Title = title;
            data.Name = name;
            if (data.Name == null) data.Name = title;
            data.Color = color;

            // Get timespan (range start/stop)

            // Default...
            DateTime end = Core.Util.Time.GetDefaultTimezoneNow();
            DateTime start = end.AddDays(-14);
            if (customTimeRange != null) {
                start = customTimeRange.Start.Value;
                end = customTimeRange.End.Value;
            }
            TimeSpan interval = Core.Util.Time.GetTimespanFromString(handler.Params.String("interval","1d"));
            TimeSpan timespan =  end - start; 

            // Try from contentxt...
            
            var endFromContext = handler.Params.String("end", null);
            if (endFromContext != null) {
                end = handler.Params.DateTime("end", DateTime.MaxValue);
                start = end.AddDays(-14); // default 
            }
            var startFromContext = handler.Params.String("start", null);
            if (startFromContext != null) {
                start = handler.Params.DateTime("start", DateTime.MinValue);
            }
            var timeSpanFromContext = handler.Params.String("timespan", null);
            if (timeSpanFromContext != null) {
                timespan = Util.Time.GetTimespanFromString(timeSpanFromContext);
                start = end - timespan;
            }
            string labelFormat = Core.Config.DateFormat; //TODO

            // Interval Day
            if (interval.TotalHours == 24) {
                if (start.Year != end.Year) {
                    labelFormat = "dd.MM.yyyy";
                } else if (timespan.TotalDays > 7) {
                    labelFormat = "dd.MM";
                } else {
                    labelFormat = "dddd";
                }
                // Snap
                if (customTimeRange == null) {
                    end = end.Date.AddDays(1);
                    start = end - timespan;
                }
            }

            // Interval Hour
            else if (interval.TotalHours == 1) {
                labelFormat = "HH:00";
                // Snap
                end = new DateTime(end.Year, end.Month, end.Day, end.Hour, 0, 0);
                start = end - timespan;
            }

            // Use fixed intervals or snap to dates?
            if (handler.Params.String("interval") == "monthly" || handler.Params.String("interval") == "daily") {
                DateTime currentDT = start;
                while(currentDT < end) {
                    // Init
                    DateTime rangeStart = DateTime.MinValue;
                    DateTime rangeEnd = DateTime.MinValue;
                    // What interval?
                    if (handler.Params.String("interval") == "monthly") {
                        rangeStart = currentDT.StartOfMonth();
                        rangeEnd = currentDT.EndOfMonth();
                        labelFormat = "MM.yyyy";
                        currentDT = currentDT.AddMonths(1);
                    } else if (handler.Params.String("interval") == "daily") {
                        rangeStart = currentDT.StartOfDay();
                        rangeEnd = currentDT.EndOfDay();
                        labelFormat = "dd.MM";
                        currentDT = currentDT.AddDays(1);
                    } else {
                        throw new Exception("Unknown date interval");
                    }
                    // Get datapoints
                    var rangeEntities = entitiesForRange(entities, rangeStart.ToUniversalTime(), rangeEnd.ToUniversalTime());
                    var rangeVal = valueForEntities(rangeEntities);
                    var rangeLabel = rangeStart.ToString(labelFormat);
                    var dataPoint = new TimeSeriesChartDataPoint() {
                        Label = rangeLabel, //TODO
                        Value = rangeVal
                    };
                    data.DataPoints.Add(dataPoint);
                }
            } else {
                // Create n intervals
                int n = (int)System.Math.Ceiling(timespan.TotalSeconds / interval.TotalSeconds);
                for (int i = 0; i < n; i++) {
                    DateTime rangeStart = start + (new TimeSpan(0, 0, (int)interval.TotalSeconds * i)); //TODO
                    DateTime rangeEnd = rangeStart + interval; //TODO
                    var rangeEntities = entitiesForRange(entities, rangeStart.ToUniversalTime(), rangeEnd.ToUniversalTime());
                    var rangeVal = valueForEntities(rangeEntities);
                    var rangeLabel = rangeStart.ToString(labelFormat);
                    var dataPoint = new TimeSeriesChartDataPoint() {
                        Label = rangeLabel, //TODO
                        Value = rangeVal
                    };
                    data.DataPoints.Add(dataPoint);
                }
            }

            // Return
            return data;
        }

        public static TimeSeriesChartDataSeries GetTimeSeriesForEntitiesWithDelegates<T>(
            IQueryable<T> entities, 
            Core.Handler.Handler handler,
            LabelForEntity<T> labelForEntity,
            ValueForEntity<T> valueForEntity,
            string title = null,
            string name = null,
            string line = ChartLines.LINE_SOLID,
            bool dots = true,
            string color = null) where T : ModelObject {
            // Init defaults
            var data = new TimeSeriesChartDataSeries();
            data.Title = title;
            data.Color = color;
            data.Name = name;
            data.Line = line;
            data.Dots = dots;
            if (data.Name == null) data.Name = title;
            // Add all entities using delegate
            foreach(var entity in entities) { 
                var rangeVal = valueForEntity(entity);
                var rangeLabel = labelForEntity(entity);
                var dataPoint = new TimeSeriesChartDataPoint() {
                    Label = rangeLabel, //TODO
                    Value = rangeVal
                };
                data.DataPoints.Add(dataPoint);
            }

            // Return
            return data;
        }

    }

    public class TimeSeriesChartData {
        public string Name;
        public string Metric;
        public string Format;
        public double? MinValue = 0;
        public double? MaxValue = null; // auto
        public double? MinMaxPadding = null; // auto
        public string AreaMin;
        public string AreaMax;
        public string AreaColor;
        public int? ZeroLine;
        public bool XAxisIsTime = true;
        public int XAxisLabelsEvery = 1;
        public int? XAxisLabelsRotation = null;
        public bool YAxisLines = false;
        public string LegendPosition = null;
        public string TimeFormat = Core.Config.DateTimeFormat;
        public List<TimeSeriesChartDataSeries> Series = new List<TimeSeriesChartDataSeries>();
        public List<TimeSeriesChartSegment> Segments = new List<TimeSeriesChartSegment>();

        public void CreateAlternatingDailySegments() {
            CreateAlternatingSegments(
                delegate (string label1, string label2) {
                    var dt1 = DateTime.MinValue;
                    if(!string.IsNullOrEmpty(label1)) dt1 = DateTime.Parse(label1);
                    var dt2 = DateTime.MinValue;
                    if(!string.IsNullOrEmpty(label2)) dt2 = DateTime.Parse(label2);
                    return dt1.DayOfYear != dt2.DayOfYear;
                },
                delegate (string label1, string label2) {
                    var dt1 = DateTime.MinValue;
                    if(!string.IsNullOrEmpty(label1)) dt1 = DateTime.Parse(label1);
                    var dt2 = DateTime.MinValue;
                    if(!string.IsNullOrEmpty(label2)) dt2 = DateTime.Parse(label2);
                    // Return date of starting range
                    return dt1.ToString("ddd");
                    // Return middle of two dates
                    //var dtd = (dt2 - dt1).TotalSeconds; // difference in seconds
                    //var dtm = dt1.AddSeconds(dtd);
                    //return dtm.ToString("ddd") + "/"+dtm.ToString()+"/"+dtd;
                }
            );
        }

        public void CreateUniqueColorsForSeries() {
            var colors = ChartColors.D3_CATEGORY_10;
            var i = 0;
            foreach(var serie in this.Series) {
                serie.Color = colors[i];
                i++; if (i >= colors.Length) i = 0;
            }
        }

        public TimeSeriesChartDataSeries CreateAverageFromSeries(string title, string color, string name = "Average") {
            var avgData = new Dictionary<string,double>();
            foreach (var serie in this.Series) {
                foreach (var dp in serie.DataPoints) {
                    if(avgData.ContainsKey(dp.Label)) {
                        avgData[dp.Label] = (avgData[dp.Label] + dp.Value) / 2.0;
                    } else {
                        avgData[dp.Label] = dp.Value;
                    }
                }
            }
            var avgSerie = new TimeSeriesChartDataSeries();
            avgSerie.Title = title;
            avgSerie.Color = color;
            avgSerie.Name = name;
            avgSerie.Line = ChartLines.LINE_SOLID;
            foreach(var key in avgData.Keys) {
                avgSerie.DataPoints.Add(new TimeSeriesChartDataPoint() {
                    Label = key,
                    Value = avgData[key]
                });
            }
            this.Series.Add(avgSerie);
            return avgSerie;
        }

        public void CreateAlternatingSegments(TimeSeries.IsSegmentDifferent segmentDifferent = null, TimeSeries.GetSegmentLabel segmentLabel = null) {
            if(segmentDifferent == null) {
                segmentDifferent = delegate (string label1, string label2) {
                    return label1 != label2;
                };
            }
            if (segmentLabel == null) {
                segmentLabel = delegate (string label1, string label2) {
                    return label1;
                };
            }
            var series = this.Series.First();
            string lastLabel = null;
            int count = 0;
            for(var i = 0; i < series.DataPoints.Count; i++) {
                var dp = series.DataPoints[i];
                if(segmentDifferent(lastLabel,dp.Label) || i == (series.DataPoints.Count-1)) {
                    if (lastLabel != null) {
                        this.Segments.Add(new TimeSeriesChartSegment() {
                            Label = segmentLabel(lastLabel,dp.Label),
                            LabelStart = lastLabel,
                            LabelEnd = dp.Label,
                            Mod2 = (count % 2)
                        });
                    }
                    count++;
                    lastLabel = dp.Label;
                }
            }
        }
    }
    
    public class TimeSeriesChartDataSeries {
        public string Name;
        public string Title;
        public string Color;
        public string Line = ChartLines.LINE_SOLID;
        public bool Dots;
        public List<TimeSeriesChartDataPoint> DataPoints = new List<TimeSeriesChartDataPoint>();
    }
    
    public class TimeSeriesChartDataPoint {
        public string Label;
        public double Value;
    }
    
    public class TimeSeriesChartSegment {
        public string Label;
        public string LabelStart;
        public string LabelEnd;
        public string Color;
        public int Mod2;
    }
    

}
