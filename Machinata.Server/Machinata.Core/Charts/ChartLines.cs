using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Charts {
    public class ChartLines {
        
        public const string LINE_NONE = null;
        public const string LINE_SOLID = "solid";
        public const string LINE_DASHED = "dashed";
        public const string LINE_DASHED_4 = "dashed-4";
        public const string LINE_DASHED_3 = "dashed-3";
        public const string LINE_DASHED_2 = "dashed-2";
        public const string LINE_DASHED_1 = "dashed-1";

    }
}
