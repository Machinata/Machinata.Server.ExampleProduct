using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        #region InsertVariable for CMS Pages

        private ContentNode _getContentNodeAndChildrenForPath(string cmsPath, bool throw404IfNotExists = true, ModelContext db = null, bool ignoreHidden = false) {
            if (db == null) {
                if (this.Handler == null) throw new Exception("No handler was provided for this template. The template method InsertContent requires a handler.");
                db = this.Handler.DB;
            }
            if (!cmsPath.StartsWith(ContentNode.NODE_PATH_SEP)) cmsPath = ContentNode.NODE_PATH_SEP + cmsPath;
            var node = db.ContentNodes().Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children)).SingleOrDefault(n => n.Path.ToLower() == cmsPath.ToLower() && n.NodeType == ContentNode.NODE_TYPE_NODE && (n.Published == true || ignoreHidden));
            if (node == null && throw404IfNotExists) throw new Backend404Exception("not-found", $"The CMS page {cmsPath} could not be found or is not published.");
            return node;
        }

        public ContentNode InsertContent(string variableName, string cmsPath, string language = null, bool throw404IfNotExists = true) {
            // Get the node
            var node = _getContentNodeAndChildrenForPath(cmsPath, throw404IfNotExists);
            // Pass on to node function...
            InsertContent(variableName, node, language);

            return node;
        }

        public void InsertContent(string variableName, string cmsPath, ModelContext db, string language = null, bool throw404IfNotExists = true) {
            // Get the node
            var node = _getContentNodeAndChildrenForPath(cmsPath, throw404IfNotExists, db);
            // Pass on to node function...
            InsertContent(variableName, node, language);
        }

        /// <summary>
        /// Inserts a list of pages from CMS projected onto a flat (single) template.
        /// Page contents are automatically compiled into variables which can be used in the template.
        /// For example, the template might include the following with variableName set to 'car':
        /// 
        ///     <h2>{car.title}</h2>
        ///     <img src='{car.image}'/>
        ///     
        /// Then a series of CMS pages with one title and one image can be used. If multiple of the same type are to be used, 
        /// a template might look like:
        /// 
        ///     <h2>{car.title}</h2>
        ///     <img class='front' src='{car.image-1}'/>
        ///     <img class='back' src='{car.image-2}'/>
        ///     
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="cmsPath">The CMS path.</param>
        /// <param name="language">The language.</param>
        /// <param name="throw404IfNotExists">if set to <c>true</c> [throw404 if not exists].</param>
        /// <exception cref="System.Exception">No handler was provided for this template. The template method InsertContent requires a handler.</exception>
        public void InsertProjectedPages(string variableName, string templateName, string cmsPath, string language = null, bool throw404IfNotExists = true, Action<ContentNode,PageTemplate> forEachPage = null, bool publishedOnly = false) {
            if (this.Handler == null) throw new Exception("No handler was provided for this template. The template method InsertContent requires a handler.");
            var pages = this.Handler.DB.ContentNodes().Include("Children.Children.Children").GetPagesAtPath(cmsPath);

            if (publishedOnly == true) {
                pages = pages.Where(p => p.Published == true);
            }

            pages = pages.OrderBy(n => n.Sort);

            this.InsertProjectedPages(variableName, templateName, pages, language, throw404IfNotExists, forEachPage);
        }

        public void InsertProjectedPages(string variableName, string templateName, IEnumerable<ContentNode> pages, string language = null, bool throw404IfNotExists = true, Action<ContentNode,PageTemplate> forEachPage = null)  {
            if (this.Handler == null) throw new Exception("No handler was provided for this template. The template method InsertContent requires a handler.");
            var templates = new List<PageTemplate>();
            foreach (var page in pages) {
                var template = this.LoadTemplate(templateName);
                if (forEachPage != null) forEachPage(page, template);
                template.InsertProjectedContent(page, language);
                templates.Add(template);
            }
            this.InsertTemplates(variableName, templates);
        }

        public void InsertPages(string variableName, string templateName, string cmsPath, string language = null, bool throw404IfNotExists = true, bool publishedOnly = true) {
            if (publishedOnly) {
                var pages = this.Handler.DB.ContentNodes()
                    .Include("Children.Children.Children")
                    .GetPagesAtPath(cmsPath)
                    .Published()
                    .OrderBy(e => e.Sort)
                    .AsQueryable();
                InsertPages(variableName, templateName, pages, language);
            } else {
                var pages = this.Handler.DB.ContentNodes()
                    .Include("Children.Children.Children")
                    .GetPagesAtPath(cmsPath)
                    .OrderBy(e => e.Sort)
                    .AsQueryable();
                InsertPages(variableName, templateName, pages, language);
            }
        }

        public void InsertPages(string variableName, string templateName, IQueryable<ContentNode> pages, string language = null) {
            if (this.Handler == null) throw new Exception("No handler was provided for this template. The template method InsertContent requires a handler.");
            var templates = new List<PageTemplate>();
            foreach (var page in pages) {
                
                var template = this.LoadTemplate(templateName);
                template.InsertContent("page.content", page, language);
                template.InsertVariable("page.title", page.Title);
                template.InsertVariable("page.name", page.Name);
                template.InsertVariable("page.short-url", page.ShortURL);
                template.InsertVariable("page.path", page.Path);
                template.InsertVariable("page.public-id", page.PublicId);

                var translation = page.TranslationForLanguage(language);
                if (translation != null) {
                    template.InsertVariable("translation.title", translation.Title);
                }

                templates.Add(template);
            }
            this.InsertTemplates(variableName, templates);
        }

        public void InsertPagedLayoutedContent(string variableName, string templateName, IEnumerable<ContentNode> pages, int defaultPageSize = 10, string langauge = null, bool throw404IfNotExists = true) {
            var pageNumber = this.Handler.Params.Int("page", 1);
            var pageSize = this.Handler.Params.Int("page-size", defaultPageSize);
            var entries = pages.Paginate(pageNumber, pageSize);
            var templates = new List<PageTemplate>();
            foreach (var entry in entries) {
                var template = this.LoadTemplate(templateName);
                template.InsertLayoutedContent("content", entry.Path, langauge, throw404IfNotExists: throw404IfNotExists, ignoreHidden:!throw404IfNotExists);
                template.InsertVariable("page.id", entry.PublicId);
                template.InsertVariable("page.short-url", entry.ShortURL);
                templates.Add(template);
            }
            this.InsertTemplates(variableName, templates);
        }

        public void InsertPagedLayoutedContent(string variableName, string templateName, string cmsPath, int defaultPageSize = 10, string langauge = null, bool throw404IfNotExists = true) {
            InsertPagedLayoutedContent(variableName, templateName, this.Handler.DB.ContentNodes().GetPagesAtPath(cmsPath).Published(), defaultPageSize, langauge, throw404IfNotExists);
        }

        public void InsertPagedLayoutedContent(string variableName, string templateName, ContentNode page, int defaultPageSize = 10, string langauge = null, bool throw404IfNotExists = true) {
            if (page.Published == false && throw404IfNotExists) throw new Backend404Exception("not-found", $"The page {page.PublicId} could not be found or is not published.");
            List<ContentNode> pages = new List<ContentNode>(1);
            pages.Add(page);
            InsertPagedLayoutedContent(variableName, templateName, pages, defaultPageSize, langauge, throw404IfNotExists: throw404IfNotExists);
        }

        public ContentNode InsertLayoutedContent(string variableName, string cmsPath, string langauge = null, bool throw404IfNotExists = true, bool ignoreHidden = false) {
            // Get root node
            var node = _getContentNodeAndChildrenForPath(cmsPath, throw404IfNotExists, null, ignoreHidden);
            if (node == null) {
                this.InsertVariable(variableName, "");
                return null;
            }
            // Get the layout
            ContentLayout layout = null;
            if (node.Layout != null) {
                layout = node.Context.ContentLayouts().SingleOrDefault(cl => cl.Name == node.Layout);
            }
            // Get the template name from the layout, or fallback to default
            string optionsClasses = string.Join(" ", node.Options.Keys.Select(k => "option-" + k));
            string templateName = "layout.none"; // default
            if (layout != null) templateName = layout.TemplateName;
            // Load template and insert the projected content
            var template = this.LoadTemplate(templateName);
            template.InsertProjectedContent(node, langauge);
            // Layout variables
            template.InsertVariable("layout.template", templateName);
            if (layout != null) {
                template.InsertVariable("layout.name", layout.Name);
                template.InsertVariable("layout.css", layout.CSSClass + " " + optionsClasses);
            } else {
                template.InsertVariable("layout.name", "");
                template.InsertVariable("layout.css", optionsClasses);
            }
            // Insert all subpages (this inserts all children as layouted content)
            if (template.HasVariable("page.children")) {
                var childTemplates = new List<PageTemplate>();
                var childNodes = node.Children.Where(n => n.NodeType == ContentNode.NODE_TYPE_NODE && n.Published == true).OrderBy(n => n.Sort).ToList();
                var count = 1;
                foreach (var childNode in childNodes) {
                    var childTemplate = this.LoadTemplate("layout.children"); // Soecial template for just inserting all the layouted content
                    childTemplate.InsertLayoutedContent("layout.children", childNode.Path, langauge, false);
                    childTemplate.InsertVariable("page.number", count);
                    childTemplates.Add(childTemplate);
                    count++;
                }
                template.InsertTemplates("page.children", childTemplates);
            }
            // Insert cms content (inserts page contents as regular cms page)
            if (template.HasVariable("page.content")) {
                template.InsertContent("page.content", node, langauge, true);
            }
            if (template.HasVariable("page.content.images")) {
                template.InsertContent("page.content.images", node, langauge, true, ContentNode.NODE_TYPE_IMAGE);
            }
            // Insert the final template
            this.InsertTemplate(variableName, template);

            // Node
            return node;
        }

        #endregion

        #region InsertVariable for Content Nodes

        public ContentNode InsertProjectedContent(ModelContext db, string nodePath, string language = null) {
            ContentNode node = null;
            if (db != null && !string.IsNullOrEmpty(nodePath)) {
                node = db.ContentNodes()
                    .Include("Children")
                    .Include("Children.Children")
                    .GetNodeByPath(nodePath, false);
            }
            InsertProjectedContent(node, language);
            return node;
        }

        public void InsertProjectedContent(ContentNode page, string language = null) {
            var typeTracker = new Dictionary<string, int>();
            var translation = page.TranslationForLanguage(language != null ? language : this.Language);
            var insertedVars = new List<string>();
            if (translation != null) {
                // Apply meta information
                if (this.Handler != null) (this.Handler as Handler.PageTemplateHandler)?.Meta.LoadFromNode(translation);
                // Process all the content
                foreach (var child in translation.Children.OrderBy(n => n.Sort)) {
                    // Insert variable based on content node
                    {
                        if (!typeTracker.ContainsKey(child.NodeType)) typeTracker[child.NodeType] = 1;
                        else typeTracker[child.NodeType] += 1;
                        if (typeTracker[child.NodeType] == 1) {
                            this.InsertVariable("content." + child.NodeType + "-raw", child.Value);
                            this.InsertVariable("content." + child.NodeType, child.ValueForTemplate);
                            this.InsertVariableUnsafe("content." + child.NodeType + "-unsafe", child.ValueForTemplate);
                            // Options
                            foreach (var key in child.Options.Keys) {
                                this.InsertVariable("content." + child.NodeType + ".option-" + key, child.Options[key]);
                            }
                            child.NodeTypeMeta.InsertAdditionalVariables(child,this,"content." + child.NodeType);
                            insertedVars.Add(child.NodeType);
                        }
                    }
                    // Insert variable based on index of type
                    {
                        var varname = "content." + child.NodeType + "-" + typeTracker[child.NodeType];
                        this.InsertVariable(varname + "-raw", child.Value);
                        this.InsertVariable(varname, child.ValueForTemplate);
                        this.InsertVariableUnsafe(varname + "-unsafe", child.ValueForTemplate);
                        // Options
                        foreach (var key in child.Options.Keys) {
                            this.InsertVariable(varname + ".option-" + key, child.Options[key]);
                        }
                        insertedVars.Add(child.NodeType + "-" + typeTracker[child.NodeType]);
                    }
                    // Insert varialbe based on description
                    if(child.Options.Keys.Contains("description")) {
                        var desc = child.Options["description"] as string;
                        if(desc != null) {
                            var formattedDesc = desc.ToLower().Replace(" ", "-");
                            this.InsertVariable("content." + child.NodeType + "-" + formattedDesc + "-raw", child.Value);
                            this.InsertVariable("content." + child.NodeType + "-" + formattedDesc, child.ValueForTemplate);
                        }
                    }
                }
                // Insert translation variables
                this.InsertVariable("translation.name", translation.Name);
                this.InsertVariable("translation.short-url", translation.ShortURL);
                this.InsertVariable("translation.path", translation.Path);
                this.InsertVariable("translation.public-id", translation.PublicId);

            }
            // Insert auxiliary variables
            // Example:
            // {content.has-image} ==> has-image if one or more images
            //                         no-image if no images
            foreach (var var in this._variables.Where(v => v.StartsWith("content.has-"))) {
                if (insertedVars.Contains(var.Replace("content.has-", ""))) {
                    this.InsertVariable(var, var.Replace("content.has-", "has-"));
                } else {
                    this.InsertVariable(var, var.Replace("content.has-", "no-"));
                }
            }
            // Remove empty non-existant content variables
            foreach (var var in this._variables.Where(v => v.StartsWith("content."))) {
                this.InsertVariable(var, "");
            }
            // Compile options
            string optionsClasses = string.Join(" ", page.Options.Keys.Select(k => "option-" + k));
            if (translation != null) {
                foreach (var optKey in translation.Options.Keys) {
                    this.InsertVariable("page.option-" + optKey, translation.Options[optKey]);
                }
            }
            // Insert rest
            this.InsertVariable("page.options", optionsClasses);
            this.InsertVariable("page.name", page.Name);
            this.InsertVariable("page.short-url", page.ShortURL);
            this.InsertVariable("page.path", page.Path);
            this.InsertVariable("page.public-id", page.PublicId);
            this.InsertVariable("page.readable-id", page.ReadableId);
        }

        /// <summary>
        /// Inserts the content for the given ContentNode with the given language.
        /// If no language is specified, then the template language or default language is used.
        /// In addition to inserting the rich content for the variable name, also the summary can be
        /// inserted using {VARIABLE-NAME.summary}
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="node">The node.</param>
        /// <param name="language">The language.</param>
        /// <returns>The summary of the matched translation node.</returns>
        /// <exception cref="System.Exception">The node must be a 'node' type for InsertContentNodeEditor!</exception>
        public string InsertContent(string variableName, ContentNode node, string language = null, bool useFallbackLanguage = true, string nodeTypeFilter = null) {

            // Init
            string summary = null;
            var contentTemplate = this.LoadTemplate("content-rendered");
            if (language == null) language = this.Language;
            List<string> supportedLangages = ContentNode.GetSupportedLanguages();
            var contentCount = 0;

            // Null node?
            if (node == null) {

                contentTemplate.InsertVariable("node.path", "");
                contentTemplate.InsertVariable("node.public-id", "");
                contentTemplate.InsertVariable("node.published", "false");

            } else {

                // Validate
                if (node.NodeType != ContentNode.NODE_TYPE_NODE) throw new Exception("The node must be a 'node' type for InsertContentNodeEditor!");

                // Load children
                node.Include("Children");

                // Insert basic variables
                contentTemplate.InsertVariable("node.path", node.Path);
                contentTemplate.InsertVariable("node.public-id", node.PublicId);
                contentTemplate.InsertVariable("node.published", node.Published);
            }

            // Translations
            var translationTemplates = new List<PageTemplate>();
            if (node != null) {
                // Get the translations we want
                var translations = node.TranslationsForLanguage(language);

                // Iterate each translation (sorted by our default lang list)
                foreach (var translation in translations.OrderBy(t => supportedLangages.IndexOf(t.Value))) {

                    // If fallback is disabled, we need to check that the language exactly matches,
                    // or if the trans node is a default lang (null), that the requested lang is the system default
                    if (useFallbackLanguage == false) {
                        if (!(translation.Value == null && language.ToLower() == Core.Config.LocalizationDefaultLanguage.ToLower())
                            &&
                            (translation.Value?.ToLower() != language.ToLower())) {
                            // This is not the exact language we are looking for
                            continue;
                        }
                    }

                    // Make sure it is loaded
                    translation.Include("Children");

                    // Common properties
                    var translationDisplayVal = translation.Value;
                    if (translationDisplayVal == null) translationDisplayVal = "default";

                    // Translation contents
                    {
                        // Init
                        var template = this.LoadTemplate("content-rendered.translation");
                        var translationContentTemplates = new List<PageTemplate>();
                        // Translation variables
                        template.InsertVariable("translation.public-id", translation.PublicId);
                        template.InsertVariable("translation.language", translationDisplayVal);
                        template.InsertVariable("translation.short-url", translation.ShortURL);
                        // Contents
                        var typeTracker = new Dictionary<string, int>();
                        foreach (var contentNode in translation.GetContentChildren()) {
                            // Filter?
                            if(nodeTypeFilter != null) {
                                if (contentNode.NodeType != nodeTypeFilter) continue;
                            }
                            // Bookkeeping
                            if (!typeTracker.ContainsKey(contentNode.NodeType)) typeTracker[contentNode.NodeType] = 1;
                            else typeTracker[contentNode.NodeType] += 1;
                            // Load the template...
                            // First try a template overload on the current template context
                            // For example, we are in template 'layout.slider' and are requesting the template 'node.link',
                            // then we search for 'layout.slider.node.link'
                            var nodeTemplateName = "content-rendered.node." + contentNode.NodeType;
                            //var nodeTemplate = PageTemplate.Cache.Find(this.Package, this.Name + "." + nodeTemplateName, this.Extension, false);
                            var nodeTemplate = PageTemplate.LoadForTemplateContextAndName(this, this.Name + "." + nodeTemplateName, true);
                            // If not found, then we search directly for the asked template 'node.link'.
                            if (nodeTemplate == null || nodeTemplate.Data.Length == 0) {
                                nodeTemplate =  PageTemplate.LoadForTemplateContextAndName(this, nodeTemplateName, this._allowUndefinedTemplate());
                            }

                            nodeTemplate.InsertVariable("node.classes", "");
                            nodeTemplate.InsertVariable("node.type", contentNode.NodeType);
                            nodeTemplate.InsertTextForLanguage("node.type-title", "content-type-" + contentNode.NodeType);
                            nodeTemplate.InsertVariable("node.public-id", contentNode.PublicId);
                            nodeTemplate.InsertVariable("node.type-index", typeTracker[contentNode.NodeType]);
                            nodeTemplate.InsertVariable("node.type-index-mod2", (typeTracker[contentNode.NodeType]-1) % 2);
                            nodeTemplate.InsertVariableUnsafe("node.value", contentNode.ValueForTemplate);
                            contentNode.NodeTypeMeta.InsertAdditionalVariables(contentNode,nodeTemplate,"node");
                            // Options
                            foreach (var key in contentNode.Options.Keys) {
                                nodeTemplate.InsertVariable("node.option-" + key, contentNode.Options[key]);
                            }
                            translationContentTemplates.Add(nodeTemplate);
                            contentCount++;
                        }
                        template.InsertTemplates("translation.contents", translationContentTemplates);
                        // Add to stack
                        translationTemplates.Add(template);
                        summary = translation.Summary;
                    }
                }
            }

            // Final template insertion
            contentTemplate.InsertTemplates("node.translations", translationTemplates);
            this.InsertTemplate(variableName, contentTemplate);
            this.InsertVariableJSXMLSafe(variableName + ".summary", summary);
            this.InsertVariable(variableName + ".content-count", contentCount);
            this.InsertVariable(variableName + ".has-content", contentCount == 0 ? "no-content" : "has-content");

            // Return our summary
            return summary;
        }

        #endregion

        #region InsertVariable for Content Nodes Editor

        /// <summary>
        /// Inserts the content node editor for node path.
        /// The node (including its translations and content) is automatically loaded) if it exists.
        /// If the node at the path does not exist, a default empty editor is displayed.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="nodePath">The node path.</param>
        /// <param name="bindToInput">The bind to input.</param>
        /// <param name="defaultContentType">Default type of the content.</param>
        /// <param name="supportedContentTypes">The supported content types.</param>
        public void InsertContentNodeEditorForNodePath(ModelContext db, string variableName, string nodePath, string bindToInput = null, string defaultContentType = null, string supportedContentTypes = null, string contentSource = null) {
            ContentNode node = null;
            if (db != null && !string.IsNullOrEmpty(nodePath)) {
                node = db.ContentNodes()
                    .Include("Children")
                    .Include("Children.Children")
                    .GetNodeByPath(nodePath, false);
            }
            InsertContentNodeEditor(variableName, node, nodePath, bindToInput, defaultContentType, supportedContentTypes, contentSource);
        }

        /// <summary>
        /// Inserts the content node editor for a given ContentNode.
        /// If the node is null, then a empty editor is displayed.
        /// Note: The caller is responsible for making sure the Translation (node.Children) and Content (node.Children.Children) is loaded. 
        /// It is best to preload the tranlsations and content using:
        ///     .Include("Children").Include("Children.Children")
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="node">The node.</param>
        /// <param name="intendedPath">The intended path.</param>
        /// <param name="bindToInput">The bind to input.</param>
        /// <param name="defaultContentType">Default type of the content.</param>
        /// <param name="supportedContentTypes">The supported content types.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">The node must be a 'node' type for InsertContentNodeEditor!</exception>
        public int InsertContentNodeEditor(string variableName, ContentNode node, string intendedPath = null, string bindToInput = null, string defaultContentType = null, string supportedContentTypes = null, string contentSource = null) {

            // Init
            var editorTemplate = this.LoadTemplate("content-editor");
            List<string> supportedLangages = new List<string>(ContentNode.GetSupportedLanguages());
            var translationCount = 0;

            // Null node?
            if (node == null) {

                editorTemplate.InsertVariable("node.public-id", "");
                editorTemplate.InsertVariable("node.path", intendedPath);
                editorTemplate.InsertVariable("node.published", "false");
                editorTemplate.InsertVariable("node.layout", "");

            } else {

                // Validate
                if (node.NodeType != ContentNode.NODE_TYPE_NODE) throw new Exception("The node must be a 'node' type for InsertContentNodeEditor!");

                // Load children
                //node.Include("Children");
                //node.Include("Children.Children");

                // Insert basic variables
                editorTemplate.InsertVariable("node.path", node.Path);
                editorTemplate.InsertVariable("node.public-id", node.PublicId);
                editorTemplate.InsertVariable("node.published", node.Published);
                editorTemplate.InsertVariable("node.layout", node.Layout);
            }

            // Translation buttons
            {
                var templateStack = new List<PageTemplate>();
                supportedLangages.Insert(0, "default");
                supportedLangages.Add("add"); // add button
                foreach (string lang in supportedLangages) {
                    // Init
                    var template = this.LoadTemplate("content-editor.translation.button");
                    template.InsertVariable("translation.language", lang);
                    template.InsertTextForLanguage("translation.language-title", "lang-" + lang);
                    // Add to stack
                    templateStack.Add(template);
                }
                editorTemplate.InsertTemplates("node.translations.buttons", templateStack);
            }

            // Layout buttons
            {
                var layoutButtonsStack = new List<PageTemplate>();
                var layoutOptionsStack = new List<PageTemplate>();
                if (node != null) {
                    var layouts = node.Context.ContentLayouts();
                    foreach (var layout in layouts) {
                        // Button
                        var templateButton = this.LoadTemplate("content-editor.layout.button");
                        templateButton.InsertVariable("layout.name", layout.Name);
                        layoutButtonsStack.Add(templateButton);
                        // Options
                        var templateOptions = this.LoadTemplate("content-editor.layout.options");
                        var layoutOptionButtonsStack = new List<PageTemplate>();
                        foreach (var option in layout.Options.Keys) {
                            //TODO:@dan support for option values, not just toggle
                            var selected = false;
                            if (node != null) {
                                if (node.Options[option] != null) {
                                    selected = true;
                                }
                            }
                            var templateOption = this.LoadTemplate("content-editor.layout.options.button");
                            templateOption.InsertVariable("option.name", option);
                            if (selected) templateOption.InsertVariable("option.selected", "selected");
                            else templateOption.InsertVariable("option.selected", "");
                            layoutOptionButtonsStack.Add(templateOption);
                        }
                        templateOptions.InsertTemplates("layout.options", layoutOptionButtonsStack);
                        templateOptions.InsertVariable("layout.name", layout.Name);
                        layoutOptionsStack.Add(templateOptions);
                    }
                }
                editorTemplate.InsertTemplates("node.layouts.buttons", layoutButtonsStack);
                editorTemplate.InsertTemplates("node.layouts.options", layoutOptionsStack);
            }

            // Translations
            var translationTemplates = new List<PageTemplate>();
            if (node != null) {
                // Get all translations
                var translations = node.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION);
                // Iterate each translation (sorted by our default lang list)
                foreach (var translation in translations.OrderBy(t => supportedLangages.IndexOf(t.Value))) {
                    // Make sure it is loaded
                    translationCount++;
                    //translation.Include("Children");
                    // Common properties
                    var translationDisplayVal = translation.Value;
                    if (translationDisplayVal == null) translationDisplayVal = "default";

                    // Translation contents
                    {
                        // Init
                        var template = this.LoadTemplate("content-editor.translation");
                        var translationContentTemplates = new List<PageTemplate>();
                        // Translation variables
                        template.InsertVariable("translation.public-id", translation.PublicId);
                        template.InsertVariable("translation.language", translationDisplayVal);
                        template.InsertTextForLanguage("translation.language-title", "lang-" + translationDisplayVal);
                        // Contents
                        foreach (var contentNode in translation.GetContentChildren()) {
                            var nodeTemplate = this.LoadTemplate("content-editor.node-template");
                            nodeTemplate.InsertVariable("node.classes", "");
                            nodeTemplate.InsertVariable("node.type", contentNode.NodeType);
                            nodeTemplate.InsertTextForLanguage("node.type-title", "content-type-" + contentNode.NodeType);
                            nodeTemplate.InsertVariable("node.public-id", contentNode.PublicId);
                            nodeTemplate.InsertVariable("node.value", contentNode.ValueForEditor);
                            nodeTemplate.InsertVariableJSONXMLSafe("node.options", contentNode.Options.Data);
                            translationContentTemplates.Add(nodeTemplate);
                        }
                        template.InsertTemplates("translation.contents", translationContentTemplates);
                        // Add to stack
                        translationTemplates.Add(template);
                    }
                }
            }

            // Add Translation template
            {
                // Init
                var template = this.LoadTemplate("content-editor.translation");
                var translationContentTemplates = new List<PageTemplate>();
                // Translation variables
                template.InsertVariable("translation.public-id", "");
                template.InsertVariable("translation.language", "add");
                template.InsertTextForLanguage("translation.language-title", "lang-" + "add");
                template.InsertVariable("translation.contents", "");
                // Add to stack
                translationTemplates.Add(template);
            }

            // Insert variables for translations (contents, buttons, count)
            editorTemplate.InsertTemplates("node.translations", translationTemplates);

            // Content Template
            {
                var template = this.LoadTemplate("content-editor.node-template");
                template.InsertVariable("node.classes", "template hidden-content");
                template.InsertVariable("node.type", "");
                template.InsertVariable("node.type-title", "");
                template.InsertVariable("node.public-id", "");
                template.InsertVariable("node.value", "");
                template.InsertVariable("node.options", "");
                editorTemplate.InsertTemplate("editor.node-template", template);
            }

            // Content Node Types
            {
                var templates = new List<PageTemplate>();
                foreach (var contentType in ContentNode.GetAllTypeMetasForContent()) {
                    var template = this.LoadTemplate("content-editor.node-type");
                    template.InsertVariable("content-type.id", contentType.ID);
                    template.InsertVariable("content-type.allow-multiple", contentType.AllowMultiplePerPage());
                    template.InsertVariable("content-type.category", contentType.GetCategory());
                    template.InsertVariable("content-type.icon", contentType.GetIcon());
                    template.InsertTextForLanguage("content-type.title", "content-type-" + contentType.ID);
                    templates.Add(template);
                }
                editorTemplate.InsertTemplates("editor.content-types", templates);
            }
            // Generic vars
            if (defaultContentType == null) defaultContentType = "html";
            editorTemplate.InsertVariable("editor.bind-to-input", bindToInput);
            editorTemplate.InsertVariable("editor.default-content-type", defaultContentType);
            editorTemplate.InsertVariable("editor.supported-content-types", supportedContentTypes);
            editorTemplate.InsertVariable("editor.content-source", contentSource);
            // Final template insertion
            this.InsertTemplate(variableName, editorTemplate);
            return translationCount;
        }

        #endregion

        #region Generate HTML for Content Nodes

        public void InsertHTMLContent(string path, bool onlyPublished = true, bool includeChildren = true, string customName = null, string templateName = "content-html") {
            this.ChangeTemplate(templateName);
            var template = RecursivelyInsertHTMLContent(
                template: null,
                path: path,
                onlyPublished: onlyPublished,
                includeChildren: includeChildren,
                customName: customName,
                templateName: templateName
                );
            this.InsertTemplate("pages", template);
        }

        public void InsertHTMLContent(Model.ModelObject entity, bool onlyPublished = true, bool includeChildren = true, string customName = null, string templateName = "content-html") {
            InsertHTMLContent(
                path: ContentNode.GetNodePathForEntity(entity),
                onlyPublished: onlyPublished,
                includeChildren: includeChildren,
                customName: customName,
                templateName: templateName
            );
        }

        public PageTemplate RecursivelyInsertHTMLContent(
            PageTemplate template,
            string path,
            bool onlyPublished,
            bool includeChildren,
            string customName,
            string templateName = "content-html",
            bool throwExceptionIfNotExists = true) {

            if (template == null) template = this.LoadTemplate(templateName + ".page");

            // Get node
            ContentNode node = this.Handler.DB.ContentNodes()
                .Include("Children")
                .Include("Children.Children")
                .GetNodeByPath(path, throwExceptionIfNotExists);

            // Skip if published or null
            if (
                node == null
                ||
                (onlyPublished && node.Published == false)
                ||
                node.Children.Count == 0) {
                template.Data.Clear();
                return template;
            }
            
            // Basic variables
            template.InsertVariable("node.name", customName != null ? customName : node.Name);
            template.InsertVariable("node.path", node.Path.Trim('/'));

            // Previews
            {
                var subtemplates = new List<PageTemplate>();
                foreach (var preview in node.GetNodeCMSPreviews()) {
                    var subtemplate = this.LoadTemplate(templateName+".page.preview");
                    subtemplate.InsertVariable("preview.route-path", preview.RoutePath);
                    subtemplate.InsertVariable("preview.url", Core.Config.PublicURL + preview.RoutePath);
                    subtemplates.Add(subtemplate);
                }
                template.InsertTemplates("previews", subtemplates);
            }
            
            // Translation headers
            {
                var subtemplates = new List<PageTemplate>();
                foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                    var subtemplate = this.LoadTemplate(templateName+".page.translation.header");
                    subtemplate.InsertVariable("language", lang.ToUpper());
                    subtemplates.Add(subtemplate);
                }
                template.InsertTemplates("translation-headers", subtemplates);
            }

            // Content
            {
                var subtemplates = new List<PageTemplate>();
                foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                    var subtemplate = this.LoadTemplate(templateName+".page.translation");
                    subtemplate.InsertContent(
                        variableName: "content", 
                        node: node, 
                        language: lang, 
                        useFallbackLanguage: false
                    );
                    subtemplates.Add(subtemplate);
                }
                template.InsertTemplates("translations", subtemplates);
            }

            // Sub-pages
            {
                var subtemplates = new List<PageTemplate>();
                if (includeChildren) {
                    foreach (var child in node.ChildrenForType(ContentNode.NODE_TYPE_NODE).OrderBy(n => n.Sort)) {
                        var subtemplate = RecursivelyInsertHTMLContent(
                            template: null,
                            path: child.Path,
                            onlyPublished: onlyPublished,
                            includeChildren: includeChildren,
                            customName: null,
                            templateName: templateName
                        );
                        subtemplates.Add(subtemplate);
                    }
                }
                template.InsertTemplates("children", subtemplates);
                template.InsertVariable("children.count", subtemplates.Count);
            }

            return template;

        }

        #endregion 
    }
}
