using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using System.Reflection;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        /// <summary>
        /// Inserts a menu created by the menu builder.
        /// An entire menu tree can be inserted, using the variable name,
        /// or individual sections can be inserted using {VARIABLE_NAME.section-SECTION_ID}.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="menu">The menu.</param>
        /// <param name="templateName">Name of the template.</param>
        public void InsertMenu(string variableName, Core.Builder.MenuBuilder menu, string templateName = "menu") {

            var sectionTemplates = new List<PageTemplate>();
            var sectionCount = 0;
            List<string> menuClasses = new List<string>();

            foreach (var section in menu.GetSections().OrderBy(s => s.Sort != null).ThenBy(s => s.Sort).ThenBy(s=>s.Title)) {
                if (section.Hidden == true) continue;
                sectionCount++;

                var sectionTemplate = this.LoadTemplate(templateName + ".section");
                sectionTemplate.InsertVariable("section.title", section.Title);
                sectionTemplate.InsertVariable("section.id", section.ID);
                sectionTemplate.InsertVariable("section.icon", section.Icon);
                sectionTemplate.InsertVariable("section.classes", section.Classes + (section.Selected?" selected":""));

                if (section.OnClick != null) {
                    sectionTemplate.InsertVariable("section.on-click", section.OnClick);
                    sectionTemplate.InsertVariable("section.path", "");
                } else {
                    sectionTemplate.InsertVariable("section.path", section.Path);
                }

                sectionTemplate.InsertVariable("section.path", section.Path);
                sectionTemplate.InsertVariable("section.selected", section.Selected);
                sectionTemplate.InsertVariable("section.sort", section.Sort);
                
                var itemTemplates = new List<PageTemplate>();
                foreach(var item in section.Items.OrderBy(s => s.Sort != null).ThenBy(s => s.Sort)) {
                    if (item.Hidden == true) continue;

                    var itemTemplate = this.LoadTemplate(templateName + ".section.item");
                    itemTemplate.InsertVariable("item.title", item.Title);
                    itemTemplate.InsertVariable("item.id", item.ID);
                    itemTemplate.InsertVariable("item.icon", item.Icon);
                    itemTemplate.InsertVariable("item.classes", item.Classes + (item.Selected?" selected":""));
                    if (item.OnClick != null) {
                        itemTemplate.InsertVariable("item.on-click", item.OnClick);
                        itemTemplate.InsertVariable("item.path", "");
                    } else {
                        itemTemplate.InsertVariable("item.path", item.Path);
                    }
                   
                    itemTemplate.InsertVariable("item.on-click", item.OnClick);
                    itemTemplate.InsertVariable("item.sort", item.Sort);
                    itemTemplate.InsertVariable("item.selected", item.Selected);
                    itemTemplate.InsertVariable("item.target", item.Target);
                    itemTemplate.InsertVariable("item.target-attribute", item.Target != null ? "target=\""+item.Target+"\"":"");

                    // Subitems?
                    if(item.SubItems != null && item.SubItems.Count > 0) {
                        var subItemTemplates = new List<PageTemplate>();
                        foreach (var subItem in item.SubItems) {
                            if (subItem.Hidden == true) continue;

                            var subItemTemplate = this.LoadTemplate(templateName + ".section.item");
                            subItemTemplate.InsertVariable("item.title", subItem.Title);
                            subItemTemplate.InsertVariable("item.id", subItem.ID);
                            subItemTemplate.InsertVariable("item.icon", subItem.Icon);
                            subItemTemplate.InsertVariable("item.classes", subItem.Classes + (subItem.Selected ? " selected" : ""));
                            subItemTemplate.InsertVariable("item.path", subItem.Path);
                            subItemTemplate.InsertVariable("item.selected", subItem.Selected);
                            subItemTemplate.InsertVariable("item.target", subItem.Target);
                            subItemTemplate.InsertVariable("item.target-attribute", subItem.Target != null ? "target=\"" + subItem.Target + "\"" : "");
                            subItemTemplate.InsertVariable("item.sub-items", "");
                            subItemTemplate.InsertVariable("item.has-sub-items", "false");
                            subItemTemplates.Add(subItemTemplate);
                        }
                        itemTemplate.InsertTemplates("item.sub-items", subItemTemplates);
                        itemTemplate.InsertVariable("item.has-sub-items", "true");
                    } else {
                        itemTemplate.InsertVariable("item.sub-items", "");
                        itemTemplate.InsertVariable("item.has-sub-items", "false");
                    }

                    itemTemplates.Add(itemTemplate);
                }

                sectionTemplate.InsertTemplates("section.items", itemTemplates);
                sectionTemplate.DiscoverVariables();

                sectionTemplates.Add(sectionTemplate);
                this.InsertTemplate(variableName+".section-"+section.ID,sectionTemplate);
            }

            // Insert the templates
            this.InsertTemplates(variableName + ".sections", sectionTemplates);

            // Bookkeeping
            if (sectionCount > 10) menuClasses.Add("option-more-than-10-sections");
            if (sectionCount > 15) menuClasses.Add("option-more-than-15-sections");
            if (sectionCount > 20) menuClasses.Add("option-more-than-20-sections");
            this.InsertVariable(variableName + ".section-count", sectionCount);
            this.InsertVariable(variableName + ".classes", string.Join(" ",menuClasses));
        }
        
        
    }
}
