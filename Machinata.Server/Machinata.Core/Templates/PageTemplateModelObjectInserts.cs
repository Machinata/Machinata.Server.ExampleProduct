using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Reflection;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public void InsertVariables(string variableName, Core.Model.ModelObject entity) {
            InsertVariables(variableName, entity, new FormBuilder());
        }

        public void InsertVariables(string variableName, Core.Model.ModelObject entity, FormBuilder form) {
            var props = entity.GetPropertiesForForm(form);
            foreach(var prop in props) {
                var formProp = new EntityFormBuilderProperty(entity,prop,form);
                var name = variableName + "." + prop.GetVariableName();
                if (prop.PropertyType == typeof(Model.Properties)) {
                    var ps = formProp.GetPropertyValue() as Model.Properties;
                    ps.LoadDefaultsForProperty(prop);
                    foreach(var k in ps.Keys) {
                        this.InsertVariable(name + "." + k.ToDashedLower(), ps[k]);
                    }
                    this.InsertVariableUnsafe(name + "-json", ps.Data);
                    this.InsertVariableUnsafe(name + "-js", ps.Data==null?"null":ps.Data);

                } else {
                    this.InsertVariable(name, formProp.GetFormattedPropertyValue(this.Language));
                    if (this.HasVariable(name + "-unformatted")) this.InsertVariable(name + "-unformatted", formProp.GetPropertyValue());
                    if (this.HasVariable(name + "-raw")) this.InsertVariable(name + "-raw", formProp.GetPropertyValue());
                    if (this.HasVariable(name + "-unsafe")) this.InsertVariableUnsafe(name + "-unsafe", formProp.GetPropertyValue() as string);
                    if(formProp.GetPropertyType() == typeof(DateTime)) {
                        var value = formProp.GetPropertyValue();
                        if (value != null) {
                            var dt = (DateTime)value;
                            this.InsertVariable(name + ".date", dt.ToString(Core.Config.DateFormat));
                            this.InsertVariable(name + ".time", dt.ToString(Core.Config.TimeFormat));
                        } else {
                            this.InsertVariable(name + ".date", string.Empty);
                            this.InsertVariable(name + ".time", string.Empty);
                        }
                    }
                }
                if (prop.PropertyType.IsNullableType()) {
                    if (prop.GetValue(entity) == null) {
                        this.InsertVariable(name + ".is-null", "null");
                    }
                    else {
                        this.InsertVariable(name + ".is-null", string.Empty);
                    }

                }
            }
        }
        
        /// <summary>
        /// Automatically inserts a stack of templates for each entity.
        /// If the action forEachEntity is specified, then this is called
        /// for each entity/template.
        /// </summary>
        public void InsertTemplates<T>(string variableName, IEnumerable<T> entities, string templateName, Action<T,PageTemplate> forEachEntity = null, string entityVariableName = "entity") where T : ModelObject {
            var templates = new List<PageTemplate>();
            int count = 0;
            foreach(var entity in entities) {
                count++;
                var template = this.LoadTemplate(templateName);
                template.InsertVariables(entityVariableName, entity);
                template.InsertVariable(entityVariableName+".list-number", count);
                if (forEachEntity != null) forEachEntity(entity, template);
                templates.Add(template);
            }
            this.InsertTemplates(variableName, templates);
        }

        /// <summary>
        /// Automatically inserts a stack of templates for each object.
        /// If the action forEachEntity is specified, then this is called
        /// for each object template.
        /// </summary>
        public void InsertTemplatesForObjects<T>(string variableName, IEnumerable<T> entities, string templateName, Action<T, PageTemplate> forEachEntity = null)  {
            var templates = new List<PageTemplate>();
            int count = 0;
            foreach (var entity in entities) {
                count++;
                var template = this.LoadTemplate(templateName);
                template.InsertVariable("entity.list-number", count);
                if (forEachEntity != null) forEachEntity(entity, template);
                templates.Add(template);
            }
            this.InsertTemplates(variableName, templates);
        }


        /// <summary>
        /// Automatically inserts a stack of templates for each grouped series of entities.
        /// You can specify both a template for the group, and for the entity
        /// </summary>
        public void InsertTemplates(string variableName, IEnumerable<IGrouping<object,ModelObject>> groupedEntities, string entityTemplateName, string groupTemplateName) {
            var groupTemplates = new List<PageTemplate>();
            foreach(var group in groupedEntities) {
                var groupTemplate = this.LoadTemplate(groupTemplateName);
                groupTemplate.InsertTemplates("group.entities",group.ToList(),entityTemplateName);
                groupTemplate.InsertVariable("group.name", group.Key);
                groupTemplate.InsertVariable("group.id", Core.Util.String.CreateShortURLForName(group.Key.ToString()));
                groupTemplates.Add(groupTemplate);
            }
            this.InsertTemplates(variableName, groupTemplates);
        }        
    }
}
