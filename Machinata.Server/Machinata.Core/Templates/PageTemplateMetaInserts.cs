using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {
        

        public void InsertMetaInformation(string variableName, MetaInformation meta) {
            this.InsertVariable(variableName + ".title", meta.Title);
            this.InsertVariable(variableName + ".description", meta.Description);
            this.InsertVariable(variableName + ".keywords", meta.Keywords);
            this.InsertVariable(variableName + ".image", meta.Image);
        }

        public void InsertMetaTags(string variableName, MetaInformation meta) {
            /*
            <meta property="og:image" content="http://boldomatic.icasa.office.nerves.ch/content/post/6DUzZQ/fuck-you-silicon-valley?size=800"/>
            <meta property="og:title" content="Post by LanguageMany on Boldomatic"/>
            <meta property="og:description" content="fuck you silicon valley"/>
            <meta property="og:type" content="article"/>
            <meta property="og:url" content="http://boldomatic.icasa.office.nerves.ch/p/6DUzZQ/fuck-you-silicon-valley"/>
            <meta property="og:image:width" content="1544"/>
            <meta property="og:image:height" content="800"/>
            <meta property="og:article:author" content="LanguageMany"/>
            <meta property="twitter:title" content="Post by LanguageMany on Boldomatic"/>
            <meta property="twitter:description" content="fuck you silicon valley"/>
            <meta property="twitter:image:source" content="http://boldomatic.icasa.office.nerves.ch/content/post/6DUzZQ/fuck-you-silicon-valley?size=800"/>
            <meta property="twitter:site" content="Boldomatic"/>
            <meta property="twitter:url" content="http://boldomatic.icasa.office.nerves.ch/p/6DUzZQ/fuck-you-silicon-valley"/>
            <meta property="twitter:card" content="photo"/>
            <meta name="title" content="Post by LanguageMany on Boldomatic"/>
            <meta name="description" content="fuck you silicon valley"/>
            <meta name="author" content="LanguageMany"/>
            */
            var defaultOGType = "article";
            var defaultTwitterType = "photo";
            StringBuilder tags = new StringBuilder();
            if(!string.IsNullOrEmpty(meta.Title)) {
                tags.AppendLine($"<meta property=\"og:title\" content=\"{_tagSafe(meta.Title)}\" />");
                tags.AppendLine($"<meta property=\"twitter:title\" content=\"{_tagSafe(meta.Title)}\" />");
                tags.AppendLine($"<meta name=\"title\" content=\"{_tagSafe(meta.Title)}\" />");
            }
            if (!string.IsNullOrEmpty(meta.Description)) {
                tags.AppendLine($"<meta property=\"og:description\" content=\"{_tagSafe(meta.Description)}\" />");
                tags.AppendLine($"<meta property=\"twitter:description\" content=\"{_tagSafe(meta.Description)}\" />");
                tags.AppendLine($"<meta name=\"description\" content=\"{_tagSafe(meta.Description)}\" />");
            }
            if (!string.IsNullOrEmpty(meta.Author)) {
                tags.AppendLine($"<meta property=\"og:article:author\" content=\"{_tagSafe(meta.Author)}\" />");
                tags.AppendLine($"<meta name=\"author\" content=\"{_tagSafe(meta.Author)}\" />");
            }
            if (!string.IsNullOrEmpty(meta.Image)) {
                string fullImageURL = meta.Image;
                if (!fullImageURL.StartsWith("http")) fullImageURL = Core.Config.PublicURL + fullImageURL;
                tags.AppendLine($"<meta property=\"og:image\" content=\"{_tagSafe(fullImageURL)}\" />");
                tags.AppendLine($"<meta property=\"og:type\" content=\"{_tagSafe(defaultOGType)}\" />");
                tags.AppendLine($"<meta property=\"twitter:image:source\" content=\"{_tagSafe(fullImageURL)}\" />");
                tags.AppendLine($"<meta property=\"twitter:card\" content=\"{_tagSafe(defaultTwitterType)}\" />");
            }
            this.InsertVariableUnsafe(variableName, tags);
        }

        private static string _tagSafe(string str) {
            if (str == null) return "";
            return str.Replace("\"", "\\\"").Replace("\n"," ");
        }
        
        
    }
}
