using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using System.Reflection;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public void InsertForm(string variableName, FormBuilder form, string action = null, string apiCall = null, string onSuccess = null, string onError = null) {
            InsertForm(variableName, null, form, action, apiCall, onSuccess, onError);
        }

        public PageTemplate _createFormPropertyTemplate(Machinata.Core.Model.ModelObject entity, FormBuilderProperty formProp, PropertyInfo prop, string templateName = "entity-form.property") {
            // Init
            PageTemplate propertyTemplate = null;
            if (formProp.GetPropertyType() == typeof(ContentNode)) {
                // Sanity
                if (entity.Context == null) throw new Exception("Inserting entity forms which Content Nodes for entities which are not yet in the DB is currently not supported.");
                // Create the template
                propertyTemplate = this.LoadTemplate(templateName +  ".content-node");
                var nodePath = ContentNode.GetNodePathForEntityProperty(entity, formProp.GetPropertyName());
                string defaultContentType = null;
                string supportedContentTypes = null;
                if (formProp.GetContentTypes() != null) supportedContentTypes = string.Join(",", formProp.GetContentTypes());
                propertyTemplate.InsertContentNodeEditorForNodePath(entity.Context, "content-editor", nodePath,formProp.GetFormName(), defaultContentType, supportedContentTypes, formProp.Form.ContentSource);
            } else if (formProp.GetPropertyType() == typeof(Properties)) {
                propertyTemplate = this.LoadTemplate(templateName);
                var val = formProp.GetPropertyValue() as Properties;
                val.LoadDefaultsForProperty(prop);
            } else {
                propertyTemplate = this.LoadTemplate(templateName);
            }
            // Get the options based on type (if at all)
            var formValue = formProp.GetFormattedPropertyValue(this.Language);
            string propOptionsTitles = null;
            string propOptionsValues = null;
            if (formProp.GetPropertyType().IsEnum) {
                var opts = System.Enum.GetNames(formProp.GetPropertyType());
                var titles = new List<string>();
                var enumValues = System.Enum.GetValues(formProp.GetPropertyType());
                foreach(Enum e in enumValues) {
                    titles.Add(e.GetEnumTitle(this.Language));
                }
                propOptionsValues = string.Join(",", opts);
                propOptionsTitles = string.Join(",", titles);
            } else if (formProp.GetPropertyType() == typeof(bool?)) {
                propOptionsValues = ",False,True";
                propOptionsTitles = "{text.null},{text.false},{text.true}";
                var val = formProp.GetPropertyValue();
                if(val == null) formValue = null;
                else formValue = val.ToString();
            } else if (formProp.GetPropertyType() == typeof(bool)) {
                propOptionsValues = "False,True";
                propOptionsTitles = "{text.false},{text.true}";
                formValue = formProp.GetPropertyValue().ToString();
            } else if (formProp.GetFormType() == "checkboxlist" || formProp.GetFormType() == "radiolist" ) {
                var opts = formProp.GetFormOptions();
                if (opts == null) throw new Exception($"The form property {formProp.GetPropertyName()} is null or invalid for a Dictionary<string,string>.");
                if (opts.Keys.Any(k => k.Contains(","))) throw new Exception($"A {formProp.GetFormType()} form property cannot contain a options key with a ',' in it.");
                if (opts.Values.Any(k => k.Contains(","))) throw new Exception($"A {formProp.GetFormType()} form property cannot contain a options value with a ',' in it.");
                propOptionsTitles = string.Join(",",opts.Keys);
                propOptionsValues = string.Join(",",opts.Values);
            } else if (typeof(ModelObject).IsAssignableFrom(formProp.GetPropertyType())) {
                var db = this.Handler.DB;
                if (db == null) throw new Exception("Could not get a ModelObject reference for property options listing because the template context is null.");
                var set = db.GetSetQueryableByName(formProp.GetPropertyType().Name);
                if (set == null) throw new Exception($"Could not get a DbSet for property because the type {formProp.GetPropertyType().Name} is not known.");
                var entities = set.ToList().OrderBy(e => e.ToString());
                propOptionsValues = string.Join(",", entities.Select(e=>e.PublicId));
                propOptionsTitles = string.Join(",", entities.Select(e=>e.ToString()));
            } else {
                var opts = formProp.GetFormOptions();
                if(opts != null && opts.Count > 0) {
                    propOptionsValues = string.Join(",", opts.Values);
                    propOptionsTitles = string.Join(",", opts.Keys);
                }
                
            }
            // Common classes
            var classes = "";
            if (formProp.GetFormType() == "hidden") classes += " " + "hidden";
            // Insert variables
            propertyTemplate.InsertVariable("property.classes", classes);
            propertyTemplate.InsertVariable("property.title",formProp.GetPropertyTitle(this.Language));
            propertyTemplate.InsertVariable("property.tooltip", formProp.GetPropertyTooltip(this.Language));
            propertyTemplate.InsertVariable("property.help", formProp.GetPropertyHelp(this.Language));
            propertyTemplate.InsertVariable("property.name",formProp.GetPropertyName());
            propertyTemplate.InsertVariable("property.type",formProp.GetPropertyType().Name);
            propertyTemplate.InsertVariable("property.parent.type",entity?.TypeName);
            propertyTemplate.InsertVariable("property.options",propOptionsValues);
            propertyTemplate.InsertVariable("property.options-titles",propOptionsTitles);
            propertyTemplate.InsertVariable("property.value",formValue);
            propertyTemplate.InsertVariable("property.raw-value",formProp.GetPropertyValue());
            propertyTemplate.InsertVariable("property.universal-value",formProp.GetUniversalPropertyValue(this.Language));
            propertyTemplate.InsertVariable("property.form-name",formProp.GetFormName());
            propertyTemplate.InsertVariable("property.form-type",formProp.GetFormType());
            propertyTemplate.InsertVariable("property.min-length", formProp.GetFormMinLength());
            propertyTemplate.InsertVariable("property.max-length", formProp.GetFormMaxLength());
            propertyTemplate.InsertVariable("property.min-value", formProp.GetFormMinValue());
            propertyTemplate.InsertVariable("property.max-value", formProp.GetFormMaxValue());
            propertyTemplate.InsertVariable("property.required",formProp.GetFormRequired());
            propertyTemplate.InsertVariable("property.readonly",formProp.GetFormReadOnly());
            propertyTemplate.InsertVariable("property.required-star",formProp.GetFormRequired()?"*":"");
            propertyTemplate.InsertVariable("property.format",formProp.GetFormFormat());
            propertyTemplate.InsertVariable("property.pattern",formProp.GetFormPattern());
            propertyTemplate.InsertVariable("property.placeholder",formProp.GetFormPlaceholder());
            propertyTemplate.InsertVariable("property.autocomplete",formProp.GetFormAutoComplete());
            // Return
            return propertyTemplate;
        }
        

        public void InsertForm(string variableName, Core.Model.ModelObject entity, FormBuilder form, string action = null, string apiCall = null, string onSuccess = null, string onError = null) {
            // Init
            var formTemplate = this.LoadTemplate("entity-form");
            List<PropertyInfo> properties = null;
            if (entity != null) properties = entity.GetPropertiesForForm(form);
            else properties = new List<PropertyInfo>();
            var propertyTemplates = new List<PageTemplate>();
            // Remove custom from regular (custom overrides)
            if (form.CustomProperties != null) {
                foreach (var cp in form.CustomProperties) {
                    properties.RemoveAll(p => p.GetFormName(form) == cp.GetFormName());
                }
            }
            // Do entity properties
            foreach(var prop in properties) {
                // Create template
                PageTemplate propertyTemplate = _createFormPropertyTemplate(entity,new EntityFormBuilderProperty(entity,prop,form),prop);
                propertyTemplates.Add(propertyTemplate);
            }
            // Do custom
            if (form.CustomProperties != null) {
                foreach (var prop in form.CustomProperties) {
                    // Create template
                    PageTemplate propertyTemplate = _createFormPropertyTemplate(entity,prop,null);
                    propertyTemplates.Add(propertyTemplate);
                }
            }
            formTemplate.InsertTemplates("properties",propertyTemplates);

            // Buttons
            if (form.Buttons.Any()) {
                var button = form.Buttons.First();
                formTemplate.InsertVariable("form.button-value",button.Value);
                formTemplate.InsertVariable("form.button-name", button.Name);
            }
            else {
                formTemplate.InsertVariable("form.button-value", "{text.form-save}");
                formTemplate.InsertVariable("form.button-name", "button");
            }


            // General variables
            formTemplate.InsertVariable("form.content-source", form.ContentSource);
            if (onSuccess != null) formTemplate.InsertVariableUnsafe("form.on-success", $"on-success=\"{onSuccess}\"");
            else formTemplate.InsertVariable("form.on-success", "");
            if (onError != null) formTemplate.InsertVariableUnsafe("form.on-error", $"on-error=\"{onError}\"");
            else formTemplate.InsertVariable("form.on-error", "");
            if (apiCall != null) formTemplate.InsertVariableUnsafe("form.api-call", $"api-call=\"{apiCall}\"");
            else formTemplate.InsertVariable("form.api-call", "");
            if (action != null) formTemplate.InsertVariableUnsafe("form.action", $"action=\"{action}\"");
            else formTemplate.InsertVariable("form.action", "");
            formTemplate.InsertVariable("form.method", form.Method);
            formTemplate.InsertVariable("form.form-builder-id", form.Id);
            this.InsertTemplate(variableName,formTemplate);
        }


        /// <summary>
        /// Inserts the given variables from context into the template eg. "name" -> this.InsertVariable("name") 
        /// Only string vars
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="variables">The variables.</param>
        public void InsertVariablesFromContext(Core.Handler.Handler handler, IEnumerable<string> variables = null, bool multiline = false) {
            foreach (var variable in variables) {
                var data = handler.Params.String(variable);
                if (multiline && data.Contains("\n")) {
                    this.InsertVariableXMLSafeWithLineBreaks(variable, data);
                } else {
                    this.InsertVariable(variable,data );
                }
            }
        }

    }
}
