using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Templates {

    public partial class PageTemplate {

        #region Factory Methods

        public PageTemplate Copy() {
            // Note: this is explicitly not using the no-longer-recommended IClonable
            return new PageTemplate(this);
        }
        
        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {

            // Discover all page template handlers
            _logger.Trace("Search for PageTemplate handlers...");
            var pageTemplateHandlers = Core.Handler.Handler.GetBlankHandlersForType<Handler.PageTemplateHandler>();
            var pageTemplateExtensions = new List<string>();
            _logger.Trace("Found  "+pageTemplateHandlers.Count);
            foreach(var handler in pageTemplateHandlers) {
                _logger.Trace("Found PageTemplateHandler "+handler.Name+" ("+handler.TemplateExtension+")");
                if(!pageTemplateExtensions.Contains("*"+handler.TemplateExtension)) pageTemplateExtensions.Add("*"+handler.TemplateExtension);
            }
            // Discover all templates
            _logger.Trace("Searching in "+Core.Config.TemplatePath+" for "+string.Join(",",pageTemplateExtensions)+"...");
            string[] templateFiles = Core.Util.Files.GetFiles(
                Core.Config.TemplatePath, 
                string.Join(",",pageTemplateExtensions),
                SearchOption.AllDirectories);
            foreach(var templateFilePath in templateFiles) {
                // Get the filename
                var templateFile = templateFilePath.Replace(Core.Config.TemplatePath, "").TrimStart(Core.Config.PathSep[0]);
                templateFile = templateFile.Replace(Core.Config.PathSep, "/");
                // Break it down into package and file
                var templateFileSegs = templateFile.Split('/');
                var templatePackage = templateFileSegs.First();
                var templateFileName = string.Join("/",templateFileSegs.Skip(1).Take(templateFileSegs.Length - 1).ToArray());
                var templateName = Core.Util.Files.GetPathWithoutExtension(templateFileName);
                var templateExtension = Core.Util.Files.GetFileExtension(templateFileName);
                _logger.Trace("  Discovered "+templateName + " ("+templateExtension+") in "+templatePackage);

                // Pre-Load
                PageTemplate.Cache.Get(templatePackage, templateName, templateExtension);
            }
        }

        public static PageTemplate LoadForHandlerAndRoutePath(Handler.PageTemplateHandler handler, string routePath, bool returnEmptyTemplateIfNotExists) {
            return LoadForPackageNameAndRoutePath(handler.PackageName, handler.FallbackPackageName, routePath,handler.TemplateExtension, returnEmptyTemplateIfNotExists);
        }

        /// <summary>Loads a template with the given name for the current template context.</summary>
        /// <param name="template">The template.</param>
        /// <param name="name">The name. Note: if the name starts with ```/``` the current context template name is ignored and the search for the template will begin at the path given by the name.</param>
        /// <param name="returnEmptyTemplateIfNotExists">if set to <c>true</c> [return empty template if not exists].</param>
        /// <returns></returns>
        public static PageTemplate LoadForTemplateContextAndName(PageTemplate template, string name, bool returnEmptyTemplateIfNotExists) {
            
            _logger.Trace("Loading template for "+template.Name+" with name "+name);

            // Figure out the new name
            var namePath = template.Name.Split('/');
            if (namePath.Length > 1) namePath = namePath.Take(namePath.Length - 1).ToArray();
            string templateName = String.Join("/",namePath) + "/" + name;
            if(name.StartsWith("/")) {
                templateName = name.Substring(1); // trim leading /
            }

            // Load the template...
            var ret = PageTemplate.Cache.Find(template.Package, template.Handler?.FallbackPackageName, templateName, template.Extension, !returnEmptyTemplateIfNotExists);

            // Do we want a blank copy if not exist?
            if (ret == null) ret = template.GetBlankCopy(template.Name+"."+templateName);
            ret.Handler = template.Handler;
            ret.Language = template.Language;
            ret.Currency = template.Currency;
            return ret;
        }

        public static PageTemplate LoadForPackageNameAndRoutePath(string packageName, string fallbackPackageName, string routePath, string extension, bool returnEmptyTemplateIfNotExists) {

            _logger.Trace("Loading template for "+packageName+" with route "+routePath);

            // Index default path?
            if (routePath == "/") routePath = "/default";
            // Get the template name based on the routepath
            var templateName = routePath;
            // Replace vars
            templateName = Core.Routes.Route.RouteParamRegex.Replace(templateName, "");
            templateName = Core.Routes.Route.RouteOptionalParamRegex.Replace(templateName, "");
            // Trim trailing slash
            templateName = templateName.TrimStart('/');
            templateName = templateName.TrimEnd('/');
            // Trim double slash
            templateName = templateName.Replace("//","/");
            // Load the template
            var ret = PageTemplate.Cache.Find(packageName, fallbackPackageName, templateName, extension, !returnEmptyTemplateIfNotExists);
            if (ret == null) ret = new PageTemplate(packageName, templateName, extension);
            return ret;
        }

        #endregion

    }
}
