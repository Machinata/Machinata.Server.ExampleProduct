using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using System.Reflection;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public void InsertEntityList(
            string variableName, 
            System.Collections.ICollection entities, 
            FormBuilder form = null,
            string link = null, 
            bool loadFirstLevelReferences = false,
            FormBuilder total = null) {
            var list = new List<Core.Model.ModelObject>();
            if (entities != null) {
                foreach (var obj in entities) {
                    list.Add(obj as Core.Model.ModelObject);
                }
            }
            InsertEntityList(
                variableName: variableName,
                entities: list.AsQueryable(),
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences,
                total: total
            );
        }

        public void InsertEntityList(
           string variableName,
           ModelObject entity,
           FormBuilder form = null,
           string link = null,
           bool loadFirstLevelReferences = false,
           FormBuilder total = null) {
            var list = new List<Core.Model.ModelObject>();
            if (entity != null) {
                list.Add(entity as Core.Model.ModelObject);
            }
            InsertEntityList(
                variableName: variableName,
                entities: list.AsQueryable(),
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences,
                total: total
            );
        }

        /// <summary>
        /// Insert a entity listing using the specified form for a list of entities.
        /// If a link is given, then each entity is automatically linked. Links may include variables such as
        /// '{entity.public-id}'.
        /// </summary>
        public void InsertEntityList(
            string variableName, 
            IQueryable<Core.Model.ModelObject> entities, 
            FormBuilder form = null, 
            string link = null, 
            bool loadFirstLevelReferences = false, 
            bool enableAutoSortAndFilters = true,
            FormBuilder total = null) {
            _insertEntityList(
                variableName: variableName,
                entities: entities,
                selectedEntities: null,
                selectionAPICall: null,
                quantityEntities: null,
                quantityAPICall: null,
                allowMultipleSelection: false,
                enableAutoSortAndFilters: enableAutoSortAndFilters,
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences,
                total: total
            );
        }

        /// <summary>
        /// Insert a entity listing using the specified form for a list of entities.
        /// If a editAPICall is given, the list can be edited and saved via the apicall
        /// </summary>
        public void InsertEditableEntityList(
            string variableName,
            IQueryable<Core.Model.ModelObject> entities,
            string editAPICall,
            FormBuilder form = null,
            bool loadFirstLevelReferences = false,
            bool enableAutoSortAndFilters = true,
            string editAPIOnSuccess = null) {
            _insertEntityList(
                variableName: variableName,
                entities: entities,
                selectedEntities: null,
                selectionAPICall: null,
                quantityEntities: null,
                quantityAPICall: null,
                allowMultipleSelection: false,
                enableAutoSortAndFilters: enableAutoSortAndFilters,
                form: form,
                link: null,
                loadFirstLevelReferences: loadFirstLevelReferences,
                total: null,
                editAPICall: editAPICall,
                editAPIOnSuccess: editAPIOnSuccess

            );
        }


        /// <summary>
        /// Inserts a QUANTITY list.
        /// You must provide the total set of entities (entities), and the quantity entities (quantityEntities) which describe how much of each.
        /// You must ensure that for each entity in quantityEntities the corresponding entity exists, otherwise it will not be listed.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="entities">The entities.</param>
        /// <param name="quantityEntities">The quantity entities.</param>
        /// <param name="form">The form.</param>
        /// <param name="link">The link.</param>
        /// <param name="quantityAPICall">The quantity API call.</param>
        /// <param name="loadFirstLevelReferences">if set to <c>true</c> [load first level references].</param>
        public void InsertQuantityList(
            string variableName, 
            IQueryable<Core.Model.ModelObject> entities, 
            IEnumerable<Core.Model.IQuanityModelObject> quantityEntities,
            FormBuilder form = null, 
            string link = null, 
            string quantityAPICall = null,
            bool loadFirstLevelReferences = false) {
            _insertEntityList(
                variableName: variableName,
                entities: entities,
                selectedEntities: null,
                selectionAPICall: null,
                quantityEntities: quantityEntities,
                quantityAPICall: quantityAPICall,
                allowMultipleSelection: false,
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences,
                total: null
            );
        }
        
        
        /// <summary>
        /// Insert a SINGLE entity selection listing using the specified form for a list of entities.
        /// The selectedEntity defines which entity is selected in the list.
        /// If a selectionAPICall is given, then the api call is automatically called when the selection changes.
        /// If a link is given, then each entity is automatically linked. Links may include variables such as
        /// '{entity.public-id}'.
        /// </summary>
        public void InsertSelectionList(string variableName, IQueryable<Core.Model.ModelObject> entities, Core.Model.ModelObject selectedEntity, FormBuilder form = null, string selectionAPICall = null, string link = null, bool loadFirstLevelReferences = false, bool enableAutoSortAndFilters = true) {
            var selectedEntities = new List<ModelObject>();
            if(selectedEntity != null) selectedEntities.Add(selectedEntity);
            _insertEntityList(
                variableName: variableName,
                entities: entities,
                selectedEntities: selectedEntities,
                selectionAPICall: selectionAPICall,
                quantityEntities: null,
                quantityAPICall: null,
                allowMultipleSelection: false,
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences
            );
        }
        
        /// <summary>
        /// Insert a MULTIPLE entity selection listing using the specified form for a list of entities.
        /// The selectedEntities defines which entities are selected in the list.
        /// If a selectionAPICall is given, then the api call is automatically called when the selection changes.
        /// If a link is given, then each entity is automatically linked. Links may include variables such as
        /// '{entity.public-id}'.
        /// </summary>
        public void InsertSelectionList(string variableName, IQueryable<Core.Model.ModelObject> entities, IEnumerable<Core.Model.ModelObject> selectedEntities, FormBuilder form = null, string selectionAPICall = null, string link = null, bool loadFirstLevelReferences = false, bool enableAutoSortAndFilters = true) {
            _insertEntityList(
                variableName: variableName,
                entities: entities,
                selectedEntities: selectedEntities,
                selectionAPICall: selectionAPICall,
                quantityEntities: null,
                quantityAPICall: null,
                allowMultipleSelection: true,
                form: form,
                link: link,
                loadFirstLevelReferences: loadFirstLevelReferences
            );
        }

        /// <summary>
        /// Inserts a entity list. This is the master method that does all the heavy lifting. 
        /// Depending on the paremters, the selction list
        /// can be highly customized for different uses.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="entities">The entities.</param>
        /// <param name="selectedEntities">The selected entities.</param>
        /// <param name="selectionAPICall">The selection API call.</param>
        /// <param name="allowMultipleSelection">if set to <c>true</c> [allow multiple selection].</param>
        /// <param name="form">The form.</param>
        /// <param name="link">The link.</param>
        /// <param name="loadFirstLevelReferences">if set to <c>true</c> [load first level references].</param>
        /// <param name="enableAutoSortAndFilters">if set to <c>true</c> [enable automatic sort and filters].</param>
        /// <param name="total">The total.</param>
        /// <exception cref="System.Exception">
        /// </exception>
        private void _insertEntityList(
                string variableName, 
                IQueryable<Core.Model.ModelObject> entities, 
                IEnumerable<Core.Model.ModelObject> selectedEntities, 
                string selectionAPICall, 
                IEnumerable<Core.Model.IQuanityModelObject> quantityEntities,
                string quantityAPICall, 
                bool allowMultipleSelection, 
                FormBuilder form, 
                string link = null, 
                bool loadFirstLevelReferences = false, 
                bool enableAutoSortAndFilters = false,
                FormBuilder total = null,
                string editAPICall = null,
                string editAPIOnSuccess = null
            ) {
            // Init everything
            if (form == null) form = new FormBuilder(Forms.Admin.LISTING);
            var formSystem = new FormBuilder(Forms.System.LISTING);
            PageTemplate listTemplate = null;

            if (editAPICall == null) {
                listTemplate =   this.LoadTemplate("entity-list");
            } else {
                listTemplate = this.LoadTemplate("editable-entity-list");
            }


            var entityTemplates = new List<PageTemplate>();
            var itemTemplateName = "entity-list.item";
            string icon = null;
            if (link != null) icon = "right";
            if (link != null && link.StartsWith("javascript:")) icon = "active-state";
            // Classes
            var tableClasses = new List<string>();
            if (selectedEntities != null) {
                if(allowMultipleSelection) itemTemplateName = "entity-list.item.selection.multiple";
                else itemTemplateName = "entity-list.item.selection.single";
            }
            if (quantityEntities != null) {
                itemTemplateName = "entity-list.item.quantity";
            }

            // Disable enableAutoSortAndFilters if there is no Handler (e.g. EmailTemplate)
            if (Handler == null) {
                enableAutoSortAndFilters = false;
            }
            
            // Loop entities
            int count = 0;
            foreach(var entity in entities) {
                // Init
                count++;
                var properties = entity.GetPropertiesForForm(form);
                var propertiesSystem = entity.GetPropertiesForForm(formSystem);
                var entityTemplate = this.LoadTemplate(itemTemplateName);
                var propertyTemplates = new List<PageTemplate>();
                if(loadFirstLevelReferences) entity.LoadFirstLevelObjectReferences();
                // First?
                if (count == 1) {
                    // Do header
                    var headerTemplate = this.LoadTemplate("entity-list.header");
                    var headerPropertyTemplates = new List<PageTemplate>();
                    // Selection header?
                    if (selectedEntities != null) {
                        var headerPropertyTemplate = this.LoadTemplate("entity-list.header.property");
                        headerPropertyTemplate.InsertVariable("entity.property.name", "{text.selected}");
                        headerPropertyTemplate.InsertVariable("entity.property.title", "{text.selected}");
                        headerPropertyTemplate.InsertVariable("entity.property.form-name", "selection-header-selection");
                        headerPropertyTemplates.Add(headerPropertyTemplate);
                    }
                    // Quantity header?
                    if (quantityEntities != null) {
                        var headerPropertyTemplate = this.LoadTemplate("entity-list.header.property");
                        headerPropertyTemplate.InsertVariable("entity.property.name", "{text.quantity}");
                        headerPropertyTemplate.InsertVariable("entity.property.title", "{text.quantity}");
                        headerPropertyTemplate.InsertVariable("entity.property.form-name", "quantity-header-quantity");
                        headerPropertyTemplates.Add(headerPropertyTemplate);
                    }
                    foreach(var prop in properties) {
                        // Figure out which template to load (if we are doing auto-filters and sorting, we
                        // use a more advanced template
                        PageTemplate headerPropertyTemplate;
                        var formProp = new EntityFormBuilderProperty(entity,prop,form);
                        if (enableAutoSortAndFilters && !prop.IsDerived()) {
                            // Auto sort and filter!
                            headerPropertyTemplate = this.LoadTemplate("entity-list.header.property-sortfilter");
                            string propertyQuery = "";
                            var currentSortBy = this.Handler.Params.String("sort");
                            var currentSortDir = this.Handler.Params.String("dir", "desc");
                            bool isActiveSortBy = currentSortBy == prop.Name.ToLower();

                            var pageQueryKeys = new List<string>();
                            if (Handler.Context != null) {
                                pageQueryKeys = this.Handler.Context.Request.QueryString.AllKeys.Where(k => k != "page").ToList();
                            }
                            if (!pageQueryKeys.Contains("sort")) pageQueryKeys.Add("sort");
                            if (!pageQueryKeys.Contains("dir")) pageQueryKeys.Add("dir");

                            foreach (var key in pageQueryKeys) {
                                if(propertyQuery != "") propertyQuery += "&";
                                var val = this.Handler.Params.String(key);
                                if (key == "sort") {
                                    val = prop.Name.ToLower();
                                }
                                if (key == "dir" && isActiveSortBy) {
                                    if (val == "asc")   val = "desc";
                                    else                val = "asc";
                                } else if (key == "dir") {
                                    val = currentSortDir;
                                }
                                propertyQuery += key + "=" + val;
                            }
                            headerPropertyTemplate.InsertVariable("entity.property.query",propertyQuery);
                            if(isActiveSortBy) headerPropertyTemplate.InsertVariable("entity.property.is-active","selected");
                            else headerPropertyTemplate.InsertVariable("entity.property.is-active","");
                            headerPropertyTemplate.InsertVariable("entity.property.sort-dir",currentSortDir);
                            headerPropertyTemplate.InsertVariable("entity.property.sort-dir-updown",currentSortDir=="desc"?"up":"down");
                        } else {
                            // Just plain old template...
                            headerPropertyTemplate = this.LoadTemplate("entity-list.header.property");
                            
                        }
                        // Do some common variables
                        headerPropertyTemplate.InsertVariable("entity.property.name",prop.Name);
                        headerPropertyTemplate.InsertVariable("entity.property.title",formProp.GetPropertyTitle(this.Language));
                        headerPropertyTemplate.InsertVariable("entity.property.tooltip",formProp.GetPropertyTooltip(this.Language));
                        headerPropertyTemplate.InsertVariable("entity.property.help",formProp.GetPropertyHelp(this.Language));
                        headerPropertyTemplate.InsertVariable("entity.property.form-name", prop.GetFormName(form));
                        headerPropertyTemplate.InsertVariable("entity.property.type",formProp.GetPropertyType().Name);
                       
                        headerPropertyTemplates.Add(headerPropertyTemplate);
                    }
                    headerTemplate.InsertTemplates("entity.properties",headerPropertyTemplates);
                    entityTemplates.Add(headerTemplate);
                }
                // Do values
                var rowLink = link;
                foreach(var prop in properties) {

                    if (editAPICall == null) {
                        // Get correct template
                        PageTemplate propertyTemplate = null;
                        if (link == null) {
                            propertyTemplate = this.LoadTemplate("entity-list.item.property");
                        } else {
                            propertyTemplate = this.LoadTemplate("entity-list.item.property-with-link");
                        }
                        // Get the value
                        var formProp = new EntityFormBuilderProperty(entity, prop, form);
                        string propVal = formProp.GetFormattedPropertyValue(this.Language);
                        // Instert variables
                        if (rowLink != null) rowLink = rowLink.Replace("{entity." + formProp.GetFormName() + "}", propVal);
                        propertyTemplate.InsertVariable("entity.property.name", formProp.GetPropertyName());
                        propertyTemplate.InsertVariable("entity.property.title", formProp.GetPropertyTitle(this.Language));
                        propertyTemplate.InsertVariable("entity.property.tooltip", formProp.GetPropertyTooltip(this.Language));
                        propertyTemplate.InsertVariable("entity.property.help", formProp.GetPropertyHelp(this.Language));
                        propertyTemplate.InsertVariable("entity.property.value", propVal);
                        propertyTemplate.InsertVariable("entity.property.raw-value", formProp.GetPropertyValue());
                        propertyTemplate.InsertVariable("entity.property.type", formProp.GetPropertyType().Name);
                        propertyTemplate.InsertVariable("entity.property.form-name", formProp.GetFormName());
                        propertyTemplates.Add(propertyTemplate);
                    }
                    else {

                        var formProp = new EntityFormBuilderProperty(entity, prop, form);
                        var propertyTemplate = this._createFormPropertyTemplate(entity, formProp, prop, "entity-list.item.editable-property");
                        propertyTemplates.Add(propertyTemplate);

                    }
                }
                // Selection?
                if (selectedEntities != null) {
                    bool isSelected = selectedEntities.Contains(entity);
                    entityTemplate.InsertVariable("entity.checked",isSelected?"checked":"");
                    entityTemplate.InsertVariable("entity.selection-api-call",selectionAPICall);
                }
                // Quantity?
                if(quantityEntities != null) {
                    string quantityVal = "";
                    var quantityEntity = quantityEntities.Where(qe => qe.PublicIdForQuantityEntity == entity.PublicId).SingleOrDefault();
                    if (quantityEntity != null) quantityVal = quantityEntity.Quantity.ToString();
                    entityTemplate.InsertVariable("entity.quantity-value", quantityVal);
                    entityTemplate.InsertVariable("entity.quantity-api-call",quantityAPICall);
                }
                // Classes
                var classes = "";
                if(entity.GetProperties().Any(p => p.Name == "StyleClass")) {
                    var val = entity.GetProperties().SingleOrDefault(p => p.Name == "StyleClass").GetValue(entity);
                    if (val != null) classes += val.ToString() + " ";
                }
                if (entity.GetType().InheritsOrImplements(typeof(IPublishedModelObject))) {
                    if (((IPublishedModelObject)entity).Published) classes += "published" + " ";
                    else classes += "unpublished" + " ";
                }
                if (entity.GetType().InheritsOrImplements(typeof(IEnabledModelObject))) {
                    if (((IEnabledModelObject)entity).Enabled) classes += "enabled" + " ";
                    else classes += "disabled" + " ";
                }
                if (link != null && link != "") classes += "link" + " ";
                // Insert generated variables
                entityTemplate.InsertVariable("entity.classes",classes);
                entityTemplate.InsertTemplates("entity.properties",propertyTemplates);
                entityTemplate.InsertVariable("entity.link",rowLink);
                entityTemplate.InsertVariable("entity.link-icon",icon);
                // Insert default system varialbes (such as public id, type, et cetear)
                foreach (var prop in propertiesSystem) {
                    var formProp = new EntityFormBuilderProperty(entity, prop, formSystem);
                    entityTemplate.InsertVariable("entity."+ formProp.GetPropertyName().ToDashedLower(), formProp.GetFormattedPropertyValue(this.Language));
                }
               entityTemplates.Add(entityTemplate);
            }
            // Total?
            if(total != null && count != 0) {
                // Create a dummy entity that contains the totals using the total form
                var entityTemplate = this.LoadTemplate(itemTemplateName);
                var propertyTemplates = new List<PageTemplate>();
                var entitySample = entities.FirstOrDefault();
                var allProps = entitySample.GetPropertiesForForm(form);
                var totalProps = entitySample.GetPropertiesForForm(total);
                foreach (var prop in allProps) {
                    var propertyTemplate = this.LoadTemplate("entity-list.item.property");
                    object propVal = null;
                    var formProp = (new EntityFormBuilderProperty(entitySample,prop,form)).AsCustom();
                    var customDescription = total.CustomProperties?.FirstOrDefault(p => p.Name == prop.Name);
                    if (customDescription != null && customDescription.FormValue != null) propVal = customDescription.FormValue.ToString();
                    // Is this a property we want a total for?
                    if (totalProps.Contains(prop)) {
                        // Get the total prop
                        try {
                            // Calculate depending on type of property
                            if (prop.PropertyType == typeof(int)) {
                                formProp.FormValue = entities.Sum(e => (int)prop.GetValue(e, null));
                            } else if (prop.PropertyType == typeof(int?)) {
                                formProp.FormValue = entities.Sum(e => (int?)prop.GetValue(e, null));
                            } else if (prop.PropertyType == typeof(Price)) {
                                formProp.FormValue = entities.Sum(e => (Price)(prop.GetValue(e, null)));
                            } else if (prop.PropertyType == typeof(decimal?)) {
                                formProp.FormValue = entities.Sum(e => (decimal?)(prop.GetValue(e, null)));
                            } else if (prop.PropertyType == typeof(decimal)) {
                                formProp.FormValue = entities.Sum(e => (decimal)(prop.GetValue(e, null)));
                            } else if (prop.PropertyType == typeof(double)) {
                                double sum = 0;
                                foreach(var entity in entities) {
                                    double val = (double)prop.GetValue(entity);
                                    sum += val;
                                }
                                formProp.FormValue = sum;
                                // formProp.FormValue = entities.Sum(e => (Double)(prop.GetValue(e)));
                            } else if (prop.PropertyType == typeof(TimeSpan)) {
                                formProp.FormValue = new TimeSpan(entities.Sum(e => ((TimeSpan)(prop.GetValue(e, null))).Ticks));
                            } else {
                                throw new Exception($"The totals property '{prop.Name}' of type '{prop.PropertyType.Name}' is not supported by _insertEntityList. Please implement the linq query to generate this sum.");
                            }
                        }catch(Exception e) {
                            throw new Exception($"Could not calculate the total of '{prop.Name}' of type '{prop.PropertyType.Name}': {e.Message}",e);
                        }
                        propVal = formProp.GetFormattedPropertyValue(this.Language);
                    }
                    // Instert variables
                    propertyTemplate.InsertVariable("entity.property.name",formProp.GetPropertyName());
                    propertyTemplate.InsertVariable("entity.property.title",formProp.GetPropertyTitle(this.Language));
                    propertyTemplate.InsertVariable("entity.property.tooltip",formProp.GetPropertyTooltip(this.Language));
                    propertyTemplate.InsertVariable("entity.property.help",formProp.GetPropertyHelp(this.Language));
                    propertyTemplate.InsertVariable("entity.property.value",propVal);
                    propertyTemplate.InsertVariable("entity.property.type",formProp.GetPropertyType().Name);
                    propertyTemplate.InsertVariable("entity.property.form-name", formProp.GetFormName());
                    propertyTemplates.Add(propertyTemplate);
                }
                // Add to template stack
                entityTemplate.InsertTemplates("entity.properties",propertyTemplates);
                entityTemplate.InsertVariable("entity.classes","total");
                entityTemplates.Add(entityTemplate);
            }




            // Editable Table
            if (editAPICall != null) {
                tableClasses.Add("bb-editable-table");
            }
            listTemplate.InsertVariable("api-call", editAPICall);
            listTemplate.InsertVariable("on-success", editAPIOnSuccess);



            // Classes
            listTemplate.InsertVariable("classes",  tableClasses.Any() ? string.Join(" ", tableClasses) : string.Empty);




            // Post process
            //foreach(var t in entityTemplates) {
            //    var c = "";
            //    if (t == entityTemplates.Last()) c += " last";
            //    t.InsertVariable("entity.classes", c);
            //}
            // Special handling for empty lists...
            if (count == 0) entityTemplates.Add(this.LoadTemplate("entity-list.empty"));
            // Register all the entities (or empty)
            listTemplate.InsertTemplates("entities",entityTemplates);
            // Do we have buttons from the form?
            //TODO
            this.InsertVariable(variableName+".count",count);
            this.InsertTemplate(variableName,listTemplate);
        }

        /*
        public void InsertProperties(string variableName, Properties props) {
            // Properties lising
            var val = prop.GetValue(entity) as Properties;
            val.LoadDefaultsForProperty(prop);
            foreach(var key in props.Keys) {
                if (showLabels) {
                    propertyTemplate = this.LoadTemplate("property-list.item");
                } else {
                    propertyTemplate = this.LoadTemplate("property-list.item-nolabel");
                }
                propertyTemplate.InsertVariable("property.name",key);
                propertyTemplate.InsertVariable("property.title",key.ToSentence());
                propertyTemplate.InsertVariable("property.value",val[key]);
                propertyTemplates.Add(propertyTemplate);
            }
        }*/

        /// <summary>
        /// Inserts a property list for an entity using the given form builder.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="form">The form.</param>
        /// <param name="loadFirstLevelReferences">if set to <c>true</c> [load first level references].</param>
        /// <param name="includeNavigationProperties">if set to <c>true</c> [include navigation properties].</param>
        /// <param name="showLabels">if set to <c>true</c> [show labels].</param>
        /// <param name="showCard">if set to <c>true</c> [show card]. Note: this will only show a card if it is not a default ModelObject card.</param>
        public void InsertPropertyList(string variableName, Core.Model.ModelObject entity, FormBuilder form = null, bool loadFirstLevelReferences = false, bool includeNavigationProperties = false, bool showLabels = true, bool showCard = true) {
            // Init
            if (form == null) form = new FormBuilder(Forms.Admin.VIEW);
            var listTypes = new string[]{ "System.Collections.Generic.ICollection" };
            var listTemplate = this.LoadTemplate("property-list");
            var properties = entity.GetPropertiesForForm(form);
            if (!includeNavigationProperties) {
                properties = properties.Where(
                    p => listTypes.Where(
                        lt => p.PropertyType.ToString().StartsWith(lt)).Count() == 0
                    ).ToList();
            }
            var propertyTemplates = new List<PageTemplate>();
            if(loadFirstLevelReferences) entity.LoadFirstLevelObjectReferences();
            if(loadFirstLevelReferences && includeNavigationProperties) entity.LoadFirstLevelNavigationReferences();

            // Do values
            foreach (var prop in properties) {
                // Init
                var propertyTemplatesFromProp = _createPropertyListPropertyTemplate(entity, new EntityFormBuilderProperty (entity,prop, form),form, showLabels, listTypes);
                propertyTemplates.AddRange(propertyTemplatesFromProp);
            }

            // Do custom
            if (form.CustomProperties != null) {
                foreach (var prop in form.CustomProperties) {
                    // Create template
                    var propertyTemplatesFromProp = _createPropertyListPropertyTemplate(entity, prop, form, showLabels,listTypes);
                    propertyTemplates.AddRange(propertyTemplatesFromProp);
                }
            }

            listTemplate.InsertTemplates("properties",propertyTemplates);
                
            // Card?
            if(showCard) {
                var card = entity.VisualCard();
                if (!card.IsDefaultCard) listTemplate.InsertCard("property-list.card", card);
                else listTemplate.InsertVariable("property-list.card","");
            } else {
                listTemplate.InsertVariable("property-list.card","");
            }

            // Final insert
            this.InsertTemplate(variableName,listTemplate);
        }

        private IEnumerable<PageTemplate> _createPropertyListPropertyTemplate (ModelObject entity, FormBuilderProperty formProp, FormBuilder form, bool showLabels, string [] listTypes) {

            PageTemplate propertyTemplate = null;
            List<PageTemplate> propertyTemplates = new List<PageTemplate>();
            //var formProp = new EntityFormBuilderProperty(entity, prop, form);

            if (showLabels) {
                propertyTemplate = this.LoadTemplate("property-list.item");
            } else {
                propertyTemplate = this.LoadTemplate("property-list.item-nolabel");
            }
            var valDisplay = "";

            if (formProp.GetPropertyType() == typeof(ContentNode)) {

                // Content node
                var val = formProp.GetPropertyValue();
                propertyTemplate.InsertVariable("property.name", formProp.GetFormName());
                propertyTemplate.InsertVariable("property.title", formProp.GetPropertyTitle(this.Language));
                propertyTemplate.InsertVariable("property.tooltip", formProp.GetPropertyTooltip(this.Language));
                propertyTemplate.InsertVariable("property.help", formProp.GetPropertyHelp(this.Language));
                if (val != null) {
                    var node = (ContentNode)val;
                    propertyTemplate.InsertContent("property.value", node);
                } else {
                    propertyTemplate.InsertVariable("property.value", "");
                }
               

            } else if (formProp.GetFormType() == "imageurl") {

                if (showLabels) {
                    propertyTemplate = this.LoadTemplate("property-list.item.image");
                } else {
                    propertyTemplate = this.LoadTemplate("property-list.item-nolabel.image");
                }
                valDisplay = formProp.GetFormattedPropertyValue(this.Language);

            } else if (formProp.GetPropertyType() == typeof(Properties)) {

                // Properties lising
                var val = formProp.GetPropertyValue() as Properties;

                // Todo: move LoadDefaultsForProperty to generic 
                EntityFormBuilderProperty entityFormProperty = formProp as EntityFormBuilderProperty;
                if (entityFormProperty != null && val != null) {
                    val.LoadDefaultsForProperty(entityFormProperty.Property);
                    foreach (var key in val.Keys) {
                        // Exclude?
                        if(form.PropertiesToExclude != null && form.PropertiesToExclude.Contains(formProp.GetPropertyName()+"."+key)) {
                            // Form has this key marked as exclude...
                            continue;
                        }
                        if (showLabels) {
                            propertyTemplate = this.LoadTemplate("property-list.item");
                        } else {
                            propertyTemplate = this.LoadTemplate("property-list.item-nolabel");
                        }
                        propertyTemplate.InsertVariable("property.name", key);
                        propertyTemplate.InsertVariable("property.title", key.ToSentence());
                        propertyTemplate.InsertVariable("property.value", val[key]);
                        propertyTemplate.InsertVariable("property.tooltip", formProp.GetPropertiesPropertyTooltip(this.Language,key));
                        propertyTemplates.Add(propertyTemplate);
                    }
                    return propertyTemplates;
                }


            } else if (listTypes.Where(lt => formProp.GetPropertyType().ToString().StartsWith(lt)).Any()) {

                // List type?
                var val = formProp.GetPropertyValue();
                if (val != null) {
                    System.Collections.ICollection valCollection = val as System.Collections.ICollection;
                    if (valCollection != null) {
                        valDisplay = valCollection.Count.ToString();
                    }
                }

            } else {

                // Simple type...
                valDisplay = formProp.GetFormattedPropertyValue(this.Language);

            }
            propertyTemplate.InsertVariable("property.name", formProp.GetFormName());
            propertyTemplate.InsertVariable("property.title", formProp.GetPropertyTitle(this.Language));
            propertyTemplate.InsertVariable("property.tooltip", formProp.GetPropertyTooltip(this.Language));
            propertyTemplate.InsertVariable("property.help", formProp.GetPropertyHelp(this.Language));
            propertyTemplate.InsertVariable("property.type", formProp.GetPropertyType().Name);
            propertyTemplate.InsertVariable("property.value", valDisplay);

            propertyTemplates.Add(propertyTemplate);

            return propertyTemplates;
        }
        
    }
}
