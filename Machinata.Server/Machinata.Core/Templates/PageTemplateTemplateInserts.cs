using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Template Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        private bool _allowUndefinedTemplate() {
            var handler = this.Handler as Handler.PageTemplateHandler;
            if (handler != null) return handler.AllowUndefinedTemplates;
            return false;
        }

        /// <summary>
        /// Loads a template by name within the current template context.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <returns></returns>
        public PageTemplate LoadTemplate(string templateName) {
            return PageTemplate.LoadForTemplateContextAndName(this, templateName, _allowUndefinedTemplate());
        }

        public void ChangeTemplate(string templateName) {
            var newTemplate = PageTemplate.LoadForTemplateContextAndName(this, templateName, _allowUndefinedTemplate());
            this._data.Clear();
            this._data.Append(newTemplate.Data);
            this._variables.Clear();
            this._variables.AddRange(newTemplate._variables);
        }

        public void InsertTemplate(string variableName, PageTemplate template) {
            // Register any new variables
            foreach(var variable in template._variables) {
                if (!_variables.Contains(variable)) _variables.Add(variable);
            }
            InsertVariableUnsafe(variableName, template.Data);
        }

        public void InsertTemplate(string variableName, string templateName) {
            var template = PageTemplate.LoadForTemplateContextAndName(this, templateName, _allowUndefinedTemplate());
            InsertTemplate(variableName, template);
        }

        
        /// <summary>
        /// Automatically inserts a stack of templates.
        /// </summary>
        public void InsertTemplates(string variableName, IEnumerable<PageTemplate> templates, string joinSeparator = null) {
            StringBuilder stack = new StringBuilder();
            foreach(var template in templates) {
                // Append to the stack
                stack.Append(template.Data);
                // Join/Concat
                if (joinSeparator != null && templates.Last() != template) {
                    stack.Append(joinSeparator);
                }

                // Register any new variables
                foreach(var variable in template._variables) {
                    if (!_variables.Contains(variable)) _variables.Add(variable);
                }
            }
            InsertVariableUnsafe(variableName, stack);
            InsertVariable(variableName+".count", templates.Count());
        }

        /// <summary>
        /// Automatically inserts all the template variables (ie {templatE.xyz}) ('templatE.xyz' should be 'template.xyz' but this tries to insert a template in the documentation of this very method) that are in this template...
        /// </summary>
        public void InsertTemplateVariables() {
            _logger.Trace("Inserting template variables...");
            foreach(string var in _variables.ToList()) {
                if(var != null && var.StartsWith("template.")) {
                    _logger.Trace("  Found "+var);
                    this.InsertTemplate(var, var.Replace("template.",""));
                }
            }
        }
        
    }
}
