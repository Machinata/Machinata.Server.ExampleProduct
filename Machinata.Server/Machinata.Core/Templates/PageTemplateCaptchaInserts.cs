using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {


        /// <summary>
        /// Inserts a randomly genrated captcha with the default template 'catpcha' with the following variables:
        /// captcha.encoded: the encoded catpcha
        /// captcha.url: the direct URL for the captcha image
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public string InsertCaptcha(string variableName, string code = null) {
            if(code == null) {
                code = Core.Ids.Captcha.Default.RandomCode();
            }
            var encoded = Core.Ids.Captcha.Default.EncodeCode(code);
            var template = this.LoadTemplate("captcha");
            template.InsertVariable("captcha.encoded", encoded);
            template.InsertVariable("captcha.url", "/static/captcha?code="+Uri.EscapeDataString(encoded));
            this.InsertTemplate(variableName, template);
            return code;
        }
        
        
    }
}
