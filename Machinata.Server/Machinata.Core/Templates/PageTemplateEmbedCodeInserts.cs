using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using System.Reflection;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public void InsertEmbedCode(string variableName, string url) {
            if(string.IsNullOrEmpty(url)) {
                this.InsertVariable(variableName, "");
            } else {
                this.InsertVariableUnsafe(variableName, Core.Util.EmbedCodes.GetEmbedCodeForURL(url));
            }
        }
        
        
    }
}
