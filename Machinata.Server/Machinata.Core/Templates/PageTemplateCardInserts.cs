using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {
        

        public void InsertCard(string variableName, Core.Model.ModelObject entity) {
            var card = entity.VisualCard();
            card.InsertIntoTemplate(variableName, this);
        }

        public void InsertCard(string variableName, Core.Cards.CardBuilder card) {
            card.InsertIntoTemplate(variableName, this);
        }

        public void InsertCards(string variableName, IQueryable<Core.Model.ModelObject> entities, string link = null) {
            var cards = entities.ToList().Select(e => e.VisualCard()).ToList();
            if(link != null) {
                cards.ForEach(c => c.Link(link));
            }
            InsertCards(variableName, cards);
        }

        public void InsertCards(string variableName, List<Core.Cards.CardBuilder> cards) {
            if(cards == null || cards.Count == 0) {
                this.InsertTemplate(variableName, "cards.empty");
                this.InsertVariable(variableName+".count", 0);
                return;
            } 
            // Create dynamic template to insert them all
            string cardVars = "";
            foreach(var card in cards) {
                cardVars += "{" + card.CardID + "}";
            }
            this.InsertVariable(variableName, cardVars);
            this.InsertVariable(variableName+".count", cards.Count);
            foreach(var card in cards) {
                this.InsertCard(card.CardID, card);
            }
        }
        
        
    }
}
