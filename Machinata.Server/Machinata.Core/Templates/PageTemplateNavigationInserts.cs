using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using System.Reflection;
using Machinata.Core.Model;
using Machinata.Core.Builder;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Navigation Methods
    /// </summary>
    public static class PageTemplateNavigationInserts {


        public static void InsertNavigation(this PageTemplate template, NavigationBuilder navigation, string variableName, string templateName) {
            if (navigation.Items == null || navigation.Items.Count == 0) {
                template.InsertVariable(variableName, "");
                template.InsertVariable(variableName + ".items", "");
            } else {
                navigation.CompilePaths(template);
                var navItems = new List<PageTemplate>();
                var level = 0;
                foreach (var item in navigation.Items) {
                    // Make sure we don't excede the max display depth
                    if (navigation.MaxDisplayDepth != null && level >= navigation.MaxDisplayDepth) break;

                    var itemTemplate = template.LoadTemplate(templateName + ".item");
                    // Siblings?
                    if (item.Siblings.Count > 0) {
                        itemTemplate.InsertVariable("item.has-siblings", "has-siblings");
                        var siblingTemplates = new List<PageTemplate>();
                        foreach (var sibling in item.Siblings) {
                            var siblingTemplate = template.LoadTemplate(templateName + ".item.sibling");
                            if (sibling.Classes == null) sibling.Classes = "";
                            if (sibling.Path == item.Path) sibling.Classes += " sibling-is-item";
                            siblingTemplate.InsertVariable("sibling.level", level);
                            siblingTemplate.InsertVariable("sibling.path", sibling.Path);
                            siblingTemplate.InsertVariable("sibling.link", sibling.Link);
                            siblingTemplate.InsertVariable("sibling.title", sibling.Title);
                            siblingTemplate.InsertVariable("sibling.classes", sibling.Classes);
                            siblingTemplates.Add(siblingTemplate);
                        }
                        itemTemplate.InsertTemplates("item.siblings", siblingTemplates);
                    } else {
                        itemTemplate.InsertVariable("item.has-siblings", "no-siblings");
                        itemTemplate.InsertVariable("item.siblings", "");
                    }
                    // General item variables
                    itemTemplate.InsertVariable("item.level", level);
                    itemTemplate.InsertVariable("item.path", item.Path);
                    itemTemplate.InsertVariable("item.link", item.Link);
                    itemTemplate.InsertVariable("item.title", item.Title);
                    itemTemplate.InsertVariable("item.classes", item.Classes);
                    navItems.Add(itemTemplate);
                    level++;
                }
                var navTemplate = template.LoadTemplate(templateName);
                navTemplate.InsertTemplates(variableName+".items", navItems);

                // Global inserts
                template.InsertTemplate(variableName, navTemplate);
                template.InsertTemplates(variableName+".items", navItems);
                template.InsertVariable(variableName+".page-title", navigation.PageTitle);
                template.InsertVariable(variableName+".title-path", navigation.TitlePath);
                template.InsertVariable(variableName+".current-path", navigation.Path);
                template.InsertVariable(variableName + ".prev-path", navigation.PreviousPath);
                template.InsertVariable(variableName + ".prev-title", navigation.PreviousTitle);
                template.InsertVariable(variableName+".prev-prev-path", navigation.PreviousPreviousPath);
                template.InsertVariable(variableName+".current-url", Core.Config.PublicURL + navigation.Path);
                template.DiscoverVariables();  // Force discovery of any text variables we inserted

            }
        }

    }
}
