using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        #region InsertVariable Unsafe Raw
        public void InsertVariableUnsafe(string name, string value) {
            _data.Replace("{" + name + "}", value);
        }

        public void InsertVariableUnsafe(string name, int value) {
            InsertVariableUnsafe(name, value.ToString());
        }

        public void InsertVariableUnsafe(string name, bool value) {
            InsertVariableUnsafe(name, value.ToString().ToLowerInvariant());
        }

        public void InsertVariableUnsafe(string name, StringBuilder value) {
            InsertVariableUnsafe(name, value.ToString());
        }
        #endregion
        
        #region InsertVariable Web-Safe Shortcuts
        
        public void InsertVariable(string name, string value) {
            InsertVariableXMLSafe(name, value);
        }

        public void InsertVariableEmpty(string name) {
            InsertVariable(name, string.Empty);
        }
        public void InsertVariable(string name, bool value) {
            InsertVariableXMLSafe(name, value);
        }
        public void InsertVariable(string name, int value) {
            InsertVariableXMLSafe(name, value);
        }
        public void InsertVariable(string name, int? value) {
            if(value.HasValue) InsertVariableXMLSafe(name, value.Value);
            else InsertVariableXMLSafe(name, null);
        }
        public void InsertVariable(string name, object value) {
            if(value == null) InsertVariableXMLSafe(name, null);
            else InsertVariableXMLSafe(name, value.ToString());
        }

        #endregion

        #region InsertVariable Web-Safe
        
        public void InsertVariableJSSafe(string name, string value) {
            value = value.Replace("'", "\\'");
            value = value.Replace("\"", "\\\"");
            InsertVariableUnsafe(name, value);
        }

        public void InsertVariableCSVSafe(string name, string value) {
            value = value.Replace("\"", "\\\"");
            value = value.Replace("\n", "\\n");
            InsertVariableUnsafe(name, value);
        }

        public void InsertVariableJSXMLSafe(string name, string value) {
            if (value == null) value = "";
            value = value.Replace("'", "\\'");
            InsertVariableXMLSafe(name, value);
        }

        public void InsertVariableJSONXMLSafe(string name, string value) {
            if (value == null) value = "";
            InsertVariableXMLSafe(name, value);
        }

        public void InsertVariableURISafe(string name, string value) {
            value = HttpUtility.UrlEncode(value);
            InsertVariableXMLSafe(name, value);
        }

        public void InsertVariableXMLSafeWithLineBreaks(string name, string value) {
            if (value == null) {
                InsertVariableUnsafe(name, "");
            } else {
                value = value.Replace("\n", "{line-break}");
                value = value.Replace("&", "&amp;");
                value = value.Replace("<", "&lt;");
                value = value.Replace(">", "&gt;");
                value = value.Replace("\"", "&quot;");
                value = value.Replace("{line-break}","<br/>");
                InsertVariableUnsafe(name, value);
            }
        }

        public void InsertVariableXMLSafe(string name, string value) {
            bool ESCAPE_VARIABLE_CHARS = false; //TODO: @dan @micha

            if (value == null) {
                InsertVariableUnsafe(name, "");
            } else {
                var xmlEscaped = Core.Util.XML.XMLEncodeString(value);
                if (ESCAPE_VARIABLE_CHARS) xmlEscaped = xmlEscaped.Replace("{", "[").Replace("}", "]");
                InsertVariableUnsafe(name, xmlEscaped);
            }
        }

        public void InsertVariableXMLSafe(string name, int number) {
            InsertVariableXMLSafe(name, number.ToString());
        }

        public void InsertVariableXMLSafe(string name, bool boolean) {
            InsertVariableXMLSafe(name, boolean.ToString().ToLower());
        }
        #endregion

        #region InsertVariable XML
        public void InsertVariableXMLDecoded(string name, string value) {
            if (value == null) {
                InsertVariableUnsafe(name, "");
            } else {
                InsertVariableUnsafe(name, Core.Util.XML.XMLDecodeString(value));
            }
        }
        #endregion
        
        #region InsertVariable JSON
        public void InsertJSON(string name, object value) {
           if (value == null) {
                InsertVariableUnsafe(name, string.Empty);
           }else {
                InsertVariableUnsafe(name, Core.JSON.Serialize(value));
           }
        }
        #endregion
        

    }
}
