using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Machinata.Core.Model.MailingUnsubscription;

namespace Machinata.Core.Templates {

    public class EmailTemplate : PageTemplate {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Constants



        #endregion

        #region Protected Fields
        protected ModelContext _db;

        #endregion

        #region Public Properties

        public string Subject { get; set; }

        // Unsubscribe data
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Business { get; set; }

        // Email Data
        public string User { get; set; }
        public string SecretCode { get; set; }
        public string Email { get; set; }
        public string MailingTitle { get; set; }




        #endregion

        #region Constructors

        /// <summary>
        /// Creates an EmailTemplate with PackageName: Machinata.Email WARNING THIS IS LOADED FROM THE DISK SINCE IT SKIPS THE CACHING ALOG
        /// Language will be set to default language if none is passed
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <param name="language">The language.</param>
        public EmailTemplate(ModelContext db, string templateName, string extension = ".htm", string language = null) 
            : base(Core.Config.EmailPackage, templateName, extension, Core.Config.EmailFallbackPackage) {
            var supportedLanguages = Core.Config.LocalizationSupportedLanguages.ToList();
            // allow admin language
            supportedLanguages.Add(Core.Config.LocalizationAdminLanguage);
            if (string.IsNullOrEmpty(language) || !supportedLanguages.Contains(language)) {
                this.Language = Core.Config.LocalizationDefaultLanguage;
            } else {
                this.Language = language;
            }

            _db = db;
            this.ThemeName = Core.Config.ThemeDefaultName;
            // Text variables
            this.InsertTextVariables();
        }


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public void SendBusinessEmail(Business business, string email, Categories unsubsribeType, string unsubscribeSubtype, IEnumerable<EmailAttachment> attachements = null, MailAddress replyTo = null, string mailingTitle = null, bool async = true) {
            this.Category = unsubsribeType.ToString().ToLower();
            this.SubCategory = unsubscribeSubtype;
            this.Business = business.PublicId;
            this.SecretCode = UnsubscribeService.GetCode(business);
            this.MailingTitle = mailingTitle;
            if (UnsubscribeService.GetUnsubscription(this._db, business, unsubsribeType, unsubscribeSubtype) == null) {
                Send(email, attachements, replyTo, async);
            }
        }


        public void SendUserEmail(User user, string email, Categories unsubsribeType, string unsubscribeSubtype, IEnumerable<EmailAttachment> attachements = null, MailAddress replyTo = null, string mailingTitle = null, bool async = true, string bcc = null) {

            // User variables
            this.InsertVariable("user.name", user.Name);
            this.InsertVariable("user.username", user.Username);

            this.Category = unsubsribeType.ToString().ToLower();
            this.SubCategory = unsubscribeSubtype;
            this.User = user.Username.ToLower();
            this.SecretCode = user.GetSecretCode();
            this.MailingTitle = mailingTitle;
            if (UnsubscribeService.GetUnsubscription(this._db, user, unsubsribeType, unsubscribeSubtype) == null) {
                Send(
                    email: email,
                    attachements: attachements,
                    replyTo: replyTo,
                    async: async,
                    bcc: bcc);
            }
        }

        /// <summary>
        /// Sends the email. Only use this if EmailContact is not associated with a User or Business!
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <param name="unsubsribeType">Type of the unsubsribe.</param>
        /// <param name="unsubscribeSubtype">The unsubscribe subtype.</param>
        /// <param name="attachements">The attachements.</param>
        public void SendEmail(string email, Categories unsubsribeType, string unsubscribeSubtype, IEnumerable<EmailAttachment> attachements = null, MailAddress replyTo = null, string mailingTitle = null, bool async = true, string bcc = null) {
            this.Category = unsubsribeType.ToString().ToLower();
            this.SubCategory = unsubscribeSubtype;
            this.SecretCode = UnsubscribeService.GetCode(email);
            this.MailingTitle = mailingTitle;
            this.Email = email;
            if (UnsubscribeService.IsUnsubscribed(this._db, email, unsubsribeType, unsubscribeSubtype) == false) {
                Send(
                    email: email,
                    attachements: attachements,
                    replyTo: replyTo,
                    async: async,
                    bcc: bcc);
            }
        }

        public void SendEmail(EmailContact contact, Categories unsubsribeType, string unsubscribeSubtype, IEnumerable<EmailAttachment> attachements = null, MailAddress replyTo = null, string mailingTitle = null, bool async = true, bool sendCopyToAdmin = false) {
            string email = contact.Address;
            this.Category = unsubsribeType.ToString().ToLower();
            this.SubCategory = unsubscribeSubtype;
            this.SecretCode = UnsubscribeService.GetCode(email);
            this.MailingTitle = mailingTitle;
            this.Email = email;

            if (UnsubscribeService.IsUnsubscribed(this._db, email, unsubsribeType, unsubscribeSubtype) == false) {
                Send(
                    to: new List<EmailContact>() { contact },
                    cc: new List<EmailContact>(),
                    bcc: new List<EmailContact>(),
                    attachements: attachements,
                    replyTo: replyTo,
                    async: async,
                    sendCopyToAdmin: sendCopyToAdmin
                   );
            }
        }


        public void Send(IEnumerable<EmailContact> to, IEnumerable<EmailContact> cc, IEnumerable<EmailContact> bcc, Categories unsubsribeType, string unsubscribeSubtype, IEnumerable<EmailAttachment> attachements, bool ignoreUnsubscribe, MailAddress replyTo = null, bool async = false) {

            var email = to.First().Address;

            this.Category = unsubsribeType.ToString().ToLower();
            this.SubCategory = unsubscribeSubtype;
            this.SecretCode = UnsubscribeService.GetCode(email);
           // this.MailingTitle = mailingTitle;
            this.Email = email;

            // TODO CHECK ALL EMAILS FOR UNSUBSRIBED
            if (ignoreUnsubscribe || UnsubscribeService.IsUnsubscribed(this._db, to.First().Address, unsubsribeType, unsubscribeSubtype) == false) {
                this.Send(to, cc, bcc, attachements, replyTo, async, sendCopyToAdmin: false);

            }
        }


        public void SendWithoutUnsubscribeCheck(IEnumerable<EmailContact> to, IEnumerable<EmailContact> cc, IEnumerable<EmailContact> bcc, IEnumerable<EmailAttachment> attachements, MailAddress replyTo = null, bool async = false) {
            this.Send(to, cc, bcc, attachements, replyTo, async, sendCopyToAdmin: false);
        }

        private void Send(string email, IEnumerable<EmailAttachment> attachements, MailAddress replyTo = null, bool async = true, string bcc = null, bool sendCopyToAdmin = false) {
            Compile();

            var toList = MessageCenter.ParseContactsList(email);
            var bccList = new List<EmailContact>();
            var ccList = new List<EmailContact>();

            if (bcc != null) {
                bccList = MessageCenter.ParseContactsList(bcc);
            }

            // Extend bcc with our copy
            if (sendCopyToAdmin) {
                bccList.AddRange(MessageCenter.ParseContactsList(Core.Config.NotificationEmails));
            }

            // Send main mail
            MessageCenter.SendMessageToEmail(toList, ccList, bccList, this.Subject, this.Data.ToString(), "Machinata.Core", attachements, replyTo, async);

            // Send special admin copy (has different subject)
            if (sendCopyToAdmin) {
                MessageCenter.SendMessageToEmail(MessageCenter.ParseContactsList(Core.Config.NotificationEmails), new List<EmailContact>(), new List<EmailContact>(), this.Subject + " (Copy, original to " + string.Join(",", toList) + ")", this.Data.ToString(), "Machinata.Core", attachements, replyTo, async);
            }
        }



        private void Send(IEnumerable<EmailContact> to, IEnumerable<EmailContact> cc, IEnumerable<EmailContact> bcc, IEnumerable<EmailAttachment> attachements, MailAddress replyTo = null, bool async = true, bool sendCopyToAdmin = false) {

            Compile();

            // Extend bcc with our copy
            if (sendCopyToAdmin) {
                var copyList = MessageCenter.ParseContactsList(Core.Config.NotificationEmails);
                if (bcc == null) {
                    bcc = copyList;
                } else {
                    copyList.AddRange(bcc);
                    bcc = copyList;
                }
            }

            // Send main email
            MessageCenter.SendMessageToEmail(to, cc, bcc, this.Subject, this.Content.ToString(), this.Package, attachements, replyTo, async);

            // Deprecated: we use bcc instead
            // Send special admin copy (has different subject)
            //if (sendCopyToAdmin) {
            //    MessageCenter.SendMessageToEmail(MessageCenter.ParseContactsList(Core.Config.NotificationEmails), new List<EmailContact>(), new List<EmailContact>(), this.Subject + " (Copy, original to "+string.Join(",",to.Select(e=>e.Address))+")", this.Data.ToString(), "Machinata.Core", attachements, replyTo, async);
            //}
        }

        /// <summary>
        /// Prepares the Template for sending, only call this explicitly if no email should be sent,
        /// e.g. debugging purposes
        /// </summary>
        public virtual void Compile() {
            InsertCommonVariables();
            _data = new StringBuilder(CSS.InlineCSSForHTMLDocument(Content, LoadCSS()));
        }



        /// <summary>
        /// Inserts the common variables and sends the template as an email
        /// </summary>
        /// <param name="attachements">The attachements.</param>
        public void SendAdminEmail(IEnumerable<EmailAttachment> attachements = null) {
            try {
                Compile();
            } catch (Exception e) {
                _logger.Error(e, "Could not compile admin email.");
            }
            MessageCenter.SendMessageToAdminEmail(Subject, this.Data.ToString(), Package, attachements, true);
        }


        /// <summary>
        /// Inserts the common variables and sends the template as an email
        /// </summary>
        /// <param name="attachements">The attachements.</param>
        public void SendNotificationEmail(IEnumerable<EmailAttachment> attachements = null) {
            try {
                Compile();
            } catch (Exception e) {
                _logger.Error(e, "Could not compile admin email.");
            }
            MessageCenter.SendMessageToNotificationEmail(Subject, this.Data.ToString(), Package, attachements, true);
        }

        public void InsertEntity(string variableName, string title, ModelObject entity, FormBuilder form = null) {
            if (entity != null) {
                if (form == null) { form = new FormBuilder(Forms.Frontend.VIEW); }
                var template = this.LoadTemplate("order.entity");
                template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: form,
                    showLabels: false,
                    showCard: false);
                template.InsertVariable("title", title);
                this.InsertTemplate(variableName, template);

            } else {
                this.InsertVariable(variableName, string.Empty);
            }
        }

        public void InsertList(string variableName, string title, IQueryable<ModelObject> entities, FormBuilder form = null, FormBuilder totalForm = null, string link = null) {

            if (form == null) {
                form = new FormBuilder(Forms.Admin.LISTING);
            }

            if (entities.Any()) {
                var template = this.LoadTemplate("section.entity-list");
                template.InsertVariable("title", title);
                template.InsertEntityList(
                    variableName: "entities",
                    entities: entities,
                    form: form,
                    loadFirstLevelReferences: false,
                    total: totalForm,
                    link: link);

                this.InsertTemplate(variableName, template);
            } else {
                this.InsertVariable(variableName, string.Empty);
            }

        }

        public void InsertAddress(string variableName, string title, ModelObject entity) {
            if (entity != null) {
                var template = this.LoadTemplate("section.entity");
                template.InsertVariableXMLSafeWithLineBreaks("entity", entity.ToString().Replace(", ", "\n"));
                template.InsertVariable("title", title);
                this.InsertTemplate(variableName, template);

            } else {
                this.InsertVariable(variableName, string.Empty);
            }
        }

        /// <summary>
        /// Inserts the CMS content and subject.
        /// Subjects is the title node from the given cmsPath
        /// Content is the 
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="cmsPath">The CMS path.</param>
        /// <param name="contentPath">Relative path of the content.</param>
        public void InsertCmsContentAndSubject(ModelContext db, string cmsPath, string contentPath = "/Content", string language = "default") {
            // CMS Content
            this.InsertContent(
                variableName: "cms-content",
                cmsPath: cmsPath + contentPath,
                throw404IfNotExists: false,
                db: db
            );

            // TODO language title
            // Subject
            var subject = ContentNode.GetByPath(db, cmsPath)?.Title;
            if (subject == null) {
                throw new Exception("No Subject found in CMS");
            }

            this.Subject = subject;
        }

        /// <summary>
        /// Inserts the variables into subject placeholders from the handler context variables
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="variables">The variables.</param>
        /// <exception cref="System.Exception">Subject is empty.</exception>
        public void InsertVariablesIntoSubject(Core.Handler.Handler handler, IEnumerable<string> variables = null) {

            // Check subject
            if (string.IsNullOrEmpty(this.Subject)) {
                throw new Exception("Subject is empty.");
            }

            // Subject Vars
            foreach (var variable in variables) {
                this.Subject = this.Subject.Replace("{" + variable + "}", handler.Params.String(variable));
            }

        }


        #endregion

        #region Private Methods /////////////////////////////////////////////////////////////////////////
        private void InsertCommonVariables() {
            // Insert header and footer...
            this.InsertTemplate("email.head", "email.head");
            this.InsertTemplate("email.header", "email.header");
            this.InsertTemplate("email.footer", "email.footer");

            // Email variables
            this.InsertVariable("email.subject", this.Subject);
            this.InsertVariable("email.category", this.Category);
            this.InsertVariable("email.sub-category", this.SubCategory);
            this.InsertVariable("email.language", this.Language);

            // Standard variables
            var cdnurl = Core.Config.CDNURL;
            if (cdnurl == "") cdnurl = Core.Config.PublicURL; // Test servers might have blank cdn url (ie no cdn), so for an email we force one if it is empty
            this.InsertVariable("cdn-url", cdnurl);
            this.InsertVariable("public-url", Core.Config.PublicURL);
            this.InsertVariable("server-url", Core.Config.ServerURL);
            this.InsertVariable("build-id", Core.Config.StaticBundleCacheEnabled ? Core.Config.BuildID : DateTime.Now.Ticks.ToString());
            // Email Logo
            this.InsertVariable("email-logo-path", Core.Config.EmailLogoPath);

            // Unsubscribe URL
            if (!string.IsNullOrEmpty(this.SecretCode)) {

                var unsubscribeQuery = UnsubscribeService.GetUnsubscribeQueryString(this.SecretCode, this.Email, this.Business, this.User, this.Category, this.SubCategory, MailingUnsubscription.Action.Unsubscribe.ToString().ToLowerInvariant());
                this.InsertVariable("unsubscribe-query", unsubscribeQuery);

                var manageSubscribctionQuery = UnsubscribeService.GetUnsubscribeQueryString(this.SecretCode, this.Email, this.Business, this.User, this.Category, this.SubCategory, MailingUnsubscription.Action.Confirm.ToString().ToLowerInvariant());
                this.InsertVariable("manage-subscription-query", manageSubscribctionQuery);
            } else {
                this.InsertVariable("unsubscribe-query",string.Empty);
                this.InsertVariable("manage-subscription-query", string.Empty);
            }

            // Tracking Infos
            var trackingInfo = GetTrackingInfo();
            this.InsertVariable("tracking-info", trackingInfo);
            this.InsertVariable("tracking-info-with-prefix", trackingInfo != null ? "?" + GetTrackingInfo() : null);

            // Project
            this.InsertVariable("project-name", Core.Config.ProjectName);


            // Auto-insert text variables
            this.DiscoverVariables();
            this.InsertTemplateVariables();
            this.InsertTextVariables();
        }

        private string GetTrackingInfo() {
            string trackingInfo = "utm_medium=email";
            if (!string.IsNullOrEmpty(Category)) {
                trackingInfo += "&utm_source=" + Category;
            } else {
                return null; // no tracking if required souce is unavailable
            }
            if (!string.IsNullOrEmpty(MailingTitle)) {
                trackingInfo += "&utm_campaign=" + MailingTitle;
            }
            if (!string.IsNullOrEmpty(SubCategory)) {
                trackingInfo += "&utm_content=" + SubCategory;
            }
            return trackingInfo;
        }

        private string LoadCSS() {
            // Load the css bundle
            //TODO: @dan memory caching for the cache file?

            // Derive the CSS bundle path
            // This is based on the package name, and is backwards compatible with the old hardcode "css/machinata-email-bundle.css" path...
            //var bundlePath = "css/machinata-email-bundle.css";
            //var bundlePath = "css/"+(_package.ToLower().Replace("Machinata.Product.", "Machinata.").Replace(".","-")) +"-bundle.css";
            var bundlePath = "css/"+Core.Config.EmailCSSBundle;

            try {
                // this needs probably force create=true in GetBundleCachePath
                string cachePath = Core.Bundles.Bundle.GetBundleCachePath(
                    db: _db,
                    packageName: _package,
                    bundlePath: bundlePath,
                    language: this.Language,
                    themeName: this.ThemeName,
                    debug: false,
                    profile: null,
                    outputFormat: null

                );
                return System.IO.File.ReadAllText(cachePath);
            } catch (Exception e) {
                _logger.Error(e, "Could not load email css bundle \""+ bundlePath + "\"");
                return "";
            }
        }




        #endregion

        #region Virtual Methods /////////////////////////////////////////////////////////////////



        #endregion
    }
}
