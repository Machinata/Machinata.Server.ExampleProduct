using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        /// <summary>
        /// Automatically inserts all the text variables (ie {text.xyz}) that are in this template...
        /// </summary>
        public void InsertTextVariables() {
            _logger.Trace("Inserting text variables...");
            foreach(string var in _variables) {
                if(var.StartsWith("text.")) {
                    _logger.Trace("  Found "+var);
                    this.InsertTextVariableForLanguage(var, this.Language);
                }
            }
        }

        /// <summary>
        /// Inserts a specific text variable (ie {text.xyz}) in the template.
        /// If a language is specified, that language is used, otherwise the
        /// default language of the template is used.
        /// 
        /// Typically this method is automatically called through the use of 
        /// InsertTextVariables(), which should be automatically called by the handler
        /// for the template.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="language">The language.</param>
        /// <exception cref="System.Exception">A text variable must start with 'text.'.</exception>
        public void InsertTextVariableForLanguage(string variableName, string language = null) {
            if (!variableName.StartsWith("text.")) throw new Exception("A text variable must start with 'text.'.");
            var textId = variableName.Substring(5);
            this.InsertTextForLanguage(variableName, textId, language);
        }

        /// <summary>
        /// Inserts a translated text (specified by textId) for the variable with variableName.
        /// If a language is specified, that language is used, otherwise the
        /// default language of the template is used.
        /// 
        /// Use this method if you know exactly what text you wish to place into the template at a
        /// specific variable. Otherwise typically you just use a {text.xyz} variable which is automatically
        /// placed.
        /// </summary>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="textId">The text identifier.</param>
        /// <param name="language">The language.</param>
        /// <exception cref="System.Exception"></exception>
        public void InsertTextForLanguage(string variableName, string textId, string language = null, string defaultValueIfMissing = null) {
            textId = textId.ToLower();
            if (language == null) language = this.Language;
            if (language == null) throw new Exception($"No language was passed and no language is set on the template for {variableName} with text id {textId}.");
            if (defaultValueIfMissing == null) defaultValueIfMissing = "[TRANS:" + textId + "." + this.Language + "]"; // "MISSING_TRANSLATION_" + textId + "_" + this.Language;
            var text = Core.Localization.Text.GetTranslatedTextByIdForLanguage(textId, this.Language, defaultValueIfMissing);
            this.InsertVariable(variableName, text);
        }

    }
}
