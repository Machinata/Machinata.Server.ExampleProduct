using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.SQL.SQLParser
{
    static public class SqlKeyWords
    {
        static private SortedSet<string> _keywords;
        static private int MaxKeywordLength;

        static SqlKeyWords()
        {
            InitializeSqlTokens();
        }

        static private void InitializeSqlTokens()
        {
            _keywords = new SortedSet<string>();
            _keywords.Add("*");
            _keywords.Add("ACTION");
            _keywords.Add("ADD");
            _keywords.Add("ALTER");
            _keywords.Add("AS");
            _keywords.Add("ASC");
            _keywords.Add("BY");
            _keywords.Add("CLUSTERED");
            _keywords.Add("COLUMN");
            _keywords.Add("CONSTRAINT");
            _keywords.Add("CREATE");
            _keywords.Add("DEFAULT");
            _keywords.Add("DELETE");
            _keywords.Add("DESC");
            _keywords.Add("DROP");
            _keywords.Add("EXEC");
            _keywords.Add("EXECUTE");
            _keywords.Add("FROM");
            _keywords.Add("FUNCTION");
            _keywords.Add("GROUP");
            _keywords.Add("INSERT");
            _keywords.Add("INTO");
            _keywords.Add("INDEX");
            _keywords.Add("KEY");
            _keywords.Add("LEVEL");
            _keywords.Add("NONCLUSTERED");
            _keywords.Add("OCT");
            _keywords.Add("ORDER");
            _keywords.Add("PRIMARY");
            _keywords.Add("PROC");
            _keywords.Add("PROCEDURE");
            _keywords.Add("RULE");
            _keywords.Add("SCHEMA");
            _keywords.Add("SELECT");
            _keywords.Add("STATISTICS");
            _keywords.Add("STATUS");
            _keywords.Add("TABLE");
            _keywords.Add("TRIGGER");
            _keywords.Add("UPDATE");
            _keywords.Add("USER");
            _keywords.Add("UNIQUE");
            _keywords.Add("VALUES");
            _keywords.Add("VIEW");
            _keywords.Add("WHERE");
            _keywords.Add("NOT NULL");
            
            MaxKeywordLength = _keywords.Max(s => s.Length);
        }

        //hack: This is probably an extraordinary candidate for optimization.
        static public string GetSqlKeyWord(Char[] theCharArray)
        {
            //TODO: dan: what the fuck is this method doing??? 

            int charsToAnalyze = new int[] { theCharArray.Length, MaxKeywordLength }.Min();
            List<string> possibleKeyWords = null;

            for (int charIndex = 0; charIndex < charsToAnalyze; charIndex += 1)
            {
                var testChar = char.ToUpper(theCharArray[charIndex]);
                if (possibleKeyWords == null)
                {
                    possibleKeyWords = _keywords.Where(k => k[charIndex] == theCharArray[charIndex]).ToList<string>();
                }
                else
                {
                    //narrow down the list from the previous list
                    possibleKeyWords = possibleKeyWords.Where(s => s[charIndex] == theCharArray[charIndex]).ToList<string>();
                }

                if (possibleKeyWords == null || possibleKeyWords.Count == 0)
                {
                    return null;
                }
                if (possibleKeyWords.Count == 1)
                {
                    //bug: need to not trigger keywords on incorrect tokens like "selecta"
                    string keyword = new String(theCharArray, 0, possibleKeyWords[0].Length).ToUpper();
                    if (possibleKeyWords[0] == keyword) {
                        return keyword;
                    }
                    else {
                        return null;
                    }
                }
            }
            return null;
        }

    }
}
