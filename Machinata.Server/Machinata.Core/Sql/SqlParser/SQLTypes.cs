using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.SQL.SQLParser
{
    static public class SqlTypes
    {
        static private SortedSet<string> _types;

        static SqlTypes()
        {
            InitializeSqlTypes();
        }

        static private void InitializeSqlTypes()
        {
            _types = new SortedSet<string>();
            // MySQL
            _types.Add("CHAR");
            _types.Add("VARCHAR");
            _types.Add("TINYTEXT");
            _types.Add("TEXT");
            _types.Add("BLOB");
            _types.Add("MEDIUMTEXT");
            _types.Add("MEDIUMBLOB");
            _types.Add("LONGTEXT");
            _types.Add("LONGBLOB");
            _types.Add("ENUM");
            _types.Add("SET");
            _types.Add("TINYINT");
            _types.Add("SMALLINT");
            _types.Add("MEDIUMINT");
            _types.Add("INT");
            _types.Add("BIGINT");
            _types.Add("FLOAT");
            _types.Add("DOUBLE");
            _types.Add("DECIMAL");
            _types.Add("DATE");
            _types.Add("DATETIME");
            _types.Add("TIMESTAMP");
            _types.Add("TIME");
            _types.Add("YEAR");
        }

        static public string GetSqlType(string text) {
            var str = text.ToUpper();
            return _types.SingleOrDefault(t => t == str);
        }

    }
}
