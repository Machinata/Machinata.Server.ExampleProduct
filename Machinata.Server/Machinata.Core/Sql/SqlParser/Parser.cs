using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.SQL.SQLParser {

    public class Parser {

        public List<SqlStatement> Statements;

        public string OriginalSQL;

        public string GeneratedSQL {
            get {
                return string.Join("; ",this.Statements.Select(s => s.GeneratedSQL));
            }
        }

        public Parser() {
            
        }

        public void Parse(string sql) {
            this.OriginalSQL = sql;
            this.Statements = new List<SqlStatement>();

            foreach(var strstatement in sql.Split(';')) {
                var statement = new SqlStatement(strstatement);
                this.Statements.Add(statement);
            }
        }

        public void Reduce() {
            foreach(var statement in this.Statements) {
                statement.Reduce();
            }
        }
    }
}
