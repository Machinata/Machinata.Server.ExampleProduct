using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.SQL.SQLParser {

    /// <summary>
    /// WORK IN PROGRESS
    /// </summary>
    public class SqlStatement {

        public enum StatementTypes {
            Unknown,
            CreateTable,
            AlterTableAddKey,
            AlterTableAddPrimaryKey,
            AlterTableAddConstraint
        }

        public string OriginalSQL;
        public List<SqlToken> SQLTokens;
        public string Table;
        public List<SqlColumn> Columns;
        public StatementTypes StatementType;
        
        public string GeneratedSQL {
            get {
                return string.Join(" ", this.SQLTokens.Select(tt => tt.Text));
            }
        }

        public SqlStatement(string sql) {
            this.OriginalSQL = sql.Trim();
            this.StatementType = StatementTypes.Unknown;
            this.Columns = new List<SqlColumn>();

            // Get tokens
            this.SQLTokens = SqlTokenizer.Tokenize(this.OriginalSQL);
            this.SQLTokens = this.SQLTokens.Where(t => t.TokenType != SqlToken.TokenTypes.Whitespace && t.TokenType != SqlToken.TokenTypes.Newline).ToList();

            if(SqlToken.MatchTokens(this.SQLTokens,
                new SqlToken(SqlToken.TokenTypes.Keyword,"CREATE"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"TABLE"),
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.StatementType = StatementTypes.CreateTable;
                this.Table = this.SQLTokens.ElementAt(2).Text;
                this.Columns = GetColumns(this.SQLTokens);
            } else if(SqlToken.MatchTokens(this.SQLTokens,
                new SqlToken(SqlToken.TokenTypes.Keyword,"ALTER"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"TABLE"),
                new SqlToken(SqlToken.TokenTypes.Name),
                new SqlToken(SqlToken.TokenTypes.Keyword,"ADD"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"PRIMARY"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"KEY")
                )) {
                this.StatementType = StatementTypes.AlterTableAddPrimaryKey;
                this.Table = this.SQLTokens.ElementAt(2).Text;
                this.Columns = GetColumns(this.SQLTokens);
            }  else if(SqlToken.MatchTokens(this.SQLTokens,
                new SqlToken(SqlToken.TokenTypes.Keyword,"ALTER"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"TABLE"),
                new SqlToken(SqlToken.TokenTypes.Name),
                new SqlToken(SqlToken.TokenTypes.Keyword,"ADD"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"KEY")
                )) {
                this.StatementType = StatementTypes.AlterTableAddKey;
                this.Table = this.SQLTokens.ElementAt(2).Text;
                this.Columns = GetColumns(this.SQLTokens);
            } else if(SqlToken.MatchTokens(this.SQLTokens,
                new SqlToken(SqlToken.TokenTypes.Keyword,"ALTER"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"TABLE"),
                new SqlToken(SqlToken.TokenTypes.Name),
                new SqlToken(SqlToken.TokenTypes.Keyword,"ADD"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"CONSTRAINT")
                )) {
                this.StatementType = StatementTypes.AlterTableAddConstraint;
                this.Table = this.SQLTokens.ElementAt(2).Text;
                this.Columns = GetColumns(this.SQLTokens);
            }
        }

        public void Reduce() {
            if(this.StatementType == StatementTypes.CreateTable) {
                // Trim off everything after the create main parentheses
                var i = this.SQLTokens.FindLastIndex(t => t.TokenType == SqlToken.TokenTypes.CloseParenthesis);
                this.SQLTokens = this.SQLTokens.Take(i+1).ToList();
            }
            // Reduce columns
            foreach(var column in this.Columns) {
                column.Reduce();
            }
            // Sort columns

        }
        
        

        public List<SqlColumn> GetColumns(IList<SqlToken> tokens) {
            var ret = new List<SqlColumn>();
            var p = 0;
            var currentColumn = new List<SqlToken>();
            for (int i = 0; i < tokens.Count(); i++) {
                var t = tokens[i];
                if (t.TokenType == SqlToken.TokenTypes.OpenParenthesis) {
                    p++;
                    if(p > 1) currentColumn.Add(t);
                } else if (t.TokenType == SqlToken.TokenTypes.CloseParenthesis) {
                    if(p > 1) currentColumn.Add(t);
                    p--;
                } else if (p == 1 && t.TokenType == SqlToken.TokenTypes.Comma) {
                    //ret.Add(string.Join(" ", currentColumn.Select(tt => tt.Text)));
                    ret.Add(new SqlColumn(currentColumn));
                    currentColumn = new List<SqlToken>();
                } else if (p >= 1) {
                    currentColumn.Add(t);
                } else {
                    //currentColumn.Add(t);
                }
                
            }
            if(currentColumn.Count > 0) {
                //ret.Add(string.Join(" ", currentColumn.Select(tt => tt.Text)));
                ret.Add(new SqlColumn(currentColumn));
            }
            return ret;
        }
    }
}
