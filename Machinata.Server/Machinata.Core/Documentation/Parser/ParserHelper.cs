using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Documentation.Parser {
    public static class ParserHelper {

        public static IList<string> GetNSParts(string fullName) {
            return fullName.Split(new string[] { ".", "[\"", "\"]" }, StringSplitOptions.RemoveEmptyEntries);
        }


        /// <summary>
        /// Returns first index of a key word in a line of code
        /// 
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="keyword">The keyword.</param>
        /// <returns>-1 if either of the strings are empty or not found</returns>
        public static int IndexOfKeyword(string content, string keyword) {
            if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(keyword)) {
                return -1;
            }
            var regex = new Regex(@"\s" + keyword + @"\s");
            var match = regex.Match(content);
            if (match.Success) {
                return match.Index;
            }
            return -1;
            
        }
    }


}
