using Machinata.Core.Model;
using Machinata.Core.Documentation.Model;
using Machinata.Core.Documentation.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machinata.Core.Documentation.Parser {

    /// <summary>
    /// Basic GhostDoc/Nerves Parser Logic
    /// Take first line after /// for signature,name etc
    /// </summary>
    /// <seealso cref="Machinata.Core.Documentation.Parser.Parser" />
    public abstract class GhostDocParser : Parser {
        public const string COMMENT_PREFIX = @"///";

        protected string[] Lines { get; private set; }
        protected int LineNumber { get; private set; }

        internal override IEnumerable<DocumentationItem> ParseImpl(string content, bool includeSourceCode) {

            var items = new List<DocumentationItem>();

            // Split into lines
            this.Lines = content.Replace("\r", "").Split('\n');

            var currentComment = new StringBuilder();

            // Code for comment
            var currentCode = new StringBuilder();
            // Keep reference to the last item for the current code
            DocumentationItem lastItem = null;

            // Line Numer
            this.LineNumber = 0;

            foreach (var line in this.Lines) {

                this.LineNumber++;


                // Very last item at bottom of file
                if (this.LineNumber == Lines.Count() && lastItem != null && includeSourceCode == true) {
                    lastItem.Code = currentCode.ToString();
                }

                if (line.Trim() == string.Empty) {
                    continue;
                }


                PreProcess(lastItem,line);

                if (line.Trim().StartsWith(COMMENT_PREFIX, StringComparison.InvariantCulture)) {
                    // Collect comment lines for next DocumentationItem
                    currentComment.AppendLine(line.Replace(COMMENT_PREFIX, string.Empty));
                    if (lastItem != null) {

                        lastItem.Code = CleanupCode(currentCode.ToString());
                        currentCode.Clear();
                        lastItem = null;
                    }
                   
                } else if (currentComment.Length > 0 && IgnoreLine(line) == false) {
                    // First line which is not a comment
                    var item = new DocumentationItem();

                    if (items.Count == 0) {
                        currentCode.Clear();
                    }

                    var comment = currentComment.ToString();
                                    
                    item.Type = GetType(line);

                    item.Signature = GetSignature(line, item.Type);

                    // Override type?
                    var type = Extract("type", comment);
                    if (string.IsNullOrEmpty(type) == false) {
                        item.Type = type;
                    }
                  
                   
                    // Basic GhostDoc tags
                    item.Summary = Extract("summary", comment);
                    item.Deprecated = Extract("deprecated", comment);
                    item.Hidden = HasNonContentTag("hidden", comment);
                    item.Example = Extract("example", comment);
                    item.Inherits = Extract("inherits", comment);
                    item.Returns = Extract("returns", comment);
                    item.Value = Extract("value", comment);
                    item.LineNumber = this.LineNumber;

                    // Get the name from line, override if provided
                    item.FullName = GetFullName(item, items, line, item.Type);
                    var fullnameFromComment = Extract("name", comment);
                    if (fullnameFromComment != null) item.FullName = fullnameFromComment;
                    item.Name = GetNameFromFullName(item.FullName);

                    // Value (default from code)
                    if (string.IsNullOrEmpty(item.Value)) {
                        item.Value = GetValue(item.Type, line);
                    }

                    // Parameters explicit
                    item.Parameters = ExtractMultipleWithAttribute("param",comment);

                    // Auto parsed parameters
                    var parametersFromSignature = GetParameters(line, item.Type);
                    foreach(var param in parametersFromSignature.Keys) {

                        // Add if not already explicitly defined
                        if (item.Parameters?.Keys.Contains(param) == false) {
                            item.Parameters[param] = parametersFromSignature[param];
                        }
                    }
                    

                    // Get namespace automatically and override if needed
                    item.Namespace = GetNamespace(item);
                    var namespaceFromComment = Extract("namespace", comment);
                    if (namespaceFromComment != null) item.Namespace = namespaceFromComment;

                    // Process Item after
                    this.PostProcess(item);

                    // Defaults
                    if (item.DataType == null) item.DataType = item.Type;
                    if (item.SymanticName == null) item.SymanticName = item.Name;

                    // First line is also code
                    currentCode.AppendLine(line);

                    // Collect items
                    items.Add(item);

                    // Keep track of last item to add to comment later
                    lastItem = item;

                    // Clear current comment for next item
                    currentComment.Clear();


                } else {
                    currentCode.AppendLine(line);
                }
                
            }


            return items;
        }

        public virtual Properties GetParameters(string line, string type) {
            return new Properties();
        }

        public virtual void PostProcess(DocumentationItem item) {
           
        }

        public virtual string CleanupCode(string code) {
            return code;
        }

        public virtual void PreProcess(DocumentationItem lastItem, string line) {
       
        }


        /// <summary>
        /// Gets the value/default from code line (e.g.)
        /// </summary>
        /// <returns>null if unkwnow</returns>
        protected virtual string GetValue(string type, string line) {
            if (type == Parser.TYPE_VARIABLE) {
                var indexOfEqual = line.IndexOf('=');
                if (indexOfEqual > 0 && indexOfEqual< line.Length-1) {
                    string value = line.Substring(indexOfEqual);
                    value = value.Trim(';');
                    value = value.Trim('=');
                    value = value.Trim();
                    return value;
                }
            }
            return null;
        }

        /// <summary>
        /// To Ignore a line as code
        /// E.g. we dont want in c# [RequestHandler] to be interpreted as the signature
        /// </summary>
        protected abstract bool IgnoreLine(string line);


        /// <summary>
        /// In we retrieve the namespace out of the fullname
        /// </summary>
        public virtual string GetNamespace(DocumentationItem item) {
            var parts = GetNSParts(item.FullName);
            if (parts.Count > 1) {
                var nameSpace = string.Join(".", parts.Take(parts.Count - 1));
                return nameSpace;
            }
            return "undefined";
        }
        public abstract string GetSignature(string line, string type);


        protected virtual string GetNameFromFullName(string fullName) {
            var parts = GetNSParts(fullName);
            return string.Join(".", parts.Last());
        }

        protected IList<string> GetNSParts(string fullName) {
            return ParserHelper.GetNSParts(fullName);
        }

        protected abstract string GetFullName(DocumentationItem item, IEnumerable<DocumentationItem> previousItems, string line, string type);


        /// <summary>
        /// Gets the type. of the current line
        /// for forward looking use the content/lineNumber
        /// </summary>
        /// <returns></returns>
        public abstract string GetType(string line);

        /// <summary>
        /// Extracts the content of a specified tag like <summary></summary>.
        /// </summary>
        protected string Extract(string name, string content) {
            if (string.IsNullOrWhiteSpace(content)) {
                return null;
            }
            var startTag = "<" + name + ">";
            var start = content.IndexOf(startTag, StringComparison.InvariantCulture);
            var endTag = "</" + name + ">";
            var end = content.IndexOf(endTag, StringComparison.InvariantCulture);

            if (start > 0 && end > 0 && end > start) {
                var tag =  content.Substring(start + startTag.Length, end - (start + startTag.Length));
                return tag;
            }
            return null;
       }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        protected Properties ExtractMultipleWithAttribute(string name, string content) {
            if (string.IsNullOrWhiteSpace(content)) {
                return new Properties();
            }

            var result = new Properties();
            var start = -1;
            do {
                var startTag = "<" + name + " name=\"";
                start = content.IndexOf(startTag, StringComparison.InvariantCulture);
                var endTag = "</" + name + ">";
                var end = content.IndexOf(endTag, StringComparison.InvariantCulture);

                if (start > 0 && end > 0 && end > start) {

                    var attrNameEnd = content.IndexOf("\"", start + startTag.Length + 1, StringComparison.InvariantCulture);
                    if (attrNameEnd > start) {
                        var attrName = content.Substring(start + startTag.Length, attrNameEnd - startTag.Length - start);
                        var attrLenght = end - (start + startTag.Length + attrName.Length + 2);
                        var tag = content.Substring(attrNameEnd + 2, attrLenght);
                        result[attrName] = tag;
                        content = content.Substring(attrNameEnd + 2 + attrLenght + endTag.Length);
                    }
                }
            } while (start > 0);
            return result;
        }

        protected bool HasNonContentTag(string name, string content) {
            if (string.IsNullOrWhiteSpace(content)) {
                return false;
            }
            var tag = "<" + name + "/>";
            var start = content.IndexOf(tag, StringComparison.InvariantCulture);
            return start >= 0;
        }
    }
}
