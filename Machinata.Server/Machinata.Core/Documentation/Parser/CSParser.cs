using Machinata.Core.Documentation.Model;
using Machinata.Core.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Machinata.Core.Documentation.Parser {
    public class CSParser : GhostDocParser {
        public override string Language
        {
            get
            {
                return "cs";
            }
        }

        public string CurrentStructure { get; private set; }
        public string CurrentStructureType { get; private set; }

        public string CurrentNameSpace { get; private set; }

        public override string GetType(string line) {
            if (string.IsNullOrWhiteSpace(line)) {
                return null;
            }

            // Constructor
            if (string.IsNullOrEmpty(this.CurrentStructure) == false && Regex.IsMatch(line, @"\s"+ this.CurrentStructure + @"(\s)*\(")) {
                return Parser.TYPE_CONSTRUCTOR;
            }

            // Function: signature in one line
            if (line.Contains("(") && line.Contains(")") && line.Contains(";") == false) {
                return Parser.TYPE_FUNCTION;
            }
            // Function (abstract)
            else if (line.Contains("(") && line.Contains(")") && line.Contains(";") == true && ( line.Contains(" abstract ") == true || this.CurrentStructureType == Parser.TYPE_INTERFACE)) {
                return Parser.TYPE_FUNCTION;
            }
            // Function one liner
            if (line.Contains("(") && line.Contains(")") && line.Contains("{") == true && line.Contains("}") == true) {
                return Parser.TYPE_FUNCTION;
            }
            // Function: signature multiple lines
            if (ExtractMultilineFunctionSignature(line, this.LineNumber, this.Lines) != null) {
                return Parser.TYPE_FUNCTION;
            }

            // Class
            var className = ExtractClassFromLine(line);
            if (string.IsNullOrEmpty(className) == false) {
                return Parser.TYPE_CLASS;
            }

            // Interface
            var @interface = ExtractStructureFromLine(line,"interface");
            if (string.IsNullOrEmpty(@interface) == false) {
                return Parser.TYPE_INTERFACE;
            }


            // Variable and not a initialized auto property
            if (line.Contains("get;") == false && line.Contains("set;") == false &&

                (line.Contains('=')
                || (line.Contains("(") == false && line.Trim().EndsWith(";", StringComparison.InvariantCulture))

                )) {
                return Parser.TYPE_VARIABLE;
            }

            // enum multiple lines
            if (ParserHelper.IndexOfKeyword(line, "enum") >=0 )  {
                return Parser.TYPE_ENUM;
            }

            // Propertiy one liner
            if ((line.Contains("{") && line.Contains("}")) && (line.Contains("get ") || line.Contains("set") || line.Contains("get;"))) {
                return Parser.TYPE_PROPERTY;
            }

            // Property multiple lines
            if (line.Contains(";") == false) {
                return Parser.TYPE_PROPERTY;
            }


            return Parser.TYPE_UNKNOWN;
        }

        /// <summary>
        /// Looks forward in the next lines whether this is a function signature in multiple lines
        /// </summary>
        private string ExtractMultilineFunctionSignature(string startLine, int lineNumber, string[] lines) {
            // Not a multiline function signature
            if (startLine.Contains("(") == false || startLine.Contains(")") == true) {
                return null;
            }
            var signature = new StringBuilder(startLine);
            var currentLine = string.Empty;
            do {
                currentLine = lines.ElementAt(lineNumber);
                currentLine = currentLine.Trim();
                signature.AppendLine(currentLine);

                lineNumber++;

            } while (currentLine.EndsWith(","));

            // We have a multiline signature
            if (currentLine.EndsWith(")")|| currentLine.EndsWith("{")) {
                return signature.ToString().Trim().TrimEnd('{').TrimEnd('{');
            } else {
                return null;
            }

            
        }

        protected override string GetFullName(DocumentationItem item, IEnumerable<DocumentationItem> previousItems, string line, string type) {
            var name = GetName(line, type);

            var ns = this.CurrentNameSpace;

            if ( IsStructure( type) == false || CurrentStructure.Contains(".") == true ) {
                var className = this.CurrentStructure;
                if (className!= null && IsStructure(type) == true) {
                    // NESTED CLASS
                    // TODO: MORE LEVELS OF NESTING
                   className = className.Split('.').First();
                }
                return $"{ns}.{className}.{name}";
            } else {
                return $"{ns}.{name}";
            }
          
        }

        private string GetName(string line, string type) {
            if (type == Parser.TYPE_CLASS) {
                return ExtractClassFromLine(line);
            } else if (type == Parser.TYPE_INTERFACE) { // TODO ONE FOR STRUCT,CLASS,INTERFACE
                return ExtractStructureFromLine(line, "interface");
            } else if (type == Parser.TYPE_FUNCTION || type == Parser.TYPE_CONSTRUCTOR) {
                var indexOfBracket = line.IndexOf("(");
                var name = line.Substring(0, indexOfBracket);
                var lastSpaceBefore = name.LastIndexOf(' ');
                return name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
            } else if (type == Parser.TYPE_VARIABLE) {
                var indexOfE = line.IndexOf("=");
                // with an equal
                if (indexOfE >= 0) {
                    var name = line.Substring(0, indexOfE);
                    name = name.Trim();
                    var lastSpaceBefore = name.LastIndexOf(' ');
                    name = name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
                    return name;
                }
                // without equal: e.g. public string FormNamespace;
                {
                    var lastSpaceBefore = line.LastIndexOf(' ');
                    var name = line.Substring(lastSpaceBefore + 1, line.Length - lastSpaceBefore - 1);
                    return name.Trim(';');
                }

            } else if (type == Parser.TYPE_PROPERTY) {
                var indexOfE = line.IndexOf("{");
                string name = line;

                // Inline Property
                if (indexOfE > 0) {
                    name = line.Substring(0, indexOfE);
                } else {
                    // Property multiple lines
                    name = line;
                }
                name = name.Trim();

                // Indexer (currently handled as property)
                //https://stackoverflow.com/questions/20313308/c-sharp-beginner-delete-all-between-two-characters-in-a-string-regex
                name = Regex.Replace(name, @"\[.*?\]", string.Empty);

                var lastSpaceBefore = name.LastIndexOf(' ');
                return name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
            } else if (type == Parser.TYPE_ENUM) {
                const string searchString = "enum";
                var index = ParserHelper.IndexOfKeyword(line, searchString);
                string name = line;
                if (index > 0) {
                    name = line.Substring(index + searchString.Length + 1, name.Length - index - searchString.Length -1);
                }
                var indexDouble = name.IndexOf(":");
                if (indexDouble > 0) {
                    name = name.Substring(0, indexDouble);
                }
                name = name.Trim().TrimEnd('{').Trim();
                return name;
            }
            return "unknown";
        }

        public override string GetSignature(string line, string type) {
            if (type == Parser.TYPE_PROPERTY) {
                return line.Trim();
            }
            if (type == Parser.TYPE_FUNCTION || type == Parser.TYPE_CONSTRUCTOR) {

                // mulitline signature
                var multiLineSignature = ExtractMultilineFunctionSignature(line, this.LineNumber, this.Lines);
                if (multiLineSignature != null) {
                    return multiLineSignature;
                }

                // one line signature
                var indexBraket = line.IndexOf("{");
                if (indexBraket > 0) {
                    return line.Substring(0, indexBraket).Trim();
                }
            }
            if (line != null) {
                line = line.Replace("{", string.Empty);
                line = line.Trim();
            }
            return line;
        }


        private string ExtractNamespaceFromLine(string line) {
            var regex = new Regex(@"namespace\s");
            if (string.IsNullOrWhiteSpace(line) == false) {
                if (regex.IsMatch(line)) {
                    var ns = line;
                    ns = ns.Replace("namespace", string.Empty);
                    ns = ns.Replace("{", string.Empty);
                    ns = ns.Trim();
                    return ns;
                }
            }
            return null;
        }

        private string ExtractClassFromLine(string line) {
            var regex = new Regex(@"\sclass\s");
            if (string.IsNullOrWhiteSpace(line) == false) {
                line = RemoveEnclosedString(line);
                var match = regex.Match(line);
                if (match.Index > 0) {
                    var indexAfterClass = match.Index + "class ".Length;
                    var c = line.Substring(indexAfterClass, line.Length - indexAfterClass);
                    c = c.Replace("class", string.Empty);
                    c = c.Replace("{", string.Empty);
                    var indexOfColon = c.IndexOf(':');
                    if (indexOfColon > 0) {
                        c = c.Substring(0, indexOfColon - 1);
                    }
                    c = c.Trim();
                    return c;
                }
            }
            return null;
        }

        /// <summary>
        /// Removes all occurences of an opening and closing substring
        /// e.g. string.Concat("we want this string removed" + "and this")
        /// results in string.Concat( +)
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns></returns>
        private string RemoveEnclosedString(string line) {
           if (string.IsNullOrWhiteSpace(line) == true) {
                return line;
            }
            var start = line.IndexOf("\"");

            if (start < 0) {
                return line;
            }

            var end = line.IndexOf( "\"", start + 1);

            if (start > 0 && end > 0) {
                var stringToDelete = line.Substring(start, end - start + 1);
                line = line.Replace(stringToDelete, string.Empty);
                return RemoveEnclosedString(line);
             } else {
                return line;
            }
            
        }


        /// <summary>
        /// Extracts interface, class, struct from a given line of code
        /// </summary>
        /// <param name="line">The line.</param>
        /// <param name="keyword">The keyword (class, interface, struct).</param>
        /// <returns></returns>
        private string ExtractStructureFromLine(string line, string keyword) {
            var regex = new Regex(@"\s"+keyword+@"\s");
            if (string.IsNullOrWhiteSpace(line) == false) {
                line = RemoveEnclosedString(line);
                var match = regex.Match(line);
                if (match.Index > 0) {
                    var indexAfterClass = match.Index + (keyword +" ").Length;
                    var c = line.Substring(indexAfterClass, line.Length - indexAfterClass);
                    c = c.Replace(keyword, string.Empty);
                    c = c.Replace("{", string.Empty);
                    var indexOfColon = c.IndexOf(':');
                    if (indexOfColon > 0) {
                        c = c.Substring(0, indexOfColon - 1);
                    }
                    c = c.Trim();
                    return c;
                }
            }
            return null;
        }

        protected override bool IgnoreLine(string line) {

            // Is it an Attribute?
            if (string.IsNullOrEmpty(line) == false && line.Trim().StartsWith("[", StringComparison.InvariantCulture)) {
                return true;
            }
            return false;
        }

        public override void PreProcess(DocumentationItem lastItem, string line) {
            base.PreProcess(lastItem, line);

            // Ignore comment lines
            if (line.Trim().StartsWith(@"//", StringComparison.InvariantCulture) == true) {
                return;
            }

            var className = ExtractClassFromLine(line);
            if (string.IsNullOrEmpty(className) == false) {
                this.CurrentStructure = className;
                this.CurrentStructureType = Parser.TYPE_CLASS;
            }

            var @interface = ExtractStructureFromLine(line,"interface");
            if (string.IsNullOrEmpty(@interface) == false) {
                this.CurrentStructure = @interface;
                this.CurrentStructureType = Parser.TYPE_INTERFACE;
            }

            var @enum = ExtractStructureFromLine(line, "enum");
            if (string.IsNullOrEmpty(@enum) == false) {

                // enum nested in class
                if (string.IsNullOrEmpty(this.CurrentStructure) == false) {
                    this.CurrentStructure += "." + @enum;
                } else { 
                    // enum outside class
                    this.CurrentStructure = @enum;
                }
                this.CurrentStructureType = Parser.TYPE_ENUM;
            }

            // enum: end of
            if (line.Trim() == "}" && this.CurrentStructureType == Parser.TYPE_ENUM) {

                var nsParts = this.CurrentStructure.Split('.');

                if (nsParts.Length == 1) {
                    this.CurrentStructure = null;
                } else {
                    // we find ourselves in the parent class
                    this.CurrentStructure = nsParts.First();
                    this.CurrentStructureType = Parser.TYPE_CLASS;
                }
            }

            var namespaceName = ExtractNamespaceFromLine(line);
            if (string.IsNullOrEmpty(namespaceName) == false) {
                this.CurrentNameSpace = namespaceName;
            }

            // TODO: REMOVE USED FOR DEBUGGING
            if (line.Contains("PublicIdForQuantityEntity")) {
                string asdf = "";
            }
        }
    }
}
