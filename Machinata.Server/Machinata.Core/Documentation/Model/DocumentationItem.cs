using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Documentation.Model {

    /// <summary>
    /// Part of Code that can be documented
    /// </summary>
    public class DocumentationItem: ModelObject {


        public DocumentationItem() {
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the namespace.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data
        /// </summary>
        /// <value>
        /// The namespace.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Namespace { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// C# Example for Machinata.Core.Model.Price.ToString: ToString
        /// JS Example for Machinata.Reporting.Data.loadData: loadData
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Name { get; set; }

        /// <summary>
        /// Represents the item name in a more programmer-understable fashion.
        /// For example, a function with the signature xyz = function(a,b,c) would have a name of
        /// 'xyz(a,b,c)' instead of just 'xyz'.
        /// This makes the documentation much more easier to read.
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string SymanticName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price.ToString
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data.loadData
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string FullName { get; set; }
        

        /// <summary>
        /// Gets or sets the signature.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price.ToString(string format = null, bool changeThousandsSeperator = true, bool changeZeroCents = true)
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data.loadData(dataSource, onSuccess)
        /// </summary>
        /// <value>
        /// The signature.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// Either 
        ///     function
        ///     class
        ///     variable
        ///     constant
        ///     ...
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Type { get; set; }

        /// <summary>
        /// Represents the datatype for variable types.
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string DataType { get; set; }

        /// <summary>
        /// The comment summary, for example in C# the [summary] XML tag 
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.VIEW)]
        public string Summary { get; set; }
        
        /// <summary>
        /// The comment value, for example in C# the [value] XML tag
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Value { get; set; }

        /// <summary>
        /// The comment example, for example in C# the [example] XML tag
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Example { get; set; }

        /// <summary>
        /// To mark an item deprecated
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Deprecated { get; set; }

        /// <summary>
        /// To show an items inheritance
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Inherits { get; set; }

        /// <summary>
        /// To show an items inheritance
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Returns { get; set; }

        /// <summary>
        /// To mark an item hidden
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Hidden { get; set; }

        /// <summary>
        /// The actual file the item comes from
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string File { get; set; }

        /// <summary>
        /// Paramaters which have been defined in the comment 
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)] // Problem: these is a SortedDictionary
        [FormBuilder(Forms.Admin.VIEW)]
        public Properties Parameters { get; set; }

        /// <summary>
        /// This an additional item generated from the parser (e.g. missing class or namespace)
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Generated { get; set; }




        /// <summary>
        /// Linenumber from the source file
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        public int LineNumber { get; set; }

        public int NamespaceLevel
        {
            get
            {
                return this.NamespaceURL.Count(ns => ns == '-');
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        public string NamespaceURL
        {
            get { return _createShortUrl(this.Namespace); }
        }

        private string _createShortUrl(string path) {
            return path.Replace(".", "-");
        }


        [FormBuilder(Forms.System.LISTING)]
        public string FullNameURL
        {
            get { return _createShortUrl(this.FullName); }
        }


        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string SummaryShort
        {
            get {
                if (this.Summary == null) return null;
                return Core.Util.String.CreateSummarizedText(this.Summary, 30, true, true, true);
            }
        }

      

        public override JObject GetJSON(FormBuilder form = null) {
            var current = base.GetJSON(form);
            //current["parameters"] = this.Parameters?.ToJObject();
            return current;
        }

        public override string ToString() {
            return this.FullName;
        }

        /// <summary>
        /// Gets the hierarchy  in list of strings
        /// </summary>
        public IList<string> GetNSParts() {
            return this.FullName.Split('.');
        }
    }


   
}
