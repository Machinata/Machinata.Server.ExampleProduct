using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Machinata.Core.Util;
using Machinata.Core.Lifecycle;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Documentation.Model {


    /// <summary>
    /// Collection of Commentable Items such as Functions, regions, variables etc
    /// in same Language (c#, js etc)
    /// in same Namespace (Machinata.Core )
    /// /// </summary>
    public class DocumentationPackage : ModelObject, IPublishedModelObject{

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language, either C#, JS, CSS, or HTML.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The package name, for example Machinata.Core or machinata-core (JS).
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; }

        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Credits { get; set; }

        [FormBuilder(Forms.Frontend.JSON)]
        public string License { get; set; }

        [FormBuilder(Forms.Frontend.JSON)]
        public string Link { get; set; }

        /// <summary>
        /// Date of creation
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        public DateTime Date { get; set; }

        /// <summary>
        /// Original path where package is generated from
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        public string Path { get; set; }


        public IEnumerable<DocumentationItem> Items;

        [FormBuilder(Forms.Frontend.JSON)]
        public bool Published { get; set; }

        [FormBuilder(Forms.Frontend.JSON)]
        public int? ItemsCheck { get; set; }

        public override JObject GetJSON(FormBuilder form = null) {
            var current = base.GetJSON(form);
            current["items"] = new JArray(this.Items.Select(c => c.GetJSON(form)));
            return current;
        }

      

        public override string PublicId
        {
            get
            {
                return this.Name;
            }
        }

      

        /// <summary>
        /// Reads from a local filesystem dir.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static IEnumerable<DocumentationPackage> ReadFromDirectory(string path) {
            var files = Directory.GetFiles(path);
            foreach (var file in files) {
                var content = File.ReadAllText(file);
                var documentation = Core.JSON.Deserialize(content, typeof(DocumentationPackage), true) as DocumentationPackage;
                yield return documentation;
            }
        }

        /// <summary>
        /// Reads all files at a specific cms page
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static IEnumerable<DocumentationPackage> ReadFromCMS(ModelContext db, string path) {

            ContentNode page; 

            var files = ContentNode.DownloadFileNodesByPath(db, path, out page);
            foreach (var file in files) {
                DocumentationPackage documentation = ReadFromFile(file);

                documentation.Published = page.Published;

                yield return documentation;
            }
        }

        private static DocumentationPackage ReadFromFile(string file) {
            var content = File.ReadAllText(file);
            var documentation = Core.JSON.Deserialize(content, typeof(DocumentationPackage), true) as DocumentationPackage;

            if (documentation.ItemsCheck != null && documentation.ItemsCheck != documentation.Items.Count()) {
                throw new BackendException("import-documentation", $"Error importing/parsing documentation from '{file}'. Expected {documentation.ItemsCheck} items but only imported {documentation.Items.Count()}");
            }



            // Hack why is this needed....cannot save empty properties in json?
            foreach (var item in documentation.Items) {
                if (item.Parameters == null) {
                    item.Parameters = new Properties();
                }
            }

            return documentation;
        }

        /// <summary>
        /// Reads from the subpages of a given path
        /// </summary>
        /// <param name="db"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IEnumerable<DocumentationPackage> ReadFromCMSPages(ModelContext db, string path) {

            var node = db.ContentNodes()
             .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
             .GetNodeByPath(path);

            var documentations = new List<DocumentationPackage>();

            // pages
            var pages = node.ChildrenForType(ContentNode.NODE_TYPE_NODE);

            foreach(var page in pages) {
                var docs = ReadFromCMS(db, page.Path);
                documentations.AddRange(docs);
            }

            return documentations;
        }

        /// <summary>
        /// Parses Code with comments and generates a DocumentationPackage in Machinata json format
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="extension">only files with this extension extension to filter.</param>
        /// <param name="prefix">only files with this prefix</param>
        /// <param name="name">name of the package. if empty or null it will thake the folder name</param>
        /// <returns></returns>
        public static DocumentationPackage ParseFromDirectory(string path, string extension, string prefix, bool generateMissingNamespaceItems = false, string name = null, bool includeSourceCode= false, bool published = true) {
            var files = Directory.GetFiles(path, "*" + extension, SearchOption.AllDirectories).AsQueryable();
            files = files.Where(f => System.IO.Path.GetFileName(f).StartsWith(prefix, StringComparison.InvariantCulture));
            var language = extension.Replace(".", string.Empty);
            var packageItems = new List<DocumentationItem>();

            foreach (var file in files) {
                var content = File.ReadAllText(file);
                var items = Documentation.Parser.Parser.Parse(language, content, file, includeSourceCode);
                packageItems.AddRange(items);
            }

            // Missing Namespace Elements
            if (generateMissingNamespaceItems == true) {
                GenerateMissingNamespaceItems(packageItems);
            }

            var package = new DocumentationPackage();

            // Meta
            package.License = ReadMetaFile(path, "license");
            package.Link = ReadMetaFile(path, "link");
            package.Credits = ReadMetaFile(path, "credits");
            package.Path = path;
            package.Language = language;
            package.Date = DateTime.UtcNow;
            package.Published = published;

            // Name
            if (string.IsNullOrEmpty(name) == true) {
                package.Name = System.IO.Path.GetFileName(path);
            } else {
                package.Name = name;
            }


            // Items
            package.Items = packageItems;
            package.ItemsCheck = packageItems.Count;

            return package;
        }

        /// <summary>
        /// Generates the missing namespace and classes items 
        /// </summary>
        /// <param name="packageItems">The package items.</param>
        //private static void GenerateMissingNamespaceItemsold(List<DocumentationItem> packageItems) {
        //    var missingNamespaceItems = new List<DocumentationItem>();
        //    foreach (var item in packageItems.Where(pi=>Parser.Parser.IsStructure (pi.Type)).GroupBy(i => i.Namespace)) {
        //        var nsParts = Parser.ParserHelper.GetNSParts(item.Key);
        //        string currentNs = "";
        //        foreach (var nspart in nsParts) {
        //            currentNs += nspart;
        //            var currentnsParts = Parser.ParserHelper.GetNSParts(currentNs);
        //            string parentNs = string.Empty;
        //            if (currentnsParts.Count > 1) {
        //                parentNs = string.Join(".", currentnsParts.Take(currentnsParts.Count - 1));
        //            }

        //            var existingNSItem = packageItems.FirstOrDefault(i => i.Type == Parser.Parser.TYPE_NAMESPACE && i.Name == currentNs);
        //            var existingClassItem = packageItems.FirstOrDefault(i => Parser.Parser.IsStructure( i.Type) && i.FullName == currentNs);
        //            var existingFromNewOnes = missingNamespaceItems.FirstOrDefault(i =>  i.FullName == currentNs);

        //            if (existingNSItem == null && existingFromNewOnes == null && existingClassItem == null) {

        //                var missingNamespaceItem = new DocumentationItem();
        //                missingNamespaceItem.Name = nspart;
        //                missingNamespaceItem.FullName = currentNs;
        //                missingNamespaceItem.Namespace = parentNs;
        //                missingNamespaceItem.Type = Parser.Parser.TYPE_NAMESPACE;
        //                missingNamespaceItem.Generated = true;

        //                missingNamespaceItems.Add(missingNamespaceItem);

        //            }
        //            currentNs += ".";
        //        }
        //    }


        //    packageItems.AddRange(missingNamespaceItems);

        //    // For debugging duplicated items
        //    //var grouped = packageItems.GroupBy(ns => ns.FullName  + " " + ns.Type);
        //    //  && g.Count(p=>p.Generated == true) > 1
        //    //var duplicated = string.Join("\n", grouped.Where(g => g.Count() > 1).Select(d=>d.Key));
        //}



        private static void GenerateMissingNamespaceItems(List<DocumentationItem> packageItems) {
            var missingNamespaceItems = new List<DocumentationItem>();


            foreach (var item in packageItems.OrderBy(p => p.FullName)) {
                var nsParts = Parser.ParserHelper.GetNSParts(item.FullName);
                string currentNs = "";
                int level = 0;
                foreach (var nspart in nsParts) {
                    currentNs += nspart;
              
                    var currentnsParts = Parser.ParserHelper.GetNSParts(currentNs);
                    string parentNs = string.Empty;
                    if (currentnsParts.Count > 1) {
                        parentNs = string.Join(".", currentnsParts.Take(currentnsParts.Count - 1));
                    }

                    var existingNSItem = packageItems.FirstOrDefault(i => i.Type == Parser.Parser.TYPE_NAMESPACE && i.Name == currentNs);
                    var existingClassItem = packageItems.FirstOrDefault(i => Parser.Parser.IsStructure(i.Type) && i.FullName == currentNs);
                    var existingFromNewOnes = missingNamespaceItems.FirstOrDefault(i => i.FullName == currentNs);

                    if (existingNSItem == null && existingFromNewOnes == null && existingClassItem == null) {

                        var missingNamespaceItem = new DocumentationItem();
                        missingNamespaceItem.Name = nspart;
                        missingNamespaceItem.FullName = currentNs;
                        missingNamespaceItem.Namespace = parentNs;

                        // if item is namespace its container has to be also a namespace
                        if (item.Type == Parser.Parser.TYPE_NAMESPACE) {
                            missingNamespaceItem.Type = Parser.Parser.TYPE_NAMESPACE;
                            missingNamespaceItem.Generated = true;
                        // if item is a class its container has to be namespace (except todo nested classes)
                        } else if (Parser.Parser.IsStructure(item.Type)) {
                            // TODO: nested classes
                            missingNamespaceItem.Type = Parser.Parser.TYPE_NAMESPACE;
                            missingNamespaceItem.Generated = true;
                        } else if (level <  nsParts.Count -2) {
                            missingNamespaceItem.Type = Parser.Parser.TYPE_NAMESPACE;
                            missingNamespaceItem.Generated = true;
                        } else if (level == nsParts.Count -2)  {
                            missingNamespaceItem.Type = Parser.Parser.TYPE_CLASS;
                            missingNamespaceItem.Generated = true;
                        } else {
                            // this is a leave like a function or variable
                            continue;
                        }


                        missingNamespaceItems.Add(missingNamespaceItem);

                    }
                    currentNs += ".";
                    level++;
                }
            }


            packageItems.AddRange(missingNamespaceItems);
            
        }

        private static string ReadMetaFile(string path, string name) {
            var fileName = path + "/bundle." + name;
            if (File.Exists(fileName) == false) {
                return null;
            }
            var content = File.ReadAllText(fileName);
            return content;
        }

        /// <summary>
        /// Gets the direct child namespaces.
        /// </summary>
        /// <param name="name">The name.</param>
        public IEnumerable<DocumentationItem> GetChildNamespaceURLs(string name, bool recursive) {
            var namespaces =  this.Items
                .Where(i => i.NamespaceURL.StartsWith(name, StringComparison.InvariantCulture) == true && i.NamespaceURL!= name );
            if (recursive == false) {
                var level = name.Count(n => n == '-');
                namespaces = namespaces.Where(n => n.NamespaceLevel -1 == level);
            }
            return namespaces.DistinctBy(ns => ns.NamespaceURL);
        }

        /// <summary>
        /// Gets the items with the exact namespace
        /// </summary>
        public IEnumerable<DocumentationItem> GetItemsForNamesapceURL(string name) {
            return this.Items.Where(i => i.NamespaceURL == name);
        }

        public IEnumerable<DocumentationItem> GetItemsForNamespace(string name, bool includeNamespace = false) {

            IEnumerable<DocumentationItem> items = null;
            if (includeNamespace) {
                items = this.Items.Where(i => i.FullName == name || i.Namespace == name);
            } else {
                items = this.Items.Where(i => i.Namespace == name);
            }

            bool removeDuplicatePartialClasses = true;
            // REMOVE DUPLICATED PARTIAL CLASS ITEMS -> TODO: WHAT HAPPENS WITH THE SUMMERIES FOR EACH CLASS
            if (removeDuplicatePartialClasses == true) {
                var duplicates = items.Where(i => i.Type == Parser.Parser.TYPE_CLASS).GroupBy(i => i.FullName).SelectMany(g=>g.Skip(1));
                items = items.Where(i => duplicates.Contains(i) == false);
            }


            return items;
        }

        public IEnumerable<DocumentationItem> GetRootNamespaces() {
            // TODO: micha fix parsing etc instead of filtering out here
            var items = this.Items.Where(i => i.Namespace.Count(x => x == '.') == 0 && i.Namespace != "" && i.Namespace!= "undefined" && i.Namespace.StartsWith("$") == false);
            return items.DistinctBy(i => i.Namespace);
        }

        /// <summary>
        /// Writes the documentations to the Config.DocumentationFilesPath
        /// creates a new folder if not available yet
        /// </summary>
        /// <param name="documentations">The documentations.</param>
        /// <param name="path">The path.</param>
        /// <param name="emptyDir">if set to <c>true</c> it deletes all files in folder.</param>
        public static void WriteDocumentations(IEnumerable<DocumentationPackage> documentations, string path, bool emptyDir, string srcDir) {

          
            // Create if not existing
            if (Directory.Exists(path) == false) {
                Directory.CreateDirectory(path);
            }

            if (emptyDir) {
                var files = Directory.GetFiles(path);
                foreach (var file in files) {
                    File.Delete(file);
                }
            }

            foreach (var documentation in documentations) {
                var jobject = documentation.GetJSON(new FormBuilder(Forms.Frontend.JSON));
                var data = Core.JSON.Serialize(jobject, true, false, true);


                //var namePrefix = documentation.Path.Replace(srcDir,string.Empty).Replace("\\","_").TrimStart('_');
                var namePrefix = System.IO.Path.GetFileNameWithoutExtension(documentation.Path);

                File.WriteAllText(path + System.IO.Path.DirectorySeparatorChar+ namePrefix+ "_" +  documentation.Language + ".json", data);
            }

        }

        public override string ToString() {
            if (string.IsNullOrEmpty(this.Name) == false) {
                return this.Name;
            }
            return base.ToString();
        }

        /// <summary>
        /// Gets all availble documentations from cms, and file system and caches them
        /// currently from CMS
        /// </summary>
        public static IEnumerable<DocumentationPackage> GetDocumentations(ModelContext db, bool forceReload = false) {
            lock (_packagesLock) {
                if (_packages == null || forceReload) {

                    var cmsFiles = new List<DocumentationPackage>();
                    var cmsPagesFiles = new List<DocumentationPackage>();
                    var fileSytem = new List<DocumentationPackage>();

                    try {
                        cmsFiles =  DocumentationPackage.ReadFromCMS(db, Config.DocumentationCMSPath).ToList();
                    } catch { }

                    try {
                        cmsPagesFiles = DocumentationPackage.ReadFromCMSPages(db, Config.DocumentationCMSPath).ToList();
                    } catch { }


                    try {
                        fileSytem = DocumentationPackage.ReadFromFileSystem().ToList();
                    } catch { }

                    var all = cmsFiles.Concat(cmsPagesFiles).Concat(fileSytem);
                    _packages = all;
                }
                return _packages;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<DocumentationPackage> ReadFromFileSystem() {
            var files = Directory.GetFiles(Core.Config.StaticPath + Core.Config.PathSep + "documentation" +Core.Config.PathSep +"packages","*.json");
            foreach (var file in files) {
                yield return ReadFromFile(file);
            }
        }

        [OnApplicationStartup]
        public static void OnApplicationStartup() {
            Core.Caching.CacheReset.CacheResetEvent += ReloadDocumentations;
        }

        private static void ReloadDocumentations() {
            using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                var documentations = GetDocumentations(db, true);
            }
        }

        private static IEnumerable<DocumentationPackage> _packages = null;
        private static object _packagesLock = new object();
    }


}
