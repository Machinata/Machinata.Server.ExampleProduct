using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Machinata.Core.Cards;
using Machinata.Core.Builder;
using static Machinata.Core.Model.ScheduledTask;

namespace Machinata.Core.TaskManager {

    public class TaskManagerMeta : Model.ModelObject {

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string State {
            get {
                return TaskManager.State.ToString();
            }
        }

        public override CardBuilder VisualCard() {
            var card = new Core.Cards.CardBuilder()
                .Type("TaskManager")
                .Icon("stopwatch")
                .Sub(this.State);
            if (TaskManager.State != TaskManager.TaskManagerState.Running) card.BlinkSub();
            return card;
        }

    }

    public class TaskManager {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public enum TaskManagerState {
            Stopping,
            Stopped,
            Starting,
            Running,
            Restarting,
            Aborting,
            Aborted
        }

        /// <summary>
        /// The state of the task manager, including transitional states.
        /// </summary>
        public static TaskManagerState State = TaskManagerState.Stopped;

        /// <summary>
        /// The schedular thread instance which runs in the background checking periodically if there is a new task to start.
        /// </summary>
        private static Thread _schedularThread = null;

        /// <summary>
        /// Internal variable to stop the schedular thread from running. This allows for gracefull stop without abort.
        /// </summary>
        private static bool _schedularThreadRequestStop = false;

        /// <summary>
        /// The process function for the schedule thread which runs until _schedularThreadRequestStop == true.
        /// </summary>
        private static void _schedularThreadProcess() {
            _logger.Info("Task Manager started");
            TaskManager.State = TaskManagerState.Running;

            while (!_schedularThreadRequestStop) {
                // Check to see if we have a task to run
                try {
                    TaskManager.CheckAndRunScheduledTasks();
                } catch (Exception e) {
                    // MessageCenter.SendMessageToAdminEmail("Error CheckAndRunScheduledTasks", e.StackTrace, "Machinata.Core");
                    EmailLogger.SendMessageToAdminEmail("Error CheckAndRunScheduledTasks", "Machinata.Core", null, null, e);
                }
                // Wait 
                Thread.Sleep(Core.Config.TaskManagerSchedularIntervalMS);
            }
            TaskManager.State = TaskManagerState.Stopped;
            _logger.Info("Task Manager stopped");
        }

        /// <summary>
        /// Starts the schedular thread. This should be called once on application start.
        /// </summary>

        public static void StartSchedular() {
            if (_schedularThread != null && _schedularThread.ThreadState == ThreadState.Running) return;

            _logger.Info("Starting Task Manager...");
            TaskManager.State = TaskManagerState.Starting;
            // reset the scheduled tasks if not properly set to waiting
            using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                foreach (var task in db.ScheduledTasks().Where(st => st.Enabled)) {
                    task.SetState(ScheduledTask.ScheduledTaskState.Waiting);
                }
                TaskManager.ActiveTaskList.Clear();
            }

            _schedularThreadRequestStop = false;
            _schedularThread = new Thread(_schedularThreadProcess);
            _schedularThread.Start();
        }

        /// <summary>
        /// Gracefully stops the schedular thread.
        /// </summary>
        public static void StopSchedular() {
            _logger.Info("Stopping Task Manager...");
            TaskManager.State = TaskManagerState.Stopping;
            _schedularThreadRequestStop = true;
        }

        /// <summary>
        /// Restarts the schedular thread after trying to gracefully stop it. If the thread does not stop in time,
        /// it will be aborted.
        /// </summary>
        public static void RestartSchedular() {
            _logger.Info("Restarting Task Manager...");
            TaskManager.State = TaskManagerState.Restarting;
            if (_schedularThread != null) {
                // If the thread exists, try to gracefully kill it
                _schedularThreadRequestStop = true;
                //_schedularThread.Interrupt();
                // Kill (abort) on timeout
                if (!_schedularThread.Join(4000)) {
                    _logger.Info("Aborting Task Manager...");
                    TaskManager.State = TaskManagerState.Aborting;
                    _schedularThread.Abort();
                    TaskManager.State = TaskManagerState.Aborted;
                }
                _schedularThread = null;
            }
            // Create a new instance and start it
            StartSchedular();
        }

        /// <summary>
        /// Validates that the schedular thread is running properly and restarts if needed.
        /// </summary>
        public static void ValidateSchedularAndRestartIfNeeded() {
            // Check if there is a reason to restart
            bool doRestart = false;
            string restartReason = "";
            if (_schedularThread == null) {
                doRestart = true;
                restartReason = "_schedularThread is null";
            } else if (!_schedularThread.IsAlive) {
                doRestart = true;
                restartReason = "!_schedularThread.IsAlive";
            } else if (_schedularThread.ThreadState == ThreadState.Aborted) {
                doRestart = true;
                restartReason = "_schedularThread.ThreadState == ThreadState.Aborted";
            } else if (_schedularThread.ThreadState == ThreadState.Stopped) {
                doRestart = true;
                restartReason = "_schedularThread.ThreadState == ThreadState.Stopped";
            } else if (_schedularThread.ThreadState == ThreadState.Unstarted) {
                doRestart = true;
                restartReason = "_schedularThread.ThreadState == ThreadState.Unstarted";
            }
            if (doRestart) {

                // Restart
                try {
                    RestartSchedular();
                } catch (Exception e) {
                    MessageCenter.SendMessageToAdminEmail(
                       subject: $"TaskManager Error, Restart Fail",
                       content: "Could not restart the Task Manager Schedular Thread: " + restartReason + $"Exception: {e.ToString()} /n InnerException: /n " + e.InnerException?.ToString() + $"/n Message: {e.Message}",
                       senderPackage: "Machinata.Core");
                }
            }
        }

        /// <summary>
        /// The active task list.
        /// </summary>
        public static List<Task> ActiveTaskList = new List<Task>();

        /// <summary>
        /// The finished task list. 
        /// </summary>
        public static List<Task> FinishedTaskList = new List<Task>();

        /// <summary>
        /// Loads the scheduled tasks from CMS.
        /// TODO: only dan, micha should be able to change the tasks in cms
        /// </summary>
        public static List<ScheduledTask> GetScheduledTasks() {
            // Init
            var scheduledTaskList = new List<ScheduledTask>();
            using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                var environmentRole = Core.Config.Environment + "-" + Core.Config.Role;
                return db.ScheduledTasks().Where(st => st.Role == environmentRole).ToList();
            }

        }

      

        private static object _checkandRunLock = new object();
        private static IList<string> _notWorkingTasks = new List<string>();

        /// <summary>
        /// Checks the and runs any scheduled tasks that are due to run.
        /// In the live environment, this is invoked through the heartbeat from AWS on
        /// http://server/heartbeat (see HeartbeatHandler).
        /// This is more robust than a Thread, as it will fail safely back if the server
        /// is shutdown for any reason (like IIS killing the thread for recycling).
        /// To test run a scheduled task, you can invoke the /heartbeat call or go to the mechanic 
        /// in the admin panel.
        /// </summary>
        public static void CheckAndRunScheduledTasks() {
            // Loop all scheduled tasks finding ones that should be run
             lock (_checkandRunLock)
            {
                foreach (ScheduledTask scheduledTask in GetScheduledTasks()) {
                    if (scheduledTask.ShouldRun()) {

                        // Register
                        scheduledTask.SetState( ScheduledTask.ScheduledTaskState.Running);
                       
                        try {

                            // Will it fail? avoid trying to instantiate an nonexistent class more than once
                            if (_notWorkingTasks.Contains(scheduledTask.Name)) {
                                continue;
                            }

                            // Create task
                            Task task = TaskManager.CreateTask(scheduledTask.Name);
                            task.Setup(scheduledTask,"schedular");
                            TaskManager.RunTask(task);

                        } catch (Exception e) {

                            scheduledTask.SetLastRun(DateTime.UtcNow);
                            scheduledTask.SetState(ScheduledTask.ScheduledTaskState.Waiting);
                            SendErrorMessage(scheduledTask.Name,e);
                        }
                    }
                }

                // Clean up old run tasks
                int tasksToKeepInMemory = 30;
                while (FinishedTaskList.Count() > tasksToKeepInMemory + 1) {
                    FinishedTaskList.RemoveRange(0, tasksToKeepInMemory);
                }

                // Active tasks warnings breached?
                foreach (Task task in ActiveTaskList) {
                    if (task.State == Task.TaskState.Running && task.ActiveWorkThread != null) {
                        if (task.MaxWarnings > 0 && task.Warnings > task.MaxWarnings) {
                            task.Abort();
                        }
                    }
                }
            }
        }

        public static void SendErrorMessage(string taskName, Exception e) {
           MessageCenter.SendMessageToAdminEmail(
               subject: $"ScheduledTask Error: {taskName}",
               content: $"Exception: {e.ToString()} /n InnerException: /n " + e.InnerException?.ToString() + $"/n Message: {e.Message}",
               senderPackage: "Machinata.Module.Core");
        }

        /// <summary>
        /// Gets the available tasks through reflection.
        /// </summary>
        /// <param name="dll">The DLL.</param>
        /// <returns>A list of full type names to the task class.</returns>
        public static IList<Type> GetAvailableTasks() {
            // Use reflection to gather all tasks
            IList<Type> tasks = new List<Type>();
            var types = Core.Reflection.Types.GetMachinataTypes();
            foreach (Type type in types) {
                System.Type t = type;
                bool isRequestHandler = false;
                while (t.BaseType != null) {
                    if (t.ToString() == "Machinata.Core.TaskManager.Task") {
                        isRequestHandler = true;
                        break;
                    }
                    t = t.BaseType;
                }
                if (!type.IsAbstract && isRequestHandler) tasks.Add(type);
            }
            return tasks;
        }

        public static IEnumerable<Task> GetAvailableTaskInstances() {
            var tasks = TaskManager.GetAvailableTasks();
            var availableTasks = tasks.Select(t => TaskManager.CreateTask(t.FullName));
            return availableTasks.ToList();
        }

        public static IEnumerable<TaskType> GetAvailableTaskNames() {
            var tasks = GetAvailableTaskInstances().Select(t=> new TaskType { Name = t.Name});
            return tasks;
        }

        public static Task CreateInstance(Type type, Assembly assembly= null) {
            if (assembly == null) assembly = type.Assembly;
            var task = assembly.CreateInstance(type.FullName) as Core.TaskManager.Task;
            return task;
        }

        public static Task CreateTaskInstance(string typeName) {
            var task = GetAvailableTasks().SingleOrDefault(t => t.Name == typeName);
            if (task != null) {
                return CreateInstance(task);
            }
            return null;
        }

      

        /// <summary>
        /// Instantiates a task for the given full type name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="dll">The DLL to invoke the code from.</param>
        /// <returns></returns>
        /// <exception cref="Core.Util.BackendException">
        /// create-task-failed;Could not create the task '+name+': +e.Message
        /// or
        /// create-task-failed;Could not create the task '+name+'!
        /// </exception>
        public static Task CreateTask(string fullName) {
            Task task = null;
            try {
                var type = TaskManager.GetAvailableTasks().SingleOrDefault(t => t.FullName == fullName);
                task = type.Assembly.CreateInstance(type.FullName) as Task;
            } catch (Exception e) {
                if (!_notWorkingTasks.Contains(fullName)) {
                    _notWorkingTasks.Add(fullName);
                }
                throw new Exceptions.BackendException("create-task-failed", "Could not create the task '" + fullName + "': " + e.Message, e);
            }
            if (task == null) throw new Exceptions.BackendException("create-task-failed", "Could not create the task '" + fullName + "'!");
            return task;
        }

        /// <summary>
        /// Registers and runs a task within the assembly.
        /// </summary>
        /// <param name="task">The task.</param>
        public static void RunTask(Task task) {
            // Register in active list
            if (ActiveTaskList.Select(atl => atl.Name).Contains(task.Name)) {
                MessageCenter.SendMessageToAdminEmail("Task Error",$"Task: {task.Name} already in ActiveTaskList", "Machinata.Module.Core");
            }
            ActiveTaskList.Add(task);

            // Create thread for task
            Thread taskThread = new Thread(new ThreadStart(task.ProcessAndFinish));
            taskThread.Name = task.Name;
            task.ActiveWorkThread = taskThread;
            taskThread.Start();
        }

        /// <summary>
        /// Properly finishes a task.
        /// </summary>
        /// <param name="task">The task.</param>
        public static void TaskFinished(Task task) {
            task.ActiveWorkThread = null;
            ActiveTaskList.Remove(task);
            FinishedTaskList.Add(task);
        }

        /// <summary>
        /// Gets the task by unique identifier from either active or finished lists.
        /// </summary>
        /// <param name="guid">The unique identifier.</param>
        /// <returns></returns>
        public static Task GetTaskByGUID(string guid) {
            foreach (Task task in ActiveTaskList) {
                if (task.GUID == guid) return task;
            }
            foreach (Task task in FinishedTaskList) {
                if (task.GUID == guid) return task;
            }
            return null;
        }

        #region Handler Application Startup

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {
            if (Core.Config.TaskManagerEnabled) {
                TaskManager.StartSchedular();
            } else {
                _logger.Warn("Task manager is disabled!");
            }
        }

        #endregion
    }


    public class TaskType : ModelObject {

        public override string PublicId
        {
            get
            {
                return this.Name;
            }
        }


        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; }
    }
}
