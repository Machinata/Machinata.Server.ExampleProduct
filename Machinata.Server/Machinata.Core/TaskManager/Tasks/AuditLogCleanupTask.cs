using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;

namespace Machinata.Core.TaskManager.Tasks {

    public class AuditLogCleanupTask : Task {

        public override void Process() {

            // Check 
            if (Core.Config.AuditLogsAutoRemoveAfterDays < 0) {
                throw new Exception($"Please define AuditLogsAutoRemoveAfterDays");
            }

            // Audit Logs
            var auditLogs = this.DB.AuditLogs().AsQueryable();

            // Older Logs
            var timeSpan = new TimeSpan( 
                days: Core.Config.AuditLogsAutoRemoveAfterDays,
                hours: 0, 
                minutes: 0, 
                seconds: 0,
                milliseconds: 0
                );

            var minDate = DateTime.UtcNow - timeSpan;
            Log("Filter out AuditLogs newer than: " + minDate.ToString(Core.Config.DateFormat));
            auditLogs = auditLogs.Where(al => al.Created < minDate);
          

            // Check Skip Targets
            if (Core.Config.AuditLogsAutoRemoveSkipTargets.Count == 0) {
                throw new Exception($"Please define AuditLogsAutoRemoveSkipTargets");
            }

            Log("Filter out AuditLogs with Targets: " + string.Join(", ", Core.Config.AuditLogsAutoRemoveSkipTargets));
            auditLogs = auditLogs.Where(al => Core.Config.AuditLogsAutoRemoveSkipTargets.Contains(al.TargetType) == false);

            var auditLogsGroups = auditLogs.GroupBy(al => al.TargetType);
            int count = 0;
            foreach( var auditlogGroup in auditLogsGroups) {
                Log($"Found '{auditlogGroup.Key}' AuditLogs: {auditlogGroup.Count()}.");
                foreach(var auditLog in auditlogGroup) {
                    Log($"Deleting AuditLog '{auditLog.Target}' Created: {auditLog.Created.ToString(Core.Config.DateFormat)}.");
                    this.DB.DeleteEntity(auditLog);
                    count++;
                }
                // Save per Target
                this.DB.SaveChanges();
            }

            Log($"Deleted {count} AuditLogs.");
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }
    }
}
