using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;

namespace Machinata.Core.TaskManager.Tasks {

    public class ContentCleanupTask : Task {

        public override void Process() {

            int deleted = 0;
            int total = this.DB.ContentFiles().Count();
            var contentFiles = this.DB.ContentFiles().OrderBy(e => e.Id).ToList(); // we must cache this unfortunately to iterate it safely...
            for (int i = 0; i < total; i++) {

                // Get the content file
                try {
                    var cf = contentFiles.Skip(i).FirstOrDefault();
                    if(cf == null) continue;

                    // Is it content node?
                    if (cf.Source == nameof(ContentNode)) {
                        // Is it used somewhere?
                        string url = cf.ContentURL;
                        var nodeCount = this.DB.ContentNodes().Where(cn => 
                            cn.Value == url 
                            || 
                            (cn.Value != null && cn.Value.Contains(url))
                            ||
                            (cn.Options.Data != null && cn.Options.Data.Contains(url))
                        ).Count();
                        if (nodeCount == 0) {
                            this.Log($"Content File {cf.FileName} ({cf.ContentURL}) unused, will delete...");
                            try {
                                Data.DataCenter.DeleteFile(this.DB, cf);
                                this.DB.SaveChanges();
                            } catch(Exception deleteException) {
                                throw deleteException;
                            } finally {
                                deleted++;
                            }
                        }
                    }
                } catch(Exception e) {
                    LogWithWarning("Error", e);
                }

                RegisterProgress(i, total);
            }

            Log($"Deleted {deleted} files.");
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }
    }
}
