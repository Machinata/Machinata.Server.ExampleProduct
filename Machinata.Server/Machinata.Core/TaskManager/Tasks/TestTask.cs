using Machinata.Core.Messaging;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.TaskManager.Tasks {

    public class TestTask : Task {
        public override void Process() {

            int count = 60;
            for (int x = 0; x < count; ++x) {
                LogFormat("Item: {0}, TestMode = {1}", x, this.TestMode);
                System.Threading.Thread.Sleep(300);
                RegisterProgress(x, count);
            }

        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            return ScheduledTaskConfig.GetDefault();
        }
    }
}
