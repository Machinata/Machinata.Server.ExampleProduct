using Machinata.Core.Messaging;
using Machinata.Core.TaskManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;

namespace Machinata.Core.TaskManager.Tasks {

    public class TestBatchTask : BatchTask<TestItem> {
        public override IEnumerable<TestItem> GetAllItems(string taskId) {
            int count = 10000;
            var items = new List<TestItem>();
            for (int x = 0; x < count; ++x) {
                items.Add(new TestItem() { BatchTaskItemId = taskId + "-" + x.ToString() });
            }
            return items;
        }

        
        public override object Process(string taskId, TestItem item) {
          //  int id = int.Parse( item.BatchTaskItemId.Replace("Test-", ""));
            System.Threading.Thread.Sleep(3333);
            return null;
        }

        protected override int BatchSize
        {
            get
            {
                return 10; 
            }

            set
            {
                base.BatchSize = value;
            }
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            return ScheduledTaskConfig.GetDefault();
        }

        /// <summary>
        /// Initializes this instance for Testing batchtasks
        /// </summary>
        public override void Initialize() {
            this.DB = Core.Model.ModelContext.GetModelContext(null);
            this.Start(Guid.NewGuid().ToString());
            this.DB.SaveChanges();
        }


        public override TestItem GetProcessedTypedItem(string taskId, string id, IEnumerable<TestItem> allItems) {
            return new TestItem() { BatchTaskItemId = id };
        }

        public override string GetTitle(string taskId) {
            return $"Test Task: {taskId}";
        }
    }

    public class TestItem : BatchTaskItem {
        public string BatchTaskItemId { get; set; }
        public string BatchTaskItemTitle { get { return this.BatchTaskItemId; } }
    }
}
