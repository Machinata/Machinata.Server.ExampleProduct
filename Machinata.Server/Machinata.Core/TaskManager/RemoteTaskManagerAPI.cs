using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.TaskManager {

    /// <summary>
    /// Helper class to access remote TasksManager features
    /// </summary>
    public class RemoteTaskManagerAPI {

        #region Lock

        private static object _assignmentLock = new object();

        #endregion

        /// <summary>
        /// Gets the list of tasks for a given target (MachineId or '*')
        /// Slave has to continue running or assigned tasks before starting one in waiting status
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="target">The target.</param>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        public static IOrderedQueryable<RemoteTask> GetTaskList(ModelContext db, string target) {

            UpdateKnownTargets(target);

            return GetRemoteTasks(
                    db, 
                    target, 
                    RemoteTask.RemoteTaskState.Waiting,
                    RemoteTask.RemoteTaskState.Assigned,
                    RemoteTask.RemoteTaskState.Running,
                    RemoteTask.RemoteTaskState.Canceling,
                    RemoteTask.RemoteTaskState.Paused
                    );
        }

        

        /// <summary>
        /// A client has to aqcuire a specific task before working on it
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="publicId">The public identifier.</param>
        /// <param name="target">The target.</param>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        public static RemoteTask AqcuireRemoteTask(ModelContext db, string publicId, string target) {
            lock (_assignmentLock) {

                var remoteTask = GetRemoteTasks(db, target, RemoteTask.RemoteTaskState.Waiting).GetByPublicId(publicId);

                if (remoteTask != null) {
                    remoteTask.State = RemoteTask.RemoteTaskState.Assigned;
                    db.SaveChanges();

                }
                return remoteTask;
            }
        }

        /// <summary>
        /// Slave has to inform master about the remote tasks, status changes eta etc
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="publicId">The public identifier.</param>
        /// <param name="target">The target.</param>
        /// <param name="role">The role.</param>
        /// <param name="state">The state.</param>
        /// <param name="eta">The eta.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static RemoteTask UpdateTask(ModelContext db, string publicId, string target, string role, string state, string eta, string message) {
            lock (_assignmentLock) {

                // Find the task
                var task = GetRemoteTasks
                                (
                                    db,
                                    target,
                                    RemoteTask.RemoteTaskState.Assigned,
                                    RemoteTask.RemoteTaskState.Waiting,
                                    RemoteTask.RemoteTaskState.Running,
                                    RemoteTask.RemoteTaskState.Canceling
                                )
                                .GetByPublicId(publicId);

            
                // State
                var oldState = task.State;
                var stateFromClient =  EnumHelper.ParseEnum<RemoteTask.RemoteTaskState>(state);
       
                const string ETA_KEY = "eta";

                // Finished
                if (stateFromClient == RemoteTask.RemoteTaskState.Finished) {
                    if (oldState == RemoteTask.RemoteTaskState.Running){
                        task.StatusInfos[ETA_KEY] = "0";
                        task.Finished = DateTime.UtcNow;
                        task.State = stateFromClient;
                    } 

                }

                // Canceled
                if (oldState == RemoteTask.RemoteTaskState.Canceling && stateFromClient == RemoteTask.RemoteTaskState.Canceled) {
                    task.StatusInfos[ETA_KEY] = "0";
                    task.Finished = DateTime.UtcNow;
                    task.State = stateFromClient;
                }

                // Running  
                if (stateFromClient == RemoteTask.RemoteTaskState.Running) {
                    if (oldState == RemoteTask.RemoteTaskState.Assigned) {
                        task.Started = DateTime.UtcNow;
                        task.State = stateFromClient;
                    } else {
                        task.StatusInfos[ETA_KEY] = eta;
                    }
                }

                // Error
                if (stateFromClient == RemoteTask.RemoteTaskState.Error) {
                    if (task.State == RemoteTask.RemoteTaskState.Running || task.State == RemoteTask.RemoteTaskState.Assigned) {
                        task.State = stateFromClient;
                    }
                }

                // todo list of messages?
                if (!string.IsNullOrWhiteSpace(message)) {
                    task.StatusInfos["message"] = message;
                }

                return task;
            }
        }

        /// <summary>
        /// Gets the remote tasks for api
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="role">The role.</param>
        /// <param name="states">The states.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">target-needed;Target has to be defined</exception>
        public static IOrderedQueryable<RemoteTask> GetRemoteTasks(ModelContext db, string target, params RemoteTask.RemoteTaskState[] states) {
            if (string.IsNullOrEmpty(target)) {
                throw new BackendException("target-needed", "Target has to be defined");
            }

            var remoteTasks = db.RemoteTasks().
                                Where(st =>
                                    st.Enabled == true &&
                                        (st.Target == target || st.Target == "*"));
            var result = new List<RemoteTask>();
            foreach (var state in states) {
                result.AddRange(remoteTasks.Where(rt => rt.State == state));
            }
            return result.AsQueryable().OrderBy(st => st.Id);

        }

        /// <summary>
        /// Gets the remote task targets from configuration.
        /// </summary>
        /// <value>
        /// The remote task targets configuration.
        /// </value>
        public static IEnumerable<RemoteTaskTarget> RemoteTaskTargetsConfig
        {
            get
            {
                var key = "TaskManagerRemoteTargets";
                var targetsConfig = Core.Config.Dynamic.GetStringList(key, false);
                var targets = new List<RemoteTaskTarget>();
                foreach (var target in targetsConfig) {
                    if (target.Contains(":")) {
                        // todo task types per slave
                        var values = target.Split(':');
                        targets.Add(new RemoteTaskTarget { Name = values.First(),/* Type = values.Skip(1).First()*/ });

                    } else {
                        targets.Add(new RemoteTaskTarget  {  Name = target  } );
                    }

                }
                return targets;
            }

        }

        private static List<RemoteTaskTarget> _knownTargets = new List<RemoteTaskTarget>();

        /// <summary>
        /// Targets from which we have a sign of life
        /// </summary>
        /// <value>
        /// The known targets.
        /// </value>
        public static IEnumerable<RemoteTaskTarget> KnownTargets
        {
            get
            {
                return _knownTargets;
            }
        }
                
        public static IEnumerable<RemoteTaskTarget> GetRemoteTaskTargetsConfig(bool includingAsterisk= true)
        {
            var targets = RemoteTaskTargetsConfig.ToList();
            if (includingAsterisk) {
                targets.Add(new RemoteTaskManagerAPI.RemoteTaskTarget() { Name = "*" });
            }
            return targets;
        }

        /// <summary>
        /// UI Helper class to show status of target/slaves
        /// </summary>
        /// <seealso cref="Machinata.Core.Model.ModelObject" />
        public class RemoteTaskTarget : ModelObject {
          //  [FormBuilder(Forms.Admin.LISTING)]
            public string Name { get; set; }

            [FormBuilder(Forms.Admin.LISTING)]
            public string Title { get { return this.Name == "*" ? "{text.any}" : this.Name; } }

            [FormBuilder(Forms.Admin.LISTING)]
            public DateTime? LastSignOfLife { get; set; }

            [FormBuilder(Forms.Admin.LISTING)]
            public bool Unknown { get; set; } = false;

            // todo filter types only enabled task types per slave
            [FormBuilder(Forms.Admin.LISTING)]
            public IEnumerable<string> Types { get; set; }

            [FormBuilder(Forms.Admin.LISTING)]
            public bool Healthy
            {
                get {
                    return  this.LastSignOfLife.HasValue && DateTime.UtcNow - this.LastSignOfLife < new TimeSpan(0, 1, 0);
                }
            }

            public override string PublicId
            {
                get
                {
                    return this.Name;
                }
            }

            public string StyleClass { get { return ! this.Healthy ? "warning" : "good"; } }
        }


        private static void UpdateKnownTargets(string target) {

            var registered = (_knownTargets.SingleOrDefault(kt => kt.Name == target));
            if (registered != null) {
                registered.LastSignOfLife = DateTime.UtcNow;
            } else {
                var known = RemoteTaskTargetsConfig.SingleOrDefault(t => t.Name == target);
                if (known != null) {
                    _knownTargets.Add(known);
                    known.LastSignOfLife = DateTime.UtcNow;
                } else {
                    var unknown = new RemoteTaskTarget();
                    unknown.Name = target;
                    unknown.Unknown = true;
                    unknown.LastSignOfLife = DateTime.UtcNow;
                    _knownTargets.Add(unknown);
                }
            }

        }
    }
}
