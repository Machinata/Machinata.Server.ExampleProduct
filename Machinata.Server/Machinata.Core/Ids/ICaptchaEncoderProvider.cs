using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids {

    public interface ICaptchaEncoderProvider {

        string RandomCode(int length = 6);
        string EncodeCode(string input);
        string DecodeCode(string input);
        bool ValidateCode(string encoded, string usercode, bool throwException = true);

    }
}
