using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;

namespace Machinata.Core.Ids.Providers {

    public class StandardSerialIdGenerator : ISerialIdProvider {
        
        public string GetSerialIdForEntity(ModelObject entity) {
            // Validate type number
            int typeNumber = entity.TypeNumber;
            if (typeNumber == ModelObject.MODELOBJECT_NO_TYPENUMBER) throw new Exception("This entity does not have a type number associated with it.");
            // Validate id
            if (entity.Id == ModelObject.MODELOBJECT_NO_ID) throw new Exception("The entity does not yet have a id assigned for a serial id.");
            // Validate date
            var date = entity.Created;
            if (date == null || date == DateTime.MinValue) throw new Exception("The entity does not yet have a date assigned for a rolling id.");
            // Compute serial id
            var serialId = string.Format($"{typeNumber.ToString("D2")}.{date.ToString("yy")}.{GetSerialId(entity).ToString("D5")}");
            return serialId;
        }

        public virtual int GetSerialId(ModelObject entity) {
            return entity.Id;
        }

    }

}
