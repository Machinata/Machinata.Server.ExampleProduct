using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids.Providers {

    public class StandardIdRoller : IIdRollerProvider {
        
        #region Interface Implementation

        public int RollId(int input) {
            return EncodeIdToInt(input);
        }
        public int UnrollId(int input) {
            return DecodeIdFromInt(input);
        }

        #endregion


        private static int _startingValue = 10000;
        private static int _primeMultiplier = 13;
        private static int _exponentFactor = 5;


        public static int EncodeIdToInt(int id) {
            return _startingValue + (id * _primeMultiplier)^_exponentFactor;
        }

        public static int DecodeIdFromInt(int obfuscatedId) {
            throw new NotImplementedException();
        }

    }
}
