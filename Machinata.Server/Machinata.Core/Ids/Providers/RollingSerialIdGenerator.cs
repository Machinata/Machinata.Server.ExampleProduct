using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;

namespace Machinata.Core.Ids.Providers {

    public class RollingSerialIdGenerator : StandardSerialIdGenerator {
        
        public override int GetSerialId(ModelObject entity) {
            return entity.RollingId;
        }

    }

}
