using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids {
    
    public class Obfuscator {
        public static IIdObfuscatorProvider Default = new Core.Ids.Providers.StandardIdObfuscator();
    }

    public class Roller {
        public static IIdRollerProvider Default = new Core.Ids.Providers.StandardIdRoller();
    }

    public class Captcha {
        public static ICaptchaEncoderProvider Default = new Core.Ids.Providers.StandardCaptchaEncoder();
    }

    public class Code {
        public static ICodeProvider Default = new Core.Ids.Providers.StandardCodeGenerator();
    }

    /// <summary>
    /// Provides serial id's for entities such as order or invoice numbers.
    /// </summary>
    public class SerialIdGenerator {
        public static ISerialIdProvider Default = Activator.CreateInstance(Type.GetType(Core.Config.ProviderSerialId)) as ISerialIdProvider;
    }
    
    
}
