using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids {

    public interface ISerialIdProvider {

        string GetSerialIdForEntity(Model.ModelObject entity);

    }
}
