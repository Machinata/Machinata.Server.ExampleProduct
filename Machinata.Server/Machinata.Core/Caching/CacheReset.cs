using Machinata.Core.Lifecycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Caching {
    public class CacheReset {

        // Cache Reseted
        public delegate void CacheResetDelegate();
        public static event CacheResetDelegate CacheResetEvent;

       
        public static void FireEvent() {
            CacheReset.CacheResetEvent?.Invoke();
        }
    }
}
