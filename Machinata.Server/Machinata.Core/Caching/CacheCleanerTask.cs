using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Caching {
    public class CacheCleanerTask : TaskManager.Task {
        public override void Process() {
            var cacheFolders = new List<DirectoryInfo>();

            Log("Scanning cache directories:");

            foreach (var path in Directory.EnumerateDirectories(Core.Config.CachePath)) {
                var dirInfo = new DirectoryInfo(path);
                cacheFolders.Add(dirInfo);
                Log($" - {dirInfo.Name}");
            }

            Log("Clean up:");

            foreach( var path in cacheFolders) {
                Log($" -{path.FullName}");
                int result = RemoveOldFiles(path, Core.Config.CacheFileMaxDays);

                Log($"Deleted {result} files");
            }
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        private int RemoveOldFiles(DirectoryInfo dir, int cacheFileMaxDays) {
            var allFiles = dir.GetFiles();
            var oldFiles = allFiles.Where(f => f.LastWriteTimeUtc < DateTime.UtcNow.AddDays(-cacheFileMaxDays));
            var deleteCount = 0;
            foreach (var file in oldFiles) {
             Log("Deleting " + file.FullName + " due to age limit");
                var result = Delete(file);
                if (result) { deleteCount++; }
            }
            return deleteCount;
        }

        private bool Delete(FileInfo file) {
            try {
                file.Delete();
                return true;
            } catch (Exception e) {
                LogWithWarning($"Could not delete: {file.FullName}", e);
                return false;
            }
        }
    }
}
