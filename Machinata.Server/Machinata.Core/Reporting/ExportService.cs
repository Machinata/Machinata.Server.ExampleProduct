using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Reporting {
   public class ExportService {
        public static byte[] ExportXLSX<T>(IEnumerable<T> entities, IEnumerable<PropertyInfo> propertiesToExport = null) {
         
            if (propertiesToExport == null) {
                propertiesToExport = typeof(T).GetProperties().AsEnumerable();
            }

            XLSX xlsx = new XLSX(typeof(T).Name);

            foreach (T entity in entities) {
                var row = new Dictionary<string, string>();
                foreach (var prop in propertiesToExport) {
                    row[prop.Name] = prop.GetValue(entity)?.ToString();
                }
                xlsx.AddRow(row);
            }

            return xlsx.GetContent();
        }

        public static byte[] ExportXLSX<T>(IEnumerable<T> entities, FormBuilder form) where T : ModelObject {

            XLSX xlsx = new XLSX(typeof(T).Name);

            foreach (T entity in entities) {
                var row = new Dictionary<string, string>();
                foreach (var prop in entity.GetPropertiesForForm(form)) {
                    if (prop.PropertyType == typeof(Properties)) {
                        var properties = prop.GetValue(entity) as Properties;

                        foreach (var property in properties.Keys) {
                            if (properties.Keys.Contains(property)) {
                                row[property] = properties[property]?.ToString();
                            } else {
                                row[property] = null;
                            }
                        }

                    }
                    else if (prop.PropertyType.IsSubclassOf(typeof(ModelObject))) {
                        var relatedObject = prop.GetValue(entity) as ModelObject;
                        if (relatedObject != null) {
                            foreach (var property in relatedObject.GetPropertiesForForm(form)) {
                                row[prop.GetFormName(form, false) + "_" + property.GetFormName(form, false)] = property.GetValue(relatedObject)?.ToString();
                            }
                        }

                    } else {
                        row[prop.GetFormName(form, false)] = prop.GetValue(entity)?.ToString();
                    }
                }
                xlsx.AddRow(row);
            }

            return xlsx.GetContent();
        }


        /// <summary>
        /// Imports a XLSX into a ModelObject using the given form
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content">The content.</param>
        /// <param name="form">The form, which can only contain one 'Properties' Property to which all additional fields will be exploaded </param>
        /// <param name="log">debug infos about import </param>
        /// <returns></returns>
        /// <exception cref="BackendException">not-supported;Only one Property field supported</exception>
        public static IEnumerable<T> ImportXLSX<T>(
            ModelContext db,
            byte[] content,
            FormBuilder form, 
            Action<T, ModelContext, Dictionary<string, string>, int> importRow = null,
            StringBuilder log = null,
            bool allowFormulas = false,
            int? saveChangesEvery = null

            ) where T : ModelObject {
            var stream = new MemoryStream(content);
            return ImportXLSX<T>(db, stream,form, importRow, log, allowFormulas, saveChangesEvery);
        }

        public static IEnumerable<T> ImportXLSX<T>(
            ModelContext db, 
            Stream input, 
            FormBuilder form, 
            Action<T, ModelContext, Dictionary<string, string>, int> importRow = null ,
            StringBuilder log = null,
            bool allowFormulas = false,
            int? saveChangesEvery = null

            ) where T: ModelObject {

            XLSX xlsx = new XLSX(input, allowFormulas);
            int nRow = 0;
            var result = new List<T>();

            if (log == null) {
                log = new StringBuilder();
            }

            log.AppendLine("-----------------------------------------------------------------------------");
            log.AppendLine("Importing Worksheet: " + xlsx.Name);
            log.AppendLine("-----------------------------------------------------------------------------");

            foreach (var row in xlsx.GetRows()) {

                log.AppendLine("Importing Row: " + (nRow+1));
                log.AppendLine(string.Join(",",row.Values));
                T entity = null;

                try {
                    entity = (T)Activator.CreateInstance(typeof(T));
                    var formProperties = entity.GetPropertiesForForm(form);

                    if (formProperties.Count(fp => fp.PropertyType == typeof(Properties)) > 1) {
                        throw new BackendException("not-supported", "Only one Property field supported");
                    }
                    
                    foreach (var property in formProperties) {
                        if (property.PropertyType == typeof(Properties)) {
                            var propertyValues = row.Where(r => !formProperties.Select(fp => fp.Name.ToLowerInvariant()).Contains(r.Key.ToLowerInvariant())).ToList();
                            var properties = new Properties();
                            foreach (var columns in propertyValues) {
                                properties[columns.Key] = columns.Value;
                                row.Remove(columns.Key);
                            }
                            row[property.Name] = properties.ToString();
                        } else if (property.PropertyType == typeof(ContentNode)) {
                            log.AppendLine("Ignoring ContentNode Column: " + property.Name);
                            form.Exclude(property.Name);
                        }
                    }
                    entity.Populate(new PopulateProvider(row.ToDictionary(d => d.Key.ToLower(), e => e.Value), null), form);

                    // Additional imports for this row
                    importRow?.Invoke(entity, db, row, nRow);

                    // Add to result
                    result.Add(entity);

                    // Save Batch?
                    if (saveChangesEvery.HasValue && nRow > 0 && (nRow % saveChangesEvery == 0)) {
                        db.SaveChanges();
                    }

                } catch (Exception e) {
                    throw new BackendException("import-error", "Could not import row number: " + (nRow + 1) + $"{row.ToString(';')}", e);
                }

                nRow++;

            }

            log.AppendLine("-----------------------------------------------------------------------------");
            log.AppendLine($"Importing complete:  " + xlsx.Name + ", Rows: " + nRow);
            log.AppendLine("-----------------------------------------------------------------------------");

            return result;

        }

    

        /// <summary>
        /// Imports ContentNode Columns for existing Entities which already have PublidId
        /// Supports suffix in column name: Description_de, without suffix it will be imported to 'default' language node
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="form">The form.</param>
        /// <param name="db">The database.</param>
        /// <exception cref="BackendException">import-error;Could not import row numer:  + nRow</exception>
        public static void ImportXLSXContentNodes<T>(IEnumerable<T> entities, Stream stream, FormBuilder form, ModelContext db) where T : ModelObject {
            XLSX xlsx = new XLSX(stream, false);
            int nRow = 0;
         
            foreach (var row in xlsx.GetRows()) {
                T entity = null;
                try {
                    entity = entities.ElementAt(nRow);
                    var formProperties = entity.GetPropertiesForForm(form);
                    var contentNodeValues = new Dictionary<string, Dictionary<string, string>>();

                    foreach (var property in formProperties) {
                        if (property.PropertyType == typeof(ContentNode)) {
                            var columnsForProperty = row.Keys.Where(k => k.StartsWith(property.Name, StringComparison.Ordinal));
                            foreach (var columnForProperty in columnsForProperty) {
                                string language = "default";
                                if (columnForProperty.Contains("_")){
                                    var splits = columnForProperty.Split('_');
                                    if (splits.Count() > 1 && splits[1].Length == 2) {
                                        language = splits.Skip(1).First().ToLowerInvariant();
                                    }
                                }
                               
                                if (!contentNodeValues.ContainsKey(property.Name)) {
                                    contentNodeValues[property.Name] = new Dictionary<string, string>();
                                }

                                contentNodeValues[property.Name][language] = row[columnForProperty];
                            }
                        }
                    }

                    // Create nodes
                    foreach (var contentNodeValue in contentNodeValues) {
                        var node = ContentNode.CreateContentNodeForEntityProperty(db, entity, contentNodeValue.Key, contentNodeValue.Value, ContentNode.NODE_TYPE_HTML);
                        node.Name = contentNodeValue.Key;
                        var property = entity.GetProperties().First(p => p.Name == contentNodeValue.Key);
                        property.SetValue(entity, node);
                        db.ContentNodes().Add(node);
                    }
                    
                } catch (Exception e) {
                    throw new BackendException("import-error", "Could not import row numer: " + nRow, e);
                }
                nRow++;

            }
           
        }



        public static byte[] ExportPDF(string html, string css = "") {
            var pdf = new PDF.PDFDocument();
            pdf.Content = html;
            pdf.CSS = css;
            return pdf.GetContent();
        }
        


    }
}
