using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Core.Handler {

    /// <summary>
    /// Helper class that abstracts the concept of events for a singe request lifecylce.
    /// Provides basic (shim) support for triggers of different types.
    /// Observers are currently not supported. One must manually pull to see
    /// if a trigger was called.
    /// </summary>
    public partial class HandlerEvents {

        #region Event IDs
        
        public const string HANDLER_EVENT_CURRENCY_CHANGED = "HANDLER_EVENT_CURRENCY_CHANGED";
        public const string HANDLER_EVENT_LANGUAGE_CHANGED = "HANDLER_EVENT_LANGUAGE_CHANGED";

        #endregion

        
        #region Private Fields
        
        private List<string> _triggeredEvents = null;

        #endregion

        #region Public Methods
        
        public void Trigger(string eventId) {
            if (_triggeredEvents == null) _triggeredEvents = new List<string>();
            _triggeredEvents.Add(eventId);
        }

        public bool DidTrigger(string eventId) {
            if (_triggeredEvents == null) return false;
            return _triggeredEvents.Contains(eventId);
        }

        #endregion


    }
}
