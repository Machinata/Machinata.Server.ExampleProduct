
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Builder;

namespace Machinata.Core.Handler {


    /// <summary>
    /// An extended handler for processing page templates. This handler automatically provides
    /// a template context based on the called route and will automatically return the output
    /// when the method finishes. A PageTemplateHandler always has a single extension type (such as
    /// HTML) defined.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public abstract partial class PDFPageTemplateHandler : PageTemplateHandler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        #region Private Fields
        
        protected string CSS { get; set; }
        protected string FileName { get; set; }
        protected string ContentFormat { get; set; } = "pdf";

        public double MarginLeft = Core.Config.PDFPageMarginLeft;
        public double MarginRight = Core.Config.PDFPageMarginRight;
        public double MarginTop = Core.Config.PDFPageMarginTop;
        public double MarginBottom = Core.Config.PDFPageMarginBottom;
        /// <summary>
        /// The page width in mm
        /// </summary>
        public double PageWidth = Core.Config.PDFPageWidth;
        /// <summary>
        /// The page height in mm
        /// </summary>
        public double PageHeight = Core.Config.PDFPageHeight;
        
        public string HeaderLeft = null;
        public string HeaderCenter = null;
        public string HeaderRight = null;
        public string FooterLeft = null;
        public string FooterCenter = null;
        public string FooterRight = null;

        public string CoverPage = null;
        public string TrailerPage = null;

        public string TemplateGuideImage = null;
        
        /// <summary>
        /// If greater than 1, the PDF document is automatically duplicated N (Copies) times.
        /// </summary>
        public int Copies = 1;
        
        #endregion

        
        #region Private Methods
        
        private string _parseHeadTag(PageTemplate template, string name) {
            var matches = Regex.Matches(template.Data.ToString(), "<"+name+">([^<]*)<\\/"+name+">", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var ret = "";
            if (matches != null && matches.Count > 0) {
                foreach (var match in matches) {
                    ret += ((Match)match).Groups[1].Value;
                }
                return ret;
            } else {
                return null;
            }
        }

        private string _parseMetaTag(PageTemplate template, string name, string defaultValue) {
            var match = Regex.Match(template.Data.ToString(), "<meta name=\""+name+"\" content=\"([^</]*)\" ?\\/>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (match != null && match.Groups.Count > 1) {
                return match.Groups[1].Value;
            } else {
                return defaultValue;
            }
        }
        
        private double _parseMetaTag(PageTemplate template, string name, double defaultValue) {
            return double.Parse(_parseMetaTag(template, name, defaultValue.ToString()));
        }

        private string _loadPDFTemplate(string templateName) {
            var template = PageTemplate.LoadForTemplateContextAndName(this.Template, templateName, AllowUndefinedTemplates);
            template.InsertTextVariables();
            return template.Content;
        }

        #endregion

        #region Public Methods
        
        public void RemoveAllHeaders() {
            this.HeaderLeft = null;
            this.HeaderCenter = null;
            this.HeaderRight = null;
        }

        public void RemoveAllFooters() {
            this.FooterLeft = null;
            this.FooterCenter = null;
            this.FooterRight = null;
        }

        /// <summary>
        /// Generates the PDF date for route.
        /// Shortcut method that automatically creates the handler and invokes the PDF templating system...
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns></returns>
        public static byte[] GeneratePDFDateForRoute(string route, Model.User user) {
            // Create the appropriate handler
            var handler = Core.Handler.Handler.CreateForRoute<PDFPageTemplateHandler>(routePath:route, language: null, verb: Verbs.Any, user: user);
          
           
            handler.Invoke();
            handler.InsertCommonVariables();
            // Return pdf bytes
            return handler.GeneratePDFData();
        }

        /// <summary>
        /// Generates the PDF data by creating a PDF document and setting it up using the handler's templates.
        /// </summary>
        /// <returns></returns>
        public byte[] GeneratePDFData() {
            var pdf = new PDF.PDFDocument();
            pdf.Content = this.Template.Content;
            pdf.CSS = this.CSS;
            pdf.MarginBottom = this.MarginBottom;
            pdf.MarginLeft = this.MarginLeft;
            pdf.MarginRight = this.MarginRight;
            pdf.MarginTop = this.MarginTop;
            pdf.PageWidth = this.PageWidth;
            pdf.PageHeight = this.PageHeight;
            pdf.CoverPage = this.CoverPage;
            pdf.TrailerPage = this.TrailerPage;
            pdf.HeaderLeft = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.HeaderLeft, this.Language);
            pdf.HeaderCenter = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.HeaderCenter, this.Language);
            pdf.HeaderRight = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.HeaderRight, this.Language);
            pdf.FooterLeft = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.FooterLeft, this.Language);
            pdf.FooterCenter = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.FooterCenter, this.Language);
            pdf.FooterRight = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, this.FooterRight, this.Language);
            pdf.TemplateGuideImage = this.TemplateGuideImage;
            pdf.Copies = this.Copies;
            return pdf.GetContent();
        }

        /// <summary>
        /// Generates the PNG data for a PDF.
        /// This method will create the PDF data, write it to disk, then convert to PNG and read back to disk.
        /// This method does not perform any cache and does not leave any data on disk.
        /// </summary>
        /// <returns></returns>
        public byte[] GeneratePNGData() {

            // Init
            byte[] pngBytes = null;
            var guid = Guid.NewGuid().ToString();
            string pdfFile = Core.Config.CachePath + Core.Config.PathSep + "pdf"+ Core.Config.PathSep + guid + ".pdf";
            string pngFile = Core.Config.CachePath + Core.Config.PathSep + "pdf"+ Core.Config.PathSep + guid + ".png";
            
            // Make sure cache dir exists
            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(pdfFile));
            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(pngFile));

            try {
                
                // Write PDF and save as image
                SaveToFile(pdfFile);
                Core.PDF.PDFConverter.SavePDFAsImage(pdfFile, pngFile);

                // Read bytes
                pngBytes = File.ReadAllBytes(pngFile);

            } finally {
                // Cleanup
                if(File.Exists(pdfFile)) File.Delete(pdfFile);
                if(File.Exists(pngFile)) File.Delete(pngFile);
            }

            // Return
            return pngBytes;
        }
        
        public string SaveToFile(string filename) {
            string filepath = filename;
            var bytes = GeneratePDFData();
            File.WriteAllBytes(filepath, bytes);
            return filepath;
        }

        public void SetFileName(string filename) {
            this.FileName = filename;
        }
        #endregion

        #region Handler Virtual Implementations
        
        public override string PackageName {
            get {
                return "Machinata.PDF";
            }
        }

        /// <summary>
        /// Gets a value indicating whether the handler will allow undefined templates.
        /// This is useful for some specific handlers, for example a PDF handler, where you don't
        /// need the full template complexity and just want to use blank templates.
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow undefined templates]; otherwise, <c>false</c>.
        /// </value>
        public override bool AllowUndefinedTemplates {
            get {
                return true;
            }
        }

        public override ContentType GetDefaultContentType() {
            return ContentType.StaticFile;
        }

        public override void SetupForContext(HttpContext context) {
            base.SetupForContext(context);
            if(Core.Config.PDFGetLanguageFromContext) {
                this.SetLanguageFromContext();
            }
        }

        public override void SetupForContentType() {

            if (Params.String("format") == "png") {
                this.ContentFormat = Params.String("format");
            }

            if (this.ContentType == ContentType.StaticFile) {
                // TemplatePage
                _template = PageTemplate.LoadForHandlerAndRoutePath(this, this.Route.GetRoutePathOrAliasIfSet(), this.AllowUndefinedTemplates);
                _template.Language = this.Language;
                _template.Handler = this;
                
                // Footer et cetera...
                this.HeaderLeft = _loadPDFTemplate("pdf.header-left");
                this.HeaderCenter = _loadPDFTemplate("pdf.header-center");
                this.HeaderRight = _loadPDFTemplate("pdf.header-right");
                this.FooterLeft = _loadPDFTemplate("pdf.footer-left");
                this.FooterCenter = _loadPDFTemplate("pdf.footer-center");
                this.FooterRight = _loadPDFTemplate("pdf.footer-right");
                // CSS
                _loadCSS();
            } else {
                base.SetupForContentType();
            }


        }

        private static object _loadCSSLock = new object();
        private void _loadCSS() {
            // Load the css bundle
            //TODO: @dan memory caching for the cache file?
            {
                // Load bundle
                string cachePath = Core.Bundles.Bundle.GetBundleCachePath(
                    db: this.DB,
                    packageName: this.PackageName,
                    bundlePath: "css/machinata-pdf-bundle.css",
                    language: this.Language,
                    themeName: this.ThemeName,
                    debug: this.Params.Bool("debug",false),
                    profile: this.Params.String("profile", null),
                    outputFormat: this.Params.String("format", null)
                );
                this.CSS = System.IO.File.ReadAllText(cachePath);
            }
        }

        private static object _testLock = new object();
        public override void Finish() {
            if(_template != null) {

                // Insert all the variables
                InsertCommonVariables();
                
                // Parse our page information
                this.PageWidth = _parseMetaTag(_template, "PageWidth", this.PageWidth);
                this.PageHeight = _parseMetaTag(_template, "PageHeight", this.PageHeight);
                this.MarginTop = _parseMetaTag(_template, "MarginTop", this.MarginTop);
                this.MarginBottom = _parseMetaTag(_template, "MarginBottom", this.MarginBottom);
                this.MarginLeft = _parseMetaTag(_template, "MarginLeft", this.MarginLeft);
                this.MarginRight = _parseMetaTag(_template, "MarginRight", this.MarginRight);
                this.HeaderLeft = _parseMetaTag(_template, "HeaderLeft", this.HeaderLeft);
                this.HeaderCenter = _parseMetaTag(_template, "HeaderCenter", this.HeaderCenter);
                this.HeaderRight = _parseMetaTag(_template, "HeaderRight", this.HeaderRight);
                this.FooterLeft = _parseMetaTag(_template, "FooterLeft", this.FooterLeft);
                this.FooterCenter = _parseMetaTag(_template, "FooterCenter", this.FooterCenter);
                this.FooterRight = _parseMetaTag(_template, "FooterRight", this.FooterRight);
                
                this.CoverPage = _parseMetaTag(_template, "CoverPage", this.CoverPage);
                if(!string.IsNullOrEmpty(this.Params.String("cover-text"))) {
                    var coverHTML = this.Params.String("cover-text") ;
                    coverHTML = coverHTML.Replace("\n", "<br/>");
                    coverHTML = coverHTML.Replace("\\\\", "<br/>");
                    this.CoverPage = $"<html><body class='cover'><div class='cover-content'>{coverHTML}</div></body></html>";
                }

                this.TrailerPage = _parseMetaTag(_template, "TrailerPage", this.TrailerPage);

                this.Copies = int.Parse(_parseMetaTag(_template, "Copies", this.Copies.ToString()));
                this.Copies = Params.Int("copies", this.Copies);

                // Load inline css
                var inlineFromHead = _parseHeadTag(this.Template, "style");
                if (inlineFromHead != null) this.CSS += inlineFromHead;

                // Debug mode?
                if (Params.Bool("debug", false)) {
                    // Writeout
                    Template.InsertVariable("debug.style", CSS);
                    this.Context.Response.Write(_template.Data);
                    this.Context.Response.Flush();
                } else {
                    // Generate PDF
                    {
                        Template.InsertVariable("debug.style", string.Empty);
                        byte[] bytes = null;
                        string contentType = null;
                        // PNG or PDF?
                        if (this.ContentFormat == "png") {
                            bytes = this.GeneratePNGData();
                            contentType = "image/png";
                        } else {
                             bytes = this.GeneratePDFData();
                            contentType = "application/pdf";
                        }
                        // Download or send direct?
                        if (Params.Bool("download", true) == false) {
                            Context.Response.Clear();
                            Context.Response.ContentType = contentType;
                            Context.Response.BinaryWrite(bytes);
                            Context.Response.Flush();
                        } else {
                            HTTP.SendBinaryData(this.Context, bytes, this.FileName);
                        }
                    }
                    
                }

            } else {
                base.Finish();
            }
        }

        public void InsertCommonVariables() {
            // Insert header and footer...
            this.Template.InsertTemplate("pdf.head", "pdf.head");

            // Standard variables
            // CDN url must be http on a dev env otherwise the PDF image provider can't download the images due to SSL issues
            var build = Core.Config.BuildID;
            if (!Core.Config.StaticBundleCacheEnabled) build += "-"+ Core.Config.StartupTime.Ticks.ToString();
            this.Template.InsertVariable("cdn-url", Core.Config.CDNURL);
            this.Template.InsertVariable("public-url", Core.Config.PublicURL);
            this.Template.InsertVariable("server-url", Core.Config.ServerURL);
            this.Template.InsertVariable("build-id", build);
            this.Template.InsertVariableUnsafe("space", "<div class='space'>  </div>");

            // Project
            this.Template.InsertVariable("project-name", Core.Config.ProjectName);

            // Auto-insert text variables
            this.Template.DiscoverVariables();
            this.Template.InsertTemplateVariables();
            this.Template.InsertTextVariables();
        }
        #endregion
    }
}
