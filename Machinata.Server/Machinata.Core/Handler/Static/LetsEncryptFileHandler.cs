using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    /// <summary>
    /// Basic static file handler to support standard Let'sEncrypt integrataion (WACS.exe)
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public class LetsEncryptFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        
        [RequestHandler("/.well-known/{file}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void WellKnownFile(string file) {

            if (file.Contains("..")) throw new BackendException("security",".well-known file has insecure name");
            string filePath = Core.Config.BasePath + ".well-known" + Core.Config.PathSep + file;
            if (File.Exists(filePath)) {
                SendFile(filePath);
            } else {
                throw new Backend404Exception();
            }
        }
        

    }
}
