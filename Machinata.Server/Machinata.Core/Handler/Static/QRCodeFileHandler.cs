using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {

    
    public class QRCodeFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathQRCodes = CachePath + "-qrcode";

        public static TimeSpan DefaultCacheTime = new TimeSpan(30, 0, 0, 0); // 30 days
        
        [RequestHandler("/static/qrcode", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void QRCode() {

            // Init cachepath
            string data = this.Params.String("data");
            int size = this.Params.Int("size", 600);
            bool download = this.Params.Bool("download", false);
            string codeHash = Core.Encryption.DefaultHasher.HashString(data);
            string format = Params.String("format", "png");
            string color = Params.String("color", "black");
            string bg = Params.String("background", "transparent");
            string icon = this.Params.String("icon");
            int iconSizePercent = this.Params.Int("icon-size",40);
            int iconBorderPercent = this.Params.Int("icon-border",1);
            bool forceUTF8 = this.Params.Bool("utf8", false);

            string iconCacheSuffix = string.Empty;
            if (string.IsNullOrEmpty(icon) == false) {
                iconCacheSuffix = "_" +  Core.Encryption.DefaultHasher.HashString(icon)+"is"+iconSizePercent+"ib"+iconBorderPercent;
            }

            string forceUTF8Suffix = string.Empty;
            if (forceUTF8 == true) {
                forceUTF8Suffix = "_utf8";
            }

            string cachePath = CachePathQRCodes + Core.Config.PathSep + codeHash + "_" + size+ "_" + color.Replace("#","")+ "_" + bg.Replace("#","") + iconCacheSuffix + forceUTF8Suffix  + "." + format;
            
            
            // Create the bundle as cached if needed
            bool forceCreate = !Core.Config.StaticQRCodeCacheEnabled;
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, ()=> {
                // Init
                _logger.Info("Creating qr code "+data);

                var modulesPerPixel = 1.0 / 28.0;
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q, forceUTF8);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap bitmap = null;
              
                if (string.IsNullOrEmpty(icon) == false) {
                    _logger.Info("Loading icon: " + icon);
                    Bitmap iconImage = null;
                    iconImage = DownloadIconToBitmap(icon);
                    var darkColor = Core.Util.Colors.ColorForHTMLCode(color);
                    var lightColor = Core.Util.Colors.ColorForHTMLCode(bg);
                    if (bg == "transparent") lightColor = Color.White; // transparent doesnt work...
                    bitmap = qrCode.GetGraphic(
                        pixelsPerModule: (int)(size * modulesPerPixel), // 1 => 29px
                        darkColor: darkColor,
                        lightColor: lightColor,
                        drawQuietZones: false,
                        icon: iconImage,
                        iconSizePercent: iconSizePercent,
                        iconBorderWidth: (int)(size* (iconBorderPercent/100.0))
                    );
                } else {
                    // Use seperate call because transparant doenst work with icon
                    bitmap = qrCode.GetGraphic((int)(size * modulesPerPixel), color, bg, false); // 1 => 29px
                }


                if (format == "pdf") {
                    // Save a WMF
                    var imgFormat = System.Drawing.Imaging.ImageFormat.Wmf;
                    string cachePathWMF = cachePath + ".wmf";
                    bitmap.Save(cachePathWMF, imgFormat);
                    // Convert WMF to PDF
                    Core.PDF.PDFConverter.CreatePDFFromImage(cachePathWMF, cachePath);
                    // Delete WMF
                    System.IO.File.Delete(cachePathWMF);
                } else {
                    var imgFormat = System.Drawing.Imaging.ImageFormat.Png;
                    if (format == "png") imgFormat = System.Drawing.Imaging.ImageFormat.Png;
                    else if (format == "jpg") imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                    else if (format == "gif") imgFormat = System.Drawing.Imaging.ImageFormat.Gif;
                    else if (format == "wmf") imgFormat = System.Drawing.Imaging.ImageFormat.Wmf;
                    else throw new Exception("Format is not supported.");
                    bitmap.Save(cachePath, imgFormat);
                }
            }, forceCreate);
            
            if (download == true) {
                string filename = this.Params.String("filename", "QRCode_" + codeHash + "." + format);
                SendFile(cachePath, DefaultCacheTime, filename);
            } else {
                SendFile(cachePath, DefaultCacheTime);
            }
           
        }

        /// <summary>
        /// Downloads an icon into a Bitmap via stream
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Bitmap DownloadIconToBitmap(string url) {

            try {

                if (Core.Config.IsTestEnvironment) {
                    Core.Util.HTTP.IgnoreSSLCertificates();
                } 
                if(!url.StartsWith("http://") && !url.StartsWith("https://")) {
                    url = Core.Config.ServerURL + url;
                }
                Uri uri = new Uri(url);
                var filename = Guid.NewGuid().ToString();
                string extension = System.IO.Path.GetExtension(uri.LocalPath);
                var tempFilePath = CachePathQRCodes + Core.Config.PathSep + filename + extension;
                WebRequest request = HttpWebRequest.Create(uri);
                using (WebResponse response = request.GetResponse()) {
                    Stream responseStream = response.GetResponseStream();
                    var image = new Bitmap(responseStream);
                    return image;
                }
            } catch (Exception e) { 
                throw new BackendException("icon-error", "Could not load icon from given url",e);
            }

          
        }


    }
}
