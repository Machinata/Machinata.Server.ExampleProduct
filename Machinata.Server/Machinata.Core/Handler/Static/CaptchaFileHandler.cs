using BarcodeLib;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {



    public class CaptchaFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathCaptchas = CachePath + "-captcha";

        public static TimeSpan DefaultCacheTime = new TimeSpan(30, 0, 0, 0); // 30 days
        
        [RequestHandler("/static/captcha", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void Captcha() {

            // Init cachepath
            string code = this.Params.String("code");
            int size = this.Params.Int("size", 12);
            string format = "png";
            string color = Params.String("color", "black"); //TODO: @dan
            string bg = Params.String("background", "transparent"); //TODO: @dan
            string cachePath = CachePathCaptchas + Core.Config.PathSep + code + "_" + Core.Config.BuildID + "_" + size+ "_" + color.Replace("#","")+ "_" + bg.Replace("#","") + "." + format;
            
            // Create the bundle as cached if needed
            bool forceCreate = !Core.Config.StaticQRCodeCacheEnabled;
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, ()=> {
                // Init
                _logger.Info("Creating captcha code "+code);

                var text = "error";
                try {
                    text = Core.Ids.Captcha.Default.DecodeCode(code);
                } catch { }
                var font = new Font("Courier", size, FontStyle.Bold); //TODO: @dan
                SizeF textSize;

                using(Bitmap bitmap = new Bitmap(1, 1)) {
                    using (Graphics g = Graphics.FromImage(bitmap)) {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        textSize = g.MeasureString(text,font);
                    }
                }

                using(Bitmap bitmap = new Bitmap((int)textSize.Width, (int)textSize.Height)) {
                    using (Graphics g = Graphics.FromImage(bitmap)) {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(text,font,Brushes.Black,0,0);
                        bitmap.Save(cachePath, System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
                
                

            }, forceCreate);
            
            SendFile(cachePath, DefaultCacheTime);
        }
        
        

    }
}
