using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Core.Handler {
    
    public partial class HandlerRequest {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        

        #region Private Static Fields

        private HttpContext _context = null;
        
        #endregion

        #region Constructor

        public HandlerRequest(HttpContext context) {
            _context = context;
        }
        
        #endregion
        
        #region Public Methods
        
        public HttpPostedFile File(int index = 0) {
            // Here we read the max request length from .net (web.config) - this way there is a single setting
            // TODO: this could actually be removed entirely... Since IIS/.net validates this anyways...
            int maxRequestLength = 300 * 1024 * 1024; // 300MB fallback, in bytes
            var section = System.Configuration.ConfigurationManager.GetSection("system.web/httpRuntime") as System.Web.Configuration.HttpRuntimeSection;
            if (section != null) {
                // The web.config is in kilobytes: https://docs.microsoft.com/en-us/dotnet/api/system.web.configuration.httpruntimesection.maxrequestlength?view=netframework-4.8
                maxRequestLength = section.MaxRequestLength * 1024;
            }
            // Validate request
            if (_context.Request.ContentLength > maxRequestLength) throw new BackendException("file-too-large", $"The file is too large. Please upload a file smaller than {maxRequestLength/(1024*1024)} MB.");
            if (_context.Request.Files.Count == 0) throw new Backend404Exception("no-file", "No file has been uploaded.");
            if (_context.Request.Files.Count <= index) throw new Exception($"The request file at index {index} does not exist.");
            // Return the file
            var file = _context.Request.Files[index];
            return file;
        }

        public bool HasFiles() {
            return _context.Request.Files.Count > 0;
        }

        public string IP {
            get {
                return Core.Util.HTTP.GetRemoteIP(this._context);
            }
        }

        public string UserAgent {
            get {
                return Core.Util.HTTP.GetUserAgent(this._context);
            }
        }


        public byte[] ReadFileFromRequest(string extension) {
            var file = this.File();
            if (!file.FileName.EndsWith(extension, StringComparison.Ordinal)) throw new BackendException("invalid-file", $"The file must be a (.{extension}) file.");
            byte[] fileData = null;
            using (var binaryReader = new System.IO.BinaryReader(file.InputStream)) {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            return fileData;
        }


        /// <summary>
        /// Read a string from the request input stream.
        /// WARNING: You can only call this method once. Use Handler.Params.Data() instead.
        /// </summary>
        /// <returns></returns>
        public string ReadFromRequestStream () {
            // Via https://stackoverflow.com/questions/15577871/reading-posted-json-data-in-asp-net
            var result = string.Empty;
            this._context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(_context.Request.InputStream)) {
                result = inputStream.ReadToEnd();
            }
            return result;
        }

        #endregion


    }
}
