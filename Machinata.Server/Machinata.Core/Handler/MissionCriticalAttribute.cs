using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {

    /// <summary>
    /// Mark Methods as MissionCritical to enable reporting Exceptions to admin
    /// Default behaviour all Exceptions
    /// Define in Constructor which type of Exceptions should by caucht
    /// </summary>
    /// <seealso cref="System.Attribute" />
    // TODO: not used yet
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MissionCriticalAttribute : Attribute {

        public MissionCriticalAttribute(Type type) {
            this.Type = type;
        }

        public MissionCriticalAttribute() {
            this.Type = typeof(Exception);
        }

        public Type Type { get; private set; }

    }



}
