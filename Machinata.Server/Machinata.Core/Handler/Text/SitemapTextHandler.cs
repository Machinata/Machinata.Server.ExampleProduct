using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Handler {

    /// <summary>
    /// Any static method can provide information for the website sitemap.
    /// The method must return a list of urls (absolute but without host).
    /// The method boiler plate looks like this:
    /// 
    ///    public static List<string> YourSitemapProvider(ModelContext db)
    /// 
    /// Example:
    /// 
    ///    [SitemapProvider]
    ///    public static List<string> YourSitemapProvider(ModelContext db) {
    ///        var ret = new List<string>();
    ///        foreach(var pid in db.ContentNodes().GetPagesAtPath("Journal").Published().Select(n => n.PublicId)) {
    ///            ret.Add($"/journal/"+pid);
    ///            ret.Add($"/de/journal/"+pid);
    ///        }
    ///        return ret;
    ///    }
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class SitemapProviderAttribute : Attribute {
        
    }


    public class SitemapTextHandler : Core.Handler.TextHandler {
        
        public static List<string> GetSitemap(ModelContext db) {
            // Init
            var paths = new List<string>();

            if (Core.Config.SitemapEnabled == false) return paths;

            // Get all routes that need to be on the sitemap from request handlers
            {
                var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(SitemapRouteAttribute));
                foreach (var method in methods) {
                    //foreach(RequestHandlerAttribute attr in method.GetCustomAttributes(typeof(RequestHandlerAttribute), false)) {
                    //    _registerSitemapPath(attr.Path);
                    //}
                    // Find routes that match this method
                    var routes = Routes.Route.GetRoutesForMethod(method);
                    foreach(var route in routes) {
                        _validateSitemapPath(route.Path);
                        if(!paths.Contains(route.Path)) paths.Add(route.Path);
                    }
                }
            }

            // Get all providers for sitemaps
            {
                var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(SitemapProviderAttribute));
                foreach (var method in methods) {
                    var ret = method.Invoke(null, new object[] { db }) as List<string>;
                    foreach(var path in ret) {
                        _validateSitemapPath(path);
                        if(!paths.Contains(path)) paths.Add(path);
                    }
                }
            }

            // Nothing added?
            if (paths.Count == 0) throw new Backend404Exception("empty-sitemap","The sitemap is empty.");

            // Sort
            paths.Sort(new Routes.AscendingRoutePriorityComparer());

            return paths;
        }
    
        [RequestHandler("/sitemap.txt", AccessPolicy.PUBLIC_ARN)]
        public void Sitemap() {
            if (Core.Config.SitemapEnabled == false) throw new Backend404Exception("sitemap-disabled","The sitemap is not enabled for this site.");

            // Output
            foreach (var path in GetSitemap(this.DB)) {
                StringOutput.AppendLine(Core.Config.PublicURL + path);
            }
        }

        private static void _validateSitemapPath(string path) {
            // Validate
            if (path.Contains("{") && path.Contains("}")) throw new BackendException("invalid-path",$"The path '{path}' is not valid as it contains variables.");
            if (!path.StartsWith("/")) throw new BackendException("invalid-path",$"The path '{path}' is not valid as it does not start with a /.");
        }
    }
}