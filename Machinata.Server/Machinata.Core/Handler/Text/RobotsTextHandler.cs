using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;


namespace Machinata.Core.Handler {

    public class RobotsTextHandler : Core.Handler.TextHandler {
        
        [RequestHandler("/robots.txt", AccessPolicy.PUBLIC_ARN)]
        public void Robots() {
            this.StringOutput.Append(Core.Config.Dynamic.String("RobotsTXT"));
        }
    }
}