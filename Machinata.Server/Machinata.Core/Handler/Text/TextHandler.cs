using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Handler {

    
    public abstract partial class TextHandler : Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Private Static Fields
        #endregion

        #region Private Fields
        
        #endregion

        #region Public Fields
        
        #endregion

        #region Handler Virtual Implementations
        
        public override string DefaultMimeType
        {
            get
            {
                return "text/plain";
            }
        }

        public override ContentType GetDefaultContentType() {
            return ContentType.Text;
        }

        public override void PrepareForError(Uri uri, BackendException be) {
            this.ContentType = ContentType.Text;
        }

        public override void OnError(BackendException be) {
            // Process base
            base.OnError(be);

            // Show error
            this.StringOutput.AppendLine("Server Error");
            this.StringOutput.AppendLine("");
            this.StringOutput.AppendLine(be.GetMaskedFullMessageTrace().Trim() + " ("+be.Code+")");
            this.StringOutput.AppendLine("");
            if (Core.Config.ErrorStackTraceEnabled) this.StringOutput.AppendLine(be.GetMaskedFullStackTrace());
            if (Core.Config.ErrorLogTraceEnabled) this.StringOutput.AppendLine(be.GetLogTrace());
        }
        
        #endregion

        #region Handler Specific Implementations
        

        #endregion
    }
}
