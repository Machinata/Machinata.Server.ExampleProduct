
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.StructuredData;

namespace Machinata.Core.Handler {


    /// <summary>
    /// An extended handler for processing page templates. This handler automatically provides
    /// a template context based on the called route and will automatically return the output
    /// when the method finishes. A PageTemplateHandler always has a single extension type (such as
    /// HTML) defined.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public abstract partial class PageTemplateHandler : Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Private Static Fields
        #endregion

        #region Private Fields
        
        protected PageTemplate _template = null;
        private NavigationBuilder _navigation = new NavigationBuilder();
        protected string _language = Core.Config.LocalizationDefaultLanguage;
        
        #endregion

        #region Public Fields
        
        public PageTemplate Template {
            get {
                return _template;
            }
        }

        public virtual string TemplateExtension {
            get {
                return ".htm";
            }
        }

        /// <summary>
        /// Gets a value indicating whether the handler will allow undefined templates.
        /// This is useful for some specific handlers, for example a PDF handler, where you don't
        /// need the full template complexity and just want to use blank templates.
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow undefined templates]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool AllowUndefinedTemplates {
            get {
                return false;
            }
        }

        public string ThemeName {
            get {
                return this.Template.ThemeName == null ? "default" : this.Template.ThemeName;
            }
        }

        /// <summary>
        /// Gets the navigation builder.
        /// You can use the builder to allow for syntax as the following:
        /// 
        /// this.Navigation.Add("database","Database");
        /// this.Navigation.Add("list-entities","List Entities");
        /// 
        /// This handler supports integration of navigation items as text
        /// variables (including inline definitions) such as:
        /// 
        /// this.Navigation.Add("list-entities","{text.list-entities=List Entities}");
        /// 
        /// Inline translations are not recommended as they are hardcoded and won't be discovered by the 
        /// translation system.
        /// </summary>
        /// <value>
        /// The navigation.
        /// </value>
        public NavigationBuilder Navigation {
            get {
                return _navigation;
            }
        }

        /// <summary>
        /// The meta information either set or collected for this page.
        /// </summary>
        public Model.MetaInformation Meta = new Model.MetaInformation();



            

        #endregion

        #region Virtual Methods

        public virtual void DefaultNavigation() { }

        /// <summary>
        /// If not null, this will set the template language regardless of the detected language.
        /// </summary>
        /// <returns></returns>
        public virtual string DefaultLanguage() { return null; }

        public virtual string DefaultTheme() { return null; }

        public virtual void InsertAdditionalVariables() { }

        #endregion

        #region Handler Virtual Implementations

        public override ContentType GetDefaultContentType() {
            return ContentType.TemplatePage;
        }

        public override void SetupForContentType() {
            
            if(this.ContentType == ContentType.TemplatePage) {

                // TemplatePage
                _template = PageTemplate.LoadForHandlerAndRoutePath(this, this.Route.GetRoutePathOrAliasIfSet(), this.AllowUndefinedTemplates);
                _template.Handler = this;
                _template.ThemeName = this.DefaultTheme();

                // Language
                if(this.DefaultLanguage() != null) _template.Language = this.DefaultLanguage();
                else _template.Language = this.Language;

                // Currency
                _template.Currency = this.Currency;
                
                // Forced login?
                if(Core.Config.SecurityAllPagesRequireLogin == true && this._currentException == null) {
                    // Excluded?
                    if (!Core.Config.SecurityExcludePagesFromRequireLogin.Contains(this.Route.Path)) {
                        if (this.User == null) throw new Backend401Exception("login-required", "A login is required to access any page on this website.", "login");
                    }
                }

                // Default navigation
                this.DefaultNavigation();

            } else {
                base.SetupForContentType();
            }
        } 

        public override void PrepareForError(Uri uri, BackendException be) {
            // Figure out the template
            var template = Core.Templates.PageTemplate.Cache.Find(this.PackageName, this.FallbackPackageName, "error-"+be.Status.ToString(), ".htm", false);
            if(template == null) template = Core.Templates.PageTemplate.Cache.Find(this.PackageName, this.FallbackPackageName, "error", ".htm", true);
            template.Handler = this;

            // Set new url
            var uriBuilder = new UriBuilder(uri);
            uriBuilder.Path = template.Name;
            var newUri = uriBuilder.Uri;
            this.Uri = newUri;
            
            // Setup handler
            this.ContentType = ContentType.TemplatePage;
            this.OriginalRoute = null;//TODO: this doesnt quite work since the route is not guaranteed to be the master route, also the route could contain variables... new Routes.Route(uri.LocalPath,Verbs.Any,null,this.Language);;
            this.Route = new Routes.Route(newUri.LocalPath,Verbs.Any,newUri.LocalPath,this.Language);
            this.RequestPath = newUri.LocalPath;
        }

        public override void OnError(BackendException be) {
            // Process base
            base.OnError(be);

            // Init
            string message = be.GetMaskedFullMessageTrace();
            string stackTrace = be.GetMaskedFullStackTrace();
            if (!Core.Config.ErrorStackTraceEnabled) stackTrace = "";

            // Get log trace
            string logTrace = "";
            string logTraceEnabled = "false";
            if (Core.Config.ErrorLogTraceEnabled) {
                logTrace = be.GetLogTrace();
                if(logTrace != null) logTraceEnabled = "true";
            }
            

            this.Template.InsertVariable("error.code", be.Code);
            this.Template.InsertVariable("error.message", message);
            this.Template.InsertVariable("error.log-trace", logTrace);
            this.Template.InsertVariable("error.log-trace-enabled", logTraceEnabled);
            this.Template.InsertVariable("error.stack-trace", stackTrace);
            this.Template.InsertVariable("error.stack-trace-enabled", Core.Config.ErrorStackTraceEnabled.ToString().ToLower());
        }

      

        public override void Finish() {
            if(_template != null) {

                // Insert header and footer...
                this.Template.InsertTemplate("page.head", "page.head");
                this.Template.InsertTemplate("page.header", "page.header");
                this.Template.InsertTemplate("page.footer", "page.footer");

                // Auto-insert template variables
                this.Template.InsertTemplateVariables();

                // Menu items via templates 
                var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "default/menu.item.", this.TemplateExtension);
                this.Template.InsertTemplates("page.menu-items", menuItems);

                // Figure out page title
                if (this.Meta.Title == null || this.Meta.Title == "automatic") {
                    this.Meta.Title = this.Navigation.PageTitle;
                }
                if (this.Meta.Description == null || this.Meta.Description == "automatic") {
                    this.Meta.Description = this.Meta.DescriptionFromContent;
                }
                // Add trailer to title
                if (!string.IsNullOrEmpty(this.Meta.TitleTrailer) && this.Meta.Title != null && !this.Meta.Title.Contains(this.Meta.TitleTrailer)) {
                    var exclusionDetected = false;
                    foreach (var exclusion in this.Meta.ExcludeTitleTrailerIfInTitle) {
                        if (this.Meta.Title.Contains(exclusion)) {
                            exclusionDetected = true;
                            break;
                        }
                    }
                    if (!exclusionDetected) this.Meta.Title += this.Meta.TitleTrailer;
                }

                // Figure out page title
                string pageTitle = this.Navigation.PageTitle;
                if (this.Meta.UseMetaTitleForPageTitle) {
                    if (!string.IsNullOrEmpty(this.Meta.Title)) pageTitle = this.Meta.Title;
                }

                // Navigation
                this.Template.InsertNavigation(this.Navigation,"page.navigation","page.navigation" );

                // Auto-insert template variables, again
                this.Template.InsertTemplateVariables();

                // Page vars
                this.Template.InsertVariable("page.title", pageTitle);
                this.Template.InsertVariable("page.url", this.Uri.ToString());
                this.Template.InsertVariable("page.query", this.Uri.Query);

                // Meta information
                this.Template.InsertMetaTags("page.meta-tags", this.Meta);

                // Structured Data
                this.Template.InsertStructuredData("page.structured-data", this.Template.StructuredData);

                // Routes
                var routeToUse = this.Route;
                if (this.OriginalRoute != null) routeToUse = this.OriginalRoute;
                this.Template.InsertVariable("page.route", routeToUse.Path);
                if (this.Template.HasVariableStartingWith("page.route.alias")) {
                    var aliases = routeToUse.GetAliases();
                    foreach (var alias in aliases) {
                        // Discover and insert route parameters (if any)
                        var routePath = this.ApplyCurrentRouteParemetersToRoute(alias.Path);
                        this.Template.InsertVariable("page.route.alias." + alias.Language, routePath);
                    }
                }

                // Project
                this.Template.InsertVariable("project-name", Core.Config.ProjectName);

                // Auth-status
                var policyCSS = ".requires-policy { display: none; }";
                if (this.User != null) {
                    var userPolicies = this.User.GetPolicyList();
                    this.Template.InsertVariable("user.auth-status", "logged-in");
                    this.Template.InsertVariable("user.name", this.User.Name);
                    this.Template.InsertVariable("user.username", this.User.Username);
                    this.Template.InsertVariable("user.email", this.User.Email);
                    this.Template.InsertVariable("user.policy-list", string.Join(" ", userPolicies.Select(s => "has-policy-" + s)));
                    foreach (var p in userPolicies) {
                        policyCSS += "\n.has-policy-" + p + " .requires-policy.policy-" + p + " { display: inherit; }";
                    }
                    // Special overwrite if superuser
                    if (userPolicies.Contains("superuser")) policyCSS += "\n.requires-policy { display: inherit; }";
                } else {
                    this.Template.InsertVariable("user.auth-status", "logged-out");
                    this.Template.InsertVariable("user.policy-list", "");
                }
                this.Template.InsertVariable("user.policy-classes", policyCSS);

                // Standard variables
                var build = Core.Config.BuildID;
                if (this.Params.Bool("debut", false)) build = Guid.NewGuid().ToString();
                this.Template.InsertVariable("analytics.enabled", Core.Config.AnalyticsEnabled);
                this.Template.InsertVariable("analytics.id", Core.Config.AnalyticsId);
                this.Template.InsertVariable("analytics.gtm-id", Core.Config.AnalyticsGTMId);
                this.Template.InsertVariable("page.language", this.Template.Language);
                this.Template.InsertVariable("cdn-url", Core.Config.CDNURL);
                this.Template.InsertVariable("server-url", Core.Config.ServerURL);
                this.Template.InsertVariable("public-url", Core.Config.PublicURL);
                this.Template.InsertVariable("build-id", build);
                this.Template.InsertVariable("debug", this.Params.Bool("debug", false));
                this.Template.InsertVariable("page.theme", this.ThemeName);
                this.Template.InsertVariable("server.version", Core.Config.BuildVersion);
                this.Template.InsertVariable("server.timestamp", Core.Config.BuildTimestamp.ToDateTimeString());

                // Remove blank background images, a common occurance in templates
                this.Template.Data.Replace("background-image:url();", "");
                this.Template.Data.Replace("background-image:url(/);", "");

                // Call virtual method for any additional varialbs
                this.InsertAdditionalVariables();

                // Auto-insert text variables
                this.Template.DiscoverVariables();
                this.Template.InsertTextVariables();
                this.Template.InsertIconVariables();

                // Track PageView
                this.TrackPageView(pageTitle);

                // Language header
                this.Context.Response.Headers["Content-Language"] = this.Language;

                // Set caching
                this.WriteCacheHeaders();

                // New method: Use util method that also supports compression
                Core.Util.HTTP.WriteStringWithCompressionSupport(_template.Data, this.Context);

                // Old method: Writeout (deprecated)
                //this.Context.Response.Write(_template.Data);
                //this.Context.Response.Flush();

            } else {
                base.Finish();
            }
        }

     
        private void TrackPageView(string pageTitle) {

            // Check enabled
            if (Core.Config.AnalyticsEnabled == false || Core.Config.AnalyticsEnablePageViewTracking == false) { return; }

            // Try to translate to default language
            if (!string.IsNullOrEmpty(pageTitle)) {
                pageTitle = Core.Localization.TextParser.ReplaceTextVariablesForData(this.PackageName, pageTitle, this.Language);
            } else {
                pageTitle = "unknown";
            }

            Core.Analytics.AnalyticsTracker.DefaultTrackerForHandler(this).TrackPageView(pageTitle, this.RequestPath);
        }

        #endregion
    }
}
