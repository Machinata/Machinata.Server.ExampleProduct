using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {
    
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class RequestHandlerAttribute : Attribute
    {
        
        /// <summary>
        /// Gets or sets the content type. This will help to auto load the response types and mime types as well as any templates.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public ContentType Content { get; protected set; }

        /// <summary>
        /// Gets or sets the HTTP verb for the request handler. Multiple can be defined or
        /// also 'Any'.
        /// </summary>
        /// <value>
        /// The verb.
        /// </value>
        public Verbs Verb { get; protected set; }

        /// <summary>
        /// Gets or sets the paths.
        /// </summary>
        /// <value>
        /// The paths.
        /// </value>
        public string Path { get; protected set; }


        /// <summary>
        /// Gets or sets the ARN (Access Resource Name) for this request handler.
        /// </summary>
        /// <value>
        /// The arn.
        /// </value>
        public string ARN { get; protected set; }

        /// <summary>
        /// Gets or sets the language for the route.
        /// Per default this null, but if the route is specific to a language 
        /// then it could be set.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public string Language { get; protected set; }

        /// <summary>
        /// Gets or sets the alias for which this route is for. This allows handlers
        /// to use this information (for example, loading the correct template) or enables
        /// automation of language switching between all alias pairs.
        /// </summary>
        /// <value>
        /// The alias for.
        /// </value>
        public string AliasFor { get; protected set; }
        
        /// <summary>
        /// If true, then this route is not checked for standard request validation from 
        /// .net (ie no SQL injection et cetera).
        /// </summary>
        public bool SkipRequestValidation { get; protected set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestHandlerAttribute"/> class.
        /// </summary>
        /// <param name="verb">The verb.</param>
        /// <param name="path">The path.</param>
        /// <param name="skipRequestValidation">If true, then this route is not checked for standard request validation from .net (ie no SQL injection et cetera).</param>
        /// <exception cref="System.ArgumentException">The argument 'path' must be specified.</exception>
        public RequestHandlerAttribute(string path, string arn = null, string langauge = null, Verbs verb = Verbs.Any, ContentType content = ContentType.Default, string aliasFor = null, bool skipRequestValidation = false) {
            // Sanity
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("The argument 'path' must be specified.");
            // Init properties
            this.Verb = verb;
            this.Path = path;
            this.Content = content;
            this.ARN = arn;
            this.Language = langauge;
            this.AliasFor = aliasFor;
            this.SkipRequestValidation = skipRequestValidation;
        }
        
    }


    /// <summary>
    /// A shortcut attribute for setting a request handler with the aliasFor property
    /// set to Routes.Route.ALIAS_FOR_FIRST_ROUTE.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.RequestHandlerAttribute" />
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class AliasRequestHandlerAttribute : RequestHandlerAttribute {

        /// <summary>
        /// Initializes a new instance of the <see cref="AliasRequestHandlerAttribute"/> class.
        /// A shortcut attribute for setting a request handler with the aliasFor property
        /// set to Routes.Route.ALIAS_FOR_FIRST_ROUTE.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="arn"></param>
        /// <param name="langauge"></param>
        /// <param name="verb">The verb.</param>
        /// <param name="content"></param>
        /// <param name="aliasFor"></param>
        public AliasRequestHandlerAttribute(string path, string arn = Core.Model.AccessPolicy.DEFAULT_ARN, string langauge = null, Verbs verb = Verbs.Any, ContentType content = ContentType.Default)
            : base(path,arn,langauge,verb,content,Routes.Route.ALIAS_FOR_FIRST_ROUTE) {

        }

    }


    /// <summary>
    /// A shortcut attribute for registering multiple routes using a defined text items.
    /// For example, if you have set a .txt localization file with the following:
    /// 
    /// routes.portal.de=/portal
    /// routes.portal.en=/en/portal
    /// 
    /// Then setting a attribute such as:
    /// 
    /// [MultiRouteRequestHandler("routes.portal", PORTAL_ARN)]
    /// 
    /// would be the same as setting the following attributes:
    /// 
    /// [RequestHandler("/portal", PORTAL_ARN, "de")]
    /// [AliasRequestHandler("/en/portal", PORTAL_ARN, "en")]
    /// 
    /// Note that the first text id is the master route, with all the following being a alias.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.RequestHandlerAttribute" />
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MultiRouteRequestHandlerAttribute : RequestHandlerAttribute {

        /// <summary>
        /// A shortcut attribute for registering multiple routes using a defined text items.
        /// For example, if you have set a .txt localization file with the following:
        /// 
        /// routes.portal.de=/portal
        /// routes.portal.en=/en/portal
        /// 
        /// Then setting a attribute such as:
        /// 
        /// [MultiRouteRequestHandler("routes.portal", PORTAL_ARN)]
        /// 
        /// would be the same as setting the following attributes:
        /// 
        /// [RequestHandler("/portal", PORTAL_ARN, "de")]
        /// [AliasRequestHandler("/en/portal", PORTAL_ARN, "en")]
        /// 
        /// Note that the first text id is the master route, with all the following being a alias.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="arn">The arn.</param>
        /// <param name="verb">The verb.</param>
        /// <param name="content">The content.</param>
        public MultiRouteRequestHandlerAttribute(string path, string arn = Core.Model.AccessPolicy.DEFAULT_ARN, Verbs verb = Verbs.Any, ContentType content = ContentType.Default)
            : base(path,arn,null,verb,content) {

        }

    }
    

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CMSPathAttribute : Attribute {

        public string Path;

        public CMSPathAttribute(string path) {
            if (!path.StartsWith("/")) path = "/" + path;
            this.Path = path;
        }
    }

    /// <summary>
    /// This attribute can be added to a RequestHandler process function that has a route mapped
    /// for automatic inclusion in the sitemap.
    /// The route cannot have variables.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class SitemapRouteAttribute : Attribute {
        
    }
}
