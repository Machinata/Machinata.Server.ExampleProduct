using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Handler {


    /// <summary>
    /// A abstract generic helper class from common API CRUD operations.
    /// When implementing a basic API handler, you can inherit from this with the type
    /// you are dealing with, and call the CRUD functions from your RequestHandler routes.
    /// All the helper methods use the standard form names.
    /// Note: you will still need to setup all the routes and access policies.
    /// Once setup, a method will look like this:
    ///    [RequestHandler("/api/admin/users/create")]
    ///    public void Create() {
    ///        CRUDCreate();
    ///    }
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Machinata.Core.Handler.APIHandler" />
    public abstract partial class CRUDAPIHandler<T> : APIHandler where T : ModelObject, new() {
        
        protected T CRUDCreate(string returnedEntityName = null, FormBuilder form = null) {
            this.RequireWriteARN();
            // Create the new entity
            if (form == null)
            {
                form = new FormBuilder(Forms.Admin.CREATE);
            }
            var entity = new T();
            entity.Populate(this,form);
            // Post Populate
            this.CreatePopulate(entity);
            entity.Validate();
            // Save
            this.DB.Set<T>().Add(entity);
            this.DB.SaveChanges();
            // Create object to return, including all the data from the create form and the public id
            var retProps = new Dictionary<string, object>();
            foreach(var prop in entity.GetPropertiesForForm(form.Include("PublicId"))) {
                if(prop.CanRead) {
                    var val = prop.GetValue(entity);
                    if (val is ModelObject) continue;
                    retProps[prop.Name] = val;
                }
            }
            var retRoot = new Dictionary<string, object>();
            if (returnedEntityName == null) returnedEntityName = typeof(T).Name;
            retRoot[returnedEntityName] = retProps;
            // Return
            SendAPIMessage("create-success", retRoot);
            return entity;
        }
        
        protected T CRUDEdit(string publicId, FormBuilder form = null) {
            this.RequireWriteARN();
            // Make changes and save
            var entity = this.DB.Set<T>().GetByPublicId(publicId);
            if (form == null)
            {
                form = new FormBuilder(Forms.Admin.EDIT);
            }
            entity.Populate(this,form);
            // Post Populate
            this.EditPopulate(entity);
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success");
            return entity;
        }

        protected T CRUDDelete(string publicId) {
            this.RequireWriteARN();
            // Delete and save
            var entity = this.DB.Set<T>().GetByPublicId(publicId);
            this.PreDelete(entity);
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
            return entity;
        }

        public void CRUDToggleSelection<S>(string entityId, string toggleId, string property) where S:ModelObject {
            this.RequireWriteARN();
            // Init
            var prop = typeof(T).GetProperties().SingleOrDefault(p => p.Name == property);
            var entity = this.DB.Set<T>().Include(property).GetByPublicId(entityId);
            var entityToToggle = this.DB.Set<S>().GetByPublicId(toggleId);
            var enable = this.Params.Bool("value",false);
            var list = prop.GetValue(entity) as ICollection<S>;
            if(enable) {
                list.Add(entityToToggle);
            } else {
                list.Remove(entityToToggle);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        public void CRUDToggleSingleSelection<S>(string entityId, string toggleId, string property) where S : ModelObject {
            this.RequireWriteARN();
            // Init
            var prop = typeof(T).GetProperties().SingleOrDefault(p => p.Name == property);
            var entity = this.DB.Set<T>().Include(property).GetByPublicId(entityId);
            var entityToToggle = this.DB.Set<S>().GetByPublicId(toggleId);
            var enable = this.Params.Bool("value", false);
            var element = prop.GetValue(entity) as S;
            if (enable) {
                
                prop.SetValue(entity, entityToToggle);
            } else {
                prop.SetValue(entity, entityToToggle);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }


        /// <summary>
        /// Post Populate customization of Edit can be done here
        /// </summary>
        protected virtual void EditPopulate(T entity) { }

        /// <summary>
        /// Post Populate customization of Create can be done here
        /// </summary>
        protected virtual void CreatePopulate(T entity) { }



        /// <summary>
        /// Pre deletion customization can be done here
        /// </summary>
        protected virtual void PreDelete(T entity) { }

    }
}
