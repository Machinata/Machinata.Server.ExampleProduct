using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Handler {

    
    public abstract partial class APIHandler : Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields

        #endregion

        #region Public Fields

        #endregion

        #region Handler Virtual Implementations

        public override void SetupForContext(HttpContext context) {
            base.SetupForContext(context);
            this.SetLanguageFromContext();
            this.SetCurrencyFromContext();
        }

        public override bool ShouldHandleExceptionsInline() {
            return true;
        }

        public override ContentType GetDefaultContentType() {
            return ContentType.JSON;
        }

        public override void SetupForContentType() {
            base.SetupForContentType();
        }

        public override void PrepareForError(Uri uri, BackendException be) {
            this.ContentType = ContentType.JSON;
        }

        public override void OnError(BackendException be) {
            // Process base
            base.OnError(be);
            
            // Prepare error data
            var error = new Dictionary<string, string> {
                {"code", be.Code},
                {"message", be.GetMaskedFullMessageTrace()},
            };
            if (Core.Config.ErrorStackTraceEnabled) error["stacktrace"] = be.GetMaskedFullStackTrace();
            if (Core.Config.ErrorLogTraceEnabled) error["logtrace"] = be.GetLogTrace();

            // Log, since this doesnt escalate to a error page handler...
            _logger.Error(be, be.GetFullDescriptionIncludingTrace());

            // Send message
            SendAPIMessage("error", error);
        }

        #endregion

        #region Handler Specific Implementations

        /// <summary>
        /// Sends an API message as a JSON response using a object which is automatically serialized.
        /// The JSON is automatically formatted to DashedLowerCase format. This means
        /// that a property such as 'AuthToken' is formatted as 'auth-token'.
        /// </summary>
        /// <param name="type">The message type.</param>
        /// <param name="message">The message.</param>
        /// <param name="allowAnonymousCall">if set to <c>true</c> [allow anonymous call].</param>
        public void SendAPIMessage(string type, object message = null, bool allowAnonymousCall = false) {
            // Get json
            string jsonPayload = Core.JSON.Serialize(message);
            SendRawAPIMessage(type, jsonPayload, allowAnonymousCall);
        }

        /// <summary>
        /// Sends a raw API message as a JSON response using a jsonPayload string.
        /// Note: The jsonPayload must be valid JSON!
        /// </summary>
        /// <param name="type">The message type.</param>
        /// <param name="jsonPayload">The json payload in valid JSON as a string.</param>
        /// <param name="allowAnonymousCall">if set to <c>true</c> [allow anonymous call].</param>
        public void SendRawAPIMessage(string type, string jsonPayload, bool allowAnonymousCall) {
            // Write output
            if(allowAnonymousCall) Context.Response.Headers["Access-Control-Allow-Origin"] = "*"; //TODO: is there a better model for doing this?
            Context.Response.ContentType = "application/json";
            Context.Response.CacheControl = "no-cache";
            Context.Response.Write(
                "{\"type\":\""+type+"\",\"data\":"+jsonPayload+"}"
            );
        }

        #endregion
    }
}
