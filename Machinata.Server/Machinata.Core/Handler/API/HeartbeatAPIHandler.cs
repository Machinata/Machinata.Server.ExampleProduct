
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class HeartbeatAPIHandler : Core.Handler.APIHandler {
        

        [RequestHandler("/api/heartbeat", AccessPolicy.PUBLIC_ARN)]
        public void Heartbeat() {
            // Validate task manager
            Core.TaskManager.TaskManager.ValidateSchedularAndRestartIfNeeded();
            //TODO: send heartbeat message
            SendAPIMessage("hearbeat-success");
        }
        
        
    }
}
