using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Cards {

    public class CardBuilder {

        #region Enums and Structs

        public enum TextType {
            Title,
            Subtitle,
            Meta
        }

        internal struct TextWithType {
            public string Text;
            public TextType Type;
        }

        internal struct ActionWithTitle {
            public string Title;
            public string URL;
            public string Script;
        }

        #endregion

        #region Private Variables

        private string _iconName;
        private string _typeName;
        private string _link;

        private static int _cardUID = 0;
        private string _cardID;

        private Model.ModelObject _entity;
        
        private List<TextWithType> _main = new List<TextWithType>();
        
        private List<TextWithType> _sub = new List<TextWithType>();
        
        private List<TextWithType> _tag = new List<TextWithType>();

        private List<ActionWithTitle> _actions = new List<ActionWithTitle>();

        private List<string> _classes = new List<string>();
        
        private List<string> _breadcrumbs = null;
        private string _selectedBreadcrumb = null;
        
        private bool _isDefaultCard = false;
        private bool _isGrouping = false;

        #endregion

        #region Public Properties

        public string CardID {
            get {
                return _cardID;
            }
        }

        public string FormattedLink {
            get {
                var ret = _link;
                if(_entity != null && _link != null) {
                    ret = ret.Replace("{entity.public-id}", _entity.PublicId);
                }
                return ret;
            }
        }
        
        public bool IsDefaultCard { get { return _isDefaultCard; } }
        public bool IsGrouping { get { return _isGrouping; } }

        #endregion

        #region Constructors

        public CardBuilder() {
            _cardUID++;
            _cardID = "card" + _cardUID;
        }
        public CardBuilder(Model.ModelObject entity) {
            _cardUID++;
            _cardID = "card" + _cardUID;
            this.Entity(entity);
        }

        #endregion

        #region Chaining Methods
        
        public CardBuilder DefaultCard(bool isDefault = true) {
            _isDefaultCard = isDefault;
            return this;
        }

        public CardBuilder Grouping(bool isGrouping = true) {
            _isGrouping = isGrouping;
            return this;
        }

        public CardBuilder Wide() {
            return this.Class("wide");
        }

        public CardBuilder Entity(Model.ModelObject entity) {
            _entity = entity;
            this.Type(entity.TypeName);
            this.Icon(entity.TypeName.ToLower());
            return this;
        }

        public CardBuilder Class(string className) {
            _classes.Add(className);
            return this;
        }

        public CardBuilder Icon(string icon) {
            _iconName = icon;
            return this;
        }

        public CardBuilder Link(string link) {
            _link = link;
            return this;
        }

        public CardBuilder Type(string typeName) {
            _typeName = typeName;
            _main.Insert(0,new TextWithType() { Text = typeName, Type = TextType.Meta });
            return this;
        }

        public CardBuilder ClearType() {
            _main.Remove(_main.SingleOrDefault(t => t.Text == _typeName));
            _typeName = null;
            return this;
        }
        
        public CardBuilder Main(string text, TextType type = TextType.Meta) {
            _main.Add(new TextWithType() { Text = text, Type = type });
            return this;
        }

        public CardBuilder Title(string text) {
            return this.Main(text, TextType.Title);
        }

        public CardBuilder Subtitle(string text) {
            return this.Main(text, TextType.Subtitle);
        }

        public CardBuilder Subtitle(Enum text) {
            return Subtitle(text.GetEnumTranslationTextVariable());
        }

        public CardBuilder Sub(string text, TextType type = TextType.Meta) {
            _sub.Add(new TextWithType() { Text = text, Type = type });
            return this;
        }

        public CardBuilder Sub(Enum text, TextType type = TextType.Meta) {
            return Sub(text.GetEnumTranslationTextVariable(), type);
        }
        
        public CardBuilder BlinkSub() {
            return this.Class("sub-blink");
        }

        public CardBuilder Tag(string text, TextType type = TextType.Meta) {
            _tag.Add(new TextWithType() { Text = text, Type = type });
            return this;
        }

        public CardBuilder ActionWithLink(string title, string url) {
            _actions.Add(new ActionWithTitle() { Title = title, URL = url });
            return this;
        }

        /// <summary>
        /// Will automatically prefix with javascript:
        /// </summary>
        /// <param name="title"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public CardBuilder ActionWithScript(string title, string url) {
            _actions.Add(new ActionWithTitle() { Title = title, Script = url });
            return this;
        }

        public CardBuilder Breadcrumb(List<string> crumbs, string selected = null) {
            _breadcrumbs = crumbs;
            _selectedBreadcrumb = selected;
            return this;
        }

        public CardBuilder Breadcrumb<T>(IEnumerable<T> statusFlow, T selected) where T : struct, IConvertible {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type (enum)");
            var crumbs = new List<string>();
            foreach(var status in statusFlow) {
                crumbs.Add((status as Enum).GetEnumTranslationTextVariable());
            }
            var selectedTitle = (selected as Enum).GetEnumTranslationTextVariable();
            return Breadcrumb(crumbs,selectedTitle);
        }

        #endregion

        #region Template Methods

        public void InsertIntoTemplate(string variableName, Templates.PageTemplate template) {
            // Generate all templates
            Templates.PageTemplate cardTemplate = template.LoadTemplate("card");;
            if (this.FormattedLink != null) {
                cardTemplate.InsertTemplate("card.contents", "card.link");
                cardTemplate.InsertVariable("card.link", this.FormattedLink);
            }
            cardTemplate.InsertTemplate("card.contents", "card.contents");
            var mainTemplates = new List<Templates.PageTemplate>();
            var subTemplates = new List<Templates.PageTemplate>();
            var tagTemplates = new List<Templates.PageTemplate>();
            var actionTemplates = new List<Templates.PageTemplate>();
            var attachmentTemplates = new List<Templates.PageTemplate>();
            foreach(var text in _main) {
                var t = cardTemplate.LoadTemplate("card.text");
                t.InsertVariable("card.text.type", text.Type.ToString().ToLower());
                t.InsertVariable("card.text.text", text.Text);
                mainTemplates.Add(t);
            }
            foreach(var text in _sub) {
                var t = cardTemplate.LoadTemplate("card.text");
                t.InsertVariable("card.text.type", text.Type.ToString().ToLower());
                t.InsertVariable("card.text.text", text.Text);
                subTemplates.Add(t);
            }
            foreach(var text in _tag) {
                var t = cardTemplate.LoadTemplate("card.text");
                t.InsertVariable("card.text.type", text.Type.ToString().ToLower());
                t.InsertVariable("card.text.text", text.Text);
                tagTemplates.Add(t);
            }
            // Actions
            foreach (var action in _actions) {
                var t = cardTemplate.LoadTemplate("card.actions.action");
                if (string.IsNullOrEmpty(action.URL) == false) {
                    t.InsertVariable("action.url", action.URL);
                }
                else if (string.IsNullOrEmpty(action.Script) == false) {
                    var script = action.Script;
                    var prefix = "javascript:";
                    if (script.StartsWith(prefix) == false) {
                        script = prefix + script;
                    }
                    t.InsertVariable("action.url", script);
                }
                t.InsertVariable("action.title", action.Title);
                actionTemplates.Add(t);
            }
            // Breadcrumb
            if (_breadcrumbs != null) {
                var t = cardTemplate.LoadTemplate("card.breadcrumb");
                var crumbTemplates = new List<Templates.PageTemplate>();
                foreach(var crumb in _breadcrumbs) {
                    var tt = t.LoadTemplate("card.breadcrumb.item");
                    var crumbClasses = "";
                    if (_selectedBreadcrumb == crumb) crumbClasses += " selected";
                    tt.InsertVariable("item.title",crumb);
                    tt.InsertVariable("item.class",crumbClasses);
                    crumbTemplates.Add(tt);
                }
                t.InsertTemplates("breadcrumb.items", crumbTemplates);
                attachmentTemplates.Add(t);
            }
            // Compile extra classes
            if (this.IsGrouping) _classes.Add("is-grouping");
            string classes = string.Join(" ",_classes);
            // Insert all variables
            cardTemplate.InsertVariable("card.title", _main?.FirstOrDefault().Text);
            cardTemplate.InsertVariable("card.classes", classes);
            cardTemplate.InsertVariable("card.type",_typeName);
            cardTemplate.InsertVariable("card.icon",_iconName);
            cardTemplate.InsertVariable("card.id",_cardID);
            cardTemplate.InsertTemplates("card.main",mainTemplates);
            cardTemplate.InsertTemplates("card.sub",subTemplates);
            cardTemplate.InsertTemplates("card.tag",tagTemplates);

            if (actionTemplates.Any()) {
                var actionsTemplate = cardTemplate.LoadTemplate("card.actions");
                actionsTemplate.InsertTemplates("actions", actionTemplates);
                cardTemplate.InsertTemplate("card.actions", actionsTemplate);
            }
            else{
                cardTemplate.InsertVariable("card.actions",string.Empty);
            }
         

            cardTemplate.InsertTemplates("card.attachments",attachmentTemplates);
            template.InsertTemplate(variableName,cardTemplate);
        }

        #endregion
    }
}
