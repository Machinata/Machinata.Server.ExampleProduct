using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Bundles {

    public class CSSBundleGenerator : BundleGenerator {

        public override bool ShouldExtractVariables(string file) {
            return true;
        }

        public override bool IsLineAVariableDefinition(string line) {
            if (line.Trim().StartsWith("$")) return true;
            else return false;
        }

        public override void ExtractVariableFromLine(string line) {
            var regex = new Regex("\\$([a-zA-Z0-9-.]+):( )*([a-zA-Z0-9-.,()#%]+)(;)");
            var variableMatches = regex.Matches(line);
            foreach(Match match in variableMatches) {
                string varName = match.Groups[1].Value;
                string varValue = match.Groups[3].Value;
                // Overwrite if exists
                if (Variables.ContainsKey(varName)) Variables[varName] = varValue;
                else Variables.Add(varName, varValue);
                _logger.Trace("  Found variable "+varName);
            }
        }

        public override void InsertVariable(StringBuilder contents, string name, string value) {
            contents.Replace("$" + name, value);
        }

        public override bool ShouldCompressFile(string file) {
            if (!file.Contains(".min.css")) return true;
            else return base.ShouldCompressFile(file);
        }

        public override void Compress(StringBuilder contents) {
            if (contents == null || contents.Length == 0) return;

            var ret = Core.Util.CSS.Minify(contents.ToString());
            contents.Clear();
            contents.Append(ret);
                
        }
    }

}
