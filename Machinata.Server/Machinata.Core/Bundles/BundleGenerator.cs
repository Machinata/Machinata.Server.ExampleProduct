using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Bundles {

    public abstract class BundleGenerator {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        protected static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        private StringBuilder _bundleContents = new StringBuilder();
        private Dictionary<string, string> _variables = new Dictionary<string, string>();

        protected Dictionary<string,string> Variables {
            get {
                return _variables;
            }
        }

        public string Package;
        public Core.Model.Theme Theme;
        public string Language;
        public bool Debug;
        public string Profile;
        
        private const string HEADER_LINE =  "==================================================================================================";
        private const string FILE_LINE =    "--------------------------------------------------------------------------------------------------";

        public void Load(string package, string path, Core.Model.Theme theme, string language, bool debug, string profile, string outputFormat, bool allowHotswapping=true) {
            
            this.Package = package;
            this.Theme = theme;
            this.Language = language;
            this.Debug = debug;
            this.Profile = profile;

            // Get the config path
            string bundlePath = Core.Config.StaticPath + Core.Config.PathSep + path.Replace("/",Core.Config.PathSep);
            string bundleConfigPathJSON = bundlePath + Core.Config.PathSep + "bundle.json";
            string bundleConfigPathTxt = bundlePath + Core.Config.PathSep + "bundle.txt";
            if (allowHotswapping) {
                bundleConfigPathJSON = Core.Util.Files.GetHotSwappableFile(bundleConfigPathJSON, package);
                bundleConfigPathTxt = Core.Util.Files.GetHotSwappableFile(bundleConfigPathTxt, package);
            }
            BundleMeta bundleMeta = null;

            // What format exists?
            // Newer JSON format has priority...
            if (System.IO.File.Exists(bundleConfigPathJSON)) {

                // JSON format exists!
                _logger.Trace("Bundle config "+bundleConfigPathJSON);
                bundleMeta = BundleMeta.LoadFromJSON(bundleConfigPathJSON);

            } else if (System.IO.File.Exists(bundleConfigPathTxt)) {
                
                // Txt format exists!
                _logger.Trace("Bundle config "+bundleConfigPathTxt);
                bundleMeta = BundleMeta.LoadFromTxt(bundleConfigPathTxt);

            } else {
                throw new Backend404Exception("not-found","Could not find the bundle " + path);
            }

            // Custom output format?
            if (outputFormat != null) bundleMeta.OutputFormat = outputFormat;

            // Insert all theme variables
            _variables.Add("language", language);
            _variables.Add("theme-name", theme.Name);
            _variables.Add("theme-title", theme.Title);
            _variables.Add("theme-is-default", theme.SetAsDefault.ToString().ToLower());
            foreach(var key in theme.Properties.Keys) {
                var keyStr = key.ToDashedLower();
                var valObj = theme.Properties[key];
                var valStr = valObj?.ToString();
                _variables.Add(keyStr, valStr);
                _variables.Add("theme."+ keyStr, valStr);
                double num;
                if (double.TryParse(valStr, out num)) {
                    _variables.Add(keyStr+"-px", valStr+"px");
                    _variables.Add("theme." + keyStr + "-px", valStr + "px");
                }
            }
            // Additional standard variables
            _variables.Add("public-url", Core.Config.CDNURL);
            _variables.Add("cdn-url", Core.Config.CDNURL);
            _variables.Add("build-id", Core.Config.BuildID);
            _variables.Add("build-version", System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString());
            if (Core.Config.BuildTimestamp != null && Core.Config.BuildTimestamp != DateTime.MinValue) {
                _variables.Add("build-timestamp", Core.Config.BuildTimestamp.ToUniversalTime().ToString("o"));
                _variables.Add("build-date", DateTime.UtcNow.ToString(Core.Config.DateTimeFormat)+" UTC");
            } else {
                _variables.Add("build-timestamp", "");
                _variables.Add("build-date", "");
            }
            _variables.Add("build-user", Core.Config.BuildUser);
            _variables.Add("build-machine", Core.Config.BuildMachine);
            _variables.Add("debug", debug.ToString().ToLower());
            _variables.Add("build-profile", profile);
            
            // Do config variables
            if (ShouldInsertConfigVariables(null)) {
                _variables.Add("config.date-format", Core.Config.DateFormat);
                _variables.Add("config.date-time-format", Core.Config.DateTimeFormat);
                _variables.Add("config.time-format", Core.Config.TimeFormat);
                _variables.Add("config.default-timezone", Core.Config.DefaultTimezone);
                _variables.Add("config.date-range-separator", Core.Config.DateRangeSeperator);
                _variables.Add("config.maps-access-token", Core.Config.MapsAccessToken);
            }

            // Bundle header
            {
                AddComment(HEADER_LINE);
                AddComment("" + path);
                if (bundleMeta.Credits != null) AddComment($"{bundleMeta.Credits.Replace("{year}",DateTime.UtcNow.Year.ToString()).Trim().Replace("\n", ", ")}");
                if (bundleMeta.License != null) AddComment($"License: {bundleMeta.License.Split('\n').First().Trim()}");
                if (bundleMeta.Link != null) AddComment($"Link: {bundleMeta.Link.Trim()}");
                if(profile != null) AddComment($"Profile: {profile}");
                if(debug == true) AddComment($"Debug: yes");
                AddComment($"Language: {language}");
                AddComment($"Package: {package}");
                AddComment($"Theme: {theme.Name}");
                AddComment($"Build: {Core.Config.BuildID}");
                AddComment($"Date: {DateTime.UtcNow.ToString(Core.Config.DateTimeFormat)} UTC");
                AddComment(HEADER_LINE);
                AddLine("");
                AddLine("");
            }

            // Pre process
            PreProcess(bundleMeta, _bundleContents);

            // Bundle files
            bool didAddFile = false;
            foreach (var bundleFile in bundleMeta.Files) {
                _logger.Trace("  File: " + bundleFile.Filename);

                // Profile matches?
                if (profile != null) {
                    bool matches = false;
                    if (bundleFile.Profiles.Contains("*")) matches = true;
                    if (bundleFile.Profiles.Contains(profile)) matches = true;
                    if (bundleFile.Profiles.Contains("+"+profile)) matches = true;
                    if (!matches) continue;
                } else {
                    bool matches = true;
                    if (bundleFile.Profiles != null && bundleFile.Profiles.Any(p => p.StartsWith("+"))) matches = false;
                    if (!matches) continue;
                }
                didAddFile = true;

                // Load file
                if (!System.IO.File.Exists(bundleFile.Filepath)) throw new Backend404Exception("not-found", "Could not find the bundle file " + path + "/" + bundleFile.Filename);
                var bundleLines = new StringBuilder();


                //TODO: do we need to get the encoding somehow on per-file basis? Or can we do ISO 8859 for everything?
                var bundleFilePath = Core.Util.Files.GetHotSwappableFile(bundleFile.Filepath, package);
                var bundleEncoding = System.Text.Encoding.GetEncoding("iso-8859-1"); // Allows support for extended ascii characters
                var bundleContents = System.IO.File.ReadAllText(bundleFile.Filepath, bundleEncoding);
                bundleContents = AlterBundleFile(bundleContents, bundleMeta, bundleFile);

                // Do variable extraction
                if (ShouldExtractVariables(bundleFile.Filename) && bundleFile.IsVendor == false) {
                    // Split file into lines and process each looking for variable defintions
                    using (StringReader sr = new StringReader(bundleContents)) {
                        string line;
                        while ((line = sr.ReadLine()) != null) {
                            if (IsLineAVariableDefinition(line)) {
                                ExtractVariableFromLine(line);
                            } else {
                                bundleLines.AppendLine(line);
                            }
                        }
                    }
                } else {
                    bundleLines.Append(bundleContents);
                }

                // Do inline translations
                if (ShouldInsertTextVariables(bundleFile.Filename) && bundleFile.IsVendor == false) {
                    Core.Localization.TextParser.ParseDataForTextVariables(bundleFile.Filepath, package, bundleLines);
                    Core.Localization.TextParser.InsertTextVariablesForData(package, bundleLines, language);
                    //TODO: how do we know which icon bundle to use? @dankrusi
                    Core.Localization.TextParser.InsertIconVariablesForData(package, bundleLines, language);
                }

                // Dont do variables on vendor files...
                if (bundleFile.IsVendor == false) {
                    // Replace all variables
                    foreach (var varName in _variables.Keys.OrderByDescending(k => k.Length)) {
                        InsertVariable(bundleLines, varName, _variables[varName]);
                    }

                    // Constants
                    if (language != null) {
                        var countries = CountryCodes.GetCountriesAsJson(language);
                        InsertVariable(bundleLines, "const.countries", countries);
                        InsertVariable(bundleLines, "const.country-codes-western", JSON.Serialize(CountryCodes.CodesWestern));
                        InsertVariable(bundleLines, "const.country-codes-euefta", JSON.Serialize(CountryCodes.CodesEUEFTA));
                        InsertVariable(bundleLines, "const.country-codes-asia", JSON.Serialize(CountryCodes.CodesAsia));
                    }

                    // Entire theme JSON as variable
                    InsertVariable(bundleLines, "theme.json", theme.Properties.Data);
                }

                // Process

                // Compress?
                if (!debug) {
                    if (Core.Config.CompressionBundleEnabled && bundleFile.Compress && this.ShouldCompressFile(bundleFile.Filename)) {
                        try {
                            Compress(bundleLines);
                        } catch (Exception e) {
                            throw new Exception("Could not compress file " + bundleFile.Filename, e);
                        }
                    }
                }

                // Append to bundle
                AddComment(FILE_LINE);
                AddComment("" + bundleFile.Filename);
                if(bundleFile.Credits != null) AddComment($"{bundleFile.Credits.Replace("{year}",DateTime.UtcNow.Year.ToString()).Trim().Replace("\n",", ")}");
                if(bundleFile.License != null) AddComment($"License: {bundleFile.License.Split('\n').First().Trim()}");
                if(bundleFile.Link != null) AddComment($"Link: {bundleFile.Link.Trim()}");
                AddComment($"File: {path + "/" + bundleFile.Filename}");
                if (ShouldInsertTextVariables(bundleFile.Filename)) AddComment($"Language: {language}");
                AddComment(FILE_LINE);
                AddLine("");
                _bundleContents.Append(bundleLines.ToString());
                AddLine("");
                AddLine("");
            }

            // Post process
            PostProcess(bundleMeta, _bundleContents);

            if (didAddFile == false) {
                throw new BackendException("invalid-profile",$"The bundle profile '{profile}' is not valid. No files matched the profile or the bundle has no files.");
            }
        }

        public virtual string ExtractIdForFile(BundleMeta bundleMeta, BundleMetaFile bundleFile) {
            var id = bundleFile.Filename; // fallback
            if (bundleMeta.IdPattern != null) {
                var regexPattern = bundleMeta.IdPattern.Replace(".", "\\.").Replace("*", "(.*)");
                var regexId = new Regex(regexPattern, RegexOptions.IgnoreCase);  // See regex test https://regexr.com/4u729
                var matchesId = regexId.Matches(bundleFile.Filename);
                if (matchesId.Count == 1 && matchesId[0].Groups.Count == 2) {
                    id = matchesId[0].Groups[1].Value;
                }
            }
            id = id.Replace("\"", "-");
            id = id.Replace(" ", "-");
            id = id.Replace("_", "-");
            id = id.ReplaceSuffix("-1","");
            return id;
        }


        public virtual string AlterBundleFile(string bundleContents, BundleMeta bundleMeta, BundleMetaFile bundleFile) {
            return bundleContents;
        }


        #region Variable Extraction

        public virtual bool ShouldExtractVariables(string file) {
            return false;
        }

        public virtual void ExtractVariableFromLine(string line) {}

        public virtual bool IsLineAVariableDefinition(string line) {
            return false;
        }

        #endregion


        public virtual bool ShouldInsertConfigVariables(string file) {
            return false;
        }

        public virtual bool ShouldInsertTextVariables(string file) {
            return false;
        }

        public virtual bool ShouldCompressFile(string file) {
            return false;
        }

        public virtual void AddComment(string comment) {
            _bundleContents.AppendLine("/* " + comment + " */");
        }

        public virtual void AddLine(string line) {
            _bundleContents.AppendLine(line);
        }

        public virtual void InsertVariable(StringBuilder contents, string name, string value) {

        }

        public virtual void PreProcess(BundleMeta bundleMeta, StringBuilder contents) {

        }

        public virtual void PostProcess(BundleMeta bundleMeta, StringBuilder contents) {

        }

        public virtual void Compress(StringBuilder contents) {
            
        }

        public virtual void WriteToFile(string filepath) {
            System.IO.File.WriteAllText(filepath, _bundleContents.ToString());
        }
    }
}
