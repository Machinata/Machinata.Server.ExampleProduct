using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Bundles {

    public class BundleMeta {
        
        public string Filepath;
        public string Link;
        public string License;
        public string Credits;

        public string IdPattern;
        public string OutputFormat;

        public JToken Config;

        public List<BundleMetaFile> Files = new List<BundleMetaFile>();

        public static BundleMeta LoadFromJSON(string bundleConfigPath) {
            
            var bundleMeta = new BundleMeta();
            var json = Core.JSON.ParseJsonAsJObject(System.IO.File.ReadAllText(bundleConfigPath), false);
            
            // Meta stuff...
            string licenseFile = bundleConfigPath.Replace(".json",".license");
            if(System.IO.File.Exists(licenseFile)) bundleMeta.License = System.IO.File.ReadAllText(licenseFile);
            string creditsFile = bundleConfigPath.Replace(".json",".credits");
            if(System.IO.File.Exists(creditsFile)) bundleMeta.Credits = System.IO.File.ReadAllText(creditsFile);
            string linkFile = bundleConfigPath.Replace(".json",".link");
            if(System.IO.File.Exists(linkFile)) bundleMeta.Link = System.IO.File.ReadAllText(linkFile);
            var jsonMeta = json["meta"];
            if(jsonMeta != null) {
                if(jsonMeta["license"] != null) bundleMeta.License = jsonMeta["license"].ToString();
                if(jsonMeta["credits"] != null) bundleMeta.Credits = jsonMeta["credits"].ToString();
                if (jsonMeta["link"] != null) bundleMeta.Link = jsonMeta["link"].ToString();
            }

            // Config stuff
            var jsonConfig = json["config"];
            if(jsonConfig != null) {
                if (jsonConfig["id-pattern"] != null) bundleMeta.IdPattern = jsonConfig["id-pattern"].ToString();
                if (jsonConfig["output-format"] != null) bundleMeta.OutputFormat = jsonConfig["output-format"].ToString();
            }
            bundleMeta.Config = jsonConfig;

            // Files
            var bundlePath = System.IO.Path.GetDirectoryName(bundleConfigPath);
            if (json["files"].Type == Newtonsoft.Json.Linq.JTokenType.String) {
                // Auto mode, just all all files matching the pattern
                var matchingFiles = System.IO.Directory.GetFiles(bundlePath, json["files"].ToString());
                foreach(var file in matchingFiles) {
                    var bundleMetaFile = new BundleMetaFile();
                    bundleMetaFile.Filename = Core.Util.Files.GetFileNameWithExtension(file);
                    bundleMetaFile.Filepath = file;
                    bundleMetaFile.Profiles = new List<string>();
                    bundleMetaFile.Profiles.Add("*");
                    bundleMeta.Files.Add(bundleMetaFile);
                }
            } else {
                var jsonFiles = json["files"];
                if (jsonFiles == null) throw new Exception("Bundle JSON does not contain files array!");
                foreach (var jsonFile in jsonFiles) {
                    // File path
                    var bundleMetaFile = new BundleMetaFile();
                    bundleMetaFile.Filename = jsonFile["file"].ToString();
                    bundleMetaFile.Filepath = bundlePath + Core.Config.PathSep + bundleMetaFile.Filename;
                    bundleMetaFile.JSON = jsonFile as JObject;

                    // Meta
                    string licenseFile2 = bundleMetaFile.Filepath + ".license";
                    if (System.IO.File.Exists(licenseFile2)) bundleMetaFile.License = System.IO.File.ReadAllText(licenseFile2);
                    string creditsFile2 = bundleMetaFile.Filepath + ".credits";
                    if (System.IO.File.Exists(creditsFile2)) bundleMetaFile.Credits = System.IO.File.ReadAllText(creditsFile2);
                    string linkFile2 = bundleMetaFile.Filepath + ".link";
                    if (System.IO.File.Exists(linkFile2)) bundleMetaFile.Link = System.IO.File.ReadAllText(linkFile2);
                    if (jsonFile["license"] != null) bundleMetaFile.License = jsonFile["license"].ToString();
                    if (jsonFile["credits"] != null) bundleMetaFile.Credits = jsonFile["credits"].ToString();
                    if (jsonFile["link"] != null) bundleMetaFile.Link = jsonFile["link"].ToString();
                    if (jsonFile["profiles"] != null) bundleMetaFile.Profiles = jsonFile["profiles"].ToString().Split(',').ToList();
                    if (jsonFile["comment"] != null) bundleMetaFile.Comment = jsonFile["comment"].ToString();
                    if (jsonFile["compress"] != null) bundleMetaFile.Compress = (jsonFile["compress"].ToString().ToLower() == "false" ? false : true);
                    if (jsonFile["is-vendor"] != null) bundleMetaFile.IsVendor = (jsonFile["is-vendor"].ToString().ToLower() == "false" ? false : true);
                    

                    // Register
                    bundleMeta.Files.Add(bundleMetaFile);
                }
            }
            return bundleMeta;
        }

        public static BundleMeta LoadFromTxt(string bundleConfigPath) {
            var bundleMeta = new BundleMeta();
            var configLines = System.IO.File.ReadAllLines(bundleConfigPath);

            // Meta stuff...
            string licenseFile = bundleConfigPath.Replace(".txt",".license");
            if(System.IO.File.Exists(licenseFile)) bundleMeta.License = System.IO.File.ReadAllText(licenseFile);
            string creditsFile = bundleConfigPath.Replace(".txt",".credits");
            if(System.IO.File.Exists(creditsFile)) bundleMeta.Credits = System.IO.File.ReadAllText(creditsFile);
            string linkFile = bundleConfigPath.Replace(".txt",".link");
            if(System.IO.File.Exists(linkFile)) bundleMeta.Link = System.IO.File.ReadAllText(linkFile);

            // Files
            var bundlePath = System.IO.Path.GetDirectoryName(bundleConfigPath);
            foreach(var configLine in configLines) {
                // Skip comments
                if (configLine.StartsWith("#") || configLine.Trim() == "") continue;

                // File path
                var bundleMetaFile = new BundleMetaFile();
                bundleMetaFile.Filename = configLine;
                bundleMetaFile.Filepath = bundlePath + Core.Config.PathSep + configLine;
                
                // Meta
                string licenseFile2 = bundleMetaFile.Filepath + ".license";
                if(System.IO.File.Exists(licenseFile2)) bundleMetaFile.License = System.IO.File.ReadAllText(licenseFile2);
                string creditsFile2 = bundleMetaFile.Filepath + ".credits";
                if(System.IO.File.Exists(creditsFile2)) bundleMetaFile.Credits = System.IO.File.ReadAllText(creditsFile2);
                string linkFile2 = bundleMetaFile.Filepath + ".link";
                if(System.IO.File.Exists(linkFile2)) bundleMetaFile.Link = System.IO.File.ReadAllText(linkFile2);

                // Register
                bundleMeta.Files.Add(bundleMetaFile);
            }

            return bundleMeta;
        }
    }

    public class BundleMetaFile {
        
        public string Filename;
        public string Filepath;
        public string Comment;
        public string Link;
        public string License;
        public string Credits;
        public List<string> Profiles;
        public bool Compress = true;
        public bool IsVendor = false;

        public JObject JSON = null;

    }
}
