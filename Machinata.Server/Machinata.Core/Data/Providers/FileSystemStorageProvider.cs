using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using System;
using System.IO;

namespace Machinata.Core.Data {
    public class FileSystemStorageProvider : IStorageProvider {

        public FileSystemStorageProvider() {
            var cacheDir = GetCacheDirectory();
            var exists = Directory.Exists(cacheDir);
            if (!exists) {
                Directory.CreateDirectory(cacheDir);
            }
        }

        public bool DeleteFile(string key,string extension) {
            var path = Path.Combine(GetCacheDirectory(), GetStorageKey(key,extension));
            if (!File.Exists(path)) {
                //TODO:@micha, shouldnt return false?
                //throw new BackendException("file-not-found", $"File {key} not found");
                return false;
            }
            File.Delete(path);
            return true;
        }

        public string GetFile(string key,string extension) {
            var storageKey = GetStorageKey(key, extension);
            if (string.IsNullOrEmpty(storageKey)) {
                throw new BackendException("no-key-defined", "No file key was provided");
            }
            
            var path = Path.Combine(GetCacheDirectory(), storageKey);
            if (!File.Exists(path)) {
                throw new BackendException("file-not-found", $"File {path} not found");
            }
            return path;
        }

       
        public string GetProviderName() {
            return GetSchema();
        }

        public string GetSchema() {
            return "file";
        }
        private static string GetCacheDirectory() {
            return Core.Config.FileStorageProviderPath;
        }

        public string PutFile(string fileName,string extension, Stream dataStream) {

            var guid = Guid.NewGuid().ToString();
            var storageFileName = guid + "." + extension;
            var path = Path.Combine(GetCacheDirectory(), storageFileName);

            FileStream fileStream = File.Create(path, (int)dataStream.Length);
            byte[] bytesInStream = new byte[dataStream.Length];
            dataStream.Read(bytesInStream, 0, bytesInStream.Length);
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
            fileStream.Close();
            dataStream.Close();
            return guid;
        }

        private static string GetStorageKey(string key, string extension) {
            return $"{key}.{extension}";
        }
    }
}