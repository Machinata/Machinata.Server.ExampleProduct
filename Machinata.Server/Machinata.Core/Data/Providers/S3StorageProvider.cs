using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.IO;

namespace Machinata.Core.Data {
    public class S3StorageProvider : IStorageProvider {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// 
        /// hint: IAM Role
        /// http://mikeferrier.com/2011/10/27/granting-access-to-a-single-s3-bucket-using-amazon-iam/
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        public bool DeleteFile(string key,string extension) {
            
            // Delete cached local file
            var cachedFilePath = GetCachePathForKey(key,extension);
            if (File.Exists(cachedFilePath)) {
                File.Delete(cachedFilePath);
            }

            string bucketName = Config.S3DefaultBucket;

            // Delete from S3
            DeleteFromS3(bucketName, key);

            return true;
           
        }

        public string GetFile(string key,string extension) {
            string cachePath = GetCacheDirectory();
            string cacheStoragePath = GetCachePathForKey(key,extension);
            string bucketName = Config.S3DefaultBucket;
            string storageKey = GetStorageKey(key, extension);

            Caching.FileCacheGenerator.CreateIfNeeded(cacheStoragePath, () => {
                // Init
                _logger.Trace("Downloading file from s3 " + key);

                // Get file from s3
                DownloadFromS3(bucketName, storageKey, cachePath);

            });
            return cacheStoragePath;
        }

        private static string GetCacheDirectory() {
            return Path.Combine(Core.Config.CachePath, "s3");
        }
        private static string GetCachePathForKey(string key,string extension) {
            return Path.Combine(GetCacheDirectory(), GetStorageKey(key,extension));
        }

        private static string GetStorageKey(string key, string extension) {
            return $"{key}.{extension}";
        }

        public string GetProviderName() {
            return GetSchema();
        }

        public string GetSchema() {
            return "s3";
        }


        public string PutFile(string fileName, string extension, Stream dataStream) {

            string folder = Config.S3DefaultFolder;
            string bucket = Config.S3DefaultBucket;
          
            string guid = Guid.NewGuid().ToString();

            // Create timestamp
            string timestamp = Encryption.DefaultHasher.HashString(DateTime.Now.ToFileTimeUtc().ToString());

            // Prepare to put file into s3 bucket
            string key = Path.Combine(folder, guid) + "-" + timestamp;
            string storeKey = GetStorageKey(key, extension);
            UploadDataToS3(dataStream, bucket, storeKey);

            return key;
        }

        private void UploadDataToS3(Stream data, string bucketName, string key, bool allowPublicRead = false) {
            using (var client = GetS3Client()) {

                S3CannedACL acl = S3CannedACL.AuthenticatedRead;
                if (allowPublicRead) { acl = S3CannedACL.PublicRead; }

                try {
                    PutObjectRequest putRequest = new

                        PutObjectRequest {
                        BucketName = bucketName,
                        Key = key,
                        CannedACL = acl,
                        InputStream = data
                        // FilePath = filePath,
                        // ContentType = "text/plain"
                    };

                    PutObjectResponse response = client.PutObject(putRequest);

                } catch (AmazonS3Exception amazonS3Exception) {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity"))) {
                        throw new Exception("Check the provided AWS Credentials.");
                    } else {
                        throw new Exception("Error occurred: " + amazonS3Exception.Message);
                    }
                }
            }
        }

        private static AmazonS3Client GetS3Client() {
            return new AmazonS3Client(Config.AWSAccessKey,Config.AWSSecretKey, RegionEndpoint.USEast1);
        }

        private void DownloadFromS3(string bucketName, string key, string pathToDownload) {
            using (var client = GetS3Client()) {

                GetObjectRequest request = new GetObjectRequest {
                    BucketName = bucketName,
                    Key = key
                };

                using (GetObjectResponse response = client.GetObject(request)) {
                    string dest = Path.Combine(pathToDownload, key);
                    if (!File.Exists(dest)) {
                        response.WriteResponseStreamToFile(dest);
                    }
                }
            }
        }

        private void DeleteFromS3(string bucketName, string key) {
            using (var client = GetS3Client()) {

                DeleteObjectRequest request = new DeleteObjectRequest {
                    BucketName = bucketName,
                    Key = key
                };

                var response = client.DeleteObject(request);
            }
        }
    } 
}