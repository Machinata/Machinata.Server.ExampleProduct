﻿

wizard.dont-show-tip-again.en=Don’t show this tip again
wizard.dont-show-tip-again.de=Dieser Tipp nicht mehr anzeigen


wizard.reset-all-tips.en=Reset tips
wizard.reset-all-tips.tooltip.en=Reset all tips to show again
wizard.reset-all-tips.success.en=Tips have been reset. You will now see the tips again while browsing.

wizard.reset-all-tips.de=Tipps zurücksetzen
wizard.reset-all-tips.tooltip.de=Alle Tipps zurücksetzen um sie erneut anzuzeigen
wizard.reset-all-tips.success.de=Die Tipps wurden zurückgesetzt. Sie sehen die Tipps nun wieder beim Browsen.