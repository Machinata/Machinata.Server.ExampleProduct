using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Localization {

    public partial class Text {

        /// <summary>
        /// Defines the default language for inline language definitions or any text definition 
        /// where the language is not set.
        /// </summary>
        public const string DEFAULT_INLINE_LANGUAGE_ID = "en"; // This should not be changed

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {
            LoadAllLocalizationTextsFromFiles();
            //TODO:@micha @dan this breaks the seed on a new database
            if (Core.Config.LocalizationEnableDatabase) {
                using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                    LoadAllLocalizationTextsFromDB(db);
                }
            }
        }


        public static void LoadAllLocalizationTextsFromFiles() {
            // Discover all localization files
            // These are stored as txt files for each package
            string textExtension = ".txt";
            _logger.Trace("Searching in "+Core.Config.LocalizationPath+" for "+textExtension+"...");
            string[] localizationFiles = Core.Util.Files.GetFiles(
                Core.Config.LocalizationPath, 
                "*"+textExtension,
                SearchOption.AllDirectories);
            foreach(var templateFilePath in localizationFiles) {
                // Get the filename
                var templateFile = templateFilePath.Replace(Core.Config.LocalizationPath, "").TrimStart(Core.Config.PathSep[0]);
                templateFile = templateFile.Replace(Core.Config.PathSep, "/");
                // Break it down into package and file
                var templateFileSegs = templateFile.Split('/');
                var templatePackage = templateFileSegs.First();
                var templateFileName = string.Join("/",templateFileSegs.Skip(1).Take(templateFileSegs.Length - 1).ToArray());
                var templateName = Core.Util.Files.GetPathWithoutExtension(templateFileName);
                var templateExtension = Core.Util.Files.GetFileExtension(templateFileName);
                _logger.Trace("  Discovered "+templateName + " ("+templateExtension+") in "+templatePackage);
                // Load the file
                foreach (var line in System.IO.File.ReadAllLines(templateFilePath)) {
                    if (line.Trim() == "" || line.StartsWith("#")) continue;
                    // Find key
                    int equalIndex = line.IndexOf('=');
                    string key = line.Substring(0, equalIndex);
                    string value = line.Substring(equalIndex + 1);
                    // Extract language from key
                    string language = null;
                    string id = key;
                    if(Regex.IsMatch(key,"(.*)\\.[a-zA-Z-][a-zA-Z-]")) {
                        id = key.Substring(0,key.LastIndexOf('.'));
                        language = key.Substring(key.LastIndexOf('.') + 1);
                    }
                    //_logger.Trace("    Found "+id + "="+value + " (lang="+language+")");
                    // Register
                    Text.RegisterText(templateFileName, templatePackage, id, value, language, SourceTypes.LocalizationFile);
                }
                
            }
        }

        /// <summary>
        /// Loads all localization texts from database.
        /// </summary>
        /// <param name="db">The database.</param>
        public static void LoadAllLocalizationTextsFromDB(ModelContext db) {
            _logger.Info("Loading Localizations from DB...");
            var texts = db.LocalizationTexts();
            foreach (var text in texts) {
                _logger.Trace($"Registring text variable: {text.TextId}, {text.Language}, {text.Language}");
                Text.RegisterText(text.Source, text.Package, text.TextId, text.Value, text.Language, SourceTypes.Database);

            }
            _logger.Info($"Loaded {texts.Count()} text variables from db.");
        }

       

        /// <summary>
        /// Registers a text in the cache using package, id, value and language.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public static Text RegisterText(string source, string package, string id, string value, string language, SourceTypes sourceType) {
            // Init
            if (language == null) language = DEFAULT_INLINE_LANGUAGE_ID;
            // Validate
            if (language.Length > 3) {
                var msg = $"The language '{language}' is not valid (text id {id} in package {package} in {source}).";
                _logger.Error(msg);
                //throw new Exception(msg);
                return null;
            }
            // Construct text object
            Text text = new Text();
            text.Id = id;
            text.Package = package;
            text.Language = language.ToLower();
            text.Value = value;
            text.Source = source;
            text.SourceType = sourceType;
            // Pass on
            return RegisterText(text);
        }

        /// <summary>
        /// Registers a text in the cache using a Text object. If a default is not set, then
        /// the default will be set using the id.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static Text RegisterText(Text text) {
            // Validate
            if (text.Language == null) throw new Exception("You cannot register a text without a language set.");
            // Register by id on path, but only if the langauge is set
            if (text.Language != null) {
              
                if (!_texts.ContainsKey(text.Id)) {
                    _texts[text.Id] = new Dictionary<string, List<Text>>();
                }

                if (!string.IsNullOrEmpty(text.Value)) {
                    if (!_texts[text.Id].ContainsKey(text.Language)) {
                        _texts[text.Id][text.Language] = new List<Text>();
                    }
                       _texts[text.Id][text.Language].Add(text);
                }
               
            }
            return text;
        }


        public static string GetTranslatedRoute(string id, string language) {
            if (string.IsNullOrEmpty(language)) {
                language = Core.Config.LocalizationDefaultLanguage;
            }
            var translated = GetTranslatedTextByIdForLanguage(id, language, null);
            if (translated == null) {
                return id;
            }
            return translated;
        }

        /// <summary>
        /// Gets the text by identifier for the language. If the language does not contain the text by id,
        /// then the default is returned (just the id). If that is not found, then the default value is
        /// returned (usually null).
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="language">The language.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetTranslatedTextByIdForLanguage(string id, string language = "en", string defaultValue = null) {
            // Try to get full translation for language
            if (_texts.ContainsKey(id) && _texts[id].ContainsKey(language) && _texts[id][language].Any(t=>!string.IsNullOrEmpty(t.Value))) {
                return _texts[id][language].Last(t => !string.IsNullOrEmpty(t.Value)).Value;
            }
            // Try to get a fallback translation using the default language
            if (Core.Config.LocalizationEnableFallback) {
                if (_texts.ContainsKey(id)) {
                    if (_texts[id].ContainsKey(Core.Config.LocalizationDefaultLanguage) && _texts[id][Core.Config.LocalizationDefaultLanguage].Any(t => !string.IsNullOrEmpty(t.Value))) {
                        return _texts[id][Core.Config.LocalizationDefaultLanguage].Last(t => !string.IsNullOrEmpty(t.Value)).Value;
                    }
                    if (_texts[id].ContainsKey(DEFAULT_INLINE_LANGUAGE_ID) && _texts[id][DEFAULT_INLINE_LANGUAGE_ID].Any(t => !string.IsNullOrEmpty(t.Value))) {
                        return _texts[id][DEFAULT_INLINE_LANGUAGE_ID].Last(t => !string.IsNullOrEmpty(t.Value)).Value;
                    }
                }
            }
            // Return the provided default
            return defaultValue;
        }

        /// <summary>
        /// Gets the text by identifier in the configured localization default language
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetTranslatedTextById(string id, string defaultValue = null) {
            return GetTranslatedTextByIdForLanguage(id, Core.Config.LocalizationDefaultLanguage, defaultValue);
        }

        /// <summary>
        /// Unregisters a text from cache
        /// </summary>
        /// <param name="textId">The text identifier.</param>
        /// <param name="language">The language.</param>
        /// <param name="type">The type.</param>
        public static void UnRegisterText(string textId, string language, SourceTypes type) {
            if (_texts.ContainsKey(textId) && _texts[textId].ContainsKey(language)) {
                _texts[textId][language].RemoveAll(tl => tl.SourceType == type);
                // Remove key if no more entries
                if (!_texts[textId][language].Any()) {
                    _texts[textId].Remove(language);
                }
            }
        }

    }

}
