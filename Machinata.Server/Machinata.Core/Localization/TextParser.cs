using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Localization {

    public class TextParser {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants
        
        public const string TEXT_VARIABLE_REGEX = "{text.([a-zA-Z0-9-.]+)}";
        public const string TEXT_VARIABLE_REGEX_INLINE = "{text.([a-zA-Z0-9-.]+)=[^}\n]*}"; //"{text.([a-zA-Z0-9-.]+)=(.*)}" -> this didn't stop at first closing }
        public const string ICON_VARIABLE_REGEX = "{icon.([a-zA-Z0-9-.]+)}";

        #endregion

        public static void ParseDataForTextVariables(string source, string package, StringBuilder data) {

            // Register text ids withou inline translations
            ParseDataWithoutInlineTranslation(source, package, data);

            // Get matches for all text variables
            Regex textVariableMatchesRegex = new Regex(TEXT_VARIABLE_REGEX_INLINE);  
            var textVariableMatches = textVariableMatchesRegex.Matches(data.ToString());
            // Process each match
            foreach (var match in textVariableMatches) {
                string varName = match.ToString().Trim('{').Trim('}');
                if(varName.StartsWith("text.")) {
                    // Does it contain a inline translation?
                    if(varName.Contains("=")) {
                        int equalIndex = varName.IndexOf('=');
                        var textId = varName.Substring(0, equalIndex).ReplacePrefix("text.","");
                        string textLang = null;
                        // Check if a language is definded in the text id
                        if(Regex.IsMatch(textId,"(.*)\\.[a-zA-Z-][a-zA-Z-]")) {
                            textLang = textId.Substring(textId.LastIndexOf('.') + 1);
                            textId = textId.Substring(0,textId.LastIndexOf('.'));
                        }
                        // Get text inline translation
                        var textVal = varName.Substring(equalIndex+1);
                        // Get the actual variablename
                        string originalVarName = varName;
                        varName = "text."+textId;
                        // Register the inline translation
                        _logger.Trace($"    Variable contains inline translation '{textVal}' (id={textId}, lang={textLang})");
                        var discoveredText = Core.Localization.Text.RegisterText(source, package, textId, textVal, textLang, Text.SourceTypes.Template);
                        // Replace all occurrences with a generic variable
                        data.Replace("{" + originalVarName + "}", "{" + varName + "}");
                    } 
                }
            }
           
        }

        private static void ParseDataWithoutInlineTranslation(string source, string package, StringBuilder data) {
            // Get matches for all text variables
            Regex textVariableMatchesRegex = new Regex(TEXT_VARIABLE_REGEX);
            var textVariableMatches = textVariableMatchesRegex.Matches(data.ToString());
            // Process each match
            foreach (var match in textVariableMatches) {
                string varName = match.ToString().Trim('{').Trim('}');
                if (varName.StartsWith("text.")) {
                    var textId = varName.ReplacePrefix("text.", "");
                    _logger.Trace("Text without translation: " + textId);
                    Core.Localization.Text.RegisterText(source, package, textId, null, Core.Config.LocalizationDefaultLanguage, Text.SourceTypes.Template);
                }
            }

        }

        /// <summary>
        /// Inserts the text variables for data inline.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="data">The data.</param>
        /// <param name="language">The language.</param>
        public static void InsertTextVariablesForData(string package, StringBuilder data, string language) {
            // Get matches for all text variables
            Regex textVariableMatchesRegex = new Regex(TEXT_VARIABLE_REGEX);  
            var textVariableMatches = textVariableMatchesRegex.Matches(data.ToString());
            // Process each match and put in the replacement
            foreach (var match in textVariableMatches) {
                string varName = match.ToString().Trim('{').Trim('}');
                var textId = varName.Substring(5);
                var text = Core.Localization.Text.GetTranslatedTextByIdForLanguage(textId, language, "[TRANS:"+textId+"."+language+"]");
                data.Replace("{" + varName + "}", text);
            }
        }

        /// <summary>
        /// Replaces the text variables for data and returns a new string.
        /// Note: Use the inline InsertTextVariablesForData when possible instead.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="data">The data.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public static string ReplaceTextVariablesForData(string package, string data, string language) {
            var sb = new StringBuilder(data);
            InsertTextVariablesForData(package, sb, language);
            return sb.ToString();
        }

        public static void InsertIconVariablesForData(string package, StringBuilder data, string language, string iconBundleName = null) {
            // Get matches for all icon variables
            Regex iconVariableMatchesRegex = new Regex(ICON_VARIABLE_REGEX);
            var iconVariableMatches = iconVariableMatchesRegex.Matches(data.ToString());
            // Process each match and put in the replacement
            foreach (var match in iconVariableMatches) {
                string varName = match.ToString().Trim('{').Trim('}');
                var iconId = varName.Substring(5);
                var iconCode = Core.Imaging.IconFactory.GetIconHTML(iconId, iconBundleName).Replace("\"","'");
                data.Replace("{" + varName + "}", iconCode);
            }
        }

    }
}
