using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core {

    public partial class JSON {
        
        public static Newtonsoft.Json.Linq.JObject ParseJsonAsJObject(byte[] data) {
            return ParseJsonAsJObject(Encoding.UTF8.GetString(data));
        }

        public static IEnumerable<JObject> ParseJsonArrayAsJObjects(byte[] data) {
            return ParseJsonArrayAsJObjects(Encoding.UTF8.GetString(data));
        }

        public static IEnumerable<JObject> ParseJsonArrayAsJObjects(string data) {
            var array = Newtonsoft.Json.Linq.JArray.Parse(data);
            return array.Children<JObject>().ToList();
        }

        public static Newtonsoft.Json.Linq.JObject ParseJsonAsJObject(string data, bool returnNullIfInvalid = false) {
            try {
                return Newtonsoft.Json.Linq.JObject.Parse(data);
            } catch(Exception e) {
                if (returnNullIfInvalid) {
                    return null;
                }
                throw e;
            }
        }


        /// <summary>
        /// Gets the depth of JSON structure
        /// rewrite from https://stackoverflow.com/questions/38046665/how-to-find-depth-of-nodes-in-a-nested-json-file
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static int GetDepth(string data) {
            if (string.IsNullOrWhiteSpace(data)) {
                return 0;
            }
            var parsed = ParseJsonAsJObject(data);
            return GetDepthToken(parsed);
            
        }

        private static int GetDepthToken(JToken obj) {
            var depth = 0;
            if (obj.Children().Count() > 0) {
                foreach (var child in obj.Children()) {
                    var tmpDepth = GetDepthToken(child);
                    if (tmpDepth > depth) {
                        depth = tmpDepth;
                    }

                }

            }
            return 1 + depth;
        }


        public static string Serialize(object data, bool lowerDashed = true, bool enumToString = false, bool indendFormatting = false) {
            // Create settings
            var settings = new JsonSerializerSettings {
                  
            };

            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            if (enumToString) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }
            
            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }

        public static string SerializeForStructuredData(object data,  bool indendFormatting = false) {
            // Create settings
            var settings = new JsonSerializerSettings {

            };
            
            // CamelCase
            settings.ContractResolver = new DefaultContractResolver {
                NamingStrategy = new CamelCaseNamingStrategy(true, true)
            };
          
            // Enum To String
            if (true) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }

            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }


        public static JObject FromObject(object data) {
            // Create settings
            var settings = new JsonSerializerSettings {
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };
            var serializer = JsonSerializer.Create(settings);
         
            // Serialize
            return JObject.FromObject(data,serializer);
        }

        public static T ToObject<T>(JObject jobject) {
            // Create settings
            var settings = new JsonSerializerSettings {
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };
            var serializer = JsonSerializer.Create(settings);

            // Serialize
            return jobject.ToObject<T>(serializer);
        }


        public static JToken FromObjects(object data) {
            // Create settings
            var settings = new JsonSerializerSettings {
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };
            var serializer = JsonSerializer.Create(settings);

            // Serialize
            return JToken.FromObject(data, serializer);
        }


        public static T Deserialize<T>(string data) {
            // Deserialize
            return JsonConvert.DeserializeObject<T>(data);
        }


        public static T Deserialize<T>(string data, bool lowerDashed) {
            var settings = new JsonSerializerSettings { };
            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            // Deserialize
            return JsonConvert.DeserializeObject<T>(data,settings);
        }


        public static object Deserialize(string data, Type type) {

            // Deserialize
            return JsonConvert.DeserializeObject(data, type);
        }

        public static object Deserialize(string data, Type type, bool lowerDashed) {
            var settings = new JsonSerializerSettings { };
         
            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            // Deserialize
            return JsonConvert.DeserializeObject(data, type,settings);
        }

        public class IgnoreSpecifiedContractResolver : DefaultContractResolver
        {
            // As of 7.0.1, Json.NET suggests using a static instance for "stateless" contract resolvers, for performance reasons.
            // http://www.newtonsoft.com/json/help/html/ContractResolver.htm
            // http://www.newtonsoft.com/json/help/html/M_Newtonsoft_Json_Serialization_DefaultContractResolver__ctor_1.htm
            // "Use the parameterless constructor and cache instances of the contract resolver within your application for optimal performance."
            static IgnoreSpecifiedContractResolver instance;

            static IgnoreSpecifiedContractResolver() {
                instance = new IgnoreSpecifiedContractResolver() {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            public static IgnoreSpecifiedContractResolver Instance { get { return instance; } }

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                var property = base.CreateProperty(member, memberSerialization);
                property.GetIsSpecified = null;
                property.SetIsSpecified = null;
                //property.Converter = null;
                //property.ItemConverter = null;
                return property;
            }
        }

       
    }
}
