using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Util;

using Newtonsoft.Json.Utilities;
using Newtonsoft.Json.Serialization;

namespace Machinata.Core {

    public partial class JSON {

        private class DashedLowerNamingStrategy : NamingStrategy {

            /// <summary>
            /// Initializes a new instance of the <see cref="DashedLowerNamingStrategy"/> class.
            /// </summary>
            /// <param name="processDictionaryKeys">
            /// A flag indicating whether dictionary keys should be processed.
            /// </param>
            /// <param name="overrideSpecifiedNames">
            /// A flag indicating whether explicitly specified property names should be processed,
            /// e.g. a property name customized with a <see cref="JsonPropertyAttribute"/>.
            /// </param>
            public DashedLowerNamingStrategy(bool processDictionaryKeys, bool overrideSpecifiedNames) {
                ProcessDictionaryKeys = processDictionaryKeys;
                OverrideSpecifiedNames = overrideSpecifiedNames;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DashedLowerNamingStrategy"/> class.
            /// </summary>
            /// <param name="processDictionaryKeys">
            /// A flag indicating whether dictionary keys should be processed.
            /// </param>
            /// <param name="overrideSpecifiedNames">
            /// A flag indicating whether explicitly specified property names should be processed,
            /// e.g. a property name customized with a <see cref="JsonPropertyAttribute"/>.
            /// </param>
            /// <param name="processExtensionDataNames">
            /// A flag indicating whether extension data names should be processed.
            /// </param>
            public DashedLowerNamingStrategy(bool processDictionaryKeys, bool overrideSpecifiedNames, bool processExtensionDataNames)
                : this(processDictionaryKeys, overrideSpecifiedNames) {
                //ProcessExtensionDataNames = processExtensionDataNames;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DashedLowerNamingStrategy"/> class.
            /// </summary>
            public DashedLowerNamingStrategy() {
            }

            /// <summary>
            /// Resolves the specified property name.
            /// </summary>
            /// <param name="name">The property name to resolve.</param>
            /// <returns>The resolved property name.</returns>
            protected override string ResolvePropertyName(string name) {
                return name.ToDashedLower();
            }

        }
    }
}
