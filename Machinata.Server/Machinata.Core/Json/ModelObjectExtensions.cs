using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Util;

namespace Machinata.Core {
    
        public static class JSONModelObjectExtensions {

            public static JObject ConvertToJObject<T>(this T entity, FormBuilder form) where T : Core.Model.ModelObject {
                // Sanity
                if (entity == null) return null;
                // Init
                var obj = new JObject();
                // Do entity properties
                List<PropertyInfo> properties = entity.GetPropertiesForForm(form);
                foreach (var prop in properties) {
                    string propName = prop.Name.ToDashedLower();
                    object val = null;
                    if (prop.CanRead) {
                        val = prop.GetValue(entity);
                    }
                    if (val != null) obj.Add(propName, JToken.FromObject(val));
                    else obj.Add(propName, null);
                }
                // Do custom
                if (form.CustomProperties != null) {
                    foreach (var prop in form.CustomProperties) {
                        //TODO:
                    }
                }
                // Return
                return obj;
            }


            public static List<JObject> ConvertToJObjects<T>(this IQueryable<T> set, FormBuilder form) where T : Core.Model.ModelObject {
                List<JObject> ret = new List<JObject>();
                foreach (var entity in set) {
                    ret.Add(entity.ConvertToJObject(form));
                }
                return ret;
            }


        }
}
