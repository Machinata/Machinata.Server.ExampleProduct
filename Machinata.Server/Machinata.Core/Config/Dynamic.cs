using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Configuration;
using Machinata.Core.Exceptions;
using System.Net;
using System.Reflection;
using System.Web.Hosting;
using Machinata.Core.Model;
using System.Collections.Concurrent;

namespace Machinata.Core {
    
    public partial class Config {

        /// <summary>
        /// This helper class wraps the DynamicConfig db objects for easy access to dynamically set
        /// configuration parametners. All configs fallback to the regular config system for defaults (which are required).
        /// </summary>
        public static class Dynamic {
            
            private static ConcurrentDictionary<string, string> _configCache = new ConcurrentDictionary<string, string>();

            /// <summary>
            /// Resets the cache. This should be called any time a DynamicConfig is altered or deleted.
            /// </summary>
            public static void ResetCache() {
                _configCache.Clear();
            }

            public static IDictionary<string,string> GetCache() {
                return _configCache.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }


            public static string String(string key, bool throwException = true) {
                // Already in cache?
                if(_configCache.ContainsKey(key)) {
                    _logger.Trace($"Getting key '{key}' from cache...");
                    return _configCache[key];
                } else {
                    // Try to get from db
                    string ret = null;
                    _logger.Trace($"Getting key '{key}' from DB...");
                    using (ModelContext db = ModelContext.GetModelContext(null)) {
                        var config = db.DynamicConfigs().SingleOrDefault(e => e.Key == key);
                        if (config != null) {
                            // Hit from DB
                            ret = config.Value;
                        } else {
                            // Default value?
                            ret = Core.Config.GetStringSetting(key,null);
                            if (ret == null && throwException) throw new Exception($"The dynamic config '{key}' is not set and has no default value in a config file.");
                        }
                    }
                    // Register in cache
                    _configCache[key] = ret;
                    return ret;
                }
            }

            public static void Set(string key, string val) {
                using (ModelContext db = ModelContext.GetModelContext(null)) {
                    var config = db.DynamicConfigs().SingleOrDefault(e => e.Key == key);
                    if (config == null) {
                        // Create
                        config = new DynamicConfig();
                        config.Key = key;
                        db.DynamicConfigs().Add(config);
                    }
                    // Update
                    config.Value = val;
                    // Save
                    db.SaveChanges();
                }
            }

            public static int? Int(string key, bool throwException = true) {
                var val = String(key, throwException);
                if (val == null) return null;
                else return int.Parse(val);
            }

            public static bool Bool(string key, bool throwException = true) {
                var val = String(key, throwException);
                if (val == null) return false;
                if (val == "true") return true;
                else if (val == "false") return false;
                else throw new Exception($"Invalid bool dynamic config with key '{key}'.");
            }

            public static decimal? Decimal(string key, bool throwException = true) {
                var val = String(key, throwException);
                if (val == null) return null;
                else return decimal.Parse(val);
            }

            public static Price Price(string key, bool throwException = true) {
                var val = String(key, throwException);
                if (val == null) return null;
                else return new Model.Price(val);
            }

            public static IEnumerable<string> GetStringList(string name, bool throwException = true) {
                try {
                    return String(name, throwException).Split(',').ToList();
                }
                catch(Exception e) {
                    if (throwException) {
                        throw e;
                    }
                }
                return new List<string>();
            }

        }
        

    }
}
