using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class IEnumberableExtensions {


        /// <summary>
        /// Returns the second element or null of a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static T SecondOrDefault<T>(this IEnumerable<T> items) {
            return items.Skip(1).FirstOrDefault();
        }

        /// <summary>
        /// Returns the second element of a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static T Second<T>(this IEnumerable<T> items) {
            return items.Skip(1).First();
        }


        /// <summary>
        /// Distincts the by selector
        /// https://stackoverflow.com/questions/489258/linqs-distinct-on-a-particular-property
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source) {
                if (seenKeys.Add(keySelector(element))) {
                    yield return element;
                }
            }
        }
    }
}
