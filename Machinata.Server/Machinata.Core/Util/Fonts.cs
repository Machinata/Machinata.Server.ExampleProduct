using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Fonts {

        public static Dictionary<string,string> GetAvailableFonts() {
            var ret = new Dictionary<string, string>();
            var fontPath = Core.Config.StaticPath + Core.Config.PathSep + "fonts";
            foreach (var file in System.IO.Directory.GetFiles(fontPath)) {
                // Valid file?
                if (file.EndsWith(".otf") || file.EndsWith(".ttf")) {
                    // Get alias
                    var alias = Core.Util.Files.GetFileNameWithoutExtension(file);
                    var aliasFile = Core.Util.Files.GetPathWithoutExtension(file) + ".txt";
                    if (System.IO.File.Exists(aliasFile)) {
                        alias = System.IO.File.ReadAllText(aliasFile);
                    }
                    // Register a single font
                    ret.Add(file,alias);
                }
            }
            return ret;
        }

    }
}
