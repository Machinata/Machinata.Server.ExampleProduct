using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public class Files {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        protected static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        /// <summary>
        /// Gets the using search options and allows multiple comma sepearated filters.
        /// </summary>
        /// <param name="sourceFolder">The source folder.</param>
        /// <param name="filters">The filters, can be comma separated.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public static string[] GetFiles(string sourceFolder, string filters, System.IO.SearchOption searchOption) {
           return filters
                .Split(',')
                .SelectMany(filter => System.IO.Directory.GetFiles(sourceFolder, filter, searchOption))
                .ToArray();
        }

        public static string GetFileExtension(string path) {
            return Path.GetExtension(path);
        }

        public static string GetFileNameWithExtension(string path) {
            return Path.GetFileName(path);
        }

        public static string GetFileNameWithoutExtension(string path) {
            return Path.GetFileNameWithoutExtension(path);
        }

        public static string GetPathWithoutExtension(string path) {
            var lastDotIndex = path.LastIndexOf('.');
            if (lastDotIndex >= 0) return path.Substring(0, lastDotIndex);
            else return path;
        }

        public static string GetFileExtensionWithoutDot(string path) {
            return GetFileExtension(path).Replace(".","");
        }

        /// <summary>
        /// Gets the mimetype for a given filename or extension.
        /// Custom types can be added in Core.Util.Constants.DefaultMimeTypes.
        /// If the mimetype is unknown, then application/octet-stream is returned.
        /// </summary>
        /// <param name="filename">The filename or extension. If only a extension is passed, then one should include the dot, for example '.txt'.</param>
        /// <returns></returns>
        public static string GetMimeType(string filename) {
            // First we try our own list
            var fileType = GetFileExtension(filename);
            if (Core.Util.Constants.DefaultMimeTypes.ContainsKey(fileType)) return Core.Util.Constants.DefaultMimeTypes[fileType];
            // Fallback to .net
            return System.Web.MimeMapping.GetMimeMapping(filename);
        }

        /// <summary>
        /// Gets the direct path to a 'hot' project file rather for a file in the bin path.
        /// This works through making some assumtions from which package the file originated from,
        /// which is in some cases not possible (for example in cross-module template files).
        /// 
        /// Note: If Core.Config.HotSwappingEnabled is not enabled this method does not do anything.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="package">The package.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">HotSwappingEnabled cannot be enabled on non test environments! See config IsTestEnvironment and HotSwappingEnabled.</exception>
        public static string GetHotSwappableFile(string path, string package) {
            if(Core.Config.HotSwappingEnabled) {
                // Validate that the setting is never enabled on a live environment
                if (!Core.Config.IsTestEnvironment) throw new Exception("HotSwappingEnabled cannot be enabled on non test environments! See config IsTestEnvironment and HotSwappingEnabled.");
                // Init some paths we will need
                if (package == null) package = Core.Config.ProjectPackage; // default
                var basePathSegs = Core.Config.BasePath.Trim(Core.Config.PathSep[0]).Split(Core.Config.PathSep[0]);
                var projectPackage = basePathSegs.Last();
                var rootPath = string.Join(Core.Config.PathSep, basePathSegs.Take(basePathSegs.Count()-1));
                var pathWithoutBase = path.Replace(Core.Config.BasePath,"").ReplacePrefix("bin" + Core.Config.PathSep,"");
                var modulesSubPath = Core.Config.HotSwappingModulesSubPath;
                if (modulesSubPath != null && modulesSubPath != "") modulesSubPath = modulesSubPath + Core.Config.PathSep;
                else modulesSubPath = "";
                // Get the assumed package
                var packageToUse = package;
                if (!package.Contains("Machinata.Product.") && !package.Contains("Machinata.Module.") && !package.Contains("Machinata.Core")) packageToUse = projectPackage;
                // Make guesses to where the file originated from
                // 1st Priority: packageToUse and projectPackage
                var newPathGuesses = new List<string>();
                newPathGuesses.Add(rootPath + Core.Config.PathSep + modulesSubPath + packageToUse + Core.Config.PathSep + pathWithoutBase);
                newPathGuesses.Add(rootPath + Core.Config.PathSep + projectPackage + Core.Config.PathSep + pathWithoutBase);
                // 2nd Priority: Additional hints...
                if (Core.Config.HotSwappingAdditionalPackageHints != null) {
                    foreach(var packageHint in Core.Config.HotSwappingAdditionalPackageHints) {
                        if (packageHint != "") {
                            newPathGuesses.Add(rootPath + Core.Config.PathSep + modulesSubPath + packageHint + Core.Config.PathSep + pathWithoutBase);
                            newPathGuesses.Add(rootPath + Core.Config.PathSep + packageHint + Core.Config.PathSep + pathWithoutBase);
                        }
                    }
                }
                // 3rd Priority: All module packages registered in system...
                foreach(var moduleName in Core.Reflection.Modules.GetMachinataModuleNames()) {
                    newPathGuesses.Add(rootPath + Core.Config.PathSep + modulesSubPath + moduleName + Core.Config.PathSep + pathWithoutBase);
                    newPathGuesses.Add(rootPath + Core.Config.PathSep + moduleName + Core.Config.PathSep + pathWithoutBase);
                }
                // See if any path matches
                foreach(var newPath in newPathGuesses) {
                    if (File.Exists(newPath)) {
                        _logger.Trace($"Hot Swapping {path} with {newPath}");
                        return newPath;
                    }
                }
                _logger.Warn($"Could not Hot Swap {path} with any guess {string.Join(",",newPathGuesses)} (package={package})");
                return path;
            } else {
                return path;
            }
        }

        //https://stackoverflow.com/questions/7146021/copy-all-files-in-directory
        /// <summary>
        /// Copies all files and directories to the targetDir (recursive)
        /// </summary>
        /// <param name="sourceDir"></param>
        /// <param name="targetDir"></param>
        public static void CopyDirectory(string sourceDir, string targetDir) {
            Directory.CreateDirectory(targetDir);

            foreach (var file in Directory.GetFiles(sourceDir)) {
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));
            }

            foreach (var directory in Directory.GetDirectories(sourceDir)) {
                CopyDirectory(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
            }
        }

        /// <summary>
        /// Blocks until the file is not locked any more.
        /// </summary>
        /// <param name="fullPath"></param>
        public static bool WaitForFile(string fullPath, int timeoutMilliseconds = 10000) {
            int numTries = 0;
            int waitIntervalMS = 100;
            int maxTries = timeoutMilliseconds / waitIntervalMS;
            while (true) {
                ++numTries;
                try {
                    // Attempt to open the file exclusively.
                    using (FileStream fs = new FileStream(fullPath,
                        FileMode.Open, FileAccess.ReadWrite,
                        FileShare.None, 100)) {
                        fs.ReadByte();

                        // If we got this far the file is ready
                        break;
                    }
                } catch (Exception ex) {
                    //Log.LogWarning("WaitForFile {0} failed to get an exclusive lock: {1}",fullPath, ex.ToString());

                    if (numTries > maxTries) {
                        //Log.LogWarning( "WaitForFile {0} giving up after 10 tries", fullPath);
                        return false;
                    }

                    // Wait for the lock to be released
                    System.Threading.Thread.Sleep(waitIntervalMS);
                }
            }

            //Log.LogTrace("WaitForFile {0} returning true after {1} tries",fullPath, numTries);
            return true;
        }
    }
}
