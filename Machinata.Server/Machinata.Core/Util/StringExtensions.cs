using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class StringExtensions {
        
        public static string ReplacePrefix(this string str, string find, string replace) {
            if(str.StartsWith(find)) {
                return replace + str.Substring(find.Length);
            } else {
                return str;
            }
        }

        public static string ReplaceSuffix(this string str, string find, string replace) {
            if(str.EndsWith(find)) {
                var pos = str.IndexOf(find);
                return str.Substring(0,pos) + replace;
            } else {
                return str;
            }
        }

        public static string ReplaceFirst(this string text, string search, string replace) {
            // https://stackoverflow.com/questions/8809354/replace-first-occurrence-of-pattern-in-a-string
            int pos = text.IndexOf(search);
            if (pos < 0) {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        public static bool IsInt(this string str) {
            int number;
            return int.TryParse(str, out number);
        }

        public static bool IsInt64(this string str) {
            Int64 number;
            return Int64.TryParse(str, out number);
        }

        public static bool IsDecimal(this string str) {
            decimal number;
            return decimal.TryParse(str, out number);
        }
        
        public static int ToInt(this string str) {
            return int.Parse(str);
        }
        
        public static long ToLong(this string str) {
            return long.Parse(str);
        }
        
        public static double ToDouble(this string str) {
            return double.Parse(str);
        }

        public static string Capitalize(this string str) {
            if (str == null) return str;
            if (str == "") return str;
            return str.Substring(0,1).ToUpper() + str.Substring(1);
        }
        
        public static string ToDashedLower(this string str, bool ignoreMultipleCapitols = true) {
            // Execption if all is uppercase (like UID)
            if (str.ToUpper() == str) return str.ToLower();
            // Convert key from CamelCase to cms style (not-camel-case)
            if (!ignoreMultipleCapitols) {
                str = Regex.Replace(str, "(\\B[A-Z])", "-$1");
            } else {
                //https://stackoverflow.com/questions/15374217/how-to-use-regular-expressions-to-insert-space-into-a-camel-case-string
                str = Regex.Replace(str, "([A-Z])([A-Z])([a-z])|([a-z])([A-Z])", "$1$4-$2$3$5");
            }
            str = str.ToLower();
            return str;
        }

        public static string ToSentence(this string str, bool ignoreMultipleCapitols = true) {
            // Execption if all is uppercase (like UID)
            if (str.ToUpper() == str) return str.ToLower();
            if (!ignoreMultipleCapitols) {
                str = Regex.Replace(str, "(\\B[A-Z])", " $1");
            } else {
                str = Regex.Replace(str, "([A-Z])([A-Z])([a-z])|([a-z])([A-Z])", "$1$4 $2$3$5"); 
            }
            return str;
        }

        public static string FromDashedLower(this string str, bool ignoreMultipleCapitols = false) {
            throw new NotImplementedException();
        }

        public static int UnobfuscateId(this string str) {
            return Core.Ids.Obfuscator.Default.UnobfuscateId(str);
        }

        public static string TextVar(this string str) {
            return "{text." + str.ToLowerInvariant() + "}";
        }

        public static string TemplateVar(this string str) {
            return "{" + str + "}";
        }

        public static string Random(this Array array) {
            return array.GetValue(Core.Util.Math.RandomInt(0,array.Length)) as string;
        }


        /// <summary>
        /// Creates an in memory PageTemplate from the string
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static Templates.PageTemplate GetPageTemplate(this string str) {
            var template = new Templates.PageTemplate(str);
            return template;
        }


        public static IEnumerable<string> Split(this string str, string separator, int? max = null) {
            if (max.HasValue) {
                return str.Split(new string[] { separator }, max.Value, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            return str.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }


        //// str - the source string
        //// index- the start location to replace at (0-based)
        //// length - the number of characters to be removed before inserting
        //// replace - the string that is replacing characters
        ///https://stackoverflow.com/questions/5015593/how-to-replace-part-of-string-by-position
        public static string ReplaceAt(this string str, int index, int length, string replace) {
            return str.Remove(index, System.Math.Min(length, str.Length - index))
                    .Insert(index, replace);
        }


        /// <summary>
        /// https://stackoverflow.com/questions/4335878/c-sharp-trimstart-with-string-parameter
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="trimString">The trim string.</param>
        /// <returns></returns>
        public static string TrimStart(this string target, string trimString) {
            if (string.IsNullOrEmpty(trimString)) return target;

            string result = target;
            while (result.StartsWith(trimString)) {
                result = result.Substring(trimString.Length);
            }

            return result;
        }

        public static string TrimEnd(this string target, string trimString) {
            if (string.IsNullOrEmpty(trimString)) return target;

            string result = target;
            while (result.EndsWith(trimString)) {
                result = result.Substring(0, result.Length - trimString.Length);
            }

            return result;
        }


        /// <summary>
        /// https://stackoverflow.com/questions/1857513/get-substring-everything-before-certain-char
        /// </summary>
        /// <param name="text"></param>
        /// <param name="stopAt"></param>
        /// <returns></returns>
        public static string GetUntilOrEmpty(this string text, string stopAt = "-") {
            if (!string.IsNullOrWhiteSpace(text)) {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0) {
                    return text.Substring(0, charLocation);
                }
            }

            return string.Empty;
        }

    }
}
