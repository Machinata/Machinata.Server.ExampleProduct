using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Comparers {

        /// <summary>
        /// Simple reverse-comparer using the objects generic comparer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <seealso cref="System.Collections.Generic.IComparer{T}" />
        public class DescendingComparer<T> : IComparer<T> where T : IComparable<T> {
            public int Compare(T x, T y) {
                return y.CompareTo(x);
            }
        }

        /// <summary>
        /// Compares (in descending order) the length of strings.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.IComparer{System.String}" />
        public class DescendingStringLengthComparer : IComparer<string>{
            public int Compare(string x, string y) {
                int nXLen = x.Length;
                int nYLen = y.Length;
                if (nXLen > nYLen) return -1;
                if (nXLen < nYLen) return 1;
                return string.Compare(x, y, false);  
            }
        }

        /// <summary>
        /// Compares (in descending order) the length of paths in strings
        /// by counting the number of '/'.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.IComparer{System.String}" />
        public class DescendingPathLengthComparer : IComparer<string>{
            public int Compare(string x, string y) {
                int nXPaths = x.Count(c => c == '/');
                int nYPaths = y.Count(c => c == '/');
                if (nXPaths > nYPaths) return -1;
                if (nXPaths < nYPaths) return +1;
                return string.Compare(x, y, false);  
            }
        }

        

    }
}
