using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Util {

    public class HTTP {

        /// <summary>
        /// Enables all SSL versions.
        /// WARNING: This is a permanant change during runtime.
        /// </summary>
        public static void EnableAllSSLVersions() {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
        }

        /// <summary>
        /// Ignores the SSL certificates on all connection.
        /// WARNING: Only for use on test/dev environments!
        /// </summary>
        /// <exception cref="System.Exception">HTTP.IgnoreSSLCertificates() can only be used on a test environement!</exception>
        public static void IgnoreSSLCertificates() {
            if (!Core.Config.IsTestEnvironment) throw new Exception("HTTP.IgnoreSSLCertificates() can only be used on a test environement!");
            //Change SSL checks so that all checks pass
            ServicePointManager.ServerCertificateValidationCallback =
                new System.Net.Security.RemoteCertificateValidationCallback(
                    delegate
                    { return true; }
                );
        }

        public static string GetRemoteIP(HttpContext context) {
            if (context == null) return null;
            if (context.Request.Headers["X-Forwarded-For"] != null) return context.Request.Headers["X-Forwarded-For"].Split(',').LastOrDefault().Trim();
            try {
                return context.Request.UserHostAddress;
            } catch (Exception e) {
                return null;
            }
        }

        public static string GetLocalIPAddress() {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList) {
                if (ip.AddressFamily == AddressFamily.InterNetwork) {
                    return ip.ToString();
                }
            }
            return null;
        }

        public static string GetRequestProtocol(HttpContext context) {
            if (context == null) return null;
            if (context.Request.Headers["X-Forwarded-Proto"] != null) return context.Request.Headers["X-Forwarded-Proto"].Trim();
            try {
                return context.Request.Url.Scheme;
            }
            catch (Exception e) {
                return null;
            }
        }

        public static string GetUserAgent(HttpContext context) {
            if (context == null || context.Request == null) return null;
            else return context.Request.UserAgent;
        }

        public static IEnumerable<string> GetUserLanguages(HttpContext context) {
            if (context == null || context.Request == null) return null;
            else return context.Request.UserLanguages;
        }

        public static string GetUserLanguagesString(HttpContext context) {
            var languages = GetUserLanguages(context);
            if (languages != null && languages.Any()) {
                return string.Join(",", languages);
            }
            return null;
        }

        /// <summary>
        /// Invalidates the cookie if it exists
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="name">The name.</param>
        public static void InvalidateCookie (HttpContext context, string name) {
            if (context?.Request?.Cookies[name] != null) {
                context.Response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
            }
        }

        public static void SetCookie(HttpContext context, string name, string value, DateTime expiration) {
            context.Response.Cookies[name].Value = value;
            context.Response.Cookies[name].Expires = expiration;
        }

        public static string UrlPathEncode(string path) {
            return WebUtility.UrlEncode(path);
        }

        public static void SendBinaryData(HttpContext context, byte[] bytes, string fileName) {
            // Clear all content output from the buffer stream
            context.Response.Clear();

            // Add a HTTP header to the output stream that specifies the default filename
            // for the browser's download dialog
            context.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");

            // Add a HTTP header to the output stream that contains the 
            // content length(File Size). This lets the browser know how much data is being transfered
            context.Response.AddHeader("Content-Length", bytes.Length.ToString());

            // Set the HTTP MIME type of the output stream
            context.Response.ContentType = "application/octet-stream";

            // Write the data out to the client.
            context.Response.BinaryWrite(bytes);

            context.Response.Flush();
        }

        public static string GetCachedDownload(string url, bool useDailyCache = true) {
            // Init the cache path and dir
            Uri uri = new Uri(url);
            var ext = Core.Util.Files.GetFileExtension(uri.AbsolutePath);
            var date = "";
            if (useDailyCache) date = "_" + DateTime.UtcNow.ToString("yyyy-MM-dd");
            var filename = Core.Encryption.DefaultHasher.HashString(url)+date + ext;
            var cachePath = Core.Config.CachePath + Core.Config.PathSep + "http" + Core.Config.PathSep + filename;

            //TODO: this should be removed at some point
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
            }

            // Download file
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, () => {
                WebClient client = new WebClient();
                client.DownloadFile(url, cachePath);
            });
            
            // Return the path
            return cachePath;
        }

        /// <summary>
        /// Writes the file to a HTTP context with streaming support (HTTP_RANGE/Accept-Ranges).
        /// If the request support compression, the stream is automatically compressed using gzip or deflate.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="HttpException">
        /// 416;Requested Range Not Satisfiable
        /// or
        /// 416;Requested Range Not Satisfiable
        /// </exception>
        public static void WriteFileWithStreamingSupport(string filepath, HttpContext context, string filename = null, string charset = null) {
            // Exists?
            //TODO

            // Init
            bool debugging = false;
            FileInfo fileInfo = new FileInfo(filepath);
            long size = fileInfo.Length;
            long start = 0;
            long end = size - 1;
            long length = size;

            // Get mime type
            string fileType = Core.Util.Files.GetFileExtension(filepath);
            string mimeType = Core.Util.Files.GetMimeType(fileType);

            // Debugging
            if(debugging) context.Response.Headers["X-Streaming-Support"] = "enabled";

            // Set content type header
            if (charset == null) {
                context.Response.ContentType = mimeType;
            } else {
                context.Response.ContentType = mimeType + "; charset="+charset;
            }
            //TODO: @dan what about charset encoding for text documents? " ;charset=utf-8"

            if(filename != null) {
                context.Response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            }

            // Set content length header
            context.Response.Headers["Accept-Ranges"] = "0-" + size.ToString();

            // Set last modified header
            context.Response.Cache.SetLastModified(fileInfo.LastWriteTime);
            context.Response.Cache.SetMaxAge(new TimeSpan(12, 0, 0));//TODO:@dan 

            // Set CORS headers?
            if(mimeType != null && mimeType.Contains("font")) {
                context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            }
            
            // Compression?
            bool doCompression = false;
            string acceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
            if (!string.IsNullOrEmpty(acceptEncoding)
                    &&
                    (acceptEncoding.Contains("gzip") || acceptEncoding.Contains("deflate"))
                ) {
                
                // Debugging
                if(debugging) context.Response.Headers["X-Streaming-Support"] += ",accept-encoding-compression";

                doCompression = true;
            }
            if (doCompression == true && Core.Config.CompressionHTTPGZIPEnabled) {  
                if (acceptEncoding.Contains("gzip")) {
                    context.Response.Filter = new System.IO.Compression.GZipStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
                    context.Response.Headers["Content-Encoding"] = "gzip";
                    // Debugging
                    if(debugging) context.Response.Headers["X-Streaming-Support"] += ",compression-gzip";
                } else {
                    context.Response.Filter = new System.IO.Compression.DeflateStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
                    context.Response.Headers["Content-Encoding"] = "deflate";
                    // Debugging
                    if(debugging) context.Response.Headers["X-Streaming-Support"] += ",compression-deflate";
                }
            }
                

            // Set filename
            //if (filename != null) {
            //    SetResponseFilename(context, filename);
            //}

            // Ranged (streaming) download?
            // See https://stackoverflow.com/questions/35458421/unable-to-play-videos-on-ipad-using-asp-net-handler
            // See https://blogs.visigo.com/chriscoulson/easy-handling-of-http-range-requests-in-asp-net/
            if (!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_RANGE"])) {
                // Debugging
                if(debugging) context.Response.Headers["X-Streaming-Support"] += ",range";

                long anotherStart = start;
                long anotherEnd = end;
                string[] arr_split = context.Request.ServerVariables["HTTP_RANGE"].Split(new char[] { Convert.ToChar("=") });
                string range = arr_split[1];
 
                // Make sure the client hasn't sent us a multibyte range
                if (range.IndexOf(",") > -1)
                {
                    // (?) Shoud this be issued here, or should the first
                    // range be used? Or should the header be ignored and
                    // we output the whole content?
                    context.Response.AddHeader("Content-Range", "bytes " + start + "-" + end + "/" + size);
                    throw new HttpException(416, "Requested Range Not Satisfiable");
 
                }
 
                // If the range starts with an '-' we start from the beginning
                // If not, we forward the file pointer
                // And make sure to get the end byte if spesified
                if (range.StartsWith("-"))
                {
                    // The n-number of the last bytes is requested
                    anotherStart = size - Convert.ToInt64(range.Substring(1));
                }
                else
                {
                    arr_split = range.Split(new char[] { Convert.ToChar("-") });
                    anotherStart = Convert.ToInt64(arr_split[0]);
                    long temp = 0;
                    anotherEnd = (arr_split.Length > 1 && Int64.TryParse(arr_split[1].ToString(), out temp)) ? Convert.ToInt64(arr_split[1]) : size;
                }
                // Check the range and make sure it's treated according to the specs.
                // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
                // End bytes can not be larger than $end.
                anotherEnd = (anotherEnd > end) ? end : anotherEnd;
                // Validate the requested range and return an error if it's not correct.
                if (anotherStart > anotherEnd || anotherStart > size - 1 || anotherEnd >= size)
                {
 
                    context.Response.AddHeader("Content-Range", "bytes " + start + "-" + end + "/" + size);
                    throw new HttpException(416, "Requested Range Not Satisfiable");
                }
                start = anotherStart;
                end = anotherEnd;
 
                length = end - start + 1; // Calculate new content length
                context.Response.Headers["Content-Length"] = length.ToString();
                context.Response.Headers["Content-Range"] = "bytes " + start + "-" + end + "/" + size;
                context.Response.StatusCode = 206;
                context.Response.WriteFile(filepath, start,length);
            } else {
                // Debugging
                if(debugging) context.Response.Headers["X-Streaming-Support"] += ",no-range";
                // Send entire thing
                context.Response.Headers["Content-Length"] = length.ToString();
                context.Response.WriteFile(filepath);
            }
            
            // Flush
            // Note: flush will stop any additional headers from being written, so we don't
            // do this if using a compression filter.
            if(!doCompression) context.Response.Flush();

        }

        /// <summary>
        /// Writes the string to http context with compression support.
        /// Note: Core.Config.CompressionHTTPGZIPEnabled must be enabled.
        /// </summary>
        /// <param name="stringOutput">The string output.</param>
        /// <param name="context">The context.</param>
        public static void WriteStringWithCompressionSupport(StringBuilder stringOutput, HttpContext context) {

            // Init
            bool debugging = false;
                        
            // Compression?
            bool doCompression = false;
            string acceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
            if (!string.IsNullOrEmpty(acceptEncoding)
                    &&
                    (acceptEncoding.Contains("gzip") || acceptEncoding.Contains("deflate"))
                ) {
                
                // Debugging
                if(debugging) context.Response.Headers["X-Streaming-Support"] += ",accept-encoding-compression";

                doCompression = true;
            }
            if (doCompression == true && Core.Config.CompressionHTTPGZIPEnabled) {  
                if (acceptEncoding.Contains("gzip")) {
                    context.Response.Filter = new System.IO.Compression.GZipStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
                    context.Response.Headers["Content-Encoding"] = "gzip";
                    // Debugging
                    if(debugging) context.Response.Headers["X-Streaming-Support"] += ",compression-gzip";
                } else {
                    context.Response.Filter = new System.IO.Compression.DeflateStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
                    context.Response.Headers["Content-Encoding"] = "deflate";
                    // Debugging
                    if(debugging) context.Response.Headers["X-Streaming-Support"] += ",compression-deflate";
                }
            }
              
            // Write out
            context.Response.Write(stringOutput);
            
            // Flush
            // Note: flush will stop any additional headers from being written, so we don't
            // do this if using a compression filter.
            if(!doCompression) context.Response.Flush();

        }


        public static string GetURL(string url, Encoding encoding = null) {
            var client = new WebClient();
            if (encoding != null) client.Encoding = encoding;
            else client.Encoding = Encoding.UTF8;
            string data;
            try {
                data = client.DownloadString(url);
            } catch (Exception e) {
                throw new Exception($"Could not download '{url}': " + e.Message, e);
            }
            return data;
        }

        public static JToken GetJSON(string url) {
            var data = GetURL(url);
            JToken ret = null;
            try {
                ret = JToken.Parse(data);
            }catch(Exception e) {
                ret = JToken.Parse("{ data: " + data + "}");
            }
            return ret;
        }

        public class WebClientWithTimeout : WebClient {

            private TimeSpan _timeout;

            public WebClientWithTimeout(TimeSpan timeout) {
                _timeout = timeout;
            }

            protected override WebRequest GetWebRequest(Uri uri) {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = (int)_timeout.TotalMilliseconds;
                return w;
            }
        }
    }
}
