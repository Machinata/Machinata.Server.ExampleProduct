using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class CountryCodes {

        public const string  COUNTRY_LOCALIZATION_PREFIX= "country-";
        
        private static string[] COUNTRY_CODES_EU = new string[] { "be", "bg", "cz", "dk", "de", "ee", "ie", "el", "es", "fr", "hr", "it", "cy", "lv", "lt", "lu", "hu", "mt", "nl", "at", "pl", "pt", "ro", "se", "sk", "fi", "se", "uk" };
        
        private static string[] COUNTRY_CODES_EFTA = new string[] { "is", "li", "no", "ch" };

        private static string[] COUNTRY_CODES_NA = new string[] { "us", "ca" };

        private static string[] COUNTRY_CODE_GB = new string[] { "gb" };

        private static string[] COUNTRY_CODES_ASIA = new string[] { "af", "am", "az", "bh", "bd", "bt", "bn", "kh", "cn", "ge", "hk", "in", "id", "ir", "iq", "il", "jp", "jo", "kz", "kw", "kg", "la", "lb", "mo", "my", "mv", "mn", "np", "kp", "om", "pk", "ph", "qa", "sa", "sg", "kr", "lk", "sy", "tw", "tj", "th", "tr", "tm", "ae", "uz", "vn", "ye" };
        
        public static IEnumerable<string> Codes {
            get {
                return Machinata.Core.Localization.Text.GetAllTextIds()
                        .Where(t => t.StartsWith(COUNTRY_LOCALIZATION_PREFIX, StringComparison.InvariantCulture))
                        .Select(c => c.ReplacePrefix(COUNTRY_LOCALIZATION_PREFIX, string.Empty))
                        .Where(c => c.Length == 2);
            }
        }
        
        public static IEnumerable<string> CodesEuropeanUnion {
            get {
                return COUNTRY_CODES_EU.OrderBy(c => c);;
            }
        }
        
        public static IEnumerable<string> CodesEuropeanFreeTradeAssociation {
            get {
                return COUNTRY_CODES_EFTA.OrderBy(c => c);;
            }
        }
        
        public static IEnumerable<string> CodesNorthAmerica {
            get {
                return COUNTRY_CODES_NA.OrderBy(c => c);;
            }
        }
        
        public static IEnumerable<string> CodesWestern {
            get {
                var western = COUNTRY_CODES_EFTA.Union(COUNTRY_CODES_EU).Union(COUNTRY_CODES_NA).Union(COUNTRY_CODE_GB);
                return western.OrderBy(c => c);
            }
        }

        public static IEnumerable<string> CodesEUEFTA {
            get {
                var western = COUNTRY_CODES_EFTA.Union(COUNTRY_CODES_EU).Union(COUNTRY_CODE_GB);
                return western.OrderBy(c => c);
            }
        }

        public static IEnumerable<string> CodesAsia {
            get {
                return COUNTRY_CODES_ASIA.OrderBy(c => c);
            }
        }

        public static IDictionary<string, IEnumerable<string>> Regions
        {
            get
            {
                var regions = new Dictionary<string, IEnumerable<string>>();
                regions["eu"] = CodesEuropeanUnion;
                regions["efta"] = CodesEuropeanFreeTradeAssociation;
                regions["eftaeu"] = CodesEuropeanUnion.Union(CodesEuropeanFreeTradeAssociation);
                regions["na"] = CodesNorthAmerica;
                regions["western"] = CodesEuropeanUnion;
                regions["asia"] = CodesAsia;
                return regions;
            }
        }

        public static IEnumerable<string> GetRegionsForCountry(string countryCode) {
            return Regions.Where(v => v.Value.Contains(countryCode)).Select(r => r.Key);
        }

        public static string GetRegionForCountry(string countryCode, bool throwException = true) {
            var regions = GetRegionsForCountry(countryCode);
            int count = regions.Count();
            if (count != 1 && throwException) {
                throw new Exception($"Country was found in {count} regions");
            } else if (count !=1) {
                return null;
            }
            return regions.First();
        }

        public static string GetCountriesAsJson(string lang)
        {
            return JSON.Serialize(GetCountries(lang));
        }

        public static IEnumerable<Country> GetCountries(string lang) {
            foreach(var code in Codes) {
                var name = Localization.Text.GetTranslatedTextByIdForLanguage(COUNTRY_LOCALIZATION_PREFIX + code, lang);
                yield return new Country() { Code = code, Name = name };
            }
        }

        public static string GetCountryName(string lang, string code) {
            var countries = GetCountries(lang);
            var country = countries.FirstOrDefault(c => c.Code == code);
            return country?.Name;
        }



    }

    public class Country {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
