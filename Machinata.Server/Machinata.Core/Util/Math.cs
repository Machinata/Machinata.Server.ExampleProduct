using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Math {

        private static Random _rnd = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Returns a random int between a min and max (excluding the max).
        /// This method automatically uses a static unique seed.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns></returns>
        public static int RandomInt(int min, int max) {
            return _rnd.Next(min, max);
        }

        /// <summary>
        /// Returns a random int.
        /// This method automatically uses a static unique seed.
        /// </summary>
        /// <returns></returns>
        public static int RandomInt() {
            return _rnd.Next();
        }

        /// <summary>
        /// Rounds a decimal to the given places (1 by default), but snaps the result if it rounds to ".0".
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="decimalPlaces">The decimal places.</param>
        /// <returns></returns>
        public static string SnapRoundToString(decimal val, int decimalPlaces = 1) {
            var roundedVal = decimal.Round(val, decimalPlaces);
            if (roundedVal == (int)val) return ((int)val).ToString();
            else return roundedVal.ToString();
        }

        /// <summary>
        /// Returns a random int between a min and max (excluding the max).
        /// This method automatically uses a static unique seed.
        /// </summary>
        public static double RandomDouble(double min, double max) {
            return _rnd.NextDouble() * (max - min) + min;
        }
       
 
    }
}
