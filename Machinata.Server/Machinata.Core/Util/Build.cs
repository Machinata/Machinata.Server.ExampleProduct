using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Build {

        public struct BuildInfo {
            public string BuildVersion;
            public string BuildGUID;
            public DateTime BuildTimestamp;
            public string BuildDate;
            public string BuildUser;
            public string BuildMachine;
            public string BuildOS;
            public string BuildArch;
            public string BuildProduct;

            public override string ToString() {
                var ret = "";
                ret += $"version={this.BuildVersion}\n";
                ret += $"guid={this.BuildGUID}\n";
                ret += $"date={this.BuildTimestamp.ToString(Core.Config.DateTimeFormat)}\n";
                ret += $"timestamp={Core.Util.Time.GetUTCMillisecondsFromDateTime(this.BuildTimestamp)}\n";
                ret += $"user={this.BuildUser}\n";
                ret += $"machine={this.BuildMachine}\n";
                ret += $"os={this.BuildOS}\n";
                ret += $"arch={this.BuildArch}\n";
                ret += $"product={this.BuildProduct}\n";
                return ret;
            }

            public JObject ToJSON() {
                var ret = new JObject();
                ret.Add("version", this.BuildVersion);
                ret.Add("guid", this.BuildGUID);
                ret.Add("date", this.BuildTimestamp.ToString(Core.Config.DateTimeFormat));
                ret.Add("timestamp", Core.Util.Time.GetUTCMillisecondsFromDateTime(this.BuildTimestamp));
                ret.Add("user", this.BuildUser);
                ret.Add("machine", this.BuildMachine);
                ret.Add("os", this.BuildOS);
                ret.Add("arch", this.BuildArch);
                ret.Add("product", this.BuildProduct);
                return ret;
            }

            public string ToJSONString() {
                return Core.JSON.Serialize(data: this.ToJSON(), lowerDashed: true, enumToString: true, indendFormatting: true);
            }




        }

        [AttributeUsage(AttributeTargets.Method, Inherited = false)]
        public class BuildInfoAttribute : Attribute {

        }


        public static BuildInfo GetProductBuildInfo() {
            var buildInfoMethods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(BuildInfoAttribute));
            if (buildInfoMethods.Count > 0) {
                // Using reflection and a provider
                var ret = (BuildInfo)buildInfoMethods.First().Invoke(null, null);
                ret.BuildVersion = buildInfoMethods.First().DeclaringType.Assembly.GetName().Version.ToString();
                if (ret.BuildGUID == null || ret.BuildGUID == "") {
                    ret.BuildGUID = Core.Encryption.DefaultHasher.HashString(ret.BuildVersion);
                }
                return ret;
            } else {
                // Fallback
                var ret = GetBuildInfoForModule(Core.Config.ProjectPackage);
                ret.BuildVersion = Assembly.GetCallingAssembly().GetName().Version.ToString();
                if (ret.BuildGUID == null || ret.BuildGUID == "") {
                    ret.BuildGUID = Core.Encryption.DefaultHasher.HashString(ret.BuildVersion);
                }
                return ret;
            }
        }


        public static BuildInfo GetBuildInfoForModule(string module) {
            //TODO: migrate to new reflection system
            string timestampFile = Core.Config.BinPath + Core.Config.PathSep + "Config" + Core.Config.PathSep + module + ".timestamp";
            return _parseTimestampFile(timestampFile);
        }


        private static string _getBuildIDFromAllTimestampFiles() {
            // Compile a build id
            string buildID = "";
            foreach (var assembly in Core.Reflection.Assemblies.GetMachinataAssemblies()) {
                //_logger.Trace("Found Assembly " + assembly.GetName());
                long buildTimestampNumber = 0;
                var module = assembly.GetName().Name;
                Core.Util.Build.BuildInfo buildInfo = new Core.Util.Build.BuildInfo();
                try {
                    buildInfo = Core.Util.Build.GetBuildInfoForModule(assembly.GetName().Name);
                    buildTimestampNumber = buildInfo.BuildTimestamp.Ticks;
                    if (buildTimestampNumber > 0) {
                        buildTimestampNumber = buildTimestampNumber - (new DateTime(2000, 01, 01).Ticks);
                    } else {

                    }
                } catch (Exception e) {
                    buildTimestampNumber = assembly.GetName().Version.Build;
                    //_logger.Warn("  Could not get timestamp, falling back to assembly build number: " + e.Message);
                }
                //_logger.Trace("  Build Timestamp Number " + buildTimestampNumber);
                buildID += buildTimestampNumber.ToString();
            }
            buildID = Core.Encryption.DefaultHasher.HashString(buildID);
            return buildID;
        }

        private static BuildInfo _parseTimestampFile(string timestampFile) {
            var ret = new BuildInfo();
            // Load a timestamp string
            // A timestamp string looks like "BuildTime=Fri 02/08/2019  9:34:32.09;BuildUser=dankrusi"  (including quotes)
            var timestampString = System.IO.File.ReadAllText(timestampFile);
            timestampString = timestampString
                .Replace("\n"," ")
                .Replace("\r"," ")
                .Trim()
                .Trim('\"')
                .Trim();
            // Init
            string buildTimeString = null;
            // Detect version
            if(timestampString.Contains(";") && timestampString.Contains("=")) {
                // New version
                var segs = timestampString.Split(';');
                foreach(var seg in segs) {
                    var key = seg.Split('=').First();
                    var val = seg.Split('=').Last();
                    if (key == "BuildTime") buildTimeString = val;
                    else if (key == "BuildUser") ret.BuildUser = val;
                    else if (key == "BuildMachine") ret.BuildMachine = val;
                }
            } else {
                // Old version - Only a timestamp, ie "Fri 02/08/2019  9:34:32.09"
                buildTimeString = timestampString;
            }
            try {
                if (buildTimeString != null && buildTimeString != "") {
                    //if (timestampString.Contains("/")) throw new Exception("Your machine has not been configured correctly and the timestamp in the build .timestamp file is ambigious. Please update your OS settings.");
                    ret.BuildTimestamp = Core.Util.Time.ParseWindowsConsoleTimestamp(buildTimeString);
                }
            }catch(Exception e) {
                
            }
            return ret;
        }




    }
}
