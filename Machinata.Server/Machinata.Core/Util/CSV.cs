using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public class CSV {

        #region Private Members /////////////////////////////////////////////////////////////////////////////

        private List<Dictionary<string, string>> _rows;
        private string _filePath;

        #endregion 


        public string Name
        {
            get
            {
                return "CSV";
            }
        }

        public string Path
        {
            get
            {
                return _filePath;
            }
        }


        public CSV() {
            _rows = new List<Dictionary<string, string>>();
        }
        

        /// <summary>
        /// Parses a csv
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="delimiter">if null ',' </param>
        public CSV(string filePath, string delimiter = null) {
            _filePath = filePath;
            _rows = new List<Dictionary<string, string>>();
            using (var fileStream = File.OpenText(filePath)) {
                this.ParseCSV(delimiter, fileStream);
            }

        }

        public CSV(string delimiter, StreamReader fileStream, string filePath) {
            _filePath = filePath;
            _rows = new List<Dictionary<string, string>>();
            ParseCSV(delimiter, fileStream);
        }

        private void ParseCSV(string delimiter, StreamReader fileStream) {
            var csv = new CsvHelper.CsvReader(fileStream);

            if (delimiter != null) {
                csv.Configuration.Delimiter = delimiter;
                csv.Configuration.HasHeaderRecord = true;
            }

            var records = csv.GetRecords<dynamic>();

            var columns = new List<string>();
            var r = 0;
            foreach (var record in records) {
                var row = new Dictionary<string, string>();
                foreach (var keyval in record) {
                    row[keyval.Key] = keyval.Value;
                }
                _rows.Add(row);
                r++;
            }
        }

        public void AddRow(Dictionary<string, string> keyValues) {
            _rows.Add(keyValues);
        }

        public IEnumerable<Dictionary<string,string>> GetRows() {
            return _rows;
        }

        /// <summary>
        /// Checks if all the given keys are present in all rows
        /// </summary>
        /// <param name="keys"></param>
        public void CheckColumns(params string [] keys) {
            if (_rows == null) {
                throw new BackendException("check-error", $"CSV has not been initialized yet.");
              
            }
            foreach(var key in keys) {
                int nRow = 0;
                foreach(var row in _rows) {
                    if (row.Keys.Contains(key) == false) {
                       throw new BackendException("check-error", $"CSV row {nRow} is missing key '{key}'");
                    }
                    nRow++;
                }
            }
        }


        /// <summary>
        /// Downloads and parses a CSV from a ContenNode.
        /// Hint: has one global download lock and one global read lock
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="path">The path.</param>
        /// <param name="logger">The logger.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">invalid-data;No Excel file could be found at  + path</exception>
        public static CSV ReadFromContentNode(ModelContext db, string path, NLog.ILogger logger, string name= null, bool allowNoFile = false) {

            logger.Debug("Getting content from CMS...");
            var node = db.ContentNodes()
                .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .GetNodeByPath(path);
            var fileNodes = node.TranslationForLanguage(null).ChildrenForType(ContentNode.NODE_TYPE_FILE);
            ContentNode fileNode = null;

            // No specific name
            if (name == null) {
                fileNode = fileNodes.FirstOrDefault();
            } else {
                // Check name
                var nodesWithName = fileNodes.Where(fn => fn.Options != null && fn.Options["title"]?.ToString() == name);
                if (nodesWithName.Count() > 1) {
                    throw new BackendException("csv-error", $"There are multiple files with the name {name}. Please provide with unique names.");
                }
                fileNode = nodesWithName.FirstOrDefault();
            }

            if (fileNode == null) {
                if (allowNoFile == false) {
                    throw new BackendException("invalid-data", "No Excel file could be found at " + path);
                }
                logger.Debug("Done.");
                return null;
            }

            // Download file
            logger.Debug("Downloading file...");
            string filePath = null;
            lock (_fileDownloadLock) {
                filePath = Core.Data.DataCenter.DownloadFile(db, fileNode.Value);
            }
            logger.Debug("Done.");

            // Open CSV
            logger.Debug("Reading CSV...");
            CSV csv = null;
            lock (_csvReadingLock) {
                csv = new CSV(filePath);
            }

            return csv;
        }

        private static object _fileDownloadLock = new object();
        private static object _csvReadingLock = new object();




    }

}

