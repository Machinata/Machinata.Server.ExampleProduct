using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class Time {

        /// <summary>
        /// The unix epoch date (1970, Jan 1) in UTC.
        /// </summary>
        public static DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

        /// <summary>
        /// Days in solar calendar of 365.2425 days.
        /// </summary>
        public static decimal AVERAGE_DAYS_PER_MONTH = 30.44m;

        /// <summary>
        /// The default timezone info as defined in Core.Config.DefaultTimezone.
        /// </summary>
        public static TimeZoneInfo DEFAULT_TIMEZONE_INFO = TimeZoneInfo.FindSystemTimeZoneById(Core.Config.DefaultTimezone);

        /// <summary>
        /// Gets the UTC milliseconds (utcms) from a date time based on the unix epoch date.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static long GetUTCMillisecondsFromDateTime(System.DateTime date) {
            return ((long)((date - UNIX_EPOCH).TotalMilliseconds));
        }

        /// <summary>
        /// Gets a date time from UTC milliseconds (utcms).
        /// </summary>
        /// <param name="utcms">The utcms.</param>
        /// <returns></returns>
        public static DateTime GetDateTimeFromUTCMilliseconds(long utcms) {
            //return UNIX_EPOCH.AddSeconds(utcms).ToLocalTime();
            return UNIX_EPOCH.AddMilliseconds(utcms).ToUniversalTime();
        }

        /// <summary>
        /// Converts a string to a date time. If the string is a integer,
        /// the format of the string is assumed to be a UTC Milliseconds (utcms) datetime
        /// and is automatically converted, otherwise the datetime is converted using .net
        /// standard conversion.
        /// You cannot pass empty or null strings to this method.
        /// </summary>
        /// <param name="dateString">The date string.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Cannot ConvertStringToUniversalTime because string is null.
        /// or
        /// Cannot ConvertStringToUniversalTime because string is empty.
        /// </exception>
        public static DateTime ConvertStringToDateTime(string dateString) {
            // Sanity
            if (dateString == null) throw new Exception("Cannot ConvertStringToUniversalTime because string is null.");
            if (dateString == "") throw new Exception("Cannot ConvertStringToUniversalTime because string is empty.");
            // Is it a utcms string?
            long utcms;
            if(long.TryParse(dateString, out utcms)) {
                // Convert from utcms string
                return GetDateTimeFromUTCMilliseconds(utcms);
            } else {
                // Do standard conversion
                //if(dateString.EndsWith(" UTC"))
                return DateTime.Parse(dateString);
            }
        }

        public static DateTime ConvertStringToUTCDateTime(string dateString) {
            var date = ConvertStringToDateTime(dateString);
            return date.ToUniversalTime();
        }

        public static bool IsBetweenTimeRange(DateTime date, string timerange) {
            var segs = timerange.Split('-');
            if (segs.Length != 2) throw new Exception($"The timerange '{timerange}' is not valid.");
            int minHour;
            int maxHour;
            try {
                minHour = int.Parse(segs[0]);
                maxHour = int.Parse(segs[1]);
            } catch {
                throw new Exception($"The timerange '{timerange}' is not valid.");
            }
            if(minHour >= maxHour) throw new Exception($"The timerange '{timerange}' is not valid.");
            // Conver to day minutes
            int minTotalMinutes = minHour * 60;
            int maxTotalMinutes = maxHour * 60;
            if(date.TotalMinutesInDay() < minTotalMinutes || date.TotalMinutesInDay() > maxTotalMinutes) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines whether date is on one of the specified valid days.
        /// Valid days can be a comma seperated string of english weekday names.
        /// For example: "Monday, Tuesday, Wednesday, Thursday, Friday"
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="validDays">The valid days.</param>
        /// <returns></returns>
        public static bool IsOnDayOfWeek(DateTime date, string validDays) {
            var days = validDays.ToLower().Replace(" ","").Split(',');
            return days.Contains(date.DayOfWeek.ToString().ToLower());
        }

        public static Model.DateRange ConvertStringToUTCDateRange(string dateRangeString) {
            // Init
            var ret = new Model.DateRange();
            if (string.IsNullOrEmpty(dateRangeString)) return ret;
            var segs = dateRangeString.Split(new [] { Core.Config.DateRangeSeperator }, StringSplitOptions.None);
            // Sanity
            if (segs.Length != 2) throw new BackendException("invalid-date-range",$"The date range '{dateRangeString}' is not valid.");
            ret.Start = ConvertStringToUTCDateTime(segs[0]);
            ret.End = ConvertStringToUTCDateTime(segs[1]);
            return ret;
        }

        public static DateTime ConvertToDefaultTimezone(DateTime date) {
            //if (date.Kind != DateTimeKind.Utc) throw new Exception("Could not convert date to default timezone because the date is not UTC.");
            return TimeZoneInfo.ConvertTimeFromUtc(date, DEFAULT_TIMEZONE_INFO);
        }

        /// <summary>
        /// From utc to local default timezone
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime ToDefaultTimezone(this DateTime date) {
            return ConvertToDefaultTimezone(date);
        }

        public static DateTime ConvertToUTCTimezone(DateTime date) {
            return date.ToUniversalTime();
        }

        public static DateTime GetDefaultTimezoneNow() {
            return ConvertToDefaultTimezone(DateTime.UtcNow);
        }

        public static DateTime GetDefaultTimezoneToday() {
            var now = GetDefaultTimezoneNow(); //TODO: @micha bug?
            return new DateTime(now.Year, now.Month, now.Day);
        }

        /// <summary>
        /// Gets the UTC timezone today for default timezone.
        /// This is useful if you want to compare system dates for the local date boundaries.
        /// For example, say you have an object saved with just the date information for 07.11.2017.
        /// In the db, this will be saved as 06.11.2017 11:00 PM, as this is the corresponding UTC.
        /// If you want to quickly find all objects that are on 07.11.2017, you will actually need to 
        /// compare with the corresponding UTC timezone dates, ie 06.11.2017 11:00 PM to 07.11.2017 11:00 PM.
        /// This method helps you.
        /// For example: 
        /// var today = Core.Util.Time.GetUTCTimezoneTodayForDefaultTimezone();
        /// return entities.Where(s => s.Date >= today);
        /// </summary>
        /// <returns></returns>
        public static DateTime GetUTCTimezoneTodayForDefaultTimezone() {
            // Get the today date for default timezone
            var today = Core.Util.Time.ConvertToDefaultTimezone(DateTime.UtcNow).Date;
            // Return it as UTC
            return Core.Util.Time.ConvertToUTCTimezone(today);
        }

        public static DateTime GetUTCTimezoneDateForDefaultTimezone(DateTime dateTime) {
            // Get the today date for default timezone
            var today = Core.Util.Time.ConvertToDefaultTimezone(dateTime).Date;
            // Return it as UTC
            return Core.Util.Time.ConvertToUTCTimezone(today);
        }


        // https://stackoverflow.com/questions/38039/how-can-i-get-the-datetime-for-the-start-of-the-week
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday) {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0) {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
        
        public static DateTime StartOfMonth(this DateTime dt) {
            return new DateTime(dt.Year, dt.Month, 1);
        }


        public static DateTime StartOfYear(this DateTime dt) {
            return new DateTime(dt.Year, 1, 1);
        }

        public static DateTime EndOfMonth(this DateTime dt) {
            var ret = new DateTime(dt.Year, dt.Month, 1);
            ret = ret.AddMonths(1);
            ret = ret.AddSeconds(-1);
            return ret;
        }

        public static DateTime EndOfYear(this DateTime dt) {
            var ret = new DateTime(dt.Year + 1, 1, 1);
            ret = ret.AddSeconds(-1);
            return ret;
        }

        public static DateTime StartOfDay(this DateTime dt) {
            return new DateTime(dt.Year, dt.Month, dt.Day);
        }
        public static DateTime EndOfDay(this DateTime dt) {
            const bool USE_OLD_METHOD = false;
            if(USE_OLD_METHOD) {
                return dt.Date.AddDays(1).AddTicks(-1); //dankrusi: this seems to be buggy if you can't save a tick in resolution
            } else {

            }
            var ret = new DateTime(dt.Year, dt.Month, dt.Day);
            ret = ret.AddDays(1);
            ret = ret.AddSeconds(-1);
            return ret;
        }

        /// <summary>
        /// Gets the start of week (default time zone) from a utc datetime and returns a utc time
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        public static DateTime StartOfDefaultTimezoneWeekInUtc(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday) {
            var defaultTimeZone = ConvertToDefaultTimezone(dt);
            var startDefaultTimezone = defaultTimeZone.StartOfWeek(startOfWeek);
            return ConvertToUTCTimezone(startDefaultTimezone);
        }

        /// <summary>
        /// Gets the start of the quarter (default time zone) from a utc datetime and returns a utc time
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        public static DateTime StartOfDefaultTimezoneQuarterInUtc(this DateTime dt) {
            var defaultTimeZone = ConvertToDefaultTimezone(dt);
            var startDefaultTimezone = defaultTimeZone.StartOfQuarter();
            return ConvertToUTCTimezone(startDefaultTimezone);
        }

        public static DateTime StartOfDefaultTimezoneEndOfQuarterInUtc(this DateTime dt) {
            var defaultTimeZone = ConvertToDefaultTimezone(dt);
            var startDefaultTimezone = defaultTimeZone.EndOfQuarter();
            return ConvertToUTCTimezone(startDefaultTimezone);
        }

        /// <summary>
        /// Last tick of the week, careful usage!
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        public static DateTime EndOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday) {
            return dt.StartOfWeek(startOfWeek).AddDays(7).AddTicks(-1);
        }

        /// <summary>
        /// End the of week. Start has to be the start of the week!
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public static DateTime EndOfWeek(this DateTime dt) {
            return dt.AddDays(7).AddTicks(-1);
        }



        public static int QuarterNumber(this DateTime dateTime) {
            int quarterNumber = (dateTime.Month - 1) / 3 + 1;
            return quarterNumber;
        }

        // https://stackoverflow.com/questions/1492079/calculate-the-start-date-and-name-of-a-quarter-from-a-given-date        
        /// <summary>
        /// First day of the quarter,
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime StartOfQuarter(this DateTime dateTime) {
            int quarterNumber = dateTime.QuarterNumber();
            DateTime firstDayOfQuarter = new DateTime(dateTime.Year, (quarterNumber - 1) * 3 + 1, 1);
            // DateTime lastDayOfQuarter = firstDayOfQuarter.AddMonths(3).AddDays(-1);
            return firstDayOfQuarter;
        }

        /// <summary>
        /// Last day of the quarter
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime EndOfQuarter(this DateTime dateTime) {
            DateTime lastDayOfQuarter = dateTime.StartOfQuarter().AddMonths(3).AddDays(-1);
            return lastDayOfQuarter;
        }
        


        //https://stackoverflow.com/questions/11154673/get-the-correct-week-number-of-a-given-date
        public static int GetIso8601WeekOfYear(this DateTime time) {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday) {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return System.Globalization.CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        
        public static List<Core.Util.Time.PublicHoliday> GetPublicHolidays(DateTime start, DateTime end, string countryCode = null) {
            if (countryCode == null) countryCode = Core.Config.GetStringSetting("DefaultHolidayCountryCode");
            var countyCode = Core.Config.GetStringSetting("DefaultHolidayCountyCode",null); 
            var holidays = Nager.Date.DateSystem.GetPublicHoliday(countryCode, start, end).ToList();
            var ret = new List<Core.Util.Time.PublicHoliday>();
            foreach(var holiday in holidays) {
                // If our project has a county code set, make sure that this holiday matches, other we ignore it
                // Note: this does not apply to global holidays, which are always valid
                if(countyCode != null && holiday.Global == false) {
                    if (!holiday.Counties.Contains(countyCode)) continue;
                }
                ret.Add(new PublicHoliday() {
                    CountyAdministrationHoliday = holiday.CountyAdministrationHoliday,
                    CountyOfficialHoliday = holiday.CountyOfficialHoliday,
                    Date = holiday.Date,
                    LocalName = holiday.LocalName,
                    Name = holiday.Name
                });
            }
            return ret;
        }

        /// <summary>
        /// Gets the (western) working days - taking into account weekends and 
        /// public holidays. If no country code is specified, then then setting
        /// DefaultHolidayCountryCode is used.
        /// Note: When using this method, you should always use the local (default) timezone
        /// if you originally converted a local timezone to UTC time.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="countryCode">The country code.</param>
        /// <returns></returns>
        public static int GetWesternWorkingDays(DateTime start, DateTime end, string countryCode = null) {
            var holidays = GetPublicHolidays(start, end, countryCode).Select(e => e.Date).ToList();
            return GetWesternBusinessDays(start, end, holidays);
        }

        /// <summary>
        /// Gets the (western) business days, not including holidays.
        /// If holidays are provided, then those are taken into account (not just weekends).
        /// Use GetWesternWorkingDays to automatically include holidays.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="holidays">The holidays.</param>
        /// <returns></returns>
        public static int GetWesternBusinessDays(DateTime start, DateTime end, List<DateTime> holidays = null) {
            // Init
            var businessDays = 0;
            start = start.Date;
            //end = end.Date;
            var range = end - start;
            var days = System.Math.Ceiling(range.TotalDays);
            // Special case
            if (start == end) {
                //TODO
            }
            // Loop each day in range
            for(var d = 0; d < days; d++) {
                var date = start.AddDays(d).Date;
                if(date.DayOfWeek == DayOfWeek.Saturday) {
                    // Weekend
                } else if(date.DayOfWeek == DayOfWeek.Sunday) {
                    // Weekend
                } else {
                    bool isHoliday = false;
                    if (holidays != null) {
                        foreach (var holiday in holidays) {
                            if(holiday.Date == date) {
                                isHoliday = true;
                                break;
                            }
                        }
                    }
                    // Business day!
                    if(!isHoliday) businessDays++;
                }
            }
            return businessDays;
        }

        public static TimeSpan GetTimespanFromString(string namedTimespan, TimeSpan defaultTimespan = new TimeSpan()) {
            if (string.IsNullOrEmpty(namedTimespan)) {
                return defaultTimespan;
            }

            // Fixed internvals                       
            if (namedTimespan == "year") return new TimeSpan(365, 0, 0, 0); // d,h,m,s
            else if (namedTimespan == "month") return new TimeSpan(30, 0, 0, 0); // d,h,m,s
            else if (namedTimespan == "day") return new TimeSpan(1, 0, 0, 0); // d,h,m,s
            else if (namedTimespan == "hour") return new TimeSpan(0, 1, 0, 0); // d,h,m,s
            else if (namedTimespan == "minute") return new TimeSpan(0, 0, 1, 0); // d,h,m,s
            else if (namedTimespan == "monthly")  return new TimeSpan(31, 0, 0, 0); // d,h,m,s
            else if (namedTimespan == "daily") return new TimeSpan(1, 0, 0, 0); // d,h,m,s
            
            // Custom intervals
            if (namedTimespan.EndsWith("y")) {
                var interval = namedTimespan.Replace("y", "");
                return new TimeSpan(int.Parse(interval)*365, 0, 0, 0); // d,h,m,s
            } else if (namedTimespan.EndsWith("d")) {
                var interval = namedTimespan.Replace("d", "");
                return new TimeSpan(int.Parse(interval), 0, 0, 0); // d,h,m,s
            } else if (namedTimespan.EndsWith("h")) {
                var interval = namedTimespan.Replace("h", "");
                return new TimeSpan(int.Parse(interval), 0, 0); // h,m,s
            } else if (namedTimespan.EndsWith("m")) {
                var interval = namedTimespan.Replace("m", "");
                return new TimeSpan(0, int.Parse(interval), 0); // h,m,s
            } else if (namedTimespan.EndsWith("s")) {
                var interval = namedTimespan.Replace("s", "");
                return new TimeSpan(0, 0, int.Parse(interval)); // h,m,s
            }else {
                throw new Exception($"The internval {namedTimespan} is not valid.");
            }
        }

        public static IEnumerable<DateTime> GetDates(DateTime start, DateTime end) {
            List<DateTime> dates = new List<DateTime>();
            start = start.Date;
            do {
                dates.Add(start);
            } while ((start = start.AddDays(1)) < end);
            return dates;
        }

        /// <summary>
        /// Gets the months between the given dates
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns>list of 'year,month'</returns>
        public static IEnumerable<Tuple<int, int>> GetMonths(DateTime start, DateTime end) {
            var dates = new List<DateTime>();
            for (DateTime day = (start.Date); day < end; day = day.AddDays(1)) {
                if (!dates.Any(d => d.Year == day.Year && d.Month == day.Month)) {
                    dates.Add(day);
                }
            }
            return dates.Select(d => new Tuple<int, int>(d.Year, d.Month));
        }

        public static string HumanReadableDurationForDateRange(Model.DateRange range) {
            decimal days = (decimal)((range.End.Value - range.Start.Value).TotalDays);
            return HumanReadableDurationForDays(days);
        }
    
        public static string HumanReadableDurationForDays(decimal days) {
            if (days == 0) {
                return null;
            } else if (days < 1) {
                return Core.Util.Math.SnapRoundToString(days,1) + " {text.days}";
            }  else if (days == 1) {
                return "1 {text.day}";
            } else if (days == 7) {
                return "1 {text.week}";
            } else if (days == 14) {
                return "2 {text.weeks}";
            } else if (days < 15) {
                return (int)days + " {text.days}";
            } else if (days == 15) {
                return "0.5 {text.months}";
            } else if (days == 21) {
                return "3 {text.weeks}";
            } else if (days < 30) {
                return (int)days + " {text.days}";
            } else if (days == 30) {
                return "1 {text.month}";
            } else if (days < 365) {
                return Core.Util.Math.SnapRoundToString((days/(365/12)),1) + " {text.months}";
            } else {
                return Core.Util.Math.SnapRoundToString((days/365),1) + " {text.years}";
            }
        }

        public static DateTime ParseWindowsConsoleTimestamp(string timestamp) {
            // "Fri 03/08/2019  9:34:32.09" on a USA computer
            // "08.03.2019  9:34:32.09" on EU computer
            // "11/11/2019 17:56:41,23" on English/Switzerland
            var dayTrims = new string[]{"Mon ", "Tue ", "Wed ", "Thu ", "Fri ", "Sat ", "Sun "};
            foreach(var dayTrim in dayTrims) {
                timestamp = timestamp.ReplacePrefix(dayTrim, "");
            }
            timestamp = timestamp.Replace("  "," ");
            var provider = System.Globalization.CultureInfo.InvariantCulture;
            if (timestamp.Count(c => c == '/') == 2 && timestamp.Count(c=> c == ',') == 1){
                // English/Switzerland windows
                return DateTime.ParseExact(timestamp, "dd/MM/yyyy H:mm:ss,ff", provider);
            }
            if (timestamp.Count(c => c == '/') == 2) {
                // USA probably - ??
                return DateTime.ParseExact(timestamp,"MM/dd/yyyy H:mm:ss.ff",provider);
            } else {
                // EU probably - ??
                return DateTime.ParseExact(timestamp,"dd.MM.yyyy H:mm:ss.ff",provider);
            }
        }


        /// <summary>
        /// Parses the date exactl with the Core.Config.DateFormat
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <returns></returns>
        /// <exception cref="BackendException">date-error</exception>
        public static DateTime ParseDate(string date, DateTime defaultValue, bool throwException) {
            // Date
            DateTime orderDate = defaultValue;
            var provider = System.Globalization.CultureInfo.InvariantCulture;
            var success = DateTime.TryParseExact(date, Core.Config.DateFormat, provider, System.Globalization.DateTimeStyles.None, out orderDate);
            if (success == false && throwException == true) {
                throw new BackendException("date-error", $"Could not parse the date: {date}");
            }
            return orderDate;
        }


        /// <summary>
        /// Checkes whether the given string is a weekday 
        /// english and case insensitive
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsWeekday(string value) {

            if (string.IsNullOrWhiteSpace(value)) {
                return false;
            }
            var weekdays = Core.Util.EnumHelper.GetEnumValues<DayOfWeek>(typeof(System.DayOfWeek)).Select(d => d.ToString().ToLower());

            if (weekdays.Contains(value.ToLower()) == true) {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Parses the date exactl with the Core.Config.DateFormat
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <returns></returns>
        /// <exception cref="BackendException">date-error</exception>
        public static DateTime ParseDateTime(string date, DateTime defaultValue, bool throwException) {
            // Date
            DateTime datetime = defaultValue;
            var provider = System.Globalization.CultureInfo.InvariantCulture;
            var success = DateTime.TryParseExact(date, Core.Config.DateTimeFormat, provider, System.Globalization.DateTimeStyles.None, out datetime);
            if (success == false && throwException == true) {
                throw new BackendException("date-error", $"Could not parse the date: {date}");
            }
            return datetime;
        }

        #region Extension Methods DateTime

        public static int TotalMinutesInDay(this DateTime date) {
                    return (date.Hour * 60) + (date.Minute);
        }

        public static string ToDefaultTimezoneDateTimeString(this DateTime date) {
            return Core.Util.Time.ConvertToDefaultTimezone(date).ToString(Core.Config.DateTimeFormat);
        }

        public static string ToDefaultTimezoneDateString(this DateTime date) {
            return Core.Util.Time.ConvertToDefaultTimezone(date).ToString(Core.Config.DateFormat);
        }

        public static string ToDefaultTimezoneDateString(this DateTime? date) {
            if (date == null) {
                return null;
            }
            return Core.Util.Time.ConvertToDefaultTimezone(date.Value).ToString(Core.Config.DateFormat);
        }

        public static string ToDefaultTimezoneTimeString(this DateTime date) {
            return Core.Util.Time.ConvertToDefaultTimezone(date).ToString(Core.Config.TimeFormat);
        }

        public static bool IsDateInLaterMonth(this DateTime isBefore, DateTime isAfter) {
            return isBefore < isAfter && isBefore.Month != isAfter.Month;
        }

        /// <summary>
        /// Gets a Config.DateFormat representation of the date
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string ToDateString(this DateTime date) {
            return date.ToString(Core.Config.DateFormat);
        }

        public static string ToHumanReadableDateString(this DateTime date) {
            return date.ToString(Core.Config.DateFormatHumanReadable);
        }

        public static string ToMonthString(this DateTime date) {
            return date.ToString(Core.Config.MonthFormat);
        }


        /// <summary>
        /// Gets a Config.DateTimeFormat representation of the date
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string ToDateTimeString(this DateTime date) {
            return date.ToString(Core.Config.DateTimeFormat);
        }

        #endregion


        public class PublicHoliday {
        
            public bool CountyAdministrationHoliday { get; set; }
            public bool CountyOfficialHoliday { get; set; }
            public DateTime Date { get; set; }
            public string LocalName { get; set; }
            public string Name { get; set; }
        
        }
    }

}


