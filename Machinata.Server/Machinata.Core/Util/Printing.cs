using System;
using System.ComponentModel.DataAnnotations;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Localization;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Machinata.Core.Util.Printing {

    public class PrintJob {
        
        public const string PARAMETER_SOURCE = "Source";
        public const string PARAMETER_PREVIEW = "Preview";
        public const string PARAMETER_TITLE = "Title";
        public const string PARAMETER_QUANTITY = "Quantity";
        public const string REMOTE_TASK_NAME = "MachinataEmbedded.Module.Printing.Printing.PrintingTask";

        /// <summary>
        /// Creates the print job.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="source">The source.</param>
        /// <param name="preview">The image preview, can be null.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="target">The target.</param>
        /// <param name="start">if set to <c>true</c> [start].</param>
        /// <returns></returns>
        public static RemoteTask CreatePrintJob(ModelContext db, string source, string preview, string title, int quantity, string target = "*", bool start = false) {
            var task = new RemoteTask {
                Target = target,
                State = RemoteTask.RemoteTaskState.Created,
                Name = REMOTE_TASK_NAME
            };
            task.Parameters[PARAMETER_QUANTITY] = Core.Config.IsTestEnvironment ? 1 : quantity;
            task.Parameters[PARAMETER_SOURCE] = source;
            task.Parameters[PARAMETER_TITLE] = title;
            if(!string.IsNullOrEmpty(preview)) task.Parameters[PARAMETER_PREVIEW] = preview;

            if (start) {
                task.State = RemoteTask.RemoteTaskState.Waiting;
                task.Enabled = true;
                db.RemoteTasks().Add(task);
            }

            return task;
        }
    }
}
