using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class EnumHelper {

        /// <summary>
        /// Gets the enum title by looking for a suitable translation using the following formats:
        /// 
        /// {class.type-name}.{enum.type-name}.{enum.value}
        /// {enum.type-name}.{enum.value}
        /// enum.{enum.value}
        /// 
        /// If no translation is found, then the default System.Enum.GetName is returned.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public static string GetEnumTitle(this Enum val, string language) {
            // Attempt to get a translation for this enum value
            var type = val.GetType();
            string[] textIds = {
                "{class.type-name}.{enum.type-name}.{enum.value}",
                "{enum.type-name}.{enum.value}",
                "enum.{enum.value}"
            };
            foreach(var textId in textIds) {
                var formattedTextId = textId.Replace("{class.type-name}",type.DeclaringType?.Name.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{enum.type-name}", type.Name.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{enum.value}",val.ToString().ToDashedLower());
                var translation = Core.Localization.Text.GetTranslatedTextByIdForLanguage(formattedTextId, language, null);
                if (translation != null) return translation;
            }
            // Fallback
            return System.Enum.GetName(type, (Enum)val);
        }

        /// <summary>
        /// Gets the enum translation identifier (if any) using the following formats:
        /// 
        /// {class.type-name}.{enum.type-name}.{enum.value}
        /// {enum.type-name}.{enum.value}
        /// enum.{enum.value}
        /// 
        /// If no translation is found, then null is returned.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        public static string GetEnumTranslationId(this Enum val) {
            var type = val.GetType();
            string[] textIds = {
                "{class.type-name}.{enum.type-name}.{enum.value}",
                "{enum.type-name}.{enum.value}",
                "enum.{enum.value}"
            };
            foreach(var textId in textIds) {
                var formattedTextId = textId.Replace("{class.type-name}",type.DeclaringType?.Name.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{enum.type-name}", type.Name.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{enum.value}",val.ToString().ToDashedLower());
                var texts = Core.Localization.Text.GetTexts(formattedTextId);
                if (texts != null && texts.Count > 0) return formattedTextId;
            }
            return null;
        }

        /// <summary>
        /// Gets the enum translation text variable to be used in a template, such as a {text.status-type.sent}.
        /// This method will automatically fallback to plain-text if no translation exists, and is usefull if the
        /// language is not yet known.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        public static string GetEnumTranslationTextVariable(this Enum val) {
            var translationTextId = EnumHelper.GetEnumTranslationId(val);
            if (translationTextId != null) return "{text." + translationTextId.ToString() + "}";
            else return val.ToString(); // Fallback
        }

        public static string GetEnumDescription(this Enum value) {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) {
                return attributes[0].Description;
            } else {
                return value.ToString();
            }
        }

        public static T ParseEnum<T>(string value) {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T ParseEnum<T>(string value, bool ignoreCase, T defaultValue) where T:struct{
            T parsed= default(T);
            if (Enum.TryParse(value, ignoreCase, out parsed)) {
                return parsed;
            }
            return defaultValue;
        }

        public static IEnumerable<T> GetEnumValues<T>(Type EnumType) {
            return Enum.GetValues(EnumType).Cast<T>().AsEnumerable();
        }
    }
}
