using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public static class ReflectionExtensions {
        
        public static string GetVariableName(this PropertyInfo prop) {
            return prop.Name.ToDashedLower();
        }

        public static bool IsDerived(this PropertyInfo prop) {
            var notMapped = prop.GetCustomAttribute<NotMappedAttribute>();
            if (notMapped != null) return true;
            return false;
        }

    }
}
