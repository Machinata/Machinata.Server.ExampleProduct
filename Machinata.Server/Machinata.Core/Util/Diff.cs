using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Diff {

        public static string GetHTMLDiff(string a, string b) {
            if (a == null) a = "";
            if (b == null) b = "";
            var differ = new DiffMatchPatch.DiffMatchPatch(0,0,0,0,0,0,0,0);
            var diffs = differ.DiffMain(a, b);
            return differ.DiffPrettyHtml(diffs);
        }


        public static string GetTextDiff(string a, string b) {
            if (a == null) a = "";
            if (b == null) b = "";
            var differ = new DiffMatchPatch.DiffMatchPatch(0, 0, 0, 0, 0, 0, 0, 0);
            var diffs = differ.DiffMain(a, b);
            return differ.DiffPrettyHtml(diffs);
        }

    }
}
