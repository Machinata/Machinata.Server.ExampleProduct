using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public static class Dictionary {

        public static Dictionary<string,string> ExtendBaseDictionaryWithSpecificDictionary(Dictionary<string,string> baseDic, Dictionary<string,string> specDic) {
            var ret = new Dictionary<string, string>();
            foreach(var k in baseDic) {
                ret.Add(k.Key, k.Value);
            }foreach(var k in specDic) {
                ret[k.Key] = k.Value;
            }
            return ret;
        }

        /// <summary>
        /// Gets the string value for a string,string dictioinary .if key doenst exists null will be returned.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetStringValue(this Dictionary<string,string> dictionary, string key, bool keyToLower = false) {
            if (keyToLower) {
                key = key.ToLower();
            }
            if (dictionary.ContainsKey(key)) {
                return dictionary[key];
            }
            return null;
        }

        public static string ToString(this Dictionary<string, string> dictionary, char separator) {
            return string.Join(separator.ToString(), dictionary.Select(x => x.Key + "=" + x.Value));
        }

        public static string ToString(this IDictionary<string, string> dictionary, char separator) {
            return string.Join(separator.ToString(), dictionary.Select(x => x.Key + "=" + x.Value));
        }


        /// <summary>
        /// https://stackoverflow.com/questions/7230383/c-convert-dictionary-to-namevaluecollection
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <returns></returns>
        public static NameValueCollection ToNameValueCollection(this IDictionary<string, string> dict) {
            return dict.Aggregate(
                new NameValueCollection(),
                (seed, current) => {
                    seed.Add(current.Key, current.Value);
                    return seed;
                     });
        }


    }
}
