using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Util {
    public class EmbedCodes {

        /// <summary>
        /// Automatically gets a embed code for the given URL.
        /// Only specific services are supported.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static string GetEmbedCodeForURL(string url) {
            // Convert to https
            if (url.StartsWith("http://")) url = url.Replace("http://", "https://");
            if (!url.StartsWith("https://")) url = "https://" + url;
            // Get embed
            if(url.StartsWith("https://www.youtube.com/watch?v=")) {
                // Youtube
                // https://www.youtube.com/watch?v=p22kE9B2pKg
                var id = url.Replace("https://www.youtube.com/watch?v=", "");
                // Original: "<div style=\"position:relative;height:0;padding-bottom:56.25%\"><iframe src=\"https://www.youtube.com/embed/"+id+"\" width=\"640\" height=\"360\" frameborder=\"0\" style=\"position:absolute;width:100%;height:100%;left:0\" allowfullscreen></iframe></div>";
                return "<div class=\"embed youtube\"><iframe src=\"https://www.youtube.com/embed/"+id+"\" frameborder=\"0\" style=\"width:100%;height:100%;\" allowfullscreen></iframe></div>";
            } else if(url.StartsWith("https://soundcloud.com/")) {
                // SoundCloud
                // https://soundcloud.com/liluzivert/15-ysl
                // Original: "<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url="+HttpUtility.UrlEncode(url)+"&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>";
                return "<div class=\"embed soundcloud\"><iframe scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url="+HttpUtility.UrlEncode(url)+"&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe></div>";
            } else if(url.StartsWith("https://open.spotify.com/")) {
                // Spotify
                // https://open.spotify.com/track/1tAGSGWlQmYWR8X5zuUpvh
                var id = url.Replace("https://open.spotify.com/", "");
                // Original: "<iframe src=\"https://open.spotify.com/embed/"+id+"\" width=\"600\" height=\"100\" frameborder=\"0\" allowtransparency=\"true\"></iframe>";
                return "<div class=\"embed spotify\"><iframe src=\"https://open.spotify.com/embed/"+id+"\" frameborder=\"0\" allowtransparency=\"true\"></iframe></div>";
            } else if(url.StartsWith("https://vimeo.com/")) {
                // Vimeo 
                // https://vimeo.com/230559377
                var id = url.Replace("https://vimeo.com/", "");
                return "<div class=\"embed vimeo\"><iframe src=\"https://player.vimeo.com/video/"+id+"\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>";
            }  else if(url.StartsWith("https://twitter.com/")) {
                // Twitter 
                // https://twitter.com/srfnews/status/900271529425907712
                // Original: <blockquote class="twitter-tweet" data-lang="en-gb"><p lang="de" dir="ltr">Bundesratssitz: Dass die Tessiner immer den Kürzeren ziehen, könne man nicht sagen, sagt Politologe Lutz. <a href="https://t.co/aeAVZZwJzd">https://t.co/aeAVZZwJzd</a> ^mh</p>&mdash; SRF News (@srfnews) <a href="https://twitter.com/srfnews/status/900271529425907712">23 August 2017</a></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                return "<div class=\"embed twitter\"><blockquote class=\"twitter-tweet\"><p></p><a href=\""+url+"\"></a></blockquote><script async src=\"//platform.twitter.com/widgets.js\" charset=\"utf-8\"></script></div>";
            } else {
                throw new BackendException("invalid-embed-url",$"The URL '{url}' is not supported. The following services are supported: YouTube, Spotify, SoundCloud, Vimeo, Twitter.");
            }
        }

    }
}
