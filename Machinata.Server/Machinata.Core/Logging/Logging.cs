using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;

namespace Machinata.Core {

    /// <summary>
    /// Utility class for setting up different log targets using the server configuration.
    /// </summary>
    public class Logging {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string LOG_SEPARATOR = "=================================================================";
        
        private static Dictionary<string,NLog.Targets.Target> _loadedTargets = new Dictionary<string, NLog.Targets.Target>();
        private static Dictionary<string,NLog.Config.LoggingRule> _loadedRules = new Dictionary<string, NLog.Config.LoggingRule>();

        private static List<string> _startupLogs = null;
        
        public static NLog.Targets.LimitedMemoryTarget MemoryTarget {
            get {
                if (_loadedTargets.ContainsKey("memory")) return _loadedTargets["memory"] as NLog.Targets.LimitedMemoryTarget;
                else return null;
            }
        }

        public static NLog.Targets.LimitedMemoryTarget StartupTarget {
            get {
                if (_loadedTargets.ContainsKey("startup")) return _loadedTargets["startup"] as NLog.Targets.LimitedMemoryTarget;
                else return null;
            }
        }

        public static void CaptureStartupLogs() {
            _loadLoggerForConfiguration("startup","*","Trace",int.MaxValue);
        }

        public static List<string> GetStartupLogs() {
            return _startupLogs;
        }

        public static void RegisterStartupLogs() {
            if (StartupTarget == null) {
                _startupLogs = new List<string>();
                return;
            }
            // Get logs
            _startupLogs = StartupTarget.Logs.ToList(); // copy
            // Shut it down
            LogManager.Configuration.RemoveTarget("startup");
            LogManager.ReconfigExistingLoggers();
        }
    
        public static void OnApplicationStartup() {
            // Find all config keys that match environment and load the logger
            foreach(var key in Core.Config.FindForEnvironement("LogTarget-")) {
                _loadLoggerForConfiguration(key);
            }
        }

        private static void _loadLoggerForConfiguration(string configKey) {
            var logConfig = Core.Config.GetKeyValueSetting(configKey);
            int maxLines = 0;
            if(logConfig.ContainsKey("max-lines")) {
                maxLines = int.Parse(logConfig["max-lines"]);
            }
            _loadLoggerForConfiguration(logConfig["target"],logConfig["pattern"],logConfig["min-level"],maxLines);
        }

        private static void _loadLoggerForConfiguration(string target, string pattern, string logLevel, int maxLines) {
            // Init
            _logger.Info($"Loading logger '{target}' with pattern '{pattern}' and log level {logLevel}...");
            NLog.Targets.Target logTarget = null;
            var targetKey = target;
            var ruleKey = targetKey + "_" + pattern + "_" + logLevel;
            // Rule already exist?
            if (_loadedRules.ContainsKey(ruleKey)) {
                return;
            }
            // Do we need a config?
            if(LogManager.Configuration == null) LogManager.Configuration = new NLog.Config.LoggingConfiguration();
            // Does the target exist?
            if (!_loadedTargets.ContainsKey(targetKey)) {
                // Create new target
                if(targetKey == "memory" || targetKey == "startup") {
                    // Memory
                    _logger.Info($"  Target is memory");
                    var memoryTarget = new NLog.Targets.LimitedMemoryTarget();
                    if(maxLines > 0) memoryTarget.Limit = maxLines;
                    LogManager.Configuration.AddTarget(targetKey, memoryTarget);
                    logTarget = memoryTarget;
                } else if (targetKey == "console") {
                    // Console
                    _logger.Info ($"  Target is console");
                    var consoleTarget = new NLog.Targets.ColoredConsoleTarget ();
                    LogManager.Configuration.AddTarget (targetKey, consoleTarget);
                    logTarget = consoleTarget;
                } else {
                    // File
                    _logger.Info($"  Target is file");
                    var fileName = targetKey;
                    fileName = fileName.Replace("{yyyy}", DateTime.UtcNow.ToString("yyyy"));
                    fileName = fileName.Replace("{mm}", DateTime.UtcNow.ToString("MM"));
                    fileName = fileName.Replace("{dd}", DateTime.UtcNow.ToString("dd"));
                    var fileTarget = new NLog.Targets.FileTarget();
                    fileTarget.FileName = Core.Config.LogPath + Core.Config.PathSep + fileName;
                    LogManager.Configuration.AddTarget(targetKey, fileTarget);
                    logTarget = fileTarget;
                } 
                // Register the target
                _loadedTargets[targetKey] = logTarget;
            }
            // The target exists, now create the log rule
            var logRule = new NLog.Config.LoggingRule(pattern, LogLevel.FromString(logLevel), logTarget);
            LogManager.Configuration.LoggingRules.Add(logRule);
            _loadedRules[ruleKey] = logRule;
            // Activate the configuration
            LogManager.ReconfigExistingLoggers();
        }

    }
}
