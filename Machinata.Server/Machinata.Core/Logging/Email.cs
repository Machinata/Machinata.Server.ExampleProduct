using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core {
    public class EmailLogger {

        public static void SendMessageToAdminEmail(string subject, string senderPackage, HttpContext context, Handler.Handler handler, Exception e) {
            var content = CompileFullErrorReport(context, handler, e);
            MessageCenter.SendMessageToAdminEmail(subject, content, senderPackage);
        }


        public static void SendMessageToAdminEmail(string subject, string additionalText, string senderPackage, HttpContext context, Handler.Handler handler, Exception e) {
            var content = CompileFullErrorReport(context, handler, e);

            if (string.IsNullOrEmpty(additionalText)== false) {
                content += "\n\n\n" + "Additional: ";
                content += "\n" + additionalText;
            }

            MessageCenter.SendMessageToAdminEmail(subject, content, senderPackage);
        }


        public static string CompileFullErrorReport(HttpContext context, Handler.Handler handler, Exception e) {
            // Get additional informatio about the error
            if (e == null) return "Exception was 'null'";
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(e, true);
            System.Diagnostics.StackFrame stackFrame = stackTrace == null ? null : stackTrace.GetFrame(0);
            int errorLine = stackFrame == null ? 0 : stackFrame.GetFileLineNumber();
            var errorFile = stackFrame == null ? "" : stackFrame.GetFileName();
            var errorMethod = stackFrame == null ? "" : stackFrame.GetMethod().ToString();
            var errorExceptionType = e.GetType().FullName; string emailData = "Message: " + e.Message;
            if (e.InnerException != null) emailData += " " + e.InnerException.Message;
            if (e.InnerException != null && e.InnerException.InnerException != null) emailData += " " + e.InnerException.InnerException.Message;
            if (context != null) {
                if (context.Request != null && context.Request.Url != null) emailData += "\n\nRequest: " + context.Request.Url.ToString();
                if (context.Request != null && context.Request.UrlReferrer != null) emailData += "\n\nReferrer: " + context.Request.UrlReferrer.ToString();
                if (context.Request != null && context.Request.UserAgent != null) emailData += "\n\nUserAgent: " + context.Request.UserAgent.ToString();
                if (context.Request != null && context.Request.Form != null) emailData += "\n\nPostData: " + context.Request.Form.ToString();
                if (context.Request != null && context.Request.Headers != null) emailData += "\n\nHeaders: " + context.Request.Headers.ToString();
                if (context.Request != null && context.Request.Headers != null) emailData += "\n\nRequest TotalBytes: " + context.Request.TotalBytes;
            }
            if (handler != null) {
                try { if (handler.AuthToken != null) emailData += "\n\nAuthToken: " + handler.AuthToken.Hash; } catch { }
                try { if (handler.User != null) emailData += "\n\nUser: " + handler.User.Username; } catch { }
               // if (handler.ProcessTimer != null) emailData += "\n\nProcess Time: " + handler.ProcessTimer.ElapsedMilliseconds.ToString();
            }
            emailData += "\n\nTimestamp: UTC " + DateTime.UtcNow.ToLongDateString() + " " + DateTime.UtcNow.ToLongTimeString();
            emailData += "\n\nIP: " + Util.HTTP.GetRemoteIP(context);
            emailData += "\n\nException: " + errorExceptionType;
            emailData += "\n\nMethod: " + errorMethod;
            emailData += "\n\nFile: " + errorFile + ":" + errorLine;
            if (handler != null) emailData += "\n\nHandler: " + handler.GetType().FullName;
            emailData += "\n\nTrace 1: " + e.StackTrace;
            if (e.InnerException != null) emailData += "\n\nTrace 2: " + e.InnerException.StackTrace;
            if (e.InnerException != null && e.InnerException.InnerException != null) emailData += "\n\nTrace 3: " + e.InnerException.InnerException.StackTrace;

            // Strip auth token data for security reasons...
            if (handler != null && handler.AuthToken != null) {
                try {
                    emailData = emailData.Replace(handler.AuthToken.Hash, handler.AuthToken.Hash.Substring(0, 8) + "...");
                } catch { }
            }

            return emailData;
        }
    }
}
