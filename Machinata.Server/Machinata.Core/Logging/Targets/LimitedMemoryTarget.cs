﻿using NLog;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.Targets {

    /// <summary>
    /// Custom NLOG target for limited memory
    /// Original from https://stackoverflow.com/questions/19916795/nlog-memorytarget-maximum-size
    /// </summary>
    /// <seealso cref="NLog.Targets.TargetWithLayout" />
    [Target("LimitedMemory")]
    public sealed class LimitedMemoryTarget : TargetWithLayout
    {
        private Queue<string> logs = new Queue<string>();

        public LimitedMemoryTarget() {
            this.Limit = 1000;
        }

        public List<string> Logs { 
          get { lock (logs) { return logs.ToList(); } }
        }
        
        public int Limit { get; set; }

        protected override void Write(LogEventInfo logEvent) {
            string msg = this.Layout.Render(logEvent);
            lock (logs) {
                logs.Enqueue(msg);
                if (logs.Count > Limit) {
                    logs.Dequeue();
                }
            }
        }
    }
}
