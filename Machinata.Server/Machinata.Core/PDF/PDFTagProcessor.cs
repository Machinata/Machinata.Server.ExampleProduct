using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.PDF {

    /// <summary>
    /// See https://rupertmaier.wordpress.com/2014/08/22/creating-a-pdf-with-an-image-in-itextsharp/
    /// </summary>
    /// <seealso cref="iTextSharp.tool.xml.html.Image" />
    public class PDFTagProcessorImage : iTextSharp.tool.xml.html.Image
{
    public override IList<IElement> End(IWorkerContext ctx, Tag tag, IList<IElement> currentContent)
    {
        var src = String.Empty;
 
        if (!tag.Attributes.TryGetValue(HTML.Attribute.SRC, out src))
            return new List<IElement>(1);
 
        if (String.IsNullOrWhiteSpace(src))
            return new List<IElement>(1);

        /*if (src.StartsWith("data:imagestream/", StringComparison.InvariantCultureIgnoreCase))
        {
            var name = src.Substring(src.IndexOf("/", StringComparison.InvariantCultureIgnoreCase) + 1);

            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
            {
                return CreateElementList(ctx, tag, Image.GetInstance(stream));
            }
        }*/

        if(src.StartsWith("/")) {
            // Rewrite the src url with a cached file url
            var publicURL = Core.Config.PublicURL;
                // Disable https on test env (will fail since not valid SSL)
                // if (Core.Config.IsTestEnvironment) publicURL = publicURL.Replace("https://", "http://");
                try {
                    var newSrc = Core.Util.HTTP.GetCachedDownload(publicURL + src);
                    tag.Attributes[HTML.Attribute.SRC] = newSrc;
                }catch(Exception e) {
                    throw new Exceptions.BackendException("pdf-image-download-error", "Could not download an image to generate PDF: " + src,e);
                }
        }
        
 
        return base.End(ctx, tag, currentContent);
    }
 
    protected IList<IElement> CreateElementList(IWorkerContext ctx, Tag tag, iTextSharp.text.Image image)
    {
        var htmlPipelineContext = GetHtmlPipelineContext(ctx);
        var result = new List<IElement>();
        var element = GetCssAppliers().Apply(new Chunk((iTextSharp.text.Image) GetCssAppliers().Apply(image, tag, htmlPipelineContext), 0, 0, true), tag, htmlPipelineContext);
        result.Add(element);
 
        return result;
    }
}
}
