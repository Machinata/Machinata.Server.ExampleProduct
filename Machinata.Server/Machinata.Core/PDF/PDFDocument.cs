using HtmlAgilityPack;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Machinata.Core.PDF {

    /// <summary>
    /// Provides a common logic for generating PDF documents using HTML, including
    /// features for headers and footers as well as page margins and sizes.
    /// Also provides interfaces for loading fonts for use within the PDF and
    /// for automatically binding images.
    /// </summary>
    public class PDFDocument {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        private const bool USE_PIPELINES = true;
        
        public double MarginLeft = 0;
        public double MarginRight = 0;
        public double MarginTop = 0;
        public double MarginBottom = 0;
        public double PageWidth = 210; // A4
        public double PageHeight = 297; // A4
        
        public Font DefaultFont;

        /// <summary>
        /// If greater than 1, the PDF document is automatically duplicated N (Copies) times.
        /// </summary>
        public int Copies = 1;

        public PDFDocument() {
            
        }

     
        public string CSS { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the content. Typically this a full HTML page.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public string Content { get; set; } = string.Empty;


        /// <summary>
        /// Gets or sets the cover page (appended to begging of document). Typically this a full HTML page.
        /// </summary>
        /// <value>
        /// The cover page.
        /// </value>
        public string CoverPage { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the trailer page (appended to end of document). Typically this a full HTML page.
        /// </summary>
        /// <value>
        /// The trailer page.
        /// </value>
        public string TrailerPage { get; set; } = string.Empty;
        
        public string HeaderLeft = null;
        public string HeaderCenter = null;
        public string HeaderRight = null;

        public string FooterLeft = null;
        public string FooterCenter = null;
        public string FooterRight = null;
        
        public string HeaderLeftFirstPage = null;
        public string HeaderCenterFirstPage = null;
        public string HeaderRightFirstPage = null;

        public string FooterLeftFirstPage = null;
        public string FooterCenterFirstPage = null;
        public string FooterRightFirstPage = null;

        public string TemplateGuideImage = null;

        public int NumPages = 0;

        public bool DebugOutput = false;

        public byte[] GetContent() {
            return _compilePDF();
        }

        private Document _createDocument() {
            return new Document(
                    new Rectangle(Utilities.MillimetersToPoints((float)this.PageWidth), Utilities.MillimetersToPoints((float)this.PageHeight)),
                    Utilities.MillimetersToPoints((float)this.MarginLeft),
                    Utilities.MillimetersToPoints((float)this.MarginRight),
                    Utilities.MillimetersToPoints((float)this.MarginTop),
                    Utilities.MillimetersToPoints((float)this.MarginBottom));
        }
        
        private static object _systemFontsLock = new object();
        private static bool _systemFontsLoaded = false;
        private void _loadPDFFonts() {
            lock (_systemFontsLock) {
                if (!_systemFontsLoaded) {
                    var fonts = Core.Util.Fonts.GetAvailableFonts();
                    foreach (var file in fonts.Keys) {
                        var alias = fonts[file];
                         _logger.Trace($"Loading font {file} with alias {alias}...");
                        // Register a single font
                        // See https://stackoverflow.com/questions/34412993/itextsharp-html-to-pdf-conversion-unable-to-change-font
                        FontFactory.Register(file, alias);
                    }
                    _systemFontsLoaded = true;
                }
            }
            // Set base font
            this.DefaultFont = GetFont(Core.Config.PDFBaseFont);
        }

        public Font GetFont(string fontName) {
            return FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }

        public int GetAlignmentFromString(iTextSharp.text.Document document, string str) {
            if (str == "baseline") return Element.ALIGN_BASELINE;
            if (str == "bottom") return Element.ALIGN_BOTTOM;
            if (str == "center") return Element.ALIGN_CENTER;
            if (str == "justified") return Element.ALIGN_JUSTIFIED;
            if (str == "left") return Element.ALIGN_LEFT;
            if (str == "middle") return Element.ALIGN_MIDDLE;
            if (str == "right") return Element.ALIGN_RIGHT;
            if (str == "top") return Element.ALIGN_TOP;
            return 0;
        }

        public Rectangle GetCoordinatesFromString(iTextSharp.text.Document document, string str, float width, float height) {
            float llx = 0;
            float lly = 0;
            float urx = width;
            float ury = height;
            if(str.StartsWith("top")) {
                if (height == 0) height = document.TopMargin;
                llx = 0;
                lly = document.PageSize.Height - height;
                urx = width;
                ury = document.PageSize.Height;
            } else if(str.StartsWith("bottom-")) {
                if (height == 0) height = document.BottomMargin;
                llx = 0;
                lly = 0;
                urx = width;
                ury = height;
            }
            return new Rectangle(llx, lly, urx, ury);
        }
        

        private static object _compilePDFLock = new object();
        private byte[] _compilePDF() {

            byte[] bytes;
            
            // Load all fonts
            _loadPDFFonts();

            // Get the final html to render
            string html = this.Content;
            html = _stripUnsupportedHtmlElements(this.Content);
            //html = Core.CSS.InlineCSSForHTMLDocument(html, this.CSS, null, true, true);

            // Add the first page header pusher
            if (!string.IsNullOrEmpty(this.HeaderLeft) || !string.IsNullOrEmpty(this.HeaderRight) || !string.IsNullOrEmpty(this.HeaderCenter)) {
                if (Core.Config.PDFFirstPageHeaderPusher > 0) html = html.Replace("<body>", "<body><div style='height:" + Core.Config.PDFFirstPageHeaderPusher + "px;'></div>");
            }

            HtmlPipelineContext htmlContext = null;
            ICSSResolver cssResolver = null;
            if (USE_PIPELINES) {
                // CSS
                cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);
                cssResolver.AddCss(this.CSS, true);

                // CSS Appliers (fonts)
                XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider();
                var fonts = Core.Util.Fonts.GetAvailableFonts();
                foreach (var file in fonts.Keys) {
                    var alias = fonts[file];
                    fontProvider.Register(file,alias);
                }
                CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
                
                // HTML
                htmlContext = new HtmlPipelineContext(cssAppliers);

                // Set the image provider
                //htmlContext.SetImageProvider(new PDFImageProvider(this));

                // Set the tag factory (add our own custom tags)
                var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                tagProcessors.RemoveProcessor(HTML.Tag.IMG);
                tagProcessors.AddProcessor(HTML.Tag.IMG, new PDF.PDFTagProcessorImage());
                htmlContext.SetTagFactory(tagProcessors);

                // Set the link provider
                //htmlContext.setLinkProvider();
            }

            // Do first run to get total pages
            _runPipeline(html, htmlContext, cssResolver, new PDFCounterPageEventsHandler(this));

            // Do the final run
            bytes = _runPipeline(html, htmlContext, cssResolver, new PDFStaticPageEventsHandler(this));

            // Cover page?
            int numberOfCoverPages = 0;
            if(!string.IsNullOrEmpty(this.CoverPage)){
                // Run a pipeline just for the cover content
                string coverHTML = this.Content;
                coverHTML = _stripUnsupportedHtmlElements(this.CoverPage);
                var coverBytes = _runPipeline(coverHTML, htmlContext, cssResolver, new PDFStaticPageEventsHandler(this));
                
                // Init new document
                var ms = new MemoryStream();
                Document document = _createDocument();
                PdfCopy copy = new PdfSmartCopy(document, ms);
                document.Open();
                // Add cover page
                {
                    var reader = new PdfReader(coverBytes);
                    numberOfCoverPages = reader.NumberOfPages;
                    for (int i = 1; i <= reader.NumberOfPages; i++) {
                        copy.AddPage(copy.GetImportedPage(reader, i));
                        copy.FreeReader(reader);
                    }
                    reader.Close();
                }
                // Add each original page
                {
                    var reader = new PdfReader(bytes);
                    for (int i = 1; i <= reader.NumberOfPages; i++) {
                        copy.AddPage(copy.GetImportedPage(reader, i));
                        copy.FreeReader(reader);
                    }
                    reader.Close();
                }
                // Cleanup and swap out return bytes
                document.Close();
                bytes = ms.ToArray();
            }

            // Create duplicates?
            if(this.Copies > 1) {
                // Init new document
                var ms = new MemoryStream();
                Document document = _createDocument();
                PdfCopy copy = new PdfSmartCopy(document, ms);
                document.Open();
                // Add each duplicate (copy)
                var reader = new PdfReader(bytes);
                // Single cover page?
                for (int i = 1; i <= numberOfCoverPages; i++) { 
                    copy.AddPage(copy.GetImportedPage(reader, i));
                    copy.FreeReader(reader);
                }
                // Add each page, n times
                for (int n = 0; n < this.Copies; n++) { 
                    for (int i = 1+numberOfCoverPages; i <= reader.NumberOfPages; i++) { // This is offset by the cover pages count (a cover page could in theory be two pages)
                        copy.AddPage(copy.GetImportedPage(reader, i));
                        copy.FreeReader(reader);
                    }
                }
                // Cleanup and swap out return bytes
                reader.Close();
                document.Close();
                bytes = ms.ToArray();
            }

            // Cover page?
            if(!string.IsNullOrEmpty(this.TrailerPage)){
                // Run a pipeline just for the cover content
                string trailerHTML = this.Content;
                trailerHTML = _stripUnsupportedHtmlElements(this.TrailerPage);
                var trailerBytes = _runPipeline(trailerHTML, htmlContext, cssResolver, new PDFStaticPageEventsHandler(this));
                
                // Init new document
                var ms = new MemoryStream();
                Document document = _createDocument();
                PdfCopy copy = new PdfSmartCopy(document, ms);
                document.Open();
                
                // Add each original page
                {
                    var reader = new PdfReader(bytes);
                    for (int i = 1; i <= reader.NumberOfPages; i++) {
                        copy.AddPage(copy.GetImportedPage(reader, i));
                        copy.FreeReader(reader);
                    }
                    reader.Close();
                }
                // Add trailer page
                {
                    var reader = new PdfReader(trailerBytes);
                    numberOfCoverPages = reader.NumberOfPages;
                    for (int i = 1; i <= reader.NumberOfPages; i++) {
                        copy.AddPage(copy.GetImportedPage(reader, i));
                        copy.FreeReader(reader);
                    }
                    reader.Close();
                }
                // Cleanup and swap out return bytes
                document.Close();
                bytes = ms.ToArray();
            }
            
            

            return bytes;
        }

        private static object _runPipelineLock = new object();
        private byte[] _runPipeline(string html, HtmlPipelineContext htmlContext, ICSSResolver cssResolver, IPdfPageEvent pageEventsHandler) {
            
            // Init document
            var ms = new MemoryStream();
            var document = _createDocument();
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            writer.PageEvent = pageEventsHandler;
            document.Open();

            if (USE_PIPELINES) {
                // Pipelines
                PdfWriterPipeline pdfPipeline = new PdfWriterPipeline(document, writer);
                HtmlPipeline htmlPipleline = new HtmlPipeline(htmlContext, pdfPipeline);
                CssResolverPipeline cssPipeline = new CssResolverPipeline(cssResolver, htmlPipleline);
                // XML Worker
                XMLWorker worker = new XMLWorker(cssPipeline, true);
                XMLParser parser = new XMLParser(worker);
                //parser.AddListener(worker);
                using (TextReader sr = new StringReader(html)) {
                    parser.Parse(sr);
                }
            } else {
                //Get a stream of our HTML
                using (var msHTML = new MemoryStream(Encoding.UTF8.GetBytes(html))) {
                    //Get a stream of our CSS
                    using (var msCSS = new MemoryStream(Encoding.UTF8.GetBytes(this.CSS))) {
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msHTML, msCSS, Encoding.UTF8, FontFactory.FontImp);
                    }
                }
                
            }

            // Close and return
            document.Close();
            
            return ms.ToArray();
        }
        

        private static string _stripUnsupportedHtmlElements(string html) {
            
            // Load the document
            var htmlDocument = new HtmlDocument {
                OptionWriteEmptyNodes = true,
                OptionFixNestedTags = true,
                OptionAutoCloseOnEnd =true,
                OptionOutputAsXml = true,
                OptionCheckSyntax = true
            };
            htmlDocument.LoadHtml(html);

            // Remove unsupporte nodes (used for testing)
            htmlDocument.DocumentNode.Descendants().Where(n => n.Name == "script" || n.Name == "style" || n.Name == "link" || n.Name == "meta" || n.Id == "navigation")
                .ToList()
                .ForEach(n => n.Remove());

            // Automatically make some nodes uppercase text
            if (Core.Config.GetBoolSetting("PDFSupportUppercaseTag")) {
                htmlDocument.DocumentNode.Descendants().Where(n => n.Attributes.Any(a => a.Name == "uppercase" && a.Value == "true"))
                    .ToList()
                    .ForEach(n => 
                        n.InnerHtml = n.InnerText
                            .ToUpper()
                            .Replace("&AMP;","&amp;")
                            .Replace("&GT;","&gt;")
                            .Replace("&LT;","&lt;")
                    );
            }

            // Automatically line-break \n texts...
            if (Core.Config.GetBoolSetting("PDFSupportConvertNewLinesTag")) {
                htmlDocument.DocumentNode.Descendants().Where(n => n.Attributes.Any(a => a.Name == "convert-newlines" && a.Value == "true"))
                    .ToList()
                    .ForEach(n => n.InnerHtml = n.InnerText.Trim().Replace("\n","[line-break]").Replace("\r","")); // Note: inserting <br/>'s here breaks because they for somereason get converted to <br> on .InnerHtml
            }

            // Convert document to string
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new StringWriter(stringBuilder)) {
                htmlDocument.Save(stringWriter);
                stringWriter.Flush();
            }
            var ret = stringBuilder.ToString();

            // Strip some oddities
            var st = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><span><!--ctype ht -->";
            var se = "</span>";
            if (ret.StartsWith(st)) ret = ret.Remove(0, st.Length);
            if (ret.EndsWith(se)) ret = ret.Remove(ret.Length - se.Length);

            // Insert line breaks
            ret = ret.Replace("[line-break]", "<br/>");

            return ret;
        }
        



    }

}

