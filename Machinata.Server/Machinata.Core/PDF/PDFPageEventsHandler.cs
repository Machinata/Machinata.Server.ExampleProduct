using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.PDF {

    /// <summary>
    /// Abstract class for providing a header/footer handler with context to our internal document.
    /// </summary>
    /// <seealso cref="iTextSharp.text.pdf.PdfPageEventHelper" />
    public abstract class PDFPageEventsHandler : PdfPageEventHelper {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public PDFDocument Document { get; set; }
        public PDFPageEventsHandler(PDFDocument doc) {
            this.Document = doc;
        }

    }

    /// <summary>
    /// Dummy implementation which just counts pages.
    /// This is used to figure out how many pages we have - doing the pdf generation
    /// using two steps is much easier and robust than using templates to write in
    /// the total page count later.
    /// It also allows for altering page header footer based on first/last page.
    /// </summary>
    /// <seealso cref="Machinata.Core.PDF.PDFPageEventsHandler" />
    public class PDFCounterPageEventsHandler : PDFPageEventsHandler {

        public PDFCounterPageEventsHandler(PDFDocument doc) : base(doc) { }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document) {
            // Keep track of page number
            this.Document.NumPages = writer.PageNumber;
            
        }
    }

    /// <summary>
    /// A static (ie pdf elements) header/footer implementation in which
    /// each corner of the document can be added with header/footer information
    /// using a fixed configuration file.
    /// </summary>
    /// <seealso cref="Machinata.Core.PDF.PDFPageEventsHandler" />
    public class PDFStaticPageEventsHandler : PDFPageEventsHandler {
        

        public PDFStaticPageEventsHandler(PDFDocument doc) : base(doc) {

        }
        
        public override void OnOpenDocument(PdfWriter writer, Document document) {
           
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document) {
            // See http://developers.itextpdf.com/question/how-add-html-headers-and-footers-page
            
            // Header
            _writeHeaderFooter(Core.Config.PDFHeader, Core.Config.PDFHeaderLeft, Core.Config.PDFHeaderLeftFirstPage, this.Document.HeaderLeft, "top-left", writer, document);
            _writeHeaderFooter(Core.Config.PDFHeader, Core.Config.PDFHeaderCenter, Core.Config.PDFHeaderCenterFirstPage, this.Document.HeaderCenter, "top-center", writer, document);
            _writeHeaderFooter(Core.Config.PDFHeader, Core.Config.PDFHeaderRight, Core.Config.PDFHeaderRightFirstPage, this.Document.HeaderRight, "top-right", writer, document);

            // Footer
            _writeHeaderFooter(Core.Config.PDFFooter, Core.Config.PDFFooterLeft, Core.Config.PDFFooterLeftFirstPage, this.Document.FooterLeft, "bottom-left", writer, document);
            _writeHeaderFooter(Core.Config.PDFFooter, Core.Config.PDFFooterCenter, Core.Config.PDFFooterCenterFirstPage, this.Document.FooterCenter, "bottom-center", writer, document);
            _writeHeaderFooter(Core.Config.PDFFooter, Core.Config.PDFFooterRight, Core.Config.PDFFooterRightFirstPage, this.Document.FooterRight, "bottom-right", writer, document);
            
            // Templage guide?
            if(this.Document.TemplateGuideImage != null) {
                string imageFilePath = Core.Config.StaticPath + Core.Config.PathSep + this.Document.TemplateGuideImage.Replace("/",Core.Config.PathSep);
                var img = iTextSharp.text.Image.GetInstance(imageFilePath);
                img.ScaleAbsolute(
                    Utilities.MillimetersToPoints((float)this.Document.PageWidth),
                    Utilities.MillimetersToPoints((float)this.Document.PageHeight)
                );
                img.SetAbsolutePosition(0, 0);
                writer.DirectContentUnder.AddImage(img);
            }
        }

        private void _writeHeaderFooter(string baseSettingsStr, string specificSettingsStr, string firstpageSettingsStr, string textOrImage, string defaultPosition, iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document) {
            if (string.IsNullOrEmpty(textOrImage)) return;

            // Load settings
            var settings = Core.Util.Dictionary.ExtendBaseDictionaryWithSpecificDictionary(
                Core.Util.String.GetKeyValuesFromString(baseSettingsStr),
                Core.Util.String.GetKeyValuesFromString(specificSettingsStr)
            );
            if(writer.PageNumber == 1) {
                settings = Core.Util.Dictionary.ExtendBaseDictionaryWithSpecificDictionary(
                    settings,
                    Core.Util.String.GetKeyValuesFromString(firstpageSettingsStr)
                );
            }

            // Text or Image?
            bool isImage = false;
            if (textOrImage.EndsWith(".eps") || textOrImage.EndsWith(".wmf")) {
                isImage = true;
            }

            // Font
            var font = this.Document.DefaultFont;
            if(settings.ContainsKey("font-name")) {
                font = this.Document.GetFont(settings["font-name"]);
            }
            float fontSize = 12;
            if(settings.ContainsKey("font-size")) {
                fontSize = float.Parse(settings["font-size"]);
            }
            font.Size = fontSize;

            // W/H
            float width = document.PageSize.Width;
            if(settings.ContainsKey("width")) {
                width = Utilities.MillimetersToPoints(float.Parse(settings["width"]));
            }
            float height = 0;
            if(settings.ContainsKey("height")) {
                height = Utilities.MillimetersToPoints(float.Parse(settings["height"]));
            }
                
            // Bounds
            var position = defaultPosition;
            if(settings.ContainsKey("position")) {
                position = settings["position"];
            }
            var rect = this.Document.GetCoordinatesFromString(document, position, width, height);
                
            // Vertical align
            int verticalAlign = Element.ALIGN_MIDDLE;
            if (position.StartsWith("top")) verticalAlign = Element.ALIGN_TOP;
            if (position.StartsWith("bottom")) verticalAlign = Element.ALIGN_BOTTOM;
            if(settings.ContainsKey("vertical-align")) {
                verticalAlign = this.Document.GetAlignmentFromString(document, settings["vertical-align"]);
            }

            // Horizontal align
            int horizontalAlign = Element.ALIGN_CENTER;
            if (position.EndsWith("left")) horizontalAlign = Element.ALIGN_LEFT;
            if (position.EndsWith("right")) horizontalAlign = Element.ALIGN_RIGHT;
            if(settings.ContainsKey("horizontal-align")) {
                horizontalAlign = this.Document.GetAlignmentFromString(document, settings["horizontal-align"]);
            }
            
            // Padding
            float padding = 0;
            if(settings.ContainsKey("padding")) {
                padding = Utilities.MillimetersToPoints(float.Parse(settings["padding"]));
            }
            float paddingLeft = padding;
            if(settings.ContainsKey("padding-left")) {
                paddingLeft = Utilities.MillimetersToPoints(float.Parse(settings["padding-left"]));
            }
            float paddingRight = padding;
            if(settings.ContainsKey("padding-right")) {
                paddingRight = Utilities.MillimetersToPoints(float.Parse(settings["padding-right"]));
            }
            float paddingTop = padding;
            if(settings.ContainsKey("padding-top")) {
                paddingTop = Utilities.MillimetersToPoints(float.Parse(settings["padding-top"]));
            }
            float paddingBottom = padding;
            if(settings.ContainsKey("padding-bottom")) {
                paddingBottom = Utilities.MillimetersToPoints(float.Parse(settings["padding-bottom"]));
            }

            // Replace variables
            textOrImage = textOrImage.Replace("{pdf.page-number}",writer.PageNumber.ToString());
            textOrImage = textOrImage.Replace("{pdf.page-count}",this.Document.NumPages.ToString());
            

            
            // Create table
            PdfPTable table = new PdfPTable(1);
            table.TotalWidth = rect.Width;

            // Create cell
            PdfPCell cell = null;
            if(isImage) {
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Core.Config.StaticPath + Core.Config.PathSep + "images" + Core.Config.PathSep + textOrImage);
                cell = new PdfPCell(img, true);
            } else {
                Paragraph p = new Paragraph(textOrImage, font);
                cell = new PdfPCell(p);
            }
            cell.FixedHeight = rect.Height;
            cell.Border = 0;
            if(this.Document.DebugOutput) cell.Border = 5;
            if(this.Document.DebugOutput) cell.BackgroundColor = BaseColor.PINK;
            cell.PaddingLeft = paddingLeft;
            cell.PaddingRight = paddingRight;
            cell.PaddingTop = paddingTop;
            cell.PaddingBottom = paddingBottom;
            cell.HorizontalAlignment = horizontalAlign;
            cell.VerticalAlignment = verticalAlign;
            table.AddCell(cell);

            // Write out
            table.WriteSelectedRows(0, -1, rect.Left, rect.Top, writer.DirectContent);
     
            
        }
        
        
        
    }

    /// <summary>
    /// A CSS header/footer implementation.
    /// WIP - does not work properly.
    /// </summary>
    /// <seealso cref="Machinata.Core.PDF.PDFPageEventsHandler" />
    public class PDFCSSPageEventsHandler : PDFPageEventsHandler {
        
        private string _preparsedHeader = null;
        private string _preparsedFooter = null;

        public PDFCSSPageEventsHandler(PDFDocument doc) : base(doc) {
            _preparsedHeader = Core.CSS.InlineCSSForHTMLSnippet(this.Document.HeaderCenter,this.Document.CSS);
            _preparsedFooter = Core.CSS.InlineCSSForHTMLSnippet(this.Document.FooterCenter,this.Document.CSS);
        }
        
        public override void OnOpenDocument(PdfWriter writer, Document document) {
            
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document) {
            throw new NotImplementedException("This somehow doesnt work...");

            // See https://stackoverflow.com/questions/34410350/itextsharp-pdf-header-with-html-string-c-sharp
            // See http://developers.itextpdf.com/question/how-add-html-headers-and-footers-page
            base.OnEndPage(writer, document);
            

            ColumnText ct = new ColumnText(writer.DirectContent);
            XMLWorkerHelper.GetInstance().ParseXHtml(new ColumnTextElementHandler(ct), new StringReader(this.Document.HeaderCenter));
            ct.SetSimpleColumn(document.Left, document.Top, document.Right, document.GetTop(-20), 10, Element.ALIGN_MIDDLE);
            ct.Go();

        }
        
        
    }

    public class ColumnTextElementHandler : IElementHandler {
        
        ColumnText ct = null;

        public ColumnTextElementHandler(ColumnText ct) {
            this.ct = ct;
        }

        public void Add(IWritable w) {
            if (w is WritableElement) {
                foreach (IElement e in ((WritableElement)w).Elements()) {
                    ct.AddElement(e);
                }
            }
        }
    }
}
