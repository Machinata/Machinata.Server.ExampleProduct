using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.PDF {

    public class PDFImageProvider : AbstractImageProvider
    {
        public PDFDocument Document { get; set; }

        public PDFImageProvider(PDFDocument doc) {
            this.Document = doc;
            
        }

        public override string GetImageRootPath() {
            return Core.Config.StaticPath;
        }

        public override iTextSharp.text.Image Retrieve(string src) {
            return base.Retrieve(src);
        }

        public override void Store(string src, iTextSharp.text.Image img) {
            base.Store(src, img);
        }


    }
}
