using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {
    //MigrateDatabaseToLatestVersion
    //DropCreateDatabaseAlways
    //CreateDatabaseIfNotExists
    /// <summary>
    /// Implementation of IDatabaseInitializer for Machinata.
    /// If the database does not exist, it is automatically created.
    /// Note: this class no longer validates the database for model. Use ModelMigration instead.
    /// </summary>
    /// <seealso cref="System.Data.Entity.CreateDatabaseIfNotExists{Machinata.Core.Model.ModelContext}" />
    public class ModelSeed : CreateDatabaseIfNotExists<ModelContext> {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public const string SEED_DB_STATUS_ROUTE = "/admin/db-status";

        private string _safelyGetHTTPQueryParam(string key) {
            /*if (System.Web.HttpContext.Current != null 
                && System.Web.HttpContext.Current.Request != null
                && System.Web.HttpContext.Current.Request.QueryString  != null) {
                return System.Web.HttpContext.Current.Request.QueryString[key];
            } else return null;*/
            try {
                return System.Web.HttpContext.Current.Request.QueryString[key];
            } catch(Exception e) {
                _logger.Warn(e,$"No System.Web.HttpContext.Current.Request.QueryString for key {key}:");
                return null;
            }
        }

        public override void InitializeDatabase(ModelContext context) {
            _logger.Trace("InitializeDatabase");
            
            // Don't do any initialization routines if not enabled
            if (context.EnableModelValidationOnStartup == false) return;
            
            // Init
            bool dbExists = context.Database.Exists();
            if (!dbExists) {
                _logger.Warn($"Database {context.Database.Connection.Database} does not exist!");
                
                // Special db migration actions...
                // Auto-drop?
                if( 
                    dbExists == false && 
                    _safelyGetHTTPQueryParam("machinata-db-migration-autodrop") == "true") {
                    // Delete
                    if(dbExists) context.Database.Delete();
                    // Mark as not exists so it is re-created
                    dbExists = false;
                    {
                        // Create, seed, save!
                        context.Database.Create();
                        Seed(context);
                        context.SaveChanges();
                        // Specific datasets?
                        if(System.Web.HttpContext.Current.Request.QueryString["machinata-db-migration-datasets"] != null) {
                            var datasets = System.Web.HttpContext.Current.Request.QueryString["machinata-db-migration-datasets"];
                            foreach(var dataset in datasets.Split(',')) {
                                ProjectSeed(context, dataset);
                            }
                        }
                    }
                    // Redirect
                    System.Web.HttpContext.Current.Response.Redirect("/admin/db-status",false);
                    System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //System.Web.HttpContext.Current.Response.Flush();
                    //System.Web.HttpContext.Current.Response.End();
                    return;
                }
                
                _logger.Warn("Model is NOT compatable or does not exist!");

                // Something changed!
                string errorType = dbExists ? "is no longer compatible with the current Machinata Version" : " does not exist";
                string msg = $"The database backing the model {context.GetType().Name} on {context.Database.Connection.Database}@{Core.Config.DatabaseHost} {errorType}.";
                StringBuilder html = new StringBuilder();
                
                // Build html for automatic drop
                html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}?machinata-db-migration-autodrop=true'>Re-Create Database {context.Database.Connection.Database}@{Core.Config.DatabaseHost} with Required Data</a></p>");
                html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}?machinata-db-migration-autodrop=true&machinata-db-migration-datasets={Core.Model.SeedDatasets.DATASET_DUMMY}'>Re-Create Database {context.Database.Connection.Database}@{Core.Config.DatabaseHost} with Required and Dummy Data</a></p>");
                html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}s?machinata-db-migration-autodrop=true&machinata-db-migration-datasets={Core.Model.SeedDatasets.DATASET_RANDOM}'>Re-Create Database {context.Database.Connection.Database}@{Core.Config.DatabaseHost} with Required and Random Data</a></p>");
                throw new RichException(msg,html.ToString());

            } else {
                _logger.Trace($"Database exists...");
            }
            

            
            
                
        }

        #region Old Auto-Drop Code
        /*public override void InitializeDatabase(ModelContext context) {
            _logger.Trace("InitializeDatabase");
            
            // Don't do any initialization routines if not enabled
            if (context.EnableModelValidationOnStartup == false) return;
            
            // Init
            bool dbCompatible = true;
            bool dbExists = context.Database.Exists();
            if (!dbExists) {
                _logger.Warn($"Database {context.Database.Connection.Database} does not exist! (DatabaseMigrationValidateModelOnStartup = {Core.Config.DatabaseMigrationValidateModelOnStartup}, DatabaseMigrationEnableAutoDrop = {Core.Config.DatabaseMigrationEnableAutoDrop})");
                dbCompatible = false;
            } else {
                _logger.Trace($"Database exists...");
            }
            if (Core.Config.DatabaseMigrationValidateModelOnStartup == true && dbExists) {
                _logger.Trace("Validating if model is compatible...");
                bool throwIfNoMetaData = true; // this means that if the data in table __MigrationHistory is missing, an exception is thrown
                if (Core.Config.DatabaseMigrationEnableContinueWorking == true) throwIfNoMetaData = false;
                dbCompatible = context.Database.CompatibleWithModel(throwIfNoMetadata: throwIfNoMetaData);
                if(dbCompatible) {
                    _logger.Trace("Model is compatable");
                } else {
                    _logger.Warn("Model is NOT compatable!");
                }
            }

            // Special db migration actions...
            {
                // Auto-drop?
                if( 
                    dbCompatible == false && 
                    Core.Config.DatabaseMigrationEnableAutoDrop == true && 
                    _safelyGetHTTPQueryParam("machinata-db-migration-autodrop") == "true") {
                    // Delete
                    if(dbExists) context.Database.Delete();
                    // Mark as not exists so it is re-created
                    dbExists = false;
                    {
                        // Create, seed, save!
                        context.Database.Create();
                        Seed(context);
                        context.SaveChanges();
                        // Specific datasets?
                        if(System.Web.HttpContext.Current.Request.QueryString["machinata-db-migration-datasets"] != null) {
                            var datasets = System.Web.HttpContext.Current.Request.QueryString["machinata-db-migration-datasets"];
                            foreach(var dataset in datasets.Split(',')) {
                                ProjectSeed(context, dataset);
                            }
                        }
                    }
                    // Redirect
                    System.Web.HttpContext.Current.Response.Redirect("/admin/db-status",false);
                    System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //System.Web.HttpContext.Current.Response.Flush();
                    //System.Web.HttpContext.Current.Response.End();
                    return;
                }

                
                // Continue working?
                if(
                    dbCompatible == false && 
                    Core.Config.DatabaseMigrationEnableContinueWorking == true && 
                    _safelyGetHTTPQueryParam("machinata-db-migration-continue-working") == "true") {
                    
                    // Remove migration history
                    context.Database.ExecuteSqlCommand("DELETE FROM `__MigrationHistory`");
                    
                    // Redirect
                    System.Web.HttpContext.Current.Response.Redirect("/admin/db-status",false);
                    System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //System.Web.HttpContext.Current.Response.Flush();
                    //System.Web.HttpContext.Current.Response.End();
                    return;
                }
            }
            
            // Check if the model changed
            if (!dbCompatible) {
                
                _logger.Warn("Model is NOT compatable or does not exist!");

                // Something changed!
                string errorType = dbExists ? "is no longer compatible with the current Machinata Version" : " does not exist";
                string msg = $"The database backing the model {context.GetType().Name} on {context.Database.Connection.Database}@{Core.Config.DatabaseHost} {errorType}.";
                StringBuilder html = new StringBuilder();
                
                // Build html for automatic drop
                if(Core.Config.DatabaseMigrationEnableAutoDrop == true) html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}?machinata-db-migration-autodrop=true'>Re-Create Database with Required Data</a></p>");
                if(Core.Config.DatabaseMigrationEnableAutoDrop == true) html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}?machinata-db-migration-autodrop=true&machinata-db-migration-datasets={Core.Model.SeedDatasets.DATASET_DUMMY}'>Re-Create Database with Required and Dummy Data</a></p>");
                if(Core.Config.DatabaseMigrationEnableAutoDrop == true) html.Append($"<p><a href='{Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE}s?machinata-db-migration-autodrop=true&machinata-db-migration-datasets={Core.Model.SeedDatasets.DATASET_RANDOM}'>Re-Create Database with Required and Random Data</a></p>");
                throw new RichException(msg,html.ToString());
            }
                
        }*/
        #endregion

        /// <summary>
        /// Registers the original create db SQL and runs all the seed routines.
        /// </summary>
        /// <param name="context">The context to seed.</param>
        protected override void Seed(ModelContext context) {
            _logger.Trace("Seed");
            base.Seed(context);

            // Add some custom stored procedures
            {
                // Helper function for ordering randomly (used by IQueryable<T>.Randomize)
                string newguidFunction = "CREATE FUNCTION `NewGuid`() RETURNS DOUBLE RETURN RAND();";
                context.Database.ExecuteSqlCommand(newguidFunction);
            }
            
            // Save the SQL used to create the table
            var sql = context.GetObjectContext().CreateDatabaseScript();
            context.Database.ExecuteSqlCommand("ALTER TABLE `__MigrationHistory` ADD COLUMN `SqlScript` LONGTEXT");
            context.Database.ExecuteSqlCommand("UPDATE `__MigrationHistory` SET `SqlScript`={0} ORDER BY `MigrationId` DESC LIMIT 1",sql);
            
            // Run the seed routines
            _runAllSeedRoutines(context, typeof(OnModelSeedAttribute), Core.Model.SeedDatasets.DATASET_REQUIRED);
            _runAllSeedRoutines(context, typeof(OnModelSeedConnectionsAttribute), Core.Model.SeedDatasets.DATASET_REQUIRED);
            _runAllSeedRoutines(context, typeof(OnProjectSeedAttribute), Core.Model.SeedDatasets.DATASET_REQUIRED);
            _runAllSeedRoutines(context, typeof(OnProjectSeedConnectionsAttribute), Core.Model.SeedDatasets.DATASET_REQUIRED);
        }
        
        public static void ProjectSeed(ModelContext context, string dataset = Core.Model.SeedDatasets.DATASET_ALL) {
            // Run the seed routines
            _runAllSeedRoutines(context, typeof(OnProjectSeedAttribute), dataset);
            _runAllSeedRoutines(context, typeof(OnProjectSeedConnectionsAttribute), dataset);
        } 

        /// <summary>
        /// Helper method to run the seed routines on all ModelClass objects.
        /// The attribute type defines which routine exactly (ModelSeed, ModelSeedConnections, ProjectSeed,...)
        /// to run.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="dataset">The dataset.</param>
        private static void _runAllSeedRoutines(ModelContext context, Type attributeType, string dataset) {
            var assemblies = Core.Reflection.Assemblies.GetMachinataAssemblies();
            var seedType = attributeType.Name.Replace("Attribute", "").Replace("On","");
            foreach (var assembly in assemblies) {
                foreach (var type in  assembly.GetTypes()) {
                    var onModelCreatingMethods = type
                        .GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                        .Where(t => t.GetCustomAttributes(attributeType, inherit: false).Any());
                    foreach(var mi in onModelCreatingMethods) {
                        // Execute the seed
                        _logger.Trace($"Running seed routine {seedType} for '{type.Name}'...");
                        context.ChangeTracker.DetectChanges();
                        var entriesBefore = context.ChangeTracker.Entries().Count();
                        try {
                            if(dataset != null) mi.Invoke(null, new object[] { context, dataset });
                            else mi.Invoke(null, new object[] { context });
                        } catch(Exception e) {
                            _logger.Error(e);
                            throw e;
                        }
                        context.ChangeTracker.DetectChanges();
                        var entriesAfter = context.ChangeTracker.Entries().Count();
                        // Register in log
                        Core.Model.SeedLog.CreateSeedLog(context, seedType, type.Name, dataset,entriesAfter-entriesBefore);
                    }
                    
                }
            }
            
            // Save
            context.SaveChanges();
        }
    }
}
