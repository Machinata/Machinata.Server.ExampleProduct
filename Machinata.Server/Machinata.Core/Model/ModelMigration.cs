using Machinata.Core.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Machinata.Core.Handler;
using System.Data.Entity.Infrastructure;
using MySql.Data.MySqlClient;

namespace Machinata.Core.Model {



    /// <summary>
    /// WORK IN PROGRESS
    /// </summary>
    public class ModelMigration {

        public class PrioritizedSQLStatement {
            public int Priority;
            public string Statement;
            
            public const int PRIORITY_VERYHIGH = 5000;
            public const int PRIORITY_HIGH = 1000;
            public const int PRIORITY_MEDIUM = 500;
            public const int PRIORITY_LOW = 100;
        }

        public class MigrationEntity : ModelObject, IPublishedModelObject {
            
            [FormBuilder(Forms.Admin.LISTING)]
            [FormBuilder(Forms.Admin.VIEW)]
            public string Type { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            [FormBuilder(Forms.Admin.VIEW)]
            public string Name { get; set; }
            
            [FormBuilder()]
            public string SQLOnDB { get; set; }

            [FormBuilder()]
            public string SQLForModel { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            [FormBuilder(Forms.Admin.VIEW)]
            public string RecommendedAction { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            [FormBuilder(Forms.Admin.VIEW)]
            public string RecommendedSQL { get; set; }
            
            [FormBuilder()]
            public string RawSQLOnDB { get; set; }

            [FormBuilder()]
            public string RawSQLForModel { get; set; }

            public bool MarkAsLowRecommendation { get; set; }

            public bool Published {
                get {
                    if (MarkAsLowRecommendation == true) return false;
                    return RecommendedAction != null;
                }
            }

            public override AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
                return null;
            }

            public List<MigrationColumnEntity> Columns {
                get {
                    if (this.Type != "Table") return null;
                    var ret = new List<MigrationColumnEntity>();

                    // Model Columns
                    if(this.RawSQLForModel != null) {
                        var parser = new Core.SQL.SQLParser.Parser();
                        parser.Parse(this.RawSQLForModel);
                        parser.Reduce();
                        var statement = parser.Statements.SingleOrDefault();
                        foreach (var column in statement.Columns) {
                            ret.Add(new MigrationColumnEntity() {
                                Type = column.ColumnType.ToString(),
                                Name = column.ColumnName,
                                NameForModel = column.FirstName,
                                UID = column.ColumnUID,
                                SQLForModel = column.GeneratedSQL
                            });
                        }
                    }

                    // DB Columns
                    if(this.RawSQLOnDB != null) {
                        var parser = new Core.SQL.SQLParser.Parser();
                        parser.Parse(this.RawSQLOnDB);
                        parser.Reduce();
                        var statement = parser.Statements.SingleOrDefault();
                        foreach (var column in statement.Columns) {
                            // Existing?
                            var existing = ret.SingleOrDefault(c => c.UID == column.ColumnUID);
                            if(existing == null) {
                                ret.Add(new MigrationColumnEntity() {
                                    Type = column.ColumnType.ToString(),
                                    Name = column.ColumnName,
                                    NameOnDB = column.FirstName,
                                    UID = column.ColumnUID,
                                    SQLOnDB = column.GeneratedSQL
                                });
                            } else {
                                existing.SQLOnDB = column.GeneratedSQL;
                                existing.NameOnDB = column.FirstName;
                            }
                        }
                    }

                    // Merge
                    foreach(var column in ret) {
                        column.DeterminIfSame();
                    }

                    // Sort
                    ret = ret.OrderBy(c => c.UID).ToList();
                    return ret;
                }
            }
            
            
        }

        
        public class MigrationColumnEntity : ModelObject, IPublishedModelObject {
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string Type { get; set; }
            
            
            [FormBuilder()]
            [FormBuilder(Forms.Admin.LISTING)]
            public string Name { get; set; }
            
            [FormBuilder()]
            public string NameOnDB { get; set; }
            
            [FormBuilder()]
            public string NameForModel { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public bool Same { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string SQLOnDB { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string SQLForModel { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string UID { get; set; }

            public void DeterminIfSame() {
                var db = this.SQLOnDB;
                if(!string.IsNullOrEmpty(db)) db = Core.SQL.Simplify.CleanupForComparison(db);
                
                var model = this.SQLForModel;
                if(!string.IsNullOrEmpty(model)) model = Core.SQL.Simplify.CleanupForComparison(model);

                if (db == model) this.Same = true;
                else this.Same = false;
            }
            
            public bool Published {
                get {
                    return !this.Same;
                }
            }
        }


        public class DBInfoEntity : ModelObject {
            
            public DBInfoEntity(ModelContext context) {
                // From config
                this.Database = Core.Config.DatabaseDatabase;
                this.Host = Core.Config.DatabaseHost;
                
                // From connection
                this.ConnectionDatabase = context.Database.Connection.Database;
                this.ConnectionDataSource = context.Database.Connection.DataSource;
                this.ConnectionString = context.Database.Connection.ConnectionString;
                this.ConnectionTimeout = context.Database.Connection.ConnectionTimeout.ToString();
                //this.ServerVerion = context.Database.Connection.ServerVersion; // throws exception

                // Assembly version
                var mysql = System.Reflection.Assembly.GetAssembly(typeof(MySql.Data.MySqlClient.MySqlConnection));
                this.ConnectorVersion = mysql.GetName().Version.ToString();

                // Session variables
                {
                    _sessionVariables = new Dictionary<string, string>();
                    using (var con = new MySqlConnection(ModelContext.GetConnectionString())) {
                        con.Open();
                        using (var cmd = new MySqlCommand("SHOW SESSION VARIABLES;", con)) {
                            using (var reader = cmd.ExecuteReader()) {
                                while (reader.Read()) {
                                    var key = reader.GetString(0);
                                    var val = reader.GetString(1);
                                    _sessionVariables.Add(key, val);
                                }
                            }
                        }
                    }
                    this.SessionVariables = "";
                    foreach(var key in _sessionVariables.Keys) {
                        this.SessionVariables += $"{key}={_sessionVariables[key]}; \n";
                    }
                    this.SessionVariables = this.SessionVariables.Trim();
                }
                this.SQLMode = _sessionVariables["sql_mode"];
                this.SQLVersion = _sessionVariables["version"] + " " + _sessionVariables["version_comment"];
                this.SQLOS = _sessionVariables["version_compile_os"] + " " + _sessionVariables["version_compile_machine"];
            }

            private Dictionary<string, string> _sessionVariables;

            [FormBuilder(Forms.Admin.VIEW)]
            public string Database { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string Host { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string ConnectionDatabase { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string ConnectionDataSource { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string ConnectionString { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string ConnectionTimeout { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string ConnectorVersion { get; set; }
            
            //[FormBuilder(Forms.Admin.VIEW)]
            public string SessionVariables { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public string SQLMode { get; set; }

            [FormBuilder(Forms.Admin.VIEW)]
            public string SQLVersion { get; set; }

            [FormBuilder(Forms.Admin.VIEW)]
            public string SQLOS { get; set; }
            
            [FormBuilder(Forms.Admin.VIEW)]
            public DateTime LastAnalysis { get; set; }
            
        }
        
        public ModelDescription ModelOnDB;
        public ModelDescription ModelFromModel;
        public ModelDescription ModelAnalyzed;

        public DBInfoEntity DBInfo;

        private ModelContext _dbContext;
        

        public ModelMigration(ModelContext context) {
            _dbContext = context;
        }

        public void ResetAnalysis() {
            var tempDB = ModelContext.CreateTemporaryContext();
        }
        

        public List<MigrationEntity> Analyze() {
            
            // Get current desc
            ModelOnDB = _dbContext.GetDescription();

            // Create a temp database to compare to
            var tempDB = ModelContext.GetTemporaryContext();
            ModelFromModel = tempDB.GetDescription();

            // DB infos
            this.DBInfo = new DBInfoEntity(_dbContext);
            this.DBInfo.LastAnalysis = tempDB.GetDBCreationTime();
            
            var migrations = new List<MigrationEntity>();

            // What about tables that no longer exist?
            var deletedTables = this.ModelOnDB.Tables.Where(tdb => this.ModelFromModel.Tables.Count(tm => tm.Name == tdb.Name) == 0);

            var tablesToAnalyze = this.ModelFromModel.Tables.ToList();
            tablesToAnalyze.AddRange(deletedTables);
            foreach(var modelTable in tablesToAnalyze) {

                // Create analysis object
                var migration = new MigrationEntity();
                migration.Type = "Table";
                migration.Name = modelTable.Name;
                migration.SQLForModel = modelTable.ReducedCreateSQL;
                migration.RawSQLForModel = modelTable.CreateSQL;

                // Does it exist?
                var matchingTableOnDB = this.ModelOnDB.Tables.SingleOrDefault(t => t.Name == modelTable.Name);
                var matchingTableOnModel = this.ModelFromModel.Tables.SingleOrDefault(t => t.Name == modelTable.Name);
                if(matchingTableOnModel == null) {
                    // Not exist on model, probably delete
                    migration.RecommendedAction = "Delete";
                    migration.RecommendedSQL = $"DROP TABLE `{modelTable.Name}`"; 
                } else if(matchingTableOnDB == null) {
                    // Not exist on db, need to create
                    migration.RecommendedAction = "Create";
                    migration.RecommendedSQL = Core.SQL.Simplify.RemoveTableSettings(modelTable.CreateSQL); // Don't use reduced!
                } else {
                    // Table exists on db, validate if it needs to change
                    migration.SQLOnDB = matchingTableOnDB.ReducedCreateSQL;
                    migration.RawSQLOnDB = matchingTableOnDB.CreateSQL;
                    var columns = migration.Columns;
                    var notSameColumns = columns.Where(c => c.Same == false).ToList();
                    if(notSameColumns.Count > 0) {
                        // Update table
                        migration.RecommendedAction = "Update";
                        migration.RecommendedSQL = "";
                        var statements = new List<PrioritizedSQLStatement>();
                        // Get all mis-matches
                        foreach(var column in notSameColumns) {
                            // What kind of column update? add, delete, modify>
                            var originalTarget = column.Type.ToUpper();
                            var target = column.Type.ToUpper();
                            if (target == "MEMBER") target = "COLUMN";
                            if (target == "KEY") target = "KEY";
                            if (target == "PRIMARYKEY") target = "PRIMARY KEY";
                            if (target == "UNIQUEKEY") target = "INDEX";
                            if (target == "CONSTRAINT") target = "CONSTRAINT";
                            if (column.SQLForModel != null && column.SQLOnDB == null) {
                                // New column
                                var verb = "ADD";
                                if (target == "COLUMN") verb = "ADD COLUMN"; // Everything else has the verb interated in SQLForModel
                                var priority = PrioritizedSQLStatement.PRIORITY_HIGH;
                                if (verb == "ADD COLUMN") priority = PrioritizedSQLStatement.PRIORITY_VERYHIGH;
                                statements.Add(new PrioritizedSQLStatement() {
                                    Statement = $"ALTER TABLE `{modelTable.Name}` {verb} {column.SQLForModel};",
                                    Priority = priority
                                });
                            } else if (column.SQLForModel == null && column.SQLOnDB != null) {
                                // Delete column
                                if(target == "PRIMARY KEY") {
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"ALTER TABLE `{modelTable.Name}` DROP {target};",
                                        Priority = PrioritizedSQLStatement.PRIORITY_LOW
                                    });
                                } else {
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"ALTER TABLE `{modelTable.Name}` DROP {target} {column.Name};",
                                        Priority = PrioritizedSQLStatement.PRIORITY_LOW
                                    });
                                }
                            } else {
                                // Update column
                                if (target == "COLUMN") {
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"ALTER TABLE `{modelTable.Name}` MODIFY {target} {column.SQLForModel};",
                                        Priority = PrioritizedSQLStatement.PRIORITY_MEDIUM
                                    });
                                } else if (target == "KEY" || target == "PRIMARY KEY" || target == "INDEX") {
                                    if (originalTarget == "UNIQUEKEY") target = "UNIQUE KEY";
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"ALTER TABLE `{modelTable.Name}` DROP INDEX {column.Name}, ADD {column.SQLForModel};",
                                        Priority = PrioritizedSQLStatement.PRIORITY_MEDIUM
                                    });
                                } else if (target == "CONSTRAINT") {
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"ALTER TABLE `{modelTable.Name}` DROP {target} {column.NameOnDB}, ADD {column.SQLForModel};",
                                        Priority = PrioritizedSQLStatement.PRIORITY_MEDIUM
                                    });
                                } else {
                                    statements.Add(new PrioritizedSQLStatement() {
                                        Statement = $"-- Migration for {target} {column.Name} on {modelTable.Name} is not yet supported!\n",
                                        Priority = PrioritizedSQLStatement.PRIORITY_LOW
                                    });
                                }
                            }

                        }
                        bool showPriorities = false;
                        if (showPriorities) {
                            migration.RecommendedSQL = string.Join("\n", statements.OrderByDescending(e => e.Priority).Select(e => e.Statement + " -- Priority: " + e.Priority));
                        } else {
                            migration.RecommendedSQL = string.Join("\n", statements.OrderByDescending(e => e.Priority).Select(e => e.Statement));
                        }
                    }
                }

                // Add migration
                migrations.Add(migration);
            }

            // Validate delete's against a rename
            foreach(var deleteMigration in migrations.Where(m => m.RecommendedAction == "Delete").ToList()) {
                // See if a create exists with same sql
                var createMigration = migrations.Where(m => m.RecommendedAction == "Create" && m.SQLForModel.Substring(m.SQLForModel.IndexOf('(')) == deleteMigration.SQLForModel.Substring(m.SQLForModel.IndexOf('('))).SingleOrDefault();
                if(createMigration != null) {
                    deleteMigration.MarkAsLowRecommendation = true;
                    createMigration.MarkAsLowRecommendation = true;
                    var renameMigration = new MigrationEntity();
                    renameMigration.Type = deleteMigration.Type;
                    renameMigration.Name = deleteMigration.Name;
                    renameMigration.SQLOnDB = deleteMigration.SQLOnDB;
                    renameMigration.SQLForModel = deleteMigration.SQLForModel;
                    renameMigration.RawSQLOnDB = deleteMigration.RawSQLOnDB;
                    renameMigration.RawSQLForModel = deleteMigration.RawSQLForModel;
                    renameMigration.RecommendedAction = "Rename";
                    renameMigration.RecommendedSQL = $"ALTER TABLE `{deleteMigration.Name}` RENAME TO `{createMigration.Name}`";
                    migrations.Add(renameMigration);
                }
            }

            // Set id
            var id = 1;
            foreach(var migration in migrations) {
                migration.Id = id++;
            }

            return migrations;

            // Get diff
            //var differ = new DiffMatchPatch.DiffMatchPatch(0,0,0,0,0,0,0,0);
            //var diffs = differ.DiffMain(this.SQLOnDB, this.SQLForModel);
            //this.HTMLDiff = differ.DiffPrettyHtml(diffs);
            
            
        }

    }
}
