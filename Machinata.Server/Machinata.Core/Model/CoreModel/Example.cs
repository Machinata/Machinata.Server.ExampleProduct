using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;

namespace Machinata.Core.Model {
    
    [Serializable()]
    //[ModelClass] // Remove comment
    public partial class Example : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Example() {
            // Parameterless constructure is required for reflection...
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [MinLength(3)]
        [MaxLength(100)]
        [Pattern(PatternAttribute.PATTERN_USERNAME)]
        [Required]
        [Placeholder("Enter example text")]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string ExampleColumn { get; set; } 

        [Column]
        [ForeignKey("ExampleEntityId")]
        public Example ExampleEntity { get; set; } 

        [Column]
        public int? ExampleEntityId { get; set; }
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [InverseProperty("ExampleEntity")]
        public ICollection<Example> ExampleCollection { get; set; }
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    /*
    public static class ModelContextExampleExtenions {
        public static DbSet<Example> Examples(this Core.Model.ModelContext context) {
            return context.Set<Example>();
        }
    }
    */

    #endregion
}
