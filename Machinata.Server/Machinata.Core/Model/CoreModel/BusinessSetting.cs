//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Data.Entity;
//using System.Data.Entity.Core.Objects.DataClasses;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Machinata.Core.Builder;

//namespace Machinata.Core.Model {
    
//    [Serializable()]
//    [ModelClass]
//    public partial class BusinessSetting : ModelObject {
        
//        #region Class Logger
//        /// <summary>
//        /// The logger helper for this class. Use this logger instance anytime you want to
//        /// log something from within this class.
//        /// </summary>
//        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
//        #endregion
        
//        #region Constructors //////////////////////////////////////////////////////////////////////

//        public BusinessSetting() {
          
//        }

//        #endregion

//        #region Public Data Store Properties //////////////////////////////////////////////////////
        
//        [Column]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.SELECTION)]
//        public string Key { get; set; }

//        [Column]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.SELECTION)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        public string Value { get; set; }

//        #endregion

//        #region Public Navigation Properties //////////////////////////////////////////////////////        

//        [Column]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        public Business Business { get; set; }
     
//        #endregion

//        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

//        #endregion

//        #region Model Creation ////////////////////////////////////////////////////////////////////

//        [OnModelCreating]
//        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
//            //modelBuilder.Entity<Business>()
//            //   .HasRequired(b => b.AddressRef).WithOptional(a => a.BusinessRef);
               
//        }
//        [OnModelSeed]
//        private static void OnModelSeed(ModelContext context) {
             
//        }

//        #endregion

//        #region Public Methods ////////////////////////////////////////////////////////////////////

//        #endregion

//        #region Private Methods ///////////////////////////////////////////////////////////////////

//        #endregion

//    }


//    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
//    public static class ModelContextBusinessSettingsExtenions {
//        public static DbSet<BusinessSetting> BusinessSettings(this Core.Model.ModelContext context) {
//            return context.Set<BusinessSetting>();
//        }
//    }

//    #endregion
//}
