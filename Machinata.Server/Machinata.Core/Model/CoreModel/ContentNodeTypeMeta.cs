using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Machinata.Core.Handler;

namespace Machinata.Core.Model {
    
    #region Content Node Type Meta Class

    /// <summary>
    /// Class for abstracting additional logic for each ContentNode type. 
    /// A ContentNodeTypeMeta class can be implemented by any module and automatically becomes
    /// avaialable in the CMS. Each type can be extended to allow for specific serialization actions
    /// between the CMS, DB, and inserting into templates.
    /// </summary>
    public abstract class ContentNodeTypeMeta {

        /// <summary>
        /// The category, either System (Node, Translation) or Content (File, Image...)
        /// </summary>
        public enum TypeCategory {
            System,
            Content
        }

        /// <summary>
        /// Gets the type id which is stored in ContentNode.Type (string).
        /// Must be implemented by subclass.
        /// </summary>
        /// <returns></returns>
        public abstract string GetID();

        /// <summary>
        /// Gets the sort priority for building "Add to Page" type lists of all content types.
        /// Note: This has nothing to do with the ContentNode.Sort value.
        /// Default: NODE_TYPE_SORT_DEFAULT_PRIORITY
        /// </summary>
        /// <returns></returns>
        public virtual int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }

        /// <summary>
        /// Gets the icon used to display what the node type is.
        /// </summary>
        /// <returns></returns>
        public abstract string GetIcon();

        /// <summary>
        /// When true, indicates that multiple elements are allowed per page.
        /// </summary>
        /// <returns></returns>
        public virtual bool AllowMultiplePerPage() { return true; }

        /// <summary>
        /// Gets the category, either System (Node, Translation) or Content (File, Image...)
        /// Default: TypeCategory.System
        /// </summary>
        /// <returns></returns>
        public virtual TypeCategory GetCategory() { return TypeCategory.System; }

        /// <summary>
        /// Converts a raw value to database value.
        /// Can be overridden by subclass.
        /// Default: No conversion.
        /// </summary>
        /// <param name="rawValue">The raw value.</param>
        /// <returns></returns>
        public virtual string ConvertToDBValue(string rawValue) {
            return rawValue;
        }

        /// <summary>
        /// Converts a raw value to editor value.
        /// Can be overridden by subclass.
        /// Default: No conversion.
        /// </summary>
        /// <param name="rawValue">The raw value.</param>
        /// <returns></returns>
        public virtual string ConvertToEditorValue(string rawValue) {
            return rawValue;
        }

        /// <summary>
        /// Converts a raw value to template value.
        /// Can be overridden by subclass.
        /// Default: Core.Util.XML.XMLEncodeString().
        /// </summary>
        /// <param name="rawValue">The raw value.</param>
        /// <returns></returns>
        public virtual string ConvertToTemplateValue(string rawValue) {
            return Core.Util.XML.XMLEncodeString(rawValue);
        }

        public virtual void InsertAdditionalVariables(ContentNode node, Templates.PageTemplate template, string variableName) {

        }

        public string ID { get { return GetID(); } }
        public int Sort { get { return GetSort(); } }
        public TypeCategory Category { get { return GetCategory(); } }

        public string Icon { get { return GetIcon(); } }
    }

    #endregion
        
    #region Core Content Node Types

    public class ContentNodeTypeNode : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_NODE; }
        public override string GetIcon() { return "node"; }
    }

    public class ContentNodeTypeTranslation : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_TRANSLATION; }
        public override string GetIcon() { return "translation"; }
    }

    public class ContentNodeTypeImage : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_IMAGE; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "picture"; }
    }

    public class ContentNodeTypeThumbnail : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_THUMBNAIL; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "wallpaper"; } // name
    }

    public class ContentNodeTypeVideo : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_VIDEO; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "video-call"; }
    }

    public class ContentNodeTypeTitle : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_TITLE; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_HIGH_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override bool AllowMultiplePerPage() { return false; }
        public override string ConvertToDBValue(string rawValue) {
            return rawValue.Replace("\\\\","\n");
        }
        public override string ConvertToEditorValue(string rawValue) {
            return rawValue.Replace("\n","\\\\");
        }
        public override string ConvertToTemplateValue(string rawValue) {
            return rawValue.Replace("&amp;","&");
        }
        public override string GetIcon() { return "text"; }
    }

    public class ContentNodeTypeDescription : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_DESCRIPTION; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_HIGH_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override bool AllowMultiplePerPage() { return false; }
        public override string GetIcon() { return "information"; }
    }

    public class ContentNodeTypeHTML : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_HTML; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_HIGH_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string ConvertToDBValue(string rawValue) {
            var ret = Uri.UnescapeDataString(rawValue);
            //ret = ret.ReplaceSuffix("<br>", "");
            //ret = ret.ReplaceSuffix("<br/>", "");
            //ret = ret.ReplaceSuffix("<p> </p>", "");
            return ret;
        }
        public override string ConvertToEditorValue(string rawValue) {
            return Uri.EscapeDataString(rawValue);
        }
        public override string ConvertToTemplateValue(string rawValue) {
            return rawValue.Replace("&nbsp;"," ");
        }
        public override string GetIcon() { return "multiline-text"; }
    }

      

    public class ContentNodeTypeEmbed : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_EMBED; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string ConvertToDBValue(string rawValue) {
            //return Uri.UnescapeDataString(rawValue);
            return System.Web.HttpUtility.UrlDecode(rawValue);
        }
        public override string ConvertToEditorValue(string rawValue) {
            //return Uri.EscapeDataString(rawValue); does not support long strings
            return System.Web.HttpUtility.UrlPathEncode(rawValue); // Must use the path method, otherwise spaces are encoded as +, which is wrong and supported on the javascript side
        }
        public override string ConvertToTemplateValue(string rawValue) {
            // Auto embed?
            if (rawValue != null && (rawValue.StartsWith("https://") || rawValue.StartsWith("http://"))) {
                return Core.Util.EmbedCodes.GetEmbedCodeForURL(rawValue);
            } else if (rawValue != null && rawValue.StartsWith("template.")) {
                return "{" + rawValue + "}";
            } else {
                return rawValue.Replace("&nbsp;"," ");
            }
        }
        public override string GetIcon() { return "source-code"; }
    }

    public class ContentNodeTypeFile : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_FILE; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "file"; }
    }
    
        
        
    public class ContentNodeTypeLink : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_LINK; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "link"; }
    }



    public class ContentNodeTypeQuote : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_QUOTE; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "quote"; }
    }


    public class ContentNodeTypeButton : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_BUTTON; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "button"; }
    }

    public class ContentNodeTypeLabel : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_LABEL; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string ConvertToTemplateValue(string rawValue) {
            return rawValue.Replace("&amp;","&");
        }
        public override string GetIcon() { return "price-tag"; }
    }

    public class ContentNodeTypeShortURL : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_SHORTURL; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override bool AllowMultiplePerPage() { return false; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "link"; }
    }

    public class ContentNodeTypeOption : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_OPTION; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_DEFAULT_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "option"; } // or support
    }

    public class ContentNodeTypeMetaInformation : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_META; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "more-details"; }
    }

    public class ContentNodeTypeSpacer : ContentNodeTypeMeta {
        public override string GetID() { return ContentNode.NODE_TYPE_SPACER; }
        public override int GetSort() { return ContentNode.NODE_TYPE_SORT_LOW_PRIORITY; }
        public override TypeCategory GetCategory() { return TypeCategory.Content; }
        public override string GetIcon() { return "insert-white-space"; }
    }

    #endregion
    
    
}
