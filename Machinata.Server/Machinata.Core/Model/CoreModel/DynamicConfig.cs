using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Collections.Concurrent;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class DynamicConfig : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public DynamicConfig() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Index]
        [MaxLength(128)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Key { get; set; } 

        [Column]
        [DataType(DataType.MultilineText)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Value { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

        
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextDynamicConfigExtenions {
        public static DbSet<DynamicConfig> DynamicConfigs(this Core.Model.ModelContext context) {
            return context.Set<DynamicConfig>();
        }
    }

    #endregion
}
