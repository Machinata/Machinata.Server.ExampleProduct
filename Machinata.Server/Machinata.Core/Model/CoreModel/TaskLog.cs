using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Data.Entity.Infrastructure;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class TaskLog : ModelObject {
    
        public enum TaskStatuses: short {
            Created = 0,
            Waiting = 10, // The task was started e.g. by a user and is waiting to get picked up by the task manager
            Started = 20, // the task manager has started processing this task
            Paused = 25,
            ItemProcessed = 30, // After each processed item it gets logged
            ItemFailed = 31, // Processing an item caused an exception
            Finished = 40,
          
            Error = 50
        }

        public enum BatchTaskDisplayStatuses {
            Created = 0 ,
            Started = 100,
            Running = 200,
            Paused = 300,
            Finished = 400,
            Error = 1000
        }



        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public TaskLog() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string TaskType { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [MaxLength(100)]
        public string TaskId { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string ItemId { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public TaskStatuses Status { get; set; }



        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static BatchTaskDisplayStatuses GetDisplayStatusFor(Model.TaskLog.TaskStatuses status) {
            if (status == TaskLog.TaskStatuses.Created) {
                return BatchTaskDisplayStatuses.Created;
            } else if (status == TaskLog.TaskStatuses.Waiting) {
                return BatchTaskDisplayStatuses.Started;
            } else if (status == TaskLog.TaskStatuses.Started) {
                return BatchTaskDisplayStatuses.Running;
            } else if (status == TaskLog.TaskStatuses.ItemProcessed) {
                return BatchTaskDisplayStatuses.Running;
            } else if (status == TaskLog.TaskStatuses.ItemFailed) {
                return BatchTaskDisplayStatuses.Running;
            } else if (status == TaskLog.TaskStatuses.Paused) {
                return BatchTaskDisplayStatuses.Paused;
            } else if (status == TaskLog.TaskStatuses.Finished) {
                return BatchTaskDisplayStatuses.Finished;
            } else if (status == TaskLog.TaskStatuses.Error) {
                return BatchTaskDisplayStatuses.Error;
            }
            throw new NotImplementedException("There is no display status for :" + status);
        }

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
            return null;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextTaskLogExtenions {
        public static DbSet<TaskLog> TaskLogs(this Core.Model.ModelContext context) {
            return context.Set<TaskLog>();
        }
    }

    #endregion
}
