using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Reflection;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices.WindowsRuntime;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Machinata.Core.Lifecycle;
using Machinata.Core.Util;

namespace Machinata.Core.Model {
    
    //[Serializable()]
    //[ModelClass] 
    public partial class Theme : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

       
        #endregion


        #region Constants ////////////////////////////////////////////////////////////////////////

    
        #endregion


        #region Constructors //////////////////////////////////////////////////////////////////////

        public Theme() {
            this.Properties = new Properties();
            this.Variables = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Name { get; set; } 

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Title { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Inherits { get; set; }

        [Column]
        //[FormBuilder(Forms.Admin.LISTING)]
        public bool SetAsDefault { get; set; } 
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Properties Properties { get; set; }


        [Column]
        public Properties Variables { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnProjectSeed]
        private static void OnProjectSeed(ModelContext context, string dataset) {
          
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        public static Theme GetThemeByName(string name) {
            // Cached?
            var cacheName = GetCachename(name);
            Theme cachedTheme = null;
            if(Core.Config.ThemeCacheEnabled) cachedTheme = GetCachedThemeByName(cacheName);
            if (cachedTheme != null) {
                return cachedTheme;
            }

            // Not cached -> load the theme from json
            Theme ret = null;

            // Load via json file
            string filePath = Core.Util.Files.GetHotSwappableFile(GetFilePath(name),null);
            ret = LoadThemeFromJSON(filePath);

            // Final exception
            if (ret == null) {
                _logger.Warn($"Theme {name} not found using default fallbacktheme");
                throw new BackendException("theme-not-found", $"The theme '{name}' could not be found");
            } else {
                AddCachedThemeByName(cacheName, ret);
            }

            return ret;

        }

      
        /// <summary>
        /// Get the theme from the cache, null if not found
        /// thread safe
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Theme GetCachedThemeByName(string name) {
            lock (_themesCacheLock) {
                if (_themesCache.ContainsKey(name)) {
                    return _themesCache[name];
                }
            }
            return null;
        }

        

        /// <summary>
        /// Retrieves all cached themes
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Theme> GetCachedThemes() {
            lock (_themesCacheLock) {
                return _themesCache.Values;
            }
        }

     

        /// <summary>
        /// Loads a theme from the given filePath from a json file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="theme">if the theme is null a new instance will be created with name and title,
        /// if the theme is not null all new properties from the current json will be loaded into the theme leaving the already existing properties as is </param>
        /// <returns></returns>
        public static Theme LoadThemeFromJSON(string filePath, Theme theme = null) {
            // NOTE: Recursive method

            // Does the file exist?
            if (File.Exists(filePath) == false) {
                return theme;
            }

            // Load JSON
            var content = File.ReadAllText(filePath);
            var jObject = JObject.Parse(content);

            // Properties
            var properties = LoadKeyValues(jObject,"properties");

            // Variables
            var variables = LoadKeyValues(jObject, "variables");
           

            // We are at the leave of inheritance
            if (theme == null) {
                theme = new Theme();
                theme.Name = jObject["name"]?.ToString();
                theme.Title = jObject["title"]?.ToString();
                theme.Inherits = jObject["inherits"]?.ToString();

                if (variables != null && variables.Any()) {
                    theme.Variables = new Properties(variables.ToDictionary(k => k.Key, v => v.Value));
                }

                foreach (var prop in properties) {

                    theme.Properties[prop.Key] = prop.Value;

                    // Variable?
                    if (prop.Value != null && prop.Value.GetType() == typeof(string)) {
                        var stringValue = prop.Value.ToString();
                        var variableName = stringValue.ReplacePrefix("$", string.Empty);
                        if (stringValue.StartsWith("$") == true && variables.ContainsKey(variableName)) {
                            theme.Properties[prop.Key] = variables[variableName];
                        }
                    }


                }

            } else {
                // We are at an ancestor of a theme we're actually loading  
                foreach (var prop in properties) {
                    if (theme.Properties.Keys.Contains(prop.Key) == false) {
                        theme.Properties[prop.Key] = prop.Value;
                    }
                }
            }

            // Load all inherited themes
            var inheritsArray = jObject["inherits"];

            if (inheritsArray != null) {
                var inherits = inheritsArray.Select(i => i.ToString());
                foreach (var inherit in inherits) {
                    var inheritFilePath = Core.Util.Files.GetHotSwappableFile(GetFilePath(inherit), null);
                    theme = LoadThemeFromJSON(inheritFilePath, theme);
                }
            }

            _logger.Trace($"Theme {filePath} -------------");
           // _logger.Trace($"Properties \n {theme.Properties.ToHumanReadableString()} -------------");

            return theme;
        }

        private static SortedDictionary<string, object> LoadKeyValues(JObject jObject, string key) {
            var obj = jObject[key];
            if (obj != null) {
                var dictionary = JsonConvert.DeserializeObject<SortedDictionary<string, object>>(obj.ToString());
                return dictionary;
            }
            return new SortedDictionary<string, object>();
        }

        /// <summary>
        /// Gets a Themes from the cache
        /// </summary>
        /// <returns></returns>
        internal static IDictionary<string,Theme> GetCache() {
            lock (_themesCacheLock){
                return _themesCache.ToDictionary(d => d.Key, v => v.Value);
            }
        }

        /// <summary>
        /// Load all available Themes in /static/themes and adds them to the cache
        /// 
        /// </summary>
        /// <returns></returns>
        public static void LoadAllThemes() {

            var basePath = GetThemesFilesPath();

            // No themes
            if (Directory.Exists(basePath)== false) {
                return;
            }

            _logger.Info("Loading themes from: " + basePath);

            var paths = Directory.GetFiles(basePath, "*.json");

            foreach (var path in paths) {
                try {
                    _logger.Info("Loading theme from: " + path);
                    var content = File.ReadAllText(path);
                    var jObject = JObject.Parse(content);
                    var name = jObject["name"]?.ToString();
                    GetThemeByName(name);
                } catch (Exception e) {
                    throw new BackendException("theme-loading-error", "Could not load theme from: " + path, e);
                }

            }
        }

        /// <summary>
        /// TODO: do we need this?
        /// </summary>
        public static void ClearThemeCache() {
            lock (_themesCacheLock) {
                _themesCache.Clear();
            }
        }

        [OnApplicationStartup]
        public static void OnApplicationStartup() {
            Theme.LoadAllThemes();
        }

        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////

        /// <summary>
        /// Get the file path for a theme name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string GetFilePath(string name) {
            var fileName = name + ".json";
            var filePath = GetThemesFilesPath() + Core.Config.PathSep + fileName;
            return filePath;
        }

        private static string GetThemesFilesPath() {
            return Core.Config.StaticPath + Core.Config.PathSep + "themes";
        }

        /// <summary>
        /// Adds to the cache
        /// thread save
        /// </summary>
        /// <param name="name"></param>
        /// <param name="theme"></param>
        private static void AddCachedThemeByName(string name, Theme theme) {
            lock (_themesCacheLock) {
                _themesCache[name] = theme;
            }
        }

        private static string GetCachename(string name) {
            return name;
        }


        #endregion


        #region Private Variables /////////////////////////////////////////////////////////////////

        private static object _themesCacheLock = new object();
        private static IDictionary<string, Theme> _themesCache = new Dictionary<string, Theme>();

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


  
}
