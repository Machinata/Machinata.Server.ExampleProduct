using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    public class MetaInformation {
        
        public string Title;
        public string TitleFromContent;
        public List<string> TitlesFromContent = new List<string>();
        public string TitleTrailer;
        public List<string> ExcludeTitleTrailerIfInTitle = new List<string>();
        public string Description;
        public string DescriptionFromContent;
        public string Keywords;
        public string Image;
        public string Author;
        public string Sitename;

        public string TitleWithContentFallback {
            get {
                if (this.Title != null) return Title;
                if (this.TitleFromContent != null) return TitleFromContent;
                return null;
            }
        }

        /// <summary>
        /// If true, page handlers will use the meta title instead of the navigation last item to page titles,
        /// as google no longer respects meta title tag...
        /// </summary>
        public bool UseMetaTitleForPageTitle = false;
        
        public string CompleteTitle {
            get {
                var completeTitle = Title;
                if (TitleTrailer != null && TitleTrailer != null && !completeTitle.Contains(TitleTrailer)) completeTitle += TitleTrailer;
                return completeTitle;
            }
        }
            
        public void LoadFromNode(ContentNode node) {
            // Load from explicit node meta options
            if (this.Title == null && node.Options["meta-title"] != null) this.Title = node.Options["meta-title"].ToString();
            if (this.Description == null && node.Options["meta-description"] != null) this.Description = node.Options["meta-description"].ToString();
            if (this.Keywords == null && node.Options["meta-keywords"] != null) this.Keywords = node.Options["meta-keywords"].ToString();
            if (this.Image == null && node.Options["meta-image"] != null) this.Image = node.Options["meta-image"].ToString();
            // Set derived values from nodes
            if (this.TitleFromContent == null && !string.IsNullOrEmpty(node.Title)) this.TitleFromContent = node.Title;
            if (this.DescriptionFromContent == null && !string.IsNullOrEmpty(node.Summary)) this.DescriptionFromContent = node.Summary;
            // Append to list
            if (!string.IsNullOrEmpty(node.Title)) this.TitlesFromContent.Add(node.Title);
        }

        public void LoadIntoNode(ContentNode node, bool overwriteIfEmpty = false) {
            if (!string.IsNullOrEmpty(this.Title) || overwriteIfEmpty) node.Options["meta-title"] = this.Title;
            if (!string.IsNullOrEmpty(this.Description) || overwriteIfEmpty) node.Options["meta-description"] = this.Description;
            if (!string.IsNullOrEmpty(this.Keywords) || overwriteIfEmpty) node.Options["meta-keywords"] = this.Keywords;
            if (!string.IsNullOrEmpty(this.Image) || overwriteIfEmpty) node.Options["meta-image"] = this.Image;
        }
    }
}
