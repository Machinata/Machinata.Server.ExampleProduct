using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Model {
    
    //[Serializable()] Not used yet
    //[ModelClass]
    public partial class StructuredDataConfig : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public StructuredDataConfig() {
            // Parameterless constructure is required for reflection...
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
      

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Value { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Category { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

      
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////


    public static class ModelContextStructuredDataConfigExtenions {
        public static DbSet<StructuredDataConfig> StructuredDataConfigs(this Core.Model.ModelContext context) {
            return context.Set<StructuredDataConfig>();
        }

        public static StructuredDataConfig GetByKey(this IQueryable<Core.Model.StructuredDataConfig> query, string category, string name, bool throwExceptions = true) {
            var result = query.Where(sd => sd.Category == category && sd.Name == name);
            if (result.Count() != 1 && throwExceptions) {
                throw new BackendException("entity-not-found", "Structured Data Config (name={name}, category={category}) not found");
            }
            return result.SingleOrDefault();
        }
    }


    #endregion
}
