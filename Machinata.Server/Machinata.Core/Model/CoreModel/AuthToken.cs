using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Web;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class AuthToken : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public AuthToken() { }

        public AuthToken(User user, HttpContext context, TimeSpan timespan, string hash = null) {
            // Validate
            if (user.Enabled == false) throw new BackendException("user-disabled",$"The account {user.Username} is not active.");
            // Link to user
            this.User = user;
            // Set expiration
            this.Expires = DateTime.Now.ToUniversalTime().Add(timespan);
            // Generate hash
            this.Hash = hash;
            if (this.Hash == null) this.Hash = AuthToken.GenerateHash();
            // Validate hash
            if (string.IsNullOrEmpty(this.Hash) || this.Hash.Length < 128) throw new Exception("The AuthToken hash is not valid or does not meet the security requirements.");
            // Set some audit properties
            this.IP = Core.Util.HTTP.GetRemoteIP(context);
            this.UserAgent = Core.Util.HTTP.GetUserAgent(context);
            this.Valid = true;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [ForeignKey("UserId")]
        [InverseProperty("AuthTokens")]
        [FormBuilder]
        public User User { get; set; } 

        [Column]
        [FormBuilder]
        public int UserId { get; set; }
        
        [Column]
        [FormBuilder]
        public string Hash { get; set; } 
        
        [Column]
        [FormBuilder]
        public string IP { get; set; } 
        
        [Column]
        [FormBuilder]
        public string UserAgent { get; set; } 

        [Column]
        [FormBuilder]
        public DateTime Expires { get; set; } 

        [Column]
        [FormBuilder]
        public bool Valid { get; set; } 
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        #endregion
        
        [NotMapped]
        public int ExpiresInDays {
            get {
                return (int)(this.Expires - DateTime.UtcNow).TotalDays;
            }
        }

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Generates a new unique GUID hash.
        /// For extra security against unknown future problems with hashes, 4 GUIDs are used.
        /// </summary>
        /// <returns></returns>
        public static string GenerateHash() {
            return Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "");
        }

        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAuthTokenExtenions {
        public static DbSet<AuthToken> AuthTokens(this Core.Model.ModelContext context) {
            return context.Set<AuthToken>();
        }
    }

    #endregion
}
