using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Text.RegularExpressions;
using Machinata.Core.Util;

namespace Machinata.Core.Model {

    public delegate void AccessPoliciesAndGroupsRebuilt(ModelContext db);

    [Serializable()]
    [ModelClass]
    public partial class AccessPolicy : ModelObject {


        public static event AccessPoliciesAndGroupsRebuilt AccessPoliciesAndGroupsRebuiltEvent;

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////
        
        //private const string _ALL_VERBS = "get|put|post|delete|options|patch|head";

        public const string SUPERUSER_ARN = "*"; // TODO @dan,@micha superuser == public arn
        public const string ROUTE_ARN = "{route}";
        public const string REDUCED_ROUTE_ARN = "{reduced-route}";
        //public const string VERB_ARN = "{verb}";
        public const string PUBLIC_ARN = "*";
        public const string ARN_SEP = ":";
        public const string DEFAULT_ARN = REDUCED_ROUTE_ARN;
        public const string ADMIN_ROOT_ARN = "/admin";
        public const string ADMIN_ARN = "/admin/*";
        public const string WRITE_ARN = "write";
        public const string DELETE_ARN = "delete";
        public const int PRIORITY_LOW = 0;
        public const int PRIORITY_NORMAL = 100;
        public const int PRIORITY_HIGH = 1000;

        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////
        
        public enum AccessType : Int16 {
            Allow = 10,
            Deny = 20
        }

        public enum OriginType : Int16 {
            System = 10,
            PolicyProvider = 20,
            User = 30
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public AccessPolicy() : base() {
            this.Users = new List<User>();
            this.AccessGroups = new List<AccessGroup>();
            this.Access = AccessType.Allow;
            this.Priority = PRIORITY_NORMAL;
            this.Origin = OriginType.User;
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        
        /// <summary>
        /// Gets or sets the shorthand policy name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Column]
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; } 

        /// <summary>
        /// Gets or sets the policy target Access Resource Name (ARN).
        /// </summary>
        /// <value>
        /// The arn.
        /// </value>
        [Column]
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ARN { get; set; }

        /// <summary>
        /// Gets or sets the access type for the policy.
        /// A policy is either allow or deny.
        /// </summary>
        /// <value>
        /// The access.
        /// </value>
        [Column]
        [Required]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public AccessType Access { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// The priority is used to allow certain policies to override others...
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        [Column]
        [Required]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Priority { get; set; }
        
        /// <summary>
        /// Gets or sets the origin.
        /// The origin describes where the policy comes from (system, policy provider, or manual by user)
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        [Column]
        [Required]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public OriginType Origin { get; set; }

        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [InverseProperty("AccessPolicies")]
        [FormBuilder(Forms.Admin.VIEW)]
        public ICollection<User> Users { get; set; }
        
        [Column]
        [InverseProperty("AccessPolicies")]
        [FormBuilder(Forms.Admin.VIEW)]
        public ICollection<AccessGroup> AccessGroups { get; set; }
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ClassId {
            get {
                var ret = this.ARN;
                if (ret == SUPERUSER_ARN) return "superuser";
                ret = (this.Access == AccessType.Deny ? "deny-" : "") + this.ARN.ToLower().Trim('/').Trim('*').Trim('/').Replace("/", "-");
                return ret;
                
            }
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        public bool MatchesARN(string arnToCheck) {
            // Check if it is super user
            if(this.ARN == AccessPolicy.SUPERUSER_ARN) {
                return true;
            } else {
                // Does the arn have a wildcard?
                if (this.ARN.Contains("*")) {
                    // Does the policy arn match the required route arn?
                    // A policy ARN may include a wildcard, which could look like this:
                    // /admin/*
                    // The pattern to match looks like the following:
                    // ^(/admin/.*?)
                    // The the pattern is automatically generated based on the policy arn
                    string regexPattern = $"^({this.ARN.Replace("*", ".*?")})";
                    Regex regex = new Regex(regexPattern);
                    var trailingWildcard = "/*";
                    if(regex.IsMatch(arnToCheck)) {
                        // Regex matches
                        return true;
                    } else if (this.ARN.EndsWith(trailingWildcard)) {
                        // Allow a ARN "/admin/" to match "/admin/*"
                        var trimmedARN = this.ARN.Substring(0, this.ARN.Length - trailingWildcard.Length);
                        if (trimmedARN.TrimEnd('/') == arnToCheck.TrimEnd('/')) return true;
                    }
                    return false;
                } else {
                    // Must be exact match
                    return this.ARN.TrimEnd('/') == arnToCheck.TrimEnd('/');
                }
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            CreateStandardSystemPolicies(context);
            CreateStandardPoliciesUsingPolicyProviders(context);
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Rebuilds the standard policies using policy providers.
        /// Warning: this will first remove all policies with OriginType.PolicyProvider.
        /// </summary>
        /// <param name="db">The database.</param>
        public static void RebuildStandardPoliciesUsingPolicyProviders(ModelContext db) {
            // Remove all from policy provider
            var policiesToRemove = db.AccessPolicies().Include(nameof(AccessPolicy.AccessGroups)).Where(p => p.Origin == OriginType.PolicyProvider);
            foreach(var p in policiesToRemove) {
                p.AccessGroups.Clear();
            }
            db.AccessPolicies().RemoveRange(policiesToRemove);
            // Create them again
            AccessPolicy.CreateStandardPoliciesUsingPolicyProviders(db);
        }

        /// <summary>
        /// Creates the standard system policies:
        /// Ready/Write
        /// Read-Only
        /// </summary>
        /// <param name="db">The database.</param>
        public static void CreateStandardSystemPolicies(ModelContext db) {
            // Create some standard policies
            db.AccessPolicies().Add(new AccessPolicy { Name = "Read/Write", ARN = WRITE_ARN, Access = AccessPolicy.AccessType.Allow, Origin = OriginType.System });
            db.AccessPolicies().Add(new AccessPolicy { Name = "Read-Only", ARN = WRITE_ARN, Access = AccessPolicy.AccessType.Deny, Priority = AccessPolicy.PRIORITY_HIGH, Origin = OriginType.System });
        }

        /// <summary>
        /// Creates the standard policies using policy providers in all modules and products.
        /// </summary>
        /// <param name="db">The database.</param>
        public static void CreateStandardPoliciesUsingPolicyProviders(ModelContext db) {
            // Find all method with attribute:
            // [PolicyProvider]
            var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(Core.Model.PolicyProviderAttribute));
            var newPolicies = new List<AccessPolicy>();
            foreach(var mi in methods) {
                var ret = mi.Invoke(null, null) as List<AccessPolicy>;
                foreach(var policy in ret) {
                    // Validate that we dont add a policy twice
                    if (!newPolicies.Any(p => p.Name == policy.Name || p.ARN == policy.ARN)) {
                        policy.Origin = OriginType.PolicyProvider;
                        newPolicies.Add(policy);
                    }
                }
            }
            // Sort
            newPolicies = newPolicies.OrderBy(ap => ap.ARN).ToList();
            // Register in db
            db.AccessPolicies().AddRange(newPolicies);
        }
        
        public static string GetDefaultRootAccessARN(string name) {
            return $"/{name.ToLower()}/*";
        }

        public static string GetDefaultAdminAccessARN(string name) {
            return $"/admin/{name.ToLower()}/*";
        }
        
        public static List<AccessPolicy> GetDefaultRootPolicies(string name) {
            return new List<AccessPolicy>() {
                new AccessPolicy() { Name = $"{name.ToSentence().Capitalize()}", ARN = GetDefaultRootAccessARN(name) }
            };
        }
        
        public static List<AccessPolicy> GetDefaultAdminPolicies(string name) {
            return new List<AccessPolicy>() {
                new AccessPolicy() { Name = $"{name.ToSentence().Capitalize()} Admin", ARN = GetDefaultAdminAccessARN(name) }
            };
        }


        /// <summary>
        /// Trigger the Policies and Groups have been rebuilt and saved
        /// </summary>
        public static void FirePoliciesAndGroupsRebuilt(ModelContext db) {
            AccessPolicy.AccessPoliciesAndGroupsRebuiltEvent?.Invoke(db);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAccessPolicyExtenions {
        public static DbSet<AccessPolicy> AccessPolicies(this Core.Model.ModelContext context) {
            return context.Set<AccessPolicy>();
        }
    }

    #endregion
}
