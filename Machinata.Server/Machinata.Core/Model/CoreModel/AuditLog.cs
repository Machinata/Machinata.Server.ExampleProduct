using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Data.Entity.Infrastructure;
using Machinata.Core.Handler;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class AuditLog : ModelObject {

        public class AuditLogPropertyChange : ModelObject {

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string Name { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string Before { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string After { get; set; }

        }
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public AuditLog() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Actor { get; set; } 

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Action { get; set; } 

        [Column]
        [FormBuilder]
        public string TargetType { get; set; } 

        [Column]
        [FormBuilder]
        public string TargetPublicID { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string ARN { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Path { get; set; } 

        [Column]
        [FormBuilder]
        public string Properties { get; set; } 

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string IP { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string AuthToken { get; set; } 
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        public List<AuditLogPropertyChange> Changes {
            get {
                var ret = new List<AuditLogPropertyChange>();
                if(!string.IsNullOrEmpty(this.Properties)) {
                    var jobj = Core.JSON.ParseJsonAsJObject("{changes:"+this.Properties+"}");
                    foreach(var jtok in jobj["changes"].Children()) {
                        ret.Add(new AuditLogPropertyChange() {
                            Name = jtok["name"].ToString(),
                            Before = jtok["before"].ToString(),
                            After = jtok["after"].ToString()
                        });
                    }
                }
                return ret;
            }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Target {
            get {
                return this.TargetType + (this.TargetPublicID!=null?" #"+this.TargetPublicID:"");
            }
        } 
        
        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Title {
            get {
                return $"{this.Actor} {this.Action} {this.Target}";
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes the with a handler.
        /// Sets IP, ARN, Path et cetera...
        /// </summary>
        /// <param name="handler">The handler.</param>
        public void InitWithHandler(Handler.Handler handler) {
            if(handler == null) {
                // System
                this.Actor = "System";
                this.IP = Core.Util.HTTP.GetLocalIPAddress();
            } else {
                // Handler context
                if (handler.User != null) this.Actor = handler.User.Username;
                else this.Actor = "Guest";
                this.IP = Core.Util.HTTP.GetRemoteIP(handler.Context);
                this.ARN = handler.RequiredRequestARN;
                this.Path = handler.RequestPath;
                try {
                    this.AuthToken = handler.AuthToken?.Hash; // Can throw exception if the token is not valid
                } catch { }
            }
        }
        
        /// <summary>
        /// Initializes the with an entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void InitWithEntry(DbEntityEntry entry) {
            // Set the type
            this.Action = entry.State.ToString();
            // Compile all changes
            var pcs = new List<AuditLogPropertyChange>();
            var valuesBefore = new Dictionary<string, string>();
            var valuesAfter = new Dictionary<string, string>();
            if(entry.State != EntityState.Added) valuesBefore = ExplodeEntryPropertyValues(entry.OriginalValues);
            if(entry.State != EntityState.Deleted) valuesAfter = ExplodeEntryPropertyValues(entry.CurrentValues);
            foreach(var key in valuesAfter.Keys) {
                var pc = new AuditLogPropertyChange();
                pc.Name = key;
                pc.After = valuesAfter[key];
                if(valuesBefore.ContainsKey(key)) pc.Before = valuesBefore[key];
                // Only add if it changed
                if(pc.Before != pc.After) pcs.Add(pc);
            }
            if (pcs.Count > 0) this.Properties = Core.JSON.Serialize(pcs);
        }

        /// <summary>
        /// Initializes the with an entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void InitWithEntity(ModelObject entity) {
            this.TargetType = entity.TypeName;
            this.TargetPublicID = entity.PublicId;
        }

        #endregion
        
        #region Public Virtual Methods /////////////////////////////////////////////////////////////
        
        public override Core.Cards.CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Sub(this.Path)
                .Sub(this.IP)
                .Sub(this.Created.ToString(Core.Config.DateTimeFormat))
                .Wide();
        }

        public override AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
            return null;
        }

        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////


        /// <summary>
        /// Explodes the entry DbPropertyValues by inspecting each value and iterating the keyval if the value itself is a DbPropertyValues.
        /// This happens for all compex types, for example.
        /// Furthermore, if the value is a JSON key value list, then that is exploded as well.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        private static Dictionary<string,string> ExplodeEntryPropertyValues(DbPropertyValues values) {
            var ret = new Dictionary<string, string>();
            foreach (var key in values.PropertyNames) {
                var val = values[key];
                if(val is DbPropertyValues) {
                    // Explode 
                    var innerValues = val as DbPropertyValues;
                    if(innerValues == null) {
                        ret.Add(key, null);
                        continue;
                    }
                    foreach(var innerKey in innerValues.PropertyNames) {
                        // Extract inner value
                        var innerVal = GetStringValueForEntryValue(innerValues[innerKey]);
                        // Encoded key/vals?
                        if (innerVal != null && innerVal.StartsWith("{") && innerVal.EndsWith("}")) {
                            // Looks like a json key/val...
                            try {
                                var jobj = Core.JSON.ParseJsonAsJObject(innerVal);
                                foreach(var jkeyval in jobj) {
                                    ret.Add(key+"."+jkeyval.Key, jkeyval.Value?.ToString());
                                }
                            } catch {
                                ret.Add(key+"."+innerKey, innerVal);
                            }
                        } else {
                            ret.Add(key+"."+innerKey, innerVal);
                        }
                    }
                } else {
                    ret.Add(key, GetStringValueForEntryValue(val));
                }
            }
            return ret;
        }

        /// <summary>
        /// Helper method.
        /// Gets the string value for entry value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        private static string GetStringValueForEntryValue(object val) {
            if (val == null) return null;
            if(val is string) {
                return (string)val;
            } if(val is DateTime) {
                return ((DateTime)val).ToString(Core.Config.DateTimeFormat);
            } else {
                return val.ToString();
            }
        }

        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAuditLogExtenions {
        public static DbSet<AuditLog> AuditLogs(this Core.Model.ModelContext context) {
            return context.Set<AuditLog>();
        }
    }

    #endregion
}
