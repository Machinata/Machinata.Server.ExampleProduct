using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Util;
using System.Text.RegularExpressions;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class Address : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Address() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [MaxLength(200)]
        public string Hash
        {
            get
            {
                return CalculateHash();
            }
        }
          
    

        [Column]
        public bool Archived { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.KETS.FORM)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.KETS.FORM)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [MaxLength(100)]
        public string Company { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.KETS.FORM)]
        [MaxLength(300)]
        public string Address1 { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.KETS.FORM)]
        [MaxLength(300)]
        public string Address2 { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.KETS.FORM)]
        [MaxLength(100)]
        public string City { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [MaxLength(4)]
        public string StateCode { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [MaxLength(3)]
        [DataType(CustomDataTypes.Country)]
        [FormBuilder(Forms.KETS.FORM)]
        public string CountryCode { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.KETS.FORM)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [MaxLength(20)]
        public string ZIP { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.KETS.FORM)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [MaxLength(20)]
        public string Phone { get; set; }
        
        [Column]
        public string EmailEncrypted { get; set; }

        [Column]
        public string EmailHash { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

      
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.System.HASH_KEY)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string Email
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.EmailEncrypted);
            }
            set
            {
                this.EmailHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.EmailEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Summary
        {
            get {
                return FormatAsString(", ");
            }
        }

        [NotMapped]
        [FormBuilder]
        public string Location
        {
            get {
                return FormatAsString(", ", "Company,Name,Email,Phone");
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public string Country
        {
            get {
                if (string.IsNullOrEmpty(this.CountryCode)) {
                    return null;
                } else {
                    return ("country-" + this.CountryCode).TextVar();
                }
            }
        }

        [NotMapped]
        public string ZipAndCity
        {
            get {
                var ret = $"{ZIP} {City}";
                return ret.Trim();
            }
        }

        [NotMapped]
        public string Firstname {
            get {

                if (this.Name != null) {
                    var splits = this.Name.Split(new char[] { ' ' }, 2);
                    return splits.First();
                }

                return null;

            }
        }

        [NotMapped]
        public string Lastname {
            get {

                if (this.Name != null) {
                    var splits = this.Name.Split(new char[] { ' ' }, 2);
                    if (splits.Count() > 1) {
                        return splits.Last();
                    }
                }
                return null;
            }
        }




        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string CalculateHash() {
            var stringToHash = this.GetObjectValueSummary(new FormBuilder(Forms.System.HASH_KEY));
            return Core.Encryption.DefaultHasher.HashString(stringToHash);
        }

        public string FormatAsString(string separator = ", ", string exclude = null) {
            var segs = new List<string>();
            if (exclude == null) exclude = "";
            if (!string.IsNullOrEmpty(this.Company) && !exclude.Contains("Company")) segs.Add(this.Company);
            if (!string.IsNullOrEmpty(this.Name) && !exclude.Contains("Name")) segs.Add(this.Name);
            if (!string.IsNullOrEmpty(this.Address1) && !exclude.Contains("Address1")) segs.Add(this.Address1);
            if (!string.IsNullOrEmpty(this.Address2) && !exclude.Contains("Address2")) segs.Add(this.Address2);
            if (!string.IsNullOrEmpty(this.ZipAndCity) && !exclude.Contains("ZipAndCity")) segs.Add(this.ZipAndCity);
            if (!string.IsNullOrEmpty(this.Country) && !exclude.Contains("Country")) segs.Add(this.Country);
       
            return string.Join(separator, segs);
        }

        public string Title { get {return "{text.address} " + ((string.IsNullOrWhiteSpace(Address1)) ? PublicId : Address1); } }



        private static string _cleanAddressSegment(string seg) {
            if (seg == null) return null;
            return seg.Trim();
        }


        public static Address ParseAddressFromString(string addrString) {
            var addrSegs = addrString.Split(',');
            var usedSegs = new List<string>();
            var unusedSegs = new List<string>();

            // Parse out address
            var ret = new Address();


            for (var i = addrSegs.Length - 1; i >= 0; i--) {
                //for (var i = 0; i < addrSegs.Length; i++) {
                var segRaw = addrSegs[i];
                var seg = _cleanAddressSegment(segRaw);
                var segUsed = false; // indicates the seg was used

                // Check if UK zip
                if (segUsed == false && i != 0) {
                    Regex regex = new Regex("^([A-Z0-9]{3,5})\\s([A-Z0-9]{3,5})$"); // "RH25 9VX", "TQ2 2SL", "N16 4HF"
                    var matches = regex.Matches(seg);
                    if (matches.Count == 1) {
                        var match = matches[0];
                        if (match.Groups.Count == 3) {
                            ret.ZIP = match.Groups[1].Value + " " + match.Groups[2].Value;
                            segUsed = true;
                            usedSegs.Add(seg);

                            // Extract city? By steppting one back...
                            if (i >= 1) {
                                var cityCandidate = addrSegs[i - 1];
                                ret.City = _cleanAddressSegment(cityCandidate);
                                usedSegs.Add(cityCandidate);
                                unusedSegs.Remove(cityCandidate);
                            }
                        }
                    }
                }

                // Check if EU zip + city
                if (segUsed == false && i != 0) {
                    Regex regex = new Regex("^([A-Z0-9]{4,8})\\s(.*)$"); // "5611JD Eindhoven"
                    var matches = regex.Matches(seg);
                    if (matches.Count == 1) {
                        var match = matches[0];
                        if (match.Groups.Count == 3) {
                            ret.ZIP = match.Groups[1].Value;
                            ret.City = match.Groups[2].Value;
                            segUsed = true;
                            usedSegs.Add(seg);
                        }
                    }
                }

                // Check if CH/DE zip + city
                if (segUsed == false && i != 0) {
                    Regex regex = new Regex("^(\\d{4,5})\\s(.*)$"); // "8003 Zurich"
                    var matches = regex.Matches(seg);
                    if (matches.Count == 1) {
                        var match = matches[0];
                        if (match.Groups.Count == 3) {
                            ret.ZIP = match.Groups[1].Value;
                            ret.City = match.Groups[2].Value;
                            segUsed = true;
                            usedSegs.Add(seg);
                        }
                    }
                }

                // Check if USA zip + city
                if (segUsed == false && i != 0) {
                    Regex regex = new Regex("^([A-Z]{2,3})\\s(\\d{5,6})$"); // "MO 64137", "DBR 364137"
                    var matches = regex.Matches(seg);
                    if (matches.Count == 1) {
                        var match = matches[0];
                        if (match.Groups.Count == 3) {
                            ret.ZIP = match.Groups[2].Value;
                            ret.StateCode = match.Groups[1].Value;
                            segUsed = true;
                            usedSegs.Add(seg);

                            // Extract city? By steppting one back...
                            if (i >= 1) {
                                var cityCandidate = addrSegs[i - 1];
                                ret.City = _cleanAddressSegment(cityCandidate);
                                usedSegs.Add(cityCandidate);
                                unusedSegs.Remove(cityCandidate);
                            }
                        }
                    }
                }

                // Check if country
                if (segUsed == false && i != 0) {
                    foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                        var cc = Core.Util.CountryCodes.GetCountries(lang).FirstOrDefault(e => e.Name.ToLower() == seg.ToLower());
                        if (cc != null) {
                            ret.CountryCode = cc.Code;
                            segUsed = true;
                            usedSegs.Add(seg);
                        }
                    }
                }
                if (segUsed == false && i != 0) {
                    if (seg == "USA") {
                        ret.CountryCode = "us";
                        segUsed = true;
                        usedSegs.Add(seg);
                    } else if (seg == "CH") {
                        ret.CountryCode = "ch";
                        segUsed = true;
                        usedSegs.Add(seg);
                    } else if (seg == "UK") {
                        ret.CountryCode = "uk";
                        segUsed = true;
                        usedSegs.Add(seg);
                    } else if (seg == "DE") {
                        ret.CountryCode = "de";
                        segUsed = true;
                        usedSegs.Add(seg);
                    } else if (seg == "The Netherlands") {
                        ret.CountryCode = "nl";
                        segUsed = true;
                        usedSegs.Add(seg);
                    }
                }

                // Check Postbox

                // Bookkeeping
                if (segUsed == false && !usedSegs.Contains(segRaw)) {
                    unusedSegs.Insert(0, segRaw);
                }

            }

            // All unused segs must be added to Address1 and Address2
            {
                for (var i = 0; i < unusedSegs.Count; i++) {
                    var segRaw = unusedSegs[i];
                    var seg = _cleanAddressSegment(segRaw);
                    if (i == 0) {
                        ret.Address1 = seg;
                    } else if (i == 1) {
                        ret.Address2 = seg;
                    } else {
                        ret.Address2 = ret.Address2 + ", " + seg;
                    }
                }
            }

            return ret;
        }


        /// <summary>
        /// Returns true of any Member of the Address is null, empty or whitespace
        /// </summary>
        /// <returns></returns>
        public bool AllFieldsNullEmptyOrWhiteSpace(FormBuilder form) {

            var properties = this.GetPropertiesForForm(form);

            bool anyNotEmpty = false;
            foreach(var property in properties) {

                var value = property.GetValue(this);
                if (value != null && string.IsNullOrWhiteSpace(value.ToString())== false ){
                    anyNotEmpty = true;
                }
            }

            return anyNotEmpty == false;
        }

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            return FormatAsString();
        }

        public override Core.Cards.CardBuilder VisualCard() {

            var line3 = this.StateCode + " " + this.ZIP + " " + this.City;
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name != null ? Core.Util.String.CreateSummarizedText(this.Name, 40, true) : null)
                .Subtitle(this.Company != null ? Core.Util.String.CreateSummarizedText(this.Company, 40, true) : null)
                .Subtitle(this.Address1 != null ? Core.Util.String.CreateSummarizedText(this.Address1, 40, true) : null)
                .Subtitle(this.Address2 != null ? Core.Util.String.CreateSummarizedText(this.Address2, 40, true) : null)
                .Subtitle(line3 != null ? Core.Util.String.CreateSummarizedText(line3, 40, true) : null)
                .Tag(this.Phone)
                .Icon("contacts")
                .Wide();
            return card;
        }

        /// <summary>
        /// Creates an archived copy of the given address
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        public static Address CreateArchived(Address address) {
            var archived = new Address();
            archived.Archived = true;
            
            archived.Address1 = address.Address1;
            archived.Address2 = address.Address2;
            archived.City = address.City;
            archived.Company = address.Company;
            archived.CountryCode = address.CountryCode;
            archived.Email = address.Email;
            archived.Name = address.Name;
            archived.Phone = address.Phone;
            archived.StateCode = address.StateCode;
            archived.ZIP = address.ZIP;

            return archived;
           

        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAddressExtenions {
        public static DbSet<Address> Addresses(this Core.Model.ModelContext context) {
            return context.Set<Address>();
        }
    }

    #endregion
}
