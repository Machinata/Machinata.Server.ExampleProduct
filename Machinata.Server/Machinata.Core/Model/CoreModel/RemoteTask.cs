using Machinata.Core.Builder;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Machinata.Core.Model {

    [Serializable()]
    [ModelClass]
    public class RemoteTask : ModelObject {

      
        public enum RemoteTaskState : short {
            Created = 0,
            Waiting = 1,
            Assigned = 2,
            Running = 3,
            Finished = 10,
            Paused = 20,
            Canceling = 30,
            Error = 100,
            Canceled = 200
        }

      

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public RemoteTask() {
            this.Parameters = new Properties();
            this.StatusInfos = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EDIT)]
        public RemoteTaskState State { get; set; }

      
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime? Started { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime? Finished { get; set; }


        /// <summary>
        /// Fullname with namespace of the Task class (without assembly)
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.System.LISTING)]
        public string Name { get; set; }

     
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public bool Enabled { get; set; }

       
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Target { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Properties Parameters { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Properties StatusInfos { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Title
        {
            get
            {
                return TaskManager.Task.SimpleTaskName(Name);
            }
        }
       

        public string StyleClass { get { return this.State == RemoteTaskState.Error ? "warning" : ""; } }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

      
       

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
       
        


        public override string ToString() {
            return $"{Title}";
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextRemoteTaskExtenions {
        public static DbSet<RemoteTask> RemoteTasks(this Core.Model.ModelContext context) {
            return context.Set<RemoteTask>();
        }
    }

    #endregion
}
