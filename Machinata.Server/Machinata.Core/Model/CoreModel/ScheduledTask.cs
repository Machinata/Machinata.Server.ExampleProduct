using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using static Machinata.Core.Model.ScheduledTask;

namespace Machinata.Core.Model {

    [Serializable()]
    [ModelClass]
    public class ScheduledTask : ModelObject {

        public enum ScheduledTaskState : short {
            Created = 0,
            Waiting = 2,
            Running = 3,
            Finished = 10,
            Error = 100
        }

        public enum ScheduledIntervalType {
            Time, Monthly
        }


        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ScheduledTask() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ScheduledTaskState State { get; set; }

     
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime LastRun { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Fullname with namespace of the Task class (without assembly)
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.System.LISTING)]
        public string Name { get; set; }

     
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public bool Enabled { get; set; }

        [Column]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.SELECTION)]
        public bool LogToDB { get; set; } = false;


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Role { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string IntervalConfig { get; set; }

      
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Title
        {
            get
            {
                return TaskManager.Task.SimpleTaskName(Name);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public TimeSpan WillRunIn
        {
            get
            {
                if (!Enabled || NextRun < DateTime.UtcNow) return new TimeSpan(0, 0, 0);
                return (NextRun - DateTime.UtcNow);
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.LISTING)]
        public TimeSpan Interval
        {
            get
            {
                try {
                    return Core.Util.Time.GetTimespanFromString(IntervalConfig);
                } catch (Exception e) {
                    var message = string.Format("There is an error in the configuration of the scheduled task: {0} , wrong interval format: {1}.", Name, IntervalConfig);
                    throw new Exceptions.BackendException("scheduled-task-error", message, e);
                }
            }
        }

        [NotMapped]
        public ScheduledIntervalType IntervalType
        {
            get
            {
                if (IntervalConfig == "monthly") {
                    return ScheduledTask.ScheduledIntervalType.Monthly;
                }
                return ScheduledIntervalType.Time;
            }
        }

        public string StyleClass { get { return this.State == ScheduledTaskState.Error ? "warning" : ""; } }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            CreateMissingScheduledTask(context);
        }

        /// <summary>
        /// Creates the missing scheduled tasks, so 
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="existingTasks">The existing tasks.</param>
        public static IEnumerable<ScheduledTask> CreateMissingScheduledTask(ModelContext context, IEnumerable<ScheduledTask> existingTasks = null) {

            var result = new List<ScheduledTask>();

            foreach (var type in TaskManager.TaskManager.GetAvailableTasks()) {
                ScheduledTaskConfig config = GetTaskConfig(type);

                // Skip if not isntall
                if (config.Install == false) {
                    continue;
                }

                // Skip if already exists (by FullName)
                if (existingTasks != null && existingTasks.Any(t => t.Name == type.FullName)) {
                    continue;
                }

                // Create ScheduledTask
                var scheduledTask = new ScheduledTask();
                scheduledTask.Name = type.FullName;
                scheduledTask.Role = Core.Config.Environment + "-" + Core.Config.Role;
                scheduledTask.Enabled = config.Enabled;
                scheduledTask.IntervalConfig = config.Interval;
                context.ScheduledTasks().Add(scheduledTask);
                result.Add(scheduledTask);

            }

            return result;
        }
        /// <summary>
        /// Gets the task configuration for a TaskManager.Task type
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static ScheduledTaskConfig GetTaskConfig(Type type) {
            var task = Core.Reflection.Types.CreateInstance<Core.TaskManager.Task>(type);
            var config = task.GetScheduledTaskConfig();
            if (config == null) {
                throw new BackendException("task-config-error", $"No TaskConfiguration GetScheduledTaskConfig defined for {type.Name}");
            }
            return config;
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        public void SetState(ScheduledTaskState state) {
            using(var db = ModelContext.GetModelContext(null)) {
                var task = db.ScheduledTasks().SingleOrDefault(st => st.Id == Id);
                task.State = state;
                db.SaveChanges();
            }
        }

        public void SetLastRun(DateTime lastRun) {
            using (var db = ModelContext.GetModelContext(null)) {
                var task = db.ScheduledTasks().SingleOrDefault(st => st.Id == Id);
                task.LastRun = lastRun;
                db.SaveChanges();
            }
        }

        public DateTime NextRun
        {
            get
            {
                if (IntervalType == ScheduledIntervalType.Monthly) {
                    var nextrun = LastRun.AddMonths(1);
                    return new DateTime(nextrun.Year, nextrun.Month, 1, 14, 0, 0); // TODO: WHAT DAY OF MONTH?
                }
                return LastRun.Add(Interval);
            }
        }

        public override string ToString() {
            return $"{Title}: Interval: {IntervalConfig}";
        }

        public bool ShouldRun() {

            if (Enabled && this.State == ScheduledTask.ScheduledTaskState.Waiting) {
                return DateTime.UtcNow >= NextRun;
            }
            return false;

        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////

        public override AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
            return null;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextScheduledTaskExtenions {
        public static DbSet<ScheduledTask> ScheduledTasks(this Core.Model.ModelContext context) {
            return context.Set<ScheduledTask>();
        }
    }

    #endregion

    public class ScheduledTaskConfig {

        public string Interval { get; set; }

        /// <summary>
        /// Auto Install Task at seed or re-generation
        /// </summary>
        public bool Install { get; set; }

        /// <summary>
        /// Is Enabled at install
        /// </summary>
        public bool Enabled { get; set; }


        /// <summary>
        /// Gets the default: Disabled, No Install, 1h, Time
        /// </summary>
        /// <returns></returns>
        public static ScheduledTaskConfig GetDefault() {
            var config = new ScheduledTaskConfig();
            config.Interval = "1h";
            config.Enabled = false;
            config.Install = false;
            return config;
        }
    }
}
