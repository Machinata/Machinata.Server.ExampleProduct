using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Machinata.Core.Handler;
using System.Threading;
using System.IO;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;

namespace Machinata.Core.Model {

    [Serializable()]
    [ModelClass]
    public partial class ContentNode : ModelObject, IPublishedModelObject, IShortURLModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants

        public const string NODE_PATH_SEP = "/";

        public const string NODE_TYPE_NODE = "node";
        public const string NODE_TYPE_TRANSLATION = "trans";
        public const string NODE_TYPE_IMAGE = "image";
        public const string NODE_TYPE_THUMBNAIL = "thumb";
        public const string NODE_TYPE_VIDEO = "video";
        public const string NODE_TYPE_HTML = "html";
        public const string NODE_TYPE_EMBED = "embed";
        public const string NODE_TYPE_FILE = "file";
        public const string NODE_TYPE_TITLE = "title";
        public const string NODE_TYPE_LINK = "link";
        public const string NODE_TYPE_BUTTON = "button";
        public const string NODE_TYPE_LABEL = "label";
        public const string NODE_TYPE_SHORTURL = "shorturl";
        public const string NODE_TYPE_DESCRIPTION = "desc";
        public const string NODE_TYPE_OPTION = "option";
        public const string NODE_TYPE_META = "meta";
        public const string NODE_TYPE_SPACER = "spacer";
        public const string NODE_TYPE_QUOTE = "quote";
        
        public const string ROOT_NODE_NAME = "root";
        public const string ROOT_NODE_PATH = "/";
        public const string ENTITIES_PATH_NAME = "/Entities";
        public const string TEMPLATES_PATH_NAME = NODE_PATH_SEP + "Templates";
        public const string TEMPLATES_NODE_NAME = "Templates";

        public const int NODE_TYPE_SORT_DEFAULT_PRIORITY = 0;
        public const int NODE_TYPE_SORT_LOW_PRIORITY = 0;
        public const int NODE_TYPE_SORT_HIGH_PRIORITY = 100;

        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public ContentNode() {
            this.Modified = DateTime.UtcNow;
            this.Children = new List<ContentNode>();
            this.Options = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        [MinLength(2)]
        [MaxLength(40)]
        [Required]
        [Index(IsUnique = false)]
        public string Name { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        [Required]
        [MaxLength(512)]
        [Index(IsUnique = true)]
        public string Path { get; set; }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        [MaxLength(256)]
        [Index(IsUnique = false)]
        public string ShortURL { get; set; }

        [Column]
        [ForeignKey("ParentId")]
        public ContentNode Parent { get; set; }
        
        [Column]
        public int? ParentId { get; set; }

        [Column]
        [InverseProperty("Parent")]
        public ICollection<ContentNode> Children { get; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        [MaxLength(8)]
        public string NodeType { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string Value { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string Summary { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string Title { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public DateTime Modified { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public int Sort { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public bool Published { get; set; } = false;
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string Layout { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public Properties Options { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public ContentNode GetExactTranslationForLanguage(string language) {
            if (language == "default") language = null;
            return this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION).Where(n => n.Value == language).SingleOrDefault();
        }
        
        public ContentNode TranslationForLanguage(string language) {
            if (language == "default") language = null;
            return this.TranslationsForLanguage(language).FirstOrDefault();
        }

        public ContentNode GetExactTranslationForLanguageOrDefault(string language) {
            if (language == "default") language = null;
            var translation =  this.TranslationsForLanguage(language).FirstOrDefault();
            if (translation != null && ( translation.Value == language || translation.Value == null)) {
                return translation;
            }
            return null;
        }

        public ContentNodeTypeMeta NodeTypeMeta {
            get {
                return ContentNode.GetAllTypeMetas().Where(nt => nt.ID == this.NodeType).SingleOrDefault();
            }
        }

        public string ValueForTemplate {
            get {
                if (this.NodeTypeMeta == null) throw new Exception("NodeTypeMeta is null (invalid or unsupported node type?)");
                return this.NodeTypeMeta.ConvertToTemplateValue(this.Value);
            }
        }

        public string ValueForEditor {
            get {
                if (this.NodeTypeMeta == null) throw new Exception("NodeTypeMeta is null (invalid or unsupported node type?)");
                return this.NodeTypeMeta.ConvertToEditorValue(this.Value);
            }
        }

        public string ValueForDB {
            get {
                if (this.NodeTypeMeta == null) throw new Exception("NodeTypeMeta is null (invalid or unsupported node type?)");
                return this.NodeTypeMeta.ConvertToDBValue(this.Value);
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string ReadableId {
            get {
                return this.Path.ReplacePrefix("/", "").Replace("/", "_").ToDashedLower().Replace("-", "_").Replace(" ", "_").Trim('_');
            }
        }

        public bool IsSystemNode {
            get {
                if (this.Name == "Entities") return true;
                else if (this.Name == "Templates") return true;
                else return false;
            }
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////


        /// <summary>
        /// Automatically incldues the content nodes for a page (NODE_TYPE_NODE) or translation 
        /// (NODE_TYPE_TRANSLATION).
        /// 
        /// Note: When possible, one should always try to include the content for a page directly
        /// in the original query.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Node must be a NODE_TYPE_NODE or NODE_TYPE_TRANSLATION in order to load it's content.</exception>
        public ContentNode IncludeContent() {
            if (this.NodeType == NODE_TYPE_NODE) {
                this.Include(nameof(ContentNode.Children));
                foreach (var translation in Children) {
                    translation.Include(nameof(ContentNode.Children));
                }
            } else if (this.NodeType == NODE_TYPE_TRANSLATION) {
                this.Include(nameof(ContentNode.Children));
            } else {
                throw new Exception("Node must be a NODE_TYPE_NODE or NODE_TYPE_TRANSLATION in order to load it's content.");
            }
            return this;
        }

        /// <summary>
        /// Gets the node CMS preview links as ContentNodePreview by finding all matching request
        /// handlers and getting their route paths.
        /// A route is matched to the CMS path by having the attribute [CMSPath].
        /// </summary>
        /// <returns></returns>
        public List<ContentNodePreview> GetNodeCMSPreviews() {
            var allCMSPreviewAttribs = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(CMSPathAttribute));
            var ret = new List<ContentNodePreview>();
            foreach(var method in allCMSPreviewAttribs) {
                var otherAttribs = method.GetCustomAttributes(typeof(CMSPathAttribute),true);
                foreach(var obj in otherAttribs) {
                    var cmsPathAttrib = obj as CMSPathAttribute;
                    var processedcmsPath = cmsPathAttrib.Path;
                    processedcmsPath = processedcmsPath.Replace("{node.name}", this.Name);
                    processedcmsPath = processedcmsPath.Replace("{node.path}", this.Path);
                    processedcmsPath = processedcmsPath.Replace("{node.publicId}", this.PublicId);
                    processedcmsPath = processedcmsPath.Replace("{node.public-id}", this.PublicId);
                    // Does the cmsPathAttrib path match the current node path? (starts with)
                    if (this.Path.StartsWith(processedcmsPath)) {
                        // Get all the routs for this method and register the language and route path
                        //var routeAttrs = method.GetCustomAttributes(typeof(RequestHandlerAttribute), true);
                        //foreach (var obj2 in routeAttrs) {
                        //    var routeAttr = obj2 as RequestHandlerAttribute;
                        //    var preview = new ContentNodePreview();
                        //    preview.RoutePath = routeAttr.Path;
                        //    preview.NodePath = this.Path;
                        //    preview.Language = routeAttr.Language;
                        //    ret.Add(preview);
                        //}
                        var routes = Routes.Route.GetRoutesForMethod(method);
                        foreach(var route in routes) {
                            var processedPath = route.Path;
                            processedPath = processedPath.Replace("{nodeName}", this.Name);
                            processedPath = processedPath.Replace("{nodePath}", this.Path);
                            processedPath = processedPath.Replace("{publicId}", this.PublicId);
                            processedPath = processedPath.Replace("{nodePublicId}", this.PublicId);
                            // Validate
                            if (processedPath.Contains("{")) continue;
                            // Register
                            var preview = new ContentNodePreview();
                            preview.RoutePath = processedPath;
                            preview.NodePath = this.Path;
                            preview.Language = route.Language;
                            ret.Add(preview);
                        }
                    }
                }
            }
            return ret;
        }
        
        public string Rename(string newName) {
            this.Include($"{nameof(Parent)}");
            if (Path == "/" || Parent == null) {
                throw new BackendException("rename-error","Renaming root node is not allowed.");
            }

            // Transform name
            newName = TransformNodeName(newName);

            // Validate name
            ValidateNodeName(newName);

            // Check new name has content
            if (string.IsNullOrWhiteSpace(newName)) {
                throw new BackendException("rename-error", "The new name cannot be empty.");
            }

            // Load siblings
            this.Parent.Include(nameof(Parent.Children));

            // Check new name free
            if (Parent.Children.Any(c => c.Name == newName)) {
                throw new BackendException("rename-error", "Cannot rename this node. There is already a node with the same name.");
            }

            // New name
            this.Name = newName;

            // Mark as modified
            this.Modified = DateTime.UtcNow;

            //Set path
            if (Parent.Path != "/") {
                this.Path = Parent.Path + "/" + this.Name;
            }
            else {
                this.Path = "/" + this.Name;
            }

            UpdateChildrenPaths(this);

            return Path;
        }


        public IEnumerable<ContentNode> ChildrenForType(string type) {
            return this.Children.Where(n => n.NodeType == type);
        }

        public IEnumerable<ContentNode> Images() {
            if (this.NodeType != NODE_TYPE_TRANSLATION) throw new Exception("The node is not a content (translation) node.");
            return this.ChildrenForType(NODE_TYPE_IMAGE);
        }


        public IEnumerable<ContentNode> Thumbnails() {
            if (this.NodeType != NODE_TYPE_TRANSLATION) throw new Exception("The node is not a content (translation) node.");
            return this.ChildrenForType(NODE_TYPE_THUMBNAIL);
        }



        public IEnumerable<ContentNode> TranslationsForLanguage(string language) {
            if(language == null) {
                // Return default
                return this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION).Where(n => n.Value == null);
            } else if(language == "*") {
                // Return all
                return this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION);
            } else {
                // Find for langauge
                var ret = this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION).Where(n => n.Value == language);
                // Get fallback
                if(ret.Count() == 0) {
                    // Fallback
                    ret = this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION).Where(n => n.Value == null);
                }
                if(ret.Count() == 0) {
                    // Fallback
                    ret = this.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION);
                }
                return ret;
            }
        }
         
        public void AddChild(ContentNode childNode) {
            // No name?
            if (childNode.Name == null) childNode.Name = Guid.NewGuid().ToString().Replace("-","");
            // Set child path to point to parent
            childNode.Path = this.Path + (this.Path==NODE_PATH_SEP?"":NODE_PATH_SEP) + childNode.Name;
            // Register child with parent
            this.Children.Add(childNode);
            this.Modified = DateTime.UtcNow;
        }

        public static string TransformNodeName(string name) {
            return Core.Util.String.CreateShortURLForName(
                name: name, 
                makeLowercase: false,  
                removeSpaces: false
            );
        }

        public static void ValidateNodeName(string name) {
            if (name.Contains(NODE_PATH_SEP)) throw new BackendException("invalid-name",$"Sorry, name cannot contain a '{NODE_PATH_SEP}' character.");
        }

        public static void ValidatePageLanguage(string language) {
            //TODO?
        }

        /// <summary>
        /// Creates a subnode at an arbitrary path, automatically creating all nodes in between.
        /// If the node exists, the method will return that already existing node.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="path">The path.</param>
        /// <returns>The created subnode.</returns>
        /// <exception cref="System.Exception">The final subnode has an incorrect path.</exception>
        public static ContentNode CreateSubnodeAtPath(ModelContext db, string path) {
            var segs = path.TrimStart(NODE_PATH_SEP[0]).Split(NODE_PATH_SEP[0]);
            var currentNode = db.ContentNodes().GetRoot();
            for (int i = 0; i < segs.Length; i++) {
                // Exists?
                var seg = segs[i];
                var currentPath = NODE_PATH_SEP;
                for (int ii = 0; ii <= i; ii++) currentPath += (currentPath==NODE_PATH_SEP?"":NODE_PATH_SEP)+segs[ii];
                if(currentNode.Context != null) currentNode.Include(nameof(ContentNode.Children));
                var node = currentNode.Children.SingleOrDefault(n => n.Name == seg);
                if (node == null) node = currentNode.CreateSubnode(seg);
                currentNode = node;
            }
            // Sanity
            if (currentNode.Path != path) throw new Exception("The final subnode has an incorrect path.");
            return currentNode;
        }

        public ContentNode CreateSubnode(string name) {
            // Validate type
            if (this.NodeType != NODE_TYPE_NODE) throw new Exception("Node is not a NODE_TYPE_NODE!");
            // Transform name
            name = TransformNodeName(name);
            // Validate name
            ValidateNodeName(name);
            // Name unique?
            if (this.Children.Where(n => n.Name.ToLower() == name.ToLower()).Count() > 0) throw new BackendException("node-exists",$"A node with this name ({name}) already exists.");
            // Create node
            var newNode = new ContentNode();
            newNode.Name = name;
            newNode.NodeType = NODE_TYPE_NODE;
            if (this.Path == NODE_PATH_SEP) newNode.Published = true;
            this.AddChild(newNode);
            this.Modified = DateTime.UtcNow;
            // Return
            return newNode;
        }

        public void RemoveTranslation(ContentNode node) {
            this.Children.Remove(node);
        }

        public ContentNode AddTranslation(string language = null) {
            // Fix for null and default
            if (language == "default") language = null;
            // Validate language
            ValidatePageLanguage(language);
            // Make sure the language doesnt already exist
            if (this.ChildrenForType(NODE_TYPE_TRANSLATION).Where(n => n.Value == language).Count() != 0) throw new BackendException("invalid-language","This node already contains this language.");
            // Create node
            var newNode = new ContentNode();
            newNode.Value = language;
            newNode.NodeType = NODE_TYPE_TRANSLATION;
            newNode.Name = "translation_"+(language==null?"default":language);
            this.AddChild(newNode);
            this.Modified = DateTime.UtcNow;
            // Return
            return newNode;
        }
        
        /// <summary>
        /// Gets all the supported language ids.
        /// In the future, this may be extended to allow for nodes to be constrained.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSupportedLanguagesForNode() {
            return Core.Config.LocalizationSupportedLanguages;
        }
        
        /// <summary>
        /// Gets all the supported language ids.
        /// In the future, this may be extended to allow for nodes to be constrained.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetSupportedLanguages() {
            return Core.Config.LocalizationSupportedLanguages;
        }
        
        public static string GetNodePathForEntity(ModelObject entity) {
            return $"/Entities/{entity.TypeName}/{entity.PublicId}";
        }

        public static string GetNodePathForEntityProperty(ModelObject entity, string property) {
            return $"{GetNodePathForEntity(entity)}/{property}";
        }

        public IEnumerable<ContentNode> GetContentChildren() {
            var contentNodeTypes = ContentNode.GetAllTypeMetasForContent();
            var contentNodeTypesString = contentNodeTypes.Select(nt => nt.ID).ToList();
            return this.Children.Where(n => contentNodeTypesString.Contains(n.NodeType));
        }

        /// <summary>
        /// Gets a standardized JSON for the node (including all it's children).
        /// If includeBinaryData is true, all /content/* files are automatically downloaded
        /// and included as base64 data. If rootPath is specified, then the node paths will all
        /// be relative to rootPath.
        /// </summary>
        /// <param name="includeBinaryData">if set to <c>true</c> [include binary data].</param>
        /// <param name="rootPath">The root path.</param>
        /// <param name="exludePaths">These paths will be ignored</param>
        /// <returns></returns>
        private JObject GetJSONForNode(bool includeBinaryData, string rootPath, IList<string> layouts, params string[] exludePaths) {
            // Get self
            var json = this.GetJSON(new FormBuilder(Forms.Admin.EXPORT));
            if (rootPath != null && rootPath != ContentNode.NODE_PATH_SEP) {
                var path = json["path"].ToString().ReplacePrefix(rootPath, "");
                if (path == "") path = ContentNode.NODE_PATH_SEP;
                json["path"] = path;
            }

            // Layouts
            if (layouts == null) { layouts = new List<string>(); }
            if (!string.IsNullOrEmpty(this.Layout)) { layouts.Add(this.Layout); }

            if (includeBinaryData == true) {

               
                // Options Binary content
                var optionsString = this.Options?.ToString();
                if (optionsString != null && optionsString.Contains("/content/")) {
                    var files = new List<JObject>();
                    foreach (var optionPair in this.Options.ToDictionary()) {

                        var path = optionPair.Value?.ToString();

                        if (path != null && path.StartsWith("/content/")) {
                            string binaryData = DownloadBinaryContent(path);
                            var jobject = new JObject();

                            jobject["path"] = path;
                            jobject["data"] = binaryData;

                            files.Add(jobject);
                        }
                    }
                    if (files.Any() == true) {
                        var jproperty = new JProperty("binary-options", files);
                        json.Add(jproperty);
                    }

                }

                // Value has binary /content/*
                if (this.Value != null && this.Value.StartsWith("/content")) {
                    // Create the appropriate handler and invoke it
                    var handler = Core.Handler.Handler.CreateForRoute<Handler.ContentFileHandler>(this.Value, null, Handler.Verbs.Get);
                    handler.Invoke();
                    // Read bytes and convert
                    var bytes = System.IO.File.ReadAllBytes(handler.FileOutput);
                    var binaryData = Convert.ToBase64String(bytes);
                    json.Add(new JProperty("binary-value", binaryData));
                }
                // Doesnt start with /content but has in it
                else if (this.Value != null && this.Value.Contains("/content/")) {

                    var paths = ParseContentPathsFromHTML(this.Value);
                    var files = new List<JObject>();

                    foreach (var path in paths) {
                        var binaryData = DownloadBinaryContent(path);
                        var jobject = new JObject();

                        jobject["path"] = path;
                        jobject["data"] = binaryData;

                        files.Add(jobject);
                    }

                    if (files.Any() == true) {
                        var jproperty = new JProperty("binary-values", files);
                        json.Add(jproperty);
                    }
                }
            }






            // Get children
            this.Include(nameof(ContentNode.Children));
            var jsonChildren = new List<JObject>();
            foreach (var child in this.Children.ToList()) {
                if (exludePaths != null) {
                    if (exludePaths.Contains(child.Path)) {
                        continue;
                    }
                }
                jsonChildren.Add(child.GetJSONForNode(includeBinaryData, rootPath, layouts, exludePaths));
            }
            json.Add(new JProperty("children", jsonChildren));
            return json;
        }

        public static string DownloadBinaryContent(string path) {
            // Create the appropriate handler and invoke it
            var handler = Core.Handler.Handler.CreateForRoute<Handler.ContentFileHandler>(path, null, Handler.Verbs.Get);
            handler.Invoke();
            // Read bytes and convert
            var bytes = System.IO.File.ReadAllBytes(handler.FileOutput);
            var binaryData = Convert.ToBase64String(bytes);
            return binaryData;
        }


        private static ContentNode GetNodeForJSON(JObject json, User user, bool includeBinaryData = true, string rootPath = null) {
            // Init node with json
            var node = new ContentNode();
            node.FromJSON(json, new FormBuilder(Forms.Admin.EXPORT).Exclude("Type"));

            // Update path
            if (rootPath != ContentNode.NODE_PATH_SEP) node.Path = rootPath + node.Path;


            // Import binary data
            if (includeBinaryData) {

                var binaryValues = json["binary-values"]?.Children();
                if (binaryValues != null) {

                    foreach (var file in binaryValues) {

                        // Re-upload the content...
                        // Get the filename
                        var path = file["path"]?.ToString();
                        var data = file["data"]?.ToString(); ;
                        var filename = Core.Util.Files.GetFileNameWithExtension(path);
                        ContentFile contentFile = UploadFile(user, data, filename);
                        // Re-write the value

                        // new value with replaced urls
                        var newValue = Core.Util.String.ReplaceFirstOccurance(node.Value, path, contentFile.ContentURL);
                        node.Value = newValue;

                    }
                }

                var binaryOptions = json["binary-options"]?.Children();
                var optionstring = node.Options?.ToString();
                if (binaryOptions != null && string.IsNullOrWhiteSpace(optionstring) == false) {

                    foreach (var file in binaryOptions) {

                        // Re-upload the content...
                        // Get the filename
                        var path = file["path"]?.ToString();
                        var data = file["data"]?.ToString(); ;
                        var filename = Core.Util.Files.GetFileNameWithExtension(path);
                        ContentFile contentFile = UploadFile(user, data, filename);
                        // Re-write the value

                        // new value with replaced urls
                        optionstring = Core.Util.String.ReplaceFirstOccurance(optionstring, path, contentFile.ContentURL);

                    }

                    node.Options = new Properties(optionstring);
                }




                // The value is a content path
                var binaryValue = json["binary-value"];
                if (binaryValue != null) {
                    // Re-upload the content...
                    // Get the filename
                    var filename = Core.Util.Files.GetFileNameWithExtension(node.Value);
                    ContentFile contentFile = UploadFile(user, binaryValue, filename);
                    // Re-write the value
                    node.Value = contentFile.ContentURL;
                }

            }
            // Import all children
            var jchildren = json["children"];
            foreach(var jchild in jchildren) {
                JObject jobj = jchild as JObject;
                var childNode = GetNodeForJSON(jobj, user, includeBinaryData, rootPath);
                node.Children.Add(childNode);
            }
            return node;
        }

        private static ContentFile UploadFile(User user, JToken binaryValue, string filename) {
            // Create base64 as bytes
            var bytes = Convert.FromBase64String(binaryValue.ToString());
            ContentFile contentFile = null;
            using (ModelContext db = ModelContext.GetModelContext(null)) {
                var uploadingUsing = db.Users().FirstOrDefault(u => u.Id == user.Id);
                contentFile = Core.Data.DataCenter.UploadFile(filename, bytes, nameof(ContentNode), Core.Config.FileStorageProviderName, uploadingUsing);
                db.ContentFiles().Add(contentFile);
                db.SaveChanges();
            }

            return contentFile;
        }

        public static List<ContentNodeTypeMeta> GetAllTypeMetas() {
            List<ContentNodeTypeMeta> ret = new List<ContentNodeTypeMeta>();
            foreach (var type in Core.Reflection.Types.GetMachinataTypes().Where(t => t.IsSubclassOf(typeof(ContentNodeTypeMeta)))) {
                var instance = Activator.CreateInstance(type) as ContentNodeTypeMeta;
                ret.Add(instance);
            }
            return ret;
        }

        public static List<ContentNodeTypeMeta> GetAllTypeMetasForContent() {
            return GetAllTypeMetas().Where(nt => nt.Category == ContentNodeTypeMeta.TypeCategory.Content).OrderByDescending(nt => nt.Sort).ToList();
        }

        /// <summary>
        /// Creates the content node for entity property with the given contents.
        /// The contents is a rich-text element per default, but can be set.
        /// This method will create a new content node at the proper location in the CMS.
        /// Note: this will overwrite any previously set data for the entity property.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="property">The property.</param>
        /// <param name="value">The contents.</param>
        /// <param name="nodeType">Type of the node.</param>
        /// <returns></returns>
        public static ContentNode CreateContentNodeForEntityProperty(ModelContext db, ModelObject entity, string property, string value, string nodeType = NODE_TYPE_HTML) {
            var valuesForLanguages = new Dictionary<string, string>();
            valuesForLanguages.Add("default", value);
            return CreateContentNodeForEntityProperty(db, entity, property, valuesForLanguages, nodeType);
        }

        /// <summary>
        /// Creates the content node for entity property with the given translations.
        /// The translations are provided as a dictionary of key values, where the key
        /// is the langauge id (or 'default').
        /// The contents is a rich-text element per default, but can be set.
        /// This method will create a new content node at the proper location in the CMS.
        /// Note: this will overwrite any previously set data for the entity property.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="property">The property.</param>
        /// <param name="valuesForLanguages">The values for languages.</param>
        /// <param name="nodeType">Type of the node.</param>
        /// <returns></returns>
        public static ContentNode CreateContentNodeForEntityProperty(ModelContext db, ModelObject entity, string property, Dictionary<string,string> valuesForLanguages, string nodeType = NODE_TYPE_HTML) {
            // Compile all translations
            var translations = new List<object>();
            foreach(var lang in valuesForLanguages.Keys) {
                translations.Add(
                    new {
                        type = NODE_TYPE_TRANSLATION,
                        lang = lang,
                        nodes = new[] {
                            new {
                                type = nodeType,
                                sort = 0,
                                data = valuesForLanguages[lang],
                                title = (nodeType == NODE_TYPE_TITLE ? valuesForLanguages[lang] : null)
                            }
                        }
                    });
            }
            // Create JSON object
            var json = new {
                nodepath = GetNodePathForEntityProperty(entity, property),
                published = true,
                type = NODE_TYPE_NODE,
                translations = translations
            };
            return UpdateContentUsingJSON(db,Core.JSON.Serialize(json));
        }

        public static JObject ExportContentAsJSON(ModelContext db, string path, bool includeBinaryData = true, params string [] exludePaths) {
            // Get node
            ContentNode node = db.ContentNodes().Include(nameof(ContentNode.Parent)).GetNodeByPath(path);
            var rootPath = node == db.ContentNodes().GetRoot() ? node.Path : node.Parent.Path;
            var layoutNames = new List<string>();
            var json = node.GetJSONForNode(includeBinaryData, rootPath, layoutNames, exludePaths);
            
            // Layouts
            var layouts = GetJSONForLayouts(db, layoutNames);
            if (layouts!=null && layouts.Any()) {
                json.Add(new JProperty("layouts", layouts));
            }

            return json;
        }

        private JArray GetTranslationJSONForNode(JArray jsonRoot, string sourceLanguage, string targetLanguage, bool useFallbackLanguage = false, string customName = null, bool onlyPublished = true, bool throwExceptionIfNotExists = true) {
            // Create root if not already
            // Note: all content is added to the same root element
            if(jsonRoot == null) {
                jsonRoot = new JArray();
            }

            // Init this node for self
            var jsonNode = new JObject();
            jsonNode["id"] = this.PublicId;
            jsonNode["title"] = (customName != null ? customName+this.Name : this.Name);
            jsonNode["path"] = this.Path;
            var jsonPage = new JObject();
            jsonPage["page"] = jsonNode;
            var jsonTexts = new JArray();
            
            // Find translation and get content
            this.Include(nameof(ContentNode.Children));
            var sourceTranslation = this.TranslationForLanguage(sourceLanguage);
            if (sourceTranslation != null) {
                // Validate if this is the actually matching source tranlsation
                // If fallback is disabled, we need to check that the language exactly matches,
                // or if the trans node is a default lang (null), that the requested lang is the system default
                bool skipContent = false;
                if (useFallbackLanguage == false) {
                    if (!(sourceTranslation.Value == null && sourceLanguage.ToLower() == Core.Config.LocalizationDefaultLanguage.ToLower())
                        &&
                        (sourceTranslation.Value?.ToLower() != sourceLanguage.ToLower())) {
                        // This is not the exact language we are looking for
                        skipContent = true;
                    }
                }

                if (!skipContent) {
                    sourceTranslation.Include(nameof(ContentNode.Children));
                    foreach (var child in sourceTranslation.GetContentChildren().OrderBy(n => n.Sort)) {
                        //TODO: filter only text types?
                        var jsonContent = new JObject();
                        jsonContent["sourceLanguage"] = sourceLanguage;
                        jsonContent["targetLanguage"] = targetLanguage;
                        jsonContent["source"] = child.Value;
                        jsonContent["target"] = "";

                        var jsonText = new JObject();
                        jsonText["type"] = child.NodeType;
                        jsonText["id"] = child.PublicId;
                        jsonText["content"] = jsonContent;
                        jsonTexts.Add(jsonText);
                    }
                }
            }
            
            // Register content 'texts', add only if there actually is content
            jsonPage["texts"] = jsonTexts;
            if(jsonTexts.Count > 0) jsonRoot.Add(jsonPage);

            // Do recursion on subpages
            var children = this.ChildrenForType(NODE_TYPE_NODE);
            if(onlyPublished) children = children.Where(n => n.Published == true);
            children = children.OrderBy(n => n.Sort);
            foreach (var child in children) {
                // Note: do not propogate customName
                child.GetTranslationJSONForNode(
                    jsonRoot: jsonRoot, 
                    sourceLanguage: sourceLanguage, 
                    targetLanguage: targetLanguage, 
                    useFallbackLanguage: useFallbackLanguage, 
                    customName: customName, 
                    throwExceptionIfNotExists: throwExceptionIfNotExists);
            }

            return jsonRoot;
        }

        public static JArray ExportContentAsTranslationJSON(ModelContext db, string path, string sourceLanguage, string targetLanguage, string customName = null, bool onlyPublished = true, bool throwExceptionIfNotExists = true) {
            ContentNode node = db.ContentNodes().Include(nameof(ContentNode.Parent)).GetNodeByPath(path, throwExceptionIfNotExists);
            if (node == null) return null;
            //var rootPath = node == db.ContentNodes().GetRoot() ? node.Path : node.Parent.Path;
            var json = node.GetTranslationJSONForNode(
                jsonRoot: null, 
                sourceLanguage: sourceLanguage, 
                targetLanguage: targetLanguage, 
                customName: customName, 
                onlyPublished: onlyPublished);
            return json;
        }

        private static IEnumerable<JObject> GetJSONForLayouts(ModelContext db, List<string> layoutNames) {
            if (layoutNames == null && !layoutNames.Any()) { return null; }
            var layoutNodes = new List<JObject>() ;
            var layouts = db.ContentLayouts().Where(cl => layoutNames.Contains(cl.Name)).ToList();
            foreach(var layout in layouts) {
                var json = layout.GetJSON(new FormBuilder(Forms.Admin.EXPORT));
                layoutNodes.Add(json);
            }

            return layoutNodes;
        }

        public static ContentNode ImportContentFromJSON(ModelContext db, JObject json, string atPath, User user, bool includeBinaryData = true) {
            // Get root node
            var rootNode = db.ContentNodes().Include(nameof(ContentNode.Children)).GetNodeByPath(atPath);

            var nodeName = json["name"].ToString();
            // Validate that the page name is free
            rootNode.CheckChildnameFree(nodeName);

            // Create node
            ContentNode node = GetNodeForJSON(json,user, includeBinaryData, atPath);

           
            // Root import
            if (node.Path == NODE_PATH_SEP) {
                foreach (var child in node.Children) {

                    // TODO: merge with existing Templates
                    if (child.Name != "Templates" || rootNode.HasChildWithName("Templates") == false) {
                        rootNode.CheckChildnameFree(child.Name);
                        rootNode.AddChild(child);
                    } else {
                        var templateNodeRoot = rootNode.Children.Single(c => c.Name == "Templates");
                        templateNodeRoot.Include(nameof(ContentNode.Children));
                        foreach(var importedTemplate in child.Children) {
                            if (templateNodeRoot.HasChildWithName(importedTemplate.Name)) {
                                // skip if we have a template with same name on destination 
                            } else {
                                templateNodeRoot.AddChild(importedTemplate);
                            }
                        }

                    }
                }
            } else {
                   rootNode.Children.Add(node);
            }

            // ContentLayouts
            if (json["layouts"] != null) {
                var layouts = GetLayoutsForJSON(json["layouts"]);
                foreach (var layout in layouts) {
                    var existing = db.ContentLayouts().FirstOrDefault(cl => cl.Name == layout.Name);
                    if (existing == null) {
                        // Add new layout
                        db.ContentLayouts().Add(layout);
                    } else {
                        // Add new option
                        if (layout.Options != null && layout.Options.Keys.Any()) {
                            foreach (var optionKey in layout.Options.Keys) {
                                if (!existing.Options.Keys.Contains(optionKey)) {
                                    existing.Options[optionKey] = layout.Options[optionKey];
                                }
                            }
                        }
                    }
                }
            }

            return node;
        }

        private static IEnumerable<ContentLayout> GetLayoutsForJSON(JToken layoutRoot) {
            foreach (JObject layoutNode in layoutRoot.Children()) {
                var node = new ContentLayout();
                node.FromJSON(layoutNode, new FormBuilder(Forms.Admin.EXPORT).Exclude("Type"));
                yield return node;
            }
        }

        public void CheckChildnameFree(string nodeName) {
            if (HasChildWithName(nodeName)) {
                throw new BackendException("page-already-exists", $"A page with the same name ('{nodeName}') already exists here.");
            }
        }

        private bool HasChildWithName(string nodeName) {
            return this.Children.Any(n => n.Name == nodeName);
        }

        public MetaInformation GetMetaInformation() {
            var meta = new MetaInformation();
            meta.LoadFromNode(this);
            return meta;
        }

        /// <summary>
        /// Updates the content using json data structure. This routine will automatically get the correct
        /// node by id or by path (nodeid, nodepath) and manage all translations and sub nodes automatically.
        /// If a node id is not specified, but a node path is, the node will automatically be created at that
        /// path. If a node id is specified, the node must exist.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="jsonString">The json string.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// No node is specified by id or by path. If an id is not specified, then a path must be.
        /// or
        /// Node is not a NODE_TYPE_NODE!
        /// or
        /// Translation node is not a NODE_TYPE_TRANSLATION!
        /// </exception>
        public static ContentNode UpdateContentUsingJSON(ModelContext db, string jsonString) {
            #region Example JSON
            /*{
                "nodeid":"4nj_RQ",
                "type":"node",
                "translations":[
                    {
                        "nodeid":"z6bQIQ",
                        "type":"trans",
                        "lang":"default",
                        "nodes":[
                        {
                            "nodeid":"OKprEw",
                            "type":"html",
                            "data":"%3Ch1%3Ea%3C%2Fh1%3E",
                            "sort":0
                        },
                        {
                            "nodeid":"ZdBMDw",
                            "type":"html",
                            "data":"%3Ch1%3Eb%3C%2Fh1%3E%3Cp%3Esdfasfasfd%20%3C%2Fp%3E%3Cp%3Easdfsafd%20%3C%2Fp%3E",
                            "sort":1
                        },
                        {
                            "nodeid":"Qh6gag",
                            "type":"file",
                            "data":"",
                            "sort":2
                        },
                        {
                            "nodeid":"r0WFRg",
                            "type":"image",
                            "data":"",
                            "sort":3
                        },
                        {
                            "nodeid":"jIPmIg",
                            "type":"html",
                            "data":"sdasdf%3Cbr%3Easdf%3Cbr%3E",
                            "sort":4
                        }
                        ]
                    },
                    {
                        "nodeid":"-knZZA",
                        "type":"trans",
                        "lang":"en",
                        "nodes":[
                        {
                            "nodeid":"ItIIbw",
                            "type":"html",
                            "data":"this%20is%20english%3Cbr%3E",
                            "sort":0
                        },
                        {
                            "nodeid":"DxhsSw",
                            "type":"image",
                            "data":"",
                            "sort":1
                        }
                        ]
                    },
                    {
                        "nodeid":"J3c6QA",
                        "type":"trans",
                        "lang":"de",
                        "nodes":[
                        {
                            "nodeid":"KusqJg",
                            "type":"html",
                            "data":"%3Ch1%3Ethis%20is%20germain!!%3C%2Fh1%3E%3Cp%3E%20%3C%2Fp%3E",
                            "sort":0
                        }
                        ]
                    },
                    {
                        "nodeid":"tsuHfg",
                        "type":"trans",
                        "lang":"fr",
                        "nodes":[
                        {
                            "nodeid":"joThGQ",
                            "type":"file",
                            "data":"",
                            "sort":0
                        },
                        {
                            "nodeid":"68LCdQ",
                            "type":"image",
                            "data":"",
                            "sort":1
                        },
                        {
                            "nodeid":"yAgmUQ",
                            "type":"html",
                            "data":"%3Ch2%3Easdf%3C%2Fh2%3E%3Ch1%3Easdfasdfasdf%20%3C%2Fh1%3E%3Col%3E%3Cli%3Easdf%20%3C%2Fli%3E%3Cli%3Easdf%20%3C%2Fli%3E%3Cli%3Easdf%20%3C%2Fli%3E%3Cli%3Easdfasfd%20%3C%2Fli%3E%3C%2Fol%3E",
                            "sort":2
                        }
                        ]
                    }
                ]
            }*/
            #endregion
            // Unpack json
            var json = Core.JSON.ParseJsonAsJObject(jsonString);
            // Get node
            var nodeId = json.GetValue("nodeid")?.ToString();
            var nodePath = json.GetValue("nodepath").ToString();
            var nodeDelete = json.GetValue("delete")?.ToString();
            var nodeMeta = new MetaInformation();
            string nodeTitle = null;
            string nodeSummary = null;
            string nodeShortURL = null;
            var contentOptions = new Dictionary<string, string>();
            ContentNode node = null;
            var contentItems = 0;
            // Get the node...
            if(string.IsNullOrEmpty(nodeId)) {
                // No id, must be specified by path
                if(string.IsNullOrEmpty(nodePath)) {
                    throw new Exception("No node is specified by id or by path. If an id is not specified, then a path must be.");
                }
                node = db.ContentNodes().Include(nameof(ContentNode.Children)).GetNodeByPath(nodePath, false);
                // New node?
                if(node == null) {
                    // Create it!
                    node = ContentNode.CreateSubnodeAtPath(db, nodePath);
                }
            } else {
                // We have a id
                node = db.ContentNodes().Include(nameof(ContentNode.Children)).GetByPublicId(nodeId);
            }
            // Sanity
            if(node == null) {
                throw new Exception($"Node could not be loaded for some reason (id={nodeId}, path={nodePath})");
            }
            // Validate type
            if (node.NodeType != NODE_TYPE_NODE) throw new Exception("Node is not a NODE_TYPE_NODE!");
            // Save published et cetera...
            if (json.GetValue("published")?.ToString() == "true") node.Published = true;
            if (json.GetValue("published")?.ToString() == "false") node.Published = false;
            if (!string.IsNullOrEmpty(json.GetValue("layout")?.ToString())) node.Layout = json.GetValue("layout").ToString();
            else node.Layout = null;
            // Options
            node.Options.Clear();
            JToken options = null;
            if (json.TryGetValue(("options"), out options)) {
                foreach (JProperty jsonOption in options) {
                    node.Options[jsonOption.Name] = jsonOption.Value;
                }
            }
            // Interate translations
            foreach(JObject jsonTranslation in (JArray)json.GetValue("translations")) {
                // Init
                var translationItems = 0;
                var translationNodeId = jsonTranslation.GetValue("nodeid")?.ToString();
                var translationType = jsonTranslation.GetValue("type").ToString();
                var translationLang = jsonTranslation.GetValue("lang").ToString();
                var translationDelete = jsonTranslation.GetValue("delete")?.ToString();
                var translationOptions = new Dictionary<string, string>();
                string translationSummary = "";
                var translationMeta = new MetaInformation();
                // Validate type
                if(translationType != NODE_TYPE_TRANSLATION) throw new Exception("Translation node is not a NODE_TYPE_TRANSLATION!");
                // Load translation node
                ContentNode translationNode = node.GetExactTranslationForLanguage(translationLang);
                if(translationNode == null) {
                    // No translation - must be a new translation!
                    translationNode = node.AddTranslation(translationLang);
                } else {
                    // Make sure children are loaded
                    translationNode.Include(nameof(ContentNode.Children));
                }
                // Set default short url from the parent node name
                translationNode.ShortURL = Core.Util.String.CreateShortURLForName(node.Name);
                // Remove all children
                foreach(var contentNode in translationNode.GetContentChildren().ToList()) db.ContentNodes().Remove(contentNode);
                // Delete?
                if(translationDelete == "true" || nodeDelete == "true") {
                    db.ContentNodes().Remove(translationNode);
                    // Skip rest
                    continue;
                }
                // Interate content node
                foreach (JObject jsonContent in (JArray)jsonTranslation.GetValue("nodes")) {
                    // Init
                    //var contentNodeId = jsonContent.GetValue("nodeid").ToString();
                    var contentType = jsonContent.GetValue("type").ToString();
                    var contentData = jsonContent.GetValue("data").ToString();
                    var contentNodeOptions = jsonContent.GetValue("options")?.ToString();
                    var contentSort = int.Parse(jsonContent.GetValue("sort").ToString());
                    var contentSummary = jsonContent.GetValue("summary")?.ToString();
                    var contentTitle = jsonContent.GetValue("title")?.ToString();
                    var contentShortURL = jsonContent.GetValue("shorturl")?.ToString();
                    // Book keeping
                    //if(!string.IsNullOrEmpty(contentData)) {
                    contentItems++;
                    translationItems++;
                    //}
                    // Get converted data
                    var contentTypeMeta = ContentNode.GetAllTypeMetas().Where(nt => nt.ID == contentType).SingleOrDefault();
                    var convertedNodeValue = contentTypeMeta.ConvertToDBValue(contentData);
                    // Create new node
                    ContentNode newNode = new ContentNode();
                    newNode.NodeType = contentType;
                    newNode.Value = convertedNodeValue;
                    newNode.Sort = contentSort;
                    if (!string.IsNullOrEmpty(contentTitle)) newNode.Title = contentTitle;
                    translationNode.AddChild(newNode);
                    // Set node options
                    if(!string.IsNullOrEmpty(contentNodeOptions)) {
                        var contentNodeOptionsJSON = Core.JSON.ParseJsonAsJObject(contentNodeOptions);
                        foreach(var keypair in contentNodeOptionsJSON) {
                            newNode.Options[keypair.Key] = keypair.Value;
                        }
                    }
                    // Register summary
                    if(!string.IsNullOrEmpty(contentSummary)) {
                        translationSummary += contentSummary + (translationSummary!=""?"\n":"");
                    }
                    // Register title
                    if (!string.IsNullOrEmpty(contentTitle)) translationNode.Title = contentTitle; // each title overwrites the last
                    // Register short url                    
                    if (!string.IsNullOrEmpty(contentShortURL)) translationNode.ShortURL = contentShortURL;
                    // Register option?
                    if (contentType == ContentNode.NODE_TYPE_OPTION && !string.IsNullOrEmpty(convertedNodeValue)) {
                        var sepPlaceholder = "{option-seperator}";
                        var formattedOption = convertedNodeValue
                            .Replace(" = ", sepPlaceholder)
                            .Replace("=", sepPlaceholder)
                            .Replace(": ", sepPlaceholder)
                            .Trim();
                        var splitIndex = formattedOption.IndexOf(sepPlaceholder);
                        if (splitIndex < 0) {
                            throw new BackendException("option-error", "Please define a value for the option");
                        }
                        var optKey = formattedOption.Substring(0, splitIndex);
                        optKey = optKey.ToDashedLower();
                        var optVal = formattedOption.Substring(splitIndex + sepPlaceholder.Length);
                        if (contentOptions.ContainsKey(optKey)) contentOptions[optKey] = optVal;
                        else contentOptions.Add(optKey, optVal);
                        if (translationOptions.ContainsKey(optKey)) translationOptions[optKey] = optVal;
                        else translationOptions.Add(optKey, optVal);
                    }
                    // Register meta
                    if(contentType == ContentNode.NODE_TYPE_META) {
                        nodeMeta.LoadFromNode(newNode);
                        translationMeta.LoadFromNode(newNode);
                    }
                }
                // Set summary
                if(!string.IsNullOrEmpty(translationSummary.Trim())) {
                    translationNode.Summary = translationSummary.Trim();
                } else {
                    translationNode.Summary = null;
                }
                if (translationLang == null || translationLang == "default") {
                    nodeSummary = translationNode.Summary;
                    nodeShortURL = translationNode.ShortURL;
                    nodeTitle = translationNode.Title;
                }
                // Update options with content options
                foreach(var optKey in translationOptions.Keys) {
                    translationNode.Options[optKey] = translationOptions[optKey];
                }
                // Update meta
                translationMeta.LoadIntoNode(translationNode,true);
                // Mark as modified
                translationNode.Modified = DateTime.UtcNow;
                if (translationItems == 0) node.RemoveTranslation(translationNode);
            }
            // Set summary
            node.Summary = nodeSummary;
            if(node.Summary == null) {
                // Fallback
                node.Summary = node.Children.FirstOrDefault(n => n.Summary != null)?.Summary;
            }
            // Set title
            node.Title = nodeTitle;
            if(node.Title == null) {
                // Fallback
                node.Title = node.Children.FirstOrDefault(n => n.Title != null)?.Title;
            }
            // Set short url from content node in a translation
            node.ShortURL = nodeShortURL;
            if(node.ShortURL == null) {
                // Fallback to any tranlsation short url
                node.ShortURL = node.Children.FirstOrDefault(n => n.ShortURL != null)?.ShortURL;
            }
            if(node.ShortURL == null) {
                // Fallback to a default generated from the name
                node.ShortURL = Core.Util.String.CreateShortURLForName(node.Name);
            }
            // Mark as modified
            node.Modified = DateTime.UtcNow;
            // Update options with content options
            foreach(var optKey in contentOptions.Keys) {
                //if (!node.Options.Keys.Contains(optKey)) node.Options[optKey] = contentOptions[optKey];
                node.Options[optKey] = contentOptions[optKey];
            }
            // Merge meta information
            nodeMeta.LoadIntoNode(node,true);
            // Delete?
            if(nodeDelete == "true") {
                db.ContentNodes().Remove(node);
            }
            // Return
            return node;
        }


        public ContentNode Duplicate(ModelContext db, string path, bool checkDestinationExists = true) {

            // Check if destination path available           
            if (checkDestinationExists && Exists(db, path)) {
                throw new BackendException("duplicate-error", "Cannot duplicate with this path, because a Node with this path already exists");
            }

            LoadSiblings();

            // New node
            var node = ContentNode.CreateSubnodeAtPath(db, path);

            // Copy Meta and Data
            node.Layout = this.Layout;
            node.NodeType = this.NodeType;
            node.Options = this.Options;
            node.Published = false;
            node.Sort = this.Parent.Children.Select(c => c.Sort).Max() + 1;
            node.Summary = this.Summary;
            node.Title = this.Title;
            node.Value = this.Value;
            node.ShortURL = this.ShortURL;

            // Children
            this.DuplicateChildren(node);

            return node;
        }

        public void LoadSiblings() {
            // Load siblings
            this.Include(nameof(this.Parent));
            if (this.Parent != null) {
                this.Parent.Include(nameof(ContentNode.Children));
            }
        }

        /// <summary>
        /// Duplicates the children of this ContentNode to another ContentNode
        /// </summary>
        /// <param name="newParent">The parent of the new children.</param>
        public void DuplicateChildren(ContentNode newParent) {
            // todo: how load all before in one reuqest?
            this.Include(nameof(this.Children));
            foreach (var child in this.Children) {
                var node = new ContentNode();
                node.Created = DateTime.UtcNow;
                node.Layout = child.Layout;
                node.NodeType = child.NodeType;
                node.Options = child.Options;
                node.Published = child.Published;
                node.Sort = child.Sort;
                node.Name = child.Name;
                node.Summary = child.Summary;
                node.Title = child.Title;
                node.ShortURL = child.ShortURL;
                node.Value = child.Value;
                node.Path = newParent.Path + "/" + child.Name;
                newParent.Children.Add(node);
                child.DuplicateChildren(node);
            }
        }

        public static bool Exists(ModelContext db, string path) {
            return GetByPath(db, path) != null;
        }

        public static ContentNode GetByPath( ModelContext db, string path) {
            return db.ContentNodes().SingleOrDefault(cn => cn.Path == path);
        }

        /// Gets the title for a translation. If no node or translation is found it will return null.
        public static string GetTranslatedTitleForPath(ModelContext db, string language, string cmsPath) {
            var node = ContentNode.GetByPath(db, cmsPath);
            if (node != null) {
                node.IncludeContent();
                var emailNodeTranslation = node.TranslationForLanguage(language);
                var title = emailNodeTranslation?.Title;
                return title;
            }
            return null;
        }
        
        public static string DownloadFileNodeByPathAndTitle(ModelContext db, string path, string title) {
            // Get node
            var node = db.ContentNodes()
                .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .GetNodeByPath(path);
            var fileNode = node.TranslationForLanguage(null)
                .ChildrenForType(ContentNode.NODE_TYPE_FILE)
                .ToList()
                .FirstOrDefault(e => e.Options["title"]?.ToString() == title);
            if (fileNode == null) throw new BackendException("invalid-data", $"No file could be found at {path} with title '{title}'.");
        
            // Download file
            string filePath = null;
            string mutexKey = fileNode.Value.Replace("/", "_").Replace(Core.Config.PathSep, "_").Replace(":", "_");
            if (mutexKey.Length > 260) {
                mutexKey = mutexKey.Substring(mutexKey.Length - 260, 260);
            }
            // Get a mutex on the cachepath derived mutexkey to make sure two threads dont try to
            // create the cache at the same time
            var mut = new System.Threading.Mutex(false, mutexKey);
            try {   
                // Wait until it is safe to enter.
                mut.WaitOne();
                filePath = Core.Data.DataCenter.DownloadFile(db, fileNode.Value);
            } finally {
                // Release the Mutex.
                mut.ReleaseMutex();
            }

            return filePath;
        }


        public static IEnumerable<string> DownloadFileNodesByPath(ModelContext db, string path, out ContentNode page) {
            // Get node
            var node = db.ContentNodes()
                .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .GetNodeByPath(path);

            var paths = new List<string>();

            var translation = node.TranslationForLanguage(null);
            if (translation == null) {

                page = null;
                return paths;
            }

            var fileNodes = translation
                .ChildrenForType(ContentNode.NODE_TYPE_FILE)
                .ToList();

            page = node;

            foreach (var fileNode in fileNodes) {

                // Download file
                string filePath = null;
                string mutexKey = fileNode.Value.Replace("/", "_").Replace(Core.Config.PathSep, "_").Replace(":", "_");
                if (mutexKey.Length > 260) {
                    mutexKey = mutexKey.Substring(mutexKey.Length - 260, 260);
                }
                // Get a mutex on the cachepath derived mutexkey to make sure two threads dont try to
                // create the cache at the same time
                var mut = new System.Threading.Mutex(false, mutexKey);
                try {
                    // Wait until it is safe to enter.
                    mut.WaitOne();
                    filePath = Core.Data.DataCenter.DownloadFile(db, fileNode.Value);
                } finally {
                    // Release the Mutex.
                    mut.ReleaseMutex();
                }

                paths.Add(filePath);
            }

            return paths;

        }

        public static IEnumerable<string> DownloadFileNodesByPath(ModelContext db, string path) {
            ContentNode page;
            return DownloadFileNodesByPath(db, path, out page);
        }

        /// <summary>
        /// Parses paths starting with '/content/' from a html snippet
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static IEnumerable<string> ParseContentPathsFromHTML(string html) {
            var startPattern = "\"/content/";
            var index = 0;
            var paths = new List<string>();
            if (string.IsNullOrWhiteSpace(html) == true) {
                return paths;
            }
            while (index < html.Length - startPattern.Length) {
                var startPath = html.IndexOf(startPattern, index);
                if (startPath < 0) {
                    break;
                }
                var endPath = html.IndexOf("\"", startPath + startPattern.Length);
                var path = html.Substring(startPath + 1, endPath - startPath - 1);
                index = endPath + 1;
                paths.Add(path);
            }
            return paths;
        }

        //public static IEnumerable<string> ParseContentPaths(string content) {
        //    var startPattern = "/content/";
        //    var index = 0;
        //    var paths = new List<string>();
        //    if (string.IsNullOrWhiteSpace(content) == true) {
        //        return paths;
        //    }
        //    while (index < content.Length - startPattern.Length) {
        //        var startPath = content.IndexOf(startPattern, index);
        //        if (startPath < 0) {
        //            break;
        //        }
        //        var endPath = content.IndexOf("\"", startPath + startPattern.Length);
        //        var path = content.Substring(startPath + 1, endPath - startPath - 1);
        //        index = endPath + 1;
        //        paths.Add(path);
        //    }
        //    return paths;
        //}

        #endregion

        #region Private Methods ////////////////////////////////////////////////////////////////

        private static void UpdateChildrenPaths(ContentNode node) {
            if (node.Context != null) {
                node.Include(nameof(node.Children));
            }
            foreach (var child in node.Children) {
                child.Path = node.Path + "/" + child.Name;
                // Mark as modified
                child.Modified = DateTime.UtcNow;
                UpdateChildrenPaths(child);
            }
        }

        #endregion

        #region Conversion Methods ////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// For ContentNodes, if the node is a NODE_TYPE_NODE or NODE_TYPE_TRANSLATION
        /// it will automatically return the summary.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() {
            if (this.NodeType == NODE_TYPE_NODE || this.NodeType == NODE_TYPE_TRANSLATION) {
                if (this.Summary != null) return this.Summary;
                else return "";
            } else {
                return base.ToString();
            }
            /*
            if (this.NodeType == NODE_TYPE_NODE) {
                if(this.Context != null ) this.Include(nameof(ContentNode.Children));
                var translation = this.Children.FirstOrDefault();
                if (translation != null) return translation.ToString();
                return "";
            } else if (this.NodeType == NODE_TYPE_TRANSLATION) {
                if (this.Summary != null) return this.Summary;
                else return "";
            } else {
                return base.ToString();
            }*/
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            {
                // Root node
                var rootNode = new ContentNode();
                rootNode.Name = ContentNode.ROOT_NODE_NAME;
                rootNode.Path = ContentNode.NODE_PATH_SEP;
                rootNode.Parent = null;
                rootNode.NodeType = NODE_TYPE_NODE;
                context.Set<ContentNode>().Add(rootNode);

                // Templates
                var templateNode = new ContentNode();
                templateNode.Name = ContentNode.TEMPLATES_NODE_NAME;
                templateNode.Published = true;
                templateNode.NodeType = NODE_TYPE_NODE;
                rootNode.AddChild(templateNode);

            }
           

        }

        #endregion
        
        #region Public Virtual Methods ////////////////////////////////////////////////////

        public override void OnDelete(ModelContext db) {
            // Do base OnDelete
            base.OnDelete(db);
            // Delete all children
            foreach(var child in this.Children.ToList()) {
                db.DeleteEntity(child);
            }
        }
        
        public override AuditLog GetAuditLog(Handler.Handler handler, System.Data.Entity.Infrastructure.DbEntityEntry entry) {
            if(this.NodeType == NODE_TYPE_NODE || this.NodeType == NODE_TYPE_TRANSLATION) {
                var audit = base.GetAuditLog(handler, entry);
                return audit;
            } else {
                return null;
            }
        }

        #endregion
    }

    public class ContentNodePreview : ModelObject {

        [FormBuilder]
        public string Language { get; set; }

        [FormBuilder]
        public string RoutePath { get; set; }
        
        [FormBuilder]
        public string NodePath { get; set; }

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextContentNodeExtenions {
        public static DbSet<ContentNode> ContentNodes(this Core.Model.ModelContext context) {
            return context.Set<ContentNode>();
        }

        public static T GetRoot<T>(this IQueryable<T> query) where T : Core.Model.ContentNode {
            T ret = null;
            try {
                ret = query.Where(e => e.Parent == null && e.Path == ContentNode.ROOT_NODE_PATH).SingleOrDefault();
            }catch(Exception e) {
                throw new BackendException("root-node-corrupt","It seems the root node has become corrupted. Please contact the sytem administrator.",e);
            }
            if (ret == null) throw new BackendException("root-node-missing","The root node is missing. Please reset the CMS.");
            return ret;
        }

        public static T GetNodeByPath<T>(this IQueryable<T> query, string path, bool throwExceptions = true) where T : Core.Model.ContentNode {
            if (!path.StartsWith(ContentNode.NODE_PATH_SEP)) path = ContentNode.NODE_PATH_SEP + path;
            var ret = query.Where(e => e.Path == path).SingleOrDefault();
            if (ret == null && throwExceptions) throw new Backend404Exception("node-not-found",$"The node at the path '{path}' does not exist.");
            return ret;
        }

        public static IEnumerable<ContentNode> GetPagesAtPath<T>(this IQueryable<T> query, string path) where T : Core.Model.ContentNode {
            var parent = query.GetNodeByPath(path, true);
            parent.Include(nameof(ContentNode.Children));
            return parent.Children.Where(n => n.NodeType == ContentNode.NODE_TYPE_NODE);
        }

        public static ContentNode GetPageAtPathForShortURL<T>(this IQueryable<T> query, string path, string shortURL) where T : Core.Model.ContentNode {
            var pages = GetPagesAtPath(query.Include(nameof(ContentNode.Children)+"."+nameof(ContentNode.Children)), path);
            return pages.Where(n => n.Children.Any(t => t.ShortURL == shortURL)).SingleOrDefault();
        }
    }

    #endregion
}
