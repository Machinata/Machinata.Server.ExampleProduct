using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class SeedLog : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public SeedLog() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string SeedType { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Entity { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Dataset { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public int Inserts { get; set; }
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

      
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
       
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
       
        public static SeedLog CreateSeedLog(ModelContext context, string type, string entity, string dataset, int inserts) {
            var log = new SeedLog();
            log.Entity = entity;
            log.Dataset = dataset;
            log.SeedType = type;
            log.Inserts = inserts;
            context.SeedLogs().Add(log);
            return log;
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSeedLogExtenions {
        public static DbSet<SeedLog> SeedLogs(this Core.Model.ModelContext context) {
            return context.Set<SeedLog>();
        }
    }

    #endregion
}
