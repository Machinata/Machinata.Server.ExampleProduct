using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.ComponentModel;
using Machinata.Core.Exceptions;
using System.Web;
using Machinata.Core.Util;

namespace Machinata.Core.Model {
    public delegate void BusinessNameChanged(Business business, string oldName);
    public delegate void BusinessCreated(Business business, Core.Handler.Handler handler);

    [Serializable()]
    [ModelClass]
    public partial class Business : ModelObject , IComparable<Business> {

      
        public static event BusinessNameChanged BusinessNameChangedEvent;
        public static event BusinessCreated BusinessCreatedEvent;

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Business() {
            this.Users = new List<User>();
            this.Addresses = new List<Address>();
            this.Settings = new Properties();
        }

        #endregion

        #region Constants ////////////////////////////////////////////////////////////////////////

        public const string SETTING_KEY_BANK = "BankName";
        public const string SETTING_KEY_KONTO_NUMMER = "BankAccountNumber";
        public const string SETTING_KEY_IBAN = "BankIBAN";
        public const string SETTING_KEY_SWIFT = "BankSwift";
        public const string SETTING_KEY_MWST_NUMMER = "VATNumber";

        public const string SETTING_KEY_PAYMENT = "Payment";
        public const string SETTING_VALUE_PAYMENT_AFTER = "after";
        public const string SETTING_VALUE_PAYMENT_BEFORE = "before";

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string SerialId { get; set; }

        [Column]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [MinLength(3)]
        [MaxLength(100)]
        public string FormalName { get; set; }

        [Column]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

    
        /// <summary>
        /// Is Owner/Operator of Machinata (e.g. Paul&Lulu = true)
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is owner; otherwise, <c>false</c>.
        /// </value>
        [Column]
        [DefaultValue(false)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool IsOwner { get; set; }

        [Column]
        [FormBuilder(Forms.API.LISTING)]
        [DefaultValue(false)]
        public bool Archived { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<Address> Addresses { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Address DefaultShipmentAddress { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Address DefaultBillingAddress { get; set; }


        [Column]
        public ICollection<User> Users { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [PropertiesKey(SETTING_KEY_BANK)]
        [PropertiesKey(SETTING_KEY_KONTO_NUMMER)]
        [PropertiesKey(SETTING_KEY_IBAN)]
        [PropertiesKey(SETTING_KEY_SWIFT)]
        [PropertiesKey(SETTING_KEY_MWST_NUMMER)]
        [PropertiesKey(SETTING_KEY_PAYMENT)]
        [PropertiesConfigKey("CoreBusinessSettingsCustomKeys")]
        public Properties Settings { get; set; }

     

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder(Forms.API.LISTING)]
        public string ShortURL {
            get {
                return Core.Util.String.CreateShortURLForCamelCased(this.Name);
            }
        }

        public override string ToString() {
            return this.FormalName;
        }
        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        public int CompareTo(Business other) {
            return this.Name.CompareTo(other.Name);
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Gets the default shipment address. If none -> newest address
        /// </summary>
        /// <returns></returns>
        public Address GetShipmentAddressForBusiness() {
            if (Context != null) {
                LoadFirstLevelNavigationReferences();
            }
            if (this.DefaultShipmentAddress!= null) {
                return DefaultShipmentAddress;
            }
            return Addresses.Where(a => !a.Archived).OrderByDescending(a => a.Id).FirstOrDefault();
        }

        /// <summary>
        /// Gets the default billing address. If none -> newest address
        /// </summary>
        /// <returns></returns>
        public Address GetBillingAddressForBusiness() {
            if (Context != null) {
                LoadFirstLevelNavigationReferences();
            }
            if (this.DefaultBillingAddress != null) {
                return DefaultBillingAddress;
            }
            return Addresses.Where(a=>!a.Archived).OrderByDescending(a => a.Id).FirstOrDefault();
        }

        public static Business GetBusinessByName(ModelContext context, string name) {
            return context.Businesses().FirstOrDefault(b => b.Name == name);
        }

        public void AddUser(User user) {
            this.Include(nameof(this.Users));
            if (this.Users.Contains(user)) throw new BackendException("already-has-user", "The business already has this user.");
            this.Users.Add(user);
        }

        public void RemoveUser(User user) {
            this.Include(nameof(this.Users));
            if (!this.Users.Contains(user)) throw new BackendException("no-user", "The business does not have this user.");
            this.Users.Remove(user);
        }

        public IEnumerable<Address> ActiveAddresses
        {
            get
            {
                if (this.Context != null) {
                    this.Include(nameof(Addresses));
                }
                return Addresses.Where(a => !a.Archived);
            }
        }

        /// <summary>
        /// Trigger the BusinessNameChagedEvent
        /// </summary>
        /// <param name="oldName">The old name.</param>
        public void NameChanged(string oldName) {
            Business.BusinessNameChangedEvent?.Invoke(this, oldName);
        }



        /// <summary>
        /// Trigger the BusinessCreatedEvent
        /// </summary>
       public static void BusinessCreated(Business business, Core.Handler.Handler handler) {
            Business.BusinessCreatedEvent?.Invoke(business, handler);
        }


        /// <summary>
        /// Gets the business for the user. If the user is attached to multiple businesses,
        /// Note: this method will always return a business. If there is no business, then the method fails.
        /// </summary>
        /// <value>
        /// The business.
        /// </value>
        /// <exception cref="Backend401Exception">login-required;You must first login to continue.</exception>
        /// <exception cref="Backend403Exception">no-business;It seems your account is not connected to any business.</exception>
        public static Core.Model.Business GetBusinessForHandler(Core.Handler.Handler handler, string arn, string cookieName, bool throwException, bool addAdminUserToBusiness = false)
        {
            // Make sure we have a user
            var user = handler.User;
            if (user == null)
            {
                if (throwException == true)
                {
                    throw new Backend401Exception("login-required", "You must first login to continue.", arn);
                }
                return null;
            }
            // Get the business selected at login
            user.Include(nameof(User.Businesses));
            // Get cookie
            var businessId = handler.Context.Request.Cookies[cookieName]?.Value;
            var business = user.Businesses.SingleOrDefault(e => e.PublicId == businessId);

            // Add the user to the business if it is an admin and not already connected
            if (addAdminUserToBusiness == true && business == null && user.IsAdminOrSuperUser == true) {
               business = handler.DB.Businesses().GetByPublicId(businessId, throwExceptions: false);
                if (business != null) {
                    user.Businesses.Add(business);
                    handler.DB.SaveChanges();
                }
            }

            if (business == null){
                if (throwException == true)
                {
                    throw new Backend403Exception("no-business", $"It seems your account ({user.Username}) is not connected to any business.", arn);
                }
                else
                {
                    return null;
                }
            }
            return business;
        }


        public static Business CreateBusiness(Handler.Handler handler) {
            var business = new Business() { Settings = new Properties() };
            business.Name = handler.Params.String("businessname");
            business.FormalName = handler.Params.String("legalname");
            business.Settings[Business.SETTING_KEY_PAYMENT] = Business.SETTING_VALUE_PAYMENT_AFTER;
            // Create the main address
            var address = new Address();
            address.Populate(handler, new FormBuilder(Forms.Admin.CREATE));
            address.Name = handler.Params.String("contactname");
            address.Company = business.FormalName;
            address.Validate();
            // Register the address
            business.Addresses.Add(address);
            // Validate and save
            business.Validate();
            handler.DB.Businesses().Add(business);

            // Save
            handler.DB.SaveChanges();


            // Set address defaults
            business.DefaultBillingAddress = address;
            business.DefaultShipmentAddress = address;

            // Inform subscribers
            //Business.BusinessCreated(business, this);
            Business.BusinessCreated(business, handler);

            return business;
        }

        #endregion

        #region Public Virtual Methods /////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
        }

        public override int TypeNumber { get { return 10; } }

        public override Cards.CardBuilder VisualCard() {

            var location = this.GetBillingAddressForBusiness()?.Location;
            return new Cards.CardBuilder(this)
                .Title(this.Name != null ? Core.Util.String.CreateSummarizedText(this.Name, 40, true) : null)
                .Subtitle(this.SerialId)
                .Sub(this.FormalName != null ? Core.Util.String.CreateSummarizedText(this.FormalName, 60, true) : null)
                .Sub(location)
                .Icon("business-card")
                .Wide();
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextBusinessExtenions {
        public static DbSet<Business> Businesses(this Core.Model.ModelContext context) {
            return context.Set<Business>();
        }

        /// <summary>
        /// Active Customer businesses
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static IQueryable<Business> CustomerBusinesses(this Core.Model.ModelContext context) {
            var ownerBusiness = context.OwnerBusiness();
            return context.Set<Business>().Where(b => !b.Archived && b.Id != ownerBusiness.Id );
        }


        //public static T GetByUsername<T>(this IQueryable<T> query, string username, bool throwException = true) where T : Core.Model.User {
        //    var usernameHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(username);
        //    var ret = query.Where(e => e.UsernameHash == usernameHash).SingleOrDefault();
        //    if (ret == null && throwException) throw new BackendException("username-invalid", "The username does not exist.");
        //    return ret;
        //}

        /// <summary>
        /// Returns all businesses which are not deleted/archived
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static IQueryable<Business> ActiveBusinesses(this Core.Model.ModelContext context) {
            return context.Set<Business>().Where(b => !b.Archived);
        }

        public static Business OwnerBusiness(this Core.Model.ModelContext context, bool throwException = true) {
            var ownerBusinesses = context.Businesses().Where(b => b.IsOwner == true);
            if (ownerBusinesses.Count() == 0 && throwException) {
                throw new Exception("No IsOwner business has been defined.");
            }
            if (ownerBusinesses.Count() > 1 && throwException) {
                throw new Exception($"Multiple IsOwner businesses have been defined: {string.Join(",",ownerBusinesses.Select(b=>b.Name))}");
            }
            return ownerBusinesses.FirstOrDefault();
        }

        /// <summary>
        /// Businesses with payment after method
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static IList<Business> PayAfterBusinesses(this Core.Model.ModelContext context) {
            var businesses = context.Businesses().ToList();
            var businessAfter = new List<Business>();
            foreach (var business in businesses) {
                if (business.Settings != null && business.Settings.Keys.Contains(Business.SETTING_KEY_PAYMENT) && business.Settings[Business.SETTING_KEY_PAYMENT].ToString() == Business.SETTING_VALUE_PAYMENT_AFTER) {
                    businessAfter.Add(business);
                }
            }
            return businessAfter;
        }
    }

    #endregion
}
