using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Messaging;
using Machinata.Core.Handler;
using System.Data.Entity.Infrastructure;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class SystemMessage : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public SystemMessage() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder]
        public string ReceiverEncrypted { get; set; }

        [Column]
        [FormBuilder]
        public string ReceiverHash { get; set; }

        [Column]
        [FormBuilder]
        public string SenderEncrypted { get; set; }

        [Column]
        [FormBuilder]
        public string SenderHash { get; set; }


        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Service { get; set; }

        [Column]
        [FormBuilder]
        
       // [FormBuilder(Forms.Admin.VIEW)]
        public string Content { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public DateTime Sent
        {
            get { return Created; }
        }

        /// <summary>
        /// Receiver of message, e.g. Push, SMS, Email
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string Receiver
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.ReceiverEncrypted);
            }
            set
            {
                this.ReceiverHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.ReceiverEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string Sender {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.SenderEncrypted);
            }
            set
            {
                this.SenderHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.SenderEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }


        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string Subject
        {
            get
            {
                if (this.Service == nameof(EmailSender)) {
                    var content = Core.JSON.ParseJsonAsJObject(this.Content, true);
                    if (content != null && content["subject"] != null) {
                        return content["subject"].ToString();
                    }
                }
                return null;
            }

        }


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////

        public override AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
            return null;
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Gets the messages by receiver, calculates hash then makes query to db
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="receiver">Email, SMS, etc..</param>
        /// <returns></returns>
        public static IQueryable<SystemMessage> GetMessagesForReceiver(ModelContext db, string receiver) {
            var receiverHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(receiver);
            return db.SystemMessages().Where(m => m.ReceiverHash == receiverHash);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSystemMessageExtenions {
        public static DbSet<SystemMessage> SystemMessages(this Core.Model.ModelContext context) {
            return context.Set<SystemMessage>();
        }
    }

    #endregion
}
