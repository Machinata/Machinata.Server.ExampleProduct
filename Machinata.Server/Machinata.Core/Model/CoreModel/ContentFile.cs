using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.ComponentModel;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class ContentFile : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums //////////////////////////////////////////////////////////////////////////////

        public enum ContentCategory : short {
            Image = 10,
            Text = 20,
            Video = 30,
            Audio = 40,
            Other = 1000
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ContentFile() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string FileName { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string FileExtension { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public ContentCategory FileCategory { get; set; }

        [Column]
        [FormBuilder]
        public string StorageKey { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [DefaultValue("file")]
        public string StorageProvider { get; set; }

        /// <summary>
        /// Gets or sets the source where the file came from.
        /// This is important, because it allows for tracking of files to 
        /// different modules or classes which keeps the store more organized.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Source { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public User User { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string FullFileName {
            get
            {
                return $"{FileName}.{FileExtension}";
            }
        }

        public string FullFileNameURISafe
        {
            get
            {
                return Core.Util.HTTP.UrlPathEncode(FullFileName);
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string ContentURL
        {
            get
            {
                return $"/content/{this.FileCategory.ToString().ToLower()}/{PublicId}/{FullFileNameURISafe}";
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContexContentFileExtenions {
        public static DbSet<ContentFile> ContentFiles(this Core.Model.ModelContext context) {
            return context.Set<ContentFile>();
        }
    }

    #endregion
}
