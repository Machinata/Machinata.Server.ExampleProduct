using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace Machinata.Core.Model {

    [Serializable()]
    [ModelClass]
    public partial class ContentLayout : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants

        

        #endregion
     

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ContentLayout() {
            this.Options = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        [MinLength(2)]
        [MaxLength(40)]
        [Required]
        public string Name { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string CSSClass { get; set; }
                   
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string TemplateName { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        [DataType(DataType.ImageUrl)]
        public string PreviewImage { get; set; }
        

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.EXPORT)]
        public Properties Options { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////

        public string GetTitle() {
            if (!string.IsNullOrWhiteSpace(this.Name)) {
                return this.Name;
            }
            return this.PublicId;
        }



        #endregion


        #region Private Methods ////////////////////////////////////////////////////////////////


        #endregion



        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            
        }

        #endregion

        #region Public Virtual Methods ////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);
            
            if(this.CSSClass == null) {
                this.CSSClass = "layout-"+ this.Name.ToLower().Replace(" ","-");
            }
            if(this.TemplateName == null) {
                this.TemplateName = "layout."+ this.Name.ToLower().Replace(" ","-");
            }
        }

        #endregion
    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextContentLayoutExtenions {
        public static DbSet<ContentLayout> ContentLayouts(this Core.Model.ModelContext context) {
            return context.Set<ContentLayout>();
        }

    }

    #endregion
}
