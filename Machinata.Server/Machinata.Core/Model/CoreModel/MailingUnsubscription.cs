using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.ComponentModel;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using Machinata.Core.Messaging.Providers;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class MailingUnsubscription : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public MailingUnsubscription() {
        }

        #endregion
            
        #region Constants ////////////////////////////////////////////////////////////////////////

        public enum Targets : short{
            User = 10,
            Business = 20,
            Email = 40
        }

        public enum Categories : short {
            // Public Categories
            Shop = 10,
            Ticket = 20,
            Mailing = 30,
            Finance = 40,
         
            Account = 50,
            Transfer = 60,
            Projects = 70,

            // Testing/ Generic
            Testing = 100,
            Generic = 200,

            // System
            HardBounce = 1000


        }

        public enum Action {
            Confirm,
            Unsubscribe
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Identifier { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public Targets Target { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Categories Category { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        /// * possible
        public string SubCategory { get; set; }



     

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        public User GetUser(ModelContext db) {
            if (this.Target != Targets.User) {
                return null;
            }
            var user = db.Users().GetByUsername(this.Identifier, false);
            if (user == null) {
                user = db.Users().GetByPublicId(this.Identifier);
            }
            return user;
        }

        public Business GetBusiness(ModelContext db) {
            if (this.Target != Targets.Business) {
                return null;
            }
            return db.Businesses().GetByPublicId(this.Identifier);
        }

        /// <summary>
        /// Gets the name of the recepient. Username if User, Business name if business, otherwise email address
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public string GetRecepientName(ModelContext db) {
            var unsubscriber = this.Identifier;
            if (this.Target == MailingUnsubscription.Targets.Business) {
                unsubscriber = this.GetBusiness(db).Name;
            } else if (this.Target == MailingUnsubscription.Targets.User) {
                unsubscriber = this.GetUser(db).Name;
            }
            return unsubscriber;
        }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string SubCategoryName
        {
            get
            {
                return UnsubscribeService.GetSubCategoryName(this.Category, this.SubCategory);
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Public Virtual Methods /////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
        }



        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextMailingUnsubscriptionExtenions {
        public static DbSet<MailingUnsubscription> MailingUnsubscriptions(this Core.Model.ModelContext context) {
            return context.Set<MailingUnsubscription>();
        }

    }

    #endregion
}
