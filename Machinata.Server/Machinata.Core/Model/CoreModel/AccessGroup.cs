using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class AccessGroup : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public AccessGroup() {
            Users = new List<User>();
            AccessPolicies = new List<AccessPolicy>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        [Index(IsUnique = true)]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name { get; set; } 
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [InverseProperty("AccessGroups")]
        [FormBuilder(Forms.Admin.VIEW)]
        public ICollection<User> Users { get; set; }

        [Column]
        [InverseProperty("AccessGroups")]
        [FormBuilder(Forms.Admin.VIEW)]
        public ICollection<AccessPolicy> AccessPolicies { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        public const string GROUP_ADMIN_READWRITE = "Read/Write Administrators";
        public const string GROUP_ADMIN_READONLY = "Read-Only Administrators";

        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            {
                var group = new AccessGroup { Name =  GROUP_ADMIN_READWRITE};
                context.Set<AccessGroup>().Add(group);
            }
            {
                var group = new AccessGroup { Name = GROUP_ADMIN_READONLY };
                context.Set<AccessGroup>().Add(group);
            }
        }


        [OnModelSeedConnections]
        private static void OnModelSeedConnections(ModelContext context, string dataset) {
            AutomaticallyAddStandardPoliciesToGroups(context);
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Automatically adds the standard policies to groups.
        /// Currently this does the following:
        /// Policies /admin/* >> Group "Admin Read/Write" and "Admin Read Only"
        /// </summary>
        /// <param name="db">The database.</param>
        public static void AutomaticallyAddStandardPoliciesToGroups(ModelContext db) {
            // Get all admin policies
            var adminPolicies = db.AccessPolicies().Where(ap => ap.ARN.StartsWith("/admin/") || ap.ARN == "/admin").ToList();
            var readwritePolicy = db.AccessPolicies().SingleOrDefault(ap => ap.ARN == AccessPolicy.WRITE_ARN && ap.Access == AccessPolicy.AccessType.Allow);
            var readonlyPolicy = db.AccessPolicies().SingleOrDefault(ap => ap.ARN == AccessPolicy.WRITE_ARN && ap.Access == AccessPolicy.AccessType.Deny);
            // Bind policies to groups
            {
                // Read write group
                var group = db.AccessGroups().Include(nameof(AccessGroup.AccessPolicies)).SingleOrDefault(ag => ag.Name == GROUP_ADMIN_READWRITE);
                if(!group.AccessPolicies.Contains(readwritePolicy)) group.AccessPolicies.Add(readwritePolicy);
                foreach(var p in adminPolicies) {
                    if(!group.AccessPolicies.Contains(p)) group.AccessPolicies.Add(p);
                }
            }
            {
                // Read only group
                var group = db.AccessGroups().Include(nameof(AccessGroup.AccessPolicies)).SingleOrDefault(ag => ag.Name == GROUP_ADMIN_READONLY);
                if(!group.AccessPolicies.Contains(readonlyPolicy)) group.AccessPolicies.Add(readonlyPolicy);
                foreach (var p in adminPolicies) {
                    if(!group.AccessPolicies.Contains(p)) group.AccessPolicies.Add(p);
                }
            }
        }
        
        public void AddPolicy(AccessPolicy policy) {
            if (this.AccessPolicies.Contains(policy)) throw new BackendException("already-has-policy","The group already has this policy.");
            this.AccessPolicies.Add(policy);
        }

        public void RemovePolicy(AccessPolicy policy) {
            if (!this.AccessPolicies.Contains(policy)) throw new BackendException("no-policy","The group does not have this policy.");
            this.AccessPolicies.Remove(policy);
        }

        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAccessGroupExtenions {
        public static DbSet<AccessGroup> AccessGroups(this Core.Model.ModelContext context) {
            return context.Set<AccessGroup>();
        }
    }

    #endregion
}
