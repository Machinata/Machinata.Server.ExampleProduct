using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using System.Web;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass]
    public partial class User : ModelObject, IEnabledModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Events ////////////////////////////////////////////////////////////////////////////

        // User properties changed
        public delegate void PropertiesChangedDelegate(User user);
        public static event PropertiesChangedDelegate PropertiesChanged;

        // Authorization changed
        public delegate void AuthorizationChangedDelegate(User user);
        public static event AuthorizationChangedDelegate AuthorizationChanged;

        // User added (currently only fired in /api/admin/businesses/business/{publicId}/users/add-new)
        public delegate void UserAddedDelegate(User user);
        public static event UserAddedDelegate UserAddedEvent;

        // User deleted
        public delegate void UserDeletedDelegate(User user);
        public static event UserDeletedDelegate UserDeletedEvent;

        #endregion

        #region Constants //////////////////////////////////////////////////////////////////////

        public const string SETTING_KEY_IP = "IP";

        public const string PASSWORD_RESET_CODE_KEY = "PASSWORD_RESET_CODE";
        public const string PASSWORD_RESET_CODE_EXPIRATION_KEY = "PASSWORD_RESET_CODE_EXPIRATION";
        public const string USER_LANGUAGE_KEY = "USER_LANGUAGE";
        public const string USER_ACTIVATION_DATE_KEY = "UserActivationDate";
        public const string USER_UNIVERSAL_LOGIN_ENDPOINT_KEY = "UniversalLoginEndpoint";
        public const string USER_UNIVERSAL_LOGIN_SYNCKEY_KEY = "UniversalLoginSyncKey";
        public const string USER_LAST_LOGIN_BUILDVERSION = "LastBuildVersion";
        public const string USER_LAST_LOGIN_BUILDTIME = "LastBuildTime";    

        public const string USERNAME_SUPERUSER = "superuser";

      

        public const string USER_NAME_SYSTEM = "system";
        public const string USERNAME_SYSTEM = USER_NAME_SYSTEM + "@nerves.ch";

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public User() {
            this.AccessGroups = new List<AccessGroup>();
            this.AccessPolicies = new List<AccessPolicy>();
            this.AuthTokens = new List<AuthToken>();
            this.Settings = new Properties();
            this.Enabled = false;
        }

        public override void Validate() {
            base.Validate();

            // Check username
            using(var db = Core.Model.ModelContext.GetModelContext(null)) {
                if (db.Users().Any(u=>u.UsernameHash == this.UsernameHash && this.Id != u.Id)) {
                    throw new BackendException("username-taken", "This username has been taken");
                }
            }
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [MinLength(3)]
        [MaxLength(200)]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Name { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Enabled { get; set; } 
        
        [Column]
        [Required]
        public string UsernameEncrypted { get; set; }

        [Column]
        [Required]
        public string UsernameHash { get; set; }

        [Column]
        [FormBuilder]
        public string PasswordHash { get; set; } 
        
        [Column]
        [FormBuilder]
        public string EmailEncrypted { get; set; } 

        [Column]
        [FormBuilder]
        public string EmailHash { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [DataType(DataType.ImageUrl)]
        public string ProfileImage { get; set; } 
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
       // [FormBuilder(Forms.Admin.EDIT)]
        //[PropertiesKey(SETTING_KEY_IP)]
        public Properties Settings { get; set; } 

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        [InverseProperty("User")]
        public ICollection<AuthToken> AuthTokens { get; set; }
        
        [Column]
        [InverseProperty("User")]
        public ICollection<APIKey> APIKeys { get; set; }

        [Column]
        [InverseProperty("Users")]
        public ICollection<AccessGroup> AccessGroups { get; set; }
        
        [Column]
        [InverseProperty("Users")]
        public ICollection<AccessPolicy> AccessPolicies { get; set; }
        
        [Column]
        [InverseProperty("Users")]
        public ICollection<Business> Businesses { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [MinLength(3)]
        [MaxLength(80)]
        [Placeholder("user@domain.com")]
        [DataType(DataType.EmailAddress)]
        [NotMapped]
        public string Email {
            get {
                return Core.Encryption.DefaultEncryption.DecryptString(this.EmailEncrypted);
            }
            set {
                this.EmailHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.EmailEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
                if(Core.Config.AccountUsernameUsesEmail) {
                    this.Username = value;
                }
            }
        }


        [FormBuilder()]
        [MinLength(3)]
        [MaxLength(100)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [NotMapped]
        public string Username
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.UsernameEncrypted);
            }
            set
            {
                this.UsernameHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.UsernameEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }


        [NotMapped]
        [FormBuilder(Forms.Admin.CREATE)]
        [MinLength(6)]
        [MaxLength(30)]
        [DataType(DataType.Password)]
        public string Password {
            set {
                this.PasswordHash = ComputePasswordHash(value);
            }
        }

        /// <summary>
        /// Gets all access policies for the user through the policies
        /// which are directly attached to the user (User.AccessPolicies) 
        /// or through policies which are indirectly attached through groups
        /// (User.AccessGroups.AccessPolicies).
        /// Note: This method will automatically load the policies and groups
        /// from the context if needed.
        /// </summary>
        /// <value>
        /// All access policies.
        /// </value>
        public IEnumerable<AccessPolicy> AllAccessPolicies {
            get {
                // Make sure policies are loaded
                this.Include<User>("AccessPolicies").Include<User>("AccessGroups");
                var ret = new List<AccessPolicy>();
                // Add own policies
                ret.AddRange(this.AccessPolicies);
                // Add all group policies   
                foreach(var group in this.AccessGroups) {
                    group.Include("AccessPolicies");
                    ret.AddRange(group.AccessPolicies);
                }
                // Return sorted by highest priority
                return ret.OrderByDescending(p => p.Priority);
            }
        }

        [NotMapped]
        public string Gravatar {
            get {
                if (this.ProfileImage != null) {
                    return this.ProfileImage;
                } else {
                    using (System.Security.Cryptography.MD5 md5Hash = System.Security.Cryptography.MD5.Create()) {
                        // See https://en.gravatar.com/site/implement/hash/
                        var gravatarHash = this.Email.ToLower();
                        var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(gravatarHash));
                        var sBuilder = new StringBuilder();
                        for (var i = 0; i < data.Length; i++) {
                            sBuilder.Append(data[i].ToString("x2"));
                        }
                        gravatarHash = sBuilder.ToString();
                        // See https://en.gravatar.com/site/implement/images/
                        return "https://www.gravatar.com/avatar/" + gravatarHash;
                    }
                }
            }
        }

        public bool IsSuperUser {
            get {
                return this.AllAccessPolicies.Any(ap => ap.ARN == AccessPolicy.SUPERUSER_ARN);
            }
        }

        /// <summary>
        /// Whether this user is an admin user.
        /// IMPORTANT: superuser is not included
        /// </summary>
        public bool IsAdminUser {
            get {
                return this.AllAccessPolicies.Any(ap => ap.ARN.StartsWith(AccessPolicy.ADMIN_ARN.TrimEnd('*'), StringComparison.Ordinal));
            }
        }

        public bool IsAdminOrSuperUser
        {
            get
            {
                return this.IsAdminUser || this.IsSuperUser;
            }
        }

        public string Language {
            get {
                if (this.Settings != null && this.Settings.Keys.Contains(USER_LANGUAGE_KEY)) {
                    return this.Settings[USER_LANGUAGE_KEY]?.ToString();
                }
                return null;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        [OnModelSeed]
        private static void OnModelSeed(ModelContext context, string dataset) {
            // Super (root) user
            { 
                User user = new User();
                user.Id = 1;
                user.Name = USERNAME_SUPERUSER;
                user.Email = USERNAME_SUPERUSER+"@nerves.ch";
                user.Username = USERNAME_SUPERUSER;
                user.Password = Core.Config.GetStringSetting("SecuritySuperuserInitialPassword").Replace("{guid}", Guid.NewGuid().ToString()); // Password is random to protect the system as soon as it is seeded
                user.Enabled = true;
                // Super user policy
                AccessPolicy superuser = new AccessPolicy();
                superuser.Name = USERNAME_SUPERUSER.ToSentence();
                superuser.ARN = AccessPolicy.SUPERUSER_ARN;
                superuser.Access = AccessPolicy.AccessType.Allow;
                user.AccessPolicies.Add(superuser);
                // Attach to database
                _logger.Info("Creating super user '" + user.Username + "' with superuser policy...");
                context.Set<User>().Add(user);
            }
            // System user
            { 
                User user = new User();
                user.Id = 3;
                user.Name = USER_NAME_SYSTEM;
                user.Email = USERNAME_SYSTEM;
                user.Username = USERNAME_SYSTEM;
                user.Password = Guid.NewGuid().ToString(); // Password is random to protect the system as soon as it is seeded
                user.Enabled = false;
                // Attach to database
                _logger.Info("Creating system user '" + user.Username + "'...");
                context.Set<User>().Add(user);
            }
        }

        [OnModelSeedConnections]
        private static void OnModelSeedConnections(ModelContext context, string dataset) {
            // Send reset password emails
            {
                var superuser = context.Users().GetByUsername(USERNAME_SUPERUSER, false);
                if (superuser != null) superuser.SendUserActivationEmail(context);
            }
        }

      
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        private string SetPasswordResetInfos(int expirationHours) {
            string resetCode = Core.Encryption.DefaultHasher.HashString(Guid.NewGuid().ToString());
            this.Settings[User.PASSWORD_RESET_CODE_EXPIRATION_KEY] = DateTime.UtcNow.AddHours(expirationHours);
            this.Settings[User.PASSWORD_RESET_CODE_KEY] = resetCode;
            return resetCode;
        }

        private void ResetPasswordResetInfos() {
            this.Settings[User.PASSWORD_RESET_CODE_EXPIRATION_KEY] = null;
            this.Settings[User.PASSWORD_RESET_CODE_KEY] = null;
        }

        /// <summary>
        /// Sends the activation email after it created and saved the activation code
        /// </summary>s
        /// <param name="db">The database.</param>
        public void SendUserActivationEmail(ModelContext db) {

            var activationCode = this.SetPasswordResetInfos(Core.Config.AccountActivationExpirationHours);
            db.SaveChanges();

            var template = new EmailTemplate(db,"account/user-activation");
            template.Subject = Core.Localization.Text.GetTranslatedTextById("account-activation");
            template.InsertVariable("activation-path", Core.Config.AccountNewUserActivationPage);
            template.InsertVariable("activation-code", activationCode);
            template.InsertVariable("secret-code", this.GetSecretCode());
            template.InsertVariable("username", this.Username);
            template.InsertVariable("expiration-hours", Core.Config.AccountActivationExpirationHours);

            // CMS Content
            template.InsertContent(
                variableName: "cms-content",
                cmsPath: "/General/AccountActivation/EmailText",
                throw404IfNotExists: false, db: db
            );
                     
            template.SendUserEmail(this, this.Email, MailingUnsubscription.Categories.Account, "account-activiation");

          
        }

        /// <summary>
        /// Sends the activation email after it created and saved the activation code
        /// </summary>s
        /// <param name="db">The database.</param>
        public void SendMessage(ModelContext db, string message, string subject = null) {
            
            var template = new EmailTemplate(db, "account/message");

            if (string.IsNullOrEmpty(subject) == true) {
                template.Subject = Core.Localization.Text.GetTranslatedTextById("message");
            } else {
                template.Subject = subject;
            }

            template.InsertVariable("username", this.Username);
            // Content
            template.InsertVariable("message", message);
            template.SendUserEmail(this, this.Email, MailingUnsubscription.Categories.Account, "message");


        }


        /// <summary>
        /// Sends the password reset email after it created and saved the reset code
        /// </summary>
        /// <param name="db">The database.</param>
        public void SendPasswordResetEmail(ModelContext db) {

            // Reset only possible if already activated/enabled
            if (!this.Enabled) {
                throw new BackendException("user-invalid", "The user account no longer active or has not been activated yet. If this is your first login, please activate your account first via the activation email you should have received.");
            }

            // Universal login user?
            if(User.IsUniversalLoginUser(this.Username)) {
                var endpoint = User.GetUniversalLoginEndpointForUser(this.Username);
                throw new LocalizedException(null, "cannot-reset-universal-login-user", this.Username, endpoint);
            }

            var resetCode = this.SetPasswordResetInfos(Core.Config.AccountPasswordResetExpirationHours);
            db.SaveChanges();

            var template = new EmailTemplate(db, "account/password-reset");
            template.Subject = Core.Localization.Text.GetTranslatedTextById("password-reset");
            template.InsertVariable("reset-password-path",Localization.Text.GetTranslatedRoute(Core.Config.AccountPasswordResetPage,this.Language));
            template.InsertVariable("activation-code", resetCode);
            template.InsertVariable("secret-code", this.GetSecretCode());
            template.InsertVariable("username", this.Username);
            template.InsertVariable("expiration-hours", Core.Config.AccountPasswordResetExpirationHours);

            // CMS Content
            template.InsertContent(
                variableName: "cms-content",
                cmsPath: "/General/PasswordReset/EmailText",
                throw404IfNotExists: false, db: db
            );

            template.SendUserEmail(this, this.Email, MailingUnsubscription.Categories.Account, "account-activation");
        }

        /// <summary>
        /// Send an email to admin about the new user and the selected acces groups
        /// </summary>
        /// <param name="newUser"></param>
        public void SendAdminNotificationEmail(User actingUser) {
            var message = new StringBuilder();
            message.AppendLine($"User '{this.Username}' ('{this.Name}') has been created by '{actingUser.Username}'");

            // Groups
            message.AppendLine("");
            message.AppendLine("AccessGroups: ");

            if (this.AccessGroups.Any()) {
                foreach (var group in this.AccessGroups) {
                    message.AppendLine($"{group.Name}");
                }
            } else {
                message.AppendLine("no group selected");
            }

            Core.Messaging.MessageCenter.SendMessageToAdminEmail("User Created", message.ToString(), "Machinata.Core");
        }

        /// <summary>
        /// Computes the password hash for this user.
        /// The password hash is made up of the following hashed string:
        ///     userSalt + globalSalt1 + password + globalSalt2
        /// where the userSalt is the user's database id.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">User does not yet have a Id.</exception>
        public string ComputePasswordHash(string password) {
            if (this.Id <= 0) throw new Exception("User does not yet have a Id.");
            var userSalt = this.Id.ToString();
            var globalSalt1 = Core.Config.EncryptionHashSalt1;
            var globalSalt2 = Core.Config.EncryptionHashSalt2;
            var stringToHash = userSalt + globalSalt1 + password + globalSalt2;
            return Core.Encryption.DefaultHasher.HashString(stringToHash);
        }

        /// <summary>
        /// Verifies that the specified password matches the one for the account.
        /// Use this method for login.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <returns></returns>
        /// <exception cref="BackendException">password-invalid;The password is not correct.</exception>
        public bool VerifyPassword(string password, bool throwException = true) {
            var ret = this.PasswordHash == this.ComputePasswordHash(password);
            if(ret == false && throwException) throw new BackendException("password-invalid", "The password is not correct.");
            return ret;
        }

        /// <summary>
        /// Validates that the password matches the requirements.
        /// Use this for creation or password changing...
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <returns></returns>
        /// <exception cref="BackendException">password-invalid</exception>
        public static bool ValidatePassword(string password, bool throwException = true) {
            // Init
            bool valid = true;
            var errors = new StringBuilder();
            if (password == null) password = "";
            
            // Empty
            if(string.IsNullOrEmpty(password)) {
                valid = false;
                errors.AppendLine("Password cannot be empty.");
            }
            // Length
            if(password.Length < 6) {
                valid = false;
                errors.AppendLine($"Password must be at least {6.ToString()} characters long.");
            }
            //TODO: other validations...

            // Return
            if(valid == false && throwException) throw new BackendException("password-invalid", errors.ToString().Trim());
            return valid;
        }

        /// <summary>
        /// Changes the password to newPassword and disables all auth tokens (except for currentAuthToken) linked to this user.
        /// This method will also validate the new password using User.ValidatePassword.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="currentAuthToken">The current authentication token.</param>
        public void ChangePassword(ModelContext db, string newPassword, AuthToken currentAuthToken = null) {
            // Set new password
            User.ValidatePassword(newPassword);
            this.Password = newPassword;

            // Reset auth tokens (this prevents logged in sessions from continuiing)
            ResetAuthTokensAndPasswordResetData(db, currentAuthToken);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }

        public void ChangeUsername(ModelContext db, string newUsername, AuthToken currentAuthToken = null)
        {
            // Set new password
            this.Username = newUsername;
            this.Validate();

            // Reset auth tokens (this prevents logged in sessions from continuiing)
            ResetAuthTokensAndPasswordResetData(db, currentAuthToken);

            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }


        public AuthToken CreateAuthToken(HttpContext context) {
            int expiresInDays = 30; // default one month
            return CreateAuthToken(context, new TimeSpan(expiresInDays, 0, 0, 0));
        }

        public AuthToken CreateAuthToken(HttpContext context, TimeSpan expiresIn, string hash = null) {
            AuthToken token = new AuthToken(this, context, expiresIn, hash);
            this.AuthTokens.Add(token);
            return token;
        }

        public string GetSecretCode() {
            return Core.Encryption.DefaultHasher.SaltAndHashString(GetKeyForSecretCode());
        }

        private string GetKeyForSecretCode() {
            return this.PublicId + "_" + this.Id;
        }

        public void CheckSecretCode(string code) {
            if (this.GetSecretCode() != code) {
                throw new BackendException("access-code-invalid", "The access code is invalid.");
            }
        }

        public void CheckResetCode(string resetCode, string secretCode) {
            this.CheckSecretCode(secretCode);
            this.CheckResetCode(resetCode);
        }

        private void CheckResetCode(string resetCode) {
            if (string.IsNullOrEmpty(resetCode) || this.GetResetCode() != resetCode || DateTime.UtcNow > GetResetCodeExpiration()) {
                throw new BackendException("reset-code-invalid", "The reset code is invalid.");
            }
        }

        public DateTime GetResetCodeExpiration() {
            DateTime date = DateTime.MinValue;
            if (this.Settings != null && this.Settings.Keys.Contains(User.PASSWORD_RESET_CODE_EXPIRATION_KEY)) {
                if (DateTime.TryParse(this.Settings[User.PASSWORD_RESET_CODE_EXPIRATION_KEY].ToString(), out date)) {
                    return date;
                }
            }
            return DateTime.MinValue;
        }

        private string GetResetCode() {
            if (this.Settings != null && this.Settings.Keys.Contains(User.PASSWORD_RESET_CODE_KEY)) {
                if (!string.IsNullOrEmpty(this.Settings[User.PASSWORD_RESET_CODE_KEY]?.ToString())) {
                    return this.Settings[User.PASSWORD_RESET_CODE_KEY].ToString();
                }
            }
            return null;
        }
        
        public void AddToAccessGroup(AccessGroup group) {
            if (this.AccessGroups.Contains(group)) throw new BackendException("already-in-group","The user is already in this group.");
            this.AccessGroups.Add(group);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }

        public void RemoveFromAccessGroup(AccessGroup group) {
            if (!this.AccessGroups.Contains(group)) throw new BackendException("not-in-group","The user is not in this group.");
            this.AccessGroups.Remove(group);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }

        public void AddPolicy(AccessPolicy policy) {
            if (this.AccessPolicies.Contains(policy)) throw new BackendException("already-has-policy","The user already has this policy.");
            this.AccessPolicies.Add(policy);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }

        public void RemovePolicy(AccessPolicy policy) {
            if (!this.AccessPolicies.Contains(policy)) throw new BackendException("no-policy","The user does not have this policy.");
            this.AccessPolicies.Remove(policy);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }
      
        public List<string> GetPolicyList() {
            return this.AllAccessPolicies.Select(ap => ap.ClassId).ToList();
        }

        /// <summary>
        /// Determines whether the user has a matching ALLOW arn for the given arn.
        /// Note: If a user has a deny ARN, then this method will return false or throw an exception.
        /// </summary>
        /// <param name="arn">The arn.</param>
        /// <param name="throwExceptions">if set to <c>true</c> [throw exceptions].</param>
        /// <returns></returns>
        /// <exception cref="Backend403Exception">
        /// authorization-required
        /// or
        /// authorization-required
        /// </exception>
        public bool HasMatchingARN(string arn, bool throwExceptions = false) {
            var policies = this.AllAccessPolicies;
            foreach(var policy in policies) {
                //_logger.Trace($"    Checking policy '{policy.Name}' with ARN '{policy.ARN}'");
                bool matches = policy.MatchesARN(arn);
                // Note: deny has priority
                if(matches && policy.Access == AccessPolicy.AccessType.Deny) {
                   // _logger.Trace($"    User has matching deny-policy (ARN '{policy.ARN}' matches '{arn}'), deny access");
                    if (throwExceptions) throw new Backend403Exception("authorization-required", $"You do not have sufficient privelages to access this page (ARN={arn}, policy denied). Please contact your administrator.", arn);
                    else return false;
                } else if(matches && policy.Access == AccessPolicy.AccessType.Allow) {
                    //_logger.Trace($"    User has matching allow-policy (ARN '{policy.ARN}' matches '{arn}'), allowing access");
                    return true;
                } 
            }
            //_logger.Trace("No policy was found, denying access");
            if (throwExceptions) throw new Backend403Exception("authorization-required", $"You do not have sufficient privelages to access this page (ARN={arn}). Please contact your administrator.", arn);
            return false;
        }


        public void ResetAuthTokensAndPasswordResetData(ModelContext db, AuthToken currentAuthToken = null) {
            // Disable all other auth tokens
            var tokensToDisable = db.AuthTokens().Where(at => at.UserId == this.Id); // all user auth tokens
            if (currentAuthToken != null) tokensToDisable = tokensToDisable.Where(at => at.Id != currentAuthToken.Id); // exclude current auth token
            tokensToDisable.ToList().ForEach(at => at.Valid = false);

            // Disable all password reset data
            this.ResetPasswordResetInfos();
        }

        public void DeactivateUser(ModelContext db) {

            // Mark as disabled and remove all auth tokens validity
            this.Enabled = false;
            ResetAuthTokensAndPasswordResetData(db);
            
            // Inform subscribers
            AuthorizationChanged?.Invoke(this);
        }

        public static string GetUniversalLoginEndpointForUser(string username) {
            if (!Core.Config.UniversalLoginEnabled) return null;

            // Parse out the username domain
            string usernameDomain = null;
            if(username.Contains("@")) {
                usernameDomain = username.Split('@').Last();
            }

            // See if the username domain matches a universal login domain...
            if (usernameDomain != null) {
                foreach (var domainConfig in Core.Config.UniversalLoginDomains) {
                    // domainConfig is in format nerves.ch=https://nerves.ch
                    var domain = domainConfig.Split('=').First();
                    var endpoint = domainConfig.Split('=').Last();

                    // What if universal login is enabled on the machine but we are also registered as the proivder?
                    if(endpoint == Core.Config.PublicURL) {
                        // Do not accept this user as a universal login, since the endpoint IS the local machine
                        continue;
                    }

                    // Matches?
                    if(domain == usernameDomain) {
                        return endpoint; // Universal login user
                    }
                }
            }

            // No match
            return null;
        }

        /// <summary>
        /// Determines whether a username is registered in Core.Config.UniversalLoginDomains as a universal login user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public static bool IsUniversalLoginUser(string username) {
            var endpoint = GetUniversalLoginEndpointForUser(username);
            if (endpoint == null) return false;
            else return true;
        }

        /// <summary>
        /// Logins the user with username and password.
        /// This method automatically uses a universal login method if the user is a universal login user.
        /// For all other users, this method will use local login.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="context">The context.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="duration">The duration.</param>
        /// <returns></returns>
        public static AuthToken LoginUserWithUsernameAndPassword(ModelContext db, HttpContext context, string username, string password, string duration = null) {

            // Universal login enabled?
            if (Core.Config.UniversalLoginEnabled) {
                // Universal login user? (ie domain matches those registered in Core.Config.UniversalLoginDomains
                if (User.IsUniversalLoginUser(username)) {
                    // Pass on to universal login function
                    var endpoint = GetUniversalLoginEndpointForUser(username);
                    return LoginUserWithUsernameAndPasswordOnUniversalLoginEndpoint(db, context, username, password, duration, endpoint);
                }
            }

            // Login locally
            return LoginUserWithUsernameAndPasswordOnLocalMachine(db, context, username, password, duration);
        }

        public static AuthToken LoginUserWithUsernameAndPasswordOnUniversalLoginEndpoint(ModelContext db, HttpContext context, string username, string password, string duration, string endpoint) {
            
            var ip = Core.Util.HTTP.GetRemoteIP(context);
            var useragent = Core.Util.HTTP.GetUserAgent(context);


            // Figure out the synckey - this is used later to verify the authenticity of sync requests
            string synckey = null;
            User userForSyncKey = db.Users().GetByUsername(username, false);
            if(userForSyncKey != null) {
                synckey = userForSyncKey.Settings[USER_UNIVERSAL_LOGIN_SYNCKEY_KEY] as string;
            }
            if(synckey == null || synckey == "") synckey = AuthToken.GenerateHash()+AuthToken.GenerateHash(); // default, if none exists yet


            // At this point we have a username, password and universal login endpoint
            // First thing is to query the endpoint if the login is valid

            var apiCall = new API.APICall("/api/universal-login/login", endpoint);
            apiCall.WithParameter("username", username);
            apiCall.WithParameter("password", password);
            apiCall.WithParameter("duration", duration);
            apiCall.WithParameter("synckey", synckey);
            apiCall.WithParameter("ip", ip);
            apiCall.WithParameter("useragent", useragent);
            apiCall.WithParameter("source", Core.Config.ServerURL);
            try {
                apiCall.Send();
            } catch(BackendException be) {
                throw be;// important!
            } catch(System.Net.WebException we) {
                throw we;// important!
            } catch(Exception e) {
                throw e;// important!
            }
            var response = apiCall.JSON();

            // If we are here - that means that we successfully got the endpoint to log us in...

            // Validate response

            // Our hash and duration to use from provider
            var localTokenHash = response["data"]["auth-token"]["hash"].ToString();
            var localDuration = response["data"]["auth-token"]["duration"].ToString();

            // Does the user exist?
            User user = db.Users().GetByUsername(username, false);
            if(user == null) {

                // User does not exist, we will need to create one

                // Init new user
                user = new User();
                user.Name = response["data"]["user"]["name"].ToString();
                user.Email = response["data"]["user"]["email"].ToString();
                user.Username = response["data"]["user"]["username"].ToString();
                user.Settings[User.SETTING_KEY_IP] = ip;
                user.Settings[User.USER_LANGUAGE_KEY] = response["data"]["user"]["language"].ToString();
                user.Enabled = true;
                db.Users().Add(user); // add user without any access groups or policies
                db.SaveChanges();
                user.Password = new Guid().ToString() + new Guid().ToString() + new Guid().ToString(); // random password
            }

            // At this point the user exists, but we need to sync with the endpoint
            var syncData = response["data"] as JObject;
            user.SyncUniversalLoginPropertiesWithJSON(db, syncData);
            user.Settings[USER_UNIVERSAL_LOGIN_ENDPOINT_KEY] = endpoint; // Make sure the endpoint is registered
            user.Settings[USER_UNIVERSAL_LOGIN_SYNCKEY_KEY] = synckey;
            db.SaveChanges();

            // Login locally using the hash and duration provided by the provider
            TimeSpan expiresIn = Core.Util.Time.GetTimespanFromString(localDuration);
            var token = user.CreateAuthToken(context, expiresIn, localTokenHash);
            db.SaveChanges();
            return token;

        }

        public static AuthToken LoginUserWithUsernameAndPasswordOnLocalMachine(ModelContext db, HttpContext context, string username, string password, string duration) {
            
            // Parse duration
            TimeSpan expiresIn = Core.Util.Time.GetTimespanFromString(duration,new TimeSpan(30,0,0,0)); // default one month
            
            // Get the user and verify the password
            var user = db.Users().GetByUsername(username);
            
            // Enabled?
            if(user.Enabled == false) throw new BackendException("user-invalid", "The user account no longer active or has not been activated yet. If this is your first login, please activate your account first via the activation email you should have received.");

            // Verify the password
            user.VerifyPassword(password, true); // will throw exception!

            // All good!
            // Create the authtoken and send as api message
            var token = user.CreateAuthToken(context, expiresIn);
            db.SaveChanges();
            return token;
        }

        /// <summary>
        /// Creates the login properties json for a universal login synchronize.
        /// See also CreateUniversalLoginPropertiesJSONForSync
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public JObject CreateUniversalLoginPropertiesJSONForSync(ModelContext db) {
            // Create list of access groups
            this.LoadFirstLevelNavigationReferences();
            var accessGroupList = this.AccessGroups.Select(e => e.Name);
            var accessPolicyList = this.AccessPolicies.Select(e => e.Name);

            // Resources URL
            var resoucesURL = Core.Config.ServerURL;
            if (!string.IsNullOrEmpty(Core.Config.PublicURL)) resoucesURL = Core.Config.PublicURL;
            if (!string.IsNullOrEmpty(Core.Config.CDNURL)) resoucesURL = Core.Config.CDNURL;

            // Profile image
            var profileImage = this.ProfileImage;
            if (profileImage != null) profileImage = resoucesURL + profileImage;

            // Compile everything
            var ret = new {
                User = new {
                    Name = this.Name,
                    ProfileImage = profileImage,
                    Email = this.Email,
                    Enabled = this.Enabled,
                    Username = this.Username,
                    Language = this.Settings[Core.Model.User.USER_LANGUAGE_KEY]
                },
                AccessGroups = accessGroupList,
                AccessPolicies = accessPolicyList
            };
            return Core.JSON.FromObject(ret);
        }

        /// <summary>
        /// Synchronizes the universal login properties with the given json generated by 
        /// CreateUniversalLoginPropertiesJSONForSync.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="json">The json.</param>
        public void SyncUniversalLoginPropertiesWithJSON(ModelContext db, JObject json) {
            // Sync easy properties
            this.Email = json["user"]["email"].ToString();
            this.Name = json["user"]["name"].ToString();
            this.Enabled = json["user"]["enabled"].ToString().ToLower() == "true";
            this.ProfileImage = json["user"]["profile-image"].ToString();

            // Sync groups
            this.LoadFirstLevelNavigationReferences(); // makes sure policies and groups are loaded
            this.AccessGroups.Clear();
            foreach(string accessGroupName in json["access-groups"].ToList()) {
                var matchingLocalGroup = db.AccessGroups().SingleOrDefault(e => e.Name == accessGroupName.ToString());
                if (matchingLocalGroup != null) this.AccessGroups.Add(matchingLocalGroup);
            }

            // Sync policies
            this.AccessPolicies.Clear();
            foreach(string accessPolicyName in json["access-policies"].ToList()) {
                var matchingLocalPolicy = db.AccessPolicies().SingleOrDefault(e => e.Name == accessPolicyName.ToString());
                if (matchingLocalPolicy != null) this.AccessPolicies.Add(matchingLocalPolicy);
            }
        }



        public static void FireUserAddedEvent(User user) {
            UserAddedEvent?.Invoke(user);
        }

        public static void FireUserDeletedEvent(User user) {
            UserDeletedEvent?.Invoke(user);
        }

        /// <summary>
        /// Get the Initials of a user.name.
        /// Only two supported first and last name
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetInitials() {
           if (this.Name == null || this.Name.Trim().Count() <1) {
                return null;
            }
            var splits = this.Name.Trim().Split(new[] { ' ' },StringSplitOptions.RemoveEmptyEntries);
            string initals = splits.First().First().ToString();
            if (splits.Count() > 1) {
                initals += splits.Last().First().ToString();
            }
            return initals.ToUpperInvariant();
        }

        #endregion

        #region Public Virtual Methods /////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);
            
            // Inform subscribers
            PropertiesChanged?.Invoke(this);
        }

        public bool Published {
            get {
                return this.Enabled;
            }
        }

        /// <summary>
        /// The user is deactivated and has no password
        /// </summary>
        public bool IsActivationPending { get {
                return this.Enabled == false && string.IsNullOrEmpty(this.PasswordHash ) == true;
            }
        }

        public override Core.Cards.CardBuilder VisualCard() {
            return new Core.Cards.CardBuilder(this)
                .Title(this.Username)
                .Subtitle(this.Email)
                .Sub(this.PublicId);
        }

        public override string ToString() {
            if (this.Name != null) return this.Name;
            if (this.Email != null) return this.Email;
            return base.ToString();
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextUserExtenions {
        public static DbSet<User> Users(this Core.Model.ModelContext context) {
            return context.Set<User>();
        }
    }
    public static class IQueryableUserExtenions {
        
        public static User SystemUser(this IQueryable<User> query) {
            return query.GetByUsername(User.USERNAME_SYSTEM);
        }

        public static IQueryable<User> Active(this IQueryable<User> query) {
            var superUser = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(User.USERNAME_SUPERUSER);
            var systemUser = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(User.USERNAME_SYSTEM);
            return query.Where(u=>u.Enabled &&  u.UsernameHash != systemUser && u.UsernameHash != superUser);
        }

        public static T GetByUsername<T>(this IQueryable<T> query, string username, bool throwException = true) where T : Core.Model.User {
            var usernameHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(username);
            var ret = query.Where(e => e.UsernameHash == usernameHash).SingleOrDefault();
            if(ret == null && throwException) throw new BackendException("username-invalid", "The username does not exist.");
            return ret;
        }

      


    }

    #endregion
}
