using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.History;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {
    
    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/system.data.entity.migrations.history.historycontext(v=vs.113).aspx
    /// https://msdn.microsoft.com/en-us/library/dn456841(v=vs.113).aspx
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.History.HistoryContext" />
    public class ModelHistory : HistoryContext {
        
        public ModelHistory(DbConnection dbConnection, string defaultSchema)
                : base(dbConnection, defaultSchema)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
        }
    }
}
