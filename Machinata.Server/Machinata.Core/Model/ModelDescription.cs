using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    public class ModelDescription {

        public class Table {

            public string Name { get; set; }

            public string CreateSQL { get; set; }
            
            public string ReducedCreateSQL {
                get {
                    var parser = new Core.SQL.SQLParser.Parser();
                    parser.Parse(this.CreateSQL);
                    parser.Reduce();
                    return parser.GeneratedSQL;
                    /*
                    var ret = this.CreateSQL;
                    ret = RemoveAutoIncrements(ret);
                    ret = RemoveAutoGeneratedConstraints(ret);
                    ret = RemoveEngineCharset(ret);
                    ret = SortCreateStatement(ret);
                    ret = SimplifySQL(ret);
                    ret = TrimSQLComments(ret);
                    return ret;*/
                }
            }

            public bool AnalysisShouldRemove = false;
            public string AnalysisAction;
        }

        public List<Table> Tables = new List<Table>();
        
    }
}
