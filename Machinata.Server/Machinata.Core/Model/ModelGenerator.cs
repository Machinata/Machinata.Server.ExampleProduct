using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// Custom model generator that adds some prefixes to allow for identification of specific foriegn keys or indexes created by EF.
    /// See https://stackoverflow.com/questions/31534945/change-foreign-key-constraint-naming-convention/31553476#31553476
    /// </summary>
    public class ModelGenerator : MySql.Data.Entity.MySqlMigrationSqlGenerator {
        
        
        protected override MigrationStatement Generate(AddForeignKeyOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

        protected override MigrationStatement Generate(DropForeignKeyOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

        protected override MigrationStatement Generate(CreateIndexOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

        protected override MigrationStatement Generate(DropIndexOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

    }
}
