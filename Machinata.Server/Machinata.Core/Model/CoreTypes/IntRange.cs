using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// A Complex Type which represents a date start range and date end range.
    /// Note: To include this in a Model Object entity, you must initialize the property
    /// (in the constructor, for example) to a object in order to save via entity framework.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(IntRangeConverter))]
    [JsonConverter (typeof(IntRangeJsonConverter))]
    public class IntRange {
        
        public int? A { get; set; }
        public int? B { get; set; }
        
        public IntRange() {
            this.A = null;
            this.B = null;
        }

        public IntRange(int? min, int? max) {
            this.A = min;
            this.B = max;
        }

        public IntRange(string str) {
            // Parse out the str 'A - B', where A and B can be floats
            var regex = new Regex(@"([-+]?[0-9]*\.?[0-9]+) ?- ?([-+]?[0-9]*\.?[0-9]+)"); // see https://regex101.com/r/QuGm0A/1
            var match = regex.Match(str);
            if (match.Success == false || match.Groups.Count != 3) throw new Exception($"IntRange from string '{str}' has invalid value");
            var strA = match.Groups[1].Value;
            var strB = match.Groups[2].Value;
            // Parse the numbers
            try {
                this.A = int.Parse(strA);
            } catch(Exception e) {
                throw new Exception($"IntRange from string '{str}' has invalid A value: {strA}");
            }
            try {
                this.B = int.Parse(strB);
            } catch (Exception e) {
                throw new Exception($"IntRange from string '{str}' has invalid B value: {strB}");
            }
        }

        public bool HasValue() {
            return (this.A != null && this.B != null);
        }
        
        public bool HasEitherMinOrMaxValue() {
            return (this.A != null || this.B != null);
        }

        public override string ToString() {
            return ToString(null);
        }


        public string ToString(string format) {
            return $"{this.A} - {this.B}";
        }

        public IntRange Clone() {
            return new IntRange(this.A, this.B);
        }

        


    }
    

    /// <summary>
    /// Allows for automatic conversion from a JSON string to a Properties object.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class IntRangeConverter : TypeConverter {

     
    }

    public class IntRangeJsonConverter : JsonConverter {
        public override bool CanConvert(Type objectType) {
            if (objectType == typeof(string)) return true;
            //if (sourceType == typeof(decimal)) return true;
            return false;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            var str = reader.Value?.ToString();
            return new IntRange(str);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            var val = value as IntRange;
            JValue o = new JValue(val?.ToString());
            o.Value = val.ToString();
            o.WriteTo(writer);
        }
    }

}