using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// A Complex Type which represents a date start range and date end range.
    /// Note: To include this in a Model Object entity, you must initialize the property
    /// (in the constructor, for example) to a object in order to save via entity framework.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(DateRangeConverter))]
    public class DateRange {
        
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        
        public DateRange() {
            this.Start = null;
            this.End = null;
        }

        public DateRange(DateTime? start, DateTime? end) {
            this.Start = start;
            this.End = end;
        }
        
        public bool HasValue() {
            return (this.Start != null && this.End != null);
        }
        
        public bool HasEitherStartOrEndValue() {
            return (this.Start != null || this.End != null);
        }

        public override string ToString() {
            return ToString(null);
        }

        public string ToDateString() {
            return ToString(Core.Config.DateFormat);
        }

        public string ToHumanReadableDateString() {
            return ToString(Core.Config.DateFormatHumanReadable);
        }

        public string ToString(string format, bool converToDefaultTimezone = true) {
            var dateStart = this.Start;
            var dateEnd = this.End;
            if (dateStart == null && dateEnd == null) return null;
            if (converToDefaultTimezone) {
                if(dateStart != null) dateStart = Core.Util.Time.ConvertToDefaultTimezone(dateStart.Value);
                if(dateEnd != null) dateEnd = Core.Util.Time.ConvertToDefaultTimezone(dateEnd.Value);
            }
            var ret = "";
            if(dateStart != null) {
                ret += dateStart.Value.ToString(format != null ? format : Core.Config.DateTimeFormat);
            }
            ret += Core.Config.DateRangeSeperator;
            if(dateEnd != null) {
                ret += dateEnd.Value.ToString(format != null ? format : Core.Config.DateTimeFormat);
            }
            return ret;
        }
        
        public DateRange Clone() {
            return new DateRange(this.Start, this.End);
        }

        public int GetNumberOfDays() {
            return (int)Math.Floor((this.End - this.Start).Value.TotalDays);
        }

        public DateRange ToDefaultTimezone() {
            return new DateRange(Core.Util.Time.ConvertToDefaultTimezone(this.Start.Value), Core.Util.Time.ConvertToDefaultTimezone(this.End.Value));
        }
        


    }
    

    /// <summary>
    /// Allows for automatic conversion from a JSON string to a Properties object.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class DateRangeConverter : TypeConverter {
        /*
        public override bool IsValid(ITypeDescriptorContext context, object value) {
            if(value is string) {
                try {
                    JsonConvert.DeserializeObject<Dictionary<string, object>>((string)value);
                    return true;
                } catch(Exception e) {
                    return false;
                }
            }
            return base.IsValid(context, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            if (value is string) {
                return new Properties((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }
        }*/
    }

}