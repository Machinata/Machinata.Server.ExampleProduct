using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Reflection {
    public class Methods
    {
        public static List<MethodInfo> _machinataMethods = null;
        public static ConcurrentDictionary<Type,List<MethodInfo>> _machinataMethodsForAttribute = new ConcurrentDictionary<Type, List<MethodInfo>>();

        /// <summary>
        /// Gets the BlackBox methods, ie all the Machinata Types' methods.
        /// This list is cached.
        /// </summary>
        /// <returns></returns>
        public static List<MethodInfo> GetMachinataMethods() {
            if(_machinataMethods == null) {
                _machinataMethods = Types.GetMachinataTypes().SelectMany(a => a.GetMethods()).ToList();
            }
            return _machinataMethods;
        }

        /// <summary>
        /// Gets the Machinata methods with a specifc attribute.
        /// The returned list is cached.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns></returns>
        public static List<MethodInfo> GetMachinataMethodsWithAttribute(Type attribute) {
            if (!_machinataMethodsForAttribute.ContainsKey(attribute)) {
                _machinataMethodsForAttribute[attribute] = GetMachinataMethods().Where(m => m.GetCustomAttributes(attribute, false).Length > 0).ToList();
            }
            return _machinataMethodsForAttribute[attribute];
        }
    }
}
