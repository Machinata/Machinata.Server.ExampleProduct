using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Machinata.Core.Reflection
{
    public class Assemblies
    {
        public static List<System.Reflection.Assembly> _machinataAssemblies = null;

        /// <summary>
        /// Gets all the Machinata assemblies.
        /// This list is cached.
        /// </summary>
        /// <returns></returns>
        public static List<System.Reflection.Assembly> GetMachinataAssemblies() {
            if(_machinataAssemblies == null) {
                _machinataAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith("Machinata.")).ToList();
            }
            return _machinataAssemblies;
        }


    }

    
}

