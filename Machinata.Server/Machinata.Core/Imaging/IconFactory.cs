using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Imaging {
    public class IconFactory {

        public static string GetIconURL(string iconId, string bundleName = null) {
            if (bundleName == null) bundleName = Core.Config.IconsDefaultBundleName;
            
            // Parse out a bundlename from the icon id as a shorthand for defining a different bundle than the default or passed value
            // Such icon ids look like {icon.icons-xyz.left-arrow} where the bundle name would be icons-xyz-bundle.svg
            var iconBundleAndIdSegs = iconId.Split('.');
            if (iconBundleAndIdSegs.Length == 2) {
                iconId = iconBundleAndIdSegs.Last();
                bundleName = iconBundleAndIdSegs.First() + "-bundle.svg";
            }

            var iconSpriteUrl = $"/static/bundle/svg/{bundleName}?profile=digital&lang=en&theme=" + Core.Config.ThemeDefaultName+"&build="+Core.Config.BuildID;
            var iconURL = $"{iconSpriteUrl}#{iconId}";
            return iconURL;
        }

        public static string GetIconHTML(string iconId, string bundleName = null) {
            
            // Parse out a bundlename from the icon id as a shorthand for defining a different bundle than the default or passed value
            // Such icon ids look like {icon.icons-xyz.left-arrow} where the bundle name would be icons-xyz-bundle.svg
            var iconBundleAndIdSegs = iconId.Split('.');
            if(iconBundleAndIdSegs.Length == 2) {
                iconId = iconBundleAndIdSegs.Last();
                bundleName = iconBundleAndIdSegs.First() + "-bundle.svg";
            }

            var iconURL = GetIconURL(iconId, bundleName);
            var iconCode = $"<svg class=\"icon icon-{iconId}\"><use xlink:href=\"{iconURL}\"></use></svg>";
            return iconCode;
        }

    }
}
