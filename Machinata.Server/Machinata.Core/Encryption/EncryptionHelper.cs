using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Encryption {
    public class EncryptionHelper {

        public static byte[] HexDecode(string hex) {
            var bytes = new byte[hex.Length / 2];
            for (int i = 0; i < bytes.Length; i++) {
                bytes[i] = byte.Parse(hex.Substring(i * 2, 2), NumberStyles.HexNumber);
            }
            return bytes;
        }

        public static string Hex(byte [] content) {
            return String.Concat(Array.ConvertAll(content, x => x.ToString("x2")));
        }

    }
}
