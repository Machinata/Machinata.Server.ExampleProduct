using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Encryption {
    
    /// <summary>
    /// The default encryption provider for Machinata.
    /// </summary>
    public class DefaultHasher {

        /// <summary>
        /// Hashes the input string using the default provider.
        /// Per default, the default provier is SHA256.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string HashString(string input) {
            // Note: this code is protected and should not change once in master!
            return Core.Encryption.Providers.SHA256Hasher.HashString(input);
        }

        /// <summary>
        /// Salts the and hashes the input string using the default hash provider and the
        /// system global salts.
        /// This method can be used to generate salted hash codes for a unque input, such as, for example,
        /// verifying the authenticity of a unsubscribe link.
        /// Note: this method should not be used for hashing passwords. Hashing passwords should use a custom method.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string SaltAndHashString(string input) {
            // Note: this code is protected and should not change once in master!
            // Make sure we have a string, otherwise it could reveal the salt and hash backwards.
            if (string.IsNullOrEmpty(input)) throw new Exception("String to SaltAndHashString cannot be null or empty.");
            var str = Core.Config.EncryptionHashSalt2 + input + Core.Config.EncryptionHashSalt1 + HashString(Core.Config.EncryptionHashSalt2 + Core.Config.EncryptionHashSalt1);
            return HashString(str);
        }
        

        public static string ComputeEmailOrUsernameHash(string email) {
            var globalSalt1 = Core.Config.EncryptionHashSalt1;
            var globalSalt2 = Core.Config.EncryptionHashSalt2;
            var stringToHash = globalSalt1 + email + globalSalt2;
            return Core.Encryption.DefaultHasher.HashString(stringToHash);
        }
    }

    public class DefaultRandomNumberGenerator {
        
        public static string GeneratePassword(int length) {
            return System.Web.Security.Membership.GeneratePassword(length, length / 3);
        }

        public static string GeneratePasswordXMLSafe(int length) {
            var pwd = GeneratePassword(length);
            while(pwd.Contains("<") || pwd.Contains(">") || pwd.Contains("&") || pwd.Contains("\"")) pwd = GeneratePassword(length);
            return pwd;
        }

    }

    /// <summary>
    /// The default encryption provider for Machinata.
    /// </summary>
    public class DefaultEncryption {

        /// <summary>
        /// Encrypts the string using AES then HMAC. The encryption keys are stored in the config.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string EncryptString(string input) {
            return Core.Encryption.Providers.AESThenHMAC.SimpleEncrypt(input, Core.Config.EncryptionCryptKeyBytes, Core.Config.EncryptionAuthKeyBytes);
        }

        /// <summary>
        /// Decrypts the string using AES then HMAC. The encryption keys are stored in the config.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string DecryptString(string input) {
            if (input == null) return null;
            return Core.Encryption.Providers.AESThenHMAC.SimpleDecrypt(input, Core.Config.EncryptionCryptKeyBytes, Core.Config.EncryptionAuthKeyBytes);
        }
    }
}
