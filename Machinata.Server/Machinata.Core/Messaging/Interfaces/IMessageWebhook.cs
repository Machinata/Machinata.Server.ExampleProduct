using Machinata.Core.Model;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Messaging.Interfaces {

    /// <summary>
    /// Interface for Webhooks which handle e.g. Bounced Emails
    /// </summary>
    public interface IMessageWebhook {

        void HandleNotification(ModelContext db, HttpContext context);
    }
}
