using Machinata.Core.Model;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Messaging.Interfaces {

    /// <summary>
    /// Modules which send messages with SubCategores have implement this Interface
    /// </summary>
    public interface IMessageSender {
        /// <summary>
        /// Gets the sub categories.
        /// </summary>
        /// <returns>short-url: Title</returns>
        IEnumerable<Tuple<string,string>> GetSubCategories(MailingUnsubscription.Categories category);
        EmailTemplate GetTestTemplate(ModelContext db);

    }

    public class GenericEmailTest: IMessageSender {
        
        public IEnumerable<Tuple<string, string>> GetSubCategories(MailingUnsubscription.Categories category) {
            var categories = new List<Tuple<string, string>>();
            return categories;
        }

        public EmailTemplate GetTestTemplate(ModelContext db) {
            var template = new EmailTemplate(db, "test");
            return template;
        }
    }
}
