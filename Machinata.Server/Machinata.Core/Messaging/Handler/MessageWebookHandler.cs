using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Messaging.Providers {
    public class MessageWebookHandler : APIHandler {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        [RequestHandler("/webhook/messaging/{providerName}", AccessPolicy.PUBLIC_ARN)]

        public void Webhook(string providerName) {
            try {
                var provider = MessageWebhook.GetProvider(providerName);
                provider.HandleNotification(this.DB, this.Context);
                this.DB.SaveChanges();
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Message Webhook Transaction error", "Machinata.Module.Core", this.Context, this, e);
                _logger.Error(e, "Message Webhook Transaction error: " + Core.EmailLogger.CompileFullErrorReport(this.Context, this, e));
                throw new BackendException("weebhook-error", "Could not process request", e);
            }



            this.SendAPIMessage("success");
        }

    }
}