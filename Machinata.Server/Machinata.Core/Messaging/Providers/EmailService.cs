using Machinata.Core.Messaging.Interfaces;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace Machinata.Core.Messaging.Providers {
    public class EmailService {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        private class SendEmailWorkerTask : Core.Worker.WorkerTask {

            private EmailSender _sender;
            private System.Net.Mail.MailMessage _mail;

            public override int MaxAttempts {
                get {
                    return 3; //TODO
                }
            }

            public SendEmailWorkerTask(EmailSender sender, System.Net.Mail.MailMessage mail) {
                _sender = sender;
                _mail = mail;
            }

            public override void Process() {
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient(_sender.Server);
                SmtpServer.Port = System.Convert.ToInt32(_sender.Port);
                SmtpServer.Credentials = new System.Net.NetworkCredential(_sender.Username,_sender.Password);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(_mail);
            }

        }

        /// <summary>
        /// Send email
        /// WARNING: THIS FUNCTION SHOULD ONLY BE USED IF 
        /// </summary>
        /// <param name="to"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="html"></param>
        /// <param name="sender"></param>
        /// <param name="attachments"></param>
        /// <param name="async"></param>
        public static void SendEmail(string to, string cc, string bcc, string subject, string message, bool html, EmailSender sender = null, IEnumerable<EmailAttachment> attachments = null, bool async = true) {
            var toContacts = MessageCenter.ParseContactsList(to);
            var ccContacts = MessageCenter.ParseContactsList(cc);
            var bccContacts = MessageCenter.ParseContactsList(bcc);
            SendEmail(toContacts, ccContacts, bccContacts, subject, message, html, sender, attachments, async);
        }

        internal static void SendEmail(IEnumerable<EmailContact> to, IEnumerable<EmailContact> cc, IEnumerable<EmailContact> bcc, string subject, string message, bool html, EmailSender sender = null, IEnumerable<EmailAttachment> attachments = null, bool async = true) {

            _logger.Log(NLog.LogLevel.Debug, $"Sending email to {to}, subject = {subject} content = {message}");

            // Init
            if (sender == null) {
                sender = EmailSender.DefaultEmailSender;
            }

            // Subject
            if (string.IsNullOrWhiteSpace(subject)) {
                throw new Exception("Can not send email without a subject");
            }

            // Create message
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.IsBodyHtml = html;
            mail.From = new System.Net.Mail.MailAddress(sender.EmailSenderEmail, sender.SenderName);
            if (string.IsNullOrEmpty(sender.OverrideAddress)) {
                if (to == null || to.Count() == 0) throw new Exception("No <to> email address provided.");
                foreach (var toAddress in to) {
                    SafelyAddAddressToMailCollection(mail.To, toAddress);
                }
                if (cc != null && cc.Any()) {
                    foreach (var address in cc) {
                        SafelyAddAddressToMailCollection(mail.CC, address);
                    }
                }
                if (bcc != null) {
                    foreach (var address in bcc) {
                        SafelyAddAddressToMailCollection(mail.Bcc, address);
                    }
                }
            } else {
                SafelyAddAddressToMailCollection(mail.To, new EmailContact() { Address = sender.OverrideAddress, Name = sender.SenderName });
            }

            mail.Subject = subject;
            mail.Body = message;

            // ReplyTo
            if (!string.IsNullOrWhiteSpace(sender.ReplyToEmail)) {
                string replyToName = sender.ReplyToEmail;
                if (!string.IsNullOrWhiteSpace(sender.ReplyToName)) {
                    replyToName = sender.ReplyToName;
                }
                mail.ReplyToList.Add(new System.Net.Mail.MailAddress(sender.ReplyToEmail, replyToName));
            }

            // Attachements
            if (attachments != null) {
                foreach (var attachment in attachments) {
                    System.Net.Mail.Attachment attachementItem = new System.Net.Mail.Attachment(attachment.Content, attachment.Name != null ? attachment.Name : "attachment");
                    mail.Attachments.Add(attachementItem);
                }

            }
            
            var task = new SendEmailWorkerTask(sender, mail);
            if (async) {
                Core.Worker.LocalPool.QueueTask(task);
            } else {
                task.Process();
            }

        }

    

       

        #region Helper Methods //////////////////////////////////////////////////////////////////////////////////////////////////////////////

     

        private static void SafelyAddAddressToMailCollection(MailAddressCollection addressCollection, EmailContact emailContact) {

            string address = null;

            if (!Config.SendProductiveEmails) {

                if  (string.IsNullOrEmpty(Core.Config.TestEmail) == true) {
                    throw new Exception("Please define a TestEmail or deactivate the SendProductiveEmails mode");
                }

                address = Core.Config.TestEmail;
               
            } else {
                address = emailContact.Address;
            }

            try {
             
                addressCollection.Add(new MailAddress(address, emailContact.Name));
               

               
            } catch (Exception e) {
                throw new Exception("Could not add email '" + address + "' to collection: " + e.Message, e);
            }

        }

        #endregion

    }
}
