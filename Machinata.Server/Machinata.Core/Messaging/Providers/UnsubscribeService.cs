using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Messaging.Interfaces;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Machinata.Core.Model.MailingUnsubscription;

namespace Machinata.Core.Messaging.Providers {
    public class UnsubscribeService {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        /// <summary>
        /// Unsubscribes an email,user or business for a given category, sub category
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="code">The code.</param>
        /// <param name="email">The email.</param>
        /// <param name="category">The category.</param>
        /// <param name="subcategory">The subcategory.</param>
        /// <param name="business">The business.</param>
        /// <param name="user">The user.</param>
        /// <returns>New Unsubsription, or the existing one if already unsubscribed</returns>
        /// <exception cref="BackendException">unsubscribe-error;Wrong parameters values provided.</exception>
        public static void Unsubscribe(ModelContext db, string code, string email, string category, string subcategory, string business, string user) {

            MailingUnsubscription result = null;
           
            var target = GetTargetType(email, business, user);

            if (target == Targets.Business) {
                var businessFromDb = db.Businesses().GetByPublicId(business);
                CheckCode(businessFromDb, code);
                result = GetUnsubscription(db, businessFromDb, EnumHelper.ParseEnum<Categories>(category), subcategory);
                if (result == null) {
                    result = Unsubscribe(db, Targets.Business, category, subcategory, businessFromDb.PublicId);
                }

            } else if (target == Targets.User) {
                var userFromDb = db.Users().GetByUsername(user);
                CheckCode(userFromDb, code);
                result = GetUnsubscription(db, userFromDb, EnumHelper.ParseEnum<Categories>(category), subcategory);
                if (result == null ) {
                    result = Unsubscribe(db, Targets.User, category, subcategory, userFromDb.PublicId);
                }
            } else if (target == Targets.Email) {
                CheckCode(email, code);
                result = GetUnsubscription(db, email, EnumHelper.ParseEnum<Categories>(category), subcategory);
                if (result == null ) {
                    result = Unsubscribe(db, Targets.Email, category, subcategory, email);
                }
            } 
        }

        public static string GetSubCategoryName(Categories category, string subCategoryKey) {
            if (subCategoryKey == "*") {
                return "Any";
            }
            var subCategoryName = GetCategories()[category.ToString()].FirstOrDefault(c => c.Item1 == subCategoryKey)?.Item2;
            return subCategoryName;
        }

        public static Categories GetCategoryName( string categoryKey) {
            return EnumHelper.ParseEnum<Categories>(categoryKey);
        }

        public static Targets GetTargetType(string email, string business, string user) {
            if (!string.IsNullOrEmpty(business)) {
                return Targets.Business;
            } else if (!string.IsNullOrEmpty(user) && string.IsNullOrEmpty(business)) {
                return Targets.User;
            } else if (!string.IsNullOrEmpty(email)) {
                return Targets.Email;
            } else {
                throw new BackendException("unsubscribe-error", "Wrong parameters values provided.");
            }
        }

        public static UnsubscribeInfo CheckCode(ModelContext db, string code, string email, string business, string user) {
            var target = GetTargetType(email, business, user);

            var info = new UnsubscribeInfo();
            
            if (target == Targets.Business) {
                var businessFromDb = db.Businesses().GetByPublicId(business);
                info.Identifier = business;
                CheckCode(businessFromDb, code);
            } else if (target == Targets.User) {
                var userFromDb = db.Users().GetByUsername(user);
                info.Identifier = user;
                CheckCode(userFromDb, code);
            } else if (target == Targets.Email) {
                info.Identifier = email;
                CheckCode(email, code);
            }

            info.Target = target;

            return info;
        }


        public static void CheckCode(string email, string code) {
            CheckCodeIsValid(code, Core.Encryption.DefaultHasher.SaltAndHashString(GetKeyForHash(email)));
        }
        public static void CheckCode(Business business, string code) {
            CheckCodeIsValid(code, Core.Encryption.DefaultHasher.SaltAndHashString(GetKeyForHash(business)));
        }
        public static void CheckCode(User user, string code) {
            CheckCodeIsValid(code,user.GetSecretCode());
        }
        private static void CheckCodeIsValid(string expected, string code) {
            if (expected != code) {
                throw new BackendException("access-code-invalid", "The access code is invalid.");
            }
        }

        
        public static MailingUnsubscription Unsubscribe(ModelContext db, Targets type, string category, string subcategory, string id) {
            var unsub = new MailingUnsubscription();

            // Check Id
            if (string.IsNullOrEmpty(id)) {
                throw new BackendException("unsubscribe-error", "No Id for has been provided.");
            }

            // Only save hash if email adress
            if (type == Targets.Email) {
                unsub.Identifier = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(id);
            }
            else {
                unsub.Identifier = id;
            }
            unsub.Target = type;
            Categories unsubscribeType;
            if (!Enum.TryParse(category, true, out unsubscribeType)) {
                throw new BackendException("unsubscribe-error", "Unknown email category");
            }
            unsub.Category = unsubscribeType;
            unsub.SubCategory = subcategory;
            if (subcategory == "*") {
                RemoveRedundantUnsubscriptions(db, type, category, unsub.Identifier);
            }
            db.MailingUnsubscriptions().Add(unsub);

            return unsub;
        }



        /// <summary>
        /// Unsubscribes all other available subcategories for a given Categoy other than the ones passed as 'subscribtions'
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="category">The category.</param>
        /// <param name="email">The email.</param>
        /// <param name="subscriptions">The subscriptions.</param>
        public static void UnsubscribeOtherSubcategories(ModelContext db, MailingUnsubscription.Categories category, string email, IEnumerable<string> subscriptions) {
            // Remove all current unsubscriptions
            RemoveAllEmailUnsubscriptions(db, email);

            // Add new ones
            var allCategories = UnsubscribeService.GetAvailableSettings();
            var categoriesToUnsubscribe = allCategories.Where(c => c.Category == category.ToString() && !subscriptions.Contains(c.SubCategoryKey));
            foreach (var toUnsubscribe in categoriesToUnsubscribe) {
                UnsubscribeService.Unsubscribe(db, MailingUnsubscription.Targets.Email, category.ToString(), toUnsubscribe.SubCategoryKey, email);
            }
        }

        /// <summary>
        /// Removes all email unsubscriptions for a given email address
        /// IMPORTANT: only for emails with ComputeEmailOrUsernameHash 
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="email">The email.</param>
        public static void RemoveAllEmailUnsubscriptions(ModelContext db, string email) {
            var emailHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(email);
            var unsubs = db.MailingUnsubscriptions().Where(mu => mu.Target == MailingUnsubscription.Targets.Email && mu.Identifier == emailHash);
            db.MailingUnsubscriptions().RemoveRange(unsubs);
        }


        /// <summary>
        /// Removes the redundant unsubscriptions. (Same Category but not '*' Subcategory)
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="type">The type.</param>
        /// <param name="category">The category.</param>
        /// <param name="id">The identifier.</param>
        private static void RemoveRedundantUnsubscriptions(ModelContext db, Targets type, string category, string id) {
            var unsubType = EnumHelper.ParseEnum<Categories>(category);
            var redundantSubs = db.MailingUnsubscriptions().Where(mu => mu.Target == type && mu.Category == unsubType && mu.Identifier == id);
            db.MailingUnsubscriptions().RemoveRange(redundantSubs);
        }

        public static string GetCode(Business business) {
            return Core.Encryption.DefaultHasher.SaltAndHashString(GetKeyForHash(business));
        }

        public string GetCode(User user) {
            return user.GetSecretCode();
        }

        public static string GetCode(string email) {
            return Core.Encryption.DefaultHasher.SaltAndHashString(GetKeyForHash(email));
        }

        public static MailingUnsubscription GetUnsubscription(ModelContext db, string email, Categories unsubscribeType, string unsubscribeSubtype) {
            IQueryable<MailingUnsubscription> unsubscriptions = GetUnsubscriptions(db, email, unsubscribeType);
            IQueryable<MailingUnsubscription> bounces = GetUnsubscriptions(db, email, Categories.HardBounce);
            return GetUnsubscription(unsubscribeSubtype, unsubscriptions);
        }

    
        public static bool IsUnsubscribed(ModelContext db, string email, Categories unsubscribeType, string unsubscribeSubtype) {
           
            IQueryable<MailingUnsubscription> bounces = GetUnsubscriptions(db, email, Categories.HardBounce);

            // Hard Bounces
            if (bounces.Any()) {
                return true;
            }

            // Unsubscription
            IQueryable<MailingUnsubscription> unsubscriptions = GetUnsubscriptions(db, email, unsubscribeType);
            var unsubscriptionSubs = GetUnsubscription(unsubscribeSubtype, unsubscriptions);

           
            return unsubscriptionSubs != null;
        }

        public static MailingUnsubscription GetUnsubscription(ModelContext db, Business business, Categories unsubsribeType, string unsubscribeSubtype) {
            IQueryable<MailingUnsubscription> unsubscriptions = GetUnsubscriptions(db, business, unsubsribeType);
            // TODO:
            //if (unsubscriptions.Any(u => u.Category == Categories.HardBounce)) {
            //    return unsubscriptions.First(u => u.Category == Categories.HardBounce);
            //}
            return GetUnsubscription(unsubscribeSubtype, unsubscriptions);
        }

        public static MailingUnsubscription GetUnsubscription(ModelContext db, User user, Categories unsubsribeType, string unsubscribeSubtype) {
            var unsubscriptions = GetUnsubscriptions(db, user, unsubsribeType);
            // TODO:
            //if (unsubscriptions.Any(u => u.Category == Categories.HardBounce)) {
            //    return unsubscriptions.First(u => u.Category == Categories.HardBounce);
            //}
            return GetUnsubscription(unsubscribeSubtype, unsubscriptions);
        }

        public static IQueryable<MailingUnsubscription> GetUnsubscriptions(ModelContext db, Business business, Categories? unsubsribeType = null) {
            var unsubs = db.MailingUnsubscriptions().Where(u => u.Target == Targets.Business && u.Identifier == business.PublicId);
            if (unsubsribeType != null) {
                unsubs = unsubs.Where(u => u.Category == unsubsribeType);
            }
            return unsubs;
        }

        public static IQueryable<MailingUnsubscription> GetUnsubscriptions(ModelContext db, string email, Categories? unsubsribeType = null) {
            var emailHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(email);
            var unsubs = db.MailingUnsubscriptions().Where(u => u.Target == Targets.Email && u.Identifier == emailHash);
            if (unsubsribeType != null) {
                unsubs = unsubs.Where(u => u.Category == unsubsribeType);
            }
            return unsubs;
        }

        public static IQueryable<MailingUnsubscription> GetEmailUnsubscriptions(ModelContext db, Categories category, string subscategory) {
            var unsubs = db.MailingUnsubscriptions().Where(u => u.Target == Targets.Email && u.Category == category && (u.SubCategory == subscategory || u.SubCategory == "*"));
            return unsubs;
        }

        public static IQueryable<MailingUnsubscription> GetEmailUnsubscriptions(ModelContext db, Categories category) {
            var unsubs = db.MailingUnsubscriptions().Where(u => u.Target == Targets.Email && u.Category == category);
            return unsubs;
        }

        public static IQueryable<MailingUnsubscription> GetUnsubscriptions(ModelContext db, User user, Categories? unsubsribeType = null) {
            var unsubs = db.MailingUnsubscriptions().Where(u => u.Target == Targets.User && u.Identifier == user.PublicId);
            if (unsubsribeType != null) {
                unsubs = unsubs.Where(u => u.Category == unsubsribeType);
            }
            return unsubs;
        }

       
        private static MailingUnsubscription GetUnsubscription(string unsubscribeSubtype, IQueryable<MailingUnsubscription> unsubscriptions) {
            if (unsubscriptions.Any(u => u.SubCategory == "*")) {
                return unsubscriptions.First(u => u.SubCategory == "*");
            }
            if (unsubscriptions.Any(u => u.SubCategory == unsubscribeSubtype)) {
                return (unsubscriptions.First(u => u.SubCategory == unsubscribeSubtype));
            }
            return null;
        }
        

        private static string GetKeyForHash(Business business) {
            return business.PublicId + "_" + business.Id;
        }

     
        private static string GetKeyForHash(string email) {
            return email;
        }

        /// <summary>
        /// Gets the available message senders through reflection.
        /// </summary>
        /// <returns>A list of full type names to the MessageSender class.</returns>
        public static IList<Type> GetAvailableMessageSenders() {
            // Use reflection to gather all tasks
            IList<Type> messageSenderTypes = new List<Type>();
            var types = Core.Reflection.Types.GetMachinataTypes();
            foreach (Type type in types) {
                System.Type t = type;
                if (t.GetInterfaces().Contains(typeof(IMessageSender))) {
                    if (!type.IsAbstract) messageSenderTypes.Add(type);
                }
            }
            return messageSenderTypes;
        }

        /// <summary>
        /// Reads all the Categories:Subcategories_Id,Subcategories_Text from all Message Senders implementing IMessageSenders
        /// </summary>
        /// <returns>With All Subcategories</returns>
        public static IDictionary<string, List<Tuple<string, string>>> GetCategories() {

            var categories = new Dictionary<string, List<Tuple<string, string>>>();
            foreach (var category in EnumHelper.GetEnumValues<Categories>(typeof(Categories))) {
                categories[category.ToString()] = new List<Tuple<string, string>>();
                foreach (var sender in GetAvailableMessageSenders()) {
                    ConstructorInfo constructor = sender.GetConstructor(Type.EmptyTypes);
                    object senderObject = constructor.Invoke(new object[] { });
                    MethodInfo method = sender.GetMethod(nameof(IMessageSender.GetSubCategories));
                    var categoriesObject = method.Invoke(senderObject, new object[] { category }) as IEnumerable<Tuple<string, string>>;
                    categories[category.ToString()].AddRange(categoriesObject.Select(c => c));

                }
            }
            return categories;
        }

        /// <summary>
        /// Gets the public Categories users can unsubcribe to
        /// </summary>
        /// <returns>With All Subcategories</returns>
        public static IEnumerable<string> GetPublicCategories() {
            var categories = EnumHelper.GetEnumValues<Categories>(typeof(Categories)).Where(c => (short)c < 100);
            return categories.Select(c => c.ToString());
        }



        public static string GetUnsubscribeQueryString(string code, string email, string business, string user, string category, string subcategory, string action) {
            string unsubscribeUrl = "?code=" + code;
            unsubscribeUrl += "&service=email";

            if (!string.IsNullOrEmpty(email)) {
                unsubscribeUrl += "&email=" + email;
            }
            if (!string.IsNullOrEmpty(business)) {
                unsubscribeUrl += "&business=" + business;
            }
            if (!string.IsNullOrEmpty(user)) {
                unsubscribeUrl += "&user=" + user;
            }
            if (!string.IsNullOrEmpty(category)) {
                unsubscribeUrl += "&category=" + category;
            }
            if (!string.IsNullOrEmpty(subcategory)) {
                unsubscribeUrl += "&subcategory=" + subcategory;
            }
            if (!string.IsNullOrEmpty(action)) {
                unsubscribeUrl += "&action=" + action;
            }

            return unsubscribeUrl;
        }


       


        public static IQueryable<MailingUnsubscription> GetUnsubscriptions(ModelContext db, string code, string username, string businessId, string email, bool checkCode = true) {
            IQueryable<MailingUnsubscription> unsubscriptions;

            if (businessId != null) {
                var business = db.Businesses().GetByPublicId(businessId);
                if (checkCode) {
                    UnsubscribeService.CheckCode(business, code);
                }
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(db, business);
            } else if (username != null) {
                var user = db.Users().GetByUsername(username);
                if (checkCode) {
                    UnsubscribeService.CheckCode(user, code);
                }
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(db, user);
            } else {
                if (checkCode) {
                    UnsubscribeService.CheckCode(email, code);
                }
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(db, email);
            }

            return unsubscriptions;
        }

        /// <summary>
        /// Manages the unsubsriptions.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <exception cref="Backend404Exception">wrong-id;Subscription toggle wrong id</exception>
        public static void ManageUnsubsriptions(Machinata.Core.Handler.APIHandler handler) {
            string id = handler.Params.String("id");
            if (string.IsNullOrEmpty(id)) { throw new Backend404Exception("wrong-id", "Subscription toggle wrong id"); }
            string category = id.Split('.').First();
            string subCategory = id.Split('.').Last();
            string business = string.IsNullOrEmpty(handler.Params.String("business")) ? null : handler.Params.String("business");
            string user = string.IsNullOrEmpty(handler.Params.String("user")) ? null : handler.Params.String("user");
            var subscribe = handler.Params.Bool("value", false);
            var email = handler.Params.String("email");

            // Remove existing Unsubs
            if (subscribe) {
                var unsubs = UnsubscribeService.GetUnsubscriptions(handler.DB, handler.Params.String("code"), user, business, email);
                var type = EnumHelper.ParseEnum<Categories>(category);

                // Specific
                var specificUnsubscriptions = unsubs.Where(u => u.Category == type &&  u.SubCategory == subCategory );
                handler.DB.MailingUnsubscriptions().RemoveRange(specificUnsubscriptions);

                // Any
                var wildcardUnsubscriptions = unsubs.Where(u => u.Category == type && u.SubCategory == "*");
                if (wildcardUnsubscriptions.Any()) {
                    // TODO: BUSINESS AND USER UNSUBSCRIPTIONS
                    UnsubscribeService.UnsubscribeOtherSubcategories(handler.DB, type, email, new List<string> { subCategory });
                    handler.DB.MailingUnsubscriptions().RemoveRange(wildcardUnsubscriptions);
                }

            }

            // Unsubscribe
            else {
                UnsubscribeService.Unsubscribe(handler.DB, handler.Params.String("code"), handler.Params.String("email"), category, subCategory, business, user);
            }
        }

        /// <summary>
        /// Shows the current Notification Settings, lets a user select and unselect categories of emails
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="apiUrl">The API URL</param>
        /// <param name="availableSettings">the settings available to show in manage page, if null all will be displayed</param>
        /// <param name="unsubscribeAllSubcategories">if action is subscribe, and this is true, all subcategories ("*") will be unsubscribed</param>
        public static void ManageSubscriptionPage(Core.Handler.PageTemplateHandler handler, string apiUrl, IEnumerable<SubscriptionSetting> availableSettings = null, bool unsubscribeAllSubcategories = false) {
            var code = handler.Params.String("code");
            var email = handler.Params.String("email");
            var business = handler.Params.String("business");
            var user = handler.Params.String("user");
            var category = handler.Params.String("category");
            var subcategory = handler.Params.String("subcategory");
            string service = handler.Params.String("service");
            string username = handler.Params.String("user");
            string businessId = handler.Params.String("business");
            string action = handler.Params.String("action");

            // Check Code
            var info = UnsubscribeService.CheckCode(handler.DB, code, email, business, user);

            // Action (manage or unsubscribe)
            var unsubscribe = action == "unsubscribe";

            // Unsubscribe
            if (unsubscribe) {
                if (unsubscribeAllSubcategories == false) {
                    UnsubscribeService.Unsubscribe(
                        db: handler.DB,
                        code: code,
                        email: email,
                        business: business,
                        user: user,
                        category: category,
                        subcategory: subcategory
                       );
                } else {
                    UnsubscribeService.Unsubscribe(
                       db: handler.DB,
                       code: code,
                       email: email,
                       business: business,
                       user: user,
                       category: category,
                       subcategory: "*"
                      );
                }
                handler.DB.SaveChanges();
            }


            // Variables
            handler.Template.InsertVariable("query", UnsubscribeService.GetUnsubscribeQueryString(code, email, business, user, category, subcategory, action));
            handler.Template.InsertVariable("unsubscriber", info.GetRecepientName(handler.DB, email));
            var categoryParsed = UnsubscribeService.GetCategoryName(category);
            handler.Template.InsertVariable("category", categoryParsed);
            handler.Template.InsertVariable("subcategory", UnsubscribeService.GetSubCategoryName(categoryParsed, subcategory));
            handler.Template.InsertVariable("action.unsubscribe", unsubscribe);

            // Manage
            IQueryable<MailingUnsubscription> unsubscriptions = UnsubscribeService.GetUnsubscriptions(handler.DB, code, username, businessId, email);

            // Available settings are all if none are provided via parameter
            if (availableSettings == null) {
                availableSettings = GetAvailableSettings();
            }

            List<SubscriptionSetting> disabledSettings = GetDisabledSettings(unsubscriptions);
            IEnumerable<SubscriptionSetting> selected = GetSelectedSettings(availableSettings, disabledSettings);
            var categoryTemplates = new List<PageTemplate>();
            foreach (var setting in availableSettings.GroupBy(s => s.Category)) {
                var template = handler.Template.LoadTemplate("manage.category");
                // Notification Settings
                template.InsertSelectionList(
                    variableName: "settings",
                    entities: setting.AsQueryable(),
                    selectedEntities: selected,
                    form: new FormBuilder(Forms.Frontend.SELECTION),
                    selectionAPICall: apiUrl + "?id={entity.public-id}" + $"&code={code}&service={service}&email={email}&business={businessId}&user={username}"
                    );
                template.InsertVariable("category.name", setting.Key);
                categoryTemplates.Add(template);

            }
            handler.Template.InsertTemplates("categories", categoryTemplates);
        }

        // Selection List of Services/Categories to enable/disable
        private static IEnumerable<SubscriptionSetting> GetSelectedSettings(IEnumerable<SubscriptionSetting> availableSettings, List<SubscriptionSetting> disabledSettings) {
            // return availableSettings.Where(a => !disabledSettings.Select(ds => ds.PublicId).Contains(a.PublicId));
            var selectedSettings = new List<SubscriptionSetting>();
            foreach(var available in availableSettings.GroupBy(ds => ds.Category)) {
                if (disabledSettings.Any(d=>d.Category == available.Key && d.SubCategoryKey == "*")) {
                    // selectedSettings.AddRange(availableSettings.Where(s => s.Category == disabled.Key));

                    // None is selected for this category
                    continue;
                } else {
                    selectedSettings.AddRange( availableSettings.Where(a => a.Category == available.Key && !disabledSettings.Select(ds => ds.PublicId).Contains(a.PublicId)));
                }
            }
            return selectedSettings;
        }

        private static List<SubscriptionSetting> GetDisabledSettings(IQueryable<MailingUnsubscription> unsubscriptions) {

            // Disabled Settings/Notifications
            var disabledSettings = new List<SubscriptionSetting>();
            foreach (var unsubscription in unsubscriptions) {
                disabledSettings.Add(new SubscriptionSetting() {
                    Category = unsubscription.Category.ToString(),
                    SubCategoryKey = unsubscription.SubCategory
                });
            }

            return disabledSettings;
        }

        public static List<SubscriptionSetting> GetAvailableSettings() {

            // Available Settings
            var availableSettings = new List<SubscriptionSetting>();
            foreach (var categoryInfos in UnsubscribeService.GetCategories()) {
                foreach (var subCategory in categoryInfos.Value) {
                    availableSettings.Add(new SubscriptionSetting() { Category = categoryInfos.Key, SubCategoryKey = subCategory.Item1, SubCategoryTitle = subCategory.Item2 });
                }
            }

            return availableSettings;
        }

        public static void AddBouncedRecepient(ModelContext db, string code, string email) {
            Unsubscribe(db, Targets.Email, Categories.HardBounce.ToString(), code, email);
        }

    }

    public class SubscriptionSetting : ModelObject {
        public override string PublicId
        {
            get
            {
                return $"{this.Category.ToLower()}.{this.SubCategoryKey}";
            }
        }
        public string Category { get; set; }
        [FormBuilder(Forms.Frontend.SELECTION)]
        public string SubCategoryTitle { get; set; }
        public string SubCategoryKey { get; set; }

        public override string ToString() {
            return this.PublicId;
        }


        public override bool Equals(object obj) {
            SubscriptionSetting ss = obj as SubscriptionSetting;
            if (ss != null && ss.PublicId == this.PublicId) {
                return true;
            }
            return false;
        }

        public override int GetHashCode() {
            return this.PublicId.GetHashCode();
        }
    }


    public class UnsubscribeInfo {
       public Targets Target { get; set; }
        public string Identifier { get; set; }
        public User GetUser(ModelContext db) {
            if (this.Target != Targets.User) {
                return null;
            }
            var user = db.Users().GetByUsername(this.Identifier, false);
            if (user == null) {
                user = db.Users().GetByPublicId(this.Identifier);
            }
            return user;
        }

        public Business GetBusiness(ModelContext db) {
            if (this.Target != Targets.Business) {
                return null;
            }
            return db.Businesses().GetByPublicId(this.Identifier);
        }

        /// <summary>
        /// Gets the name of the recepient. Username if User, Business name if business, otherwise email address
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public string GetRecepientName(ModelContext db, string email) {
            string unsubscriber = null;
            if (this.Target == MailingUnsubscription.Targets.Business) {
                unsubscriber = this.GetBusiness(db).Name;
            } else if (this.Target == MailingUnsubscription.Targets.User) {
                unsubscriber = this.GetUser(db).Name;
            }
            else if (this.Target == Targets.Email) {
                unsubscriber =  email;
            }
            return unsubscriber;
        }

    }
}
