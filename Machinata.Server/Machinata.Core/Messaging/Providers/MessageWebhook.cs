using Machinata.Core.Exceptions;
using Machinata.Core.Messaging.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;
using System.Web;
using System.Security.Cryptography;
using System.IO;

namespace Machinata.Core.Messaging.Providers {
    public class MessageWebhook {

        // TODO VIA REFLECTION
        internal static IMessageWebhook GetProvider(string providerName) {
            if (Core.Config.EnabledWebhooks.Contains(providerName)) {
                if (providerName == "mailgun") {
                    return new MailGunWebhook();
                }
                else if (providerName == "mailgun_json") {
                    return new MailGunWebhookNew();
                }
            }
            throw new BackendException("provider-error", "Unknown provider: " + providerName);
        }
    }


    /// <summary>
    /// Legacy 
    /// https://documentation.mailgun.com/en/latest/api-webhooks-deprecated.html#api-webhooks-deprecated
    /// </summary>
    /// <seealso cref="Machinata.Core.Messaging.Interfaces.IMessageWebhook" />
    public class MailGunWebhook : IMessageWebhook {

        public const string MESSAGE_TYPE_DROPPED = "dropped";
        public const string MESSAGE_TYPE_BOUNCED = "bounced";


        public void HandleNotification(ModelContext db, HttpContext context) {
            var eventName = context.Request["event"];
            var code = context.Request["code"];
            var error = context.Request["error"];
            var recipient = context.Request["recipient"];
            var timestamp = context.Request["timestamp"];
            var token = context.Request["token"];
            var signature = context.Request["signature"];

            if (eventName != MESSAGE_TYPE_BOUNCED && eventName != MESSAGE_TYPE_DROPPED) {
                return; // Ignore other than bounced or dropped
            }

            CheckSignature(timestamp, token, signature);

            // TODO: optionally check tokens cant be used twice: e.g. cache the last two hundreds.


            // Todo check error type??
            UnsubscribeService.AddBouncedRecepient(db, code, recipient);

        }

        public static void CheckSignature(string timestamp, string token, string signature) {
            var apiKey = Core.Config.MailgunApiKey;
            var hmac = new HMACSHA256(Encoding.ASCII.GetBytes(apiKey));
            var signCalculatedBytes = hmac.ComputeHash(Encoding.ASCII.GetBytes(timestamp + token));
            var signCalculatedHex = Encryption.EncryptionHelper.Hex(signCalculatedBytes);

            if (signCalculatedHex != signature) {
                throw new BackendException("signature-invalid", "Invalid call");
            }
        }
    }

    /// <summary>
    /// New Mailgun Webhook with application/json payload 
    /// </summary>
    /// <seealso cref="Machinata.Core.Messaging.Interfaces.IMessageWebhook" />
    public class MailGunWebhookNew : IMessageWebhook {
      

        public void HandleNotification(ModelContext db, HttpContext context) {
            // Read json from payload
            var dataRaw = "";
            context.Request.InputStream.Position = 0;
            using (StreamReader inputStream = new StreamReader(context.Request.InputStream)) {
                dataRaw =  inputStream.ReadToEnd();
            }

            var data = Core.JSON.ParseJsonAsJObject(dataRaw);
            var signature = data["signature"];
            var evendData = data["event-data"];

            var reason = evendData["reason"]?.ToString(); // e.g. "bounce"
            var eventType = evendData["event"]?.ToString(); // "failed"
            var severity = evendData["severity"]?.ToString(); // permanent
            var recipient = evendData["recipient"]?.ToString(); // permanent

            // Signature
            MailGunWebhook.CheckSignature(signature["timestamp"]?.ToString(), signature["token"]?.ToString(), signature["signature"]?.ToString());

            // Bounces
            if (eventType == "failed" && severity == "permanent") {
                UnsubscribeService.AddBouncedRecepient(db, eventType, recipient);
            } else if (eventType == "rejected" || eventType == "complained") {
                UnsubscribeService.AddBouncedRecepient(db, eventType, recipient);
            }

        }


    }
}
