using Machinata.Core.Messaging.Interfaces;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Model.MailingUnsubscription;

namespace Machinata.Core.Messaging {

    /// <summary>
    /// Provides Methods to Send Emails
    ///  !!!! Does not check if an Email is unsubscribed !!!!
    /// Use EmailTemplate.Send(...) Methods to automatically no send unsubscribed email addresses
    /// </summary>
    public class MessageCenter {

        public static void SendMessageToAdminEmail(
            string subject,
            string content,
            string senderPackage, 
            IEnumerable<EmailAttachment> attachements = null, 
            bool html = false,
            bool async = true,
            string bcc = null
            ) {
            var sender = EmailSender.DefaultEmailSender;

            //Prefix subject for system messages
            subject = CreateAdminSubject(subject);

            foreach (var adminEmail in Config.AdminEmails) {
                EmailService.SendEmail(adminEmail, null, bcc, subject, content, html, sender, null, async);
                LogSystemMessageEmail(sender.EmailSenderEmail, adminEmail, nameof(EmailSender), subject, content, attachements);
            }
        }

        public static void SendMessageToNotificationEmail(
           string subject,
           string content,
           string senderPackage,
           IEnumerable<EmailAttachment> attachements = null,
           bool html = false,
           bool async = true,
           string bcc = null
           ) {
            var sender = EmailSender.DefaultEmailSender;

            //Prefix subject for system messages
            subject = CreateNotificationSubject(subject);

            if (Config.NotificationEmails != null && Config.NotificationEmails.Any()) {

                var contacts = MessageCenter.ParseContactsList(Config.NotificationEmails);
                var bccContacts = MessageCenter.ParseContactsList(bcc);
                EmailService.SendEmail(contacts, null, bccContacts, subject, content, html, sender, null, async);
                LogSystemMessageEmail(sender.EmailSenderEmail, contacts, null, bccContacts, nameof(EmailSender), subject, content, attachements);
            } else {

                // Fallback
                foreach (var adminEmail in Config.AdminEmails) {
                    EmailService.SendEmail(adminEmail, null, bcc, subject, content, html, sender, null, async);
                    LogSystemMessageEmail(sender.EmailSenderEmail, adminEmail, nameof(EmailSender), subject, content, attachements);
                }
            }
        }

        private static string CreateAdminSubject(string subject) {
            var subjectPrefix = Core.Config.IsTestEnvironment ? "TEST: " : string.Empty;
            subjectPrefix += Core.Config.EmailAdminSubjectPrefix;
            subject = subjectPrefix + subject + Core.Config.EmailAdminSubjectPostfix;
            subject = ReplaceSubjectVariables(subject);
            return subject;
        }

        private static string CreateNotificationSubject(string subject) {
            var subjectPrefix = Core.Config.IsTestEnvironment ? "TEST: " : string.Empty;
            subjectPrefix += Core.Config.EmailNotificationSubjectPrefix;
            subject = subjectPrefix + subject + Core.Config.EmailNotificationSubjectPostfix;
            subject = ReplaceSubjectVariables(subject);
            return subject;
        }

        public static void SendMessageToEmail(
          IEnumerable<EmailContact> to,
          IEnumerable<EmailContact> cc,
          IEnumerable<EmailContact> bcc,
          string subject,
          string content,
          string senderPackage,
          IEnumerable<EmailAttachment> attachements = null,
          MailAddress replyTo = null,
          bool async = true  
          ) {
            var sender = EmailSender.DefaultEmailSender;
            if (replyTo != null) {
                sender.ReplyToEmail = replyTo.Address;
                sender.ReplyToName = replyTo.DisplayName;
            }

            //Prefix subject for user emails
            var subjectPrefix = Core.Config.IsTestEnvironment ? "TEST: " : string.Empty;
            subjectPrefix += Core.Config.EmailSubjectPrefix;
            subject = subjectPrefix + subject;
            subject = ReplaceSubjectVariables(subject);

            EmailService.SendEmail(to, cc, bcc, subject, content, true, sender, attachements, async);
            LogSystemMessageEmail(sender.EmailSenderEmail, to, cc, bcc, nameof(EmailSender), subject, content, attachements);
        }

        public static List<EmailContact> ParseContactsList(string addresses) {
            var contacts = new List<EmailContact>();
            if (!string.IsNullOrWhiteSpace(addresses)) {
                foreach (var address in addresses.Split(',')) {
                    contacts.Add(new EmailContact() { Address = address });
                }
            }
            return contacts;
        }

        public static List<EmailContact> ParseContactsList(List<string> addresses) {
            var contacts = new List<EmailContact>();
            foreach (var address in addresses) {
                contacts.Add(new EmailContact() { Address = address });
            }
            return contacts;
        }

        private static void LogSystemMessageEmail(string emailSenderEmail, IEnumerable<EmailContact> to, IEnumerable<EmailContact> cc, IEnumerable<EmailContact> bcc, string service , string subject, string content, IEnumerable<EmailAttachment> attachements) {
            var receiver = Core.JSON.Serialize(new { To = to, CC = cc, BCC = bcc });
            LogSystemMessageEmail(emailSenderEmail, receiver, service, subject, content, attachements);
        }

        private static SystemMessage LogSystemMessageEmail(string sender, string receiver, string service, string subject, string content, IEnumerable<EmailAttachment> attachements = null ) {
            using(var db = ModelContext.GetModelContext(null)) {
                var systemMessage = new SystemMessage();
                systemMessage.Sender = sender;
                systemMessage.Receiver = receiver;
                systemMessage.Service = service;

                var contentJson = new { Subject = subject, Content = content };
                systemMessage.Content = Core.JSON.Serialize(contentJson);

                // TODO: we save the attachemnts in db?

                db.SystemMessages().Add(systemMessage);
                db.SaveChanges();
                return systemMessage;
                 
            }
        }

        private static string ReplaceSubjectVariables(string subject) {
            subject = subject.Replace("\r", " ").Replace("\n", " ");
            subject = subject.Replace("{project-name}",Core.Config.ProjectName);
            subject = subject.Replace("{machine-id}",Core.Config.MachineID);
            subject = subject.Replace("{build-id}",Core.Config.BuildID);
            subject = subject.Replace("{environment}",Core.Config.Environment);
            return subject;
        }

        /// <summary>
        /// Gets the available message senders through reflection.
        /// </summary>
        /// <returns>A list of full type names to the MessageSender class.</returns>
        public static IList<Type> GetAvailableMessageSenders() {
            // Use reflection to gather all tasks
            IList<Type> messageSenderTypes = new List<Type>();
            var types = Core.Reflection.Types.GetMachinataTypes();
            foreach (Type type in types) {
                System.Type t = type;
                if (t.GetInterfaces().Contains(typeof(IMessageSender))) {
                    if (!type.IsAbstract) messageSenderTypes.Add(type);
                }
            }
            return messageSenderTypes;
        }

        public static IList<EmailTemplate> GetTestTemplates(ModelContext db) {
            var templates = new List<EmailTemplate>();
            foreach (var sender in GetAvailableMessageSenders()) {
                ConstructorInfo constructor = sender.GetConstructor(Type.EmptyTypes);
                object senderObject = constructor.Invoke(new object[] { });
                MethodInfo method = sender.GetMethod(nameof(IMessageSender.GetTestTemplate));
                var template = method.Invoke(senderObject, new object[] { db }) as EmailTemplate;
                if (template != null) {
                    templates.Add(template);
                }
            }
            return templates;
        }

       
    }

    public class EmailAttachment {
        public string Name { get; set; }
        public Stream Content { get; set; }
    }

    public class EmailSender {
      
        public static EmailSender DefaultEmailSender
        {
            get
            {
                var emailSender = new EmailSender();
                emailSender.Server= Core.Config.GetStringSetting("EmailServer");
                emailSender.Port = Core.Config.GetStringSetting("EmailPort");
                emailSender.Username = Core.Config.GetStringSetting("EmailUsername");
                emailSender.Password = Core.Config.GetStringSetting("EmailPassword");
                emailSender.EmailSenderEmail = Core.Config.GetStringSetting("EmailSenderEmail",emailSender.Username);
                emailSender.SenderName = Core.Config.GetStringSetting("EmailSenderName",emailSender.EmailSenderEmail);
               
                emailSender.ReplyToName = Core.Config.GetStringSetting("EmailReplyToName");
                emailSender.ReplyToEmail = Core.Config.GetStringSetting("EmailReplyToEmail");
                return emailSender;
            }
        }

        public string OverrideAddress { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Port { get; private set; }
        public string EmailSenderEmail { get; private set; }
        public string SenderName { get; set; }
        public string Server { get; private set; }
        public string ReplyToName { get; set; }
        public string ReplyToEmail { get; set; }
    }

    /// <summary>
    /// Encapsulted Email Contact (Address and Name)
    /// </summary>
    public class EmailContact {
        public string Address { get; set; }
        public string Name { get; set; }
}

}
