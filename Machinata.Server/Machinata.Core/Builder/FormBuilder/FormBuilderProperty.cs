using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    public abstract class FormBuilderProperty {
        
        public FormBuilder Form;

        
        public FormBuilderProperty(FormBuilder form) {
            this.Form = form;
        }
        
        protected abstract Type _getPropertyRawType();
        
        public abstract Type GetEntityType();
        public abstract Type GetPropertyType();
        public abstract string GetPropertyName();
        public abstract object GetPropertyValue();

        public virtual string GetUniversalPropertyValue(string language) {
            Type type = GetPropertyType();
            if (type == typeof(DateTime)) {
                object val = GetPropertyValue();
                if (val == null) return null;
                return Core.Util.Time.GetUTCMillisecondsFromDateTime((DateTime)val).ToString();
            } else if (type == typeof(DateRange)) {
                object val = GetPropertyValue();
                if (val == null) return null;
                var valDateRage = (DateRange)val;
                if (valDateRage.Start == null || valDateRage.End == null) return null;
                return 
                    Core.Util.Time.GetUTCMillisecondsFromDateTime(valDateRage.Start.Value).ToString()
                    + Core.Config.DateRangeSeperator
                    + Core.Util.Time.GetUTCMillisecondsFromDateTime(valDateRage.End.Value).ToString() ;
            } else if (typeof(ModelObject).IsAssignableFrom(type)) {
                var val = GetPropertyValue();
                if (val == null) return null;
                var entity = val as ModelObject;
                if (entity == null) return null;
                return entity.PublicId;
            }else if (type == typeof(Properties)) {
                object val = GetPropertyValue();
                if (val == null) return null;
                return val.ToString();
            }  else { 
                // Fallback
                return GetFormattedPropertyValue(language);
            }
        }

        public virtual string GetFormattedPropertyValue(string language) {
            // No val?
            object val = GetPropertyValue();
            if (val == null) return null;
            // Init
            string format = GetFormFormat();
            Type type = GetPropertyType();
            // Special type handinling...
            if (type == typeof(DateTime)) {
                // Date
                var dt = (DateTime)val;
                if (dt == DateTime.MinValue) return null;
                var date = Core.Util.Time.ConvertToDefaultTimezone(dt);
                return date.ToString(format != null ? format : Core.Config.DateTimeFormat);
            } else if (type == typeof(DateRange)) {
                // Date Range
                var range = (DateRange)val;
                if (range == null) return "";
                return range.ToString(format);
            } else if (type == typeof(TimeSpan)) {
                // TimeSpan 
                var ts = (TimeSpan)val;
                if (ts.Ticks == 0) return null;
                return ts.ToString(format);

            } else if (type == typeof(Properties)) {
                // Properties
                var properties = (Properties)val;
                return properties.ToHumanReadableString(", ");
            } else if (val is Enum) {
                // Enum
                //return System.Enum.GetName(type, (Enum)val);
                return ((Enum)val).GetEnumTitle(language);
            } else if (val is decimal && format != null) {
                // Decimal
                return ((decimal)val).ToString(format);
            }else if (val is decimal? && format != null) {
                // Decimal?
                return ((decimal?)val).Value.ToString(format);
            } else if (val is double && format != null) {
                // Double
                return ((double)val).ToString(format);
            } else if (val is double? && format != null) {
                // Double?
                return ((double?)val).Value.ToString(format);
            } else if (type.Name.Contains("ICollection") || type.Name.Contains("IEnumerable")) {
                // Collection
                var col = (IEnumerable<object>)val;
                if (col == null) return null;
                return string.Join(", ", col.Select(o => o.ToString()));
            } else if (val is bool) {
                // Bool
                var b = (bool)val;
                if (b == true) return "{text.true}";
                else return "{text.false}";
            }  else if (val is bool?) {
                // Bool?
                var b = (bool?)val;
                if (b == null) return "";
                else if (b.Value == true) return "{text.true}";
                else return "{text.false}";
            } else {
                // Fallback
                return val.ToString();
            }
        }

        private string _getFirstTranslationForTextIds(string language, string[] textIds) {
            foreach (var textId in textIds) {
                var formattedTextId = textId.Replace("{property.name}", GetPropertyName()?.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{entity.type-name}", GetEntityType()?.Name.ToDashedLower());
                formattedTextId = formattedTextId.Replace("{form.id}", this.Form?.Id?.Trim('/').Replace("/", "-").ToDashedLower());
                var translation = Core.Localization.Text.GetTranslatedTextByIdForLanguage(formattedTextId, language, null);
                if (translation != null) return translation;
            }
            return null;
        }

      

        /// <summary>
        /// Gets the property title.
        /// Try to find translation if it eixsts, otherwise fallback to a generated title.
        /// The priority is as follows:
        ///     "{entity.type-name}.{property.name}.{form.id}",
        ///     "{entity.type-name}.{property.name}",
        ///     "{property.name}.{form.id}",
        ///     "{property.name}"
        ///     [generated title]
        /// Example: CustomerSubtotal on Entity LineItem in Form PDF (/pdf)
        ///          line-item.customer-subtotal.pdf
        ///          line-item.customer-subtotal
        ///          customer-subtotal
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public virtual string GetPropertyTitle(string language) {
            string[] textIds = {
                "{entity.type-name}.{property.name}.{form.id}",
                "{entity.type-name}.{property.name}",
                "{property.name}.{form.id}",
                "{property.name}"
            };
            var translation = _getFirstTranslationForTextIds(language, textIds);
            if (translation != null) return translation;
            else return GetPropertyName().ToSentence(); // fallback
        }

        /// <summary>
        /// Gets the property title.
        /// Try to find translation if it eixsts, otherwise fallback to a generated title.
        /// The priority is as follows:
        ///     "{entity.type-name}.{property.name}.{form.id}.tooltip",
        ///     "{entity.type-name}.{property.name}.tooltip",
        ///     "{property.name}.{form.id}.tooltip",
        ///     "{property.name}.tooltip"
        ///     null
        /// Example: CustomerSubtotal on Entity LineItem in Form PDF (/pdf)
        ///          line-item.customer-subtotal.pdf.tooltip
        ///          line-item.customer-subtotal.tooltip
        ///          customer-subtotal.tooltip
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public virtual string GetPropertyTooltip(string language) {
            string[] textIds = {
                "{entity.type-name}.{property.name}.{form.id}.tooltip",
                "{entity.type-name}.{property.name}.tooltip",
                "{property.name}.{form.id}.tooltip",
                "{property.name}.tooltip"
            };
            var translation = _getFirstTranslationForTextIds(language, textIds);
            if (translation != null) return translation;
            else return null; // fallback
        } 

      
        public virtual string GetPropertiesPropertyTooltip(string language, string key) {
            string[] textIds = {
                "{entity.type-name}.{property.name}." +key+ ".tooltip",
                "{property.name}."+key+".tooltip",
                "{entity.type-name}.{property.name}." +key.ToDashedLower()+ ".tooltip",
                "{property.name}."+key.ToDashedLower()+".tooltip",
            };
            var translation = _getFirstTranslationForTextIds(language, textIds);
            if (translation != null) return translation;
            else return null; // fallback
        }

        /// <summary>
        /// Gets the property title.
        /// Try to find translation if it eixsts, otherwise fallback to a generated title.
        /// The priority is as follows:
        ///     "{entity.type-name}.{property.name}.{form.id}.help",
        ///     "{entity.type-name}.{property.name}.help",
        ///     "{property.name}.{form.id}.help",
        ///     "{property.name}.help"
        ///     null
        /// Example: CustomerSubtotal on Entity LineItem in Form PDF (/pdf)
        ///          line-item.customer-subtotal.pdf.help
        ///          line-item.customer-subtotal.help
        ///          customer-subtotal.help
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public virtual string GetPropertyHelp(string language) {
            string[] textIds = {
                "{entity.type-name}.{property.name}.{form.id}.help",
                "{entity.type-name}.{property.name}.help",
                "{property.name}.{form.id}.help",
                "{property.name}.help"
            };
            var translation = _getFirstTranslationForTextIds(language, textIds);
            if (translation != null) return translation;
            else return null; // fallback
        }

        public abstract string GetFormType();
        public abstract string GetFormPattern();
        public abstract string GetFormFormat();
        public abstract string GetFormName();
        public abstract int? GetFormMinLength();
        public abstract int? GetFormMaxLength();
        public abstract int? GetFormMinValue();
        public abstract int? GetFormMaxValue();
        public abstract bool GetFormRequired();
        public abstract bool GetFormReadOnly();
        public abstract string GetFormPlaceholder();
        public abstract string GetFormAutoComplete();
        public abstract string GetFormTooltip();
        public abstract Dictionary<string,string> GetFormOptions();

        public abstract List<string> GetContentTypes();

    }

    

    
}
