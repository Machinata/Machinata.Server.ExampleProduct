using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    /// <summary>
    /// Specifying this attribute on a property of a model object will allow
    /// the property to be built into a form automatically.
    /// For example, specifying the form '/admin/database/entity' on a property will allow a selection
    /// for that form path or name returning all properties which have that form registered.
    /// Not specifying a specific form, will allow the property to be used by PageTemplates automatically.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class FormBuilderAttribute : System.Attribute 
    {
        public FormBuilder Form = null;
        public int? Order = null;
        public string Name = null;
        
        public FormBuilderAttribute() {
            this.Form = new FormBuilder();
        }

        public FormBuilderAttribute(FormBuilder form) {
            this.Form = form;
            this.Order = null;
        }

     

        public FormBuilderAttribute(FormBuilder form, int order) {
            this.Form = form;
            this.Order = order;
            throw new NotImplementedException(); //TODO: @dan
        }

        public FormBuilderAttribute(string form) {
            this.Form = new FormBuilder(form);
            this.Order = null;
        }

        public FormBuilderAttribute(string form, string name) {
            this.Form = new FormBuilder(form);
            this.Name = name;
            this.Order = null;
        }

        public FormBuilderAttribute(string form, int order) {
            this.Form = new FormBuilder(form);
            this.Order = order;
            throw new NotImplementedException(); //TODO: @dan
        }
    }
}
