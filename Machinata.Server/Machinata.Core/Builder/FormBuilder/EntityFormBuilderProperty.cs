using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {


    public class EntityFormBuilderProperty : FormBuilderProperty {
        
        public ModelObject Entity;
        public PropertyInfo Property;

        public EntityFormBuilderProperty(ModelObject entity, PropertyInfo prop, FormBuilder form) : base(form) {
            this.Entity = entity;
            this.Property = prop;
        }

        public CustomFormBuilderProperty AsCustom(string formnamePrefix="") {
            var ret = new CustomFormBuilderProperty(this.Form);
            ret.PropertyType = this.GetPropertyType();
            ret.FormFormat = this.GetFormFormat();
            ret.FormMaxLength = this.GetFormMaxLength();
            ret.FormMinLength = this.GetFormMinLength();
            ret.FormName = formnamePrefix + this.GetFormName();
            ret.FormPattern = this.GetFormPattern();
            ret.FormPlaceholder = this.GetFormPlaceholder();
            ret.FormRequired = this.GetFormRequired();
            ret.FormType = this.GetFormType();
            ret.Name = this.GetPropertyName();
            ret.FormValue = this.GetPropertyValue();
            ret.FormTooltip = this.GetFormTooltip();
            ret.FormMinValue = this.GetFormMinValue();
            ret.FormMaxValue = this.GetFormMaxValue();

            return ret;
        }

      

        protected override Type _getPropertyRawType() {
            if (this.Property.PropertyType == typeof(bool?)) return this.Property.PropertyType;
            return Core.Util.Reflection.UnwrapNullableType(this.Property.PropertyType);
        }

        public override object GetPropertyValue() {
            if(this.Property.CanRead) {
                return this.Property.GetValue(this.Entity);
            } else {
                return null;
            }
        }
            
        public override Type GetPropertyType() {
            return this._getPropertyRawType();
        }
            
        public override Type GetEntityType() {
            return this.Entity?.GetType();
        }
            
        public override string GetPropertyName() {
            return this.Property.Name;
        }
        
        public override string GetFormType() {
            return this.Property.GetFormType();
        }

        public override string GetFormPattern() {
            var attr = this.Property.GetCustomAttribute<PatternAttribute>(true);
            if (attr != null) return attr.Regex;
            else return null;
        }

        public override string GetFormFormat() {
            var attr1 = this.Property.GetCustomAttribute<DataTypeAttribute>(true);
            if (attr1 != null && attr1.DataType == DataType.Date) return Core.Config.DateFormat;
            if (attr1 != null && attr1.DataType == DataType.DateTime) return Core.Config.DateTimeFormat;
            if (attr1 != null && attr1.DataType == DataType.Time) return Core.Config.TimeFormat;
            if (attr1 != null && attr1.DisplayFormat != null) return attr1.DisplayFormat.DataFormatString;
            var attr2 = this.Property.GetCustomAttribute<DisplayFormatAttribute>(true);
            if (attr2 != null && attr2.DataFormatString != null) return attr2.DataFormatString;
            return null;
        }

        /// <summary>
        /// Gets the form name for the property
        /// </summary>
        /// <returns></returns>
        public override string GetFormName() {
            
            // Default FormName
            return this.Property.GetFormName(this.Form);
        }

        public override int? GetFormMinLength() {
            var attr = this.Property.GetCustomAttribute<MinLengthAttribute>(true);
            if (attr != null) return attr.Length;
            else return null;
        }

        public override int? GetFormMaxLength() {
            var attr = this.Property.GetCustomAttribute<MaxLengthAttribute>(true);
            if (attr != null) return attr.Length;
            else return null;
        }

        public override int? GetFormMinValue() {
            {
                var attr = this.Property.GetCustomAttribute<RangeAttribute>(true);
                if (attr != null) return Convert.ToInt32(attr.Minimum);
            }
            {
                var attr = this.Property.GetCustomAttribute<MinimumAttribute>(true);
                if (attr != null) return Convert.ToInt32(attr.Value);
            }
            return null;
        }

        public override int? GetFormMaxValue() {
            {
                var attr = this.Property.GetCustomAttribute<RangeAttribute>(true);
                if (attr != null) return Convert.ToInt32(attr.Maximum);
            }
            {
                var attr = this.Property.GetCustomAttribute<MaximumAttribute>(true);
                if (attr != null) return Convert.ToInt32(attr.Value);
            }
            return null;
        }

        public override bool GetFormRequired() {
            var attr = this.Property.GetCustomAttribute<RequiredAttribute>(true);
            if (attr != null) return true;
            else return false;
        }
        
        public override bool GetFormReadOnly() {
            var attr = this.Property.GetCustomAttribute<ReadOnlyAttribute>(true);
            if (attr == null) return false;
            else return attr.IsReadOnly;
        }

        public override string GetFormPlaceholder() {
            var attr = this.Property.GetCustomAttribute<PlaceholderAttribute>(true);
            if (attr != null) return attr.Text;
            else return null;
        }

        public override string GetFormAutoComplete() {
            return null; //TODO
        }

        public override string GetFormTooltip() {
            return null; //TODO
        }

        public override Dictionary<string,string> GetFormOptions() {
            var attrs = this.Property.GetCustomAttributes<DataOptionAttribute>(true);
            if (attrs.Count() == 0) return null;
            var ret = new Dictionary<string, string>();
            foreach(var attr in attrs) {
                ret.Add(attr.Title, attr.Value);
            }
            return ret;

            /*
            return null; // We allow this call, just don't return anything (alternative: refactor this template code to here)
            //throw new NotImplementedException("Entity properties don't support form options. They are automatically derived from the type.");
            */
        }

        public override List<string> GetContentTypes() {
            var attr = this.Property.GetCustomAttribute<ContentTypeAttribute>(true);
            if (attr != null) return attr.ContentTypes;
            else return null;
        }
    }
}
