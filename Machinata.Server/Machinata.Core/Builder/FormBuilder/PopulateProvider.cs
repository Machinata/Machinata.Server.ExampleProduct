using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;

namespace Machinata.Core.Builder {
    public class PopulateProvider : IPopulateProvider {

        private IDictionary<string, string> _data;

        public PopulateProvider(IDictionary<string,string> data, ModelContext db) {
            this._data = data;
            this.DB = db;
        }

        public ModelContext DB
        {
            get; private set;
        }

        public string GetValue(string key) {
            if (_data.ContainsKey(key)) {
                return _data[key];
            }
            throw new KeyNotFoundException("missing key: " + key);
        }
    }


    public class HandlerPrefixPopulateProvider : IPopulateProvider {

        private Core.Handler.Handler _handler;
        private string _prefix;

        public HandlerPrefixPopulateProvider(Core.Handler.Handler handler, string prefix) {
            _handler = handler;
            _prefix = prefix;
        }

        public ModelContext DB {
            get {
                return _handler.DB;
            }
        }

        public string GetValue(string key) {
            key = _prefix + key;
            return _handler.GetValue(key);
        }
    }
}
