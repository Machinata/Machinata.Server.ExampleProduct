using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class CustomFormatAttribute : DisplayFormatAttribute
    {

        public CustomFormatAttribute(string format) : base() {
            this.DataFormatString = format;
        }
    }
}
