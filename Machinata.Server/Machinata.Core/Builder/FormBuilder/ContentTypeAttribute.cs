using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class ContentTypeAttribute : System.Attribute 
    {
        public List<string> ContentTypes = new List<string>();
        
        public ContentTypeAttribute(string types) {
            this.ContentTypes.AddRange(types.Split(','));
        }
    }
}
