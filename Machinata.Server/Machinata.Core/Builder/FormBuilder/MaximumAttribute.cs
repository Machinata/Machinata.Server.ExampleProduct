using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class MaximumAttribute : ValidationAttribute {

        public float Value = float.MaxValue;
        
        public MaximumAttribute(float val) {
            this.Value = val;
        }

        public override bool IsValid(object obj) {
            bool result = true;

            if (obj.GetType() == typeof(Model.IntRange)) {
                var intRange = (Model.IntRange)obj;
                if (intRange.A > this.Value) result = false;
                if (intRange.B > this.Value) result = false;
            } else {
                var val = float.Parse(obj.ToString());
                if (val > this.Value) result = false;
            }

            return result;
        }
    }

}
