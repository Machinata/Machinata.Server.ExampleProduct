using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class PatternAttribute : System.Attribute 
    {
        public const string PATTERN_ALPHANUMERIC = "[A-Za-z0-9]";
        public const string PATTERN_USERNAME = "[A-Za-z0-9-._]";

        public string Regex = null;
        
        public PatternAttribute(string regex) {
            this.Regex = regex;
        }
    }
}
