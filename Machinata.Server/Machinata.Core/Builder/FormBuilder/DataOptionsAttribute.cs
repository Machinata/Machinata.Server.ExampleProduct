using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
   

    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class DataOptionAttribute : Attribute {
        public string Title = null;
        public string Value = null;

        public DataOptionAttribute(string titleAndValue) : base() {
            this.Title = titleAndValue;
            this.Value = titleAndValue;
        }

        public DataOptionAttribute(string title, string value) : base() {
            this.Title = title;
            this.Value = value;
        }
    }
}
