using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    public static class FormBuilderPropertyExtensions {

        /// <summary>
        /// Gets the sort key for a form using reflection.
        /// This method will automatically sort base types before the declared type.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        public static string GetSortKey(this PropertyInfo p) {
            return 
                (p.DeclaringType.BaseType == typeof(Model.ModelObject) ? "0001" : "0000") 
                + "_" 
                + p.DeclaringType.ToString() 
                + "_" 
                + p.MetadataToken;
        }

        public static string GetFormType(this PropertyInfo p) {
            // For DateRanges, we can't set the input type to date, so we overide it here
            if (p.PropertyType == typeof(Model.DateRange)) {
                return "text";
            }
            // Custom attribute?
            var attr = p.GetCustomAttribute<DataTypeAttribute>(true);
            if (attr != null) {
                if (attr.DataType == DataType.Custom) return attr.CustomDataType;
                else return attr.DataType.ToString().ToLower();
            }
            // Ovveride some types
            // @dan: should we event do this? Maybe it would be just easier to always return text (since browsers now do more and more on the type)
            if (p.PropertyType == typeof(bool?)) {
                return "bool";
            }
            if (p.PropertyType == typeof(int)) {
                return "number";
            }
            if (p.PropertyType.IsEnum) {
                return "list";
            } else if(p.PropertyType == typeof(bool)) {
                return "toggle";
            } else if (typeof(Model.ModelObject).IsAssignableFrom(p.PropertyType)) {
                // Model Object
                return "list";
            } 
            // Default
            return "text";
        }

        /// <summary>
        /// Gets the name of the form, which is by default the Properties name
        /// </summary>
        /// <param name="p">The p.</param>
        /// <param name="form">The form, if null no -> default value</param>
        /// <param name="toLower">if set to <c>true</c> [to lower], this is the default setting, only place not used is Export xlsx</param>
        /// <returns></returns>
        public static string GetFormName(this PropertyInfo p, FormBuilder form, bool toLower = true) {

            // Standard Name
            var name = p.Name;

            // Special Name from FormBuilderAttribute?
            if (form != null) {
                // Get the FormBuilderAttribute with Names
                var formBuilderAttribute = p.GetCustomAttributes<FormBuilderAttribute>(true).FirstOrDefault(fba => fba.Form.Id == form.Id && fba.Name != null);

                // Custom Name from FormBuilderAttribute
                if (formBuilderAttribute != null && formBuilderAttribute.Name != null) {
                    name = formBuilderAttribute.Name;
                }
            }

            // To lower default behaviour
            if (toLower) {
                name = name.ToLower();
            }

            return name;
        }

        public static string GetTitle(this PropertyInfo p) {
            return p.Name.ToSentence();
        }

        
    }
}
