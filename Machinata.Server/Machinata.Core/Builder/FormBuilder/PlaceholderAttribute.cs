using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class PlaceholderAttribute : System.Attribute 
    {
        public string Text = null;
        
        public PlaceholderAttribute(string text) {
            this.Text = text;
        }
    }
}
