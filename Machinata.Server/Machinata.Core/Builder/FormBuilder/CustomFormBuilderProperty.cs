using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    public class CustomFormBuilderProperty : FormBuilderProperty {

        public string Name;
        public object DefaultValue;
        public object FormValue;
        public string FormName;
        public string FormType;
        public string FormPattern;
        public int? FormMinLength;
        public int? FormMaxLength;
        public int? FormMinValue;
        public int? FormMaxValue;
        public string FormFormat;
        public bool FormRequired;
        public bool FormReadOnly = false;
        public string FormPlaceholder;
        public string FormAutoComplete;
        public string FormTooltip;
        public Dictionary<string,string> FormOptions;
        public List<string> ContentTypes;
        public Type PropertyType = null;

        
        public CustomFormBuilderProperty(FormBuilder form) : base(form) {
        }

        protected override Type _getPropertyRawType() {
            if (this.PropertyType != null) return this.PropertyType;
            // Derive the type from the form type...
            if (this.FormType == "text") return typeof(string);
            if (this.FormType == "file") return typeof(string);
            if (this.FormType == "password") return typeof(string);
            if (this.FormType == "bool") return typeof(bool);
            if (this.FormType == "checkbox") return typeof(bool);
            if (this.FormType == "date") return typeof(DateTime);
            if (this.FormType == "datetime") return typeof(DateTime);
            if (this.FormType == "daterange") return typeof(DateRange);
            if (this.FormType == "intrange") return typeof(IntRange);
            if (this.FormType == "int") return typeof(int);
            if (this.FormType == "decimal") return typeof(decimal);
            if (this.FormType == "number") return typeof(int);
            if (this.FormType == "list") return typeof(List<string>);
            if (this.FormType == "select") return typeof(List<string>);
            if (this.FormType == "checkboxlist") return typeof(object);
            if (this.FormType == "radiolist") return typeof(object);
            if (this.FormType == "hidden") return typeof(string);
            if (this.FormType == "didoks") return typeof(string);
            if (this.FormType == "jsoncontent") return typeof(string);
            if (this.FormType == "latlon") return typeof(string);
            if (this.FormType == "country") return typeof(string);
            if (this.FormType == "multilinetext") return typeof(string);
            throw new Exception($"The Custom FormBuilder property type '{this.FormType}' is not supported. Please implement the correct type mapping in _getPropertyRawType().");
        }

        public override object GetPropertyValue() {
            if(this.FormValue == null) {
                return this.DefaultValue;
            } else {
                return this.FormValue;
            }
        }

        public override string GetPropertyName() {
            return Name;
        }

        public override string GetPropertyTooltip(string language) {
            var tooltip = GetFormTooltip();
            if (tooltip != null) return tooltip;
            else return base.GetPropertyTooltip(language);
        }

        public override Type GetPropertyType() {
            return _getPropertyRawType();
        }
            
        public override Type GetEntityType() {
            return _getPropertyRawType();
        }

        public override string GetFormType() {
            return this.FormType;
        }

        public override string GetFormPattern() {
            return this.FormPattern;
        }

        public override string GetFormFormat() {
            if (this.FormFormat != null) return this.FormFormat;
            if (this.FormType == "date") return Core.Config.DateFormat;
            if (this.FormType == "datetime") return Core.Config.DateTimeFormat;
            return null;
        }

        public override string GetFormName() {
            return this.FormName;
        }

        public override bool GetFormReadOnly() {
            return this.FormReadOnly;
        }

        public override int? GetFormMinLength() {
            return this.FormMinLength;
        }

        public override int? GetFormMaxLength() {
            return this.FormMaxLength;
        }

        public override int? GetFormMinValue() {
            return this.FormMinValue;
        }

        public override int? GetFormMaxValue() {
            return this.FormMaxValue;
        }

        public override bool GetFormRequired() {
            return this.FormRequired;
        }

        public override string GetFormPlaceholder() {
            return this.FormPlaceholder;
        }

        public override string GetFormAutoComplete() {
            return this.FormAutoComplete;
        }

        public override string GetFormTooltip() {
            return this.FormTooltip;
        }

        public override Dictionary<string,string> GetFormOptions() {
            return this.FormOptions;
        }

        public override List<string> GetContentTypes() {
            return this.ContentTypes;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceForm"></param>
        /// <param name="modelObject"></param>
        /// <param name="formnamePrefix"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static IEnumerable<CustomFormBuilderProperty> GetEntityFormPropertiesAsCustom(FormBuilder deviceForm, ModelObject modelObject, string formnamePrefix = "", string language= null) {
            var deviceProperties = modelObject.GetPropertiesForForm(deviceForm);
            var customProperties = new List<CustomFormBuilderProperty>();
            foreach (var deviceProperty in deviceProperties) {
                var entityFormProperty = new EntityFormBuilderProperty(modelObject, deviceProperty, deviceForm);
                var customProperty = entityFormProperty.AsCustom(formnamePrefix);

                // TODO: refactor with _getFirstTranslationForTextIds logic


                // Name translation
                {
                    string textId = "{entity.type-name}.{property.name}";
                    var formattedTextId = textId.Replace("{property.name}", entityFormProperty.GetPropertyName()?.ToDashedLower());
                    formattedTextId = formattedTextId.Replace("{entity.type-name}", modelObject.GetType()?.Name.ToDashedLower());
                    var translation = Core.Localization.Text.GetTranslatedTextByIdForLanguage(formattedTextId, language, null);
                    customProperty.Name = translation;
                }

                // tooltip translation
                {
                    string textId = "{entity.type-name}.{property.name}.tooltip";
                    var formattedTextId = textId.Replace("{property.name}", entityFormProperty.GetPropertyName()?.ToDashedLower());
                    formattedTextId = formattedTextId.Replace("{entity.type-name}", modelObject.GetType()?.Name.ToDashedLower());
                    var translation = Core.Localization.Text.GetTranslatedTextByIdForLanguage(formattedTextId, language, null);
                    customProperty.FormTooltip = translation;
                }

                customProperties.Add(customProperty);
            }
            return customProperties;
        }


       


    }

}
