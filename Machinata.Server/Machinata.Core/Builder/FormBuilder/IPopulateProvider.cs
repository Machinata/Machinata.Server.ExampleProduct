using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    public interface IPopulateProvider {
        string GetValue(string key);
        ModelContext DB { get; }
    }
}
