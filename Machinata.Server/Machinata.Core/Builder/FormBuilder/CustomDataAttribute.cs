using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
   

    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class CustomDataAttribute : DataTypeAttribute
    {

        public enum CustomDataTypes {
            //
            // Summary:
            //     Represents a geo location or a pixel coordinate etc...
            Location = 10

        }

        public CustomDataAttribute(CustomDataTypes type) : base(type.ToString()) {
         
        }
    }
}
