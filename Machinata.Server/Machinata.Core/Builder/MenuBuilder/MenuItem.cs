using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    public class MenuItem {

        public List<MenuItem> SubItems = new List<MenuItem>();
        
        public string Title;
        public string ID;
        public string Path;
        public string OnClick;
        public string Target;
        public string Classes;
        public string Icon;
        public bool Selected = false;
        public bool Hidden = false;
        public string Sort = "500";

        private string _arn = null;

        public string ARN {
            get {
                if (_arn != null) {
                    return _arn;
                }
                return this.Path;
            }
            set {
                _arn = value;
            }
        }

    }
}
