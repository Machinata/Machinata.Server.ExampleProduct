using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Machinata.Core.Builder {


    public class MenuBuilder {

        private List<MenuSection> _sections = new List<MenuSection>();
        private MenuSection _currentSection = null;

        public MenuBuilder() {

        }

        public MenuBuilder Section(string title, string id, string path = null, string classes = null, string icon = null, bool autoSelectionRequiresFullMatch = false) {
            var newSection = new MenuSection() {
                Title = title,
                ID = id,
                Path = path,
                Classes = classes,
                Icon = icon,
                AutoSelectionRequiresFullMatch = autoSelectionRequiresFullMatch
            };
            AddSection(newSection);
            _currentSection = newSection;
            return this;
        }

        public MenuBuilder AddSection(MenuSection menuSection) {
            _sections.Add(menuSection);
            _currentSection = menuSection;
            return this;
        }

        /// <summary>
        /// Inserts a item to the menu (a link).
        /// If no section exists, a default section is created.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="path">The path.</param>
        /// <param name="target">The target.</param>
        /// <param name="classes">The classes.</param>
        /// <returns></returns>
        public MenuBuilder Item(string title, string id, string path = null, string target = null, string classes = null, string icon = null) {
            if (_currentSection == null) Section("default","default");
            var newItem = new MenuItem() { Title = title, ID = id, Path = path, Target = target, Classes = classes, Icon = icon };
            _currentSection.Items.Add(newItem);
            return this;
        }

        public MenuBuilder SubItem(string title, string id, string path = null, string target = null, string classes = null, string icon = null) {
            if (_currentSection == null) Section("default","default");
            var newItem = new MenuItem() { Title = title, ID = id, Path = path, Target = target, Classes = classes, Icon = icon };
            var currentItem = _currentSection.Items.LastOrDefault();
            if (currentItem == null) throw new Exception("Cannot add sub-item: current section has not items!");
            currentItem.SubItems.Add(newItem);
            return this;
        }


        public MenuBuilder AddItem(MenuItem menuItem) {
            if (_currentSection == null) Section("default", "default");
            _currentSection.Items.Add(menuItem);
            return this;
        }


        public MenuBuilder AddItems(IEnumerable <MenuItem> menuItems) {
            if (_currentSection == null) Section("default", "default");
            _currentSection.Items.AddRange(menuItems);
            return this;
        }


        /// <summary>
        /// Inserts items into the currenct section using CMS pages, where each page is a item.
        /// The pages must have a title element and must have a link element.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="language">The language.</param>
        /// <param name="cmsPath">The CMS path.</param>
        /// <param name="pathPrefix">The path prefix.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">invalid-menu</exception>
        public MenuBuilder ItemsFromCMS(ModelContext db, string language, string cmsPath, string pathPrefix = null) {
            try {
                var pages = db.ContentNodes()
                    .Include("Children.Children.Children")
                    .GetPagesAtPath(cmsPath)
                    .Where(n => n.Published)
                    .OrderBy(n => n.Sort);
                foreach(var page in pages) {
                    var translation = page.TranslationForLanguage(language);
                    if (translation == null) continue;
                    var title = translation.GetContentChildren().Where(n => n.NodeType == ContentNode.NODE_TYPE_TITLE).SingleOrDefault()?.Value;
                    var path = translation.GetContentChildren().Where(n => n.NodeType == ContentNode.NODE_TYPE_LINK).SingleOrDefault()?.Value;
                    if (path == null) path = translation.ShortURL;
                    if (pathPrefix != null) path = pathPrefix + path;
                    this.Item(title, translation.ShortURL, path);
                }
            } catch(Exception e) {
                throw new BackendException("invalid-menu",$"The menu at '{cmsPath}' could not be generated.",e);
            }
            return this;
        }

        /// <summary>
        /// Inserts items into the currenct section using a collection of pages in the CMS.
        /// Each page must have a derived title and short url.
        /// A prefix to the paths can be supplied to allow for proper prefix building.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="language">The language.</param>
        /// <param name="cmsPath">The CMS path.</param>
        /// <param name="pathPrefix">The path prefix.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">invalid-menu</exception>
        public MenuBuilder PagesFromCMS(ModelContext db, string language, string cmsPath, string pathPrefix = null) {
            try {
                var pages = db.ContentNodes().Include("Children.Children").GetPagesAtPath(cmsPath).Where(n => n.Published).OrderBy(n => n.Sort);
                foreach (var page in pages) {
                    var translation = page.TranslationForLanguage(language);
                    if (translation == null) continue;
                    var title = translation.Title;
                    var path = translation.ShortURL;
                    if (pathPrefix != null) path = pathPrefix + path;
                    this.Item(title, translation.ShortURL, path);
                }
            } catch(Exception e) {
                throw new BackendException("invalid-menu",$"The menu at '{cmsPath}' could not be generated.",e);
            }
            return this;
        }

        public List<MenuSection> GetSections() {
            return _sections;
        }

        public MenuBuilder AutoSelect(Handler.Handler handler, bool onlyAllowOneSelectionPerLevel = false) {
            // Sections
            foreach (var section in _sections) {
                if (section.Path != null) {
                    var sectionPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, section.Path, handler.Language);
                    if(section.AutoSelectionRequiresFullMatch == true) {
                        if (handler.RequestPath.Equals(sectionPathProcessed)) section.Selected = true;
                    } else {
                        if (handler.RequestPath.StartsWith(sectionPathProcessed)) section.Selected = true;
                    }
                    
                }
                // Items
                foreach(var item in section.Items) {
                    if(item.Path != null) {
                        var itemPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, item.Path, handler.Language);
                        if (handler.RequestPath.StartsWith(itemPathProcessed)) {
                            if(onlyAllowOneSelectionPerLevel) section.Items.ForEach(i => i.Selected = false);
                            item.Selected = true;
                        }
                    }
                    // Sub-items
                    if(item.SubItems != null) {
                        foreach(var subItem in item.SubItems) {
                            if(subItem.Path != null) {
                                var itemPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, subItem.Path, handler.Language);
                                if (handler.RequestPath.StartsWith(itemPathProcessed)) {
                                    if(onlyAllowOneSelectionPerLevel) item.SubItems.ForEach(i => i.Selected = false);
                                    subItem.Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            return this;
        }


        public MenuBuilder AutoAuthorize(Handler.Handler handler) {
            // Sections
            foreach (var section in _sections) {
                if (section.ARN != null) {
                    //var sectionPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, section.Path, handler.Language);
                    if (handler.User != null && handler.User.HasMatchingARN(section.ARN) == false) {
                        section.Hidden = true;
                    } else if(handler.User == null && section.ARN != AccessPolicy.PUBLIC_ARN) {
                        section.Hidden = true;
                    }
                    // Items
                    foreach (var item in section.Items) {
                        if (item.ARN != null) {
                            //var itemPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, item.Path, handler.Language);
                            if (handler.User != null && handler.User.HasMatchingARN(item.ARN) == false) {
                                item.Hidden = true;
                            } else if (handler.User == null && item.ARN != AccessPolicy.PUBLIC_ARN) {
                                item.Hidden = true;
                            }
                        }
                        // Sub-items
                        if (item.SubItems != null) {
                            foreach (var subItem in item.SubItems) {
                                if (subItem.ARN != null) {
                                    //var itemPathProcessed = Core.Localization.TextParser.ReplaceTextVariablesForData(handler.PackageName, subItem.Path, handler.Language);
                                    if (handler.User != null && handler.User.HasMatchingARN(subItem.ARN) == false) {
                                        subItem.Hidden = true;
                                    } else if (handler.User == null && subItem.ARN != AccessPolicy.PUBLIC_ARN) {
                                        subItem.Hidden = true;
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            return this;
        }

        public MenuBuilder AutoSummarize(int maxLength) {
            foreach(var section in _sections) {
                section.Title = Core.Util.String.CreateSummarizedText(section.Title, maxLength, true, true);
                foreach(var item in section.Items) {
                    item.Title = Core.Util.String.CreateSummarizedText(item.Title, maxLength, true, true);
                }
            }
            return this;
        }

        public static MenuBuilder LoadMenuFromHandlers(PageTemplateHandler handler, bool autoAuthorize = true, bool autoSelect = true) {

            MenuBuilder builder = new MenuBuilder();
            var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(MenuBuilderAttribute));
            foreach (var method in methods) {
                method.Invoke(null, new object[] { builder });
            }
            if(autoAuthorize) builder.AutoAuthorize(handler);
            if(autoSelect) builder.AutoSelect(handler);

            return builder;
        }

    }
}
