using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    /// <summary>
    /// Specify this attribute on a static void method to be called when the application
    /// this signature is required: public static MenuItem GetMenuItem()
    /// </summary>
    /// <seealso cref="System.Attribute" />

    public class MenuBuilderAttribute : Attribute {

      

    }
}
