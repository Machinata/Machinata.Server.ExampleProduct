using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    public class MenuSection {

        public List<MenuItem> Items = new List<MenuItem>();

        public string Title;
        public string ID;
        public string Path;
        public string Classes;
        public string Icon;
        public string Sort;
        public bool Selected = false;
        public bool Hidden = false;
        public bool AutoSelectionRequiresFullMatch;
        public string OnClick;
        private string _arn = null;

        public string ARN {
            get {
                if (_arn != null) {
                    return _arn;
                }
                return this.Path;
            }
            set {
                _arn = value;
            }
        }

        
    }
}
