using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {
    public class OrganizationStructuredData : StructuredData {

        public PostalAddressStructuredData Address;

        public override string Type
        {
            get
            {
                return "Organization";
            }
        }

        public static OrganizationStructuredData LoadDefault(bool includeAddress, ModelContext db) {
            Address address = LoadDefaultAddress(db);
            var organisation = new OrganizationStructuredData();
            organisation.Name = Core.Config.ProjectName;
            // TODO/LATER: social URL's
            organisation.SameAs = new List<string> { Core.Config.ServerURL + "/" };

            if (includeAddress) {
                organisation.Address = PostalAddressStructuredData.Load(address);
            }

            return organisation;
        }

        public static Address LoadDefaultAddress(ModelContext db) {
            var business = db.OwnerBusiness();
            business.Include(nameof(business.DefaultBillingAddress));
            var address = business.DefaultBillingAddress;
            return address;
        }

    }

  
}
