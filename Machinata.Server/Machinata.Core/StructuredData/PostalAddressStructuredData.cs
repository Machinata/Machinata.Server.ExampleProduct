using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class PostalAddressStructuredData : StructuredData {

        public string StreetAddress { get; set; }
        public string AddressLocality { get; set; }
        public string PostalCode { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }


        public override string Type
        {
            get
            {
                return "PostalAddress";
            }
        }

        public static PostalAddressStructuredData Load(Address addressModelObject) {
            var address = new PostalAddressStructuredData();
            address.AddressCountry = addressModelObject.CountryCode?.ToUpperInvariant();
            address.PostalCode = addressModelObject.ZIP;
            address.AddressLocality = addressModelObject.City;
            address.AddressRegion = addressModelObject.StateCode?.ToUpperInvariant();
            address.StreetAddress = addressModelObject.Address1;
            address.Name = addressModelObject.Company;
            return address;
        }

      
    }

  
}
