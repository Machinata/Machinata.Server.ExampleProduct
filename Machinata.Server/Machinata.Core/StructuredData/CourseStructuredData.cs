using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class CourseStructuredData : StructuredData {
      
        public OrganizationStructuredData Provider { get; set; }

        public override string Type
        {
            get
            {
                return "Course";
            }
        }


    }


}
