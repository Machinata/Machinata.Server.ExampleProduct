using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class StructuredDataInfo {


        public IEnumerable<StructuredData> Data
        {
            get { return _structuredData; }
        }

        public void Add(StructuredData structuredData) {
            _structuredData.Add(structuredData);
        }


        private IList<StructuredData> _structuredData = new List<StructuredData>();
    }


}
