using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {
    public class PlaceStructuredData : StructuredData {

      
        public PostalAddressStructuredData Address { get; set; }

        public override string Type
        {
            get
            {
                return "Place";
            }
        }
    }


}
