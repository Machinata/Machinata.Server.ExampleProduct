using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class OfferStructuredData : StructuredData {

        public enum Availabilities  {
            InStock,
            SoldOut,
            PreOrder
        }

        private Availabilities _availablity;

        public string Availability
        {
            get
            {
                return "http://schema.org/" + _availablity;
            }
        }

        public DateTime ValidFrom { get; set; }

        public void SetAvailability(Availabilities availablity) {
            _availablity = availablity;
        }

        public override string Type
        {
            get
            {
                return "Offer";
            }
        }

        public decimal Price { get; set; }

        public string PriceCurrency { get; set; }

    }


}
