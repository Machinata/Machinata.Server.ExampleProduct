using Machinata.Core.Templates;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text;

namespace Machinata.Core.StructuredData {
    public static class StructuredDataPageTemplateExtension {

        public static void InsertStructuredData(this PageTemplate template, StructuredData structuredData) {
            template.StructuredData.Add(structuredData);
        }

        public static void InsertStructuredData(this PageTemplate template, string variableName, StructuredDataInfo data ) {
            if (data?.Data != null) {
                var result = new StringBuilder();
                foreach (var entry in data.Data) {
                    result.AppendLine(entry.ToString());
                }
                template.InsertVariableUnsafe(variableName, result);
            }
        }
    }
}
