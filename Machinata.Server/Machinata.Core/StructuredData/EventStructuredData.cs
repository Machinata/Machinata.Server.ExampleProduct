using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class EventStructuredData : StructuredData {

       
        public PlaceStructuredData Location { get; set; }
        public OfferStructuredData Offers { get; set; }
        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        // later: Person or Performer


        public override string Type
        {
            get
            {
                return "Event";
            }
        }

       

    }


}
