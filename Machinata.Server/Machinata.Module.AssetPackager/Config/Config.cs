using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.AssetPackager {
    public class Config {
        public static string AuthSecretForHeadlessAccess = Core.Config.GetStringSetting("AssetPackagerAuthSecretForHeadlessAccess");
    }
}
