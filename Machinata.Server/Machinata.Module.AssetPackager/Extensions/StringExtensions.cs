﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.AssetPackager.Extensions {
    public static class StringExtensions {

        public static string ToReplacedString(this JToken token, string debug, string debugFlag, string lang, string theme) {
            if (token != null) {
                var val = token.ToString();
                val = val.Replace("{theme}", theme);
                val = val.Replace("{lang}", lang);
                val = val.Replace("{debug}", debug);
                val = val.Replace("{debug-flag}", debugFlag);
                return val;
            }
            return null;
        }
    }
}
