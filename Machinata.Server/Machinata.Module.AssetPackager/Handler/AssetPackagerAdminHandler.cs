
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using System.IO;

namespace Machinata.Module.AssetPackager.Handler {


    public class AssetPackagerAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("asset-packager");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "product",
                Path = "/admin/asset-packager",
                Title = "{text.asset-packager}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/asset-packager")]
        public void Default() {
            var buildInfo = Core.Util.Build.GetProductBuildInfo();

            var templates = new List<PageTemplate>();
            var packages = ExportPageHandler.ListPackageConfigurations();
            foreach( var package in packages) {
                var packageName = Path.GetFileNameWithoutExtension(package);
                var template = this.Template.LoadTemplate("asset-packager.item");
                template.InsertVariable("package.full-name", Path.GetFileName(package));
                template.InsertVariable("package.name", packageName);
                var exportName = ExportPageHandler.GetExportName(packageName, this.Language, string.Empty) + ".zip";
                template.InsertVariable("package.download-name", exportName);
                templates.Add(template);
            }
            this.Template.InsertTemplates("packages", templates);


            this.Template.InsertVariable("build-guid", buildInfo.BuildGUID);

            // Navigation
           

            this.Navigation.Add("asset-packager", "{text.asset-packager}");

        }




    }
}
