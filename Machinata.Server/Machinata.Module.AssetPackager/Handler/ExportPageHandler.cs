using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;           
    
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System.Net; 
using System.Text;
using Machinata.Core.Exceptions;
using System.IO;
using Newtonsoft.Json.Linq;
using Machinata.Module.AssetPackager.Extensions;
using System.Text.RegularExpressions;

namespace Machinata.Module.AssetPackager.Handler {


    class CustomZIPNameEncoder : UTF8Encoding {
        public CustomZIPNameEncoder() : base(true) { }
        public override byte[] GetBytes(string s) {
            s = s.Replace("\\", "/");
            return base.GetBytes(s);
        }
    }

   

    public class ExportPageHandler : Core.Handler.Handler {

        private string _copyStaticDirToPath(string sourceDir, string targetDir) {
            try {
                Core.Util.Files.CopyDirectory(sourceDir, targetDir);
                return sourceDir;
             
            } catch (Exception e) {
                throw new BackendException("export-error", "Could not export static resource directory " + sourceDir + ": " + e.Message, e);
            }
        }

        private string _copyStaticFileToPath(string source, string dest) {
            try {
                var destDir = System.IO.Path.GetDirectoryName(dest);
                System.IO.Directory.CreateDirectory(destDir);
                System.IO.File.Copy(source, dest);
                return dest;
            } catch (Exception e) {
                throw new BackendException("export-error", "Could not export static resource " + source + ": " + e.Message, e);
            }
        }

        private string _downloadFileToPath(string source, string dest) {
            var destDir = System.IO.Path.GetDirectoryName(dest);
            System.IO.Directory.CreateDirectory(destDir);
            WebClient wc = new WebClient();
            wc.DownloadFile(source, dest);
            return dest;
        }

       
        private string _downloadTemplate(string inputName, string outputFile, string dest, string server, string lang, string theme, string debug, string debugFlag, string packageName, string authSecret = null) {
            if (authSecret == null) authSecret = Config.AuthSecretForHeadlessAccess;
            var downloadURL = $"{server}/{inputName}";
            if (downloadURL.Contains("?")) downloadURL += $"&auth-secret={authSecret}";
            else downloadURL += $"?auth-secret={authSecret}";
            try {
                
                
                var path = _downloadFileToPath(
                    source: downloadURL,
                    dest: $"{dest}/{outputFile}"
                );
                var contents = System.IO.File.ReadAllText($"{dest}/{outputFile}");
                //contents = contents.Replace($"?lang={lang}&amp;theme={theme}&amp;build={Core.Config.BuildID}&amp;debug={debug}", "");
                contents = Regex.Replace(contents, @"\?lang=(.)*debug="+debug, ""); // removes stuff like ?lang=en&amp;theme=cinnamon&amp;build=1f25e041-aca8-4b9d-8f3e-0562eff83f2f&amp;debug=false
                contents = contents.Replace($"-bundle.js", $"-bundle-{lang}{debugFlag}.js");
                contents = contents.Replace($"-bundle.css", $"-bundle-{lang}{debugFlag}.css");
                contents = contents.Replace($"-bundle.css", $"-bundle-{lang}{debugFlag}.css");
                contents = contents.Replace($"{Core.Config.ServerURL}/", "/");
                System.IO.File.WriteAllText($"{dest}/{outputFile}", contents);
                return path;
            } catch (Exception e) {
                throw new BackendException("export-error", "Could not export " + outputFile + " from "+downloadURL+": " + e.Message, e);
            }
        }

        [RequestHandler("/asset-packager/version", AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.Text)]
        public void Version() {

            CheckAuthorization();

            this.StringOutput.Clear();
            this.StringOutput.Append(Core.Util.Build.GetProductBuildInfo().ToJSONString());
        }

        [RequestHandler("/asset-packager/download", AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void Export() {

            CheckAuthorization();

            try {

                if (Core.Config.IsTestEnvironment) Core.Util.HTTP.IgnoreSSLCertificates();
                var packageConfigurationName = this.Params.String("export-package");
                if (packageConfigurationName == null) throw new BackendException("invalid-package", "You must specify the package via the export-package parameter.");
                var lang = this.Params.String("export-lang", "en");
                var theme = this.Params.String("export-theme", "default");

                var debug = "false";
                var debugFlag = "";
                if (this.Params.Bool("export-debug", false) == true) {
                    debug = "true";
                    debugFlag = "-debug";
                }

                var fileName = this.Params.String("export-name", Core.Config.ProjectName.ToDashedLower().Replace(" ", "-"));
                string exportName = GetExportName(packageConfigurationName, lang, debugFlag);
                if (!string.IsNullOrEmpty(fileName)) exportName = $"{fileName}-{exportName}";

                var uid = DateTime.UtcNow.Ticks.ToString();
                var dest = Core.Config.CachePath + Core.Config.PathSep + "exports" + Core.Config.PathSep + exportName + "-" + uid + Core.Config.PathSep;
                var destZip = Core.Config.CachePath + Core.Config.PathSep + "exports" + Core.Config.PathSep + exportName + "-" + uid + ".zip";
                var server = Core.Config.PublicURL;

                // Delete if exists
                if (System.IO.Directory.Exists(dest)) {
                    System.IO.Directory.Delete(dest, true);
                }

                // Load Package Configuration from json
                var packageConfiguration = LoadPackageConfiguration(packageConfigurationName + ".json");

                // Entire directories
                if (packageConfiguration["directories"] != null) {
                    foreach (var dir in packageConfiguration["directories"].Children()) {
                        try {
                            var path = _copyStaticDirToPath(
                               sourceDir: Core.Config.BinPath + dir["source"].ToReplacedString(debug, debugFlag, lang, theme),
                               targetDir: dest + dir["destination"].ToReplacedString(debug, debugFlag, lang, theme)
                           );

                        } catch (Exception e) {
                            throw new Exception("Could not process dir " + dir.ToString(), e);
                        }
                    }
                }

                // Bundles from package configuration
                if (packageConfiguration["bundles"] != null) {
                    foreach (var bundle in packageConfiguration["bundles"].Children()) {
                        var bundleSource = server + bundle["source"].ToReplacedString(debug, debugFlag, lang, theme);
                        var bundleDest = dest + bundle["destination"].ToReplacedString(debug, debugFlag, lang, theme);
                        try {
                            var path = _downloadFileToPath(bundleSource, bundleDest);

                            PatchFile(
                                patches: bundle["patches"],
                                path: path,
                                server: server,
                                lang: lang,
                                theme: theme,
                                debugFlag: debugFlag,
                                packageName: packageConfigurationName
                            );

                        } catch (Exception e) {
                            throw new Exception("Could not process " + bundle.ToString() + ". There was a problem downloading the file from " + bundleSource + " to " + bundleDest, e);
                        }
                    }
                }

                // Read static files
                if (packageConfiguration["files"] != null) {
                    foreach (var file in packageConfiguration["files"].Children()) {
                        try {
                            var path = _copyStaticFileToPath(
                               source: Core.Config.BinPath + file["source"].ToReplacedString(debug, debugFlag, lang, theme),
                               dest: dest + file["destination"].ToReplacedString(debug, debugFlag, lang, theme)
                           );

                            PatchFile(
                                patches: file["patches"],
                                path: path,
                                server: server,
                                lang: lang,
                                theme: theme,
                                debugFlag: debugFlag,
                                packageName: packageConfigurationName
                            );

                        } catch (Exception e) {
                            throw new Exception("Could not process " + file.ToString(), e);
                        }
                    }
                }

                // Pages from package configuration
                if (packageConfiguration["pages"] != null) {
                    foreach (var download in packageConfiguration["pages"].Children()) {
                        try {
                            var path = _downloadTemplate(
                                 inputName: download["source"].ToReplacedString(debug, debugFlag, lang, theme),
                                 outputFile: download["destination"].ToReplacedString(debug, debugFlag, lang, theme),
                                 dest: dest,
                                 lang: lang,
                                 theme: theme,
                                 debug: debug,
                                 server: server,
                                 debugFlag: debugFlag,
                                 packageName: packageConfigurationName,
                                 authSecret: null
                             );


                            PatchFile(
                                patches: download["patches"],
                                path: path,
                                server: server,
                                lang: lang,
                                theme: theme,
                                debugFlag: debugFlag,
                                packageName: packageConfigurationName
                            );

                        } catch (Exception e) {
                            throw new Exception("Could not process " + download.ToString(), e);
                        }
                    }
                }

                // Readme
                if (File.Exists($"{Core.Config.BasePath}/README.md")) {
                    _copyStaticFileToPath(
                        source: $"{Core.Config.BasePath}/README.md",
                        dest: $"{dest}/README.md"
                    );
                }


                // Version
                if (packageConfigurationName != "default" || true) {
                    var contents = $"Note: This is asset export was generated using {packageConfigurationName}.json\n";
                    System.IO.File.WriteAllText($"{dest}/PACKAGE-{packageConfigurationName}.txt", contents);
                }
                {
                    var contents = Core.Util.Build.GetProductBuildInfo().ToString();
                    contents += $"debug={debug}\n";
                    contents += $"lang={lang}\n";
                    contents += $"package={packageConfigurationName}\n";
                    System.IO.File.WriteAllText($"{dest}/VERSION.txt", contents);
                }
                {
                    var contents = Core.Util.Build.GetProductBuildInfo().ToJSON();
                    contents.Add("debug", debug);
                    contents.Add("lang", lang);
                    contents.Add("package", packageConfigurationName);
                    System.IO.File.WriteAllText($"{dest}/VERSION.json", Core.JSON.Serialize(data: contents, lowerDashed: true, enumToString: true, indendFormatting: true));
                }

                // Universal patches
                if (packageConfiguration["post-patches"] != null) {
                    foreach (var postPatch in packageConfiguration["post-patches"].Children()) {
                        try {
                            var dirToSearch = dest + postPatch["directory"].ToString();
                            foreach (var filesToSearch in postPatch["files"].ToString().Split(',')) {
                                var files = System.IO.Directory.GetFiles(
                                    dirToSearch,
                                    filesToSearch,
                                    SearchOption.AllDirectories);
                                foreach (var file in files) {
                                    PatchFile(
                                    patches: postPatch["patches"],
                                    path: file,
                                    server: server,
                                    lang: lang,
                                    theme: theme,
                                    debugFlag: debugFlag,
                                    packageName: packageConfigurationName
                                );
                                }
                            }


                        } catch (Exception e) {
                            throw new Exception("Could not process " + postPatch.ToString(), e);
                        }
                    }
                }


                // Zip all contents
                System.IO.Compression.ZipFile.CreateFromDirectory(
            dest,
            destZip,
            System.IO.Compression.CompressionLevel.Fastest,
            false,
            new CustomZIPNameEncoder() //BUG: .net bug in opening zip files in unix
        );

                // Delete dir
                if (System.IO.Directory.Exists(dest)) {
                    System.IO.Directory.Delete(dest, true);
                }

                // Send back
                this.Context.Response.Headers.Add("Cache-Control", "no-cache");
                this.SendFile(destZip, TimeSpan.Zero, exportName + ".zip");


            } catch (Exception e) {
                throw new BackendException("export-error", "There was an error generating the export bundle: " + e.Message, e);
            }
        }

        public static string GetExportName(string packageConfigurationName, string lang, string debugFlag) {
            return $"{packageConfigurationName}-{lang}-{Core.Config.BuildTimestamp.ToString("yyyyMMdd")}-v{Core.Config.BuildVersion}{debugFlag}";
        }

        private static void PatchFile(JToken patches, string path, string server, string lang, string theme, string debugFlag, string packageName) {
            if (patches != null) {
                var contents = System.IO.File.ReadAllText(path);
                foreach (var patch in patches.Children()) {
                    var findStr = patch["find"].ToString();
                    findStr = findStr.Replace("{debug-flag}", debugFlag);
                    findStr = findStr.Replace("{lang}", lang);
                    findStr = findStr.Replace("{theme}", theme);
                    findStr = findStr.Replace("{server}", server);
                    findStr = findStr.Replace("{package-name}", packageName);
                    var replaceStr = patch["replace"].ToString();
                    replaceStr = replaceStr.Replace("{debug-flag}",debugFlag);
                    replaceStr = replaceStr.Replace("{lang}", lang);
                    replaceStr = replaceStr.Replace("{server}", server);
                    replaceStr = replaceStr.Replace("{package-name}", packageName);
                    contents = contents.Replace(findStr, replaceStr);
                }
                System.IO.File.WriteAllText(path, contents);
            }
        }

        private void CheckAuthorization() {
            // Require login if no auth secret provided. This used by the exporter, with authorizes via the auth secret.
            if (string.IsNullOrEmpty(Config.AuthSecretForHeadlessAccess) == true || this.Params.String("auth-secret") != Config.AuthSecretForHeadlessAccess) {
                this.RequireLogin();
            }
        }

        public static JObject LoadPackageConfiguration(string packageConfigurationName) {
            var path = Core.Config.StaticPath + Core.Config.PathSep + "asset-packager" + Core.Config.PathSep + packageConfigurationName;
            if (File.Exists(path)) {
                return JObject.Parse(File.ReadAllText(path));
            }
            throw new FileNotFoundException(path);
        }

        /// <summary>
        /// Provides all asset packages configurations from the dedicated directory
        /// </summary>
        /// <returns>Local paths to the files</returns>
        public static IEnumerable<string> ListPackageConfigurations() {
            var path = Core.Config.StaticPath + Core.Config.PathSep + "asset-packager";
            if (Directory.Exists(path)) {
                return  Directory.GetFiles(path, "*.json");
               // return files.Select(f => Path.GetFileName(f));
            }
            return new List<string>();
        }
    }
}