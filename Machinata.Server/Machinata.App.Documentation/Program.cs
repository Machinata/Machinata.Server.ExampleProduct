using Machinata.Core.Documentation.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.App.Documentation {
    class Program {
        static void Main(string[] args) {

            if (args.Length == 0) {
                throw new ArgumentException("No config file defined");
            }
            var configFile = args[0];


            var configuration = LoadPackageConfiguration(configFile);

            var srcFolder = configuration["src-folder"]?.ToString();
            var destination =  configuration["destination"]?.ToString();
            var clearDestinationFolder = configuration["clear-destination"]?.Value<bool>();

            if (clearDestinationFolder.HasValue == false) {
                clearDestinationFolder = false;
            }

            var packages = new List<DocumentationPackage>();
         
            // Create Documentation Packgages
            foreach (var bundle in configuration["packages"].Children()) {

                var path = bundle["path"]?.ToString();
                var extension = bundle["extension"]?.ToString();
                var prefix = bundle["prefix"]?.ToString();
                var name = bundle["name"]?.ToString();
                var generateMissingNamespacesVal = bundle["generateMissingNamespaceItems"]?.Value<bool>();
                var includeSourceCodeVal = bundle["includeSourceCode"]?.Value<bool>();
                var published = bundle["includeSourceCode"]?.Value<bool>() == true ? true : false;


                // Namespaces
                bool generateMissingNamespaces = false;
                if (generateMissingNamespacesVal != null && generateMissingNamespacesVal.HasValue) {
                    generateMissingNamespaces = generateMissingNamespacesVal.Value;
                }

                // Remove Code?
                bool includeSourceCode = false;
                if (includeSourceCodeVal != null && includeSourceCodeVal.HasValue) {
                    includeSourceCode = includeSourceCodeVal.Value;
                }


                Console.WriteLine("*****************************************");
                Console.WriteLine("Create Documentation Package: ");
                Console.WriteLine("Path: " + path);
                Console.WriteLine("Extension: " + extension);
                Console.WriteLine("Prefix: " + prefix);

                // Package
                var package = DocumentationPackage.ParseFromDirectory(
                    path: srcFolder + Path.DirectorySeparatorChar + path,
                    extension: extension,
                    prefix: prefix, 
                    generateMissingNamespaceItems: generateMissingNamespaces ,
                    name: name,
                    includeSourceCode: includeSourceCode,
                    published: published);
               
                packages.Add(package);

            }

            Console.WriteLine("Writing Documentation Packages to: ");
            Console.WriteLine(destination);

            // Write Documentation Files
            DocumentationPackage.WriteDocumentations(packages, destination, clearDestinationFolder.Value, srcFolder);

        }




        private static JObject LoadPackageConfiguration(string path) {
           // var path = Core.Config.StaticPath + Core.Config.PathSep + "asset-packager" + Core.Config.PathSep + packageConfigurationName;
            if (File.Exists(path)) {
                return JObject.Parse(File.ReadAllText(path));
            }
            throw new FileNotFoundException(path);
        }




    }
}
