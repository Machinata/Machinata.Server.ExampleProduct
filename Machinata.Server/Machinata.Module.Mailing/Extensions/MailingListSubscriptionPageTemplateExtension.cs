using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.Logic {
    public static class MailingListSubscriptionPageTemplateExtension {

        public static void InsertMailingListSubscriptionVariables(
            this PageTemplate parentTemplate,
            ModelContext db, 
            HandlerParams parameters, 
            string variableName, 
            string onSuccess,
            string templateName = "newsletter.form",
            IEnumerable<MailingList> defaultMailingLists = null)
            {

            // API Call
            var api = "/api/mailing/subscribe";
            // Categories
            var categories = db.MailingCategories().Where(mc => mc.Public);

            // Known Contact Id
            var contactId = parameters.String("cid");

            // Fallback Lists
            string [] defaultLists = { };
            if (defaultMailingLists != null) {
                defaultLists = defaultMailingLists.Select(l => l.PublicId).ToArray();
            }

            // List ids
            var listIds = parameters.StringArray("lids", defaultLists);

            // Check lists otherwise take default lists
            var lists = db.MailingLists().GetByPublicIds(listIds, false);
            if (lists.Count() == 0) {
                lists = defaultMailingLists.AsQueryable();
            }

            MailingContact contact = null;

            // Load existing
            if (contactId != null) {
                contact = db.MailingContacts().GetByPublicId(contactId, false);
            }

            // Create new Contact
            if (contact == null) {
                contact = new MailingContact();
            }

            // Load Template
            var template = parentTemplate.LoadTemplate(templateName);

            // Variables
            template.InsertVariable("api-call", api);
            template.InsertVariable("on-success", onSuccess);
            template.InsertVariables("contact", contact);
            template.InsertVariable("mailinglists", string.Join(",",lists.ToList().Select(l=>l.PublicId)));

            // Categories
            template.InsertTemplates("categories", categories, templateName + ".category");

            // Insert to template
            parentTemplate.InsertTemplate(variableName, template);

            // New Variables 
            parentTemplate.DiscoverVariables();

        }

    }
}
