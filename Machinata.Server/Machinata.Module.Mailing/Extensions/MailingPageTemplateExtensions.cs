using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Handler.Page;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.Extensions {
    public static class MailingPageTemplateExtensions {

        public static IQueryable<MailingContact> InsertContactSearch(this PageTemplateHandler pageHandler, IEnumerable<MailingListProperty> properties, IQueryable<MailingContact> contacts) {
         
            var example = new MailingContact();
            var templates = new List<PageTemplate>();

            // Normal Properties Search
            var name = AddPropertySearchTemplate(pageHandler, templates, nameof(example.Name), nameof(example.Name));
            if (string.IsNullOrWhiteSpace(name) == false) {
                contacts = contacts.Where(c => c.Name.Contains(name));
            }
            var language = AddPropertySearchTemplate(pageHandler, templates, nameof(example.Language), nameof(example.Language));
            if (string.IsNullOrWhiteSpace(language) == false) {
                contacts = contacts.Where(c => c.Language.Contains(language));
            }
            var email = AddPropertySearchTemplate(pageHandler, templates, nameof(example.Email), nameof(example.Email));
            if (string.IsNullOrWhiteSpace(email) == false) {
                var emailHash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(email);
                contacts = contacts.Where(c => c.EmailHash == emailHash);
            }

            // List Properties.Properties Search
            foreach (var property in properties) {
                var value = AddPropertySearchTemplate(pageHandler, templates, property.Key, "property." + property.Key);
                Regex regex = new Regex("\"" + property.Key + "\":\"" + "[^\"]*" + value + "[^\"]*", RegexOptions.IgnoreCase);
                if (string.IsNullOrWhiteSpace(value) == false) contacts = contacts.Where(c => c.Properties != null && c.Properties.Data != null && regex.IsMatch(c.Properties.Data));
            }

            var searchTemplate = pageHandler.Template.LoadTemplate("mailing-contacts.search");
            searchTemplate.InsertTemplates("search-properties", templates);
            pageHandler.Template.InsertTemplate("search", searchTemplate);

            return contacts;
        }

        private static string AddPropertySearchTemplate(PageTemplateHandler pageHandler, List<PageTemplate> templates, string name, string key) {
            var propertyTemplate = pageHandler.Template.LoadTemplate("mailing-contacts.search.property");
            propertyTemplate.InsertVariable("property.name", name);
            propertyTemplate.InsertVariable("property.key", key);
            string value = pageHandler.Params.String(key);
            propertyTemplate.InsertVariable("property.value", value);
            var valueDisplay = string.IsNullOrEmpty(value) ? value : ": " + value;
            propertyTemplate.InsertVariable("property.value-display", valueDisplay);
            templates.Add(propertyTemplate);
            if (value != null) {
                return value.ToLowerInvariant();
            }
            return value;
        }

    }
}
