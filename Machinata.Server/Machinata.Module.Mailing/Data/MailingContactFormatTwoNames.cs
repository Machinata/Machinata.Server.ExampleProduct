using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using Machinata.Module.Mailing.Model;

namespace Machinata.Module.Mailing.Data {
    public class MailingContactFormatTwoNames : MailingContactFormat {


        /// <summary>
        /// Used for import/export
        /// </summary>
        [FormBuilder(Forms.Admin.EXPORT)]
        public string LastName { get; set; }

        /// <summary>
        /// Used for import/export
        /// </summary>
        [FormBuilder(Forms.Admin.EXPORT)]
        public string FirstName { get; set; }

        /// <summary>
        /// Used for import/export
        /// </summary>
        [FormBuilder(Forms.Admin.EXPORT)]
        public string Email { get; set; }


        public override MailingContact ConvertToMailingContact() {
            var converted = new MailingContact();
            converted.Name = $"{this.FirstName} {this.LastName}";
            converted.Email = this.Email;
            return converted;
        }

        public override string FormatTitle() {
            return "Name Two Columns";
        }

        public override MailingContactFormat ConvertFrom(MailingContact contact) {
            var converted = new MailingContactFormatTwoNames();
            converted.FirstName = contact.FirstName;
            converted.LastName = contact.LastName;
            converted.Email = contact.Email;
            return converted;
        }
    }

}