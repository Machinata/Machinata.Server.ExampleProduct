using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using Machinata.Module.Mailing.Model;

namespace Machinata.Module.Mailing.Data {
    public class MailingContactFormatFullAddress : MailingContactFormat {


        [FormBuilder(Forms.Admin.EXPORT)]
        public string Name { get; set; }
        
        [FormBuilder(Forms.Admin.EXPORT)]
        public string LastName { get; set; }
        
        [FormBuilder(Forms.Admin.EXPORT)]
        public string FirstName { get; set; }

        [FormBuilder(Forms.Admin.EXPORT)]
        public string Language { get; set; }

        [FormBuilder(Forms.Admin.EXPORT)]
        public string Email { get; set; }

        [FormBuilder(Forms.Admin.EXPORT)]
        public string AdminLink { get; set; }
        
        [FormBuilder(Forms.Admin.EXPORT)]
        public Address Address { get; set; }

        [FormBuilder(Forms.Admin.EXPORT)]
        public Properties Properties { get; set; }

        [FormBuilder(Forms.Admin.EXPORT)]
        public string Country { get; set; }


        public override MailingContact ConvertToMailingContact() {
            throw new NotImplementedException();
        }

        public override string FormatTitle() {
            return "Full Address";
        }

        public override MailingContactFormat ConvertFrom(MailingContact contact) {
            var converted = new MailingContactFormatFullAddress();
            converted.FirstName = contact.FirstName;
            converted.LastName = contact.LastName;
            converted.Name = contact.Name;
            converted.Email = contact.Email;
            converted.Language = contact.Language;
            converted.Address = contact.StructuredAddress;
            if(string.IsNullOrEmpty(contact.StructuredAddress?.Country) == false) {
                var countryTranslated = Core.Localization.TextParser.ReplaceTextVariablesForData("Machinata.Module.Mailing", contact.StructuredAddress?.Country, Core.Config.LocalizationDefaultLanguage);
                converted.Country = countryTranslated;
            } 
          
            converted.AdminLink = Core.Config.PublicURL + "/admin/mailing/contacts/contact/" + contact.PublicId;
        
            if (contact.Properties != null) {
                converted.Properties = new Properties(contact.Properties.ToDictionary());
            } else {
                converted.Properties = new Properties(contact.Properties.ToDictionary());
            }
            return converted;
        }
    }

}