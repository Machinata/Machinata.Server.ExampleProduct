using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Machinata.Module.Mailing.Data {
    public abstract class MailingContactFormat : ModelObject {

        public enum Direction {
            IMPORT,
            EXPORT
        }

        public abstract MailingContact ConvertToMailingContact();

        public abstract string FormatTitle();


        public static MailingContactFormat GetFormat(string name) {
            name = name.ToLowerInvariant();
            var formats = GetFormats();
            if (formats.ContainsKey(name)) {
                return formats[name];
            } else {
                throw new Exception($"Mailing Contact Format '{name}' is not supported");
            }
        }


        private static Dictionary<string, MailingContactFormat> GetFormats() {
            var result = new Dictionary<string, MailingContactFormat>();
            var formatTypes = Core.Reflection.Types.GetMachinataTypes(typeof(MailingContactFormat)).Where(t => !t.IsAbstract);
            foreach (var formatType in formatTypes) {
                result[formatType.Name.ToLowerInvariant()] = Core.Reflection.Types.CreateInstance<MailingContactFormat>(formatType);
            }
            return result;
        }

      

        /// <summary>
        /// Only gets the formats which support import
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, MailingContactFormat> GetFormats(Direction direction) {
            var formatTypes = GetFormats();
            var result = new Dictionary<string, MailingContactFormat>();
            foreach (var formatType in formatTypes) {
                try {
                    if (direction == Direction.EXPORT) {
                        formatType.Value.ConvertFrom(new MailingContact());
                        result[formatType.Key] = formatType.Value;
                    } else if (direction == Direction.IMPORT) {
                        formatType.Value.ConvertToMailingContact();
                        result[formatType.Key] = formatType.Value;
                    }
                } catch (NotImplementedException e) {

                }
            }
            return result;
        }

        public override void Validate() {
            base.Validate();

            foreach(var property in this.GetPropertiesWithAttribute(typeof(FormBuilderAttribute))) {
                var reqAttribute = property.GetCustomAttribute<RequiredAttribute>();
                var value = property.GetValue(this);
                if (reqAttribute != null && value == null) throw new BackendException("invalid-value", $"Please enter a value for {property.Name}.");

                if (value != null) {
                    foreach (var attr in property.GetCustomAttributes<ValidationAttribute>()) {
                        if (!attr.IsValid(property.GetValue(this))) {
                            throw new BackendException("invalid-value", $"Please enter a valid value for {property.Name}.");
                        }
                    }
                }
            }

        }

        public abstract MailingContactFormat ConvertFrom(MailingContact contact);
    }

}