using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.TaskManager;
using Machinata.Core.Messaging.Providers;

namespace Machinata.Module.Mailing.Model {

    [Serializable()]
    [ModelClass]
    public class MailingCategory : ModelObject{

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

      
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public MailingCategory() {
          
            this.Mailings = new List<Mailing>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Visibility to the frontend
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Public { get; set; }
        

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public ICollection<Mailing> Mailings { get; set; }

        // TODO introduce IUnscubscribeSubcategory?
        public string UnsubscribeId
        {
            get { return this.PublicId; }
        }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        public static IEnumerable<SubscriptionSetting> GetSubscriptionCategories(ModelContext db, bool onlyPublic = true) {


            IEnumerable<MailingCategory> mailingCategories = null;
            if (onlyPublic) {
                mailingCategories = db.MailingCategories().Where(mc => mc.Public);
            } else {
                mailingCategories = db.MailingCategories().AsEnumerable();
            }

            var categories = mailingCategories.Select(mc => new SubscriptionSetting() { Category = MailingUnsubscription.Categories.Mailing.ToString(), SubCategoryKey = mc.PublicId, SubCategoryTitle = mc.Name });
            return categories;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextMailingCategoryExtenions {
        [ModelSet]
        public static DbSet<MailingCategory> MailingCategories(this Core.Model.ModelContext context) {
            return context.Set<MailingCategory>();
        }
    }
   
    #endregion



}
