using System;
using System.Collections.Generic;

namespace Machinata.Module.Mailing.Model {
    internal class EmailEqualityComparer : IEqualityComparer<MailingContact> {
        public bool Equals(MailingContact x, MailingContact y) {
            return x.EmailHash.Equals(y.EmailHash);
        }

        public int GetHashCode(MailingContact obj) {
            return obj.EmailHash.GetHashCode();
        }
    }
}