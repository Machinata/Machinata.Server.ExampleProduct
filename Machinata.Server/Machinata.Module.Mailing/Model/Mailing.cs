using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Mailing.Tasks;
using Machinata.Core.Messaging.Interfaces;
using System.Net.Mail;
using Machinata.Module.Mailing.ContentProvider;
using Machinata.Core.Util;
using Machinata.Core.Messaging;
using Machinata.Core.TaskManager;
using System.Linq.Expressions;

namespace Machinata.Module.Mailing.Model {

    [Serializable()]
    [ModelClass]
    public partial class Mailing : ModelObject, IMessageSender, IPublishedModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

        public enum Statuses : short {
            Created = 0,
            ReadyToSend = 10,
            Sending = 20,
            Finished = 30
        }

        public enum Media : short {
            Undefined = 0,
            Email = 10,
            Mail = 20
        }


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Mailing() {
            this.Lists = new List<MailingList>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Media Medium { get; set; } = Media.Email;


        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder("preview")]
        public string Subject { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ContentNode Content { get; set; }

        [Column]
        public ContentNode ContentSnapshot { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder("preview")]
        public string ReplyToEmail { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder("preview")]
        public string ReplyToName { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string ContentProvider { get; set; }

        /// <summary>
        /// Flag will ensure to send only to Contacts (and different contacts with same email address) which dont have the MailinContact.LastMessageSent set
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool OnlySendToUntouchedContacts { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public ICollection<MailingList> Lists { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Required]
        public MailingCategory Category { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.LISTING)]
        public Core.Model.TaskLog.BatchTaskDisplayStatuses Status
        {
            get
            {
                using (var db = ModelContext.GetModelContext(null)) {
                    var task = MailingTask.CreateMailingTask(db);
                    var status = task.GetTaskStatus(this.PublicId);
                    return status.HasValue ? TaskLog.GetDisplayStatusFor(status.Value) : TaskLog.BatchTaskDisplayStatuses.Created;
                }
            }       
        }

        public bool Published
        {
            get
            {
                return Status < TaskLog.BatchTaskDisplayStatuses.Finished; 
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public IEnumerable<MailingContact> GetContacts(ModelContext db) {
            
            this.Include(nameof(this.Lists));
           
            var allContacts = this.Lists.SelectMany(l => l.GetContacts(db));
            if (this.Medium == Media.Email) { 

               
                if (this.OnlySendToUntouchedContacts == false) {
                    // All contacts from selected list with different 
                    return allContacts.Where(c => c.Email != null).Distinct(new EmailEqualityComparer());
                } else {
                    // Ignore 1: all contacts from Database which have been touched
                    var touchedContacts = db.MailingContacts().Where(mc =>mc.EmailHash != null &&  mc.LastMessageSent != null);


                    // Ingore 2. All contacts from mailings which have been sent out (EXCEPT THIS MAILING)
                    var finishedMailingsIds = db.Mailings().ToList().Where(m=> m.Status >= TaskLog.BatchTaskDisplayStatuses.Finished).Select(m =>m.Id);
                    var finishedMailingsWithContacts = db.Mailings()
                        .Include(nameof(Mailing.Lists) + "." + nameof(MailingList.Contacts))
                        .Where(m => finishedMailingsIds.Contains(m.Id) & m.Id != this.Id );
                    var finishedMailingContacts = finishedMailingsWithContacts.SelectMany(m => m.Lists.SelectMany(l => l.Contacts)).Where(c=>c.EmailHash != null);


                    // Contacts from this.Lists, WITHOUT ALREADY TOUCHED CONTACTS
                    var listContacts = allContacts.Where(c => c.Email != null && c.LastMessageSent == null).Distinct(new EmailEqualityComparer());

                    // Remove contacts to ignore from the original list
                    var contacts = listContacts.Except(touchedContacts, new EmailEqualityComparer());
                    contacts = contacts.Except(finishedMailingContacts, new EmailEqualityComparer());

                    // Log excluded
                    //var excluded = allContacts.Except(contacts, new EmailEqualityComparer());
                    //_logger.Info("Excluded contacts: " + excluded.Count());
                    //foreach(var contact in excluded) {
                    //    _logger.Info(contact.Name + " " + contact.Email);
                    //}

                    return contacts;

                }
            }
            else {
                throw new NotImplementedException("Only Email Contacts are supported.");
            }

        }

        /// <summary>
        /// Sends the mailing content to a contact synchoniously
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="contact">The contact.</param>
        public void SendTo(Core.Handler.Handler handler, MailingContact contact, bool sendCopyToAdmin = false) {
            if (this.Medium == Media.Email) {

                // Prepare Mail
                var emailTemplate = this.CreateMailingTemplate(handler, contact);
                this.Include(nameof(this.Category));

                // Customized Sender Name
                MailAddress replyTo = null;
                if (!string.IsNullOrWhiteSpace(this.ReplyToEmail)) {
                    replyTo = new MailAddress(this.ReplyToEmail, this.ReplyToName);
                }

                // Mailing Title
                string mailingTitle = !string.IsNullOrEmpty(this.Name) ? Core.Util.String.CreateShortURLForName(this.Name) : null;

                // Send
                if (contact.Source == MailingUnsubscription.Targets.Email) {
                    var emailContact = new EmailContact();
                    emailContact.Address = contact.Email;
                    emailContact.Name = contact.Name;
                    emailTemplate.SendEmail(emailContact, MailingUnsubscription.Categories.Mailing, this.Category.UnsubscribeId, null, replyTo, mailingTitle, async: false, sendCopyToAdmin: sendCopyToAdmin);
                } else if (contact.Source == MailingUnsubscription.Targets.Business){
                    emailTemplate.SendBusinessEmail(contact.Business, contact.Email, MailingUnsubscription.Categories.Mailing, this.Category.UnsubscribeId, null, replyTo, mailingTitle, async: false);
                } else if (contact.Source == MailingUnsubscription.Targets.User) {
                    emailTemplate.SendUserEmail(contact.User, contact.Email, MailingUnsubscription.Categories.Mailing, this.Category.UnsubscribeId, null, replyTo, mailingTitle, async: false);
                }

                // Log message
                contact.LastMessageSent = DateTime.UtcNow;

            }
        }

        public bool CheckReadyToSend(ModelContext db, bool throwException, bool checkContacts = true) {
            if (string.IsNullOrEmpty(this.Subject)) {
                if (throwException) {
                    throw new BackendException("not-ready", "The subject is missing");
                }
                return false;
            }

            if (checkContacts && this.GetContacts(db).Count() < 1) {
                if (throwException) {
                    throw new BackendException("not-ready", "No contacts to send to");
                }
                return false;
            }
            if (this.ContentSnapshot == null) {
                if (throwException) {
                    throw new BackendException("not-ready", "Generate content or preview before mailing");
                }
                return false;
            }
            if (this.IsStarted(db)) {
                if (throwException) {
                    throw new BackendException("not-ready", "The mailing is already started or has already been sent");
                }
                return false;
            }
            if (this.IsFailed(db)) {
                if (throwException) {
                    throw new BackendException("ready", "The mailing is finished but has failed items. See 'Show failed Contacts'");
                }
                return false;
            }



            return true;
        }

        /// <summary>
        /// Compare the two Conents (productive vs source) --> not working because of content ids
        /// </summary>
        /// <returns></returns>
        public bool AreContentsSame(Core.Handler.PageTemplateHandler handler) {
            throw new NotImplementedException("Not working");
            /*var contentProvider = this.GetContentProvider();

            var parentTemplate = PageTemplate.LoadForPackageNameAndRoutePath("Machinata.Email", "default/mailing", ".htm", false);
            parentTemplate.Language = handler.Language;
            contentProvider.Mailing = this;
            contentProvider.DB = handler.DB;
            contentProvider.ParentTemplate = parentTemplate;
            parentTemplate.Handler = handler;

            var productiveContent = contentProvider.GetContent(MailingContentProvider.ContentType.Productive);
            var sourceContent = contentProvider.GetContent(MailingContentProvider.ContentType.Source);

            // Compare the contents
            return productiveContent == sourceContent;*/
        }

        public IEnumerable<Tuple<string, string>> GetSubCategories(MailingUnsubscription.Categories category) {
            var categories = new List<Tuple<string, string>>();
            if (category == MailingUnsubscription.Categories.Mailing) {
                using (var db = ModelContext.GetModelContext(null)) {
                    foreach (var subcategory in db.MailingCategories()) {
                        categories.Add(new Tuple<string, string>(subcategory.UnsubscribeId, subcategory.Name));
                    }
                }
            }
            return categories;
        }

        public EmailTemplate GetTestTemplate(ModelContext db) {
            return null;
        }

        /// <summary>
        /// Creates and fills an mailing email template
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public EmailTemplate CreateMailingTemplate(Core.Handler.Handler handler, MailingContact contact) {

            var emailTemplate = new EmailTemplate(handler.DB, "mailing");
         
            emailTemplate.Handler = handler;

            MailingContentProvider provider = null;
            provider = GetContentProvider();
            provider.Initialize(this, emailTemplate);

            // Email Content from CMS
            emailTemplate.InsertVariableUnsafe(
                name: "mailing.cms-content",
                value: provider.GetContent(MailingContentProvider.ContentType.Productive)
            );
           
            // Insert entity properties 
            emailTemplate.InsertVariables("mailing", this);

            // Subject - insert mailing and contact variables
            var subjectTemplate = this.Subject.GetPageTemplate();
            subjectTemplate.InsertVariables("contact", contact);
            subjectTemplate.InsertVariables("mailing", this);
            emailTemplate.Subject = subjectTemplate.Content;

            // Personalization
            emailTemplate.InsertVariables("contact", contact);

            return emailTemplate;
        }

        public MailingContentProvider GetContentProvider() {
            MailingContentProvider provider;
            if (!string.IsNullOrEmpty(this.ContentProvider)) {
                provider = MailingContentProvider.GetByName(this.ContentProvider);
            } else {
                provider = new ContentNodeMailingContentProvider();
            }
            return provider;
        }


        public bool HasDefaultContentProvider
        {
            get
            {
                return GetContentProvider() is ContentNodeMailingContentProvider;
            }
        }

        /// <summary>
        /// Contacts which having missing properties or properties values which are set on the lists
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="contacts">The contacts.</param>
        /// <returns></returns>
        public IEnumerable<MailingContact> GetContactsWithMissingProperties(IQueryable<MailingContact> contacts) {
            var allProperties = this.Lists.Select(ml => ml.Properties).ToList().SelectMany(p => p.Keys).Distinct();
            var result = new List<MailingContact>();
            foreach(var property in allProperties) {
                var propertyMissingContacts = contacts.Where(c => !c.Properties.Keys.Contains(property) || string.IsNullOrEmpty(c.Properties[property].ToString()));
                result.AddRange(propertyMissingContacts);
            }
            return result.Distinct();
        }

        public bool IsPauseable(ModelContext db) {
            var task = MailingTask.CreateMailingTask(db);
            return task.IsPauseable(this.PublicId);
        }

        public bool IsResumeable(ModelContext db) {
            var task = MailingTask.CreateMailingTask(db);
            return task.IsResumeable(this.PublicId);
        }

        public bool IsFailed(ModelContext db) {
            var task = MailingTask.CreateMailingTask(db);
            return task.IsFailed(this.PublicId);
        }

        public bool IsStarted(ModelContext db) {
            // Get latest status
            var task = MailingTask.CreateMailingTask(db);
            var latestStatus = task.GetTaskStatus(this.PublicId);

            // Exists?
            if (latestStatus == null) return false; // not started, ever

            // Finished?
            // it's finished, thus it is no longer started
            if (latestStatus == TaskLog.TaskStatuses.Finished) return false;

            //TODO @micha: what about error'd tasks
            if (latestStatus == TaskLog.TaskStatuses.Error) return false;

            // Assume true for everything else
            return true;
        }

        /// <summary>
        /// If the content provider has AutogeneratedContent activated this will create and save it
        /// </summary>
        /// <param name="db">The database.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void PrepareContent(ModelContext db) {
            var contentProvider = this.GetContentProvider();
            if (contentProvider.AutogenerateContent) {
                contentProvider.Initialize(this, db);
                contentProvider.Save();
                db.SaveChanges();
            }
        }

        public void Delete(bool deleteLogs) {

            // Task Logs
            if (deleteLogs) {
                var task = MailingTask.CreateMailingTask(this.Context);
                task.DeleteLogs(this.PublicId);
            }
            
            // Task
            this.Context.DeleteEntity(this);
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
        }

        public override Core.Cards.CardBuilder VisualCard() {
            // Init card
            var statuses = new List<TaskLog.BatchTaskDisplayStatuses> {
                TaskLog.BatchTaskDisplayStatuses.Created,
                TaskLog.BatchTaskDisplayStatuses.Started,
                TaskLog.BatchTaskDisplayStatuses.Running,
                TaskLog.BatchTaskDisplayStatuses.Finished };

            var currentStatus = this.GetTaskStatus();

            var statusForGui = TaskLog.GetDisplayStatusFor(currentStatus);

            string progress = GetStatusDescription();
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Medium)
                .Sub(statusForGui)
                .Sub(progress)
                .Icon("mail")
                .Wide();

            card.Breadcrumb(statuses, statusForGui);

            return card;
        }

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);
           

            var snapshotPath = ContentNode.GetNodePathForEntityProperty(this, nameof(Mailing.ContentSnapshot));
            var snapshot =  ContentNode.GetByPath(db, snapshotPath);

            var contentPath = ContentNode.GetNodePathForEntityProperty(this, nameof(Mailing.Content));
            var content = ContentNode.GetByPath(db, contentPath);
     

            if (content != null) {
                db.DeleteEntity(content);
            }

            if (snapshot != null) {
                db.DeleteEntity(snapshot);
            }

            //if (this.Content != null) {
            //    db.DeleteEntity(this.Content);
            //}

            //if (this.Content != null) {
            //    db.DeleteEntity(this.Content);
            //}

            if (this.ContentSnapshot != null) {
                db.DeleteEntity(this.ContentSnapshot);
            }
           
        }

        private string GetStatusDescription() {
            var task = MailingTask.CreateMailingTask(this.Context);
            string progress = null;
            var taskStatus = this.GetTaskStatus();
            if (taskStatus == TaskLog.TaskStatuses.Started 
                || taskStatus == TaskLog.TaskStatuses.Paused
                || taskStatus == TaskLog.TaskStatuses.ItemProcessed
                || taskStatus == TaskLog.TaskStatuses.ItemFailed
                || taskStatus == TaskLog.TaskStatuses.Finished) {
              
                // Too time consuming task.GetAllItems ()
                //progress = $"Progress: {task.GetProcessedTypedItems(this.PublicId).Count()} / {task.GetAllItems(this.PublicId).Count()}";
                progress = $"Processed Items: {task.GetProgress(this.PublicId)}";
            }
            return progress;
           
        }




        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        private TaskLog.TaskStatuses GetTaskStatus() {
            var task = MailingTask.CreateMailingTask(this.Context);
            var status =  task.GetTaskStatus(this.PublicId);
            return status == null ? TaskLog.TaskStatuses.Created : status.Value;
        }
      
        #endregion
    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextMailingExtenions {
        [ModelSet]
        public static DbSet<Mailing> Mailings(this Core.Model.ModelContext context) {
            return context.Set<Mailing>();
        }
    }

    #endregion



}
