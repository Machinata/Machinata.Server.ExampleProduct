using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.TaskManager;
using static Machinata.Core.Model.MailingUnsubscription;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Data;
using System.Collections;
using System.Text.RegularExpressions;

namespace Machinata.Module.Mailing.Model {

    [Serializable()]
    [ModelClass]
    public partial class MailingContact : MailingContactFormat, BatchTaskItem {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

      
        public const string PROPERTIES_KEY_ADDRESS = "Address";
        public const string PROPERTIES_KEY_COMPANY = "Company";
        public const string PROPERTIES_KEY_TITLE = "Title";


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public MailingContact() {
            this.Properties = new Properties();
            this.MailingLists = new List<MailingList>();
            this.Source = Targets.Email;
        }

        public static MailingContact CreateTest() {
            var contact = new MailingContact();
            contact.Properties = new Properties();
            contact.MailingLists = new List<MailingList>();
            contact.Source = Targets.Email;
            contact.Properties["Birthday"] = "01.01.0001";
            contact.Properties["Gender"] = "";
            return contact;
        }


        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [MinLength(2)]
        [MaxLength(3)]
        public string Language { get; set; }


        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [PropertiesKey(PROPERTIES_KEY_ADDRESS)]
        [PropertiesKey(PROPERTIES_KEY_COMPANY)]
        public Properties Properties { get; set; }


        [Column]
        [FormBuilder]
        public string EmailEncrypted { get; set; }

        [Column]
        [FormBuilder]
        public string EmailHash { get; set; }


        /// <summary>
        /// Every time a message will be sent the date will be set
        /// Used for the Mailing.SendOnlyToUntouchedContacts flag
        /// Note: this is newly introduced and Database has to be prepared-> e.g. set all Contacts.LasteMessageSent to Default value before importing to contacts
        /// </summary>
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public DateTime? LastMessageSent { get; set; }


        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public ICollection<MailingList> MailingLists { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        /// <summary>
        /// Indicates the source of this Contact
        ///  -Email (not bound to a user or business)
        /// </summary>
        /// <value>
        /// The type of the recipient.
        /// </value>
        [NotMapped]
        public Targets Source { get; set; }

        [NotMapped]
        public User User { get; set; }

        [NotMapped]
        public Business Business { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [MinLength(3)]
        [MaxLength(80)]
        [Placeholder("user@domain.com")]
        [DataType(DataType.EmailAddress)]
        [NotMapped]
        public string Email
        {
            get
            {
                return Core.Encryption.DefaultEncryption.DecryptString(this.EmailEncrypted);
            }
            set
            {
                this.EmailHash = value == null ? null : Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(value);
                this.EmailEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }

        public string BatchTaskItemId
        {
            get
            {
                return this.EmailHash;
            }
        }

        public string BatchTaskItemTitle
        {
            get
            { 
                return this.Email;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public string PublicLinkId { get {
                if (this.User != null) return this.User.PublicId;
                if (this.Business != null) return this.Business.PublicId;
                return this.PublicId;
            } 
        }

        [NotMapped]
        [FormBuilder()]
        public string FirstName
        {
            get
            {
                if (this.Name != null) {
                    return this.Name.Split(" ", 2).FirstOrDefault();
                }
                return null;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public string LastName
        {
            get
            {
                if (this.Name != null) {
                    var splits = this.Name.Split(" ", 2);
                    if (splits.Count() == 2) {
                        return splits.ElementAt(1);
                    }
                }
                return null;
            }
        }
        

        [NotMapped]
        [FormBuilder()]
        public Core.Model.Address StructuredAddress {
            get {
                try {
                    // Validate
                    return ParseAddressFromProperties();
                } catch (Exception e) {
                    throw new BackendException("address-exception", $"Could not parse Address for contact {this.Email}", e);
                }

            }

        }

        private Address ParseAddressFromProperties() {
            if (this.Properties == null || this.Properties.Keys.Contains(PROPERTIES_KEY_ADDRESS) == false) return null;
            // Get segments by comma
            var addrString = this.Properties[PROPERTIES_KEY_ADDRESS] as string;
            addrString = Core.Localization.TextParser.ReplaceTextVariablesForData("Machinata.Module.Mailing", addrString, Core.Config.LocalizationDefaultLanguage);
            var ret = Address.ParseAddressFromString(addrString);

            // Name 
            ret.Name = this.Name;

            // Email 
            ret.Email = this.Email;

            // Company
            if (this.Properties == null || this.Properties.Keys.Contains(PROPERTIES_KEY_COMPANY) == true) {
                ret.Company = this.Properties[PROPERTIES_KEY_COMPANY]?.ToString();
            }

            return ret;
        }
        


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }

        public override Core.Cards.CardBuilder VisualCard() {
            // Init card
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle("{text.target}: " + this.Source.ToString())
                .Sub(this.Email)
                .Icon("user")
                .Wide();


            return card;
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static MailingContact FindContact(ModelContext db, string email) {
            var hash = Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(email);
            var contact = db.MailingContacts().FirstOrDefault(c => c.EmailHash == hash);
            return contact;
        }

        public static IEnumerable<MailingContact> GetContactsForUsers(IQueryable<User> users) {
            var contacts = new List<MailingContact>();
            foreach (var user in users) {
                MailingContact contact = GetContactFor(user);
                contacts.Add(contact);
            }
            return contacts;
        }

        public static MailingContact GetContactFor(User user) {
            var contact = new MailingContact();
            contact.Email = user.Email;
            contact.Name = user.Name;
            contact.Source = Targets.User;
            contact.User = user;
            return contact;
        }

        public static IEnumerable<MailingContact> GetContactsForBusinesses(IQueryable<Business> businesses) {
            var contacts = new List<MailingContact>();
            foreach (var business in businesses) {
                MailingContact contact = GetContactFor(business);
                contacts.Add(contact);
            }
            return contacts;
        }

        public static MailingContact GetContactFor(Business business) {
            var contact = new MailingContact();
            var address = business.GetBillingAddressForBusiness();
            contact.Email = address.Email;
            contact.Name = address.Name;
            contact.Source = Targets.Business;
            contact.Business = business;
            return contact;
        }


        public void SubscribeAll(ModelContext db) {
            if (this.Source != MailingUnsubscription.Targets.Email) {
                throw new BackendException("unsubsribe-error", "This is only supported for Email Contacts");
            }
            var unsubs = db.MailingUnsubscriptions().Where(mu => mu.Target == MailingUnsubscription.Targets.Email && mu.Identifier == this.EmailHash);
            db.MailingUnsubscriptions().RemoveRange(unsubs);
        }

      
        public void UnubscribeAll(ModelContext db) {

            if (this.Source != MailingUnsubscription.Targets.Email) {
                throw new BackendException("unsubsribe-error", "This is only supported for Email Contacts");
            }

            // Already unsubscribed
            UnsubscribeService.RemoveAllEmailUnsubscriptions(db, this.EmailHash);

            // Add Unsubscriptiosn
            foreach (var category in UnsubscribeService.GetPublicCategories()) {
                // All categories and subcategories
                UnsubscribeService.Unsubscribe(db, Targets.Email, category, "*", this.Email);
            }

        }

        public IEnumerable<MailingUnsubscription> GetUnsubscriptions(ModelContext db) {
            if (this.Source == MailingUnsubscription.Targets.Email) {
                return UnsubscribeService.GetUnsubscriptions(db, this.Email);
            } else if (this.Source == MailingUnsubscription.Targets.Business) {
                return UnsubscribeService.GetUnsubscriptions(db, this.Business);
            } else if (this.Source == MailingUnsubscription.Targets.User) {
                return UnsubscribeService.GetUnsubscriptions(db, this.User);
            }
            throw new BackendException("unsubscriptions-error", this.Source + " is not supported");
        }

        public IQueryable<SystemMessage> GetSystemMessages(ModelContext db) {
            if (this.Source == MailingUnsubscription.Targets.Email) {
                return SystemMessage.GetMessagesForReceiver(db, this.Email);
            } else if (this.Source == MailingUnsubscription.Targets.Business) {
                return SystemMessage.GetMessagesForReceiver(db, this.Email);
            } else if (this.Source == MailingUnsubscription.Targets.User) {
                return SystemMessage.GetMessagesForReceiver(db, this.Email);
            }
            throw new BackendException("messages-error", this.Source + " is not supported");
        }
     

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override MailingContact ConvertToMailingContact() {
            if (this.Properties == null) {
                this.Properties = new Properties();
            }
            return this;
        }

        public override string FormatTitle() {
            return "Name One Column";
        }

        public override MailingContactFormat ConvertFrom(MailingContact contact) {
            return contact;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextMailingContactExtenions {
        [ModelSet]
        public static DbSet<MailingContact> MailingContacts(this Core.Model.ModelContext context) {
            return context.Set<MailingContact>();
        }
    }

    public static class IQueryableMailingContactsExtenions {

        public static IQueryable<MailingContact> Dangling(this IQueryable<MailingContact> query) {

            // Important Include!
            query.Include(nameof(MailingContact.MailingLists));

            // No Lists
            return query.Where(mc => mc.MailingLists.Count == 0);
        }
    }

    #endregion


}
