using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing
{
    public class Config
    {

        public readonly static string DefaultSubscriptionMailingList = Core.Config.Dynamic.String("DefaultSubscriptionMailingList", false);

        public static bool MailingSubscribeCategoriesEnabled = Core.Config.GetBoolSetting("MailingSubscribeCategoriesEnabled");

        public static int MailingTaskBatchSize = Core.Config.GetIntSetting("MailingTaskBatchSize");

    }
}
