using Machinata.Core.Model;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.ContentProvider {
    public abstract class MailingContentProvider {

        public enum ContentType {
            Productive, // Snapshot
            Source      // fresh content
        }

        public Mailing.Model.Mailing Mailing { get; set; }
        public ModelContext DB { get; set; }
        public PageTemplate ParentTemplate { get; set; }

        /// <summary>
        /// Gets the productive content from the Snapshot/Generated content --> PRODUCTIVE CONTENT!
        /// </summary>
        /// <returns></returns>
        public abstract string GetContent(ContentType contentType);

        /// <summary>
        /// If 'true' the content will be generated implicitly when previewing or sending the content
        /// If 'false' the user has to generate the content manually, 'bake in'. e.g. by clicking generate content
        /// </summary>
        /// <returns></returns>
        public abstract bool AutogenerateContent { get; }


        /// <summary>
        /// Saves/snapshots the current content (overrides the old one)
        /// </summary>
        public abstract void Save();

        public virtual void Initialize(Mailing.Model.Mailing mailing, PageTemplate parentTemplate) {
            this.Mailing = mailing;
            this.DB = parentTemplate.Handler.DB;
            this.ParentTemplate = parentTemplate;
        }

        public virtual void Initialize(Mailing.Model.Mailing mailing, ModelContext db) {
            this.Mailing = mailing;
            this.DB = db;
        }

        public static MailingContentProvider GetByName(string fullName) {
            var types = Core.Reflection.Types.GetMachinataTypes(typeof(MailingContentProvider));
            var type = types.SingleOrDefault(t => t.FullName == fullName);
            return Core.Reflection.Types.CreateInstance<MailingContentProvider>(type);
        }

        protected void DeleteOldSnapshot(string snapshotPath) {
            if (ContentNode.Exists(this.DB, snapshotPath)) {
                var oldsnapshot = ContentNode.GetByPath(this.DB, snapshotPath);
                oldsnapshot.LoadSiblings();
            
                if (oldsnapshot.Parent?.Children != null) {
                    oldsnapshot.Parent.Children.Remove(oldsnapshot);
                }

                oldsnapshot.OnDelete(this.DB);

          
                this.DB.ContentNodes().Remove(oldsnapshot);
            }
        }

        protected string GetSnapshotPath() {
            this.Mailing.Include(nameof(this.Mailing.Content));
            if (this.Mailing.Content != null) {
                this.Mailing.Content.Include(nameof(this.Mailing.Content.Parent));
            }
            var snapshotPath = ContentNode.GetNodePathForEntityProperty(this.Mailing, nameof(Mailing.ContentSnapshot));
            return snapshotPath;
        }

        /// <summary>
        /// Gets and loads the content node
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <returns></returns>
        protected ContentNode GetContentNode(ContentType contentType) {
            ContentNode content = null;
            if (contentType == ContentType.Productive) {
                this.Mailing.Include(nameof(Mailing.ContentSnapshot));
                content = this.Mailing.ContentSnapshot;
            } else if (contentType == ContentType.Source) {
                this.Mailing.Include(nameof(Mailing.Content));
                content = this.Mailing.Content;
            }
            return content;
        }


    }
}
