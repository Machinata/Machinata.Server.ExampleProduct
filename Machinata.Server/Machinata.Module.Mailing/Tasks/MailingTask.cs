using Machinata.Core.Model;
using System.Collections.Generic;
using System.Linq;
using Machinata.Module.Mailing.Model;
using Machinata.Core.Util;
using System;

namespace Machinata.Module.Mailing.Tasks {

    public class MailingTask : Core.TaskManager.BatchTask<MailingContact> {

        public MailingTask() {
            this.BatchSize = Config.MailingTaskBatchSize;
        }

        public static MailingTask CreateMailingTask(ModelContext db) {
            var task = new MailingTask();
            task.SetRunContext(db, null, false);
            return task;
        }

        public override IEnumerable<MailingContact> GetAllItems(string taskId) {
            var mailing = GetMailingBy(taskId);
            return mailing.GetContacts(this.DB);
        }

        protected override void OnItemException(string taskId, MailingContact item, Exception exception) {
            base.OnItemException(taskId, item, exception);
            try {
                var mailing = GetMailingBy(taskId);
                Core.EmailLogger.SendMessageToAdminEmail(
                    subject: $"Mailing Error '{mailing.Name}': {item.Name}, {item.Email}", 
                    senderPackage: "Machinata.Module.Mailing",
                    handler: null, 
                    context: null,
                    e: exception);
            } catch (Exception e) {
                // nothing
            }
        }

        public override object Process(string taskId, MailingContact item) {

            // Get the mailing
            var mailing = GetMailingBy(taskId);

            // Create a handler: used for templates
            var handler = Core.Handler.Handler.CreateForRoute<Mailing.Handler.Page.MailingsAdminPageHandler>("/admin/mailing/mailings");

            // Send email to contact
            mailing.SendTo(handler, item);

            // Return success
            return true;
        }

        private Mailing.Model.Mailing GetMailingBy(string taskId) {
            var mailing = _mailings.SingleOrDefault(m => m.PublicId == taskId);
            if (mailing == null) {
                mailing = this.DB.Mailings().GetByPublicId(taskId);
                _mailings.Add(mailing);
            }
            return mailing;
        }

        public override MailingContact GetProcessedTypedItem(string mailingId, string itemId, IEnumerable<MailingContact> allItems) {
            var contact = allItems.FirstOrDefault(c => c.BatchTaskItemId == itemId);
            return contact;
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "5m";
            return config;
        }

        public override string GetTitle(string taskId) {
            var mailing = GetMailingBy(taskId);
            var title = $"Mailing: {taskId}, {mailing.Name}";
            return title;
        }

        // cache the mailings
        private List<Mailing.Model.Mailing> _mailings = new List<Mailing.Model.Mailing>();

      
    }
}
