using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.ContactProvider {
    public abstract class MailingContactProvider {

        public IQueryable<MailingContact> GetContacts(ModelContext db) {
            return GetContactsImpl(db);
        }

        protected abstract IQueryable<MailingContact> GetContactsImpl(ModelContext db);

        public abstract IDictionary<string,string> GetDefaultSettings();
    
        public abstract string GetContactAdminLink();

        public virtual void LoadDefaultSettingsAndMerge() {
            // Make sure all defaults are added
            var defaults = GetDefaultSettings();
            foreach (var keyval in defaults) {
                if (!this.List.ContactProviderSettings.Keys.Contains(keyval.Key)) {
                    this.List.ContactProviderSettings[keyval.Key] = keyval.Value;
                }
            }
            // Remove any not of this type...
            var toRemove = new List<string>();
            foreach (var key in this.List.ContactProviderSettings.Keys) {
                if (!defaults.Keys.Contains(key)) {
                    toRemove.Add(key);
                }
            }
            foreach (var key in toRemove) this.List.ContactProviderSettings.Delete(key);
        }


        public MailingList List { get; set; }

        public static string GetName(Type type) {
            var name = type.Name;
            name = name.Replace("ContactProvider", string.Empty);
            name = name.ToSentence();
            return name;
        }


        public static MailingContactProvider GetByName(string name, bool useDefault = true) {
            if (string.IsNullOrEmpty(name) && useDefault) {
                //return new DefaultMailingContactProvider();
                return null;
            }
            List<Type> types = GetAllTypes();
            var type = types.SingleOrDefault(t => t.Name == name);
            return Core.Reflection.Types.CreateInstance<MailingContactProvider>(type);
        }

        public static List<Type> GetAllTypes() {
            return Core.Reflection.Types.GetMachinataTypes(typeof(MailingContactProvider)).Where(t=>t.IsAbstract == false).ToList();
        }
    }

    //public class DefaultMailingContactProvider : MailingContactProvider {
       

    //    public override string GetContactAdminLink() {
    //        return null;
    //    }

    //    public override IDictionary<string, string> GetDefaultSettings() {
    //        return new Dictionary<string, string>();
    //    }

    //    protected override IQueryable<MailingContact> GetContactsImpl(ModelContext db) {
    //        return new List<MailingContact>().AsQueryable();
    //    }
    //}
}
