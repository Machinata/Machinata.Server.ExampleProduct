
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Mailing.Model;
using Machinata.Core.Messaging.Providers;

namespace Machinata.Module.Mailing.Handler {


    public class MailingContactAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Mailing.Model.MailingContact> {
        
        [RequestHandler("/api/admin/mailing/contact/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/mailing/contacts/create")]
        public void Create() {
            this.CRUDCreate();
        }

        [RequestHandler("/api/admin/mailing/contact/{publicId}/delete")]
        public void Delete(string publicId) {
            var contact = this.DB.MailingContacts()
                .GetByPublicId(publicId);

            this.DB.MailingContacts().Remove(contact);

            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("delete-success");

        }

        [RequestHandler("/api/admin/mailing/contact/{publicId}/unsubscribe")]
        public void UnsubscribeAll(string publicId) {
            // Contact
            var contact = this.DB.MailingContacts().GetByPublicId(publicId);

            // Unsubscribe All
            contact.UnubscribeAll(this.DB);
   
            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("unsubscribe-success");

        }


        [RequestHandler("/api/admin/mailing/contact/{publicId}/subscribe")]
        public void SubscribeAll(string publicId) {

            // Contact
            var contact = this.DB.MailingContacts().GetByPublicId(publicId);

            // Unscubscribe
            contact.SubscribeAll(this.DB);

            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("subsribe-success");

        }

        /// <summary>
        /// Deletes all dangling contacts. Contacts not in a MailingList
        /// </summary>
        [RequestHandler("/api/admin/mailing/contacts/delete-dangling")]
        public void DeleteDanglingContacts() {

            // Dangling Contacts
            var contacts = this.DB.MailingContacts().Dangling().ToList();
            
            // Delete each
            foreach(var contact in contacts) {
                this.DB.DeleteEntity(contact);
            }
                
            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("subsribe-success");

        }

    }
}
