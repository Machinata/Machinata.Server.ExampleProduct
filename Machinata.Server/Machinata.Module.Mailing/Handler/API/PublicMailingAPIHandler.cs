
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Mailing.Model;
using Machinata.Module.Mailing.Tasks;
using Machinata.Core.Messaging.Providers;

namespace Machinata.Module.Mailing.Handler {


    public class PublicMailingAPIHandler : APIHandler {

        [RequestHandler("/api/mailing/subscribe", AccessPolicy.PUBLIC_ARN)]
        public void Subscribe() {

            var categoryIds = this.Params.StringArray("category", new string[] { });
            var listIds = this.Params.StringArray("mailinglists", new string[] { });

            var mailingLists = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicIds(listIds);
            var mailingCategories = this.DB.MailingCategories().GetByPublicIds(categoryIds).ToList().Select(c=>c.UnsubscribeId);

            var contact = new MailingContact();
            Populate(contact);

            // Set handler language if none
            if (string.IsNullOrEmpty(contact.Language)) {
                contact.Language = this.Language;
            }

            this.DB.MailingContacts().Add(contact); 

            // Unsubscribe unselected categories
            if (Config.MailingSubscribeCategoriesEnabled) {
                if (mailingCategories.Any()) {
                    UnsubscribeService.UnsubscribeOtherSubcategories(this.DB, MailingUnsubscription.Categories.Mailing, contact.Email, mailingCategories);
                } else {
                    throw new BackendException("subscribe-error", "Please select at least one newsletter type.");
                }
            }

            foreach (var mailingList in mailingLists) {
                // Subscribe new contact to list
                mailingList.SubscribeToList(this.DB, contact);
            }


            // Save 
            this.DB.SaveChanges();

            this.SendAPIMessage("success");
        }

        private void Populate(MailingContact contact) {
            contact.Name = this.Params.String("name");
            if (string.IsNullOrEmpty(contact.Name)) {
                var name = this.Params.String("first-name") + " " + this.Params.String("last-name");
                contact.Name = name;
            }

            // Trim if too long
            if (contact.Name.Length > 100) {
                contact.Name = Core.Util.String.CreateSummarizedText(contact.Name, 100, false, true);
            }

            contact.Email = this.Params.String("email");
            contact.Properties[MailingContact.PROPERTIES_KEY_COMPANY] = this.Params.String("company");
            contact.Properties[MailingContact.PROPERTIES_KEY_ADDRESS] = this.Params.String("address");
            contact.Validate();
        }
    }

}
