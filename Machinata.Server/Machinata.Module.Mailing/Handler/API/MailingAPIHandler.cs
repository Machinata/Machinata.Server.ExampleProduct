
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Mailing.Model;
using Machinata.Module.Mailing.Tasks;

namespace Machinata.Module.Mailing.Handler {


    public class MailingAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Mailing.Model.Mailing> {
        
        [RequestHandler("/api/admin/mailing/mailing/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/mailing/mailings/create")]
        public void Create() {
            this.CRUDCreate();
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/delete")]
        public void Delete(string publicId) {
            
            // Mailing
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            // Delete including Logs
            mailing.Delete(true);

            // DB
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("delete-success");
        }

      

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/toggle-list/{toggleId}")]
        public void ListToggle(string publicId, string toggleId) {
            this.CRUDToggleSelection<Mailing.Model.MailingList>(publicId, toggleId, nameof(Mailing.Model.Mailing.Lists));
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/send-test-email")]
        public void SendTestEmail(string publicId) {
            var mailing = this.DB.Mailings()
                .Include(nameof(Model.Mailing.Lists))
                .GetByPublicId(publicId);

            // Preview Contact -> Try first one oder mockup
            var contact = mailing.GetContacts(this.DB).FirstOrDefault();
            if (contact == null) {
                // Setup contact
                contact = MailingContact.CreateTest();
                contact.Name = "Hans Muster";
            }
            contact.Email = Params.String("email");

            // Content
            mailing.PrepareContent(this.DB);

            // Send
            mailing.SendTo(this, contact);

            SendAPIMessage("send-success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/send-single-email")]
        public void SendSingleEmail(string publicId) {
            var mailing = this.DB.Mailings()
                .Include(nameof(Model.Mailing.Lists))
                .GetByPublicId(publicId);

            // Dummy contact
            var contact = MailingContact.CreateTest();
            contact.Name = Params.String("email");
            contact.Email = Params.String("email");

            // Content
            mailing.PrepareContent(this.DB);

            // Send
            mailing.SendTo(this, contact, true);

            SendAPIMessage("send-success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/set-ready-to-send")]
        public void SetReadyToSend(string publicId) {
            var mailing = this.DB.Mailings()
                .Include(nameof(Mailing.Model.Mailing.ContentSnapshot))
                .GetByPublicId(publicId);

            mailing.CheckReadyToSend(this.DB, true);

            mailing.PrepareContent(this.DB);
        
            var task = new MailingTask();
            task.SetRunContext(this.DB, this.Context, false);

            // Restart
            // --> Since we checked if mailing is ready before (CheckReadyToSend) we can allow resend (delete previous logs)
            var logsDeleted = task.DeleteLogs(publicId);
            if (logsDeleted == true) {
                this.DB.SaveChanges();
            }

            task.Start(publicId);

            this.DB.SaveChanges();

            SendAPIMessage("send-success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/resume")]
        public void Resume(string publicId) {
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            var task = new MailingTask();
            task.SetRunContext(this.DB, this.Context, false);
            task.Resume(publicId);

            this.DB.SaveChanges();

            this.SendAPIMessage("resume-success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/pause")]
        public void Pause(string publicId) {
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            var task = new MailingTask();
            task.SetRunContext(this.DB, this.Context, false);
            task.Pause(publicId);

            this.DB.SaveChanges();

            this.SendAPIMessage("pause-success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/duplicate")]
        public void Duplicate(string publicId) {
            var original = this.DB.Mailings()
                .Include(nameof(Mailing.Model.Mailing.Lists))
                .Include(nameof(Mailing.Model.Mailing.Content))
                .Include(nameof(Mailing.Model.Mailing.Category))
                .Include(nameof(Mailing.Model.Mailing.Content) + "." + nameof(ContentNode.Parent))
                .GetByPublicId(publicId);

            // Properties
            var duplicate = new Mailing.Model.Mailing();
            duplicate.Category = original.Category;
            duplicate.Medium = original.Medium;
            duplicate.Name = this.Params.String("name", original.Name);
            duplicate.ReplyToEmail = original.ReplyToEmail;
            duplicate.ReplyToName = original.ReplyToName;
            duplicate.ContentProvider = original.ContentProvider;
            duplicate.Subject = original.Subject;

            // Lists
            foreach (var list in original.Lists) {
                duplicate.Lists.Add(list);
            }
            
            this.DB.Mailings().Add(duplicate);

            // Save for id
            this.DB.SaveChanges();
      
            // Content
            if (original.Content != null) {
                duplicate.Content = original.Content.Duplicate(this.DB, "/Entities/Mailing/" + duplicate.PublicId + "/Content", false);
            }
               
            this.DB.SaveChanges();

            SendAPIMessage("duplicate-success", new {
                Mailing = new {
                    PublicId = duplicate.PublicId
                }
            });
            
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/take-snapshot")]
        public void TakeSnapshot(string publicId) {
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            var contentProvider = mailing.GetContentProvider();
            contentProvider.Initialize(mailing, this.DB);
            contentProvider.Save();
                

            this.DB.SaveChanges();

            SendAPIMessage("success", null);
        }

        [RequestHandler("/api/admin/mailing/mailing/{publicId}/retry-failed")]
        public void RetryFailed(string publicId) {
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            var task = new MailingTask();
            task.SetRunContext(this.DB, this.Context, false);
            task.RetryFailed(publicId);

            this.DB.SaveChanges();

            this.SendAPIMessage("resume-success", null);
        }



        [RequestHandler("/api/admin/mailing/mailing/{publicId}/ignore-failed")]
        public void IgnoreFailed(string publicId) {
            var mailing = this.DB.Mailings().GetByPublicId(publicId);

            var task = new MailingTask();
            task.SetRunContext(this.DB, this.Context, false);
            task.IgnoreFailed(publicId);

            this.DB.SaveChanges();

            this.SendAPIMessage("resume-success", null);
        }


    }

}
