
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Mailing.Model;
using Machinata.Module.Mailing.Handler.Page;
using Machinata.Module.Mailing.Data;

namespace Machinata.Module.Mailing.Handler {


    public class MailingListAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Mailing.Model.MailingList> {

        [RequestHandler("/api/admin/mailing/list/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }
        protected override void EditPopulate(MailingList entity) {
            entity.ContactProviderType = this.Params.String(nameof(MailingList.ContactProviderType));
            base.EditPopulate(entity);
        }

        [RequestHandler("/api/admin/mailing/lists/create")]
        public void Create() {
            this.CRUDCreate();
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/delete")]
        public void Delete(string publicId) {
            this.CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/toggle-contact/{toggleId}")]
        public void ContactToggle(string publicId, string toggleId) {
            this.CRUDToggleSelection<Mailing.Model.MailingContact>(publicId, toggleId, nameof(Mailing.Model.MailingList.Contacts));
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/add-property")]
        public void AddProperty(string publicId) {
            this.RequireWriteARN();
            // Init
            var list = this.DB.MailingLists().GetByPublicId(publicId);
            var name = this.Params.String("name");

            var property = MailingListProperty.CreateMailingListProperty(name, list);

            list.Properties[name] = null;

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Property = new { PublicId = property.PublicId } });
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/delete-property/{property}")]
        public void DeleteProperty(string publicId, string property) {
            this.RequireWriteARN();
            // Init
            var list = this.DB.MailingLists().GetByPublicId(publicId);
            list.Properties.Delete(property);

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Property = new { PublicId = publicId } });
        }


        [RequestHandler("/api/admin/mailing/list/{publicId}/property/{key}/edit")]
        public void EditProperty(string publicId, string key) {
            this.RequireWriteARN();
            // Init
            var list = this.DB.MailingLists().GetByPublicId(publicId);

            var property = new MailingListProperty();
            property.Populate(this, new FormBuilder(Forms.Admin.EDIT));

            list.Properties[key] = property.Value;

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success", new { Property = new { PublicId = publicId } });
        }


        [RequestHandler("/api/admin/mailing/list/{publicId}/import")]
        public void Import(string publicId) {
            this.RequireWriteARN();

            var importAction = this.Params.Enum<MailingList.ImportAction>("import-action", MailingList.ImportAction.Add);
            var allowFormulas = this.Params.Bool("allow-formulas", false);
            var format = this.Params.String("format", "mailingcontact");

            var form = new FormBuilder(Forms.Admin.EXPORT);
            var formatType = MailingContactFormat.GetFormat(format);

            // Mailing list to import into
            var list = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Import from request file
            var method = typeof(MailingList).GetMethod("ImportContacts").MakeGenericMethod(formatType.GetType());
            var log = method.Invoke(list, new object[] { importAction, allowFormulas, form, this.Request.ReadFileFromRequest("xlsx") });

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("import-success", new { Log = log.ToString() });
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/clear")]
        public void Clear(string publicId) {

            // Mailing list to remove all contacts from (not deleting the contacts)
            var list = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Only support Contact type Lists
            if (list.Type != MailingList.Types.Contact) {
                throw new BackendException("clear-error", "Clearing the list is only supported for Contact Lists (not business or user lists)");
            }

            int count = 0;
            // Remove contacts
            foreach (var contact in list.Contacts.ToList()) {
                list.Contacts.Remove(contact);
                count++;
            }

            // save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("clear-success", new { Contacts = count });
        }

        [RequestHandler("/api/admin/mailing/list/{publicId}/add-contact")]
        public void AddContact(string publicId) {

            // List
            var list = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Contact
            var contact = new MailingContact();
            contact.Populate(this, new FormBuilder(Forms.Admin.CREATE).Include(nameof(MailingContact.Properties)));
            contact.Validate();

            // Add to list
            list.Contacts.Add(contact);

            // Save
            this.DB.SaveChanges();

            // API
            this.SendAPIMessage("success", new { Contact = new { contact.PublicId } });


        }

    }

}
