using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System;
using System.Dynamic;
using System.Linq;
using Machinata.Core.Templates;
using System.Collections.Generic;
using Machinata.Module.Mailing.Data;
using System.Text.RegularExpressions;
using Machinata.Module.Mailing.Extensions;
using Machinata.Core.Messaging.Providers;
using Machinata.Module.Mailing.ContactProvider;

namespace Machinata.Module.Mailing.Handler.Page {

    public class MailingListAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {

        [RequestHandler("/admin/mailing/lists")]
        public void Lists() {

            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: this.DB.MailingLists(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/lists/list/{entity.public-id}"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("lists");
        }

        [RequestHandler("/admin/mailing/lists/create")]
        public void CreateList() {

            this.Template.InsertForm(
                variableName: "form",
                entity: new MailingList(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/mailing/lists/create",
                onSuccess: "/admin/mailing/lists/list/{mailing-list.public-id}"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("lists");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}")]
        public void List(string publicId) {

            var entity = this.DB.MailingLists().GetByPublicId(publicId);
            var contacts = entity.GetContacts(this.DB);
            
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW)
             );
         
            var form = new FormBuilder(Forms.Admin.LISTING);

            // Contacts
            InsertContacts(contacts.AsQueryable(), entity,  form, 20);

            // Properties
            this.Template.InsertEntityList(
            variableName: "properties",
            entities: MailingListProperty.CreateMailingProperties(entity),
            link: "{page.navigation.current-path}/property/{entity.public-id}",
            form: new FormBuilder(Forms.Admin.LISTING));

            // List
            this.Template.InsertVariable("allow-edit-contacts", entity.Type == MailingList.Types.Contact);

            // Variables
            this.Template.InsertVariables("entity", entity);
        

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("lists");
            Navigation.Add("list/" + entity.PublicId, entity.ToString());

        }

        private void InsertContacts(IQueryable<MailingContact> allContacts, MailingList list, FormBuilder form, int pageSize) {
            var link = this.GetContactUrl(list);
            this.Template.InsertVariable("contacts.count", allContacts.Count());
            var contacts = this.Template.Paginate(allContacts.AsQueryable(), this, nameof(ModelObject.Created), "asc", 1, pageSize);
            this.Template.InsertEntityList(
                variableName: "contacts",
                entities: contacts,
                form: form,
                link: link
                );
          
        }

        private string GetContactUrl(MailingList list) {
           if (list.Type == MailingList.Types.Contact) {
                return "/admin/mailing/lists/list/" + list.PublicId + "/contact/{entity.public-id}";
            } else if ( list.Type == MailingList.Types.Businesses) {
                return "/admin/mailing/contacts/contact/?business-id={entity.public-link-id}";
            } else if (list.Type == MailingList.Types.Users) {
                return "/admin/mailing/contacts/contact?user-id={entity.public-link-id}";
            } else if (list.Type == MailingList.Types.Custom) {
                return list.ContactProvider?.GetContactAdminLink();
            }
            throw new BackendException("contact-link-error", "Not implemented for " + list.Type);
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/edit")]
        public void ListEdit(string publicId) {

            var entity = this.DB.MailingLists().GetByPublicId(publicId);
            entity.ContactProvider?.LoadDefaultSettingsAndMerge();


            var form = new FormBuilder(Forms.Admin.EDIT);

            // Add selection for Custom Lists
            if (entity.Type == MailingList.Types.Custom) {
                form.SelectionListForTypes(nameof(entity.ContactProviderType), nameof(entity.ContactProviderType), entity.ContactProviderType, MailingContactProvider.GetAllTypes(), delegate (Type t) {
                    return MailingContactProvider.GetName(t);
                });
            }

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: $"/api/admin/mailing/list/{publicId}/edit",
                onSuccess: $"/admin/mailing/lists/list/{publicId}"
            );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("edit");

        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/edit-contacts")]
        public void EditContacts(string publicId) {

            var entity = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            var contacts = this.Template.Paginate(this.DB.MailingContacts(), this);
           // this.Template.

            // MailingContacts
            this.Template.InsertSelectionList(
                variableName: "entity-list",
                entities: contacts,
                selectedEntities: entity.Contacts.AsQueryable(),
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/mailing/list/{publicId}/toggle-contact/" + "{entity.public-id}"
                );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("lists");
            Navigation.Add("list/" + entity.PublicId, entity.ToString());

        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/edit-properties")]
        public void EditProperties(string publicId) {

            var entity = this.DB.MailingLists().GetByPublicId(publicId);
            
            // Properties
            this.Template.InsertEntityList(
            variableName: "entity-list",
            entities: MailingListProperty.CreateMailingProperties(entity),
            link: "{page.navigation.current-path}/property/{entity.public-id}");

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("lists");
            Navigation.Add("list/" + entity.PublicId, entity.ToString());

        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/property/{key}")]
        public void Property(string publicId, string key) {
            // Init
            var list = DB.MailingLists().GetByPublicId(publicId);

            var entity = MailingListProperty.CreateMailingListProperty(key, list);

            // Options
            this.Template.InsertPropertyList(
            variableName: "form",
            form: new FormBuilder(Forms.Admin.VIEW),
            entity: entity);

            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("list", list);

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + list.PublicId,list.ToString());
            this.Navigation.Add("property/" + key,"{text.property}: "+ key);
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/export-download", null, null, Verbs.Get, ContentType.Binary)]
        public void ExportDownload(string publicId) {
            var list = DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);
            var contacts = list.Contacts.ToList();
            var format = this.Params.String("format", "mailingcontact");
            var formatType = MailingContactFormat.GetFormat(format);
            var contactsConverted = new List<MailingContactFormat>();

            // Add imported Contacts to the list
            foreach (var contact in contacts) {
                contactsConverted.Add(formatType.ConvertFrom(contact));
            }

            var xlsx = Core.Reporting.ExportService.ExportXLSX<MailingContactFormat>(contactsConverted, new FormBuilder(Forms.Admin.EXPORT));
            var filename = GetExportFileName(list);
            SendBinary(xlsx, filename);
        }

        private string GetExportFileName(MailingList list) {
            var filename = Core.Config.ProjectID + "_" + list.Name + ".xlsx";
            return filename;
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/import")]
        public void Import(string publicId) {

            // Grab entity
            var entity = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Insert Preview Columsn and Profiles
            InsertProfileSelectionAndPreviewVariables(entity, MailingContactFormat.Direction.IMPORT);

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("import");

        }

        private void InsertProfileSelectionAndPreviewVariables(MailingList entity, MailingContactFormat.Direction direction) {
            // Header Template Example
            var columns = new List<MailingListColumn>();
            var formSelectors = new List<PageTemplate>();

            var form = new FormBuilder(Forms.Admin.EXPORT).Exclude(nameof(MailingContact.Properties));
            foreach (var format in MailingContactFormat.GetFormats(direction)) {

                // Each property for columns
                foreach (var property in format.Value.GetPropertiesForForm(form)) {
                    var formProperty = new EntityFormBuilderProperty(entity, property, form);
                    columns.Add(new MailingListColumn() { Key = formProperty.GetFormName(), Class = format.Key });
                }

                // Filter templates radio buttons
                var formatTemplate = this.Template.LoadTemplate("format.button");
                formatTemplate.InsertVariable("name", format.Key);
                formatTemplate.InsertVariable("title", format.Value.FormatTitle());
                formatTemplate.InsertVariable("checked", format.Key == "mailingcontact" ? "checked" : "notchecked");
                formSelectors.Add(formatTemplate);

            }
            columns.AddRange(MailingListColumn.CreateMailingProperties(entity));
            this.Template.InsertTemplates(
                variableName: "columns",
                entities: columns,
                templateName: "table.header.column",
                forEachEntity: new Action<MailingListColumn, PageTemplate>(delegate (MailingListColumn prop, PageTemplate template) {
                    template.InsertVariable("key", prop.Key);
                    template.InsertVariable("class", prop.Class);
                }));

            this.Template.InsertTemplates("format.buttons", formSelectors);
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/export")]
        public void Export(string publicId) {
            // Grab entity
            var entity = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Insert Preview Columns and Profiles
            this.InsertProfileSelectionAndPreviewVariables(entity, MailingContactFormat.Direction.EXPORT);

            // Filename
            var fileName = GetExportFileName(entity);
            this.Template.InsertVariable("filename", fileName);
            this.Template.InsertVariable("selected-format", "mailingcontact");
           

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("export");

        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/add-contact")]
        public void AddContact(string publicId) {

            // List
            var list = this.DB.MailingLists().Include(nameof(MailingList.Contacts)).GetByPublicId(publicId);

            // Properties of List
            var properties = MailingListProperty.CreateMailingProperties(list);

            // Contact
            var contact = new MailingContact();
            
            // Prefill properties in contact
            foreach(var property in properties) {
                contact.Properties[property.Key] = null;
            }

            // Form
            this.Template.InsertForm(
                entity: contact,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE).Include(nameof(MailingContact.Properties)),
                apiCall: $"/api/admin/mailing/list/{publicId}/add-contact",
                onSuccess: "/admin/mailing/lists/list/" + list.PublicId + "/contact/{contact.public-id}");

            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + list.PublicId, list.ToString());
            this.Navigation.Add("add-contact");


        }


        [RequestHandler("/admin/mailing/lists/list/{publicId}/contact/{contactId}")]
        public void Contact(string publicId, string contactId) {

            var contact = this.DB.MailingContacts().Include(nameof(MailingContact.MailingLists)).GetByPublicId(contactId);
            var list = contact.MailingLists.AsQueryable().GetByPublicId(publicId);

            // Form
            var form = new FormBuilder(Forms.Admin.VIEW);
            var allProperties = list.Properties.Keys.Distinct();

            // Add missing properties
            foreach (var key in allProperties) {
                if (!contact.Properties.Keys.Contains(key)) {
                    contact.Properties[key] = null;
                }
            }

            // Unsubs
            var unsubscriptions = contact.GetUnsubscriptions(this.DB);

            // Messages
            var messagesToContact = contact.GetSystemMessages(this.DB);

            // Address
            if (contact.Properties != null && contact.Properties.Keys.Contains(MailingContact.PROPERTIES_KEY_ADDRESS)) {
                this.Template.InsertCard("address", contact.StructuredAddress);
                this.Template.InsertVariableUnsafe("address.available", true);
            } else {
                this.Template.InsertVariable("address", string.Empty);
                this.Template.InsertVariableUnsafe("address.available", false);
            }

            // Unsubscriptions
            this.Template.InsertEntityList(
                variableName: "unsubscriptions",
                entities: unsubscriptions.AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING));

            // Messages
            this.Template.InsertEntityList(
              variableName: "messages",
              entities: messagesToContact.OrderByDescending(m => m.Id),
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/system/messages/message/{entity.public-id}");

            // List
            this.Template.InsertSelectionList(
              variableName: "lists",
              entities: this.DB.MailingLists().Where(ml => ml.Type == MailingList.Types.Contact),
              selectedEntities: contact.MailingLists.AsQueryable(),
              form: new FormBuilder(Forms.Admin.SELECTION),
              selectionAPICall: "/api/admin/mailing/list/{entity.public-id}/toggle-contact/" + contactId

             );

            // Properties
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: contact,
                form: form
            );

            this.Template.InsertVariable("lists.show", contact.Source == MailingUnsubscription.Targets.Email ? true : false);

            // Vars
            this.Template.InsertVariables("entity", contact);

            // Nav
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + list.PublicId, list.ToString());
            this.Navigation.Add("contact/" + contact.PublicId, contact.Name);

        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/contact/{contactId}/edit")]
        public void ContactEdit(string publicId, string contactId) {

            var contact = this.DB.MailingContacts().Include(nameof(MailingContact.MailingLists)).GetByPublicId(contactId);
            var list = contact.MailingLists.AsQueryable().GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT);
            foreach (var key in list.Properties.Keys) {
                if (!contact.Properties.Keys.Contains(key)) {
                    contact.Properties[key] = null;
                }
            }

            this.Template.InsertForm(
                variableName: "form",
                entity: contact,
                form: form,
                apiCall: $"/api/admin/mailing/contact/{contactId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + list.PublicId, list.ToString());
            this.Navigation.Add("contact/" + contact.PublicId, contact.Name);
            Navigation.Add("edit");

        }



        [RequestHandler("/admin/mailing/lists/list/{publicId}/contacts")]
        public void Contacts(string publicId) {

            // List
            var list = this.DB.MailingLists().GetByPublicId(publicId);
            var form = new FormBuilder(Forms.Admin.LISTING);
            var contacts = list.GetContacts(this.DB).AsQueryable();
            // Properties of List
            var properties = MailingListProperty.CreateMailingProperties(list);
            contacts = this.InsertContactSearch(properties, contacts);

            // Contacts
            InsertContacts(contacts, list, form, 50);

            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + list.PublicId, list.ToString());
            this.Navigation.Add("contacts", "{text.all-contacts}");
        }

        [RequestHandler("/admin/mailing/lists/list/{publicId}/duplicates")]
        public void Duplicates(string publicId) {

            var entity = this.DB.MailingLists().GetByPublicId(publicId);
            var contacts = entity.GetContacts(this.DB);
            var form = new FormBuilder(Forms.Admin.LISTING);

            // Duplicated Contacts
            var duplicatedEmails = contacts.Where(c => !string.IsNullOrEmpty(c.EmailHash)).GroupBy(c => c.EmailHash).Where(c => c.Count() > 1).SelectMany(g => g);
            var duplicatedNames = contacts.GroupBy(c => c.Name).Where(c => c.Count() > 1).SelectMany(g => g);
            var allDuplicates = duplicatedEmails.Concat(duplicatedNames).AsQueryable();

            var duplicates = this.Template.Paginate(allDuplicates, this, nameof(MailingContact.Name), "asc");

            this.Template.InsertEntityList(
               variableName: "duplicated-contacts",
               entities: duplicates,
               form: form,
               link: "{page.navigation.prev-path}/contact/{entity.public-id}"
               );

            this.Template.InsertVariable("all-duplicates.count", allDuplicates.Count());
            this.Navigation.Add("mailing");
            this.Navigation.Add("lists");
            this.Navigation.Add("list/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("contacts", "{text.duplicates}");
        }



        public static MailingListProperty CreateMailingListProperty(string key, MailingList list) {
            return new MailingListProperty() { Key = key, Value = list.Properties[key]?.ToString() };
        }
    }

    public class MailingListProperty : ModelObject {

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Key { get; set; }
        [FormBuilder(Forms.Admin.EDIT)]
        public string Value { get; set; }

        public override string PublicId
        {
            get
            {
                var key =  Uri.EscapeDataString(this.Key);
                return key;
            }
        }

        public static System.Collections.Generic.List<MailingListProperty> CreateMailingProperties(MailingList entity) {
            return entity.Properties.Keys.Select(k => new MailingListProperty() { Key = k, Value = entity.Properties[k]?.ToString() }).ToList();
        }

        public static MailingListProperty CreateMailingListProperty(string propertyKey, MailingList list) {
            return new MailingListProperty() { Key = propertyKey, Value = list.Properties[propertyKey]?.ToString() };
        }

    }

    public class MailingListColumn : ModelObject {
        [FormBuilder(Forms.Admin.EDIT)]
        public string Key { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        public string Class { get; set; }

        public static System.Collections.Generic.List<MailingListColumn> CreateMailingProperties(MailingList entity) {
            return entity.Properties.Keys.Select(k => new MailingListColumn() { Key = k, Class="options" }).ToList();
        }
    }

  
}
