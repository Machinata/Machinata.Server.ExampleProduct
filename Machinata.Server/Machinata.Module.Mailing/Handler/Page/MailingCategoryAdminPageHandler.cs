using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.Handler.Page {
    public class MailingCategoryAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {
      

        [RequestHandler("/admin/mailing/categories")]
        public void Contacts() {

            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: this.DB.MailingCategories(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/categories/category/{entity.public-id}"
                );
          
            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("categories");
        }

        [RequestHandler("/admin/mailing/categories/create")]
        public void Create() {

            this.Template.InsertForm(
                variableName: "form",
                entity:new MailingCategory(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/mailing/categories/create",
                onSuccess: "/admin/mailing/categories/category/{mailing-category.public-id}"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("categories");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/mailing/categories/category/{publicId}")]
        public void Category(string publicId) {

            var entity = this.DB.MailingCategories().GetByPublicId(publicId);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW)
                    );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("categories");
            Navigation.Add("category/" + entity.PublicId, entity.ToString());
            
        }

        [RequestHandler("/admin/mailing/categories/category/{publicId}/edit")]
        public void ContactEdit(string publicId) {

            var entity = this.DB.MailingCategories().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/mailing/category/{publicId}/edit",
                onSuccess: $"/admin/mailing/categories/category/{publicId}"
            );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("categories");
            Navigation.Add("category/" + entity.PublicId, entity.ToString());
            Navigation.Add("edit");

        }


    }
}
