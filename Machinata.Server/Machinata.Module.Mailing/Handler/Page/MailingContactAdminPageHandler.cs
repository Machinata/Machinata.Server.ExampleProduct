using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Extensions;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.Handler.Page {
    public class MailingContactAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("mailing");
        }

        #endregion

        [RequestHandler("/admin/mailing")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/mailing/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("mailing.menu-items", menuItems);
            // Navigation
            Navigation.Add("mailing");
        }

        [RequestHandler("/admin/mailing/contacts")]
        public void Contacts() {

            // Contacts
            var entities = this.DB.MailingContacts().AsQueryable();

            // Search
            // TODO: support search in sql/IQueryable
            entities = this.InsertContactSearch(new List<MailingListProperty>(), entities);

            //entities = this.Template.Filter(entities, this, nameof(MailingContact.Name));
            entities = this.Template.Paginate(entities, this, nameof(MailingContact.Name), "asc");
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
            );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
        }

        [RequestHandler("/admin/mailing/contacts/create")]
        public void CreateContact() {

            this.Template.InsertForm(
                variableName: "form",
                entity:new MailingContact(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/mailing/contacts/create",
                onSuccess: "/admin/mailing/contacts/contact/{mailing-contact.public-id}"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/mailing/contacts/contact")]
        public void ContactByParam() {
            this.Contact(null);
        }

        [RequestHandler("/admin/mailing/contacts/contact/{publicId}")]
        public void Contact(string publicId) {

            MailingContact entity = null;
            IQueryable<MailingUnsubscription> unsubscriptions = null;

            // Logged system messages for user
            var messagesToContact = new List<SystemMessage>().AsQueryable();
            if (this.Params.String("business-id") != null) {
                var business = this.DB.Businesses().GetByPublicId(this.Params.String("business-id"));
                entity = MailingContact.GetContactFor(business);
                // Unsubscriptions
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(this.DB, business);
                this.Template.InsertVariable("lists.show", false);
            } else if (this.Params.String("user-id") != null) {
                var user = this.DB.Users().GetByPublicId(this.Params.String("user-id"));
                entity = MailingContact.GetContactFor(user);
                this.Template.InsertVariable("lists.show", false);
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(this.DB, user);
            } else {
                entity = this.DB.MailingContacts().Include(nameof(MailingContact.MailingLists)).GetByPublicId(publicId);
                // List
                this.Template.InsertSelectionList(
                  variableName: "lists",
                  entities: this.DB.MailingLists().Where(ml => ml.Type == MailingList.Types.Contact),
                  selectedEntities: entity.MailingLists.AsQueryable(),
                  form: new FormBuilder(Forms.Admin.SELECTION),
                  selectionAPICall: "/api/admin/mailing/list/{entity.public-id}/toggle-contact/" + publicId

                  );

                this.Template.InsertVariable("lists.show", true);

                // Unsubscriptions
                unsubscriptions = UnsubscribeService.GetUnsubscriptions(this.DB, entity.Email);

                // System Messages
                messagesToContact =  SystemMessage.GetMessagesForReceiver(this.DB, entity.Email);
            }

            var form = new FormBuilder(Forms.Admin.VIEW);
            var allProperties = entity.MailingLists.Select(ml => ml.Properties).ToList().SelectMany(p => p.Keys).Distinct();
            foreach (var key in allProperties) {
                if (!entity.Properties.Keys.Contains(key)) {
                    entity.Properties[key] = null;
                }
            }

            // Properties
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: form
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Address
            if (entity.Properties != null && entity.Properties.Keys.Contains(MailingContact.PROPERTIES_KEY_ADDRESS)) {
                this.Template.InsertCard("address", entity.StructuredAddress);
                this.Template.InsertVariableUnsafe("address.available", true);
            } else {
                this.Template.InsertVariable("address", string.Empty);
                this.Template.InsertVariableUnsafe("address.available", false);
            }

            // Unsubscriptions
            this.Template.InsertEntityList(
                variableName: "unsubscriptions",
                entities: unsubscriptions,
                form: new FormBuilder(Forms.Admin.LISTING));
            
            // Messages
            this.Template.InsertEntityList(
              variableName: "messages",
              entities: messagesToContact.OrderByDescending(m=>m.Id),
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/system/messages/message/{entity.public-id}");

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("contact/" + entity.PublicId, entity.ToString());
            
        }

        [RequestHandler("/admin/mailing/contacts/contact/{publicId}/edit")]
        public void ContactEdit(string publicId) {

            var entity = this.DB.MailingContacts().Include(nameof(MailingContact.MailingLists)).GetByPublicId(publicId);

            var allProperties = entity.MailingLists.Select(ml => ml.Properties).ToList().SelectMany(p => p.Keys).Distinct();
            var form = new FormBuilder(Forms.Admin.EDIT);
            foreach(var key in allProperties) {
                if (!entity.Properties.Keys.Contains(key)) {
                    entity.Properties[key] = null;
                }
            }

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: $"/api/admin/mailing/contact/{publicId}/edit",
                onSuccess: $"/admin/mailing/contacts/contact/{publicId}"
            );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("contact/" + entity.PublicId, entity.ToString());
            Navigation.Add("edit");

        }

        [RequestHandler("/admin/mailing/contacts/dangling")]
        public void Dangling() {
            var entities = this.DB.MailingContacts().Dangling().OrderBy(e => e.Name);
            entities = this.Template.Filter(entities, this, nameof(MailingContact.Name));
            entities = this.Template.Paginate(entities, this, nameof(MailingContact.Name), "asc");
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("contacts");
            this.Navigation.Add("dangling");
        }

        [RequestHandler("/admin/mailing/contacts/bounced")]
        public void Bounced() {

            // Unsubscribed Contacts
            var unsubscriptions = UnsubscribeService.GetEmailUnsubscriptions(this.DB, MailingUnsubscription.Categories.HardBounce).Select(u => u.Identifier);
            var unsubscribedContacts = this.DB.MailingContacts().Where(a => unsubscriptions.Contains(a.EmailHash));
            var entities = this.Template.Paginate(unsubscribedContacts, this, nameof(MailingContact.Name), "asc");
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("contacts");
            this.Navigation.Add("bounced");
        }


    }
}
