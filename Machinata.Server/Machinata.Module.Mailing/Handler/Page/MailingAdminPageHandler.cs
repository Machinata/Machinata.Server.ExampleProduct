using Machinata.Core.FormBuilder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing.Handler.Page {
    public class MailingAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("mailing");
        }

        #endregion
        [RequestHandler("/admin/mailing")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/mailing/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("mailing.menu-items", menuItems);
            // Navigation
            Navigation.Add("mailing");
        }

        [RequestHandler("/admin/mailing/contacts")]
        public void Contacts() {

            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: this.DB.MailingContacts(),
                form: new Core.FormBuilder.FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
                );
          
            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
        }

        [RequestHandler("/admin/mailing/contacts/create")]
        public void CreateContact() {

            this.Template.InsertForm(
                variableName: "form",
                entity:new MailingContact(),
                form: new Core.FormBuilder.FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/mailing/contacts/create",
                onSuccess: "/admin/mailing/contacts/contact/{mailing-contact.public-id}"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/mailing/contacts/contact/{publicId}")]
        public void Contact(string publicId) {

            var entity = this.DB.MailingContacts().GetByPublicId(publicId);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new Core.FormBuilder.FormBuilder(Forms.Admin.VIEW)
                    );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("contact/" + entity.PublicId, entity.ToString());
            
        }

        [RequestHandler("/admin/mailing/contacts/contact/{publicId}/edit")]
        public void ContactEdit(string publicId) {

            var entity = this.DB.MailingContacts().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new Core.FormBuilder.FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/mailing/contact/{publicId}/edit",
                onSuccess: $"/admin/mailing/contacts/contact/{publicId}"
            );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("contacts");
            Navigation.Add("contact/" + entity.PublicId, entity.ToString());
            Navigation.Add("edit");

        }


    }
}
