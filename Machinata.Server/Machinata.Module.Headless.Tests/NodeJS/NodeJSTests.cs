﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Machinata.Module.Headless.Tests {
    [TestClass]
    public class NodeJS {

        [TestMethod]
        public void Headless_NodeJS_TestExecuteScript() {
            {
                var output = Headless.NodeJS.ExecuteScript("console.log('hello world');");
                Assert.AreEqual("hello world", output);
            }
            {
                var output = Headless.NodeJS.ExecuteScript("console.log(1+1);");
                Assert.AreEqual("2", output);
            }
        }

        [TestMethod]
        public void Headless_NodeJS_TestExecuteFile() {
            {
                var scriptName = "hello-world.js";
                var scriptFile = Core.Config.BinPath + Core.Config.PathSep + "Static" + Core.Config.PathSep + "testing" + Core.Config.PathSep + "nodejs" + Core.Config.PathSep + scriptName;
                var output = Headless.NodeJS.ExecuteFile(scriptFile);
                Assert.AreEqual("hello world", output);
            }
        }

        [TestMethod]
        public void Headless_NodeJS_TestExecuteFileInWorkspace1() {
            {
                var scriptName = "hello-world.js";
                var scriptFile = Core.Config.BinPath + Core.Config.PathSep + "Static" + Core.Config.PathSep + "testing" + Core.Config.PathSep + "nodejs" + Core.Config.PathSep + scriptName;
                var output = Headless.NodeJS.ExecuteFileInWorkspace(scriptFile, null);
                Assert.AreEqual("hello world", output);
            }
        }

        [TestMethod]
        public void Headless_NodeJS_TestExecuteFileInWorkspace2() {
            {
                var scriptName = "canvas-test.js";
                var scriptFile = Core.Config.BinPath + Core.Config.PathSep + "Static" + Core.Config.PathSep + "testing" + Core.Config.PathSep + "nodejs" + Core.Config.PathSep + scriptName;
                var output = Headless.NodeJS.ExecuteFileInWorkspace(scriptFile, null, new string[]{ "canvas" });
                Assert.AreEqual("true", output);
            }
        }

    }
}
