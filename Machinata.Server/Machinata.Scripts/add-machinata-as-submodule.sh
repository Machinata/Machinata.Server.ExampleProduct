git config submodule.recurse true

git submodule add https://git.nerves.ch/Machinata/MachinataServer.git

cd MachinataServer
git checkout master
cd ..

git commit -m "Register MachinataServer as submodule"