using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Model {
    
    public class TimetableConnection : ModelObject {
            
        [FormBuilder]
        public string Name { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductName { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductType { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductLine { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductIcon { get; set; }

        [FormBuilder]
        public string ProductColorBG { get; set; }

        [FormBuilder]
        public string ProductColorFG { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductDirection { get; set; }

        [FormBuilder]
        public string LocationTime { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string LocationName { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string LocationCountdown { get; set; }

        [FormBuilder]
        public string LocationDate { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string LocationPlatform { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public bool Cancelled { get; set; }

        public override string ToString() {
            return this.GetJSON(new FormBuilder()).ToString();
        }

        // Used for OTD
        /// <summary>
        /// Local time
        /// </summary>
        [FormBuilder(Forms.System.JSON)]
        public DateTime LocationDateTime { get; set; }

        // Set the original order of the StopPoitRefs to show them first
        public int StopPointRefOrder { get; set; }

        [FormBuilder(Forms.System.JSON)]
        public long LocationTimestamp {
            get {
                if (this.LocationDateTime != null) {
                    return Core.Util.Time.GetUTCMillisecondsFromDateTime(this.LocationDateTime);
                }
                return -1;
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string ProductColor { get; set; }





    }
}
