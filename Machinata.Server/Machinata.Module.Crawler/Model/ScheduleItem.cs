using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Model {
    
    public class ScheduleItem : ModelObject {
            
        [FormBuilder]
        public string Subject { get; set; }

        [FormBuilder]
        public string Sensitivity { get; set; }

        [FormBuilder]
        public DateTime Start { get; set; }

        [FormBuilder]
        public DateTime End { get; set; }
        
        [FormBuilder]
        public string FreeBusyType { get; set; }
            
            

    }
}
