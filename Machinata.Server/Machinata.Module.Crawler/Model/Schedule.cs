using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Model {
    
    public class Schedule : ModelObject {
            
        [FormBuilder]
        public string Name { get; set; }
        
        [FormBuilder]
        public string ScheduleURL { get; set; }
        
        public List<ScheduleItem> Items { get; set; } = new List<ScheduleItem>();
        
            
        public static Schedule CreateHourlySchedule(DateTime date, int startHour = 8, int endHour = 20) {
            var ret = new Schedule();
            for(int i = startHour; i <= endHour; i++) {
                ret.Items.Add(new ScheduleItem() {
                    Subject = Core.Util.Time.ConvertToDefaultTimezone(date.AddHours(i)).ToShortTimeString(),
                    Start = date.AddHours(i),
                    End = date.AddHours(i).AddMinutes(59).AddSeconds(59)
                });
            }
            return ret;
        }    

    }
}
