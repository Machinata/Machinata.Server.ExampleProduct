using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Model {
    
    public class Timetable : ModelObject {
            
        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string Name { get; set; }

        [FormBuilder]
        public string StationID { get; set; }

        [FormBuilder]
        public string TimetableURL { get; set; }

        [FormBuilder]
        public string TimetableDownload { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.System.JSON)]
        public string LegalText { get; set; }


        public List<TimetableConnection> Connections { get; set; } = new List<TimetableConnection>();
      

        public JObject GetJObject(FormBuilder form = null) {
            var timetable = base.GetJSON(form);
            var connections = this.Connections.Select(s => s.GetJSON(form));
            timetable["connections"] = JToken.FromObject(connections);
            return timetable;
        }

    }
}
