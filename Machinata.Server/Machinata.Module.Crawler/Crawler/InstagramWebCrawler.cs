using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler {
    public class InstagramWebCrawler : WebCrawler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        #endregion
        
        public class InstagramPost : ModelObject {
            
            [FormBuilder]
            public string ImageURL { get; set; }

            [FormBuilder]
            public string Username { get; set; }

            [FormBuilder]
            public string Code { get; set; }

            [FormBuilder]
            public string Caption { get; set; }

            [FormBuilder]
            public bool IsVideo { get; set; }

            public string PostURL {
                get {
                    return "https://www.instagram.com/p/" + this.Code;
                }
            }

        }
        
        public const string SERVER_URL = "https://www.instagram.com";
        public const string PROFILE_URL = "https://www.instagram.com/{username}/";

        #region Example JSON Data
        /*
        {
           "activity_counts":null,
           "config":{
              "csrf_token":"wjhPTlwdI8tEi9UjcAKJVXRXKmrR6Qh0",
              "viewer":null
           },
           "country_code":"CH",
           "language_code":"en",
           "locale":"en_US",
           "entry_data":{
              "ProfilePage":[
                 {
                    "user":{
                       "biography":null,
                       "blocked_by_viewer":false,
                       "country_block":false,
                       "external_url":"http://dankrusi.com/",
                       "external_url_linkshimmed":"http://l.instagram.com/?u=http%3A%2F%2Fdankrusi.com%2F\u0026e=ATOcxILkxJnW8-r4VMTvKq0vRwRhKRD-frx_i64_TsDeGuv_Jo-wvAUwRx5XXRkHRR2Dg0hC",
                       "followed_by":{
                          "count":1078
                       },
                       "followed_by_viewer":false,
                       "follows":{
                          "count":90
                       },
                       "follows_viewer":false,
                       "full_name":"Dan Krusi",
                       "has_blocked_viewer":false,
                       "has_requested_viewer":false,
                       "id":"50701768",
                       "is_private":false,
                       "is_verified":false,
                       "mutual_followers":null,
                       "profile_pic_url":"https://scontent-ams3-1.cdninstagram.com/vp/3c216ba1b1a10172329535d06431c160/5B1B1FD5/t51.2885-19/10499174_1435387236734121_1041813445_a.jpg",
                       "profile_pic_url_hd":"https://scontent-ams3-1.cdninstagram.com/vp/3c216ba1b1a10172329535d06431c160/5B1B1FD5/t51.2885-19/10499174_1435387236734121_1041813445_a.jpg",
                       "requested_by_viewer":false,
                       "username":"dankrusi",
                       "connected_fb_page":null,
                       "media":{
                          "nodes":[
                             {
                                "__typename":"GraphImage",
                                "id":"1702871812047050432",
                                "comments_disabled":false,
                                "dimensions":{
                                   "height":1080,
                                   "width":1080
                                },
                                "edge_media_preview_like":{
                                   "count":19
                                },
                                "gating_info":null,
                                "media_preview":"ACoqS2PO0ggg5/OtbIVznAPBH0x/kVj2xbKsSTkZAxyf84q+Fl5kZfnPToSBjoASMe5xyalDG3F7uUpGpyep9PpjvUEd5Oh+Ybl7jGPyNQo7njODzx+A/lTnLDOSR19Pw/KlcZrrNvUMOM9vT2/Cl3n1plqokjBz0yD+FVMgcZ6VRJWKtGqMuMlQPyX/AOvSx3EcwAdhv5Az25GOcj1J/D8DLcHGzaNxVTwP+AisQxlU3PwSf0/CkM1gIgc+YM4yBx/9fr7809vL2sVcHaOnHP8A+v2zWJtG4D3AqXyflXIPf8OaVhmzBfGNNigE5Jznjn0HWs0yMSSTyfamSSKrAdOCDjj6fn61FtJ/i/UVQi2JgScnAwfzqtkbQh5wc/59aXFWUUelSBADzkKCfp0PqKlPmSDn86mFSAkdOKBmPcQsHHct39x/9ar6QRFQSecDvUNxK+77x4HHJ9ajBJGTTEf/2Q==",
                                "owner":{
                                   "id":"50701768"
                                },
                                "thumbnail_src":"https://scontent-ams3-1.cdninstagram.com/vp/b9e9fe70bcaad70822b790aa1002d5bb/5B4B0334/t51.2885-15/s640x640/sh0.08/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                "thumbnail_resources":[
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/84a096f70b4739ea811103404b7ce820/5B189B30/t51.2885-15/s150x150/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "config_width":150,
                                      "config_height":150
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/00cc6f2a28c1caa6d5138b4849b52805/5B2D2E0F/t51.2885-15/s240x240/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "config_width":240,
                                      "config_height":240
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/feda834ef4e417b0ed7deca061074c5c/5B272477/t51.2885-15/s320x320/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "config_width":320,
                                      "config_height":320
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/104ada7b47881437e6450d0edc2273d5/5B4B0EF1/t51.2885-15/s480x480/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "config_width":480,
                                      "config_height":480
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/b9e9fe70bcaad70822b790aa1002d5bb/5B4B0334/t51.2885-15/s640x640/sh0.08/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "config_width":640,
                                      "config_height":640
                                   }
                                ],
                                "is_video":false,
                                "code":"Beh0OQPFxbA",
                                "date":1517218160,
                                "display_src":"https://scontent-ams3-1.cdninstagram.com/vp/ceac47fa1db7769da2978ab77e22e637/5B091A55/t51.2885-15/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                "caption":"#leipzig",
                                "comments":{
                                   "count":0
                                },
                                "likes":{
                                   "count":19
                                }
                             },
                             ........
                             {
                                "__typename":"GraphImage",
                                "id":"1552118441106783375",
                                "comments_disabled":false,
                                "dimensions":{
                                   "height":1080,
                                   "width":1080
                                },
                                "edge_media_preview_like":{
                                   "count":15
                                },
                                "gating_info":null,
                                "media_preview":"ACoqKekbP07DP1+lOjTzGVPWnvNFBMT7BDjoMe388fzreUuX1MlG5DUU8avgv0FW5FLEyL8yt3HNRzoWQY5yOfzNEnzR069vvBKz9DOMpIIUZA4HT/8AWahBPqam3mQ/KmQOuOP8O3arGR7D8B/hXDsaGpYr85Y/wj+dZrwiOQySZw2Tz3JrVj/dwO/rkfpj+ZrKuVEaIoyQWJxnOMDt6da6ZvVvsStiNXaNiYDj1B5H41aeUuoyAhHXB45/lUUMbugkUcH1Izx3x/kml8mVjwD75/TNc95XGVT97274x+pFQG454B/Jf8KllgkXJ2n8uAB1PWs7d700gOxnZooVC4BJHXn3NYeoTMDHuxnBJwOME4/pW3f9E/H+Vc/qX+tX/cX+tab6j8iyk+0rGWKr6g4A46jP+fap1+U/PKWGByOoJOCP++e/buDWChJPPpWmGKngkfSs7W0EWZdqxMrSFi4wMj15I9cn689KyNsI4y3/AHyK0XkfcBk4x6mrGAeTRewz/9k=",
                                "owner":{
                                   "id":"50701768"
                                },
                                "thumbnail_src":"https://scontent-ams3-1.cdninstagram.com/vp/a3563145175790d9305196e6048288e2/5B0DCDFB/t51.2885-15/s640x640/sh0.08/e35/19931797_1919916914960984_8653995051422056448_n.jpg",
                                "thumbnail_resources":[
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/ccb138135e903d5a18e7f6cc70efeada/5B16BBEA/t51.2885-15/s150x150/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "config_width":150,
                                      "config_height":150
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/469a60ecd6d9444ff619e4095a5f1f0e/5B084F7E/t51.2885-15/s240x240/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "config_width":240,
                                      "config_height":240
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/433b05cd65ffe98f25042a543c9fb792/5B1968DA/t51.2885-15/s320x320/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "config_width":320,
                                      "config_height":320
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/0a3c0df0eeb8c59b356bbb68be61f5bd/5B292C82/t51.2885-15/s480x480/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "config_width":480,
                                      "config_height":480
                                   },
                                   {
                                      "src":"https://scontent-ams3-1.cdninstagram.com/vp/a3563145175790d9305196e6048288e2/5B0DCDFB/t51.2885-15/s640x640/sh0.08/e35/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "config_width":640,
                                      "config_height":640
                                   }
                                ],
                                "is_video":false,
                                "code":"BWKO3-rgLyP",
                                "date":1499246958,
                                "display_src":"https://scontent-ams3-1.cdninstagram.com/vp/597cf3abe240945d85404adc05acf1c0/5B292BF2/t51.2885-15/s1080x1080/e15/fr/19931797_1919916914960984_8653995051422056448_n.jpg",
                                "caption":"#stairway #to #hell",
                                "comments":{
                                   "count":1
                                },
                                "likes":{
                                   "count":15
                                }
                             }
                          ],
                          "count":132,
                          "page_info":{
                             "has_next_page":true,
                             "end_cursor":"AQCOeJm2BodYxp1QcwJ17cv9kE92_wO_2mNFEqF5ZmaSh8GwmU6I6EWS0jf3UsNidZZgCAuGMd8JoJ6pK3cB8Os7t598o2V2HdknY_MjlAWxAw"
                          }
                       },
                       "saved_media":{
                          "nodes":[

                          ],
                          "count":0,
                          "page_info":{
                             "has_next_page":false,
                             "end_cursor":null
                          }
                       },
                       "media_collections":{
                          "count":0,
                          "page_info":{
                             "has_next_page":false,
                             "end_cursor":null
                          },
                          "edges":[

                          ]
                       }
                    },
                    "logging_page_id":"profilePage_50701768",
                    "show_suggested_profiles":false,
                    "graphql":{
                       "user":{
                          "biography":"",
                          "blocked_by_viewer":false,
                          "country_block":false,
                          "external_url":"http://dankrusi.com/",
                          "external_url_linkshimmed":"http://l.instagram.com/?u=http%3A%2F%2Fdankrusi.com%2F\u0026e=ATOcxILkxJnW8-r4VMTvKq0vRwRhKRD-frx_i64_TsDeGuv_Jo-wvAUwRx5XXRkHRR2Dg0hC",
                          "edge_followed_by":{
                             "count":1078
                          },
                          "followed_by_viewer":false,
                          "edge_follow":{
                             "count":90
                          },
                          "follows_viewer":false,
                          "full_name":"Dan Krusi",
                          "has_blocked_viewer":false,
                          "has_requested_viewer":false,
                          "id":"50701768",
                          "is_private":false,
                          "is_verified":false,
                          "mutual_followers":null,
                          "profile_pic_url":"https://scontent-ams3-1.cdninstagram.com/vp/3c216ba1b1a10172329535d06431c160/5B1B1FD5/t51.2885-19/10499174_1435387236734121_1041813445_a.jpg",
                          "profile_pic_url_hd":"https://scontent-ams3-1.cdninstagram.com/vp/3c216ba1b1a10172329535d06431c160/5B1B1FD5/t51.2885-19/10499174_1435387236734121_1041813445_a.jpg",
                          "requested_by_viewer":false,
                          "username":"dankrusi",
                          "connected_fb_page":null,
                          "edge_owner_to_timeline_media":{
                             "count":132,
                             "page_info":{
                                "has_next_page":true,
                                "end_cursor":"AQCOeJm2BodYxp1QcwJ17cv9kE92_wO_2mNFEqF5ZmaSh8GwmU6I6EWS0jf3UsNidZZgCAuGMd8JoJ6pK3cB8Os7t598o2V2HdknY_MjlAWxAw"
                             },
                             "edges":[
                                {
                                   "node":{
                                      "__typename":"GraphImage",
                                      "id":"1702871812047050432",
                                      "edge_media_to_caption":{
                                         "edges":[
                                            {
                                               "node":{
                                                  "text":"#leipzig"
                                               }
                                            }
                                         ]
                                      },
                                      "shortcode":"Beh0OQPFxbA",
                                      "edge_media_to_comment":{
                                         "count":0
                                      },
                                      "comments_disabled":false,
                                      "taken_at_timestamp":1517218160,
                                      "dimensions":{
                                         "height":1080,
                                         "width":1080
                                      },
                                      "display_url":"https://scontent-ams3-1.cdninstagram.com/vp/ceac47fa1db7769da2978ab77e22e637/5B091A55/t51.2885-15/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "edge_liked_by":{
                                         "count":19
                                      },
                                      "edge_media_preview_like":{
                                         "count":19
                                      },
                                      "gating_info":null,
                                      "media_preview":"ACoqS2PO0ggg5/OtbIVznAPBH0x/kVj2xbKsSTkZAxyf84q+Fl5kZfnPToSBjoASMe5xyalDG3F7uUpGpyep9PpjvUEd5Oh+Ybl7jGPyNQo7njODzx+A/lTnLDOSR19Pw/KlcZrrNvUMOM9vT2/Cl3n1plqokjBz0yD+FVMgcZ6VRJWKtGqMuMlQPyX/AOvSx3EcwAdhv5Az25GOcj1J/D8DLcHGzaNxVTwP+AisQxlU3PwSf0/CkM1gIgc+YM4yBx/9fr7809vL2sVcHaOnHP8A+v2zWJtG4D3AqXyflXIPf8OaVhmzBfGNNigE5Jznjn0HWs0yMSSTyfamSSKrAdOCDjj6fn61FtJ/i/UVQi2JgScnAwfzqtkbQh5wc/59aXFWUUelSBADzkKCfp0PqKlPmSDn86mFSAkdOKBmPcQsHHct39x/9ar6QRFQSecDvUNxK+77x4HHJ9ajBJGTTEf/2Q==",
                                      "owner":{
                                         "id":"50701768"
                                      },
                                      "thumbnail_src":"https://scontent-ams3-1.cdninstagram.com/vp/b9e9fe70bcaad70822b790aa1002d5bb/5B4B0334/t51.2885-15/s640x640/sh0.08/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                      "thumbnail_resources":[
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/84a096f70b4739ea811103404b7ce820/5B189B30/t51.2885-15/s150x150/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                            "config_width":150,
                                            "config_height":150
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/00cc6f2a28c1caa6d5138b4849b52805/5B2D2E0F/t51.2885-15/s240x240/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                            "config_width":240,
                                            "config_height":240
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/feda834ef4e417b0ed7deca061074c5c/5B272477/t51.2885-15/s320x320/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                            "config_width":320,
                                            "config_height":320
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/104ada7b47881437e6450d0edc2273d5/5B4B0EF1/t51.2885-15/s480x480/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                            "config_width":480,
                                            "config_height":480
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/b9e9fe70bcaad70822b790aa1002d5bb/5B4B0334/t51.2885-15/s640x640/sh0.08/e35/27573402_162573161050101_8913077133803257856_n.jpg",
                                            "config_width":640,
                                            "config_height":640
                                         }
                                      ],
                                      "is_video":false
                                   }
                                },
                                .............
                                {
                                   "node":{
                                      "__typename":"GraphImage",
                                      "id":"1552118441106783375",
                                      "edge_media_to_caption":{
                                         "edges":[
                                            {
                                               "node":{
                                                  "text":"#stairway #to #hell"
                                               }
                                            }
                                         ]
                                      },
                                      "shortcode":"BWKO3-rgLyP",
                                      "edge_media_to_comment":{
                                         "count":1
                                      },
                                      "comments_disabled":false,
                                      "taken_at_timestamp":1499246958,
                                      "dimensions":{
                                         "height":1080,
                                         "width":1080
                                      },
                                      "display_url":"https://scontent-ams3-1.cdninstagram.com/vp/597cf3abe240945d85404adc05acf1c0/5B292BF2/t51.2885-15/s1080x1080/e15/fr/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "edge_liked_by":{
                                         "count":15
                                      },
                                      "edge_media_preview_like":{
                                         "count":15
                                      },
                                      "gating_info":null,
                                      "media_preview":"ACoqKekbP07DP1+lOjTzGVPWnvNFBMT7BDjoMe388fzreUuX1MlG5DUU8avgv0FW5FLEyL8yt3HNRzoWQY5yOfzNEnzR069vvBKz9DOMpIIUZA4HT/8AWahBPqam3mQ/KmQOuOP8O3arGR7D8B/hXDsaGpYr85Y/wj+dZrwiOQySZw2Tz3JrVj/dwO/rkfpj+ZrKuVEaIoyQWJxnOMDt6da6ZvVvsStiNXaNiYDj1B5H41aeUuoyAhHXB45/lUUMbugkUcH1Izx3x/kml8mVjwD75/TNc95XGVT97274x+pFQG454B/Jf8KllgkXJ2n8uAB1PWs7d700gOxnZooVC4BJHXn3NYeoTMDHuxnBJwOME4/pW3f9E/H+Vc/qX+tX/cX+tab6j8iyk+0rGWKr6g4A46jP+fap1+U/PKWGByOoJOCP++e/buDWChJPPpWmGKngkfSs7W0EWZdqxMrSFi4wMj15I9cn689KyNsI4y3/AHyK0XkfcBk4x6mrGAeTRewz/9k=",
                                      "owner":{
                                         "id":"50701768"
                                      },
                                      "thumbnail_src":"https://scontent-ams3-1.cdninstagram.com/vp/a3563145175790d9305196e6048288e2/5B0DCDFB/t51.2885-15/s640x640/sh0.08/e35/19931797_1919916914960984_8653995051422056448_n.jpg",
                                      "thumbnail_resources":[
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/ccb138135e903d5a18e7f6cc70efeada/5B16BBEA/t51.2885-15/s150x150/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                            "config_width":150,
                                            "config_height":150
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/469a60ecd6d9444ff619e4095a5f1f0e/5B084F7E/t51.2885-15/s240x240/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                            "config_width":240,
                                            "config_height":240
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/433b05cd65ffe98f25042a543c9fb792/5B1968DA/t51.2885-15/s320x320/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                            "config_width":320,
                                            "config_height":320
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/0a3c0df0eeb8c59b356bbb68be61f5bd/5B292C82/t51.2885-15/s480x480/e15/19931797_1919916914960984_8653995051422056448_n.jpg",
                                            "config_width":480,
                                            "config_height":480
                                         },
                                         {
                                            "src":"https://scontent-ams3-1.cdninstagram.com/vp/a3563145175790d9305196e6048288e2/5B0DCDFB/t51.2885-15/s640x640/sh0.08/e35/19931797_1919916914960984_8653995051422056448_n.jpg",
                                            "config_width":640,
                                            "config_height":640
                                         }
                                      ],
                                      "is_video":false
                                   }
                                }
                             ]
                          },
                          "edge_saved_media":{
                             "count":0,
                             "page_info":{
                                "has_next_page":false,
                                "end_cursor":null
                             },
                             "edges":[

                             ]
                          },
                          "edge_media_collections":{
                             "count":0,
                             "page_info":{
                                "has_next_page":false,
                                "end_cursor":null
                             },
                             "edges":[

                             ]
                          }
                       }
                    }
                 }
              ]
           },
           "gatekeepers":{
              "ld":true,
              "seo":true
           },
           "qe":{
              "834cfd9a":{
                 "g":"",
                 "p":{

                 }
              },
              ..............
           },
           "hostname":"www.instagram.com",
           "display_properties_server_guess":{
              "pixel_ratio":1.5,
              "viewport_width":360,
              "viewport_height":480,
              "orientation":""
           },
           "environment_switcher_visible_server_guess":true,
           "platform":"web",
           "nonce":"eMEpqyTqULbfUhcno/CWVA==",
           "zero_data":{

           },
           "rollout_hash":"3156f3856bb5",
           "probably_has_app":false,
           "show_app_install":true
        }*/
        #endregion

        public const string IMAGE_EXTRACTION_REGEX = "https:\\/\\/[0-9A-Za-z-]+.cdninstagram\\.com\\/vp\\/[0-9A-Za-z-/._]+s640x640[0-9A-Za-z-/._]+\\.jpg";

        // See https://gist.github.com/cosmocatalano/4544576
        public const string JSON_EXTRACTION_PREFIX = "window._sharedData = ";
        public const string JSON_EXTRACTION_POSTFIX = ";</script>";

        public InstagramWebCrawler() : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_RANDOM_BOT);
        }

        public List<InstagramPost> GetRecentPostsForUsername(string username) {
            var ret = new List<InstagramPost>();
            var json = GetJSONExtractionFromURL(PROFILE_URL.Replace("{username}", username),JSON_EXTRACTION_PREFIX,JSON_EXTRACTION_POSTFIX);
            try {
                if (json["entry_data"] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data is empty.");
                if (json["entry_data"]["ProfilePage"] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data.ProfilePage is empty. JSON recieved: "+json.ToString());
                if (json["entry_data"]["ProfilePage"][0] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data.ProfilePage[0] is empty.");
                if (json["entry_data"]["ProfilePage"][0]["graphql"] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data.ProfilePage[0].user is empty.");
                if (json["entry_data"]["ProfilePage"][0]["graphql"]["user"] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data.ProfilePage[0].user.media is empty.");
                if (json["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"] == null) throw new BackendException("feed-fail", "Could not get instagram feed: entry_data.ProfilePage[0].user.media.nodes is empty.");
            }catch(Exception e) {
                _logger.Error(e,"Error with instagram JSON: "+json.ToString());
                throw e;
            }
            //var jposts = json["entry_data"]["ProfilePage"][0]["user"]["media"]["nodes"];
            var jposts = json["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"];
            foreach(var jpost in jposts) {
                var post = new InstagramPost();
                post.ImageURL = jpost["node"]["thumbnail_resources"].Last()["src"].ToString();
                post.Code = jpost["node"]["shortcode"]?.ToString();
                post.Caption = jpost["node"]["caption"]?.ToString();
                post.Username = username;
                post.IsVideo = jpost["node"]["is_video"]?.ToString().ToLower() == "true";
                ret.Add(post);
            }
            return ret;
        }
    }
}
