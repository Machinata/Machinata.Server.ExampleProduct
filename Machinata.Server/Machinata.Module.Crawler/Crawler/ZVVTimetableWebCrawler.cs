using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Crawler.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler {
    public class ZVVTimetableWebCrawler : WebCrawler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        #endregion
        
        
        public const string SERVER_URL = "https://online.fahrplan.zvv.ch";
        public const string TIMETABLE_REQUEST = "/bin/stboard.exe/dny?_ts={ts}";

        #region Example JSON Data
        /*
            boardType	dep
            date	09.02.19
            dirInput	
            input	Zürich,+Binz
            maxJourneys	8
            REQStationS0ID	A=1@O=Zürich,+Binz@X=8518322@Y=47362958@U=85@L=008591824@B=1@p=1549370986@
            start	1
            time	17:08
            tpl	stbResult2json


{
"station":{
"name":"Z&#252;rich, Binz",
"additionalInformation":{
"attr_MU": false,
"attr_HU": true,
"attr_HU_val": "493"
,"timetableUrl":"https://www.zvv.ch/zvv/de/fahrplan/haltestellenfahrplaene.html?hstNr=493"
}
},
"connections":[
{
"name":"1",
"date":"09.02.19",
"product":{ "name":"Bus   76", "longName":"Bus", "type":"6" ,"line":"76" ,"direction":"Z&#252;rich, Binz Center" ,"directionType":"to" ,"icon":"icon_bus" ,"color":{"fg":"000000","bg":"ffffff"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:12",
"countdown":"3",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:12",
"countdown":"3",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:12",
"countdown":"3",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:12",
"countdown":"3",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich, R&#228;ffelstrasse",
"x":8514663,
"y":47362743,
"id":"8591308",
"tupelId":"",
"type":"STATION"
},
"time":"17:13",
"countdown":"4",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Grubenstrasse",
"x":8511679,
"y":47362833,
"id":"8591083",
"tupelId":"",
"type":"STATION"
},
"time":"17:14",
"countdown":"5",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Binz Center",
"x":8511050,
"y":47361889,
"id":"8591084",
"tupelId":"",
"type":"STATION"
},
"time":"17:14",
"countdown":"5",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"634605/219377/721054/149006/85",
"cancelled":false,
"attributes_bfr":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"attributes":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"2",
"date":"09.02.19",
"product":{ "name":"Bus   76", "longName":"Bus", "type":"6" ,"line":"76" ,"direction":"Z&#252;rich, Bahnhof Wiedikon" ,"directionType":"to" ,"icon":"icon_bus" ,"color":{"fg":"000000","bg":"ffffff"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:18",
"countdown":"9",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:18",
"countdown":"9",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:18",
"countdown":"9",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:18",
"countdown":"9",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich, Manesseplatz",
"x":8521630,
"y":47365511,
"id":"299816",
"tupelId":"",
"type":"STATION"
},
"time":"17:20",
"countdown":"11",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Schmiede Wiedikon",
"x":8519068,
"y":47370123,
"id":"299745",
"tupelId":"",
"type":"STATION"
},
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Bahnhof Wiedikon",
"x":8524201,
"y":47371453,
"id":"299611",
"tupelId":"",
"type":"STATION"
},
"time":"17:23",
"countdown":"14",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"174285/65942/511904/197871/85",
"cancelled":false,
"attributes_bfr":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"attributes":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"3",
"date":"09.02.19",
"product":{ "name":"S     10", "longName":"S-Bahn", "type":"5" ,"line":"S     10" ,"direction":"Uetliberg" ,"directionType":"to" ,"icon":"icon_train" ,"color":{"fg":"000000","bg":"FFFFFF"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:19",
"countdown":"10",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:19",
"countdown":"10",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:19",
"countdown":"10",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:19",
"countdown":"10",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich Friesenberg (SZU)",
"x":8508020,
"y":47364747,
"id":"8503052",
"tupelId":"",
"type":"STATION"
},
"time":"17:21",
"countdown":"12",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich Schweighof (SZU)",
"x":8503139,
"y":47364981,
"id":"8503053",
"tupelId":"",
"type":"STATION"
},
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich Triemli (SZU)",
"x":8495220,
"y":47364927,
"id":"8503054",
"tupelId":"",
"type":"STATION"
},
"time":"17:24",
"countdown":"15",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Uitikon Waldegg",
"x":8465663,
"y":47366087,
"id":"8503055",
"tupelId":"",
"type":"STATION"
},
"time":"17:28",
"countdown":"19",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Ringlikon",
"x":8477556,
"y":47360136,
"id":"8503056",
"tupelId":"",
"type":"STATION"
},
"time":"17:30",
"countdown":"21",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Uetliberg",
"x":8487651,
"y":47352360,
"id":"8503057",
"tupelId":"",
"type":"STATION"
},
"time":"17:35",
"countdown":"26",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"427902/181274/888710/301722/85",
"cancelled":false,
"attributes_bfr":[
],
"attributes":[
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"4",
"date":"09.02.19",
"product":{ "name":"S     10", "longName":"S-Bahn", "type":"5" ,"line":"S     10" ,"direction":"Z&#252;rich HB SZU" ,"directionType":"to" ,"icon":"icon_train" ,"color":{"fg":"000000","bg":"FFFFFF"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:22",
"countdown":"13",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich Selnau (SZU)",
"x":8531626,
"y":47372074,
"id":"8503090",
"tupelId":"",
"type":"STATION"
},
"time":"17:24",
"countdown":"15",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich HB SZU",
"x":8539024,
"y":47377602,
"id":"8503088",
"tupelId":"",
"type":"STATION"
},
"time":"17:27",
"countdown":"18",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"494262/203392/261048/34231/85",
"cancelled":false,
"attributes_bfr":[
],
"attributes":[
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"5",
"date":"09.02.19",
"product":{ "name":"S     10", "longName":"S-Bahn", "type":"5" ,"line":"S     10" ,"direction":"Uetliberg" ,"directionType":"to" ,"icon":"icon_train" ,"color":{"fg":"000000","bg":"FFFFFF"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:39",
"countdown":"30",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:39",
"countdown":"30",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:39",
"countdown":"30",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:39",
"countdown":"30",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich Friesenberg (SZU)",
"x":8508020,
"y":47364747,
"id":"8503052",
"tupelId":"",
"type":"STATION"
},
"time":"17:41",
"countdown":"32",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich Schweighof (SZU)",
"x":8503139,
"y":47364981,
"id":"8503053",
"tupelId":"",
"type":"STATION"
},
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich Triemli (SZU)",
"x":8495220,
"y":47364927,
"id":"8503054",
"tupelId":"",
"type":"STATION"
},
"time":"17:44",
"countdown":"35",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Uitikon Waldegg",
"x":8465663,
"y":47366087,
"id":"8503055",
"tupelId":"",
"type":"STATION"
},
"time":"17:48",
"countdown":"39",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Ringlikon",
"x":8477556,
"y":47360136,
"id":"8503056",
"tupelId":"",
"type":"STATION"
},
"time":"17:50",
"countdown":"41",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Uetliberg",
"x":8487651,
"y":47352360,
"id":"8503057",
"tupelId":"",
"type":"STATION"
},
"time":"17:55",
"countdown":"46",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"118620/78185/636008/278465/85",
"cancelled":false,
"attributes_bfr":[
],
"attributes":[
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"6",
"date":"09.02.19",
"product":{ "name":"Bus   76", "longName":"Bus", "type":"6" ,"line":"76" ,"direction":"Z&#252;rich, Binz Center" ,"directionType":"to" ,"icon":"icon_bus" ,"color":{"fg":"000000","bg":"ffffff"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich, R&#228;ffelstrasse",
"x":8514663,
"y":47362743,
"id":"8591308",
"tupelId":"",
"type":"STATION"
},
"time":"17:43",
"countdown":"34",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Grubenstrasse",
"x":8511679,
"y":47362833,
"id":"8591083",
"tupelId":"",
"type":"STATION"
},
"time":"17:44",
"countdown":"35",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Binz Center",
"x":8511050,
"y":47361889,
"id":"8591084",
"tupelId":"",
"type":"STATION"
},
"time":"17:44",
"countdown":"35",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"192696/72074/336990/104278/85",
"cancelled":false,
"attributes_bfr":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"attributes":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"7",
"date":"09.02.19",
"product":{ "name":"S     10", "longName":"S-Bahn", "type":"5" ,"line":"S     10" ,"direction":"Z&#252;rich HB SZU" ,"directionType":"to" ,"icon":"icon_train" ,"color":{"fg":"000000","bg":"FFFFFF"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich Binz (SZU)",
"x":8518556,
"y":47362617,
"id":"8503051",
"tupelId":"",
"type":"STATION"
},
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:42",
"countdown":"33",
"date":"09.02.19",
"delay":"0",
"platform":"1",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich Selnau (SZU)",
"x":8531626,
"y":47372074,
"id":"8503090",
"tupelId":"",
"type":"STATION"
},
"time":"17:44",
"countdown":"35",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich HB SZU",
"x":8539024,
"y":47377602,
"id":"8503088",
"tupelId":"",
"type":"STATION"
},
"time":"17:47",
"countdown":"38",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"324201/146711/353784/68826/85",
"cancelled":false,
"attributes_bfr":[
],
"attributes":[
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
,
{
"name":"8",
"date":"09.02.19",
"product":{ "name":"Bus   76", "longName":"Bus", "type":"6" ,"line":"76" ,"direction":"Z&#252;rich, Bahnhof Wiedikon" ,"directionType":"to" ,"icon":"icon_bus" ,"color":{"fg":"000000","bg":"ffffff"} }
,
"mainLocation":{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:48",
"countdown":"39",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:48",
"countdown":"39",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
},
"locations":[
{
"location":{
"name":"Z&#252;rich, Binz",
"x":8518322,
"y":47362958,
"id":"299977",
"tupelId":"",
"type":"STATION"
},
"time":"17:48",
"countdown":"39",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"17:48",
"countdown":"39",
"date":"09.02.19",
"delay":"0",
"platform":"",
"isPlatformChanged":false,
"isDelayed":false,
"hasRealTime":true
}
}
,
{
"location":{
"name":"Z&#252;rich, Manesseplatz",
"x":8521630,
"y":47365511,
"id":"299816",
"tupelId":"",
"type":"STATION"
},
"time":"17:50",
"countdown":"41",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Schmiede Wiedikon",
"x":8519068,
"y":47370123,
"id":"299745",
"tupelId":"",
"type":"STATION"
},
"time":"17:52",
"countdown":"43",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
,
{
"location":{
"name":"Z&#252;rich, Bahnhof Wiedikon",
"x":8524201,
"y":47371453,
"id":"299611",
"tupelId":"",
"type":"STATION"
},
"time":"17:53",
"countdown":"44",
"date":"09.02.19",
"platform":"",
"realTime": {
"time":"",
"countdown":"",
"date":"",
"delay":"",
"platform":"",
"isPlatformChanged":false,
"isDelayed":true,
"hasRealTime":false
}
}
],
"trainInfo":"468495/164012/129552/91404/85",
"cancelled":false,
"attributes_bfr":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"attributes":[
{
"code":"NF",
"text":"Niederflurfahrzeug",
"priority":"100",
"hightPriority":false
}
],
"himMessages":[
],
"hasHIMMessage":false,
"hasGlobalHIMMessage":false
}
]
}


        */
        #endregion
            

        public ZVVTimetableWebCrawler() : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
        }

        public string GetCopyright() {
            return $"© ZVV {DateTime.Now.Year}";
        }

        public Timetable GetTimetableForStation(string stationName, string stationS0ID, DateTime dateTime, int maxConnections = 8) {
            var ret = new Timetable();

            var postData = new NameValueCollection();
            postData.Add("boardType", "dep");
            postData.Add("date", dateTime.ToString("dd.MM.yy"));
            postData.Add("dirInput", "");
            postData.Add("input", stationName.Replace(" ","+"));
            postData.Add("maxJourneys", maxConnections.ToString());
            //postData.Add("REQStationS0ID", "A=1@O=Zürich,+Binz@X=8518322@Y=47362958@U=85@L=008591824@B=1@p=1549370986@");
            //postData.Add("REQStationS0ID", "O=Zürich,+Binz@");
            //postData.Add("REQStationS0ID", "A=1@O=Lachen+SZ@X=8853188@Y=47189754@U=85@L=008503220@B=1@p=1554812778@");
            if(stationS0ID != null && stationS0ID != "") postData.Add("REQStationS0ID", stationS0ID);
            postData.Add("start", "1");
            postData.Add("time", dateTime.ToString("HH:mm"));
            postData.Add("tpl", "stbResult2json");
            var url = TIMETABLE_REQUEST.Replace("{rnd}",Core.Util.Math.RandomInt().ToString());
            var localTime = Core.Util.Time.ToDefaultTimezone(DateTime.UtcNow);
            var utcTime = DateTime.UtcNow; // web app uses utc 1970 ms
            url = url.Replace("{ts}",Core.Util.Time.GetUTCMillisecondsFromDateTime(utcTime).ToString());
            var json = PostJSON(url,postData);

            // Parse out all data
            ret.Name = json["station"]["name"].ToString();
            ret.StationID = json["station"]["additionalInformation"]["attr_HU_val"]?.ToString();
            ret.TimetableURL = json["station"]["additionalInformation"]["timetableUrl"]?.ToString();
            if(ret.TimetableURL != null) {
                //NOTE: There seems to be an error in this
                ret.TimetableURL = "https://online.fahrplan.zvv.ch/bin/stboard.exe/dn?lang={page.language}&hstNr={id}".Replace("{id}",ret.StationID);
            } else {
                ret.TimetableURL = "https://online.fahrplan.zvv.ch/bin/stboard.exe/dn?lang={page.language}&hstNr={id}".Replace("{id}",ret.StationID);
            }
            ret.TimetableDownload = "https://online.fahrplaninfo.zvv.ch/frame_hst3.php?lang={page.language}&hstNr={id}&hstName=".Replace("{id}",ret.StationID);
            foreach(var jconnection in json["connections"]) {
                try {
                    var connection = new TimetableConnection();
                    connection.Name = jconnection["name"].ToString();
                    connection.ProductName = Core.Util.XML.HTMLDecodeString(jconnection["product"]["name"].ToString());
                    connection.ProductLine = jconnection["product"]["line"].ToString();
                    connection.ProductType = jconnection["product"]["type"].ToString();
                    connection.ProductIcon = jconnection["product"]["icon"].ToString();
                    connection.ProductColorBG = jconnection["product"]["color"]["bg"].ToString();
                    connection.ProductColorFG = jconnection["product"]["color"]["fg"].ToString();
                    connection.ProductDirection = Core.Util.XML.HTMLDecodeString(jconnection["product"]["direction"].ToString());
                    connection.LocationName = Core.Util.XML.HTMLDecodeString(jconnection["mainLocation"]["location"]["name"].ToString());
                    connection.LocationTime = jconnection["mainLocation"]["time"].ToString();
                    connection.LocationDate = jconnection["mainLocation"]["date"].ToString();
                    connection.LocationPlatform = jconnection["mainLocation"]["platform"].ToString();
                    connection.LocationCountdown = jconnection["mainLocation"]["countdown"].ToString();
                    if (connection.ProductIcon != null) connection.ProductIcon = connection.ProductIcon.Replace("icon", "").Trim(' ', '_', '-');
                    if (connection.ProductLine != null) connection.ProductLine = connection.ProductLine.Replace(" ", "").Trim(' ', '_', '-');
                    ret.Connections.Add(connection);
                }catch(Exception e1) {
                    _logger.Warn(e1,"Could not parse connection: "+jconnection.ToString());
                }
            }
            return ret;
        }
    }
}
