using System;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Machinata.Module.Crawler {


    #region Notes

    /*


        called GSOD (not GHCN)


        ftp://ftp.ncdc.noaa.gov/pub/data/gsod/readme.txt


        all countries: ftp://ftp.ncdc.noaa.gov/pub/data/gsod/country-list.txt


        all stations: ftp://ftp.ncdc.noaa.gov/pub/data/noaa/isd-history.txt

        zurich is 066700


        data for station: ftp://ftp.ncdc.noaa.gov/pub/data/gsod/2018/066700-99999-2018.op.gz



        example data:

        STN--- WBAN   YEARMODA    TEMP       DEWP      SLP        STP       VISIB      WDSP     MXSPD   GUST    MAX     MIN   PRCP   SNDP   FRSHTT
        066700 99999  20180101    43.4 24    35.2 24  9999.9  0  9999.9  0    6.6 24   11.6 24   19.2   36.5    48.2*   39.2   0.07G 999.9  010000
        066700 99999  20180102    40.0 24    33.3 24  9999.9  0  9999.9  0    6.3 24    8.6 24   15.9   37.7    44.6*   34.9*  0.06G 999.9  011000
        066700 99999  20180103    43.9 24    37.6 24  9999.9  0  9999.9  0    6.2 24   12.7 24   32.1   48.4    53.6*   37.4*  0.31G 999.9  010000
        066700 99999  20180104    46.3 24    42.1 24  9999.9  0  9999.9  0    5.3 24   14.0 24   22.0   44.3    53.6*   39.0   0.60G 999.9  010000
        066700 99999  20180105    49.5 24    44.1 24  9999.9  0  9999.9  0    6.1 24    7.3 24   15.2   44.3    54.0*   44.6*  0.98G 999.9  010000
        066700 99999  20180106    43.6 24    41.4 24  9999.9  0  9999.9  0    4.6 24    2.1 24    6.4   11.5    45.9*   37.8   0.13G 999.9  110000
        066700 99999  20180107    41.9 24    38.3 24  9999.9  0  9999.9  0    5.8 24    4.9 24   12.0   18.6    44.8*   39.2*  0.00G 999.9  000000
        066700 99999  20180108    40.9 24    36.2 24  9999.9  0  9999.9  0    6.1 24    5.6 24   11.7   18.5    46.4*   38.1   0.04E 999.9  000000
        066700 99999  20180109    39.6 24    35.7 24  9999.9  0  9999.9  0    5.5 15    3.2 24    8.7   15.9    46.4*   33.8*  0.00I 999.9  100000
        066700 99999  20180110    34.9 24    33.3 24  9999.9  0  9999.9  0    3.1 24    2.9 24    7.0  999.9    41.0*   28.4*  0.00I 999.9  100000
        066700 99999  20180111    39.3 24    36.0 24  9999.9  0  9999.9  0    4.6 24    1.9 24    5.1  999.9    44.8*   35.1   0.01G 999.9  010000
        066700 99999  20180112    37.9 24    33.3 24  9999.9  0  9999.9  0    4.9 24    5.3 24   11.3   15.3    39.7*   32.0*  0.00G




        The daily elements included in the dataset (as available from each
        station) are:

        Mean temperature (.1 Fahrenheit)
        Mean dew point (.1 Fahrenheit)
        Mean sea level pressure (.1 mb)
        Mean station pressure (.1 mb)
        Mean visibility (.1 miles)
        Mean wind speed (.1 knots)
        Maximum sustained wind speed (.1 knots)
        Maximum wind gust (.1 knots)
        Maximum temperature (.1 Fahrenheit)
        Minimum temperature (.1 Fahrenheit)
        Precipitation amount (.01 inches)
        Snow depth (.1 inches)
        Indicator for occurrence of:  Fog
                                      Rain or Drizzle
                                      Snow or Ice Pellets
                                      Hail
                                      Thunder
                                      Tornado/Funnel Cloud

        For details on the contents of the dataset, see the format
        documentation shown below.  

        The data are available via:
        1) WWW -- http://www.ncdc.noaa.gov/cgi-bin/res40.pl?page=gsod.html


        DETAILS/FORMAT

        Global summary of day data for 18 surface meteorological elements
        are derived from the synoptic/hourly observations contained in
        USAF DATSAV3 Surface data and Federal Climate Complex Integrated 
        Surface Data (ISD).  Historical data are generally available for 1929 to
        the present, with data from 1973 to the present being the most complete.  
        For some periods, one or more countries' data may not be available due to
        data restrictions or communications problems.  In deriving the summary of 
        day data, a minimum of 4 observations for the day must be present (allows 
        for stations which report 4 synoptic observations/day).  Since the data are
        converted to constant units (e.g, knots), slight rounding error from the
        originally reported values may occur (e.g, 9.9 instead of 10.0).

        The mean daily values described below are based on the hours of
        operation for the station.  For some stations/countries, the
        visibility will sometimes 'cluster' around a value (such as 10
        miles) due to the practice of not reporting visibilities greater
        than certain distances.  The daily extremes and totals--maximum
        wind gust, precipitation amount, and snow depth--will only appear
        if the station reports the data sufficiently to provide a valid value.
        Therefore, these three elements will appear less frequently than 
        other values.  Also, these elements are derived from the stations'
        reports during the day, and may comprise a 24-hour period which
        includes a portion of the previous day.  The data are reported and
        summarized based on Greenwich Mean Time (GMT, 0000Z - 2359Z) since
        the original synoptic/hourly data are reported and based on GMT.

        As for quality control (QC), the input data undergo extensive
        automated QC to correctly 'decode' as much of the synoptic data as
        possible, and to eliminate many of the random errors found in the
        original data.  Then, these data are QC'ed further as the summary of
        day data are derived.  However, we expect that a very small % of the
        errors will remain in the summary of day data.

        The data are strictly ASCII, with a mixture of character data, real
        values, and integer values. 

        Following is the data format:

        First record--header record.
        All ensuing records--data records as described below.
        All 9's in a field (e.g., 99.99 for PRCP) indicates no report or
        insufficient data.

        FIELD   POSITION  TYPE   DESCRIPTION

        STN---  1-6       Int.   Station number (WMO/DATSAV3 number)
                                 for the location.

        WBAN    8-12      Int.   WBAN number where applicable--this is the
                                 historical "Weather Bureau Air Force Navy"
                                 number - with WBAN being the acronym.

        YEAR    15-18     Int.   The year.

        MODA    19-22     Int.   The month and day.

        TEMP    25-30     Real   Mean temperature for the day in degrees
                                 Fahrenheit to tenths.  Missing = 9999.9
        Count   32-33     Int.   Number of observations used in 
                                 calculating mean temperature.

        DEWP    36-41     Real   Mean dew point for the day in degrees
                                 Fahrenheit to tenths.  Missing = 9999.9
        Count   43-44     Int.   Number of observations used in 
                                 calculating mean dew point.  

        SLP     47-52     Real   Mean sea level pressure for the day
                                 in millibars to tenths.  Missing =       
                                 9999.9
        Count   54-55     Int.   Number of observations used in 
                                 calculating mean sea level pressure.

        STP     58-63     Real   Mean station pressure for the day
                                 in millibars to tenths.  Missing =       
                                 9999.9
        Count   65-66     Int.   Number of observations used in 
                                 calculating mean station pressure.  

        VISIB   69-73     Real   Mean visibility for the day in miles
                                 to tenths.  Missing = 999.9
        Count   75-76     Int.   Number of observations used in 
                                 calculating mean visibility.      

        WDSP    79-83     Real   Mean wind speed for the day in knots
                                 to tenths.  Missing = 999.9 
        Count   85-86     Int.   Number of observations used in 
                                 calculating mean wind speed.

        MXSPD   89-93     Real   Maximum sustained wind speed reported 
                                 for the day in knots to tenths.
                                 Missing = 999.9

        GUST    96-100    Real   Maximum wind gust reported for the day
                                 in knots to tenths.  Missing = 999.9

        MAX     103-108   Real   Maximum temperature reported during the 
                                 day in Fahrenheit to tenths--time of max 
                                 temp report varies by country and        
                                 region, so this will sometimes not be    
                                 the max for the calendar day.  Missing = 
                                 9999.9     
        Flag    109-109   Char   Blank indicates max temp was taken from the
                                 explicit max temp report and not from the              
                                 'hourly' data.  * indicates max temp was 
                                 derived from the hourly data (i.e., highest
                                 hourly or synoptic-reported temperature).

        MIN     111-116   Real   Minimum temperature reported during the 
                                 day in Fahrenheit to tenths--time of min 
                                 temp report varies by country and        
                                 region, so this will sometimes not be  
                                 the min for the calendar day.  Missing = 
                                 9999.9
        Flag    117-117   Char   Blank indicates min temp was taken from the
                                 explicit min temp report and not from the              
                                 'hourly' data.  * indicates min temp was 
                                 derived from the hourly data (i.e., lowest
                                 hourly or synoptic-reported temperature).

        PRCP    119-123   Real   Total precipitation (rain and/or melted
                                 snow) reported during the day in inches
                                 and hundredths; will usually not end 
                                 with the midnight observation--i.e., 
                                 may include latter part of previous day.
                                 .00 indicates no measurable              
                                 precipitation (includes a trace).        
                                 Missing = 99.99
                                 Note:  Many stations do not report '0' on
                                 days with no precipitation--therefore,  
                                 '99.99' will often appear on these days.
                                 Also, for example, a station may only
                                 report a 6-hour amount for the period 
                                 during which rain fell.
                                 See Flag field for source of data.
        Flag    124-124   Char   A = 1 report of 6-hour precipitation 
                                     amount.
                                 B = Summation of 2 reports of 6-hour 
                                     precipitation amount.
                                 C = Summation of 3 reports of 6-hour 
                                     precipitation amount.
                                 D = Summation of 4 reports of 6-hour 
                                     precipitation amount.
                                 E = 1 report of 12-hour precipitation
                                     amount.
                                 F = Summation of 2 reports of 12-hour
                                     precipitation amount.
                                 G = 1 report of 24-hour precipitation
                                     amount.
                                 H = Station reported '0' as the amount
                                     for the day (eg, from 6-hour reports),
                                     but also reported at least one
                                     occurrence of precipitation in hourly
                                     observations--this could indicate a
                                     trace occurred, but should be considered
                                     as incomplete data for the day.
                                 I = Station did not report any precip data
                                     for the day and did not report any
                                     occurrences of precipitation in its hourly
                                     observations--it's still possible that
                                     precip occurred but was not reported.

        SNDP    126-130   Real   Snow depth in inches to tenths--last     
                                 report for the day if reported more than
                                 once.  Missing = 999.9
                                 Note:  Most stations do not report '0' on
                                 days with no snow on the ground--therefore,
                                 '999.9' will often appear on these days.

        FRSHTT  133-138   Int.   Indicators (1 = yes, 0 = no/not          
                                 reported) for the occurrence during the 
                                 day of:
                                 Fog ('F' - 1st digit).
                                 Rain or Drizzle ('R' - 2nd digit).
                                 Snow or Ice Pellets ('S' - 3rd digit).
                                 Hail ('H' - 4th digit).
                                 Thunder ('T' - 5th digit).
                                 Tornado or Funnel Cloud ('T' - 6th       
                                 digit).

    */
    #endregion


    public class NOAAGSODCrawler : Machinata.Module.Crawler.WebCrawler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string SERVER_URL = "ftp://ftp.ncdc.noaa.gov";
        public const string STATIONS_URL = "/pub/data/noaa/isd-history.txt";
        public const string MEASUREMENTS_URL = "/pub/data/gsod/{year}/{station-id}-99999-{year}.op.gz";

        
        

        public NOAAGSODCrawler() : base(SERVER_URL) {
            SetCrawlerProfile(Module.Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
        }
        

        public JObject GetLocationInfoBySearching(string location) {
            var stations = GetAllStations();
            foreach(JObject station in stations) {
                if(station["name"].ToString().ToLower().StartsWith(location.ToLower())) {
                    return station;
                }
            }
            return null;
        }
        
        public static DateTime ConvertStringToDateTime(string date) {
            return DateTime.ParseExact(date, "yyyyMMdd", null);
        }
        
        private string _lineColumn(string line, int start, int end) {
            var ret = line.Substring(start-1,end-start).Trim();
            if (ret == "") return null;
            return ret;
        }

        public JArray GetAllStations() {
            var ret = new JArray();
            var data = GetURL(STATIONS_URL);
            var lines = data.Split('\n');
            foreach(var line in lines) {
                try {
                    // USAF   WBAN  STATION NAME                  CTRY ST CALL  LAT     LON      ELEV(M) BEGIN    END
                    // 010010 99999 JAN MAYEN(NOR-NAVY)           NO      ENJA  +70.933 -008.667 +0009.0 19310101 20180301
                    //var cleanedLine = new Regex("[ ]{2,}").Replace(line, " ");
                    if (line.Length != 99) continue;
                    JObject station = new JObject();
                    station["id"] = _lineColumn(line, 1, 7);
                    station["usaf"] = _lineColumn(line, 1, 7);
                    station["wban"] = _lineColumn(line, 8, 13);
                    station["name"] = _lineColumn(line, 14, 43);
                    station["country"] = _lineColumn(line, 44, 51);
                    station["call"] = _lineColumn(line, 52, 57);
                    station["lat"] = _lineColumn(line, 58, 65);
                    station["lon"] = _lineColumn(line, 66, 74);
                    station["elevation"] = _lineColumn(line, 75, 82);
                    station["begin"] = _lineColumn(line, 83, 91);
                    station["end"] = _lineColumn(line, 92, 100);
                    ret.Add(station);
                } catch (Exception e) {
                    throw new Exception("Could not parse station data: " + line, e);
                }
            }
            return ret;
        }
        
        

        public JArray GetStationMeasuredData(string stationID, int year) {
            var url = MEASUREMENTS_URL.Replace("{station-id}", stationID).Replace("{year}", year.ToString());
            var data = this.GetGZ(url);
            var ret = new JArray();
            var lines = data.Split('\n');
            foreach (var line in lines) {
                if (line.Length != 138) continue;
                if (line.StartsWith("STN---")) continue;
                JObject dp = new JObject();
                //STN--- WBAN   YEARMODA    TEMP       DEWP      SLP        STP       VISIB      WDSP     MXSPD   GUST    MAX     MIN   PRCP   SNDP   FRSHTT
                //010260 99999  20080101    29.0 24    15.1 24  1038.5 24  1023.7 24   46.6  8   12.1 24   15.5  999.9    30.7    22.1   0.00G 999.9  000000
                try { 
                    dp["stn"] = _lineColumn(line,1,7);
                    dp["wban"] = _lineColumn(line,8,14);
                    dp["date"] = _lineColumn(line,15,26);
                    dp["temp_f"] = _lineColumn(line,24,31);
                    if (dp["temp_f"].ToString() == "9999.9") dp["temp_f"] = null;
                    dp["temp_f_observations"] = _lineColumn(line,32,34);
                    dp["dewpoint_f"] = _lineColumn(line,38,42);
                    if (dp["dewpoint_f"].ToString() == "9999.9") dp["dewpoint_f"] = null;
                    dp["dewpoint_f_observations"] = _lineColumn(line,43,45);
                    dp["sealevel_pressure_mbars"] = _lineColumn(line,46,53);
                    if (dp["sealevel_pressure_mbars"].ToString() == "9999.9") dp["sealevel_pressure_mbars"] = null;
                    dp["sealevel_pressure_mbars_observations"] = _lineColumn(line,54,56);
                    dp["station_pressure_mbars"] = _lineColumn(line,57,64);
                    if (dp["station_pressure_mbars"].ToString() == "9999.9") dp["station_pressure_mbars"] = null;
                    dp["station_pressure_mbars_observations"] = _lineColumn(line,65,67);
                    dp["visibility_miles"] = _lineColumn(line,68,74);
                    if (dp["visibility_miles"].ToString() == "999.9") dp["visibility_miles"] = null;
                    dp["visib_miles_observations"] = _lineColumn(line,75,77);
                    dp["wind_speed_kts"] = _lineColumn(line,78,84);
                    if (dp["wind_speed_kts"].ToString() == "999.9") dp["wind_speed_kts"] = null;
                    dp["wind_speed_kts_observations"] = _lineColumn(line,85,87);
                    dp["wind_max_kts"] = _lineColumn(line,88,94);
                    if (dp["wind_max_kts"].ToString() == "999.9") dp["wind_max_kts"] = null;
                    dp["wind_gust_kts"] = _lineColumn(line,95,101);
                    if (dp["wind_gust_kts"].ToString() == "999.9") dp["wind_gust_kts"] = null;
                    dp["temp_max_f"] = _lineColumn(line,102,109);
                    if (dp["temp_max_f"].ToString() == "9999.9") dp["temp_max_f"] = null;
                    dp["temp_min_f"] = _lineColumn(line,111,117);
                    if (dp["temp_min_f"].ToString() == "9999.9") dp["temp_min_f"] = null;
                    dp["precipitation_inches"] = _lineColumn(line,119,124);
                    if (dp["precipitation_inches"].ToString() == "99.9") dp["precipitation_inches"] = null;
                    dp["snowdepth_inches"] = _lineColumn(line,126,131);
                    if (dp["snowdepth_inches"].ToString() == "999.9") dp["snowdepth_inches"] = null;
                    var frshtt = _lineColumn(line,133,139);
                    dp["frshtt"] = frshtt;
                    var frshtt_desc = new List<string>();
                    if (frshtt[0] == '1') { dp["fog"] = "true"; frshtt_desc.Add("fog"); }
                    if (frshtt[1] == '1') { dp["rain"] = "true"; frshtt_desc.Add("rain"); }
                    if (frshtt[2] == '1') { dp["snow"] = "true"; frshtt_desc.Add("snow"); }
                    if (frshtt[3] == '1') { dp["hail"] = "true"; frshtt_desc.Add("hail"); }
                    if (frshtt[4] == '1') { dp["thunder"] = "true"; frshtt_desc.Add("thunder"); }
                    if (frshtt[5] == '1') { dp["tornado"] = "true"; frshtt_desc.Add("tornado"); }
                    dp["frshtt_desc"] = string.Join(",", frshtt_desc);
                } catch (Exception e) {
                    throw new Exception("Could not parse weater data: " + line, e);
                }

                // Conversions
                var errorNote = "";
                try {
                    if (dp["temp_f"] != null && dp["temp_f"].Type != JTokenType.Null) {
                        errorNote = "temp_f";
                        dp["temp_f"] = double.Parse(dp["temp_f"].ToString());
                        dp["temp_c"] = _fToC((double)dp["temp_f"]);
                    }
                    if (dp["temp_max_f"] != null && dp["temp_max_f"].Type != JTokenType.Null) {
                        errorNote = "temp_max_f";
                        dp["temp_max_f"] = double.Parse(dp["temp_max_f"].ToString());
                        dp["temp_max_c"] = _fToC((double)dp["temp_max_f"]);
                    }
                    if (dp["temp_min_f"] != null && dp["temp_min_f"].Type != JTokenType.Null) {
                        errorNote = "temp_min_f";
                        dp["temp_min_f"] = double.Parse(dp["temp_min_f"].ToString());
                        dp["temp_min_c"] = _fToC((double)dp["temp_min_f"]);
                    }
                    if (dp["precipitation_inches"] != null && dp["precipitation_inches"].Type != JTokenType.Null) {
                        errorNote = "precipitation_inches";
                        dp["precipitation_inches"] = double.Parse(dp["precipitation_inches"].ToString());
                        dp["precipitation_mm"] = _inToMM((double)dp["precipitation_inches"]);
                    }
                    if (dp["snowdepth_inches"] != null && dp["snowdepth_inches"].Type != JTokenType.Null) {
                        errorNote = "snowdepth_inches";
                        dp["snowdepth_inches"] = double.Parse(dp["snowdepth_inches"].ToString());
                        dp["snowdepth_mm"] = _inToMM((double)dp["snowdepth_inches"]);
                    }
                    if (dp["wind_speed_kts"] != null && dp["wind_speed_kts"].Type != JTokenType.Null) {
                        errorNote = "wind_speed_kts";
                        dp["wind_speed_kts"] = double.Parse(dp["wind_speed_kts"].ToString());
                    }
                    if (dp["wind_max_kts"] != null && dp["wind_max_kts"].Type != JTokenType.Null) {
                        errorNote = "wind_max_kts";
                        dp["wind_max_kts"] = double.Parse(dp["wind_max_kts"].ToString());
                    }
                    if (dp["wind_gust_kts"] != null && dp["wind_gust_kts"].Type != JTokenType.Null) {
                        errorNote = "wind_gust_kts";
                        dp["wind_gust_kts"] = double.Parse(dp["wind_gust_kts"].ToString());
                    }
                } catch (Exception e) {
                    throw new Exception($"Could not do conversion {errorNote} for weather data: " + line, e);
                }

                ret.Add(dp);
            }
            return ret;
        }
        
        private static double _fToC(double fahrenheit) {
            return (fahrenheit - 32) * 5 / 9;
        }
        
        private static double _inToMM(double inches) {
            return inches * 25.4;
        }
    }
}