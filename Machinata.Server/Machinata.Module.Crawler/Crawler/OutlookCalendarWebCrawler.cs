using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Crawler.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler {

    public class OutlookCalendarWebCrawler : WebCrawler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        #endregion
        
        
        #region Example JSON Data
        /*
            
{
   "Header":{
      "ServerVersionInfo":{
         "MajorVersion":15,
         "MinorVersion":1,
         "MajorBuildNumber":1591,
         "MinorBuildNumber":10,
         "Version":"V2017_08_18"
      }
   },
   "Body":{
      "ResponseMessages":{
         "Items":[
            {
               "__type":"FindItemResponseMessage:#Exchange",
               "ResponseCode":"NoError",
               "ResponseClass":"Success",
               "HighlightTerms":null,
               "RootFolder":{
                  "IncludesLastItemInRange":true,
                  "TotalItemsInView":49,
                  "Groups":null,
                  "Items":[
                     {
                        "__type":"CalendarItem:#Exchange",
                        "ItemId":{
                           "ChangeKey":"DwAAABYAAADa9jFnXSk4SoMXKTS5jrtGAAKps193",
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NABGAAAAAACi\/1NfKNdDRZBNXTtOg7czBwBhptN7TZGdRYWuyFrBAlvWADhCmwiGAADa9jFnXSk4SoMXKTS5jrtGAAKpqxVTAAA="
                        },
                        "ParentFolderId":{
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NAAuAAAAAACi\/1NfKNdDRZBNXTtOg7czAQBhptN7TZGdRYWuyFrBAlvWADhCmwiGAAA=",
                           "ChangeKey":"AQAAAA=="
                        },
                        "Subject":"Busy",
                        "Sensitivity":"Normal",
                        "Start":"2019-01-28T13:00:00+01:00",
                        "End":"2019-01-28T13:30:00+01:00",
                        "IsAllDayEvent":null,
                        "FreeBusyType":"Busy",
                        "CalendarItemType":"Single",
                        "Location":{
                           "DisplayName":"",
                           "PostalAddress":{
                              "Street":null,
                              "City":null,
                              "State":null,
                              "Country":null,
                              "PostalCode":null,
                              "PostOfficeBox":null,
                              "Type":"Home",
                              "LocationSource":"None"
                           },
                           "IdType":"Unknown",
                           "LocationType":"Unknown"
                        }
                     },
                     {
                        "__type":"CalendarItem:#Exchange",
                        "ItemId":{
                           "ChangeKey":"DwAAABYAAADa9jFnXSk4SoMXKTS5jrtGAAKps1+m",
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NABGAAAAAACi\/1NfKNdDRZBNXTtOg7czBwBhptN7TZGdRYWuyFrBAlvWADhCmwiGAADa9jFnXSk4SoMXKTS5jrtGAAKpqxVaAAA="
                        },
                        "ParentFolderId":{
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NAAuAAAAAACi\/1NfKNdDRZBNXTtOg7czAQBhptN7TZGdRYWuyFrBAlvWADhCmwiGAAA=",
                           "ChangeKey":"AQAAAA=="
                        },
                        "Subject":"Busy",
                        "Sensitivity":"Normal",
                        "Start":"2019-01-28T13:30:00+01:00",
                        "End":"2019-01-28T14:00:00+01:00",
                        "IsAllDayEvent":null,
                        "FreeBusyType":"Busy",
                        "CalendarItemType":"Single",
                        "Location":{
                           "DisplayName":"",
                           "PostalAddress":{
                              "Street":null,
                              "City":null,
                              "State":null,
                              "Country":null,
                              "PostalCode":null,
                              "PostOfficeBox":null,
                              "Type":"Home",
                              "LocationSource":"None"
                           },
                           "IdType":"Unknown",
                           "LocationType":"Unknown"
                        }
                     },
                     ********** SNIP *********
                     {
                        "__type":"CalendarItem:#Exchange",
                        "ItemId":{
                           "ChangeKey":"DwAAABYAAADa9jFnXSk4SoMXKTS5jrtGAAJmDxZU",
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NAFRAAgI1p3Y2RSAAEYAAAAAov9TXyjXQ0WQTV07ToO3MwcAYabTe02RnUWFrshawQJb1gA4QpsIhgAAWcDIPGQRq0q\/Yg2BVz55zgAAWyauPgAAEA=="
                        },
                        "ParentFolderId":{
                           "Id":"AAMkADNiZjNhNTljLTlhNTEtNGRjZi1hZDMxLWJmNjBmOTMzNTM1NAAuAAAAAACi\/1NfKNdDRZBNXTtOg7czAQBhptN7TZGdRYWuyFrBAlvWADhCmwiGAAA=",
                           "ChangeKey":"AQAAAA=="
                        },
                        "Subject":"Busy",
                        "Sensitivity":"Normal",
                        "Start":"2019-03-01T10:00:00+01:00",
                        "End":"2019-03-01T10:30:00+01:00",
                        "IsAllDayEvent":null,
                        "FreeBusyType":"Busy",
                        "CalendarItemType":"Occurrence",
                        "Location":{
                           "DisplayName":"",
                           "PostalAddress":{
                              "Street":null,
                              "City":null,
                              "State":null,
                              "Country":null,
                              "PostalCode":null,
                              "PostOfficeBox":null,
                              "Type":"Home",
                              "LocationSource":"None"
                           },
                           "IdType":"Unknown",
                           "LocationType":"Unknown"
                        }
                     }
                  ]
               }
            }
         ]
      }
   }
}

        */
        #endregion
            

        public OutlookCalendarWebCrawler() : base() {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_CURL);
        }

        public Schedule GetCalendarEntries(string calendarURL, DateTime date) {
            return GetCalendarEntries(
                calendarURL,
                date.Date,
                date.Date.AddDays(1).AddMilliseconds(-1)
            );
        }

        public Schedule GetCalendarEntries(string calendarURL) {
            return GetCalendarEntries(
                calendarURL,
                DateTime.Today,
                DateTime.Today.AddDays(1).AddMilliseconds(-1)
            );
        }

        public Schedule GetCalendarEntries(string calendarURL, DateTime start, DateTime end) {
            var ret = new Schedule();

            // STEP 1
            // First we have to load the HTML page and get the cookies needed for the next request
            // This URL looks like:
            // https://owa.burriag.ch/owa/calendar/3bf3a59c9a514dcfad31bf60f9335354@burri.world/c0d7c4de17b84b5b97d3103353203e1c5239749711070084347/calendar.html
            var cookies = this.GetHTTPCookies(calendarURL);

            // STEP 2
            // We need the page for the calendar configration JSON object in the bottom of the page
            HttpWebRequest pageRequest = this.GetHTTPRequest(calendarURL, "GET");
            var pageBody = this.GetHTTPResponseAsString(pageRequest);

            // Parse out the calendar feed settings
            string userConfigStr = GetStringExtractionFromString(pageBody,"PageDataPayload.owaUserConfig=",";");
            string calandarConfigStr = GetStringExtractionFromString(pageBody,"PageDataPayload.calendarFolders=",";");
            var userConfigJSON = Core.JSON.ParseJsonAsJObject(userConfigStr);
            var calandarConfigJSON = Core.JSON.ParseJsonAsJObject(calandarConfigStr);
            ret.Name = userConfigJSON["SessionSettings"]["UserDisplayName"].ToString();

            // STEP 3
            // Prepare the payload for the service request (which gives us the JSON)
            var SERVICE_REQUEST_PAYLOAD = "{\"__type\":\"FindItemJsonRequest:#Exchange\",\"Header\":{\"__type\":\"JsonRequestHeaders:#Exchange\",\"RequestServerVersion\":\"Exchange2013\",\"TimeZoneContext\":{\"__type\":\"TimeZoneContext:#Exchange\",\"TimeZoneDefinition\":{\"__type\":\"TimeZoneDefinitionType:#Exchange\",\"Id\":\"W. Europe Standard Time\"}}},\"Body\":{\"__type\":\"FindItemRequest:#Exchange\",\"ItemShape\":{\"__type\":\"ItemResponseShape:#Exchange\",\"BaseShape\":\"IdOnly\",\"AdditionalProperties\":[{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"ItemParentId\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Sensitivity\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"AppointmentState\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsCancelled\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"HasAttachments\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"LegacyFreeBusyStatus\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"CalendarItemType\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Start\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"End\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsAllDayEvent\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Organizer\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Subject\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsMeeting\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"UID\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"InstanceKey\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"ItemEffectiveRights\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"JoinOnlineMeetingUrl\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"ConversationId\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"CalendarIsResponseRequested\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Categories\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsRecurring\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsOrganizer\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"EnhancedLocation\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"IsSeriesCancelled\"},{\"__type\":\"PropertyUri:#Exchange\",\"FieldURI\":\"Charm\"}]},\"ParentFolderIds\":[{\"__type\":\"FolderId:#Exchange\",\"Id\":\"{parent-folder-id}\",\"ChangeKey\":\"{parent-folder-change-key}\"}],\"Traversal\":\"Shallow\",\"Paging\":{\"__type\":\"CalendarPageView:#Exchange\",\"StartDate\":\"{start-date}\",\"EndDate\":\"{end-date}\"}}}";
            var servicePayload = SERVICE_REQUEST_PAYLOAD;
            servicePayload = servicePayload.Replace(
                "{parent-folder-id}",
                calandarConfigJSON["CalendarFolders"][0]["FolderId"]["Id"].ToString()
            );
            servicePayload = servicePayload.Replace(
                "{parent-folder-change-key}",
                calandarConfigJSON["CalendarFolders"][0]["FolderId"]["ChangeKey"].ToString()
            );
            // Set the dates
            // Dates are in format '2019-01-27T00:00:00.001' - '2019-03-03T00:00:00.000'
            servicePayload = servicePayload.Replace(
                "{start-date}",
                String.Format("{0:s}", start)
            );
            servicePayload = servicePayload.Replace(
                "{end-date}",
                String.Format("{0:s}", end)
            );

            // STEP 4
            // Create the request with all the needed headers, payload, cookies et cetera...
            // The URL looks like this:
            // https://owa.burriag.ch/owa/calendar/3bf3a59c9a514dcfad31bf60f9335354@burri.world/c0d7c4de17b84b5b97d3103353203e1c5239749711070084347/service.svc?action=FindItem&ID=-3&AC=1
            var findURL = calendarURL.Replace("/calendar.html","/service.svc?action=FindItem&ID=-1&AC=1");
            var serviceRequest = this.GetHTTPRequest(findURL, "POST");
            serviceRequest.CookieContainer = new CookieContainer();
            foreach (var cookie in cookies) {
                serviceRequest.CookieContainer.Add(new Cookie(cookie.Key, cookie.Value,"/","domain"));
            }
            serviceRequest.Headers.Add("Action", "FindItem");
            var serviceRequestData = Encoding.UTF8.GetBytes(servicePayload);
            serviceRequest.ContentType = "application/json; charset=UTF-8";
            serviceRequest.ContentLength = serviceRequestData.Length;
            using (var stream = serviceRequest.GetRequestStream()) {
                stream.Write(serviceRequestData, 0, serviceRequestData.Length);
            }

            // STEP 5
            // Get the response as JSON and parse out the items
            var serviceBodyStr = this.GetHTTPResponseAsString(serviceRequest);
            var serviceBodyJSON = Core.JSON.ParseJsonAsJObject(serviceBodyStr);
            var itemsJSON = serviceBodyJSON["Body"]["ResponseMessages"]["Items"][0]["RootFolder"]["Items"];
            foreach(var itemJSON in itemsJSON) {
                // Properties to use:
                // "Subject":"Busy",
                // "Sensitivity":"Normal",
                // "Start":"2019-01-28T13:00:00+01:00",
                // "End":"2019-01-28T13:30:00+01:00",
                // "IsAllDayEvent":null,
                // "FreeBusyType":"Busy",
                // "CalendarItemType":"Single",
                var item = new ScheduleItem();
                item.Subject = itemJSON["Subject"].ToString();
                item.Sensitivity = itemJSON["Sensitivity"].ToString();
                item.Start = DateTime.Parse(itemJSON["Start"].ToString()).ToUniversalTime();
                item.End = DateTime.Parse(itemJSON["End"].ToString()).ToUniversalTime();
                item.FreeBusyType = itemJSON["FreeBusyType"].ToString();
                ret.Items.Add(item);
            }


            return ret;
        }
    }
}
