using System;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Crawler {
      

    #region Notes
    //Homepage
    //view-source:http://www.meteoswiss.admin.ch/home.html?tab=wind
    //>>> HTML:
    //<div class="overview__local-forecast clearfix"
    //     data-json-url="/product/output/forecast-chart/version__20171121_0902/en/800100.json"
    //     data-measurements-json-url="/product/output/measured-values-v2/homepage/version__20171121_0911/en/SMA.json"
    //     data-webcam-json-url="/product/output/webcam/version__20171121_0916/en/SMA.json"
    //     data-json-config-url="/etc/designs/meteoswiss/ajax/location/"
    //     data-multidays-url="/product/output/forecast-overview-chart/version__20171121_0901/800100.json"
    //     data-all-stations="http://www.meteoswiss.admin.ch/home/weather/measurement-values/measurement-values-at-meteorological-stations.html"
    //     data-all-webcams="http://www.meteoswiss.admin.ch/home/weather/measurement-values/webcam-images.html">
    //</div>
    //Search location:

    //http://www.meteoswiss.admin.ch/etc/designs/meteoswiss/ajax/search/we.json

    //Location
    //http://www.meteoswiss.admin.ch/etc/designs/meteoswiss/ajax/location/635300.json


    //Converts to:
    //http://www.meteoswiss.admin.ch/product/output/measured-values-v2/homepage/version__20171121_0811/en/GES.json

    //http://www.meteoswiss.admin.ch/product/output/forecast-chart/version__20171121_0902/en/635300.json

    //Example JSON:
    //http://www.meteoswiss.admin.ch/product/output/measured-values-v2/homepage/version__20171121_0811/en/GES.json

    //NOTE: each dataset contains about 140 timestamps and values, timestamp is unix epoch divided by 1000 and ranges over a day
    //[
    //  {
    //    "current_time": 1511251885604,
    //    "current_time_string": "09:11",
    //    "new_day": 1511305200000,
    //    "day_string": "Today",
    //    "all_stations": "/meteo-measurement.html",
    //    "name": "Gersau",
    //    "altitude": 521,
    //    "temperature": [

    //	  [
    //        1511165400000,
    //        3.3
    //      ],
    //      [ ...
    #endregion


    public class MeteoSwissCrawler : Machinata.Module.Crawler.WebCrawler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string SERVER_URL = "http://www.meteoswiss.admin.ch";
        public const string HOME_URL = "/home.html";
        public const string SEARCH_URL = "/etc/designs/meteoswiss/ajax/search/{query}.json";
        public const string LOCATION_URL = "/etc/designs/meteoswiss/ajax/location/{location-id}.json";
        public const string FORECAST_URL = "/product/output/forecast-chart/version__{version}/en/{location-id}.json";
        public const string MEASUREMENTS_URL = "/product/output/measured-values/homepage/version__{version}/en/{station-id}.json";

        // From https://en.wikipedia.org/wiki/List_of_cities_in_Switzerland
        public const string ALL_LOCATIONS = @"Aarau 	h 1240-1250[6] 	Aarau 	AG 	21,036 	76,636 	Aarau
Aarberg 	h 1220-1225[7] 	Aarberg 	BE 	4,527 		-
Aarburg 	h ~1330[8] 	Zofingen 	AG 	7,854 	98,535 	Olten–Zofingen
Adliswil 	- 	Horgen 	ZH 	18,742 	1,334,269 	Zurich
Aesch (BL)[note 1] 	- 	Arlesheim 	BL 	10,267 	541,011 	Basel (CH)
Affoltern am Albis[note 2] 	- 	Affoltern 	ZH 	11,900 	1,334,269 	Zurich
Agno[note 2] 	m Roman & Middle Age[9] 	Lugano 	TI 	4,518 	151,037 	Lugano (CH)
Aigle 	m 1231[10] 	Aigle 	VD 	9,961 		-
Allschwil[note 2] 	- 	Arlesheim 	BL 	20,849 	541,011 	Basel (CH)
Altdorf (UR)[note 2] 	m n.d., c. Late Middle Age[11] 	- 	UR 	9,211 		-
Altstätten 	m[12] 	Rheintal 	SG 	11,438 	56,777 	Rheintal (CH)
Amriswil 	- 	Bischofszell 	TG 	13,346 	25,271 	Amriswil–Romanshorn
Appenzell[note 1] 	m[citation needed] 	Appenzell 	AI 	5,825 		-
Arbon 	h[13] first half 13th c. 	Arbon 	TG 	14,340 	53,689 	Arbon–Rorschach
Arlesheim[note 2] 	- 	Arlesheim 	BL 	9,274 	541,011 	Basel (CH)
Arosa[note 2] 	- 	Plessur 	GR 	3,219 		-
Arth[note 1] 	- 	Schwyz 	SZ 	11,735 		-
Ascona 	h[citation needed] 	Locarno 	TI 	5,515 	55,528 	Locarno (CH)
Aubonne 	h[citation needed] 	Aubonne 	VD 	3,281 	409,295 	Lausanne
Avenches 	h[citation needed] 	Avenches 	VD 	4,166 		-
Baar 	- 	- 	ZG 	24,129 	127,095 	Zug
Baden 	h[citation needed] 	Baden 	AG 	19,222 	109,255 	Baden–Brugg
Basel 	h[citation needed] 	- 	BS 	175,940 	541,011 	Basel (CH)
Bassersdorf 	- 	Bülach 	ZH 	11,616 	1,334,269 	Zurich
Bellinzona 	h[citation needed] 	Bellinzona 	TI 	18,347 	52,061 	Bellinzona
Belp 	- 	Bern-Mittelland 	BE 	11,534 	410,894 	Bern
Bern 	h[citation needed] 	Bern-Mittelland 	BE 	133,115 	410,894 	Bern
Beromünster 	m[citation needed] 	Sursee 	LU 	6,517 		-
Biasca 	m[citation needed] 	Riviera 	TI 	6,176 		-
Biel/Bienne 	h[citation needed] 	Biel/Bienne 	BE 	54,456 	104,542 	Biel/Bienne
Binningen 	- 	Arlesheim 	BL 	15,461 	541,011 	Basel (CH)
Birsfelden 	- 	Arlesheim 	BL 	10,531 	541,011 	Basel (CH)
Bischofszell 	h[citation needed] 	Bischofszell 	TG 	5,947 		-
Boudry 	h[citation needed] 	Boudry 	NE 	5,846 	89,441 	Neuchâtel
Bourg-Saint-Pierre 	h[citation needed] 	Entremont 	VS 	182 		-
Bremgarten (AG) 	h[citation needed] 	Bremgarten 	AG 	7,742 	1,334,269 	Zurich
Brig-Glis 	m[citation needed] 	Brig 	VS 	13,158 	47,041 	Brig–Visp
Brugg 	h[citation needed] 	Brugg 	AG 	11,204 	109,255 	Baden–Brugg
Buchs (SG) 	- 	Werdenberg 	SG 	12,531 	27,467 	Buchs (SG) (CH)
Bülach 	h[citation needed] 	Bülach 	ZH 	19,611 	1,334,269 	Zurich
Büren a.A. 	h[citation needed] 	Seeland 	BE 	3,596 		-
Bulle 	h[citation needed] 	Gruyère 	FR 	22,523 	31,703 	Bulle
Burgdorf 	h[citation needed] 	Burgdorf 	BE 	16,295 		-
Bussigny 	- 	Ouest Lausannois 	VD 	8,268 	409,295 	Lausanne
Carouge (GE) 	- 	- 	GE 	21,868 	579,227 	Genève (CH)
Cham 	- 	- 	ZG 	16,216 	127,095 	Zug
Châtel-Saint-Denis 	h[citation needed] 	Veveyse 	FR 	6,528 	84,869 	Vevey–Montreux
Chêne-Bougeries 	- 	- 	GE 	11,489 	579,227 	Genève (CH)
Chiasso 	h[citation needed] 	- 	TI 	8,331 	51,668 	Chiasso–Mendrisio (CH)
Chur 	h[citation needed] 	Plessur 	GR 	34,880 	58,266 	Chur
Conthey 	h[citation needed] 	Conthey 	VS 	8,674 	84,138 	Sion
Coppet 	h[citation needed] 	Nyon 	VD 	3,151 	579,227 	Genève (CH)
Cossonay 	h[citation needed] 	Cossonay 	VD 	3,642 	409,295 	Lausanne
Croglio 	h[citation needed] 	Lugano 	TI 	875 	151,037 	Lugano (CH)
Crissier 	- 	Ouest Lausannois 	VD 	7,869 	409,295 	Lausanne
Cudrefin 	h[citation needed] 	Avenches 	VD 	1,557 		-
Cully 	h[citation needed] 	Lavaux 	VD 	1,700? 	409,295 	Lausanne
Davos* 	- 	Prättigau/Davos 	GR 	11,060 		-
Delémont 	h[citation needed] 	Delémont 	JU 	12,562 	29,527 	Delémont (CH)
Diessenhofen 	h[citation needed] 	Diessenhofen 	TG 	3,767 		-
Dietikon 	- 	Dietikon 	ZH 	27,076 	1,334,269 	Zurich
Dübendorf 	- 	Uster 	ZH 	27,689 	1,334,269 	Zurich
Ebikon 	- 	Lucerne 	LU 	13,313 	226,091 	Lucerne
Échallens 	h[citation needed] 	Échallens 	VD 	5,687 	409,295 	Lausanne
Ecublens (VD) 	- 	Ouest Lausannois 	VD 	12,342 	409,295 	Lausanne
Eglisau 	h[citation needed] 	Bülach 	ZH 	5,167 	1,334,269 	Zurich
Einsiedeln 	- 	Einsiedeln 	SZ 	15,361 		-
Elgg 	h[citation needed] 	Winterthur 	ZH 	4,323 		-
Emmen 	- 	Hochdorf 	LU 	30,228 	226,091 	Lucerne
Erlach 	h[citation needed] 	Erlach 	BE 	1,450 		-
Estavayer-le-Lac 	h[citation needed] 	Broye 	FR 	6,291 		-
Flawil 	- 	Frauenfeld 	SG 	10,505 	165,860 	St. Gallen
Frauenfeld 	h[citation needed] 	Frauenfeld 	TG 	25,200 	24,864 	Frauenfeld
Freienbach 	- 	Höfe 	SZ 	16,196 	1,334,269 	Zurich
Fribourg 	h[citation needed] 	Sarine 	FR 	38,829 	105,406 	Fribourg
Geneva 	h[citation needed] 	- 	GE 	198,979 	579,227 	Genève (CH)
Gland 	- 	Nyon 	VD 	12,997 	579,227 	Genève (CH)
Glarus 	m[citation needed] 	- 	GL 	12,515 	30,367 	Glarus
Glarus Nord* 	- 	- 	GL 	18,057 	30,367 	Glarus
Gordola 	h[citation needed] 	Locarno 	TI 	4,568 	55,528 	Locarno (CH)
Gossau (SG) 	- 	St. Gallen 	SG 	18,055 	165,860 	St. Gallen
Grandcour 	h[citation needed] 	Payerne 	VD 	889 		-
Grandson 	h[citation needed] 	Grandson 	VD 	3,316 	41,079 	Yverdon-les-Bains
Greifensee 	h[citation needed] 	Uster 	ZH 	5,381 	1,334,269 	Zurich
Grenchen 	- 	Lebern 	SO 	16,985 	26,570 	Grenchen
Grüningen 	- 	Hinwil 	ZH 	3,401 	1,334,269 	Zurich
Gruyères 	- 	Gruyère 	FR 	2,194 		-
Herisau 	- 	- 	AR 	15,730 	165,860 	St. Gallen
Hermance 	h[citation needed] 	- 	GE 	1,014 	579,227 	Genève (CH)
Hinwil 	- 	Hinwil 	ZH 	11,095 	1,334,269 	Zurich
Horgen 	- 	Horgen 	ZH 	20,291 	1,334,269 	Zurich
Horw 	- 	Lucerne 	LU 	13,884 	226,091 	Lucerne
Huttwil 	h[citation needed] 	Trachselwald 	BE 	4,835 		-
Ilanz 	h[citation needed] 	Surselva 	GR 	4,736 		-
Illnau-Effretikon 	- 	Pfäffikon 	ZH 	16,298 	1,334,269 	Zurich
Interlaken 	- 	Interlaken-Oberhasli 	BE 	5,673 	23,943 	Interlaken
Ittigen 	- 	Bern-Mittelland 	BE 	11,388 	410,894 	Bern
Kaiserstuhl (AG) 	h[citation needed] 	Zurzach 	AG 	414 	1,334,269 	Zurich
Klingnau 	h[citation needed] 	Zurzach 	AG 	3,340 		-
Kloten 	h[citation needed] 	Bülach 	ZH 	19,086 	1,334,269 	Zurich
Köniz[note 2] 	- 	Bern-Mittelland 	BE 	40,938 	410,894 	Bern
Kreuzlingen 	- 	Kreuzlingen 	TG 	21,560 	23,713 	Kreuzlingen (CH)
Kriens 	- 	Lucerne 	LU 	27,110 	226,091 	Lucerne
Küsnacht (ZH) 	- 	Meilen 	ZH 	14,192 	1,334,269 	Zurich
La Chaux-de-Fonds 	h[citation needed] 	La Chaux-de-Fonds 	NE 	38,965 	53,002 	La Chaux-de-Fonds–Le Locle (CH)
La Neuveville 	h[citation needed] 	La Neuveville 	BE 	3,693 		-
La Sarraz 	h[citation needed] 	Cossonay 	VD 	2,586 		-
La Tour-de-Peilz 	h[citation needed] 	Vevey 	VD 	11,652 	84,869 	Vevey–Montreux
La Tour-de-Trême 	h[citation needed] 	Gruyère 	FR 	22,523 	31,703 	Bulle
Lachen (SZ) 	m[citation needed] 	March 	SZ 	8,599 	29,124 	Lachen
Lancy 	- 	- 	GE 	31,661 	579,227 	Genève (CH)
Langenthal 	m[citation needed] 	Oberaargau 	BE 	15,501 		-
Laufen (BL) 	h[citation needed] 	Laufen 	BL 	5,568 	541,011 	Basel (CH)
Laufenburg 	h[citation needed] 	Laufenburg 	AG 	3,569 		-
Laupen 	h[citation needed] 	Bern-Mittelland 	BE 	3,125 	410,894 	Bern
Lausanne 	h[citation needed] 	Lausanne 	VD 	137,810 	409,295 	Lausanne
Le Grand-Saconnex 	- 	- 	GE 	12,075 	579,227 	Genève (CH)
Le Landeron 	h[citation needed] 	Neuchâtel 	NE 	4,559 		-
Le Locle 	- 	Le Locle 	NE 	10,433 	53,002 	La Chaux-de-Fonds–Le Locle (CH)
Lenzburg 	h[citation needed] 	Lenzburg 	AG 	9,503 	22,188 	Lenzburg
Les Clées 	- 	Orbe 	VD 	176 		-
Leuk 	m[citation needed] 	Leuk 	VS 	3,947 		-
Lichtensteig 	h[citation needed] 	Toggenburg 	SG 	1,907 		-
Liestal 	h[citation needed] 	Liestal 	BL 	14,234 	541,011 	Basel (CH)
Locarno 	h[citation needed] 	Locarno 	TI 	16,122 	55,528 	Locarno (CH)
Losone 	h[citation needed] 	Locarno 	TI 	6,612 	55,528 	Locarno (CH)
Lugano 	h[citation needed] 	Lugano 	TI 	63,932 	151,037 	Lugano (CH)
Lutry 	h[citation needed] 	Lavaux 	VD 	9,891 	409,295 	Lausanne
Lucerne 	h[citation needed] 	Lucerne 	LU 	81,592 	226,091 	Lucerne
Lyss 	- 	Seeland 	BE 	14,706 		-
Männedorf 	- 	Meilen 	ZH 	10,830 	1,334,269 	Zurich
Maienfeld 	h[citation needed] 	Landquart 	GR 	2,835 		-
Martigny 	h[citation needed] 	Martigny 	VS 	17,998 	20,818 	Martigny
Meilen 	- 	Meilen 	ZH 	13,762 	1,334,269 	Zurich
Mellingen 	h[citation needed] 	Baden 	AG 	5,369 	109,255 	Baden–Brugg
Mendrisio 	- 	Mendrisio 	TI 	15,110 	51,668 	Chiasso–Mendrisio (CH)
Meyrin 	- 	- 	GE 	23,044 	579,227 	Genève (CH)
Möhlin 	- 	Rheinfelden 	AG 	10,913 	541,011 	Basel (CH)
Monthey 	- 	Monthey 	VS 	17,573 	32,442 	Monthey
Montreux 	- 	Vevey 	VD 	26,629 	84,869 	Vevey–Montreux
Morcote 	h[citation needed] 	Lugano 	TI 	755 		-
Morges 	h[citation needed] 	Morges 	VD 	15,889 	409,295 	Lausanne
Moudon 	h[citation needed] 	Moudon 	VD 	6,114 		-
Moutier 	m[citation needed] 	Jura bernois 	BE 	7,586 		-
Münchenbuchsee 	- 	Bern-Mittelland 	BE 	10,122 	410,894 	Bern
Münchenstein 	-h[citation needed] 	Arlesheim 	BL 	12,160 	541,011 	Basel (CH)
Münsingen 	- 	Bern-Mittelland 	BE 	11,998 		-
Muri bei Bern 	- 	Bern 	BE 	13,037 	410,894 	Bern
Murten 	h[citation needed] 	See 	FR 	8,168 		-
Muttenz 	- 	Arlesheim 	BL 	17,737 	541,011 	Basel (CH)
Neuchâtel 	h[citation needed] 	Neuchâtel 	NE 	33,772 	89,441 	Neuchâtel
Neuhausen am Rheinfall 	- 	- 	SH 	10,407 	69,517 	Schaffhausen (CH)
Neunkirch 	h[citation needed] 	- 	SH 	2,131 		-
Nidau 	h[citation needed] 	Biel/Bienne 	BE 	6,891 	104,542 	Biel/Bienne
Nyon 	h[citation needed] 	Nyon 	VD 	20,272 	579,227 	Genève (CH)
Oberwil (BL) 	- 	Arlesheim 	BL 	11,244 	541,011 	Basel (CH)
Oftringen 	- 	Zofingen 	AG 	13,511 	98,535 	Olten–Zofingen
Olten 	- 	Olten 	SO 	18,166 	98,535 	Olten–Zofingen
Onex 	- 	- 	GE 	18,825 	579,227 	Genève (CH)
Opfikon 	- 	Bülach 	ZH 	19,599 	1,334,269 	Zurich
Orbe 	h[citation needed] 	Orbe 	VD 	6,903 		-
Orsières 	m[citation needed] 	Entremont 	VS 	3,220 		-
Ostermundigen 	- 	Bern 	BE 	17,127 	410,894 	Bern
Payerne 	h[citation needed] 	Payerne 	VD 	9,486 		-
Peseux 	- 	Boudry 	NE 	5,926 	89,441 	Neuchâtel
Pfäffikon 	- 	Uster 	ZH 	11,774 	1,334,269 	Zurich
Plan-les-Ouates 	- 	- 	GE 	10,571 	579,227 	Genève (CH)
Porrentruy 	h[citation needed] 	Porrentruy 	JU 	6,876 		-
Pratteln 	- 	Liestal 	BL 	16,315 	541,011 	Basel (CH)
Prilly 	- 	Lausanne 	VD 	12,063 	409,295 	Lausanne
Pully 	- 	Lausanne 	VD 	17,972 	409,295 	Lausanne
Rapperswil-Jona 	h[citation needed] 	See-Gaster 	SG 	26,962 	46,343 	Rapperswil-Jona–Rüti
Regensberg 	h[citation needed] 	Dielsdorf 	ZH 	472 	1,334,269 	Zurich
Regensdorf 	- 	Dielsdorf 	ZH 	18,191 	1,334,269 	Zurich
Reinach (BL) 	- 	Arlesheim 	BL 	19,106 	541,011 	Basel (CH)
Renens (VD) 	- 	Lausanne 	VD 	20,523 	409,295 	Lausanne
Rheinau 	h[citation needed] 	Andelfingen 	ZH 	1,302 		-
Rheineck 	h[citation needed] 	Rheintal 	SG 	3,452 	53,689 	Arbon–Rorschach
Rheinfelden 	h[citation needed] 	Rheinfelden 	AG 	13,337 	541,011 	Basel (CH)
Richterswil 	- 	Horgen 	ZH 	13,352 	1,334,269 	Zurich
Riehen 	- 	- 	BS 	21,123 	541,011 	Basel (CH)
Risch 	- 	- 	ZG 	10,355 	127,095 	Zug
Riva San Vitale 	h[citation needed] 	Mendrisio 	TI 	2,627 	51,668 	Chiasso–Mendrisio (CH)
Rolle 	h[citation needed] 	Rolle 	VD 	6,168 		-
Romainmôtier 	h[citation needed] 	Orbe 	VD 	547 		-
Romanshorn 	h[citation needed] 	Arbon 	TG 	10,803 	25,271 	Amriswil–Romanshorn
Romont (FR) 	h[citation needed] 	Glane 	FR 	5,204 		-
Rorschach 	m[citation needed] 	Rorschach 	SG 	9,408 	53,689 	Arbon–Rorschach
Rue 	h[citation needed] 	Glane 	FR 	1,500 		-
Rüti (ZH) 	- 	Hinwil 	ZH 	12,086 	46,343 	Rapperswil-Jona–Rüti
Saillon 	h[citation needed] 	Martigny 	VS 	2,541 		-
Saint-Maurice 	- 	Saint-Maurice 	VS 	4,566 		-
Saint-Prex 	h[citation needed] 	Morges 	VD 	5,687 	409,295 	Lausanne
Saint-Ursanne 	h[citation needed] 	Porrentruy 	JU 	700? 		-
Sargans 	h[citation needed] 	Sarganserland 	SG 	6,063 		-
Sarnen 	m[citation needed] 	- 	OW 	10,233 		-
Schaffhausen 	h[citation needed] 	- 	SH 	36,148 	69,517 	Schaffhausen (CH)
Schlieren 	- 	Dietikon 	ZH 	18,749 	1,334,269 	Zurich
Schwyz 	m[citation needed] 	Schwyz 	SZ 	14,885 		-
Sembrancher 	- 	Entremont 	VS 	1,011 		-
Sempach 	h[citation needed] 	Sursee 	LU 	4,106 		-
Sierre 	- 	Sierre 	VS 	16,817 	26,132 	Sierre
Sion 	h[citation needed] 	Sion 	VS 	33,999 	84,138 	Sion
Solothurn 	h[citation needed] 	Solothurn 	SO 	16,697 	78,046 	Solothurn
Spiez 	-h[citation needed] 	Niedersimmental 	BE 	12,713 		-
Spreitenbach 	- 	Baden 	AG 	11,531 	1,334,269 	Zurich
Splügen 	m[citation needed] 	Hinterrhein 	GR 	377 		-
St. Gallen 	h[citation needed] 	St. Gallen 	SG 	75,481 	165,860 	St. Gallen
St. Moritz 	- 	Maloja 	GR 	5,084 		-
Stäfa 	- 	Meilen 	ZH 	14,291 	1,334,269 	Zurich
Stans 	- 	m[citation needed] 	NW 	8,333 		-
Steckborn 	h[citation needed] 	Steckborn 	TG 	3,754 		-
Steffisburg 	- 	Thun 	BE 	15,783 		-
Steinhausen 	- 	- 	ZG 	9,735 	127,095 	Zug
Suhr 	- 	Aarau 	AG 	9,990 	76,636 	Aarau
Stein am Rhein 	h[citation needed] 	- 	SH 	3,421 		-
Sursee 	h[citation needed] 	Sursee 	LU 	9,621 		-
Thalwil 	- 	Horgen 	ZH 	17,789 	1,334,269 	Zurich
Thônex 	- 	- 	GE 	13,906 	579,227 	Genève (CH)
Thun 	h[citation needed] 	Thun 	BE 	43,568 	80,264 	Thun
Thusis 	h[citation needed] 	Hinterrhein 	GR 	3,044 		-
Unterseen 	h 1280[14] 	Interlaken 	BE 	5,757 	23,943 	Interlaken
Urdorf 	- 	Dietikon 	ZH 	9,657 	1,334,269 	Zurich
Uster 	- 	Uster 	ZH 	34,319 	1,334,269 	Zurich
Uznach 	h[citation needed] 	See-Gaster 	SG 	6,340 		-
Uzwil 	- 	Wil 	SG 	12,816 	73,299 	Wil (SG)
Val-de-Travers 	- 	Val-de-Travers 	NE 	10,950 		-
Valangin 	h[citation needed] 	Val-de-Ruz 	NE 	506 		-
Vernier 	- 	- 	GE 	34,983 	579,227 	Genève (CH)
Versoix 	- 	- 	GE 	13,247 	579,227 	Genève (CH)
Vevey 	h[citation needed] 	Vevey 	VD 	19,780 	84,869 	Vevey–Montreux
Veyrier 	- 	- 	GE 	11,535 	579,227 	Genève (CH)
Villeneuve 	h[citation needed] 	Aigle 	VD 	5,573 	84,869 	Vevey–Montreux
Villars-sur-Glâne 	- 	Sarine 	FR 	12,128 	105,406 	Fribourg
Visp 	m[citation needed] 	Visp 	VS 	7,726 	47,041 	Brig–Visp
Volketswil 	- 	Uster 	ZH 	18,582 	1,334,269 	Zurich
Wädenswil 	- 	Horgen 	ZH 	21,797 	1,334,269 	Zurich
Waldenburg 	h[citation needed] 	Waldenburg 	BL 	1,184 		-
Walenstadt 	h[citation needed] 	Sarganserland 	SG 	5,590 		-
Wallisellen 	- 	Bülach 	ZH 	15,934 		-
Wangen an der Aare 	h[citation needed] 	Wangen 	BE 	2,305 		-
Werdenberg 	h[citation needed] 	Werdenberg 	SG 	60 	27,467 	Buchs (SG) (CH)
Weinfelden 	- 	Weinfelden 	TG 	11,288 		-
Wettingen 	- 	Baden 	AG 	20,535 	109,255 	Baden–Brugg
Wetzikon (ZH) 	- 	Hinwil 	ZH 	24,640 	1,334,269 	Zurich
Wiedlisbach 	h[citation needed] 	Wangen 	BE 	2,325 		-
Wil (SG) 	h[citation needed] 	Wil 	SG 	23,751 	73,299 	Wil (SG)
Willisau 	h[citation needed] 	Willisau 	LU 	7,777 		-
Winterthur 	h[citation needed] 	Winterthur 	ZH 	109,775 	138,252 	Winterthur
Wohlen (AG) 	- 	Bremgarten 	AG 	15,824 	22,621 	Wohlen (AG)
Yverdon-les-Bains 	h[citation needed] 	Yverdon 	VD 	29,977 	41,079 	Yverdon-les-Bains
Zermatt 	- 	Visp 	VS 	5,714 		-
Zofingen 	h[citation needed] 	Zofingen 	AG 	11,560 	98,535 	Olten–Zofingen
Zollikofen 	- 	Bern-Mittelland 	BE 	10,235 	410,894 	Bern
Zollikon 	- 	Meilen 	ZH 	12,791 	1,334,269 	Zurich
Zug 	h[citation needed] 	- 	ZG 	29,804 	127,095 	Zug
Zürich 	h[citation needed] 	Zurich 	ZH 	402,762 	1,334,269 	Zurich
Bad Zurzach 	m[citation needed] 	Zurzach 	AG 	4,165 		-";

        // From http://www.meteoswiss.admin.ch/home/measurement-and-forecasting-systems/land-based-stations/automatisches-messnetz.html?region=Table
        public const string ALL_STATIONS = @"Aadorf / Tänikon	539 m.a.s.l.	710515 / 259821	04.10.2006	Aadorf / Tänikon TAE (PDF, 750 kB, )
Acquarossa / Comprovasco	575 m.a.s.l.	714998 / 146440	28.08.2007	Acquarossa / Comprovasco COM (PDF, 750 kB, )
Adelboden	1327 m.a.s.l.	609350 / 149001	24.08.2009	Adelboden ABO (PDF, 750 kB, )
Affoltern i. E.	755 m.a.s.l.	622940 / 212500	17.12.2012	Affoltern i. E. AIE (PDF, 750 kB, )
Aigle	381 m.a.s.l.	560401 / 130713	01.09.2005	Aigle AIG (PDF, 750 kB, )
Airolo	1139 m.a.s.l.	688910 / 153400	20.12.2013	Airolo AIR (PDF, 750 kB, )
Altdorf	438 m.a.s.l.	690174 / 193558	08.12.2008	Altdorf ALT (PDF, 750 kB, )
Altenrhein	398 m.a.s.l.	760382 / 261387	24.10.2013	Altenrhein ARH (PDF, 750 kB, )
Altstätten, SG	430 m.a.s.l.	759910 / 250202	01.10.2014	Altstätten, SG ALS (PDF, 750 kB, )
Amriswil	420 m.a.s.l.	741796 / 268304	05.07.2016	Amriswil AMW (PDF, 750 kB, )
Andeer	987 m.a.s.l.	752687 / 164036	13.05.2008	Andeer AND (PDF, 750 kB, )
Andermatt	1438 m.a.s.l.	687444 / 165044	19.11.2013	Andermatt ANT (PDF, 750 kB, )
Anzère	1614 m.a.s.l.	597607 / 128205	01.10.2010	Anzère VSANZ (PDF, 750 kB, )
Appenzell	750 m.a.s.l.	748012 / 245040	19.12.2012	Appenzell APP (PDF, 750 kB, )
Arolla	2005 m.a.s.l.	603508 / 95832	27.09.2011	Arolla VSARO (PDF, 750 kB, )
Arosa	1878 m.a.s.l.	771030 / 184826	15.12.2015	Arosa ARO (PDF, 750 kB, )
Attelwil	475 m.a.s.l.	646307 / 235106	10.06.2014	Attelwil AGATT (PDF, 750 kB, )
Bad Ragaz	496 m.a.s.l.	756910 / 209351	06.02.2012	Bad Ragaz RAG (PDF, 750 kB, )
Baltschiedertal	1318 m.a.s.l.	633928 / 132421	01.10.2010	Baltschiedertal VSBAS (PDF, 750 kB, )
Bantiger	942 m.a.s.l.	606850 / 202975	20.01.2011	Bantiger BAN (PDF, 750 kB, )
Barrage Grande Dixence	2162 m.a.s.l.	597255 / 103580	27.09.2011	Barrage Grande Dixence VSGDX (PDF, 750 kB, )
Basel / Binningen	316 m.a.s.l.	610911 / 265601	09.12.2009	Basel / Binningen BAS (PDF, 750 kB, )
Bellelay	923 m.a.s.l.	579603 / 234880	29.10.2015	Bellelay BEY (PDF, 750 kB, )
Bellinzona	224 m.a.s.l.	720913 / 116588	17.05.2016	Bellinzona BLZ (PDF, 750 kB, )
Belp	510 m.a.s.l.	605082 / 195001	07.11.2017	Belp BEP (PDF, 750 kB, )
Benken / Doggen	408 m.a.s.l.	715387 / 227540	19.12.2012	Benken / Doggen DOB (PDF, 750 kB, )
Bergün / Latsch	1408 m.a.s.l.	777273 / 166619	07.10.2015	Bergün / Latsch LAT (PDF, 750 kB, )
Bern / Zollikofen	553 m.a.s.l.	601930 / 204410	03.08.2006	Bern / Zollikofen BER (PDF, 750 kB, )
Bernina / Curtinatsch	2090 m.a.s.l.	795795 / 146486	07.09.2015	Bernina / Curtinatsch BEC (PDF, 750 kB, )
Bex	402 m.a.s.l.	565806 / 121511	19.12.2012	Bex BEX (PDF, 750 kB, )
Beznau	326 m.a.s.l.	659808 / 267694	03.06.2008	Beznau BEZ (PDF, 750 kB, )
Biasca	278 m.a.s.l.	718550 / 132800	18.08.2017	Biasca BIA (PDF, 750 kB, )
Binn	1448 m.a.s.l.	657892 / 135176	17.01.2014	Binn BIN (PDF, 750 kB, )
Bischofszell	470 m.a.s.l.	735325 / 262285	10.03.2010	Bischofszell BIZ (PDF, 750 kB, )
Bivio	1856 m.a.s.l.	771282 / 148120	14.10.2015	Bivio BIV (PDF, 750 kB, )
Bière	684 m.a.s.l.	515889 / 153207	16.12.2009	Bière BIE (PDF, 750 kB, )
Blatten, Lötschental	1538 m.a.s.l.	629564 / 141084	31.05.2013	Blatten, Lötschental BLA (PDF, 750 kB, )
Blinnen	1530 m.a.s.l.	663085 / 145404	02.09.2013	Blinnen VSBLI (PDF, 750 kB, )
Boltigen	820 m.a.s.l.	595831 / 163587	04.12.2012	Boltigen BOL (PDF, 750 kB, )
Bosco/Gurin	1486 m.a.s.l.	680879 / 130027	16.09.2014	Bosco/Gurin BOS (PDF, 750 kB, )
Bourg-St-Pierre	1822 m.a.s.l.	581350 / 86251	02.09.2013	Bourg-St-Pierre VSBSP (PDF, 750 kB, )
Bouveret	374 m.a.s.l.	555264 / 138175	21.08.2012	Bouveret BOU (PDF, 750 kB, )
Braunwald	1299 m.a.s.l.	717942 / 199651	11.02.2014	Braunwald BRW (PDF, 750 kB, )
Bricola	2431 m.a.s.l.	609887 / 99430	27.09.2011	Bricola VSBRI (PDF, 750 kB, )
Brienz	567 m.a.s.l.	647552 / 176803	28.06.2012	Brienz BRZ (PDF, 750 kB, )
Brig	665 m.a.s.l.	640567 / 129069	16.10.2013	Brig BRI (PDF, 750 kB, )
Bristen	784 m.a.s.l.	696223 / 180373	29.10.2015	Bristen BRT (PDF, 750 kB, )
Bruchji	2300 m.a.s.l.	640999 / 136585	12.09.2012	Bruchji VSBRU (PDF, 750 kB, )
Brusio	856 m.a.s.l.	806477 / 127184	04.11.2015	Brusio BRP (PDF, 750 kB, )
Buchs / Aarau	387 m.a.s.l.	648389 / 248365	05.05.2008	Buchs / Aarau BUS (PDF, 750 kB, )
Buffalora	1968 m.a.s.l.	816494 / 170225	18.11.2010	Buffalora BUF (PDF, 750 kB, )
Bullet / La Frétaz	1205 m.a.s.l.	534221 / 188081	24.02.2006	Bullet / La Frétaz FRE (PDF, 750 kB, )
Bözberg	536 m.a.s.l.	652249 / 259223	09.09.2014	Bözberg UBB (PDF, 750 kB, )
Cevio	417 m.a.s.l.	689688 / 130565	01.10.2013	Cevio CEV (PDF, 750 kB, )
Cham	443 m.a.s.l.	677758 / 226878	01.07.2014	Cham CHZ (PDF, 750 kB, )
Champéry	1055 m.a.s.l.	555392 / 112526	01.10.2010	Champéry VSCHY (PDF, 750 kB, )
Chasseral	1599 m.a.s.l.	570847 / 220158	05.12.2006	Chasseral CHA (PDF, 750 kB, )
Chaumont	1136 m.a.s.l.	565060 / 211006	04.09.2013	Chaumont CHM (PDF, 750 kB, )
Choëx	891 m.a.s.l.	563181 / 120189	02.09.2013	Choëx VSCHO (PDF, 750 kB, )
Chur	556 m.a.s.l.	759466 / 193153	22.03.2007	Chur CHU (PDF, 750 kB, )
Château-d'Oex	1029 m.a.s.l.	577040 / 147655	08.02.2012	Château-d'Oex CHD (PDF, 750 kB, )
Cimetta	1661 m.a.s.l.	704433 / 117452	21.10.2009	Cimetta CIM (PDF, 750 kB, )
Clusanfe	1926 m.a.s.l.	557237 / 109533	27.09.2011	Clusanfe VSCLU (PDF, 750 kB, )
Col des Mosses	1412 m.a.s.l.	573818 / 137852	14.10.2015	Col des Mosses CDM (PDF, 750 kB, )
Col du Grand St-Bernard	2472 m.a.s.l.	579200 / 79720	11.11.2005	Col du Grand St-Bernard GSB (PDF, 750 kB, )
Coldrerio	347 m.a.s.l.	721080 / 79235	31.08.2015	Coldrerio COL (PDF, 750 kB, )
Cossonay	590 m.a.s.l.	527563 / 162465	24.09.2014	Cossonay COS (PDF, 750 kB, )
Courtelary	695 m.a.s.l.	573623 / 225597	17.03.2015	Courtelary COY (PDF, 750 kB, )
Couvet	728 m.a.s.l.	540607 / 198345	17.03.2016	Couvet COU (PDF, 750 kB, )
Crap Masegn	2480 m.a.s.l.	732820 / 189380	13.01.2011	Crap Masegn CMA (PDF, 750 kB, )
Cressier	431 m.a.s.l.	571160 / 210800	29.09.2010	Cressier CRM (PDF, 750 kB, )
Davos	1594 m.a.s.l.	783514 / 187458	13.12.2006	Davos DAV (PDF, 750 kB, )
Delémont	439 m.a.s.l.	593269 / 244543	29.08.2012	Delémont DEM (PDF, 750 kB, )
Derborence	1366 m.a.s.l.	584115 / 126364	12.09.2012	Derborence VSDER (PDF, 750 kB, )
Dietikon	384 m.a.s.l.	672890 / 251865	26.08.2016	Dietikon DIT (PDF, 750 kB, )
Disentis	1197 m.a.s.l.	708189 / 173789	13.12.2006	Disentis DIS (PDF, 750 kB, )
Durnand	1214 m.a.s.l.	572572 / 100083	12.09.2012	Durnand VSDUR (PDF, 750 kB, )
Ebnat-Kappel	623 m.a.s.l.	726347 / 237176	02.04.2012	Ebnat-Kappel EBK (PDF, 750 kB, )
Eggishorn	2893 m.a.s.l.	650279 / 141897	26.03.2012	Eggishorn EGH (PDF, 750 kB, )
Egolzwil	521 m.a.s.l.	642913 / 225540	22.09.2010	Egolzwil EGO (PDF, 750 kB, )
Ehrendingen	428 m.a.s.l.	667846 / 262266	09.09.2014	Ehrendingen OED (PDF, 750 kB, )
Einsiedeln	910 m.a.s.l.	699984 / 221069	07.03.2012	Einsiedeln EIN (PDF, 750 kB, )
Elm	958 m.a.s.l.	732265 / 198425	19.04.2011	Elm ELM (PDF, 750 kB, )
Emosson	1923 m.a.s.l.	560664 / 101691	27.09.2011	Emosson VSEMO (PDF, 750 kB, )
Engelberg	1036 m.a.s.l.	674157 / 186097	14.12.2006	Engelberg ENG (PDF, 750 kB, )
Entlebuch	768 m.a.s.l.	647935 / 204165	15.01.2014	Entlebuch ENT (PDF, 750 kB, )
Ergisch	1133 m.a.s.l.	621396 / 126882	02.09.2013	Ergisch VSERG (PDF, 750 kB, )
Eschenz	414 m.a.s.l.	707865 / 278227	17.07.2012	Eschenz ESZ (PDF, 750 kB, )
Evionnaz	482 m.a.s.l.	568197 / 114693	06.12.2013	Evionnaz EVI (PDF, 750 kB, )
Evolène / Villa	1825 m.a.s.l.	605415 / 106740	22.08.2005	Evolène / Villa EVO (PDF, 750 kB, )
Fahy	596 m.a.s.l.	562458 / 252676	12.04.2006	Fahy FAH (PDF, 750 kB, )
Faido	747 m.a.s.l.	704950 / 148266	13.03.2014	Faido FAI (PDF, 750 kB, )
Fieschertal	1175 m.a.s.l.	653990 / 142082	09.02.2016	Fieschertal FIT (PDF, 750 kB, )
Findelen	2170 m.a.s.l.	626317 / 95547	27.09.2011	Findelen VSFIN (PDF, 750 kB, )
Fionnay	1500 m.a.s.l.	589960 / 97765	04.09.2013	Fionnay FIO (PDF, 750 kB, )
Flawil	570 m.a.s.l.	733050 / 252940	30.09.2015	Flawil FLW (PDF, 750 kB, )
Flühli, LU	940 m.a.s.l.	644332 / 193311	06.10.2015	Flühli, LU FLU (PDF, 750 kB, )
Fribourg / Posieux	646 m.a.s.l.	575182 / 180076	20.06.2011	Fribourg / Posieux GRA (PDF, 750 kB, )
Frutigen	756 m.a.s.l.	616774 / 160884	18.08.2015	Frutigen FRU (PDF, 750 kB, )
Genève / Cointrin	412 m.a.s.l.	498905 / 122632	12.06.2009	Genève / Cointrin GVE (PDF, 750 kB, )
Gersau	521 m.a.s.l.	682510 / 205572	06.10.2015	Gersau GES (PDF, 750 kB, )
Giswil	471 m.a.s.l.	657320 / 188980	22.09.2010	Giswil GIH (PDF, 750 kB, )
Glarus	517 m.a.s.l.	723752 / 210568	13.10.2005	Glarus GLA (PDF, 750 kB, )
Gornergrat	3129 m.a.s.l.	626900 / 92512	17.10.2012	Gornergrat GOR (PDF, 750 kB, )
Grenchen	430 m.a.s.l.	598216 / 225348	06.12.2010	Grenchen GRE (PDF, 750 kB, )
Grimsel Hospiz	1980 m.a.s.l.	668583 / 158215	02.12.2008	Grimsel Hospiz GRH (PDF, 750 kB, )
Grono	324 m.a.s.l.	733017 / 124090	16.10.2012	Grono GRO (PDF, 750 kB, )
Grächen	1605 m.a.s.l.	630739 / 116062	31.05.2013	Grächen GRC (PDF, 750 kB, )
Gsteig, Gstaad	1196 m.a.s.l.	587680 / 136130	24.09.2014	Gsteig, Gstaad GSG (PDF, 750 kB, )
Guttannen	1055 m.a.s.l.	665296 / 167601	15.01.2014	Guttannen GTT (PDF, 750 kB, )
Göschenen	950 m.a.s.l.	688477 / 171926	23.02.2016	Göschenen GOS (PDF, 750 kB, )
Göscheneralp	1745 m.a.s.l.	681250 / 166790	16.10.2013	Göscheneralp GOA (PDF, 750 kB, )
Gösgen	380 m.a.s.l.	640417 / 245937	03.06.2008	Gösgen GOE (PDF, 750 kB, )
Gütsch, Andermatt	2283 m.a.s.l.	690050 / 167475	01.09.2005	Gütsch, Andermatt GUE (PDF, 750 kB, )
Güttingen	440 m.a.s.l.	738420 / 273960	28.08.2006	Güttingen GUT (PDF, 750 kB, )
Hallau	419 m.a.s.l.	677456 / 283472	11.10.2011	Hallau HLL (PDF, 750 kB, )
Huttwil	633 m.a.s.l.	630261 / 218189	16.10.2013	Huttwil HUT (PDF, 750 kB, )
Hörnli	1144 m.a.s.l.	713515 / 247755	03.12.2008	Hörnli HOE (PDF, 750 kB, )
Ilanz	698 m.a.s.l.	735685 / 181966	23.01.2014	Ilanz ILZ (PDF, 750 kB, )
Innerthal	903 m.a.s.l.	712019 / 218326	11.02.2014	Innerthal INN (PDF, 750 kB, )
Interlaken	577 m.a.s.l.	633019 / 169093	23.08.2007	Interlaken INT (PDF, 750 kB, )
Isérables	1237 m.a.s.l.	585332 / 112179	12.09.2012	Isérables VSISE (PDF, 750 kB, )
Jeizinen	1556 m.a.s.l.	621858 / 130817	01.10.2010	Jeizinen VSJEI (PDF, 750 kB, )
Jona	410 m.a.s.l.	706760 / 231290	04.11.2015	Jona JON (PDF, 750 kB, )
Jungfraujoch	3580 m.a.s.l.	641930 / 155275	28.08.2006	Jungfraujoch JUN (PDF, 750 kB, )
Kandersteg	1178 m.a.s.l.	618313 / 149198	04.11.2015	Kandersteg KAS (PDF, 750 kB, )
Kiesen	535 m.a.s.l.	610256 / 185527	07.11.2017	Kiesen KIS (PDF, 750 kB, )
Klosters	1186 m.a.s.l.	786857 / 193366	20.12.2013	Klosters KLA (PDF, 750 kB, )
Koppigen	484 m.a.s.l.	612662 / 218664	29.12.2011	Koppigen KOP (PDF, 750 kB, )
L' Auberson	1096 m.a.s.l.	523852 / 186295	11.11.2014	L' Auberson AUB (PDF, 750 kB, )
La Brévine	1050 m.a.s.l.	536982 / 203974	14.08.2013	La Brévine BRL (PDF, 750 kB, )
La Chaux-de-Fonds	1018 m.a.s.l.	550919 / 214861	10.11.2005	La Chaux-de-Fonds CDF (PDF, 750 kB, )
La Dôle	1670 m.a.s.l.	497061 / 142362	13.12.2006	La Dôle DOL (PDF, 750 kB, )
La Fouly	1564 m.a.s.l.	573453 / 87284	27.09.2011	La Fouly VSFLY (PDF, 750 kB, )
La Valsainte	1044 m.a.s.l.	580090 / 166314	07.11.2017	La Valsainte VST (PDF, 750 kB, )
Lachen / Galgenen	468 m.a.s.l.	707637 / 226334	27.10.2015	Lachen / Galgenen LAC (PDF, 750 kB, )
Langenbruck	731 m.a.s.l.	624250 / 244720	17.12.2012	Langenbruck LAB (PDF, 750 kB, )
Langnau i.E.	745 m.a.s.l.	628005 / 198792	27.06.2011	Langnau i.E. LAG (PDF, 750 kB, )
Lausanne	601 m.a.s.l.	538976 / 153256	04.09.2013	Lausanne LSN (PDF, 750 kB, )
Lauterbrunnen	815 m.a.s.l.	635851 / 160291	16.09.2015	Lauterbrunnen LTB (PDF, 750 kB, )
Le Moléson	1974 m.a.s.l.	567723 / 155072	14.10.2008	Le Moléson MLS (PDF, 750 kB, )
Leibstadt	341 m.a.s.l.	656379 / 272111	03.06.2008	Leibstadt LEI (PDF, 750 kB, )
Les Attelas	2727 m.a.s.l.	586862 / 105305	01.12.2017	Les Attelas ATT (PDF, 750 kB, )
Les Avants	1036 m.a.s.l.	561880 / 145178	17.03.2016	Les Avants AVA (PDF, 750 kB, )
Les Charbonnières	1045 m.a.s.l.	513821 / 169387	23.09.2014	Les Charbonnières CHB (PDF, 750 kB, )
Les Collons	1770 m.a.s.l.	596041 / 114169	02.09.2013	Les Collons VSCOL (PDF, 750 kB, )
Les Diablerets	2964 m.a.s.l.	581914 / 130622	25.09.2013	Les Diablerets DIA (PDF, 750 kB, )
Les Marécottes	990 m.a.s.l.	567375 / 107577	25.08.2015	Les Marécottes MAR (PDF, 750 kB, )
Leukerbad	1286 m.a.s.l.	614083 / 135090	04.09.2013	Leukerbad LEU (PDF, 750 kB, )
Locarno / Monti	367 m.a.s.l.	704160 / 114350	11.05.2007	Locarno / Monti OTL (PDF, 750 kB, )
Lohn, SH	585 m.a.s.l.	692910 / 289775	21.12.2012	Lohn, SH LOH (PDF, 750 kB, )
Longirod	900 m.a.s.l.	509387 / 150106	17.09.2014	Longirod LON (PDF, 750 kB, )
Lugano	273 m.a.s.l.	717874 / 95884	04.10.2006	Lugano LUG (PDF, 750 kB, )
Luzern	454 m.a.s.l.	665540 / 209848	12.06.2006	Luzern LUZ (PDF, 750 kB, )
Lägern	845 m.a.s.l.	672250 / 259460	03.06.2008	Lägern LAE (PDF, 750 kB, )
Magadino / Cadenazzo	203 m.a.s.l.	715480 / 113162	15.02.2006	Magadino / Cadenazzo MAG (PDF, 750 kB, )
Magglingen	883 m.a.s.l.	583037 / 221115	18.12.2012	Magglingen MGL (PDF, 750 kB, )
Malbun	1610 m.a.s.l.	764704 / 219182	17.05.2016	Malbun MAL (PDF, 750 kB, )
Marsens	715 m.a.s.l.	571758 / 167317	01.04.2014	Marsens MAS (PDF, 750 kB, )
Martigny	461 m.a.s.l.	570920 / 106546	30.03.2016	Martigny MAB (PDF, 750 kB, )
Martina	1041 m.a.s.l.	830464 / 196986	10.04.2013	Martina MAT (PDF, 750 kB, )
Mathod	437 m.a.s.l.	533458 / 176567	20.08.2013	Mathod MAH (PDF, 750 kB, )
Matro	2171 m.a.s.l.	714263 / 140944	24.09.2013	Matro MTR (PDF, 750 kB, )
Mattsand	1230 m.a.s.l.	627250 / 110279	01.10.2010	Mattsand VSMAT (PDF, 750 kB, )
Meiringen	589 m.a.s.l.	655844 / 175930	06.10.2011	Meiringen MER (PDF, 750 kB, )
Mervelier	540 m.a.s.l.	603884 / 243975	17.09.2014	Mervelier MEV (PDF, 750 kB, )
Moiry	2127 m.a.s.l.	610169 / 109590	27.09.2011	Moiry VSMOI (PDF, 750 kB, )
Montagnier-Bagnes	839 m.a.s.l.	583492 / 102189	19.01.2017	Montagnier-Bagnes MOB (PDF, 750 kB, )
Montana	1427 m.a.s.l.	601706 / 127482	11.02.2009	Montana MVE (PDF, 750 kB, )
Monte Generoso	1600 m.a.s.l.	722503 / 87456	23.10.2012	Monte Generoso GEN (PDF, 750 kB, )
Monte Rosa-Plattje	2885 m.a.s.l.	629149 / 89520	16.12.2009	Monte Rosa-Plattje MRP (PDF, 750 kB, )
Mormont	535 m.a.s.l.	569727 / 254223	07.09.2015	Mormont MMO (PDF, 750 kB, )
Mosen	453 m.a.s.l.	660128 / 232851	08.10.2012	Mosen MOA (PDF, 750 kB, )
Mosogno	771 m.a.s.l.	692803 / 117050	17.02.2015	Mosogno MSG (PDF, 750 kB, )
Mottec	1580 m.a.s.l.	614325 / 110730	15.04.2015	Mottec MTE (PDF, 750 kB, )
Muri, AG	577 m.a.s.l.	667276 / 235323	02.07.2013	Muri, AG MUR (PDF, 750 kB, )
Möhlin	344 m.a.s.l.	633053 / 269147	09.09.2010	Möhlin MOE (PDF, 750 kB, )
Mühleberg	480 m.a.s.l.	587788 / 202478	26.05.2008	Mühleberg MUB (PDF, 750 kB, )
Mühleberg / Stockeren	775 m.a.s.l.	588150 / 200125	29.10.2007	Mühleberg / Stockeren MSK (PDF, 750 kB, )
Naluns / Schlivera	2400 m.a.s.l.	815374 / 188987	17.11.2010	Naluns / Schlivera NAS (PDF, 750 kB, )
Napf	1404 m.a.s.l.	638133 / 206079	03.07.2007	Napf NAP (PDF, 750 kB, )
Nendaz	1938 m.a.s.l.	590166 / 107852	12.09.2012	Nendaz VSNEN (PDF, 750 kB, )
Nesselboden	1055 m.a.s.l.	605295 / 232697	11.02.2014	Nesselboden NEB (PDF, 750 kB, )
Neuchâtel	485 m.a.s.l.	563087 / 205560	14.07.2008	Neuchâtel NEU (PDF, 750 kB, )
Nyon / Changins	455 m.a.s.l.	506880 / 139573	13.10.2005	Nyon / Changins CGI (PDF, 750 kB, )
Oberiberg	1075 m.a.s.l.	701841 / 210464	09.02.2016	Oberiberg OBI (PDF, 750 kB, )
Oberriet / Kriessern	409 m.a.s.l.	764171 / 249582	05.05.2015	Oberriet / Kriessern OBR (PDF, 750 kB, )
Oberägeri	724 m.a.s.l.	688728 / 220956	08.12.2016	Oberägeri AEG (PDF, 750 kB, )
Opfikon	424 m.a.s.l.	684614 / 254698	05.07.2016	Opfikon OPF (PDF, 750 kB, )
Oron	827 m.a.s.l.	555506 / 158052	12.01.2012	Oron ORO (PDF, 750 kB, )
Orsières	929 m.a.s.l.	577022 / 96695	21.12.2012	Orsières ORS (PDF, 750 kB, )
Otemma	2357 m.a.s.l.	596477 / 85864	27.09.2011	Otemma VSOTE (PDF, 750 kB, )
Passo del Bernina	2260 m.a.s.l.	798422 / 143020	15.09.2014	Passo del Bernina BEH (PDF, 750 kB, )
Payerne	490 m.a.s.l.	562127 / 184612	05.12.2006	Payerne PAY (PDF, 750 kB, )
Pilatus	2106 m.a.s.l.	661910 / 203410	24.02.2009	Pilatus PIL (PDF, 750 kB, )
Piotta	990 m.a.s.l.	695888 / 152261	21.10.2009	Piotta PIO (PDF, 750 kB, )
Piz Corvatsch	3302 m.a.s.l.	783151 / 143522	15.12.2009	Piz Corvatsch COV (PDF, 750 kB, )
Piz Martegnas	2670 m.a.s.l.	760261 / 160570	22.11.2011	Piz Martegnas PMA (PDF, 750 kB, )
Plaffeien	1042 m.a.s.l.	586808 / 177400	08.08.2005	Plaffeien PLF (PDF, 750 kB, )
Poschiavo / Robbia	1078 m.a.s.l.	801850 / 136180	23.01.2008	Poschiavo / Robbia ROB (PDF, 750 kB, )
Pully	456 m.a.s.l.	540811 / 151514	19.01.2006	Pully PUY (PDF, 750 kB, )
Quinten	419 m.a.s.l.	734848 / 221278	24.10.2011	Quinten QUI (PDF, 750 kB, )
Rempen	650 m.a.s.l.	710610 / 221850	26.02.2016	Rempen REM (PDF, 750 kB, )
Riedholz / Wallierhof	518 m.a.s.l.	609394 / 231491	30.03.2016	Riedholz / Wallierhof WHF (PDF, 750 kB, )
Robièi	1896 m.a.s.l.	682588 / 144091	30.10.2007	Robièi ROE (PDF, 750 kB, )
Romont	692 m.a.s.l.	561476 / 172622	18.03.2016	Romont ROM (PDF, 750 kB, )
Rossberg	1119 m.a.s.l.	684227 / 214211	11.01.2012	Rossberg ROG (PDF, 750 kB, )
Rothenbrunnen	622 m.a.s.l.	751220 / 181300	18.11.2016	Rothenbrunnen ROT (PDF, 750 kB, )
Rünenberg	611 m.a.s.l.	633246 / 253846	09.08.2006	Rünenberg RUE (PDF, 750 kB, )
S. Bernardino	1639 m.a.s.l.	734112 / 147296	19.01.2006	S. Bernardino SBE (PDF, 750 kB, )
Saas Balen	1535 m.a.s.l.	637837 / 110929	01.10.2010	Saas Balen VSSAB (PDF, 750 kB, )
Safien Platz	1296 m.a.s.l.	743556 / 171437	19.01.2017	Safien Platz SAP (PDF, 750 kB, )
Saignelégier	883 m.a.s.l.	565379 / 234076	17.05.2016	Saignelégier SAI (PDF, 750 kB, )
Salanfe	1965 m.a.s.l.	564020 / 110569	27.09.2011	Salanfe VSSFE (PDF, 750 kB, )
Saleina	1600 m.a.s.l.	573291 / 92777	01.10.2010	Saleina VSSAL (PDF, 750 kB, )
Salen-Reutenen	718 m.a.s.l.	719099 / 279047	24.04.2012	Salen-Reutenen HAI (PDF, 750 kB, )
Salez / Saxerriet	436 m.a.s.l.	755558 / 233374	18.11.2016	Salez / Saxerriet SAX (PDF, 750 kB, )
Samedan	1709 m.a.s.l.	787210 / 155700	03.07.2007	Samedan SAM (PDF, 750 kB, )
Sattel, SZ	790 m.a.s.l.	690999 / 215145	22.10.2013	Sattel, SZ SAG (PDF, 750 kB, )
Savognin	1172 m.a.s.l.	765387 / 162663	10.04.2013	Savognin SVG (PDF, 750 kB, )
Schaan	444 m.a.s.l.	755510 / 228260	03.03.2016	Schaan SUA (PDF, 750 kB, )
Schaffhausen	438 m.a.s.l.	688698 / 282796	30.07.2008	Schaffhausen SHA (PDF, 750 kB, )
Schiers	626 m.a.s.l.	769617 / 205125	01.01.2014	Schiers SRS (PDF, 750 kB, )
Schmerikon	408 m.a.s.l.	713722 / 231496	31.10.2011	Schmerikon SCM (PDF, 750 kB, )
Schüpfheim	742 m.a.s.l.	643684 / 199710	09.09.2010	Schüpfheim SPF (PDF, 750 kB, )
Scuol	1304 m.a.s.l.	817135 / 186393	24.02.2006	Scuol SCU (PDF, 750 kB, )
Segl-Maria	1804 m.a.s.l.	778575 / 144976	18.03.2014	Segl-Maria SIA (PDF, 750 kB, )
Siebnen	452 m.a.s.l.	710821 / 225538	26.02.2016	Siebnen SIE (PDF, 750 kB, )
Sierre	535 m.a.s.l.	609073 / 127492	02.09.2013	Sierre VSSIE (PDF, 750 kB, )
Sihlbrugg	549 m.a.s.l.	686318 / 230586	31.08.2015	Sihlbrugg SIH (PDF, 750 kB, )
Simplon-Dorf	1465 m.a.s.l.	647683 / 116340	28.10.2016	Simplon-Dorf SIM (PDF, 750 kB, )
Sion	482 m.a.s.l.	591630 / 118575	13.10.2005	Sion SIO (PDF, 750 kB, )
Soglio	1086 m.a.s.l.	761393 / 134474	22.09.2015	Soglio SOG (PDF, 750 kB, )
Sorniot-Lac Inférieur	1990 m.a.s.l.	573830 / 112848	12.09.2012	Sorniot-Lac Inférieur VSSOR (PDF, 750 kB, )
St-Prex	425 m.a.s.l.	523549 / 148525	06.12.2011	St-Prex PRE (PDF, 750 kB, )
St. Antönien	1456 m.a.s.l.	781634 / 205103	30.09.2015	St. Antönien SAN (PDF, 750 kB, )
St. Chrischona	493 m.a.s.l.	618695 / 269036	21.02.2011	St. Chrischona STC (PDF, 750 kB, )
St. Gallen	776 m.a.s.l.	747865 / 254588	23.01.2008	St. Gallen STG (PDF, 750 kB, )
Sta. Maria, Val Müstair	1383 m.a.s.l.	828861 / 165580	15.12.2011	Sta. Maria, Val Müstair SMM (PDF, 750 kB, )
Stabio	353 m.a.s.l.	716050 / 77966	08.10.2009	Stabio SBO (PDF, 750 kB, )
Stafel	2183 m.a.s.l.	618616 / 94810	27.09.2011	Stafel VSSTA (PDF, 750 kB, )
Starkenbach	897 m.a.s.l.	737313 / 227576	01.10.2014	Starkenbach STB (PDF, 750 kB, )
Steckborn	398 m.a.s.l.	715871 / 280916	09.02.2012	Steckborn STK (PDF, 750 kB, )
Stetten	355 m.a.s.l.	665026 / 249877	02.06.2015	Stetten AGSTE (PDF, 750 kB, )
Stöckalp	1070 m.a.s.l.	664320 / 183550	27.03.2012	Stöckalp STP (PDF, 750 kB, )
Susch	1416 m.a.s.l.	802002 / 181593	10.04.2013	Susch SUS (PDF, 750 kB, )
Säntis	2502 m.a.s.l.	744200 / 234920	30.11.2005	Säntis SAE (PDF, 750 kB, )
Thun	570 m.a.s.l.	611201 / 177640	19.12.2012	Thun THU (PDF, 750 kB, )
Thusis	672 m.a.s.l.	753155 / 175089	10.04.2013	Thusis THS (PDF, 750 kB, )
Titlis	3040 m.a.s.l.	675400 / 180400	18.12.2012	Titlis TIT (PDF, 750 kB, )
Torricella / Crana	1002 m.a.s.l.	712695 / 103746	16.03.2016	Torricella / Crana CTO (PDF, 750 kB, )
Trient	1329 m.a.s.l.	565634 / 99517	12.09.2012	Trient VSTRI (PDF, 750 kB, )
Trun	834 m.a.s.l.	719985 / 178224	08.04.2013	Trun TRU (PDF, 750 kB, )
Tsanfleuron	2052 m.a.s.l.	589461 / 129932	12.09.2012	Tsanfleuron VSTSN (PDF, 750 kB, )
Tschiertschen	1273 m.a.s.l.	765538 / 187679	20.12.2013	Tschiertschen TST (PDF, 750 kB, )
Turtmann	2180 m.a.s.l.	619600 / 113183	27.09.2011	Turtmann VSTUR (PDF, 750 kB, )
Uetliberg	854 m.a.s.l.	679454 / 245034	06.06.2011	Uetliberg UEB (PDF, 750 kB, )
Ulrichen	1346 m.a.s.l.	666740 / 150760	28.05.2008	Ulrichen ULR (PDF, 750 kB, )
Unterkulm	454 m.a.s.l.	650838 / 241212	21.12.2012	Unterkulm UNK (PDF, 750 kB, )
Urnäsch	825 m.a.s.l.	739205 / 241765	22.09.2015	Urnäsch URN (PDF, 750 kB, )
Vaduz	457 m.a.s.l.	757719 / 221697	03.08.2006	Vaduz VAD (PDF, 750 kB, )
Valbella	1569 m.a.s.l.	761635 / 180385	13.10.2011	Valbella VAB (PDF, 750 kB, )
Vals	1242 m.a.s.l.	734016 / 165552	07.10.2015	Vals VLS (PDF, 750 kB, )
Vercorin	1650 m.a.s.l.	609075 / 120732	01.10.2010	Vercorin VSVER (PDF, 750 kB, )
Vevey / Corseaux	405 m.a.s.l.	552106 / 146847	08.09.2015	Vevey / Corseaux VEV (PDF, 750 kB, )
Vicosoprano	1089 m.a.s.l.	768485 / 135866	06.02.2013	Vicosoprano VIO (PDF, 750 kB, )
Villars-Tiercelin	859 m.a.s.l.	544198 / 163651	13.03.2014	Villars-Tiercelin VIT (PDF, 750 kB, )
Visp	639 m.a.s.l.	631149 / 128020	29.09.2008	Visp VIS (PDF, 750 kB, )
Visperterminen	1352 m.a.s.l.	635896 / 123602	02.09.2013	Visperterminen VSVIS (PDF, 750 kB, )
Vrin	1384 m.a.s.l.	727424 / 168526	29.09.2015	Vrin VRI (PDF, 750 kB, )
Vättis	939 m.a.s.l.	752732 / 197282	29.09.2015	Vättis VAE (PDF, 750 kB, )
Wartau	468 m.a.s.l.	756872 / 218250	17.02.2016	Wartau WAR (PDF, 750 kB, )
Weesen	425 m.a.s.l.	724983 / 221383	21.12.2012	Weesen WEE (PDF, 750 kB, )
Weissfluhjoch	2691 m.a.s.l.	780615 / 189634	30.10.2007	Weissfluhjoch WFJ (PDF, 750 kB, )
Winterthur / Seen	506 m.a.s.l.	699845 / 259054	27.11.2012	Winterthur / Seen WIN (PDF, 750 kB, )
Wittnau	463 m.a.s.l.	639941 / 258983	02.07.2013	Wittnau WIT (PDF, 750 kB, )
Wynau	422 m.a.s.l.	626400 / 233850	29.09.2009	Wynau WYN (PDF, 750 kB, )
Wädenswil	485 m.a.s.l.	693847 / 230744	04.06.2008	Wädenswil WAE (PDF, 750 kB, )
Würenlingen / PSI	334 m.a.s.l.	659540 / 265600	27.07.2011	Würenlingen / PSI PSI (PDF, 750 kB, )
Zermatt	1638 m.a.s.l.	624350 / 97560	01.09.2005	Zermatt ZER (PDF, 750 kB, )
Zervreila	1738 m.a.s.l.	728780 / 159993	03.10.2017	Zervreila ZEV (PDF, 750 kB, )
Zwillikon	461 m.a.s.l.	675138 / 238165	26.08.2016	Zwillikon ZWK (PDF, 750 kB, )
Zürich / Affoltern	444 m.a.s.l.	681428 / 253546	11.09.2007	Zürich / Affoltern REH (PDF, 750 kB, )
Zürich / Fluntern	556 m.a.s.l.	685117 / 248066	17.07.2007	Zürich / Fluntern SMA (PDF, 750 kB, )
Zürich / Kloten	426 m.a.s.l.	682710 / 259339	24.08.2009	Zürich / Kloten KLO (PDF, 750 kB, )";

        public static string VersionGeneral = null;
        public static string VersionForecast = null;
        public static string VersionMeasurements = null;
        public static string VersionWebcam = null;
        public static string VersionMultidays = null;
        public static DateTime LastVersionLoad = DateTime.MinValue;

        //public string LocationID = null;
        //public string LocationTitle = null;
        //public string LocationCityName = null;
        //public string LocationStationID = null;
        //public string LocationWebcamID = null;
        

        public MeteoSwissCrawler() : base(SERVER_URL) {
            SetCrawlerProfile(Module.Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
        }
        

        public JToken GetLocationInfoBySearching(string location) {
            // Init
            LoadDataVersions();
            string locationID = null;
            string locationTitle = null;

            // Search
            var query = Core.Util.String.RemoveDiacritics(location.Substring(0, 2).ToLower());
            var search = this.GetJSON(SEARCH_URL.Replace("{query}", query));
            var cityMatch1 = (location).ToLower();
            var cityMatch2 = Core.Util.String.RemoveDiacritics(location).ToLower();
            // Find match
            // [1]	{"862200;ZH;0;8622;1;Wetzikon ZH"}	System.Json.JsonPrimitive
            foreach(var entry in search) {
                _logger.Trace("Search Result  {0}",entry.ToString());
                var testAgainst = entry.ToString().ToLower();
                if(testAgainst.EndsWith(cityMatch1) || testAgainst.EndsWith(cityMatch2) || testAgainst.Contains(";"+cityMatch1)|| testAgainst.Contains(";"+cityMatch2)) {
                    var segs = entry.ToString().Split(';');
                    locationID = segs[0];
                    locationTitle = segs[5];
                    _logger.Info("Found Location ID {0}", locationID);
                    _logger.Info("Found Location Title {0}", locationTitle);
                    break;
                }
            }
            if (locationID == null) throw new Exception("Could not find any location for '"+location+"'."); ;

            // Get station info
            var locationInfo = this.GetLocationInfo(locationID);
            locationInfo["location_id"] = locationID;
            return locationInfo;
        }

        public static DateTime ConvertStringToDateTime(JToken token) {
            var str = token.ToString();
            var num = long.Parse(str);
            var utcOffsetMeteoSwissHours = 1; // Seems to be offset by one hour?!?
            DateTime epoch = new DateTime(1970, 1, 1, utcOffsetMeteoSwissHours, 0, 0, 0).ToUniversalTime();
            var d = epoch.AddSeconds(num/1000);
            return d;
        }
        
        public JToken GetAllLocations() {
            var lines = ALL_LOCATIONS.Split('\n');
            var ret = new JArray();
            foreach(var line in lines) {
                var props = line.Split('\t');
                var location = new JObject();
                location["title"] = props[0].Trim();
                location["state"] = props[3].Trim();
                ret.Add(location);
            }
            return ret;
        }

        public JToken GetAllStations() {
            var lines = ALL_STATIONS.Split('\n');
            var ret = new JArray();
            foreach(var line in lines) {
                var props = line.Split('\t');
                var station = new JObject();
                station["title"] = props[0];
                station["altitude"] = props[1];
                var latlon = props[2];
                station["latlon"] = props[2];
                var stationId = props[4];
                var stationIdSegs = stationId.Split(' ');
                for(var i = 0; i < stationIdSegs.Length; i++) {
                    if (stationIdSegs[i] == "(PDF,") stationId = stationIdSegs[i - 1];
                }
                station["id"] = stationId;
                ret.Add(station);
            }
            return ret;
        }

        public JToken GetLocationInfo(string locationID) {
            // Init
            LoadDataVersions();
            
            // Get station info
            // http://www.meteoswiss.admin.ch/etc/designs/meteoswiss/ajax/location/635300.json
            #region Example Data
            //{
            //	"location_id":"635300",
            //	"city_name":"Weggis",
            //	"warn_region_ids":[135,464,475,476,412,414],
            //	"station_id":"GES",
            //	"webcam_id":"GOD"
            //}
            #endregion
            var locationInfo = this.GetJSON(LOCATION_URL.Replace("{location-id}", locationID));
            //this.LocationStationID = locationInfo["station_id"].ToString();
            //this.LocationWebcamID = locationInfo["webcam_id"].ToString();
            //this.LocationCityName = locationInfo["city_name"].ToString();

            return locationInfo;
        }

        public JToken GetLocationForecastData(string locationID) {
            // Init
            LoadDataVersions();
            
            // Get forecast
            // http://www.meteoswiss.admin.ch/product/output/forecast-chart/version__20180101_0902/en/635300.json
            #region Example Data
            //  [
            //   {
            //      "current_time":1514887340541,
            //      "current_time_string":"11:02",
            //      "min_date":1514847600000,
            //      "max_date":1514930400000,
            //      "new_day":1514934000000,
            //      "day_string":"Today",
            //      "rainfall":[

            //		 [
            //            1514847600000,
            //            0
            //         ],
            //         [
            //            1514851200000,
            //            0
            //         ],

            //      ],
            //      "temperature":[

            //		 [
            //            1514847600000,
            //            7.4
            //         ],

            //      ],
            //      "variance_range":[

            //		 [
            //            1514847600000,
            //            7.4,
            //            7.4
            //         ],

            //      ],
            //      "variance_rain":[

            //		 [
            //            1514847600000,
            //            0,
            //            0.1
            //         ],
            //         [
            //            1514851200000,
            //            0,
            //            0.3
            //         ],

            //      ],
            //      "symbols":[
            //         {
            //            "timestamp":1514854800000,
            //            "weather_symbol_id":105

            //		 },
            //         {
            //            "timestamp":1514865600000,
            //            "weather_symbol_id":115
            //         },

            //      ],
            //      "wind":{
            //         "data":[

            //			[
            //               1514847600000,
            //               22.5
            //            ],
            //            [
            //               1514851200000,
            //               9.1
            //            ],

            //         ],
            //         "symbols":[
            //            {
            //               "timestamp":1514847600000,
            //               "symbol_id":"SW"
            //            },
            //            {
            //               "timestamp":1514854800000,
            //               "symbol_id":"SW"
            //            },

            //         ]
            //      }
            //   },
            //   {
            //      "current_time":null,
            //      "current_time_string":null,
            //      "min_date":1514934000000,
            //      "max_date":1515016800000,
            //      "day_string":"Wednesday",
            //       ...
            //      }
            //   }
            //				...
            //]
            #endregion
            var forecastData = this.GetJSON(FORECAST_URL.Replace("{location-id}", locationID).Replace("{version}", VersionForecast));

            return forecastData;
        }

        public JToken GetStationMeasuredData(string stationID) {
            // Init
            LoadDataVersions();
            
            // Get measured data
            // http://www.meteoswiss.admin.ch/product/output/measured-values-v2/homepage/version__20180101_0911/en/GES.json
            #region Example Data
            //[
            //   {
            //      "current_time":1514887280319,
            //      "current_time_string":"11:01",
            //      "new_day":1514934000000,
            //      "day_string":"Today",
            //      "all_stations":"/meteo-measurement.html",
            //      "name":"Gersau",
            //      "altitude":521,
            //      "temperature":[

            //		 [
            //            1514800800000,
            //            5.9
            //         ],
            //         [
            //            1514801400000,
            //            6.2
            //         ],

            //      ],
            //      "rainfall":[

            //		 [
            //            1514804400000,
            //            0
            //         ],
            //         [
            //            1514808000000,
            //            0
            //         ],
            //         [
            //            1514811600000,
            //            0
            //         ],
            //         [
            //            1514815200000,
            //            0
            //         ],
            //         [
            //            1514818800000,
            //            0
            //         ],

            //      ],
            //      "sunshine":[

            //		 [
            //            1514804400000,
            //            null
            //         ],
            //         [
            //            1514808000000,
            //            null
            //         ],

            //      ],
            //      "wind":{
            //         "data":[

            //			[
            //               1514800800000,
            //               5.4
            //            ],
            //            [
            //               1514801400000,
            //               7.9
            //            ],

            //         ],
            //         "symbols":[
            //            {
            //               "timestamp":1514804400000,
            //               "symbol_id":"S"

            //			},
            //            {
            //               "timestamp":1514811600000,
            //               "symbol_id":"S"
            //            },
            //            {
            //               "timestamp":1514818800000,
            //               "symbol_id":"NW"
            //            },
            //            {
            //               "timestamp":1514826000000,
            //               "symbol_id":"N"
            //            },
            //            {
            //               "timestamp":1514833200000,
            //               "symbol_id":"NW"
            //            },

            //         ]
            //      },
            //      "min_date":1514800800000,
            //      "max_date":1514886600000
            //   }
            //]
            #endregion
            var measuredData = this.GetJSON(MEASUREMENTS_URL.Replace("{station-id}", stationID).Replace("{version}", VersionMeasurements));
            
            return measuredData.First;
        }

        public void LoadDataVersions() {
            if (LastVersionLoad >= DateTime.UtcNow.AddMinutes(-10)) return; // it's 'cached'
            var data = this.GetURL(HOME_URL);
            VersionGeneral = _getVersionAttributeValueFromHTML(data, "data-json-url");
            VersionForecast = _getVersionAttributeValueFromHTML(data, "data-forecast-json-url");
            VersionMeasurements = _getVersionAttributeValueFromHTML(data, "data-measurements-json-url");
            VersionWebcam = _getVersionAttributeValueFromHTML(data, "data-webcam-json-url");
            VersionMultidays = _getVersionAttributeValueFromHTML(data, "data-multidays-url");
            _logger.Info("VersionGeneral {0}", VersionGeneral);
            _logger.Info("VersionMeasurements {0}", VersionMeasurements);
            _logger.Info("VersionWebcam {0}", VersionWebcam);
            _logger.Info("VersionMultidays {0}", VersionMultidays);
            _validateNotNull("VersionGeneral {0}", VersionGeneral);
            _validateNotNull("VersionMeasurements {0}", VersionMeasurements);
            _validateNotNull("VersionWebcam {0}", VersionWebcam);
            _validateNotNull("VersionMultidays {0}", VersionMultidays);
            LastVersionLoad = DateTime.UtcNow;
        }

        private static void _validateNotNull(string title, string val) {
            if (val == null) throw new Exception(title + " is null.");
        }

        private static string _getVersionAttributeValueFromHTML(string data, string attr) {
            var ret = _getAttributeValueFromHTML(data, attr);
            return _findInString(ret, "version__", "/");
        }

        private static string _getAttributeValueFromHTML(string data, string attr) {
            var toFindA = attr + "=\"";
            var toFindB = "\"";
            return _findInString(data, toFindA, toFindB);
        }

        private static string _findInString(string data, string toFindA, string toFindB) {
            var i1 = data.IndexOf(toFindA);
            if (i1 < 0) return null;
            i1 += toFindA.Length;
            var i2 = data.IndexOf(toFindB, i1);
            if (i2 < 0) return null;
            return data.Substring(i1, i2 - i1);
        }
        
    }
}