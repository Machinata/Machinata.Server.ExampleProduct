using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO.Compression;
using System.IO;
using System.Text.RegularExpressions;
using static Machinata.Core.Util.HTTP;
using System.Collections.Specialized;
using Machinata.Core.Util;

namespace Machinata.Module.Crawler {

    /// <summary>
    /// Common web crawler util class.
    /// 
    /// Currently very basic, could be much more complex in the future.
    /// </summary>
    public class WebCrawler {
        
        public const string CRAWLER_PROFILE_FIREFOX_LINUX = "FirefoxLinux";
        public const string CRAWLER_PROFILE_GOOGLEBOT_SEARCH = "GoogleBotSearch";
        public const string CRAWLER_PROFILE_GOOGLEBOT_IMAGES = "GoogleBotImages";
        public const string CRAWLER_PROFILE_CURL = "CURL";
        public const string CRAWLER_PROFILE_RANDOM_BOT = "RandomBot";
        

        public string BaseURL;

        public string UserAgent;

        public TimeSpan MinimumTimeBetweenRequests = new TimeSpan(0);
        public TimeSpan RequestTimeout = new TimeSpan(0,0,30); // 30 seconds

        private DateTime _lastRequest = DateTime.MinValue;

        /// <summary>
        /// If not set to TimeSpan.MinValue, then each response will be cached for the given timespan to disk.
        /// </summary>
        public TimeSpan ResponseCacheAge = TimeSpan.MinValue;


        public WebCrawler(string baseURL = null) {
            this.BaseURL = baseURL;
            this.UserAgent = Crawler.Config.CrawlerRandomUserAgents.ElementAt(Core.Util.Math.RandomInt(0,Crawler.Config.CrawlerRandomUserAgents.Count-1));
        }

        public string SetCrawlerProfile(string profilename) {
            var userAgents = Core.Config.GetStringListSetting("CrawlerProfile" + profilename + "UserAgent");
            this.UserAgent = userAgents.Random();
            return this.UserAgent;
        }

        /// <summary>
        /// Caches the response for the given timespan based on the URL.
        /// </summary>
        /// <param name="maxAge">The maximum age.</param>
        /// <returns></returns>
        public WebCrawler CacheResponse(TimeSpan maxAge) {
            this.ResponseCacheAge = maxAge;
            return this;
        }

        /// <summary>
        /// Caches the response for the given minutes based on the URL.
        /// </summary>
        /// <param name="minutes">The minutes.</param>
        /// <returns></returns>
        public WebCrawler CacheResponse(int minutes) {
            return this.CacheResponse(new TimeSpan(0, minutes, 0));
        }

        public void DestroyCaches() {
            Core.Caching.StringCacheGenerator.DestroyCaches(this.GetType().Name+"_*");
        }

        public virtual WebClient GetWebClient(Encoding encoding = null) {
            var client = new WebClient();
            if (encoding != null) client.Encoding = encoding;
            else client.Encoding = Encoding.UTF8;
            if(this.UserAgent != null) client.Headers["UserAgent"] = this.UserAgent;
            return client;
        }
        

        private void _waitForNextRequest() {
            while(_lastRequest > DateTime.UtcNow - this.MinimumTimeBetweenRequests) {
                System.Threading.Thread.Sleep(10);
            }
            _lastRequest = DateTime.UtcNow;
        }

        public HttpWebRequest GetHTTPRequest(string url, string method = "GET", Encoding encoding = null) {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //TODO: Set encoding
            request.Method = method;
            request.UserAgent = this.UserAgent;
            return request; 
        }
        

        public string GetHTTPResponseAsString(HttpWebRequest request, Encoding encoding = null) {
            var response = request.GetResponse();
            if (encoding == null) encoding = Encoding.UTF8;
            string ret = null;
            using (var stream = response.GetResponseStream()) {
                var reader = new StreamReader(stream, encoding);
                ret = reader.ReadToEnd();
            }
            return ret;
        }

        public Dictionary<string,string> GetHTTPCookies(string url, string method = "HEAD") {
            var ret = new Dictionary<string, string>();
            HttpWebRequest request = this.GetHTTPRequest(url, method);
            var response = request.GetResponse();
            var cookies = response.Headers.GetValues("Set-Cookie");
            foreach(var cookie in cookies) {
                // Parse it out
                // Each cookie looks like 
                // "X-BackEndCookie=3bf3a59c-9a51-4dcf-ad31-bf60f9335354=u56Lnp2ejJqBysjJnJ3MxpnSmpqZytLLycjP0sfLzszSzs/Py53MmcaezpzMgYHNz87G0s/M0s7Lq8/GxcrMxcvH; expires=Thu, 14-Mar-2019 09:53:48 GMT; path=/owa/calendar; secure; HttpOnly"
                var i1 = cookie.IndexOf('=');
                var i2 = cookie.IndexOf(';');
                var key = cookie.Substring(0, i1);
                var val = cookie.Substring(i1+1, i2-i1-1);
                ret.Add(key, val);
            }
            return ret;
        }
        

        private string _getURL(string url, Encoding encoding = null) {
            if (!url.StartsWith("http")) url = this.BaseURL + url;
            var client = GetWebClient(encoding);
            string data;
            try {
                data = client.DownloadString(url);
            } catch (Exception e) {
                throw new Exception($"Crawler could not HTTP Get '{url}': " + e.Message, e);
            }
            return data;
        }

        public string GetURL(string url, Encoding encoding = null) {
            // Cache?
            if(this.ResponseCacheAge != TimeSpan.MinValue) {
                // Use cache
                return Core.Caching.StringCacheGenerator.CreateIfNeeded(this.GetType().Name+"_"+url, ()=> {
                    return _getURL(url, encoding);
                }, this.ResponseCacheAge);
            } else {
                // No cache
                return _getURL(url, encoding);
            }
            
        }

        public string PostURL(string url, NameValueCollection postData, Encoding encoding = null) {
            if (!url.StartsWith("http")) url = this.BaseURL + url;
            var client = GetWebClient(encoding);
            string data;
            try {
                data = client.Encoding.GetString(client.UploadValues(url,postData));
            } catch (Exception e) {
                throw new Exception($"Crawler could not HTTP Post '{url}': " + e.Message, e);
            }
            return data;
        }

        public string PostData(string url, byte[] postData, Encoding encoding = null) {
            if (!url.StartsWith("http")) url = this.BaseURL + url;
            var client = GetWebClient(encoding);
            string data;
            try {
                var dataRaw = client.UploadData(url, postData);
                data = client.Encoding.GetString(dataRaw);
            } catch (Exception e) {
                throw new Exception($"Crawler could not HTTP Post '{url}': " + e.Message, e);
            }
            return data;
        }

        public string PostString(string url, string postData, Encoding encoding = null) {
            if (!url.StartsWith("http")) url = this.BaseURL + url;
            var client = GetWebClient(encoding);
            string data;
            try {
                data = client.UploadString(url, postData);
            } catch (Exception e) {
                throw new Exception($"Crawler could not HTTP Post '{url}': " + e.Message, e);
            }
            return data;
        }

        public string PostStringWithRetries(string url, string postData, Encoding encoding = null, int retries = 10) {
            int attempt = 0;
            while (attempt <= retries) {
                attempt++;
                try {
                    return PostString(url, postData, encoding);
                } catch(Exception e) {
                    if (attempt == retries) throw new Exception($"Ran out of retries (attempt {attempt}/{retries}): ", e);
                    System.Threading.Thread.Sleep(200*attempt);
                }
            }
            throw new Exception("Ran out of retries."); // Should never happen
        }

        /// <summary>
        /// Gets the URL using unchecked web request.
        /// Only use this for broken services that do not have SSL chain configured correctly.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public string GetURLUsingUncheckedWebRequest(string url, Encoding encoding = null) {
            _waitForNextRequest();

            if (!url.StartsWith("http")) url = this.BaseURL + url;
            string data;
            WebRequest request = WebRequest.Create(url);	
            request.Credentials = CredentialCache.DefaultCredentials;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            try {
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                data = reader.ReadToEnd();
                reader.Close();
                response.Close();
            } catch (Exception e) {
                throw new Exception($"Crawler could not download unchecked '{url}': " + e.Message, e);
            } finally {
                ServicePointManager.ServerCertificateValidationCallback = null;
            }
            return data;
        }

        public string GetGZ(string url) {
            if (!url.StartsWith("http")) url = this.BaseURL + url;
            var client = GetWebClient(Encoding.UTF8);
            var data = client.DownloadData(url);
            string unzipped = null;
            using(MemoryStream dataStream = new MemoryStream(data)) {
                using (GZipStream decompressionStream = new GZipStream(dataStream, CompressionMode.Decompress)) {
                    using (var sr = new StreamReader(decompressionStream)) {
                        unzipped = sr.ReadToEnd();
                    }
                }
            }
            return unzipped;
        }

        public JToken GetJSON(string url) {
            var data = GetURL(url);
            JToken ret = null;
            try {
                ret = JToken.Parse(data);
            }catch(Exception e) {
                ret = JToken.Parse("{ data: " + data + "}");
            }
            return ret;
        }

        public JToken PostJSON(string url, NameValueCollection postData) {
            var data = PostURL(url,postData);
            JToken ret = null;
            try {
                ret = JToken.Parse(data);
            }catch(Exception e) {
                ret = JToken.Parse("{ data: " + data + "}");
            }
            return ret;
        }

        /// <summary>
        /// Gets the extractions from URL using a regex expression.
        /// For example, you could pass "https:\\/\\/[0-9A-Za-z-]+.nerves\\.ch\\/vp\\/[0-9A-Za-z-/._]+s900x900[0-9A-Za-z-/._]+\\.jpg"
        /// to get all specific images from the given URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="regexPattern">The regex pattern.</param>
        /// <returns></returns>
        public List<string> GetRegexExtractionsFromURL(string url, string regexPattern) {
            var data = GetURL(url);
            var ret = new List<string>();
            Regex regex = new Regex(regexPattern);
            var matches = regex.Matches(data);
            foreach(var match in matches) {
                ret.Add(match.ToString());
            }
            return ret;
        }

        public string GetStringExtractionFromURL(string url, string prefix, string postfix) {
            var data = GetURL(url);
            try {
                return GetStringExtractionFromString(data, prefix, postfix);
            } catch(Exception e) {
                throw new Exception("Prefix or postfix could not be found in " + url,e);
            }
        }

        public string GetStringExtractionFromString(string data, string prefix, string postfix) {
            var start = data.IndexOf(prefix, 0);
            if (start < 0) throw new Exception("Prefix could not be found.");
            var end = data.IndexOf(postfix, start);
            if (end < 0) throw new Exception("Postfix could not be found.");
            start = start + prefix.Length;
            var ret = data.Substring(start, end - start);
            return ret;
        }

        public JToken GetJSONExtractionFromURL(string url, string prefix, string postfix) {
            var data = GetStringExtractionFromURL(url, prefix, postfix);
            JToken ret = null;
            try {
                ret = JToken.Parse(data);
            }catch(Exception e) {
                ret = JToken.Parse("{ data: " + data + "}");
            }
            return ret;
        }

    }
}
