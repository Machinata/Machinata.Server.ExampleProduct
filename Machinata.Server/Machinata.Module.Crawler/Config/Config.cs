using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler {
    public class Config {
        public static List<string> CrawlerRandomUserAgents = Core.Config.GetStringListSetting("CrawlerRandomUserAgents");
        //Set a Authorization Header Value for the Request
        public static string OTDTimetableAPIKey = Core.Config.GetStringSetting("OTDTimetableAPIKey");

    }
}
