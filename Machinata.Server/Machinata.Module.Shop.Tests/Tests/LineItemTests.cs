using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Machinata.Module.Shop.Model;
using Moq;
using Machinata.Core.Model;
using System.Web;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using Machinata.Module.Shop.Logic;
using Machinata.Module.Shop.Handler.API.Frontend;
using System.Linq;
using System.Net;
using System.Text;

namespace Machinata.Module.Shop.Tests {

    /// <summary>
    /// Vat Tests fail, because of rounding errors
    /// </summary>
    [TestClass]
    public class LineItemTests {
        

        //[TestMethod]
        public void TestVATExclusiveBehavior1() {
            var itemPrice = new Price(100);
            var expectedTotal = itemPrice * 2;
            int quantity = 2;
            decimal vatRate = 0.077m;

            var itemPriceNoVAT = itemPrice - (itemPrice * vatRate);


            Finance.Model.LineItem lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = itemPriceNoVAT.Clone();
            lineItem.CustomerVATRate = vatRate;
            lineItem.Quantity = quantity;

            Assert.AreEqual(expectedTotal.Value, lineItem.CustomerTotal.Value);

        }

        //[TestMethod]
        public void TestVATExclusiveBehavior2() {
            var itemPrice = new Price(100);
            var expectedTotal = itemPrice * 2;
            int quantity = 2;
            decimal vatRate = 0.077m;

            var totalPriceNoVAT = (itemPrice*quantity) - ((itemPrice*quantity) * vatRate);
            var priceNoVAT = totalPriceNoVAT / quantity;

            Finance.Model.LineItem lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = priceNoVAT.Clone();
            lineItem.CustomerVATRate = vatRate;
            lineItem.Quantity = quantity;

            Assert.AreEqual(expectedTotal.Value, lineItem.CustomerTotal.Value);

        }

       // [TestMethod]
        public void TestVATExclusiveBehavior3() {
            var itemPrice = new Price(1231.25m);
            int quantity = 1;
            var expectedTotal = itemPrice * quantity;
            decimal vatRate = 0.077m;

            // From LineItem.cs
            // total = price*quantity + price*quantity*vat
            // ==> price = total / (quantity*vat + quantity)
            var priceNoVAT = (expectedTotal.Value.Value) / (quantity * vatRate + quantity);

            Finance.Model.LineItem lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = new Price(priceNoVAT);
            lineItem.CustomerVATRate = vatRate;
            lineItem.Quantity = quantity;
            
            Console.WriteLine(priceNoVAT);

            Assert.AreEqual(expectedTotal.Value, lineItem.CustomerTotal.Value);

        }
        
    }


}
