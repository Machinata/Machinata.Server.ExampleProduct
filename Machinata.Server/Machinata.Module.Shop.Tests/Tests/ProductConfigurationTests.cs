using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Machinata.Module.Shop.Model;
using Moq;
using Machinata.Core.Model;
using System.Web;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using Machinata.Module.Shop.Logic;
using Machinata.Module.Shop.Handler.API.Frontend;
using System.Linq;
using System.Net;
using System.Text;

namespace Machinata.Module.Shop.Tests {


    [TestClass]
    public class ProductConfigurationTests {
        private const string SWISS_FRANCS = "CHF";
      


        [TestMethod]
        public void TestMergeConfigurationNothingNew() {

            var product = new Product();
            product.Name = "ProductName";

            // Default 
            var configuration = new ProductConfiguration();
            configuration.InternalPrice = new Price(12, SWISS_FRANCS);
            configuration.CustomerPrice = new Price(19, SWISS_FRANCS);
            configuration.SKU = "SKU";
            configuration.Title = "Title";
            configuration.RetailPrice= new Price(21, SWISS_FRANCS);
            configuration.CustomerVATRate = 7.7m;
            configuration.RetailVATRate = 7.9m;
            configuration.Value ="something";
            configuration.StockQuantity = 123;
            configuration.IsDefault = true;
            configuration.Id = 1;
            
            product.Configurations.Add(configuration);
            var selectedConfiguration = new ProductConfiguration();
            selectedConfiguration.Id = 2;
            product.Configurations.Add(selectedConfiguration);
            
            var merged = product.GetConfiguration(selectedConfiguration);

            Assert.AreEqual(configuration.InternalPrice.ToString(), merged.InternalPrice.ToString());
            Assert.AreEqual(configuration.CustomerPrice.ToString(), merged.CustomerPrice.ToString());
            Assert.AreEqual(configuration.SKU, merged.SKU);
            Assert.AreEqual("Title", merged.Title);
            Assert.AreEqual(configuration.RetailPrice.ToString(), merged.RetailPrice.ToString());
            Assert.AreEqual(configuration.CustomerVATRate, merged.CustomerVATRate);
            Assert.AreEqual(configuration.RetailVATRate, merged.RetailVATRate);
            Assert.AreEqual(null, merged.Value);
            Assert.AreEqual(configuration.StockQuantity, merged.StockQuantity);
            Assert.AreEqual(false, merged.IsDefault);
            Assert.AreSame(configuration.Product, merged.Product);
            Assert.AreEqual(selectedConfiguration.PublicId, merged.PublicId);


        }


        [TestMethod]
        public void TestMergeConfigurationAllNew() {

            var product = new Product();
            product.Name = "ProductName";

            // Default 
            var configuration = new ProductConfiguration();
            configuration.InternalPrice = new Price(12, SWISS_FRANCS);
            configuration.CustomerPrice = new Price(19, SWISS_FRANCS);
            configuration.SKU = "SKU";
            configuration.Title = "Title";
            configuration.RetailPrice = new Price(21, SWISS_FRANCS);
            configuration.CustomerVATRate = 7.7m;
            configuration.RetailVATRate = 7.9m;
            configuration.Value = "something";
            configuration.StockQuantity = 123;
            configuration.IsDefault = true;
            configuration.Id = 1;

            product.Configurations.Add(configuration);


            var selectedConfiguration = new ProductConfiguration();
            selectedConfiguration.Id = 2;
            selectedConfiguration.InternalPrice = new Price(13, SWISS_FRANCS);
            selectedConfiguration.CustomerPrice = new Price(20, SWISS_FRANCS);
            selectedConfiguration.SKU = "SKU-C2";
            selectedConfiguration.Title = "Title 2";
            selectedConfiguration.RetailPrice = new Price(23, SWISS_FRANCS);
            selectedConfiguration.CustomerVATRate = 7.9m;
            selectedConfiguration.RetailVATRate = 7.3m;
            selectedConfiguration.Value = "else";
            selectedConfiguration.StockQuantity = 2;
            product.Configurations.Add(selectedConfiguration);

            var merged = product.GetConfiguration(selectedConfiguration);

            Assert.AreEqual(selectedConfiguration.InternalPrice.ToString(), merged.InternalPrice.ToString());
            Assert.AreEqual(selectedConfiguration.CustomerPrice.ToString(), merged.CustomerPrice.ToString());
            Assert.AreEqual(selectedConfiguration.SKU, merged.SKU);
            Assert.AreEqual(selectedConfiguration.Title, merged.Title);
            Assert.AreEqual(selectedConfiguration.RetailPrice.ToString(), merged.RetailPrice.ToString());
            Assert.AreEqual(selectedConfiguration.CustomerVATRate, merged.CustomerVATRate);
            Assert.AreEqual(selectedConfiguration.RetailVATRate, merged.RetailVATRate);
         //   Assert.AreEqual(selectedConfiguration.Value, merged.Value);
            Assert.AreEqual(selectedConfiguration.StockQuantity, merged.StockQuantity);
            Assert.AreEqual(false, merged.IsDefault);
            Assert.AreSame(selectedConfiguration.Product, merged.Product);
            Assert.AreEqual(selectedConfiguration.PublicId, merged.PublicId);


        }


    }

}
