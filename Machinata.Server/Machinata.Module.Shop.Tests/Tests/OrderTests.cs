using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Machinata.Module.Shop.Model;
using Moq;
using Machinata.Core.Model;
using System.Web;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using Machinata.Module.Shop.Logic;
using Machinata.Module.Shop.Handler.API.Frontend;
using System.Linq;
using System.Net;
using System.Text;

namespace Machinata.Module.Shop.Tests {


    [TestClass]
    public class OrderPayBeforeTests {
        private const string SWISS_FRANCS = "CHF";
        private ProductConfiguration _configuration2;
        private Mock<ModelContext> _db;
        private Product _product;

        [TestMethod]
        public void InitializePayBeforeOrder() {
            Order order = InitializeTestOrder();

            Assert.AreEqual(Order.StatusType.Draft, order.Status);
            Assert.AreEqual(typeof(PaybeforeStrategy), order.ProcessLogic.GetType());
            Assert.AreEqual(new Price(0, SWISS_FRANCS).ToString(), order.TotalCustomer.ToString());
            Assert.AreEqual(new Price(0, SWISS_FRANCS).ToString(), order.SubtotalCustomer.ToString());
            Assert.AreEqual(new Price(0, SWISS_FRANCS).ToString(), order.Discount.ToString());
            Assert.AreEqual(new Price(0, SWISS_FRANCS).ToString(), order.VATCustomer.ToString());
            Assert.AreEqual(new Price(0, SWISS_FRANCS).ToString(), order.ShippingCustomer.ToString());

        }

        private Order InitializeTestOrder() {
            var order = new Order();
            order.SetStoredInDB(false);

            var originAddress = new Mock<Address>();
            var user = new Mock<User>();

            order.InitializeOrder(user.Object, SWISS_FRANCS, originAddress.Object, "127.0.0.1", "TestAgent", "de,en", Finance.Payment.PaymentTime.Before, Finance.Payment.PaymentMethod.CreditCard);
           _db = new Mock<ModelContext>();
              order.Context = _db.Object;
            return order;
        }

        [TestMethod]
        public void OrderAddItems() {
            Order order = SetupBasicOrderWithItems();

            Assert.AreEqual((decimal)(19 * 4 + 17 * 7 + 2 * 19), order.SubtotalCustomer.Value);
            Assert.AreEqual(SWISS_FRANCS, order.Currency);
        }



        [TestMethod]
        public void OrderDraftNoItemsNoRestrictions() {
            Order order = InitializeTestOrder();

            _db.Setup(db => db.Set<OrderRestriction>()).Returns(GetQueryableMockDbSet<OrderRestriction>( ));

          //  Assert.IsFalse(order.ProcessLogic.AllowConfirm(false)); // should be false in PayBefore
            Assert.IsFalse(order.ProcessLogic.AllowPlace(false));
            Assert.IsTrue(order.ProcessLogic.AllowDelete());
            Assert.IsFalse(order.ProcessLogic.AllowPlaceConfirm());
            Assert.IsFalse(order.ProcessLogic.AllowPlacePay());
            Assert.IsFalse(order.ProcessLogic.AllowRedraft(false));
        }

        [TestMethod]
        public void OrderAddItemsExisting() {
            Order order = SetupBasicOrderWithItems();

            var count = order.OrderItems.Count;

            order.AddOrderItem(_db.Object, _product, _configuration2, SWISS_FRANCS, 2, "Test Order Item c2 merged c1");

            Assert.AreEqual(count, order.OrderItems.Count);
        }

        [TestMethod]
        public void OrderAddItemsNew() {
            Order order = SetupBasicOrderWithItems();

            var configuration = new ProductConfiguration();
            configuration.InternalPrice = new Price(12, SWISS_FRANCS);
            configuration.CustomerPrice= new Price(19, SWISS_FRANCS);

            var configurations = new List<ProductConfiguration>(_product.Configurations);
            var count = order.OrderItems.Count;
            configurations.Add(configuration);
            _product.Configurations = configurations;

            order.AddOrderItem(_db.Object, _product, configuration, SWISS_FRANCS, 2, "Test Order Item c2 merged c1");

            Assert.AreEqual(count + 1, order.OrderItems.Count);
        }

        private Order SetupBasicOrderWithItems() {
            var order = InitializeTestOrder();
            _db = new Mock<ModelContext>();
            _db.Setup(m => m.SaveChanges());
            _product = new Product();

            var vatRate = 0.077m;

            // Default 
            var configuration = new ProductConfiguration();
            configuration.InternalPrice = new Price(12, SWISS_FRANCS);
            configuration.CustomerPrice = new Price(19, SWISS_FRANCS);
            configuration.CustomerVATRate = vatRate;
            configuration.IsDefault = true;

            // Overload 1
            _configuration2 = new ProductConfiguration();
            _configuration2.InternalPrice = new Price(13, SWISS_FRANCS);
            _configuration2.CustomerPrice = new Price(17, SWISS_FRANCS);
            _configuration2.CustomerVATRate = vatRate;

            // Overload 2
            var configuration3 = new ProductConfiguration();
            configuration3.InternalPrice = new Price(13, SWISS_FRANCS);
            configuration3.CustomerPrice = new Price(0, SWISS_FRANCS);
            configuration3.CustomerVATRate = vatRate;

            _product.Configurations = new List<ProductConfiguration> { configuration, _configuration2, configuration3 };

            // Add Items
            order.AddOrderItem(_db.Object, _product, configuration, SWISS_FRANCS, 4, "Test Order Item c1");
            order.AddOrderItem(_db.Object, _product, _configuration2, SWISS_FRANCS, 7, "Test Order Item c2 merged c1");
            order.AddOrderItem(_db.Object, _product, configuration3, SWISS_FRANCS, 2, "Test Order Item c3 merged c1");
            return order;
        }


        // refactor Test Framewwork method

        private static DbSet<T> GetQueryableMockDbSet<T>(params T[] sourceList) where T : class {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return dbSet.Object;
        }


        [TestMethod]
        public void OrderVATExcludedCH() {

            Config.ShopVATIncluded = false;
            Order order = SetupBasicOrderWithItems();
            order.ShippingAddress = new Address();
            order.ShippingAddress.CountryCode = "ch";
            order.UpdateOrder();

            Assert.AreEqual(17.94m, order.VATCustomer.Value.Value);
            Assert.AreEqual(250.94m, order.TotalCustomer.Value.Value);
            Assert.AreEqual(233m, order.SubtotalCustomer.Value.Value);

        }

        [TestMethod]
        public void OrderVATExcludedNonCH() {

            Config.ShopVATIncluded = false;
            Order order = SetupBasicOrderWithItems();
            order.ShippingAddress = new Address();
            order.ShippingAddress.CountryCode = "it";
            order.UpdateOrder();

            Assert.AreEqual(0m, order.VATCustomer.Value.Value);
            Assert.AreEqual(233m, order.TotalCustomer.Value.Value);
            Assert.AreEqual(233m, order.SubtotalCustomer.Value.Value);

        }


        [TestMethod]
        public void OrderVATIncludedCH() {

            Config.ShopVATIncluded = true;
            Order order = SetupBasicOrderWithItems();
            order.ShippingAddress = new Address();
            order.ShippingAddress.CountryCode = "ch";
            order.UpdateOrder();

            Assert.AreEqual(16.66m, order.VATCustomer.Value.Value);
            Assert.AreEqual(233m, order.TotalCustomer.Value.Value);
            Assert.AreEqual(233m, order.SubtotalCustomer.Value.Value);

        }

        [TestMethod]
        public void OrderVATIncludedNonCH() {

            Config.ShopVATIncluded = true;
            Order order = SetupBasicOrderWithItems();
            order.ShippingAddress = new Address();
            order.ShippingAddress.CountryCode = "it";
            order.UpdateOrder();

            Assert.AreEqual(0m, order.VATCustomer.Value.Value);
            Assert.AreEqual(233m, order.TotalCustomer.Value.Value);
            Assert.AreEqual(233m, order.SubtotalCustomer.Value.Value);

        }


    }


}
