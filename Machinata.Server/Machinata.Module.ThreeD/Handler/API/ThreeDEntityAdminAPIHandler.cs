using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDEntityAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.ThreeD.Model.ThreeDEntity> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();
                     
            var entity = this.DB.Set<ThreeDEntity>()
                .Include(nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDEntity.Material))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT)
                .Include(nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDEntity.Material));

            entity.Populate(this, form);
            entity.UpdateProperties();

            // Post Populate
            this.EditPopulate(entity);
            entity.Validate();

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Entity = new { entity.PublicId, entity.Path } });
        }

        protected override void EditPopulate(ThreeDEntity entity) {

            entity.Include(nameof(entity.Scene));
            entity.Scene.Include(nameof(entity.Scene.Entities));

            entity.Scene.CheckUniqueId(entity);
        }


        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/select-asset/{toggleId}")]
        public void ToggleCourse(string publicId, string toggleId) {
            this.CRUDToggleSingleSelection<ThreeDAsset>(publicId, toggleId, nameof(ThreeDEntity.Asset));
        }


       

    }
}