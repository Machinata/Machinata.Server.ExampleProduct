using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;
using Machinata.Core.Templates;
using Machinata.Module.ThreeD.Templates;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDSceneAPIHandler : APIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/threed/scenes/scene/{publicId}", AccessPolicy.PUBLIC_ARN)]
        public void Scene(string publicId) {

            var scene = this.DB.ThreeDScenes()
                 .Include(nameof(ThreeDScene.Assets))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Material))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children)).GetByPublicId(publicId);


            var template = PageTemplateSceneInserts.LoadSceneTemplate(this, scene);

            this.SendAPIMessage("success", new { Scene = new { PublicId = scene.PublicId, Content = template.Content } });




        }



      


      
    }
}