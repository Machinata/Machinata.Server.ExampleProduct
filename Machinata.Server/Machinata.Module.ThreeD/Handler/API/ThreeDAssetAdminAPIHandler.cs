using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDAssetAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.ThreeD.Model.ThreeDAsset> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/admin/threed/assets/asset/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/threed/assets/asset/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        protected override void EditPopulate(ThreeDAsset asset) {

            asset.Include(nameof(asset.Scene));
            asset.Scene.Include(nameof(asset.Scene.Assets));

            asset.Scene.CheckUniqueId(asset);
        }


    }
}