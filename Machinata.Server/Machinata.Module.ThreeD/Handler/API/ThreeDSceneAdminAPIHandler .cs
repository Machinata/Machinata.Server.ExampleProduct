using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDSceneAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.ThreeD.Model.ThreeDScene> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/admin/threed/scenes/create")]
        public void Create() {
            CRUDCreate("scene");
        }

        protected override void CreatePopulate(ThreeDScene entity) {
            entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Title);

            //entity.Include(nameof(ThreeDScene.Entities));
            entity.Entities = new List<ThreeDEntity>();

            // Create Root Entity
            var rootEntity = new ThreeDEntity();
            rootEntity.EntityId = "root";
            rootEntity.Scene = entity;
            rootEntity.Type = ThreeDEntity.EntityTypes.Default;
            rootEntity.Enabled = true;
            rootEntity.Visible = true;
            rootEntity.Selectable = false;
            entity.Entities.Add(rootEntity);

            // Calibrator
            var calibrator = new ThreeDEntity();
            calibrator.EntityId = "calibrator";
            calibrator.Type = ThreeDEntity.EntityTypes.Calibrator;
            calibrator.Visible = true;
            calibrator.Selectable = false;
            calibrator.Enabled = true;
            calibrator.UpdateProperties();
            //calibrator.Position = "";
            //this.DB.ThreeDEntities().Add(calibrator);
            entity.Entities.Add(calibrator);

            // Camera
            var camera = new ThreeDEntity();
            camera.EntityId = "camera";
            camera.Type = ThreeDEntity.EntityTypes.Camera;
            camera.Enabled = true;
            camera.Visible = true;
            camera.Selectable = false;
            camera.UpdateProperties();
            camera.Position = "6 6 6";
            entity.Entities.Add(camera);

            // Sky
            var sky = new ThreeDEntity();
            sky.EntityId = "sky";
            sky.Type = ThreeDEntity.EntityTypes.Sky;
            sky.Visible = true;
            sky.Enabled = true;
            sky.Selectable = false;
            sky.Color = "gray";
            sky.UpdateProperties();
            // this.DB.ThreeDEntities().Add(sky);
            entity.Entities.Add(sky);
        }

        [RequestHandler("/api/admin/threed/scenes/scene/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/threed/scenes/scene/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/threed/scenes/scene/{publicId}/create-entity")]
        public void CreateEntity(string publicId) {
            this.RequireWriteARN();
            var scene = this.DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Entities))
                .GetByPublicId(publicId);

            ThreeD.Model.ThreeDEntity parent = null;
            if(this.Params.String("parent") != null) parent = this.DB.ThreeDEntities()
                .Include(nameof(ThreeDEntity.Children))
                .Include(nameof(ThreeDEntity.Scene) + "." + nameof(ThreeDScene.Entities))
                .GetByPublicId(this.Params.String("parent"));


            var entity = new ThreeDEntity();
            entity.Visible = true;
            entity.Enabled = true;
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));

            this.DB.ThreeDEntities().Add(entity);

            // if no id set generate public id and use this
            if (string.IsNullOrEmpty(entity.EntityId) == true) {
                this.DB.SaveChanges();
                entity.EntityId = entity.PublicId;
            }

            scene.CheckUniqueId(entity);

            entity.UpdateProperties();

            entity.Scene = scene;
            scene.Entities.Add(entity);
            entity.Parent = parent;


            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Entity = new { PublicId = entity.PublicId, Path = entity.Path } });
        }
        
        

        [RequestHandler("/api/admin/threed/scenes/scene/{sceneId}/create-asset")]
        public void CreateAsset(string sceneId) {
            this.RequireWriteARN();

            var scene = this.DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Assets))
                .GetByPublicId(sceneId);

            var entity = new ThreeDAsset();
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));

            // Add to DB
            this.DB.ThreeDAssets().Add(entity);

            // if no id set generate public id and use this
            if (string.IsNullOrEmpty(entity.AssetId) == true) {
                this.DB.SaveChanges();
                entity.AssetId = entity.PublicId;
            }

            // Check Id
            scene.CheckUniqueId(entity);

            entity.Scene = scene;
            scene.Assets.Add(entity);
         
            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Asset = new { PublicId = entity.PublicId } });
        }


        [RequestHandler("/api/admin/threed/scenes/scene/{sceneId}/create-material")]
        public void CreateMaterial(string sceneId) {
            var scene = this.DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Materials))
                .GetByPublicId(sceneId);

            var entity = new ThreeDMaterial();
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));

            this.DB.ThreeDMaterials().Add(entity);

            // if no id set generate public id and use this
            if (string.IsNullOrEmpty(entity.MaterialId) == true) {
                this.DB.SaveChanges();
                entity.MaterialId = entity.PublicId;
            }

            scene.CheckUniqueId(entity);

            entity.Scene = scene;
            scene.Materials.Add(entity);


            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Material = new { PublicId = entity.PublicId } });
        }



        [RequestHandler("/api/admin/threed/scene/import/{name}")]
        public void SceneImport(string name) {
            this.RequireWriteARN();

            var data = this.Request.ReadFileFromRequest(".json");
            var json = Core.JSON.ParseJsonAsJObject(data);

            var form = new FormBuilder(Forms.Frontend.JSON);
            // Save using json
            var scene = ThreeDScene.ImportFromJSON(this.DB, json, name, form, this.User);


            this.DB.ThreeDScenes().Add(scene);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("import-success");
        }


        [RequestHandler("/api/admin/threed/scenes/scene/{sceneId}/duplicate")]
        public void Duplicate(string sceneId) {

            var name = this.Params.String("name");

            var scene = this.DB.ThreeDScenes()
               // if load error this might be problemr: https://stackoverflow.com/questions/44496598/entity-framework-string-was-not-recognized-as-a-valid-boolean               .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
               .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Material))
               .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Parent))
               .Include(nameof(ThreeDScene.Assets))
               .Include(nameof(ThreeDScene.Materials) + "." + nameof(ThreeDMaterial.Source))
               .GetByPublicId(sceneId);


            var newScene = new ThreeDScene();
            newScene.Title = name;
            newScene.ShortURL = Core.Util.String.CreateShortURLForName(name);
            newScene.Category = scene.Category;

            var copyForm = new FormBuilder(Forms.Frontend.JSON);

            // Assets // TODO COPY FILES???????
            newScene.Assets = new List<ThreeDAsset>();
            foreach (var asset in scene.Assets) {
                var newAsset = new ThreeDAsset();
                asset.CopyValuesTo(newAsset, copyForm);
                newScene.Assets.Add(newAsset);
            }

            // Materials
            newScene.Materials = new List<ThreeDMaterial>();
            foreach (var material in scene.Materials) {
                var newMaterial = new ThreeDMaterial();
                material.CopyValuesTo(newMaterial, copyForm);
                newScene.Materials.Add(newMaterial);

                if (material.Source != null) {
                    newMaterial.Source = newScene.Assets.FirstOrDefault(a => a.AssetId == material.Source.AssetId);
                }
            }


            // Entities 
            newScene.Entities = new List<ThreeDEntity>();
            foreach (var entity in scene.Entities.ToList()) {
                var newEntity = new ThreeDEntity();
                entity.CopyValuesTo(newEntity, copyForm);
                newScene.Entities.Add(newEntity);

                // Material
                if (entity.Material != null) {
                    newEntity.Material = newScene.Materials.FirstOrDefault(a => a.MaterialId == entity.Material.MaterialId);
                }

                // Asset
                if (entity.Asset != null) {
                    newEntity.Asset = newScene.Assets.FirstOrDefault(a => a.AssetId == entity.Asset.AssetId);
                }

                // Parent
                if (entity.ParentId != null) {
                    newEntity.ParentEntityId = newScene.Entities.FirstOrDefault(a => a.EntityId == entity.Parent.EntityId)?.EntityId;
                }
            }


            // Set Parents
            foreach(var entity in newScene.Entities.ToList()) {
                var parenEntity = newScene.Entities.FirstOrDefault(e => e.EntityId == entity.ParentEntityId);
                if (parenEntity!= null) {
                    entity.Parent = parenEntity;
                }
            }

            // Save
            this.DB.ThreeDScenes().Add(newScene);
            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Scene = new { PublicId = newScene.PublicId } });
        }


        [RequestHandler("/api/admin/threed/scenes/scene/{publicId}/entities/{parentId}/sort-children")]
        public void SortContentNodes(string publicId, string parentId) {
            this.RequireWriteARN();


            var scene = this.DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Entities))
                .GetByPublicId(publicId);



            IQueryable<ThreeDEntity> entities = null;
            // Init
            if (parentId == "root") {
                entities = scene.Entities.Where(e => e.IsRoot).AsQueryable();
            } else {
                var parentEntity = scene.Entities.AsQueryable().GetByPublicId(parentId);
                entities = parentEntity.Children.AsQueryable();
            }

            var sorts = Params.StringArray("order", new string[] { }).ToList();

            string error = null;

            if (sorts.Count() == entities.Count()) {
                foreach (var entity in entities) {
                    if (sorts.Contains(entity.PublicId)) {
                        entity.Sort = sorts.IndexOf(entity.PublicId);
                    } else {
                        error = "Entity not found in Sort Order";
                    }
                }
            } else {
                error = "Wrong number of elements provided";
            }

            if (error != null) {
                throw new BackendException("sort-error", error);
            }

            // Save
            this.DB.SaveChanges();
            // Return
            this.SendAPIMessage("success", new { Entity = new { PublicId = publicId } });
        }


    }
}