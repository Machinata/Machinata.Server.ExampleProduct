using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDMaterialAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.ThreeD.Model.ThreeDMaterial> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/admin/threed/materials/material/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

 
        [RequestHandler("/api/admin/threed/materials/material/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Set<ThreeDMaterial>()
                .Include(nameof(ThreeDMaterial.Source))
                .Include(nameof(ThreeDMaterial.Scene) + "." + nameof(ThreeDScene.Materials))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT)
                .Include(nameof(ThreeDMaterial.Source));
              

            entity.Populate(this, form);


            entity.Scene.CheckUniqueId(entity);
            
            entity.Validate();

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Material = new { entity.PublicId } });
        }


    }
}