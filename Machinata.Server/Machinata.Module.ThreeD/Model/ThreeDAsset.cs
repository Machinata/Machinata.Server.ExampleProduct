using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using System.IO;
using Machinata.Core.Data;
using Machinata.Core.Util;

namespace Machinata.Module.ThreeD.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("ThreeDAssets")]
    public partial class ThreeDAsset : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums

        public enum AssetTypes : short {
            Mesh = 1000,
            Texture = 2000
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ThreeDAsset() {
           
            
        }



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string AssetId { get; set; }
  

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [DataType(DataType.ImageUrl)]
        public string File { get; set; }
        

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Required]
        public AssetTypes Type { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public ThreeDScene Scene { get; set; }



        #endregion

        #region Public Methods  /////////////////////////////////////////




        public JObject GetJObject(FormBuilder form = null) {
            var ret = this.GetJSON(form);

            // File?
            if (string.IsNullOrEmpty(this.File) == false) {
                var handler = Core.Handler.Handler.CreateForRoute<Core.Handler.ContentFileHandler>(this.File, null, Core.Handler.Verbs.Get);
                handler.Invoke();

                // Read bytes and convert
                var bytes = System.IO.File.ReadAllBytes(handler.FileOutput);
                var binaryData = Convert.ToBase64String(bytes);
                ret.Add(new JProperty("file-data", binaryData));
            }
            return ret;

        }

        public static ThreeDAsset ImportFromJSON(ModelContext dB, JToken json, FormBuilder form, User user) {
            var asset = Core.JSON.ToObject<ThreeDAsset>((JObject)json);
            var binaryValue = json["file-data"];
            if (binaryValue != null && asset.File != null) {
                var bytes = Convert.FromBase64String(binaryValue.ToString());
                var fileName = Path.GetFileName(asset.File);
                var contentFile = DataCenter.UploadFile(fileName, bytes, "threed", null, user);
                dB.ContentFiles().Add(contentFile);
                dB.SaveChanges();
                asset.File = contentFile.ContentURL;

            }
            return asset;
        }

        #endregion

            #region Model Creation ////////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            this.AssetId = Core.Util.String.CreateShortURLForName(this.AssetId, true, true);

            if (this.File != null) {
                if(this.File.EndsWith(".obj")) {
                    this.Type = AssetTypes.Mesh;
                }
                if(this.File.EndsWith(".jpg") || this.File.EndsWith(".png")) {
                    this.Type = AssetTypes.Texture;
                }
            }
        }
        #endregion

        #region Virtual Methods ////////////////////////////////////////////////////////////////////


        public override string ToString() {
            return this.AssetId;
        }





        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextThreeDAssetExtenions {
        public static DbSet<ThreeDAsset> ThreeDAssets(this Core.Model.ModelContext context) {
            return context.Set<ThreeDAsset>();
        }
    }

    #endregion
}
