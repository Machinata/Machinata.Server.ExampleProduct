using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using System.IO;

namespace Machinata.Module.ThreeD.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("ThreeDMaterials")]
    public partial class ThreeDMaterial : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums

        public enum MaterialTypes : short {
            PBR = 1000,
            Flat = 2000
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ThreeDMaterial() {
            this.Parameters = new Properties();
            this.Type = MaterialTypes.PBR;
        }

        #endregion


        #region Constants /////////////////////////////////////////////////////////////////////////
        
        public const string PARAMETERS_ROUGHNESS_KEY = "Roughness";
        public const string PARAMETERS_METALNESS_KEY = "Metalness";
        public const string PARAMETERS_BLENDING_KEY = "Blending";
        public const string PARAMETERS_OPACITY_KEY = "Opacity";
        public const string PARAMETERS_SHADER_KEY = "Shader";
        public const string PARAMETERS_SIDE_KEY = "Side";
        public const string PARAMETERS_TRANSPARENT_KEY = "Transparent";
        public const string PARAMETERS_EMISSIVE_KEY = "Emissive";
        public const string PARAMETERS_FOG_KEY = "Fog";
        public const string PARAMETERS_REPEAT_KEY = "Repeat";
        public const string PARAMETERS_ENVMAP_KEY = "EnvMap";
        public const string PARAMETERS_SPHERICALENVMAP_KEY = "SphericalEnvMap";
        public const string PARAMETERS_WIREFRAME_KEY = "Wireframe";
        public const string PARAMETERS_WIREFRAME_LINE_WIDTH_KEY = "WireframeLineWidth";

        #endregion



        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.IMPORT)]
        public string MaterialId { get; set; }
  

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Color { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Required]
        public MaterialTypes Type { get; set; }
        

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public Properties Parameters { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public ThreeDScene Scene { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public ThreeDAsset Source { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////





        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string GetPropString() {
            var materialProps = new List<string>();
                
            // See https://aframe.io/docs/0.9.0/components/material.html#properties
            // Exaple: color: #EEEEEE; roughness: 0.3; metalness: 1.0; sphericalEnvMap: #skymap;
            
            // Common material props
            if (this.Source != null) materialProps.Add("src: " + "#"+ this.Source.AssetId);
            if (!string.IsNullOrEmpty(this.Color)) materialProps.Add("color: " + this.Color);
            if (this.Parameters.Keys.Contains(PARAMETERS_TRANSPARENT_KEY)) materialProps.Add("transparent: " + this.Parameters[PARAMETERS_TRANSPARENT_KEY]);
            if (this.Parameters.Keys.Contains(PARAMETERS_SIDE_KEY)) materialProps.Add("side: " + this.Parameters[PARAMETERS_SIDE_KEY]);
            if (this.Parameters.Keys.Contains(PARAMETERS_SHADER_KEY)) materialProps.Add("shader: " + this.Parameters[PARAMETERS_SHADER_KEY]);
            if (this.Parameters.Keys.Contains(PARAMETERS_REPEAT_KEY)) materialProps.Add("repeat: " + this.Parameters[PARAMETERS_REPEAT_KEY]);
            if (this.Parameters.Keys.Contains(PARAMETERS_BLENDING_KEY)) materialProps.Add("blending: " + this.Parameters[PARAMETERS_BLENDING_KEY]);

            // PBR Only
            if (this.Type == Model.ThreeDMaterial.MaterialTypes.PBR) {
                if (this.Parameters.Keys.Contains(PARAMETERS_EMISSIVE_KEY)) materialProps.Add("emmisive: " + this.Parameters[PARAMETERS_EMISSIVE_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_FOG_KEY)) materialProps.Add("fog: " + this.Parameters[PARAMETERS_FOG_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_ENVMAP_KEY)) materialProps.Add("envMap: " + this.Parameters[PARAMETERS_ENVMAP_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_SPHERICALENVMAP_KEY)) materialProps.Add("sphericalEnvMap: " + this.Parameters[PARAMETERS_SPHERICALENVMAP_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_WIREFRAME_KEY)) materialProps.Add("wireframe: " + this.Parameters[PARAMETERS_WIREFRAME_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_WIREFRAME_LINE_WIDTH_KEY)) materialProps.Add("wireframeLinewidth: " + this.Parameters[PARAMETERS_WIREFRAME_LINE_WIDTH_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_ROUGHNESS_KEY)) materialProps.Add("roughness: " + this.Parameters[PARAMETERS_ROUGHNESS_KEY]);
                if (this.Parameters.Keys.Contains(PARAMETERS_METALNESS_KEY)) materialProps.Add("metalness: " + this.Parameters[PARAMETERS_METALNESS_KEY]);

            }

            materialProps.RemoveAll(e => e.EndsWith(": "));

            return string.Join("; ", materialProps);
        }

        public static ThreeDMaterial ImportFromJSON(ModelContext dB, JToken json, FormBuilder form,  IEnumerable<ThreeDAsset> allAssets) {
            var material = Core.JSON.ToObject<ThreeDMaterial>((JObject)json);


            var sourceId = json["sourceId"]?.ToString();
            if (string.IsNullOrEmpty(sourceId) == false) {
                var source = allAssets.FirstOrDefault(a => a.AssetId == sourceId);
                material.Source = source;
            }

            if (material.Parameters == null) {
                material.Parameters = new Properties();
            }

            return material;
        }

        public JObject GetJObject(FormBuilder form = null) {
            var ret = this.GetJSON(form);
            ret["sourceId"] = this.Source?.AssetId;
            return ret;
        }


        #endregion

        #region Virtual Methods ////////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            this.MaterialId = Core.Util.String.CreateShortURLForName(this.MaterialId, true, true);

            // Common
            if (!this.Parameters.Keys.Contains(PARAMETERS_SHADER_KEY)) this.Parameters[PARAMETERS_SHADER_KEY ] = null;
            if(!this.Parameters.Keys.Contains(PARAMETERS_SIDE_KEY)) this.Parameters[PARAMETERS_SIDE_KEY] = null;
            if(!this.Parameters.Keys.Contains(PARAMETERS_TRANSPARENT_KEY)) this.Parameters[PARAMETERS_TRANSPARENT_KEY ] = null;
            if(!this.Parameters.Keys.Contains(PARAMETERS_REPEAT_KEY)) this.Parameters[PARAMETERS_REPEAT_KEY ] = null; 
            if(!this.Parameters.Keys.Contains(PARAMETERS_OPACITY_KEY)) this.Parameters[PARAMETERS_OPACITY_KEY ] = null; 
            if(!this.Parameters.Keys.Contains(PARAMETERS_BLENDING_KEY)) this.Parameters[PARAMETERS_BLENDING_KEY] = null;

            // PBR Only
            if (this.Type == MaterialTypes.PBR) {
                if (!this.Parameters.Keys.Contains(PARAMETERS_ROUGHNESS_KEY)) this.Parameters[PARAMETERS_ROUGHNESS_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_METALNESS_KEY)) this.Parameters[PARAMETERS_METALNESS_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_FOG_KEY)) this.Parameters[PARAMETERS_FOG_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_ENVMAP_KEY)) this.Parameters[PARAMETERS_ENVMAP_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_SPHERICALENVMAP_KEY)) this.Parameters[PARAMETERS_SPHERICALENVMAP_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_WIREFRAME_KEY)) this.Parameters[PARAMETERS_WIREFRAME_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_WIREFRAME_LINE_WIDTH_KEY)) this.Parameters[PARAMETERS_WIREFRAME_LINE_WIDTH_KEY] = null;
                if (!this.Parameters.Keys.Contains(PARAMETERS_EMISSIVE_KEY)) this.Parameters[PARAMETERS_EMISSIVE_KEY] = null;
            }
        }

        public override string ToString() {
            return this.MaterialId;
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextThreeDMaterialExtenions {
        public static DbSet<ThreeDMaterial> ThreeDMaterials(this Core.Model.ModelContext context) {
            return context.Set<ThreeDMaterial>();
        }
    }

    #endregion
}
