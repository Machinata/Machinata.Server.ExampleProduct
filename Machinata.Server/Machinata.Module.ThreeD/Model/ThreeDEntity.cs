using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("ThreeDEntities")]
    public partial class ThreeDEntity : ModelObject, IEnabledModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums

        public enum EntityTypes : short {
            Default = 1000,
            Box = 2000,
            Cylinder = 3000,
            Plane = 4000,
            Sky = 5000,
            Light = 5100,
            Sphere = 6000,
            Mesh = 7000,
            Camera = 8000,
            Text = 9000,
            Calibrator = 10000

        }

        #endregion


        #region Constructors //////////////////////////////////////////////////////////////////////

        public ThreeDEntity() {
            this.Parameters = new Properties();
            this.Components = new Properties();
        }

        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string EntityId { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Title { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Required]
        public EntityTypes Type { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Visible { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Enabled { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Classes { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string MappingIds { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [DataType(DataType.MultilineText)]
        public string Script { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Color { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Rotation { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Opacity { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Position { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Scale { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string LookAt { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Selectable { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public Properties Parameters { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public Properties Components { get; set; }

        [Column]
        public int? ParentId { get; set; }
        
        [Column]
        public int? Sort { get; set; }
       

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public ThreeDScene Scene { get; set; }

        [Column]
        [ForeignKey("ParentId")]
        public ThreeDEntity Parent { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public ThreeDAsset Asset { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public ThreeDMaterial Material { get; set; }


        [Column]
        [InverseProperty("Parent")]
        public ICollection<ThreeDEntity> Children { get; set; }



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder]
        [NotMapped]
        public string Icon {
            get {
                if (this.Type == EntityTypes.Box) return "media-stop";
                else if (this.Type == EntityTypes.Calibrator) return "compass";
                else if (this.Type == EntityTypes.Camera) return "camera";
                else if (this.Type == EntityTypes.Cylinder) return "media-stop";
                else if (this.Type == EntityTypes.Light) return "lightbulb";
                else if (this.Type == EntityTypes.Mesh) return "arrow-sorted-up";
                else if (this.Type == EntityTypes.Plane) return "media-stop";
                else if (this.Type == EntityTypes.Sky) return "image";
                else if (this.Type == EntityTypes.Sphere) return "media-record";
                else if (this.Type == EntityTypes.Text) return "sort-alphabetically";
                return "folder-open"; // Default
            }
        }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string Path {
            get {
                return string.Join("/", this.NavigationList.Select(nl=>nl.EntityId));
            }
        }

        /// <summary>
        /// List of path parts to navigate to the entity
        /// </summary>
        [NotMapped]
        public IList<ThreeDEntity> NavigationList {
            get {
                if (this.ParentId == null) {
                    return new List<ThreeDEntity>() { this };
                } else {
                    this.Include(nameof(this.Parent));
                    var parentPaths = Parent.NavigationList.ToList();
                    parentPaths.Add(this);
                    return parentPaths;
                }
            }
        }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public bool IsRoot {
            get {
                return this.ParentId == null; ;
            }
        }

        /// <summary>
        /// Used for json import and duplicate
        /// </summary>
        [NotMapped]
        public string ParentEntityId { get; set; }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Create properties for specific type
        /// TODO: remove old?, TODO: all types
        /// </summary>
        public void UpdateProperties() {
            if (this.Type == EntityTypes.Box) {
                if (this.Parameters.Keys.Contains("Height") == false) {
                    this.Parameters["Height"] = null;
                }
                if (this.Parameters.Keys.Contains("Width") == false) {
                    this.Parameters["Width"] = null;
                }
            } else if (this.Type == EntityTypes.Sphere) {
                if (this.Parameters.Keys.Contains("Radius") == false) {
                    this.Parameters["Radius"] = null;
                }
                if (this.Parameters.Keys.Contains("Height") == false) {
                    this.Parameters["Height"] = null;
                }
                if (this.Parameters.Keys.Contains("Width") == false) {
                    this.Parameters["Width"] = null;
                }
            } else if (this.Type == EntityTypes.Text) {
                if (this.Parameters.Keys.Contains("Text") == false) {
                    this.Parameters["Text"] = "";
                }
                if (this.Parameters.Keys.Contains("Anchor") == false) {
                    this.Parameters["Anchor"] = "center";
                }
                if (this.Parameters.Keys.Contains("Align") == false) {
                    this.Parameters["Align"] = "center";
                }
                if (this.Parameters.Keys.Contains("Width") == false) {
                    this.Parameters["Width"] = null;
                }
                if (this.Parameters.Keys.Contains("Height") == false) {
                    this.Parameters["Height"] = null;
                }
                if (this.Parameters.Keys.Contains("LineHeight") == false) {
                    this.Parameters["LineHeight"] = null;
                }
                if (this.Parameters.Keys.Contains("Font") == false) {
                    this.Parameters["Font"] = null;
                }
            }  else if (this.Type == EntityTypes.Light) {
                if (this.Parameters.Keys.Contains("Angle") == false) {
                    this.Parameters["Angle"] = null;
                }
                if (this.Parameters.Keys.Contains("Decay") == false) {
                    this.Parameters["Decay"] = null;
                }
                if (this.Parameters.Keys.Contains("LightType") == false) {
                    this.Parameters["LightType"] = "directional";
                }
                if (this.Parameters.Keys.Contains("GroundColor") == false) {
                    this.Parameters["GroundColor"] = null;
                }
                if (this.Parameters.Keys.Contains("Intensity") == false) {
                    this.Parameters["Intensity"] = null;
                }
                if (this.Parameters.Keys.Contains("Target") == false) {
                    this.Parameters["Target"] = null;
                }
            } else if (this.Type == EntityTypes.Camera) {
                if (this.Parameters.Keys.Contains("FOV") == false) {
                    this.Parameters["FOV"] = 20;
                }
                if (this.Parameters.Keys.Contains("MaxPolarAngle") == false) {
                    this.Parameters["MaxPolarAngle"] = 180;
                }
                if (this.Parameters.Keys.Contains("Target") == false) {
                    this.Parameters["Target"] = "0 0 0";
                }
                if (this.Parameters.Keys.Contains("MinDistance") == false) {
                    this.Parameters["MinDistance"] = 1;
                }
                if (this.Parameters.Keys.Contains("MaxDistance") == false) {
                    this.Parameters["MaxDistance"] = 100;
                }
                if (this.Parameters.Keys.Contains("AutoRotateEnabled") == false) {
                    this.Parameters["AutoRotateEnabled"] = "true";
                }
                if (this.Parameters.Keys.Contains("AutoRotateSpeed") == false) {
                    this.Parameters["AutoRotateSpeed"] = 0.2;
                }
                if (this.Parameters.Keys.Contains("EnablePan") == false) {
                    this.Parameters["EnablePan"] = "false";
                }
                if (this.Parameters.Keys.Contains("EnableKeys") == false) {
                    this.Parameters["EnableKeys"] = "false";
                }
                if (this.Parameters.Keys.Contains("Orthographic") == false) {
                    this.Parameters["Orthographic"] = "false";
                }
                if (this.Parameters.Keys.Contains("EnableZoom") == false) {
                    this.Parameters["EnableZoom"] = "true";
                }
                if (this.Parameters.Keys.Contains("ZoomSpeed") == false) {
                    this.Parameters["ZoomSpeed"] = "1";
                }
            }
        }



        public JObject GetJObject(FormBuilder form = null) {
            var ret = this.GetJSON(form);
            ret["assetId"] = this.Asset?.AssetId;
            ret["materialId"] = this.Material?.MaterialId;
            ret["parentId"] = this.Parent?.EntityId;
            return ret;
        }

        public static ThreeDEntity ImportFromJSON(ModelContext dB, JToken json, FormBuilder form, IEnumerable<ThreeDAsset> allAssets, IEnumerable<ThreeDMaterial> allMaterials) {
            var entity = Core.JSON.ToObject<ThreeDEntity>((JObject)json);

            {
                var assetId = json["assetId"]?.ToString();
                if (string.IsNullOrEmpty(assetId) == false) {
                    var asset = allAssets.FirstOrDefault(a => a.AssetId == assetId);
                    entity.Asset = asset;
                }
            }

            {
                var materialId = json["materialId"]?.ToString();
                if (string.IsNullOrEmpty(materialId) == false) {
                    var material = allMaterials.FirstOrDefault(a => a.MaterialId == materialId);
                    entity.Material = material;
                }
            }

            if (entity.Parameters == null) {
                entity.Parameters = new Properties();
            }

            if (entity.Components == null) {
                entity.Components = new Properties();
            }


            entity.ParentEntityId = json["parentId"]?.ToString();


            return entity;
        }




        #endregion

        #region Virtual Methods ////////////////////////////////////////////////////////////////////

        public override string ToString() {
            return this.EntityId;
        }

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            // Delete Children
            db.Set<ThreeDEntity>().RemoveRange(this.Children.ToList());

         
        }

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);
            this.EntityId = Core.Util.String.CreateShortURLForName(this.EntityId, true, true);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextThreeDEnttiesExtenions {
        public static DbSet<ThreeDEntity> ThreeDEntities(this Core.Model.ModelContext context) {
            return context.Set<ThreeDEntity>();
        }
    }

    #endregion
}
