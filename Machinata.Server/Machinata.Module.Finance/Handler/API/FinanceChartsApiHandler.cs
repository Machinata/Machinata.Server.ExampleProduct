using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Finance.Handler {
    
    public class FinanceChartsApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        [RequestHandler("/api/admin/finance/chart/donut/invoices/business-revenues")]
        public void DonutFinanceInvoicesBusinessRevenues() {
            
            var data = new DonutChartData();
            data.Title = "Top Businesses";
            //var businesses = this.DB.Businesses().Select(b => new { Name = b.Name, Revenue = this.DB.Orders().Where(o => o.Business == b).ToList().Sum(o => o.TotalCustomer.Cents.Value)/100.0 });
            //businesses = businesses.OrderByDescending(g => g.Revenue).Take(10);
            var businesses = this.DB.Businesses().Where(b => b.IsOwner == false).ToList();
            foreach(var b in businesses) {
                var rev = this.DB.Invoices().Where(o => o.Business.Id == b.Id).ToList().Sum(o => o.TotalCustomer.Value.Value);
                data.Items.Add(new DonutChartDataItem() { Name = b.Name, Title = b.Name, Value = (int)rev });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }
        

        [RequestHandler("/api/admin/finance/chart/timeseries/invoices/revenue-over-time")]
        public void TimeSeriesRevenueOverTime() {

            var data = new TimeSeriesChartData();
            data.Format = Core.Config.CurrencyFormat;
            data.Metric = Core.Config.CurrencyDefault;
            data.XAxisIsTime = false;
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                handler: this,
                entities: this.DB.Invoices(),
                entitiesForRange: TimeSeries.EntitiesForRangeOnCreated,
                valueForEntities: delegate (IQueryable<ModelObject> entities) {
                    // todo why toList necessary?
                    return (double)entities.Select(e=>e as Invoice).ToList().Sum(e =>  e.TotalCustomer.Value.Value);
                },
                title: "Invoice Revenue"
            ));
           
            SendAPIMessage("chart-data", data);
        }

        
        [RequestHandler("/api/admin/finance/chart/timeseries/invoices/revenue-over-time/business/{publicId}")]
        public void TimeSeriesRevenueOverTimeForBusiness(string publicId) {

            var entity = this.DB.Businesses().GetByPublicId(publicId);
            var data = new TimeSeriesChartData();
            data.Format = Core.Config.CurrencyFormat;
            data.Metric = Core.Config.CurrencyDefault;
            data.XAxisIsTime = false;
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                handler: this,
                entities: this.DB.Invoices(),
                entitiesForRange: TimeSeries.EntitiesForRangeOnCreated,
                valueForEntities: delegate (IQueryable<ModelObject> entities) {
                    // todo why toList necessary?
                    return (double)entities.Select(e=>e as Invoice).Where(i => i.Business.Id == entity.Id).ToList().Sum(e =>  e.TotalCustomer.Value.Value);
                },
                title: entity.Name + " Revenue"
            ));
           
            SendAPIMessage("chart-data", data);
        }
        

        [RequestHandler("/api/admin/finance/chart/timeseries/invoices/business-revenues")]
        public void TimeSeriesBusinessRevenues() {

            var data = new TimeSeriesChartData();
            data.Format = Core.Config.CurrencyFormat;
            data.XAxisIsTime = false;
            foreach (var business in DB.Businesses().Where(b => b.IsOwner == false)) {
                data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                    handler: this,
                    entities: this.DB.Invoices(),
                    entitiesForRange: delegate (IQueryable<ModelObject> entities, DateTime start, DateTime end) {
                        return ((IQueryable<Invoice>)entities).Where(e => e.Created > start && e.Created < end).ToList().Where(e=>e.Business == business).AsQueryable();
                    },
                    valueForEntities: delegate (IQueryable<ModelObject> entities) {
                        // todo check status
                        return (double)entities.Select(e => e as Invoice).ToList().Sum(e => e.TotalCustomer.Value.Value);
                    },
                    title: business.Name
                ));
            }

            SendAPIMessage("chart-data", data);
        }


        [RequestHandler("/api/admin/finance/chart/donut/invoice/{publicId}/categories")]
        public void DonutInvoiceCategories(string publicId) {


            var invoice = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
                .GetByPublicId(publicId);
            

            var data = new DonutChartData();
            data.Title = "Project Costs";

            var subCategories = invoice.GetItemCategoryGrouped().FirstOrDefault();
            if (subCategories != null) {
                foreach (var subCategory in subCategories.SubCategories) {
                    data.Items.Add(new DonutChartDataItem() { Name = subCategory.Key, Title = subCategory.Key, Value = (int)subCategory.Sum(c => c.CustomerTotal.Value) });
                }
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }


    }
}
