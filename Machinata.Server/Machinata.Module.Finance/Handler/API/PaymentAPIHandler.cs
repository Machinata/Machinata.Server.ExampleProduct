
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Finance.Handler {


    public class PaymentAPIHandler : APIHandler {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        [RequestHandler("/api/payment/confirm/{providerName}/{result}", AccessPolicy.PUBLIC_ARN)]
        public void PaymentResult(string providerName, string result) {

            var provider = Module.Finance.Payment.PaymentCenter.GetPaymentProvider(providerName);
            var paymentResult = provider.ConfirmPayment(this.DB, this.Context);
            var invoice = paymentResult.Invoice;

            this.DB.SaveChanges();

            this.SendAPIMessage("payment-result",  new { URL = paymentResult.RedirectPath } );

            if (!paymentResult.Success) {
                Core.EmailLogger.SendMessageToAdminEmail("Payment Error", "Machinata.Module.Finance", this.Context, this, paymentResult.GetException());
                _logger.Error(paymentResult.GetException(), "Payment Error: " + Core.EmailLogger.CompileFullErrorReport(this.Context, this, paymentResult.GetException()));
            }
           

        }

       
    }
}
