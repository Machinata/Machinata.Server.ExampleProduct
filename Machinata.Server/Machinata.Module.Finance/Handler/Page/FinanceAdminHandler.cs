
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Finance.Handler {


    public class FinanceAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("finance");
        }

        #endregion

        #region Menu Item
      
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "cash",
                Path = "/admin/finance",
                Title = "{text.finance}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/finance")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/finance/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("finance.menu-items", menuItems);
            // Navigation
            Navigation.Add("finance", "{text.finance}");
        }
    }
}
