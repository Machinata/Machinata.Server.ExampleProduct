
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Finance.Handler {


    public class BusinessFinanceAdminHandler : AdminPageTemplateHandler {
        

        [RequestHandler("/admin/finance/businesses")]
        public void Businesses() {
            var entities = this.DB.ActiveBusinesses().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/finance/businesses/business/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("businesses", "{text.businesses}");
        }


        [RequestHandler("/admin/finance/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {
            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);
          
            // Invoices
            var orders = DB.Invoices().Include("Business").Where(o => o.Business.Id == entity.Id);
            this.Template.InsertEntityList("entity.invoices", orders, new FormBuilder(Forms.Admin.LISTING), "/admin/finance/invoices/invoice/{entity.public-id}", true);


            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("businesses", "{text.businesses}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
        }
        
        

    }
}
