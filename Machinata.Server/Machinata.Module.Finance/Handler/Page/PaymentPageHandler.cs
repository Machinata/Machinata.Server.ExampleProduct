
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Payment;
using Machinata.Core.Messaging;

namespace Machinata.Module.Finance.Handler {


    /// <summary>
    /// Use this handler to
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.PageTemplateHandler" />
    public class PaymentPageHandler : PageTemplateHandler {


        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #region Handler Policies

        #endregion

        #region Handler Virtual Implementations

        public override string PackageName
        {
            get
            {
                return "Machinata.Module.Finance";
            }
        }

        public override bool AllowUndefinedTemplates
        {
            get
            {
                return true;
            }
        }


        #endregion

        [RequestHandler("/payment/{invoiceId}/{providerName}/{language}", AccessPolicy.PUBLIC_ARN)]
        public void Payment(string invoiceId, string providerName, string language) {
            var provider = PaymentCenter.GetPaymentProvider(providerName);
            var invoice = this.DB.Invoices().GetByPublicId(invoiceId);
            var code = provider.GetPaymentEmbedCode(invoice, language);

            this.Template.InsertVariableUnsafe("code", code);
        
        }


        [RequestHandler("/payment/confirm/{providerName}/{result}", AccessPolicy.PUBLIC_ARN)]
        public void PaymentConfirm(string providerName, string result) {

            var provider = Module.Finance.Payment.PaymentCenter.GetPaymentProvider(providerName);

            var paymentResult = provider.ConfirmPayment(this.DB, this.Context);
            if (paymentResult.RedirectPath != null) {
                this.Context.Response.Redirect(paymentResult.RedirectPath, false);
            }

            this.DB.SaveChanges();

            if (!paymentResult.Success) {

                // This is a known exception like unsuficcient funds
                if (Config.FinancePaymentIgnoreInternalPaymentErrorEmails == false || paymentResult.IsInternalError == false ) {
                    Core.EmailLogger.SendMessageToAdminEmail("Payment Error", "Machinata.Module.Finance", this.Context, this, paymentResult.GetException());
                } else {
                   // Ignore the exception: No email
                }
                _logger.Error(paymentResult.GetException(), "Payment Error: " + Core.EmailLogger.CompileFullErrorReport(this.Context, this, paymentResult.GetException()));
            }

        }
    }
}
