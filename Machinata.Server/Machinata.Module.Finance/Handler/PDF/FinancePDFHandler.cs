using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Module.Finance.Model;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Finance.Extensions;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Finance.Handler {


    public class FinancePDFHandler : PDFPageTemplateHandler {

        public const string FINANCE_ADMIN_ARN = "/admin/finance";

        [RequestHandler("/pdf/finance/invoice/{publicId}", AccessPolicy.PUBLIC_ARN)]
        public void ViewInvoice(string publicId) {
            // Init
            var entity = DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups))
                .Include(nameof(Invoice.Business))
                .Include(nameof(Invoice.User))
                .Include("LineItemGroups.LineItems")
                .GetByPublicId(publicId);


            // Check security
            var isAdmin = this.ValidateARN(FINANCE_ADMIN_ARN, false);
            if (isAdmin == false) {
                this.RequireAdminARNOrBusinessOrUserAssociation(FINANCE_ADMIN_ARN, entity.Business, entity.User);
            }
            
            // Invoice PDF VARS
            this.InsertInvoicePdfVariables(entity);
        }

        /// <summary>
        /// Public download url for invoices
        /// only works if 'FinanceInvoicePDFPublic' is enabled
        /// if a user is available on the invoice, the login is required
        /// </summary>
        /// <param name="hash">The hash.</param>
        /// <exception cref="Backend404Exception"></exception>
        [RequestHandler("/pdf/finance/invoice/get/{hash}", AccessPolicy.PUBLIC_ARN)]
        public void PublicInvoice(string hash) {

            this.Template.ChangeTemplate("invoice");

            // Init
            var entity = DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups))
                .Include(nameof(Invoice.Business))
                .Include(nameof(Invoice.User))
                .Include("LineItemGroups.LineItems")
                .GetByHash(hash);

            // Check public download enabled
            if (Config.FinanceInvoicePDFPublic == false) {
                throw new Backend404Exception();
            } else {
                // Require login if user available
                if (entity.User != null) {
                    this.RequireAdminARNOrBusinessOrUserAssociation(FINANCE_ADMIN_ARN, entity.Business, entity.User);
                }
            }

            // Invoice PDF VARS
            this.InsertInvoicePdfVariables(entity);

        }


    }
}
