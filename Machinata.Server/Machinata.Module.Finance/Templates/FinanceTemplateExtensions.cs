using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Finance.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public static class FinanceTemplateExtentions {

        public static void InsertInvoiceLineItems(this PageTemplate template, string variableName, Invoice entity, FormBuilder form = null) {


            // Insert all line item groups
            if (form == null) form = new FormBuilder(Forms.Admin.LISTING);//TODO: @dan
            var groupTemplates = new List<PageTemplate>();
            foreach (var group in entity.LineItemGroups) {
                var groupTemplate = template.LoadTemplate("invoice.line-items");
                groupTemplate.InsertVariables(variableName, group);
                groupTemplate.InsertEntityList(
                    variableName: variableName + ".items",
                    entities: group.LineItems.AsQueryable(),
                    form: form,
                    total: new FormBuilder(Forms.Admin.TOTAL).Custom("Description", null, null, "{text.subtotal}")
                );
                groupTemplates.Add(groupTemplate);
            }
            template.InsertTemplates(variableName, groupTemplates);


        }

        public static void InsertInvoiceSummary(this PageTemplate template, string variableName, Invoice entity, FormBuilder form = null) {
            if (form == null) form = new FormBuilder(Forms.Admin.INVOICE);//TODO: @dan
            // Insert line item summary
            template.InsertEntityList(
                variableName: variableName,
                entities: entity.LineItemGroups.AsQueryable(),
                form: form,
                total: new FormBuilder(Forms.Admin.TOTAL).Custom("Title",null,null,"{text.total}")
            );
        }

        /// <summary>
        /// Inserts an overview grouped by the category of the line items and then summarize by the subcategory
       /// </summary>
        /// <param name="template"></param>
        /// <param name="variableName"></param>
        /// <param name="entity"></param>
        /// <param name="form"></param>
        public static void InsertCategoryOverview(this PageTemplate template, string variableName, Invoice entity, FormBuilder form = null) {
            if (form == null) form = new FormBuilder(Forms.Admin.INVOICE);
            // Insert line item summary


           

            var categories = entity.GetItemCategoryGrouped();
            var groupTemplates = new List<PageTemplate>();

            foreach (var category in categories) {

                var groupTemplate = template.LoadTemplate("invoice.line-items");
                groupTemplate.InsertVariable("invoice.line-items.title", category.Category + " {text.overview}");


                var subCategoryItems = new List<OverviewLineItem>();
                foreach (var subCategory in category.SubCategories) {
                    var item = new OverviewLineItem();
                    item.LineItem = subCategory.Key;
                    item.CustomerTotal = subCategory.Sum(sc => sc.CustomerTotal);
                    item.Quantity = subCategory.Sum(sc => sc.Quantity);
                    subCategoryItems.Add(item);
                }

                groupTemplate.InsertEntityList(
                    variableName: "invoice.line-items.items",
                    entities: subCategoryItems,
                    form: form,
                    total: new FormBuilder(Forms.Admin.TOTAL)//.Custom("Description", null, null, "{text.subtotal}")
                );


                groupTemplates.Add(groupTemplate);

            }

            template.InsertTemplates(variableName, groupTemplates);

        }


        //public static void InsertSubCategoryOverview(this PageTemplate template, string variableName, Invoice entity, FormBuilder form = null) {
        //    if (form == null) form = new FormBuilder(Forms.Admin.INVOICE);
        //    // Insert line item summary

        //    IEnumerable<IGrouping<string, LineItem>> subCategories = entity.GetItemsSubCategoryGrouped();

        //    var groupTemplate = template.LoadTemplate("invoice.line-items");
        //    groupTemplate.InsertVariable("invoice.line-items.title", "{text.category-overview}");


        //    var subCategoryItems = new List<OverviewLineItem>();
        //    foreach (var subCategory in subCategories) {
        //        var item = new OverviewLineItem();
        //        item.LineItem = subCategory.Key;
        //        item.CustomerTotal = subCategory.Sum(sc => sc.CustomerTotal);
        //        item.Quantity = subCategory.Sum(sc => sc.Quantity);
        //        subCategoryItems.Add(item);
        //    }

        //    groupTemplate.InsertEntityList(
        //        variableName: "invoice.line-items.items",
        //        entities: subCategoryItems,
        //        form: form,
        //        total: new FormBuilder(Forms.Admin.TOTAL)//.Custom("Description", null, null, "{text.subtotal}")
        //    );


        //    template.InsertTemplate(variableName, groupTemplate);

        //}


        public static void InsertInvoiceTotal(this PageTemplate template, string variableName, Invoice entity, FormBuilder form = null) {
            if (form == null) form = new FormBuilder(Forms.Admin.LISTING); //TODO: @dan
            template.InsertEntityList(
                variableName: variableName,
                entities: entity.TotalAsLineItems(),
                form: form
            );
        }

        public static void InsertInvoicePaymentDetails(this PageTemplate template, string variableName, Invoice entity) {
            PageTemplate detailsTemplate = null;

         
            if (entity.PaymentMethod == Module.Finance.Payment.PaymentMethod.Invoice) {
                detailsTemplate = template.LoadTemplate("invoice.payment-details-invoice");
            } else if (entity.PaymentMethod == Module.Finance.Payment.PaymentMethod.CreditCard) {
                entity.Include(nameof(Invoice.Payments));
                detailsTemplate = template.LoadTemplate("invoice.payment-details-credit-card");
                if (entity.Payments.Count > 1) {
                    throw new NotImplementedException("Multiple payments not supported");
                }
                var payment = entity.Payments.FirstOrDefault();
                if (payment != null) {
                    detailsTemplate.InsertVariables("invoice.payment", payment);
                }
              

            } else {
                throw new NotImplementedException($"No template for this payment method found: {entity.PaymentMethod}");
            }

            

            detailsTemplate.InsertVariables("invoice.sender", entity.Sender, new FormBuilder(Forms.Admin.WILDCARD));
           
            template.InsertTemplate(variableName, detailsTemplate);
        }
        
        

    }
}
