using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance {
    public class Config {

       
        public static string MonthlyInvoiceSubject = Core.Config.GetStringSetting("MonthlyInvoiceSubject", "Monthly Invoice");

        public static string InvoiceNotificationSubject = Core.Config.GetStringSetting("InvoiceNotificationSubject", "Invoice");

        // Order Public Path
        public static string InvoicePublicPath = Core.Config.GetStringSetting("InvoicePublicPath", "/invoices/invoice");

        public static int FinanceInvoiceDueDateDays = Core.Config.GetIntSetting("FinanceInvoiceDueDateDays", 30);

        /// <summary>
        /// enable to make the invoice pdf downloadable for all who know the hash
        /// </summary>
        public static bool FinanceInvoicePDFPublic = Core.Config.GetBoolSetting("FinanceInvoicePDFPublic");


        /// <summary>
        /// Show QR Bill Code in pdf instead of scan code
        /// </summary>
        public static bool FinancePDFUseQRBill = Core.Config.GetBoolSetting("FinancePDFUseQRBill");



        /// <summary>
        /// Dont send emails for Internal Payment Errors (e.g. no Invoice)
        /// </summary>
        public static bool FinancePaymentIgnoreInternalPaymentErrorEmails = Core.Config.GetBoolSetting("FinancePaymentIgnoreInternalPaymentErrorEmails");


        /// <summary>
        /// Enables the feature to create Invoices from scratch
        /// </summary>
        public static bool FinanceAdminInvoiceCreation = Core.Config.GetBoolSetting("FinanceAdminInvoiceCreation", false);


        public static double FinanceDefaultVATRate = Core.Config.GetDoubleSetting("FinanceDefaultVATRate");



        // Datatrans
        public static class PaymentProvider {

            public static string FailurePage = Core.Config.GetStringSetting("PaymentFailurePage");
            public static string SuccessPage = Core.Config.GetStringSetting("PaymentSuccessPage");


            public static class Datatrans {
                public static string MerchantId = Core.Config.GetStringSetting("DatatransMerchantId");
                public static string SecretKey = Core.Config.GetStringSetting("DatatransSecretKey");
                public static string SecretKey2 = Core.Config.GetStringSetting("DatatransSecretKey2");                                                           
                public static string PaymentPage = Core.Config.GetStringSetting("DatatransPaymentPage");
                public static string Theme = Core.Config.GetStringSetting("DatatransTheme", "DT2015");
                public static IEnumerable<string> Methods = Core.Config.GetStringListSetting("DatatransPaymentMethods" );
            } 
            public static class Stripe {
                public static string SecretKey = Core.Config.GetStringSetting("StripeSecretKey");
                public static string PublicKey = Core.Config.GetStringSetting("StripePublicKey");
            }

            public static class PayPal {
                public static string AccessToken = Core.Config.GetStringSetting("PayPalAccessToken");
                public static string Environment = Core.Config.GetStringSetting("PayPalEnvironment");
              

            }

            public class Free {
                public static string PaymentPage = Core.Config.GetStringSetting("FreePaymentPage");
            }

            public static IEnumerable<string> PaymentProviders = Core.Config.GetStringListSetting("PaymentProviders");
        }

    }
}
