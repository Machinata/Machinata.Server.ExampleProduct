using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;

using Machinata.Module.Finance.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using Machinata.Module.Finance.Payment;

namespace Machinata.Module.Finance.Invoices {

    public class InvoiceGeneration {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        
        public static Invoice CreateInvoice(Business sender, 
                                            Address billingAddress,
                                            IEnumerable<IInvoiceable> invoiceables,
                                            PaymentMethod paymentMethod,
                                            DateTime? dueDate,
                                            User user,
                                            string currency,
                                            Business business = null) {

            // Create the invoice and setup details
            var invoice = new Invoice();
            invoice.User = user;
            invoice.Business = business;
            invoice.BillingAddress = billingAddress;
            invoice.Sender = sender;
            invoice.SenderAddress = sender.GetBillingAddressForBusiness();
            invoice.DueDate = dueDate;
            invoice.Status = Invoice.InvoiceStatus.Created;
            invoice.PaymentMethod = paymentMethod;

            // Prices /Currencies
            invoice.SubtotalCustomer = new Price(0, currency);
            invoice.TotalCustomer = new Price(0, currency);
            invoice.VATCustomer = new Price(0, currency);
            invoice.Currency = currency;

            // Title
            invoice.Title = string.Empty; // TODO additional subtitle

            // Add each order as a line item group
            foreach ( var invoiceable in invoiceables) {
                var orderGroups = invoiceable.GetLineItemGroups();
                foreach (var orderGroup in orderGroups) {
                    invoice.LineItemGroups.Add(orderGroup);
                }
            }

            // Update totals
            invoice.UpdateInvoice();
        
            // Register the invoice with each order...
            foreach (var invoiceable in invoiceables) {
                invoiceable.Invoice = invoice;
            }

            return invoice;
        }
        
    }
}
