using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Invoices {
    public interface IInvoiceable {

        List<Finance.Model.LineItemGroup> GetLineItemGroups();
        Invoice Invoice { get; set; }
    }
}
