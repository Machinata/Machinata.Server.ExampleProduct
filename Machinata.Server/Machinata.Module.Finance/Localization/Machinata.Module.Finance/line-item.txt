﻿line-item.de=Einzelposten

line-items.de=Einzelposten

line-item-group.title.de=Titel

line-item-group.description.de=Beschreibung

line-item-group.category.de=Kategorie

line-item.description.de=Beschreibung

line-item.customer-vat-rate.de=MwSt-Satz

line-item.customer-vat-rate.en=VAT Rate

line-item-group.quantity-total.pdf.en=Quantity Total
line-item-group.quantity-total.pdf.de=Anzahl Total

edit-line-items.en=Edit Line Items
edit-line-items.de=Edit Line Items

create-group.en=Create Group
create-group.de=Gruppe erstellen

finance.choose-category.en=Choose Category
finance.choose-category.de=Kategorie auswählen