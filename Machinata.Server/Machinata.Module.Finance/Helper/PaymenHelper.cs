using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Payment;
using Machinata.Module.Finance.Payment.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Helper {
    public static class PaymenHelper {

        public static void InsertPaymentDetails(this PageTemplate template, Invoice invoice, string variableName, FormBuilder form = null) {

            // Read from config  enabled providers
            var providers = PaymentCenter.GetEnabledPaymentProviders();

            // Just provide the free provider if no costs
            if (invoice.TotalCustomer.Value == 0) {
                providers = new List<IPaymentProvider>() { new FreePaymentProvider() };
            }

            // Insert payment links
            foreach(var provider in providers) {
                
                // Checkout by link
                var link = provider.GetPaymentLink(invoice);
                if (!string.IsNullOrWhiteSpace(link)) {
                    template.InsertVariableUnsafe(variableName + ".payment.links." + provider.Name.ToLower(), link);
                }

                // Checkout by embedded code
                var html = provider.GetPaymentEmbedCode(invoice, template.Language);
                if (!string.IsNullOrWhiteSpace(html)) {
                    template.InsertVariableUnsafe(variableName + ".payment.embed-code." + provider.Name.ToLower(), html);
                }

            }

            invoice.LoadLineItems();
            template.InsertInvoiceSummary(variableName + ".payment.summary", invoice, form);

        }
    }
}
