using Machinata.Core.Localization;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Module.Finance.Payment {
    
    public interface IPaymentProvider {


        /// <summary>
        /// Calls an external payment api and/or checks the result of the call and creates Payment.cs and sets Invoice and Order to paid if succesful 
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="context">The context.</param>
        /// <returns>PaymentResult</returns>
        PaymentResult ConfirmPayment(ModelContext db, HttpContext context);

        /// <summary>
        /// Gets the payment link.
        /// </summary>
        /// <param name="invoice">The invoice.</param>
        /// <returns>Payment link if supported, otherwise <null></null></returns>
        string GetPaymentLink(Invoice invoice);

        /// <summary>
        /// Gets the payment embed code.
        /// </summary>
        /// <param name="invoice">The invoice.</param>
        /// <param name="language">The language.</param>
        /// <returns>Embed code if supported, otherwisze <null></null></returns>
        string GetPaymentEmbedCode(Invoice invoice, string language);

        /// <summary>
        /// Gets the external transaction link to a payment providers dashboard link of a payment
        /// </summary>
        /// <param name="payment">The payment.</param>
        /// <returns>Link </returns>
        string GetExternalTransactionLink(Model.Payment payment);

        /// <summary>
        /// Gets a value indicating whether a can berefund live.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can refund live; otherwise, <c>false</c>.
        /// </value>
        bool CanRefundLive { get; }

        /// <summary>
        /// Refunds the specified payment of an invoice on a external payment provider api.
        /// </summary>
        /// <param name="invoice">The invoice.</param>
        /// <param name="payment">The payment.</param>
        /// <param name="actingUse">The acting use.</param>
        void Refund(Invoice invoice, Model.Payment payment, User actingUse);

        PaymentMethod GetPaymentMethod();
       
        string Name { get;}
    }

    public class PaymentResult {

        /// <summary>
        /// Path to which a 
        /// </summary>
        public string RedirectPath { get {

                if (this.Success) {
                    return Text.GetTranslatedTextById(Config.PaymentProvider.SuccessPage) + "?invoiceId=" + this.Invoice?.Hash;
                } else {
                    return
                       Text.GetTranslatedTextById(Config.PaymentProvider.FailurePage)
                           + "?invoiceId=" + this.Invoice?.Hash
                           + "&errorCode=" + this.ErrorCode
                           + "&errorMessage=" + this.ErrorMessage
                       ;
                }
            }
        }
        public Invoice Invoice { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
        public Exception Exception;

        /// <summary>
        /// Marks the error is not caused by the payment provider
        /// </summary>
        public bool IsInternalError { get; set; } = true;

        public bool Success { get; set; } = false;
       


        public Exception GetException() {
            if (this.Exception != null) {
                return this.Exception;
            }
            return new Exception($"{ErrorCode} - {ErrorMessage}");
        }
      
    }

   

}
