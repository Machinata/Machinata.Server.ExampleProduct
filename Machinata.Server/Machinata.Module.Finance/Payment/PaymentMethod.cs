using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Payment {
    
    public enum PaymentMethod : short {
        Invoice = 10,        // 
        CreditCard = 20      // 
    }
}
