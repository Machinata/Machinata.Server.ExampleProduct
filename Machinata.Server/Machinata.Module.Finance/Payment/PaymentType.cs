using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Payment {
    
    public enum PaymentTime : short {
        Before = 10,        // Pay Before Confirmation/Fulfilment
        After = 20          // Pay After Fulfilment
    }
    
}
