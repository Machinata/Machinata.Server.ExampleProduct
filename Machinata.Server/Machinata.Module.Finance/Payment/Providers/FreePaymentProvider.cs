using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using System.Web;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Core.Localization;

namespace Machinata.Module.Finance.Payment.Providers {
    public class FreePaymentProvider : IPaymentProvider {

        public string Name { get; } = "free";

        public bool CanRefundLive
        {
            get
            {
                return false;
            }
        }

        private static object _paymentLock = new object();

        public PaymentResult ConfirmPayment(ModelContext db, HttpContext context) {

            Invoice invoice = null;

            try {
                var invoiceNumber = context.Request["invoice"];
                invoice = db.Invoices().GetByPublicId(invoiceNumber);

                if (invoice.TotalCustomer.Value != 0) {
                    throw new BackendException("payment-error", "This is not a free order");
                }

                // Payment
                string transactionId = Guid.NewGuid().ToString().Replace("-", "");
                string payerId = Core.Util.HTTP.GetRemoteIP(context);

                // Dont pay if page is called twice
                // Todo: doesnt scale
                lock (_paymentLock) {
                    db.Entry(invoice).Reload();
                    if (invoice.Status < Invoice.InvoiceStatus.Paid) {
                        Model.Payment.CreateFullPaymentForInvoice(transactionId, invoice, this.Name, new Price(0, invoice.Currency), payerId);
                    }
                }

                // Redirect to success page
                return new PaymentResult() { Invoice = invoice, Success = true };

            }

            catch (Exception e) {
                return new PaymentResult() { Invoice = invoice, Exception = e };
            }

        }

        public Finance.Payment.PaymentMethod GetPaymentMethod() {
            return Finance.Payment.PaymentMethod.CreditCard;
        }

        public string GetPaymentLink(Invoice invoice) {
            var link = new StringBuilder();
            // link.Append(Text.GetTranslatedTextById(Config.PaymentProvider.Free.PaymentPage, Config.PaymentProvider.Free.PaymentPage));

            link.Append("/payment/confirm/free/result");
            link.Append("?invoice=" + invoice.PublicId);
            return link.ToString();
        }

        public string GetPaymentEmbedCode(Invoice invoice, string language) {
            return null;
        }

        public string GetExternalTransactionLink(Model.Payment paymente) {
            return null;
        }

        public void Refund(Invoice invoice, Model.Payment payment, User actingUse) {
            throw new NotImplementedException();
        }
    }
}
