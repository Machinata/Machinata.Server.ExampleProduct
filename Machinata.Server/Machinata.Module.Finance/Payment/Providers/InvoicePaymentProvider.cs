using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using System.Web;
using Machinata.Core.Templates;
using Machinata.Core.Model;

namespace Machinata.Module.Finance.Payment.Providers {
    public class InvoicePaymentProvider : IPaymentProvider {

        public string Name { get; } = "invoice";

        public PaymentResult ConfirmPayment(ModelContext db, HttpContext context) {
            return null;
        }

        public Finance.Payment.PaymentMethod GetPaymentMethod() {
            return Finance.Payment.PaymentMethod.Invoice;
        }

        public string GetPaymentLink(Invoice invoice) {
            throw new NotImplementedException();
        }

        public string GetPaymentEmbedCode(Invoice invoice, string language) {
            return null;
        }

        public string GetExternalTransactionLink(Model.Payment paymente) { return null; }

        public bool CanRefundLive { get { return false; }}
               

        public void Refund(Invoice invoice, Model.Payment payment, User actingUse) {
            throw new NotImplementedException();
        }
    }
}
