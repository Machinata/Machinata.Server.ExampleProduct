using Machinata.Module.Finance.Payment.Providers.CreditCardProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Finance.Model;
using Machinata.Core.Exceptions;
using System.Web;
using Machinata.Core.Templates;
using Machinata.Core.Model;

namespace Machinata.Module.Finance.Payment.Providers {

    public abstract class CreditCardPaymentProvider : IPaymentProvider {

        public abstract string Name { get; }

        public abstract PaymentResult ConfirmPayment(ModelContext db, HttpContext context);

        public PaymentMethod GetPaymentMethod() {
            return PaymentMethod.CreditCard;
        }

        public abstract string GetPaymentLink(Invoice invoice);

        public abstract string GetPaymentEmbedCode(Invoice invoice, string language);

        public abstract string GetExternalTransactionLink(Model.Payment payment);

        public virtual bool CanRefundLive { get; } = false;

        public virtual void Refund(Invoice invoice, Model.Payment payment, User actingUse) {
            throw new NotImplementedException();
        }

        public static CreditCardPaymentProvider DefaultProvider
        {
            get
            {
                return new DataTransPaymentProvider();
            }
        }
    }
}
