using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Finance.Model;
using System.Web;
using Machinata.Core.Exceptions;
using System.Security.Cryptography;
using System.Globalization;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Finance.Payment.Providers.CreditCardProviders {
    public class DataTransPaymentProvider : CreditCardPaymentProvider {
        public override string Name
        {
            get
            {
                return "DataTrans";
            }
        }

        /// <summary>
        /// Checks amount and signage from datatrans, creates new Payment
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        /// Payment wasn't authorized by the provider:  + errorMessage + , detail:  + errorDetail
        /// or
        /// Wrong signature
        public override PaymentResult ConfirmPayment(ModelContext db, HttpContext context) {

            Invoice invoice = null;
            try {
                var responseMesage = context.Request["responseMessage"];
                var uppTransactionId = context.Request["uppTransactionId"];
                var sign2 = context.Request["sign2"];
                var refno = context.Request["refno"];
                var errorCode = context.Request["errorCode"];
                var acqErrorCode = context.Request["acqErrorCode"];
                var errorMessage = context.Request["errorMessage"];
                var errorDetail = context.Request["errorDetail"];
                
                if (string.IsNullOrEmpty(refno)) {
                    var paymentResult = new PaymentResult();
                    paymentResult.ErrorMessage = "Missing information in request";
                    paymentResult.ErrorCode = "payment-error";
                    return paymentResult;
                }

                invoice = db.Invoices().GetByPublicId(refno);
               
                // Already paid
                if (invoice.Status == Invoice.InvoiceStatus.Paid) {
                    return new PaymentResult() { Invoice = invoice, Success = true}; 
                }


                if (responseMesage != "Authorized") {
                    var paymentResult = new PaymentResult();
                    paymentResult.ErrorMessage = "Payment wasn't authorized by the provider: " + errorMessage + ", detail: " + errorDetail;
                    paymentResult.ErrorCode = "not-authorized";
                    return paymentResult;
                }

                var expectedSign2 = CalculateSign(invoice.TotalCustomer.ToCents(), invoice.TotalCustomer.Currency, uppTransactionId, Config.PaymentProvider.Datatrans.SecretKey2);
                if (sign2 != expectedSign2) {
                    var paymentResult = new PaymentResult();
                    paymentResult.ErrorCode = "wrong-signature";
                    return paymentResult;
                }

                // confirm payment/order
                {
                    //TODO: @micha transaction id
                    var transactionFee = new Price(0, invoice.Currency);
                    var payerID = "TODO";
                    Model.Payment.CreateFullPaymentForInvoice(uppTransactionId, invoice, this.Name, transactionFee, payerID);
                    var paymentResult = new PaymentResult();
                    paymentResult.Invoice = invoice;
                    paymentResult.Success = true;
                    return paymentResult;
                }

            }
            catch (Exception e) {
                return new PaymentResult() { Invoice = invoice, Exception = e };
            }


        }

       

        /// <summary>
        /// Calculates the sign.
        /// https://admin.sandbox.datatrans.com/MenuDispatch.jsp?main=3&sub=3
        /// </summary>
        /// <param name="amountCents">The two.</param>
        /// <param name="currency">The three.</param>
        /// <param name="number">The four.</param>
        /// <returns></returns>
        private static string CalculateSign(int amountCents, string currency, string number, string secretKey) {
            var hmac = new HMACSHA256(Core.Encryption.EncryptionHelper.HexDecode(secretKey));
            var key = Config.PaymentProvider.Datatrans.MerchantId + amountCents + currency + number;
            var sign = hmac.ComputeHash(Encoding.UTF8.GetBytes(key));
            var signString = BitConverter.ToString(sign).Replace("-", "").ToLower();
            return signString;
        }



        public override string GetPaymentLink(Invoice invoice) {

            int total = invoice.TotalCustomer.ToCents();
            var sign = CalculateSign(total, invoice.TotalCustomer.Currency, invoice.PublicId, Config.PaymentProvider.Datatrans.SecretKey);

            var link = new StringBuilder();
            link.Append(Config.PaymentProvider.Datatrans.PaymentPage);
            link.Append("?sign=" + sign);
            link.Append("&merchantId=" + Config.PaymentProvider.Datatrans.MerchantId);
            link.Append("&amount=" + total);
            link.Append("&refno=" + invoice.PublicId);
            link.Append("&theme=" + Config.PaymentProvider.Datatrans.Theme);
            foreach (var paymentMethod in Config.PaymentProvider.Datatrans.Methods) {
                link.Append("&paymentmethod=" + paymentMethod);
            }
            link.Append("&currency=" + invoice.TotalCustomer.Currency);

            return link.ToString();

        }

        public override string GetPaymentEmbedCode(Invoice invoice, string language) {
            return null;
        }

        public override string GetExternalTransactionLink(Model.Payment payment) {
            return "todo";
        }
    }
}
