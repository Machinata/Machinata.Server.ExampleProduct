using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using System.Web;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Core.Localization;
using Braintree;

namespace Machinata.Module.Finance.Payment.Providers {
    public class PaypalPaymentProvider : IPaymentProvider {

        public string Name { get; } = "paypal";

        public bool CanRefundLive
        {
            get
            {
                return true;
            }
        }

        private static object _paymentLock = new object();



        // Sandbox aint working with wrong currencies (chf accounts don't work etither)
        // Paypal Sandbox account always mark as “Payment Review” or Pending when the currency of the goods you sale is different to the currency of the Sandbox Account, i made some developments for Mexican companies in MXN and the Sandbox account can’t configure as a mexican account so every time i made a test the payment was directly on Pending or Payment Review, but once online everything goes normal.
        // https://github.com/paypal/PayPal-Ruby-SDK/issues/9


        //https://developer.paypal.com/docs/accept-payments/express-checkout/ec-braintree-sdk/server-side/dotnet/
        public PaymentResult ConfirmPayment(ModelContext db, HttpContext context) {

            var paymentResult = new PaymentResult();
            
            try {

                var invoiceId = context.Request["invoice-id"];
                var nonce = context.Request["payment-method-nonce"];
                var customerId = context.Request["customer-id"];

                var invoice = db.Invoices()
                .Include(nameof(Invoice.BillingAddress))
                .GetByPublicId(invoiceId);

                paymentResult.Invoice = invoice;

                var address = invoice.BillingAddress;
                var gateway = GenerateGateway(Config.PaymentProvider.PayPal.AccessToken);



                var request = new TransactionRequest {
                    Amount = invoice.TotalCustomer.Value.Value,

                    MerchantAccountId = invoice.Currency,
                    PaymentMethodNonce = nonce,
                    //    OrderID = "Mapped to PayPal Invoice Number",
                    //Descriptor = new DescriptorRequest {
                    //    Name = invoice.SerialId
                    //},

                    BillingAddress = new AddressRequest {
                        FirstName = address.Name,
                        LastName = address.Name,
                        Company = address.Company,
                        StreetAddress = address.Address1,
                        ExtendedAddress = address.Address2,
                        Locality = address.City,
                        Region = address.StateCode,
                        PostalCode = address.ZIP,
                        CountryCodeAlpha2 = address.CountryCode
                    },

                    Options = new TransactionOptionsRequest {
                        PayPal = new TransactionOptionsPayPalRequest {
                            CustomField = "", //https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.paypal.custom_field
                            Description = "Invoice: " + invoice.SerialId,
                        },
                        SubmitForSettlement = true

                    }
                };

                Result<Transaction> result = gateway.Transaction.Sale(request);

                if (result.IsSuccess()) {
                    Price fee = new Price(result.Target.PayPalDetails.TransactionFeeAmount);
                    Model.Payment.CreateFullPaymentForInvoice(result.Target.Id, invoice, this.Name, fee, customerId);
                    paymentResult.Success = true;

                } else {
                    throw new BackendException("paypal-error", result.Message);
                }

            } catch (Exception e) {

                var backendExcpetion = e as BackendException;
                string code = "interal-error";
                string message = "Internal Error";
                if (backendExcpetion != null) {
                    code = backendExcpetion.Code;
                    message = backendExcpetion.Message;
                }
                paymentResult.ErrorCode = code;
                paymentResult.ErrorMessage = message;
                paymentResult.Exception = e;
            }

            return paymentResult;

        }

        public Finance.Payment.PaymentMethod GetPaymentMethod() {
            return Finance.Payment.PaymentMethod.CreditCard;
        }

        public string GetPaymentLink(Invoice invoice) {
            return null;
        }

        public string GetPaymentEmbedCode(Invoice invoice, string language) {

            invoice.Include(nameof(Invoice.BillingAddress));
            var template = new PageTemplate("Machinata.Module.Finance", null, "default/paypal");
            template.Language = language;
            template.InsertVariable("environment", Config.PaymentProvider.PayPal.Environment);
            template.InsertVariable("production-token", GenerateGateway( Config.PaymentProvider.PayPal.AccessToken).ClientToken.Generate());
            template.InsertVariable("sandbox-token", GenerateGateway(Config.PaymentProvider.PayPal.AccessToken).ClientToken.Generate());
            template.InsertVariable("invoice.total-customer", invoice.TotalCustomer.Value);
            template.InsertVariable("customerId", invoice.BillingAddress.Email);
            template.InsertVariables("invoice",invoice );

            //template.insert
            return template.Content;
        }

        public string GetExternalTransactionLink(Model.Payment paymente) {
            return null;
        }

        private Braintree.BraintreeGateway GenerateGateway(string accessToken) {
            var gateway = new Braintree.BraintreeGateway(accessToken);
            return gateway;
        }

        public void Refund(Invoice invoice, Model.Payment payment, User actingUse) {
            throw new NotImplementedException();
        }
    }
}
