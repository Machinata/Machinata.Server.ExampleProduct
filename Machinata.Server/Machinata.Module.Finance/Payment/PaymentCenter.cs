using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Finance.Payment.Providers;
using Machinata.Module.Finance.Payment.Providers.CreditCardProviders;
using System.Collections.Generic;

namespace Machinata.Module.Finance.Payment {

    public class PaymentCenter {

        public static IPaymentProvider GetPaymentProvider(PaymentMethod method) {
          
            if (method == PaymentMethod.CreditCard) {
                return Providers.CreditCardPaymentProvider.DefaultProvider;
            }
            else if (method == PaymentMethod.Invoice) {
                return new Providers.InvoicePaymentProvider();
            }
            throw new BackendException("unkown-payment-provider", $"The payment method {method} is unknown");
        }


        public static IPaymentProvider GetPaymentProvider(string name) {

            name = name.ToLowerInvariant();
           
            if (name == "datatrans"){
                return new DataTransPaymentProvider();
            }
            if (name == "invoice"){
                return new InvoicePaymentProvider();
            }
            if (name == "free"){
                return new FreePaymentProvider();
            }
            if (name == "stripe") {
                return new StripePaymentProvider();
            }
            if (name == "paypal") {
                return new PaypalPaymentProvider();
            }
            throw new BackendException("payment-provider-error", "Payment provider not found: " + name);
        }

        public static IEnumerable<IPaymentProvider> GetEnabledPaymentProviders() {
            foreach(var providerName in Config.PaymentProvider.PaymentProviders) {
                yield return GetPaymentProvider(providerName);
            }
        }

        
    }



}
