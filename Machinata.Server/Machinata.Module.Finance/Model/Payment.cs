using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Finance.Payment;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Finance.Model {

    [Serializable()]
    [ModelClass]
    public partial class Payment : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Payment() {

        }

        #endregion


        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [Column]
        public string PaymentProvider { get; set; }

        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [Column]
        public Price Amount { get; set; }

        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [Column]
        public DateTime Paid { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public string PaymentProviderTransactionId { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public string PaymentProviderPayerId { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public Price ProviderTransactionFee { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder()]
        [NotMapped]
        public string TransactionUrl
        {
            get
            {
                return PaymentProviderInstance.GetExternalTransactionLink(this);
            }
        }

        [FormBuilder()]
        [NotMapped]
        internal IPaymentProvider PaymentProviderInstance
        {
            get { return PaymentCenter.GetPaymentProvider(this.PaymentProvider); }
        }

        [NotMapped]
        [FormBuilder()]
        public bool IsLiveRefundable
        {
            get
            {
                return PaymentProviderInstance.CanRefundLive ;
            }
        }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        






        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
        }

        public override string ToString() {
            var result = new StringBuilder();
            result.AppendLine(base.ToString() + ": ");
            result.AppendLine("{text.payment.amount}: " + this.Amount);
            result.AppendLine(", {text.payment.paid}: " + this.Paid);
            result.AppendLine(", {text.payment.payment-provider}: " + this.PaymentProvider);
            result.AppendLine(", {text.payment.transaction-id}: " + this.PaymentProviderTransactionId);
            return result.ToString();
        }
        #endregion


        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static void CreateFullPaymentForInvoice(string transactionId, Invoice invoice, string providerName, Price transactionFee, string payerId) {
            if(invoice.Status >= Invoice.InvoiceStatus.Paid) {
                throw new BackendException("payment-error", "Invoice has already been for");
            }
           
            var payment = new Finance.Model.Payment();
            payment.Amount = invoice.TotalCustomer.Clone();
            payment.Paid = DateTime.UtcNow;
            payment.PaymentProvider = providerName;
            payment.PaymentProviderTransactionId = transactionId;
            payment.PaymentProviderPayerId = payerId;
            payment.ProviderTransactionFee = transactionFee.Clone();


            invoice.AddPaymentWithSave(invoice.Context, payment);
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderPaymentExtensions {
        [ModelSet]
        public static DbSet<Payment> Payments(this Core.Model.ModelContext context) {
            return context.Set<Payment>();
        }
    }

    #endregion

}
