using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;

namespace Machinata.Module.Finance.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class InvoiceHistory : ModelObject {
        
     
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public InvoiceHistory() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Column]
        public Invoice.InvoiceStatus Status { get; set; }
        
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public DateTime Timestamp { get { return this.Created; } }
        
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Description { get; set; }

        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string User { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        public Invoice Invoice { get; set; }

      
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
        
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        #endregion
        
        #region Private Methods ///////////////////////////////////////////////////////////////////
        
        #endregion
       
    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderStatusExtenions {
        [ModelSet]
        public static DbSet<InvoiceHistory> InvoiceHistories(this Core.Model.ModelContext context) {
            return context.Set<InvoiceHistory>();
        }
    }


    #endregion

}
