using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Finance.Model {

    /// <summary>
    /// Helper class for generating report totals based on line items.
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    public partial class LineItemTotal : ModelObject {
        

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public LineItemTotal() {
            this.TimeRange = new DateRange();
        }

        #endregion


        #region Public Data Store Properties //////////////////////////////////////////////////////

       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public string LineItem { get; set; }

        [FormBuilder(Forms.EMPTY)]
        [DataType(DataType.Date)]
        public DateRange TimeRange { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public Price CustomerSubtotal { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTING)]
      //  [FormBuilder(Forms.Admin.TOTAL)]
        public Price CustomerVAT { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public Price CustomerTotal { get; set; }

      

        public string StyleClass { get; set; }
        
        #endregion
        

    }

}
