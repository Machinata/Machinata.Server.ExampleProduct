
/// <summary>
/// Typescript global reference to Machinata
/// </summary>
/// <hidden/>
const Machinata: any = window["MACHINATA"];


/// <summary>
/// Typescript global reference to jQuery
/// </summary>
/// <hidden/>
const $: any = window["MACHINATA_JQUERY"];


const blobStream: any = window["blobStream"];
const canvas2pdf: any = window["canvas2pdf"];