

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityMeasuringTape extends TwoDEntity {

    a: TwoDEntity;
    b: TwoDEntity;
    _tapeSize: number = 100;

    constructor() {
        super();
        let self = this;

        self._group = true;
        self.canRotate = false;
        self.canMove = false;
        //self.setSize(new gsize(0, this._tapeSize));

        self.a = new TwoDEntityCircle();
        self.a.setSize(new gsize(this._tapeSize, this._tapeSize));
        self.a.canRotate = false;
        self.a.deletesParent = true;
        this.addChild(self.a);


        self.b = new TwoDEntityCircle();
        self.b.setSize(new gsize(this._tapeSize, this._tapeSize));
        self.b.canRotate = false;
        self.b.setPos(new gpos(1000, 0));
        self.b.deletesParent = true;
        this.addChild(self.b);
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.strokeStyle = "black";
        ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
        ctx.beginPath();
        ctx.moveTo(this.a._pos.x, this.a._pos.y);
        ctx.lineTo(this.b._pos.x, this.b._pos.y);
        ctx.closePath();
        ctx.stroke();

        var dist = this.a._pos.dist(this.b._pos);
        var labelPos = this.b._pos.add(this.a._pos).mul(0.5);
        renderer.drawLabel(this, this.canvas.getMetricUnitLabel(dist,2), labelPos.x, labelPos.y, "center");
    }

}
Machinata.TwoD.EntityMeasuringTape = TwoDEntityMeasuringTape;