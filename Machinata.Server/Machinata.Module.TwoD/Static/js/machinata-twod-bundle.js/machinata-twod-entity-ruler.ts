
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityRuler extends TwoDEntity {


    constructor() {
        super();
        let self = this;

        this.setLength(1000);


        // Properties
        this.props.push(
            new TwoDEntityProp()
                .title("{text.length}")
                .units("{canvas-units}")
                .type("number")
                .minValue(10)
                .maxValue(1000*100)
                .defaultValue(1000)
                .step(100)
                .getter(function (entity: TwoDEntityRuler) { return entity._size.w })
                .setter(function (entity: TwoDEntityRuler, val: number) { entity.setLength(val) })
        );
    }

    setLength(length: number) {
        this.setSize(new gsize(length, 1000 / 4));
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.translate(-this._size.w / 2, -this._size.h / 2);

        var mmPerBlock = 1000; // 1 meter

        var widthInMM = this._size.w; //TODO: what if other units?
        var scaleBlocksToShow = Math.floor(widthInMM / mmPerBlock); // meters
        while (scaleBlocksToShow < 5) {
            mmPerBlock = mmPerBlock / 10;
            scaleBlocksToShow = Math.floor(widthInMM / mmPerBlock); // meters
        }
        
        var blockW = mmPerBlock;
        var blockH = this._size.h;
        for (var i = 0; i < scaleBlocksToShow; i++) {
            var blockX = (i * blockW);
            var blockY = 0;
            ctx.fillStyle = i % 2 == 0 ? "black" : "white";
            ctx.fillRect(blockX, blockY, blockW, blockH);
        }
        // Block label
        var fontSize = (renderer.viewport.labelFontSize / renderer.viewport.drawScale());
        ctx.font = fontSize + "px " + renderer.viewport.labelFontName;
        ctx.fillStyle = "black";
        ctx.fillText(this.canvas.getMetricUnitLabel(mmPerBlock), 0, -fontSize / 2);
        ctx.textAlign = "right";
        ctx.fillText(this.canvas.getMetricUnitLabel((mmPerBlock * scaleBlocksToShow)), blockW * scaleBlocksToShow, -fontSize / 2);
        // Border
        ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
        ctx.strokeStyle = "black";
        ctx.strokeRect(0, 0, this._size.w, this._size.h);
    }

}
Machinata.TwoD.EntityRuler = TwoDEntityRuler;




