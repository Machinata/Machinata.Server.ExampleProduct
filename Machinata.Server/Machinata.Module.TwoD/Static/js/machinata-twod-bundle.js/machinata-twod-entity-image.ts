
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityImage extends TwoDEntity {

    image: HTMLImageElement;

    _imageURL: string = null;
    _imageLoaded: boolean = false;
    _autoSize: boolean = false;

    constructor() {
        super();
        let self = this;

        this._imageURL = null;
        this._imageLoaded = false;

        this.image = new Image();
        this.image.onload = function () {
            self._imageLoaded = true;
            console.log("TwoDEntityImage: image loaded: " + self.image.src);
            if (self._autoSize == true) {
                self.setSize(new gsize(self.image.width, self.image.height));
            }
            if(self.canvas != null) self.canvas.requestRedraw();
        }

        // Properties
        this.props.push(
            new TwoDEntityProp()
                .title("{text.twod.image}")
                .type("image")
                .defaultValue(null)
                .getter(function (entity: TwoDEntityImage) { return entity._imageURL })
                .setter(function (entity: TwoDEntityImage, val: string) { entity.setImage(val) })
        );
    }

    setAutoSize(autoSize: boolean): TwoDEntityImage {
        this._autoSize = autoSize;
        return this;
    }

    setImage(url: string): TwoDEntityImage {
        this._imageLoaded = false;
        this._imageURL = url;
        this.image.src = url;

        return this;
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        if (this._imageLoaded == true) {
            ctx.drawImage(this.image, -this._size.w / 2, -this._size.h/2, this._size.w, this._size.h);
        }
    }

}
Machinata.TwoD.EntityImage = TwoDEntityImage;