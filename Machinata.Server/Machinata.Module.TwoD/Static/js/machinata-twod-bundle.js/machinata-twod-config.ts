
/// <type>class</type>
class TwoDCanvasConfig {

    rotateHandleSize: number = 5.0;
    rotateHandleLineSize: number = 2.0;
    rotateHandleColor: string = "red";

    selectedBorderSize: number = 2.0;
    selectedBorderColor: string = "red";

    hoveredBorderSize: number = 1.0;
    hoveredBorderColor: string = "gray";

    gridLineSize: number = 1.0;
    gridLineColor: string = "rgba(0,0,0,0.2)"; 

    snapPointSize: number = 8.0;
    snapPointColor: string = "gray";
    snapPointSnappingDistance: number = 20;

    units: string = "mm";
}
