
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityProp {
    _title: string;
    _units: string;
    _type: string;
    _minValue: any;
    _maxValue: any;
    _defaultValue: any;
    _step: any;

    _getter: Function;
    _setter: Function;

    title(title: string) {
        this._title = title;
        return this;
    }
    units(units: string) {
        this._units = units;
        return this;
    }
    type(type: string) {
        this._type = type;
        return this;
    }
    step(step: any) {
        this._step = step;
        return this;
    }
    minValue(minValue: any) {
        this._minValue = minValue;
        return this;
    }
    maxValue(maxValue: any) {
        this._maxValue = maxValue;
        return this;
    }
    defaultValue(defaultValue: any) {
        this._defaultValue = defaultValue;
        return this;
    }
    getter(getter: Function) {
        this._getter = getter;
        return this;
    }
    setter(setter: Function) {
        this._setter = setter;
        return this;
    }

    buildStandardEditUI(entity: TwoDEntity) {
        var self = this;

        // Init elem
        var propElem = $("<div class='ui-form-row'><div class='ui-label'></div><div class='ui-input'></div></div>");
        propElem.data("entity",entity);
        propElem.data("prop", self);

        // Label
        var label = self._title;
        if (self._units != null) {
            if (self._units == "{canvas-units}") label += " [" + entity.canvas.config.units + "]";
            else label += " [" + self._units + "]";
        }
        propElem.find(".ui-label").text(label);

        // Create input element
        var propInputElem = $("<input/>");
        propInputElem.attr("type", self._type);
        // Set attributes
        if (self._step) propInputElem.attr("step", self._step);
        if (self._minValue) propInputElem.attr("min", self._minValue);
        if (self._maxValue) propInputElem.attr("max", self._maxValue);
        // Set value
        propInputElem.val(self._getter(entity));
        // Readonly?
        if (self._setter == null) propInputElem.attr("readonly", "readonly");
        // Append
        propElem.find(".ui-input").append(propInputElem);
        // Bind events
        propInputElem.on("change input", function () {
            var val = propInputElem.val();
            self._setter(entity, val);
            entity.canvas.requestRedraw();
        });


        return propElem;
    }
}


/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntitySnapPoint {
    pos: gpos;
    rot: number;
    label: string;
    entity: TwoDEntity;
    _partner: TwoDEntitySnapPoint

    constructor(label: string) {
        this.label = label;
        this.pos = new gpos(0, 0);
        this.rot = 0;
    }

    worldPos(): gpos {
        let parentPos = this.entity.worldPos();
        let parentRot = this.entity.worldRot();
        return parentPos
            .add(this.pos)
            .rotAroundPivot(parentRot, parentPos);
    }

    setPartner(other: TwoDEntitySnapPoint) {
        if (other == null) {
            if (this._partner != null) this._partner._partner = null;
            this._partner = null;
        } else {
            this._partner = other;
            other._partner = this;
        }
    }
    partner(): TwoDEntitySnapPoint {
        return this._partner;
    }

    worldRot(): number {
        return this.entity.worldRot() + this.rot;
    }
}

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntity {

    canvas: TwoDCanvas = null;
    parent: TwoDEntity = null;
    children: TwoDEntity[] = [];

    props: TwoDEntityProp[] = [];
    snapPoints: TwoDEntitySnapPoint[] = [];

    enabled: boolean = true;
    canRotate: boolean = true;
    canMove: boolean = true;
    deletesParent: boolean = false;

    _label: string;
    _rot: number;
    _pos: gpos;
    _worldRot: number; //TODO: track via parent and update always on set of _rot or _pos
    _worldPos: gpos; //TODO: track via parent and update always on set of _rot or _pos
    _size: gsize;
    _dirty: boolean = true;
    _group: boolean = false;
    debugColor: string = "green";


    constructor() {
        this._size = new gsize(0, 0);
        this._pos = new gpos(0, 0);
        this._rot = 0;
    }

    addSnapPoint(sp: TwoDEntitySnapPoint): TwoDEntity {
        sp.entity = this;
        this.snapPoints.push(sp);
        return this;
    }

    updateSnapPoints() {
        // Should be implemented by subclass, called when resized (setSize)
    }


    snapToClosebySnapPoints() {
        var maxDistanceBetweenSnapPoints = this.canvas.config.snapPointSnappingDistance;
        var closestMatchDistance = 9999999999999.0;
        let matchA: TwoDEntitySnapPoint = null;
        let matchB: TwoDEntitySnapPoint = null;
        let self = this;
        let a = this;
        if (a.snapPoints == null || a.snapPoints.length == 0) return;

        for (var spai = 0; spai < a.snapPoints.length; spai++) {
            var spa = a.snapPoints[spai];
            var spaPos = spa.worldPos();
            spa.setPartner(null); // reset

            for (var ebi = 0; ebi < self.canvas.rootEntity.children.length; ebi++) {
                
                var b = self.canvas.rootEntity.children[ebi];
                // Skip self, of course
                if (b == a) continue;
                // Skip if no snap points
                if (b.snapPoints == null || b.snapPoints.length == 0) continue;
                // Skip if bounds not close
                //TODO
                for (var spbi = 0; spbi < b.snapPoints.length; spbi++) {
                    var spb = b.snapPoints[spbi];
                    var spbPos = spb.worldPos();
                    var dist = spaPos.dist(spbPos);
                    if (dist < maxDistanceBetweenSnapPoints && dist < closestMatchDistance) {
                        closestMatchDistance = dist;
                        matchA = spa; // self
                        matchB = spb; // other
                        //TODO: break if very close (note: can't break due to reset of all snap points)
                    }
                }
            }
        }

        if (matchB != null) {
            self.snapToSnapPoint(matchA, matchB);
        }
    }

    snapToSnapPoint(spSelf: TwoDEntitySnapPoint, spOther: TwoDEntitySnapPoint) {
        //TODO: validate that spSelf is really own
        var newRot = spOther.worldRot() + spSelf.rot;
        var newPos = spOther.worldPos();
        newPos.x += -spSelf.pos.rot(newRot).x;
        newPos.y += -spSelf.pos.rot(newRot).y;
        this.setWorldRot(newRot);
        this.setWorldPos(newPos);
        spSelf.setPartner(spOther);
    }

    setLabel(label: string) {
        this._label = label;
        return this;
    }

    drawLabel(renderer: TwoDCanvasRenderer, label: string = null) {
        if (label == null) label = this._label;
        let ctx = renderer.ctx;
        var rot = this.worldRot();
        ctx.rotate(-rot);
        renderer.drawLabel(this,label, 0, 0, "center");
        ctx.rotate(+rot);
    }

    addChild(child: TwoDEntity) {
        child.parent = this;
        child._setCanvas(this.canvas);
        this.children.push(child);
        if(this.canvas != null) this.canvas.requestRedraw();
        return this;
    }

    _setCanvas(canvas: TwoDCanvas) {

        this.canvas = canvas;
        // Make sure all child children are hooked on the canvas
        for (var i = 0; i < this.children.length; i++) {
            this.children[i]._setCanvas(canvas);
        }
    }

    addTo(parent: TwoDEntity) {
        parent.addChild(this);
        return this;
    }

    addToCanvas(canvas: TwoDCanvas) {
        canvas.addEntity(this);
        return this;
    }

    delete() {
        if (this.parent == null) {
            throw "Cannot delete as this item is not attached to any parent"
        }
        this.parent.deleteChild(this);
        return this;
    }

    deleteChild(child: TwoDEntity) {
        var index = this.children.indexOf(child);
        if (index > -1) {
            this.children.splice(index, 1);
        } else {
            throw "Cannot delete child as it is not a child of this item"
        }
        this._dirty = true;
        child.parent = null;
        child.canvas = null;
        if (this.canvas.selectedEntity == child) this.canvas.selectEntity(null); // de-select
        if (this.canvas.onEntityDeleted) this.canvas.onEntityDeleted(this, child);
        this.canvas.requestRedraw();
        return this;
    }

    setRot(rot: number): TwoDEntity {
        this._rot = rot;
        if (this.parent != null) this.parent._dirty = true;

        return this;
    }

    // If true, the entity won't be drawn or impose its own bounds
    setGroup(group: boolean): TwoDEntity {
        this._group = group;

        return this;
    }

    setPos(pos: gpos): TwoDEntity {
        this._pos = new gpos(pos.x,pos.y);
        if (this.parent != null) this.parent._dirty = true;

        return this;
    }

    setSize(size: gsize): TwoDEntity {
        this._size = size;
        this._dirty = true;
        if (this.snapPoints != null && this.snapPoints.length > 0) this.updateSnapPoints();
        if (this.parent != null) this.parent._dirty = true;

        return this;
    }
    //setSize(w: number, h: number): TwoDEntity {
    //    return this.setSize(new vec2(w,h));
    //}

    worldPos(): gpos {
        if (this.parent == null) return this._pos;
        let parentPos = this.parent.worldPos();
        let parentRot = this.parent.worldRot();

        return parentPos
            .add(this._pos)
            .rotAroundPivot(parentRot, parentPos);

    }

    worldRot(): number {
        if (this.parent == null) return this._rot;
        let parentRot = this.parent.worldRot();
        return (
            this._rot + parentRot
        );
    }

    setWorldRot(newRot: number): TwoDEntity {
        let parentRot = this.parent.worldRot();
        this._rot = newRot - parentRot;
        return this;
    }

    setWorldPos(newPos: gpos): TwoDEntity {
        let parentPos = this.parent.worldPos();
        let parentRot = this.parent.worldRot();
        newPos = newPos.rotAroundPivot(-parentRot, parentPos);
        newPos = new gpos(
            newPos.x - parentPos.x,
            newPos.y - parentPos.y
        );
        this._pos = newPos;
        return this;
    }

    bounds(): gbounds {

        // Get self
        // P1---P2
        // |     |
        // P4---P3
        let myBoundsP1: gpos = new gpos( // upper left
            - this._size.w / 2,
            - this._size.h / 2
        ).rot(this._rot);
        let myBoundsP2: gpos = new gpos( // upper right
            + this._size.w / 2,
            - this._size.h / 2
        ).rot(this._rot);
        let myBoundsP3: gpos = new gpos( // lower right
            + this._size.w / 2,
            + this._size.h / 2
        ).rot(this._rot);
        let myBoundsP4: gpos = new gpos( // lower left
            - this._size.w / 2,
            + this._size.h / 2
        ).rot(this._rot);
        let myBounds: gbounds = new gbounds(
            new gpos(
                this._pos.x + (myBoundsP1.x + myBoundsP2.x + myBoundsP3.x + myBoundsP4.x) / 4.0,
                this._pos.y + (myBoundsP1.y + myBoundsP2.y + myBoundsP3.y + myBoundsP4.y) / 4.0
            ),
            new gsize(
                Math.max(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x) - Math.min(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x),
                Math.max(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y) - Math.min(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y)
            )
        );

        for (let child of this.children) {
            // Get child bounds, in our coord space
            let childBounds = child.bounds();
            childBounds.center.x += this._pos.x;
            childBounds.center.y += this._pos.y;
            myBounds.extend(childBounds);
        }

        return myBounds;
    }


    worldBounds(): gbounds {

        // Get self
        // P1---P2
        // |     |
        // P4---P3
        let worldPos = this.worldPos();
        let wrot = this.worldRot();
        let p1: gpos = new gpos( // upper left
            - this._size.w / 2,
            - this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p2: gpos = new gpos( // upper right
            + this._size.w / 2,
            - this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p3: gpos = new gpos( // lower right
            + this._size.w / 2,
            + this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p4: gpos = new gpos( // lower left
            - this._size.w / 2,
            + this._size.h / 2
        ).rot(wrot).add(worldPos);

        /*if (true) {
            this.canvas._debugDrawPoint(p1);
            this.canvas._debugDrawPoint(p2);
            this.canvas._debugDrawPoint(p3);
            this.canvas._debugDrawPoint(p4);
        }*/

        var myBounds: gbounds;
        if (this._group == true) {
            myBounds = new gbounds(new gpos(0, 0), new gsize(0, 0));
            if (this.children.length > 1) myBounds = this.children[1].worldBounds();
        } else {
            myBounds = new gbounds(
                new gpos(
                    (p1.x + p2.x + p3.x + p4.x) / 4.0,
                    (p1.y + p2.y + p3.y + p4.y) / 4.0
                ),
                new gsize(
                    Math.max(p1.x, p2.x, p3.x, p4.x) - Math.min(p1.x, p2.x, p3.x, p4.x),
                    Math.max(p1.y, p2.y, p3.y, p4.y) - Math.min(p1.y, p2.y, p3.y, p4.y)
                )
            );
        }
        

        for (let child of this.children) {
            // Get child bounds
            let childBounds = child.worldBounds();
            myBounds.extend(childBounds);
        }

        return myBounds;
    }

    rotateHandlePos(): gpos {
        let handleOffset = this._size.h / 2 + (15.0 / this.canvas._previewRenderer.viewport.drawScale());
        var worldPos = this.worldPos();
        var worldRot = this.worldRot();
        //var handlePos = worldPos.rot(worldRot).add(new gpos(0, -handleOffset));
        var handlePos = worldPos.add(new gpos(0, -handleOffset).rot(worldRot));
        return handlePos;
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;
    }

    getChildAtPoint(pos: gpos): TwoDEntity {
        for (let child of this.children) {
            if (!child.enabled) continue;
            if (child.worldBounds().containsPoint(pos)) {
                var grandchild = child.getChildAtPoint(pos);
                if (grandchild != null) return grandchild;
                return child;
            } 
        }
        return null;
    }

    onSelected(): void {
        
    }
    onDeselected(): void {

    }

    pointTo(newWorldPos: gpos) {
        let worldPos = this.worldPos();
        let newRot = Math.atan2(newWorldPos.y - worldPos.y, newWorldPos.x - worldPos.x);
        newRot += Math.PI / 2;
        this.setWorldRot(newRot)
    }

    _drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this.enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.worldBounds())) return;

        ctx.save(); {

            // Statis
            renderer.statsTotalDraws++;

            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);

            // Debug bg
            if (this.canvas.debug == true) {
                ctx.save(); {
                    ctx.globalAlpha = 0.2;
                    ctx.fillStyle = this.debugColor;
                    ctx.strokeStyle = null;
                    ctx.fillRect(
                        -this._size.w / 2,
                        -this._size.h / 2,
                        this._size.w,
                        this._size.h
                    );
                    if (this == this.canvas.selectedEntity) {
                        ctx.strokeStyle = "red";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(
                            -this._size.w / 2,
                            -this._size.h / 2,
                            this._size.w,
                            this._size.h
                        );
                    }
                    if (this == this.canvas.hoveredEntity) {
                        ctx.strokeStyle = "blue";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(
                            -this._size.w / 2,
                            -this._size.h / 2,
                            this._size.w,
                            this._size.h
                        );
                    }
                    ctx.globalAlpha = 1.0;
                } ctx.restore();
            }

            // Call implementing draw
            this.draw(renderer);

            // Draw all children
            for (let child of this.children) {
                child._drawEntity(renderer);
            }


        } ctx.restore();
    }

    _drawUI(renderer: TwoDCanvasRenderer): void {
        //TODO: optimisze wordPos/worldRot access via caching!

        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this.enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.worldBounds())) return;

        ctx.save(); {

            var drawSnapPoints = false;

            // Selected bounds
            if (this.canvas.selectedEntity == this) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();

                // Draw selection border
                ctx.save(); {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.selectedBorderColor;
                    ctx.lineWidth = this.canvas.config.selectedBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                } ctx.restore();

                // Rotate handle?
                if (this.canRotate) {

                    var handlePos = this.rotateHandlePos();
                    if (this.canvas._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                        handlePos = this.canvas.cursorPos;
                    }
                    let handleSize = this.canvas.config.rotateHandleSize / renderer.viewport.drawScale();
                    //if (handlePos.dist(this.canvas.cursorPos) < (this.canvas.config.rotateHandleSize * 1.5) / 2.0) handleSize = this.canvas.config.rotateHandleSize * 1.5 / renderer.viewport.zoom;
                    ctx.save(); {
                        // Line
                        ctx.beginPath();
                        ctx.moveTo(worldPos.x, worldPos.y);
                        ctx.lineTo(handlePos.x, handlePos.y);
                        ctx.lineWidth = this.canvas.config.rotateHandleLineSize / renderer.viewport.drawScale();
                        ctx.strokeStyle = this.canvas.config.rotateHandleColor;
                        ctx.closePath();
                        ctx.stroke();
                        // Dot
                        ctx.translate(handlePos.x, handlePos.y);
                        ctx.fillStyle = this.canvas.config.rotateHandleColor;
                        ctx.beginPath();
                        ctx.arc(0, 0, handleSize, 0, 2 * Math.PI);
                        ctx.closePath();
                        ctx.fill();
                    } ctx.restore();
                }

            } else if (this.canvas.hoveredEntity == this) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();

                // Draw selection border
                ctx.save(); {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.hoveredBorderColor;
                    ctx.lineWidth = this.canvas.config.hoveredBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                } ctx.restore();

            }

            // Snap points?
            if (this.snapPoints != null && this.snapPoints.length > 0) {
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();
                for (var i = 0; i < this.snapPoints.length; i++) {
                    var sp = this.snapPoints[i];
                    if (drawSnapPoints == true || sp.partner() != null) {
                        ctx.save(); {
                            ctx.translate(worldPos.x, worldPos.y);
                            ctx.rotate(worldRot);
                            ctx.translate(sp.pos.x, sp.pos.y);
                            ctx.rotate(sp.rot);
                            ctx.beginPath();
                            ctx.arc(
                                0, // x
                                0, // y
                                this.canvas.config.snapPointSize / renderer.viewport.drawScale(), // radius
                                0, // start angle
                                Math.PI, // end angle
                                false
                            );
                            ctx.closePath();
                            ctx.fillStyle = this.canvas.config.snapPointColor;
                            ctx.fill();
                        } ctx.restore();
                    }
                }
            }

            // Debug bounds
            if (this.canvas.debug == true) {
                //var bounds = this.bounds();
                var worldPos = this.worldPos();
                var bounds = this.worldBounds();
                ctx.save(); {
                    let lineSize = 1.0 / renderer.viewport.drawScale();
                    let dotSize = 2.0 / renderer.viewport.drawScale();
                    ctx.globalAlpha = 0.5;
                    if (true) {
                        ctx.fillStyle = null;
                        ctx.strokeStyle = this.debugColor;
                        ctx.lineWidth = lineSize;
                        ctx.strokeRect(
                            bounds.center.x - bounds.size.w / 2,
                            bounds.center.y - bounds.size.h / 2,
                            bounds.size.w,
                            bounds.size.h
                        );
                    }
                    if (true) {
                        ctx.strokeStyle = null;
                        ctx.fillStyle = "black";
                        ctx.fillRect(
                            worldPos.x - dotSize / 2,
                            worldPos.y - dotSize / 2,
                            dotSize,
                            dotSize
                        );
                    }
                    if (false) {
                        ctx.fillStyle = this.debugColor;
                        ctx.fillRect(
                            bounds.center.x - dotSize / 2,
                            bounds.center.y - dotSize / 2,
                            dotSize,
                            dotSize
                        );
                    }
                } ctx.restore();
            }

            // Draw children UI
            for (let child of this.children) {
                child._drawUI(renderer);
            }


        } ctx.restore();
    }
}
Machinata.TwoD.Entity = TwoDEntity;