
/// <summary>
/// 
/// </summary>
/// <type>class</type>
abstract class TwoDEntityFoliage extends TwoDEntity {

    _seed: number = 0;

    constructor(radius: number) {
        super();
        let self = this;

        self.setRadius(radius);
        self._seed = Math.round(Math.random() * 1000);

        // Properties
        this.props.push(
            new TwoDEntityProp()
                .title("{text.twod.circumference}")
                .units("{canvas-units}")
                .type("number")
                .minValue(600)
                .maxValue(1000 * 6)
                .defaultValue(1000)
                .step(200)
                .getter(function (entity: TwoDEntityTree) { return entity._size.w })
                .setter(function (entity: TwoDEntityTree, val: number) { entity.setRadius(val / 2) })
        );
        this.props.push(
            new TwoDEntityProp()
                .title("{text.twod.shape}")
                .type("range")
                .minValue(0)
                .maxValue(1000)
                .defaultValue(Math.round(Math.random() * 1000))
                .step(1)
                .getter(function (entity: TwoDEntityTree) { return self._seed })
                .setter(function (entity: TwoDEntityTree, val: number) { self._seed = val })
        );
    }

    setRadius(r: number): TwoDEntityFoliage {
        this.setSize(new gsize(r * 2, r * 2));
        return this;
    }
    getRadius(): number {
        return this._size.w / 2;
    }

    drawOrganicRing(renderer: TwoDCanvasRenderer, r: number, steps: number, seed2: number, rJitter: number, aJitter: number) {
        let ctx = renderer.ctx;

        ctx.beginPath();

        var seedScaled = this._seed * 1000;
        var step = 360 / steps;
        var firstX;
        var firstY;
        for (var i = 0; i < 360; i += step) {
            var angleOffset = Machinata.Math.rndWithSeed(seedScaled + seed2 + steps + i, -step * aJitter, +step * aJitter);
            var rOffset = Machinata.Math.rndWithSeed(seedScaled + seed2 + steps + i * 2, -(r * rJitter), 0);
            var angle = i + angleOffset;
            var px = Math.sin(Machinata.Math.degToRad(angle)) * (r + rOffset);
            var py = Math.cos(Machinata.Math.degToRad(angle)) * (r + rOffset);
            if (i == 0) {
                ctx.moveTo(px, py);
                firstX = px;
                firstY = py;
            }
            else ctx.lineTo(px, py);
        }
        ctx.lineTo(firstX, firstY); // close

        ctx.closePath();
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Label
        if (true) {
            this.drawLabel(renderer);
        }
    }

}
Machinata.TwoD.EntityFoliage = TwoDEntityFoliage;


/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityTree extends TwoDEntityFoliage {

    constructor() {
        super(1500); // 1.5m default
        this.setLabel("{text.twod.tree}");
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Init
        var r = this.getRadius();
        renderer.setShapeStyle();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 20, 2323, 0.2, 0.3);
            ctx.fill();
            ctx.stroke();
        }
        // Draw trunk
        if(true){
            this.drawOrganicRing(renderer, r*0.1, 10, 654, 0.1, 0.1);
            ctx.stroke();
        }
        // Foliage
        super.draw(renderer);
    }

}
Machinata.TwoD.EntityTree = TwoDEntityTree;





/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityBush extends TwoDEntityFoliage {

    constructor() {
        super(600); // default
        this.setLabel("{text.twod.bush}");
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Init
        var r = this.getRadius();
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 1 / renderer.viewport.drawScale();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 40, 2323, 0.3, 0.4);
            ctx.fill();
            ctx.stroke();
        }
        // Foliage
        super.draw(renderer);
    }

}
Machinata.TwoD.EntityBush = TwoDEntityBush;








/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityStone extends TwoDEntityFoliage {

    constructor() {
        super(500); // default
        this.setLabel("{text.twod.stone}");
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Init
        var r = this.getRadius();
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 1 / renderer.viewport.drawScale();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 8, 2323, 0.3, 0.4);
            ctx.fill();
            ctx.stroke();
        }
        // Foliage
        super.draw(renderer);
    }

}
Machinata.TwoD.EntityStone = TwoDEntityStone;
