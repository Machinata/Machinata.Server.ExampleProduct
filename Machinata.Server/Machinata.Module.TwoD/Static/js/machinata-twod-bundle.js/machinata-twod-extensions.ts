
interface CanvasRenderingContext2D {
    fillMultilineText(text: string, x: number, y: number): void;
}
Object.defineProperty(CanvasRenderingContext2D.prototype, 'fillMultilineText', {
    value: function (text: string, x: number, y: number) {
        let lines = text.split("\n");
        let fontSize = parseInt(this.font.split("px")[0]);
        for (let i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});




Object.defineProperty(canvas2pdf.PdfContext.prototype, 'fillMultilineText', {
    value: function (text: string, x: number, y: number) {
        let lines = text.split("\n");
        let fontSize = parseInt(this.font.split("px")[0]);
        for (let i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});