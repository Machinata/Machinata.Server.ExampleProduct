
/// <type>class</type>
class TwoDCanvasViewport {

    pos: gpos = new gpos(0, 0);
    zoom: number = 1.0;

    dpi: number = 96;
    scale: number = 1 / 100;
    units: string = "mm";

    viewSize: gsize = new gsize(800, 800);
    deviceSize: gsize = new gsize(800, 800);

    gridEnabled: boolean = true;
    gridSize: number = 1000; // default 1m

    scaleEnabled: boolean = true;

    labelFontSize: number = 10; 
    labelFontName: string = "Arial"; 

    setZoom(newZoom: number) {
        this.zoom = newZoom;
        this.viewSize = this.deviceSize.mul(1.0 / this.drawScale());
    }

    fitBounds(bounds: gbounds) {
        // Set to center
        this.pos = new gpos(-bounds.center.x, -bounds.center.y);

        // Get zoom to fit
        var scaleFactor = this.drawScaleForZoom(1.0); // We need this factor since our actual draw scale is different from the zoom
        var newZoomX = (this.deviceSize.w / scaleFactor) / bounds.size.w;
        var newZoomY = (this.deviceSize.h / scaleFactor) / bounds.size.h;
        var newZoom = Math.min(newZoomX, newZoomY);

        this.setZoom(newZoom - (newZoom * 0.01));
    }

    drawScale(): number {
        return this.drawScaleForZoom(this.zoom);
    }

    drawScaleForZoom(zoom: number): number {
        if (this.units == "mm") {
            return zoom * this.scale * (this.mmToIn(1) * this.dpi);
        } else {
            throw this.units + " units is not supported";
        }
    }

    worldBounds(): gbounds {
        return new gbounds(new gpos(-this.pos.x, -this.pos.y), new gsize(this.viewSize.w, this.viewSize.h));
    }

    inToPx(inches: number): number {
        // px = inches * dpi * physicalScale
        return inches * this.dpi * this.scale;
    }

    pxToIn(px: number): number {
        // px = inches * dpi * physicalScale
        // inches = px/(dpi * physicalScale)
        return px / (this.dpi * this.scale);
    }

    inToMM(inches: number): number {
        return inches * 25.4;
    }

    mmToIn(mm: number): number {
        return mm / 25.4;
    }

    mmToPx(mm: number): number {
        var inches = this.mmToIn(mm);
        return this.inToPx(inches);
    }

    pxToMM(px: number): number {
        var inches = this.pxToIn(px);
        return this.inToMM(inches);
    }
}