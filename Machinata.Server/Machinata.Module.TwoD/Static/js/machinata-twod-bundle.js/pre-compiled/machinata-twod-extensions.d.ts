interface CanvasRenderingContext2D {
    fillMultilineText(text: string, x: number, y: number): void;
}
