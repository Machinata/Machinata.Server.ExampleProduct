declare class gpos {
    x: number;
    y: number;
    constructor(x: number, y: number);
    clone(): gpos;
    log(label: string): void;
    mul(factor: number): gpos;
    rot(angle: number): gpos;
    add(other: gpos): gpos;
    subtract(other: gpos): gpos;
    dist(other: gpos): number;
    rotAroundPivot(angle: number, pivot: gpos): gpos;
}
declare class gsize {
    w: number;
    h: number;
    constructor(w: number, h: number);
    mul(factor: number): gsize;
}
declare class gbounds {
    center: gpos;
    size: gsize;
    constructor(center: gpos, size: gsize);
    topLeft(): gpos;
    topRight(): gpos;
    bottomLeft(): gpos;
    bottomRight(): gpos;
    x0(): number;
    x1(): number;
    y0(): number;
    y1(): number;
    extend(by: gbounds): gbounds;
    setX0(newX0: number): gbounds;
    setX1(newX1: number): gbounds;
    setY0(newY0: number): gbounds;
    setY1(newY1: number): gbounds;
    containsPoint(pos: gpos): boolean;
    intersects(other: gbounds): boolean;
}
