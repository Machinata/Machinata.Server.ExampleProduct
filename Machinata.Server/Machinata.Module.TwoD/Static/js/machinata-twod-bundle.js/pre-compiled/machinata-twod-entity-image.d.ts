declare class TwoDEntityImage extends TwoDEntity {
    image: HTMLImageElement;
    _imageURL: string;
    _imageLoaded: boolean;
    _autoSize: boolean;
    constructor();
    setAutoSize(autoSize: boolean): TwoDEntityImage;
    setImage(url: string): TwoDEntityImage;
    draw(renderer: TwoDCanvasRenderer): void;
}
