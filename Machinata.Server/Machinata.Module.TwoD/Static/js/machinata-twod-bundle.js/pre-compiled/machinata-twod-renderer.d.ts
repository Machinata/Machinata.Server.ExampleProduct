declare abstract class TwoDCanvasRenderer {
    ctx: CanvasRenderingContext2D;
    viewport: TwoDCanvasViewport;
    statsStartDrawTime: Date;
    statsEndDrawTime: Date;
    statsTotalDrawTimeMS: number;
    statsTotalDrawTimeFPS: number;
    statsTotalDraws: number;
    debug: boolean;
    drawLabels: boolean;
    abstract createDrawingContext(): any;
    setShapeStyle(): void;
    drawLabel(entity: TwoDEntity, text: string, x: number, y: number, alignment: CanvasTextAlign): void;
}
declare class HTMLCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {
    canvas: HTMLCanvasElement;
    createDrawingContext(): any;
}
declare class PDFCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {
    canvas: HTMLCanvasElement;
    createDrawingContext(): any;
}
