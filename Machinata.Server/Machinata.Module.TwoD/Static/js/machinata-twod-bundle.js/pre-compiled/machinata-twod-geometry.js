/// <summary>
/// 
/// </summary>
/// <type>class</type>
var gpos = /** @class */ (function () {
    function gpos(x, y) {
        this.x = x;
        this.y = y;
    }
    gpos.prototype.clone = function () {
        return new gpos(this.x, this.y);
    };
    gpos.prototype.log = function (label) {
        console.log(label, this.x, this.y);
    };
    gpos.prototype.mul = function (factor) {
        return new gpos(this.x * factor, this.y * factor);
    };
    gpos.prototype.rot = function (angle) {
        return this.rotAroundPivot(angle, new gpos(0, 0));
    };
    gpos.prototype.add = function (other) {
        return new gpos(this.x + other.x, this.y + other.y);
    };
    gpos.prototype.subtract = function (other) {
        return new gpos(this.x - other.x, this.y - other.y);
    };
    gpos.prototype.dist = function (other) {
        return Math.sqrt((this.x - other.x) * (this.x - other.x) + (this.y - other.y) * (this.y - other.y));
    };
    gpos.prototype.rotAroundPivot = function (angle, pivot) {
        var cos = Math.cos(-angle);
        var sin = Math.sin(-angle);
        var run = this.x - pivot.x;
        var rise = this.y - pivot.y;
        var cx = (cos * run) + (sin * rise) + pivot.x;
        var cy = (cos * rise) - (sin * run) + pivot.y;
        return new gpos(cx, cy);
        /*
        let sinTheta = Math.sin(angle);
        let cosTheta = Math.cos(angle);

        return new gpos(
            (cosTheta * (this.x - pivot.x) - sinTheta * (this.x - pivot.y) + pivot.x),
            (sinTheta * (this.x - pivot.x) + cosTheta * (this.y - pivot.y) + pivot.y)
        );*/
    };
    return gpos;
}());
Machinata.TwoD.gpos = gpos;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var gsize = /** @class */ (function () {
    function gsize(w, h) {
        this.w = w;
        this.h = h;
    }
    gsize.prototype.mul = function (factor) {
        return new gsize(this.w * factor, this.h * factor);
    };
    return gsize;
}());
Machinata.TwoD.gsize = gsize;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var gbounds = /** @class */ (function () {
    function gbounds(center, size) {
        this.center = center;
        this.size = size;
    }
    gbounds.prototype.topLeft = function () {
        return new gpos(this.x0(), this.y0());
    };
    gbounds.prototype.topRight = function () {
        return new gpos(this.x1(), this.y0());
    };
    gbounds.prototype.bottomLeft = function () {
        return new gpos(this.x0(), this.y1());
    };
    gbounds.prototype.bottomRight = function () {
        return new gpos(this.x1(), this.y1());
    };
    gbounds.prototype.x0 = function () {
        return this.center.x - this.size.w / 2;
    };
    gbounds.prototype.x1 = function () {
        return this.center.x + this.size.w / 2;
    };
    gbounds.prototype.y0 = function () {
        return this.center.y - this.size.h / 2;
    };
    gbounds.prototype.y1 = function () {
        return this.center.y + this.size.h / 2;
    };
    gbounds.prototype.extend = function (by) {
        if (by.x0() < this.x0()) {
            this.setX0(by.x0());
        }
        if (by.x1() > this.x1()) {
            this.setX1(by.x1());
        }
        if (by.y0() < this.y0()) {
            this.setY0(by.y0());
        }
        if (by.y1() > this.y1()) {
            this.setY1(by.y1());
        }
        return this;
    };
    gbounds.prototype.setX0 = function (newX0) {
        var delta = this.x0() - newX0;
        this.center.x -= delta / 2;
        this.size.w += delta;
        return this;
    };
    gbounds.prototype.setX1 = function (newX1) {
        var delta = newX1 - this.x1();
        this.center.x += delta / 2;
        this.size.w += delta;
        return this;
    };
    gbounds.prototype.setY0 = function (newY0) {
        var delta = this.y0() - newY0;
        this.center.y -= delta / 2;
        this.size.h += delta;
        return this;
    };
    gbounds.prototype.setY1 = function (newY1) {
        var delta = newY1 - this.y1();
        this.center.y += delta / 2;
        this.size.h += delta;
        return this;
    };
    gbounds.prototype.containsPoint = function (pos) {
        if (pos.x >= this.x0() && pos.x <= this.x1() &&
            pos.y >= this.y0() && pos.y <= this.y1())
            return true;
        else
            return false;
    };
    gbounds.prototype.intersects = function (other) {
        if (other.x1() <= this.x0())
            return false; // other is to left
        if (other.x0() >= this.x1())
            return false; // other is to right
        if (other.y1() <= this.y0())
            return false; // other is above
        if (other.y0() >= this.y1())
            return false; // other is below
        return true;
    };
    return gbounds;
}());
Machinata.TwoD.gbounds = gbounds;
