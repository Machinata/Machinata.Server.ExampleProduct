declare abstract class TwoDEntityFoliage extends TwoDEntity {
    _seed: number;
    constructor(radius: number);
    setRadius(r: number): TwoDEntityFoliage;
    getRadius(): number;
    drawOrganicRing(renderer: TwoDCanvasRenderer, r: number, steps: number, seed2: number, rJitter: number, aJitter: number): void;
    draw(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityTree extends TwoDEntityFoliage {
    constructor();
    draw(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityBush extends TwoDEntityFoliage {
    constructor();
    draw(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityStone extends TwoDEntityFoliage {
    constructor();
    draw(renderer: TwoDCanvasRenderer): void;
}
