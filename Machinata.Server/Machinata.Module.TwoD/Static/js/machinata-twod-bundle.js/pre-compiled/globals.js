/// <summary>
/// Typescript global reference to Machinata
/// </summary>
/// <hidden/>
var Machinata = window["MACHINATA"];
/// <summary>
/// Typescript global reference to jQuery
/// </summary>
/// <hidden/>
var $ = window["MACHINATA_JQUERY"];
var blobStream = window["blobStream"];
var canvas2pdf = window["canvas2pdf"];
