declare class TwoDCanvasConfig {
    rotateHandleSize: number;
    rotateHandleLineSize: number;
    rotateHandleColor: string;
    selectedBorderSize: number;
    selectedBorderColor: string;
    hoveredBorderSize: number;
    hoveredBorderColor: string;
    gridLineSize: number;
    gridLineColor: string;
    snapPointSize: number;
    snapPointColor: string;
    snapPointSnappingDistance: number;
    units: string;
}
