var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityCircle = /** @class */ (function (_super) {
    __extends(TwoDEntityCircle, _super);
    function TwoDEntityCircle() {
        var _this = _super.call(this) || this;
        _this.fillColor = "black";
        var self = _this;
        return _this;
    }
    TwoDEntityCircle.prototype.draw = function (renderer) {
        var ctx = renderer.ctx;
        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        ctx.ellipse(0, 0, this._size.w / 2, this._size.h / 2, 0, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    };
    return TwoDEntityCircle;
}(TwoDEntity));
Machinata.TwoD.EntityCircle = TwoDEntityCircle;
