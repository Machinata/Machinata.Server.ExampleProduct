/// <summary>
/// Copyright Dan Krusi 2020-2021
/// </summary>
/// <summary>
/// 
/// </summary>
var TwoDCanvasInteractionMode;
(function (TwoDCanvasInteractionMode) {
    TwoDCanvasInteractionMode["NONE"] = "NONE";
    TwoDCanvasInteractionMode["DRAG"] = "DRAG";
    TwoDCanvasInteractionMode["PAN"] = "PAN";
    TwoDCanvasInteractionMode["ROTATE"] = "ROTATE";
    TwoDCanvasInteractionMode["SELECT"] = "SELECT";
})(TwoDCanvasInteractionMode || (TwoDCanvasInteractionMode = {}));
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDCanvas = /** @class */ (function () {
    function TwoDCanvas(canvasElem) {
        this.debug = Machinata.DebugEnabled;
        this.config = new TwoDCanvasConfig();
        this.maxZoom = 10.0;
        this.minZoom = 0.1;
        this.cursorPos = new gpos(0, 0);
        this._mousePos = new gpos(0, 0);
        this._lastMousePos = new gpos(0, 0);
        this._lastCursorPos = new gpos(0, 0);
        this._interactionMode = TwoDCanvasInteractionMode.NONE;
        this._interactionPossibility = TwoDCanvasInteractionMode.NONE;
        this._lockedInteractionMode = null;
        this._interactionStartCursorPos = null;
        this._interactionEndCursorPos = null;
        var self = this;
        Machinata.debug("TwoDCanvas.constructor()");
        this._previewCanvasElem = canvasElem;
        this._previewCanvas = canvasElem[0];
        this._isDrawing = false;
        this._redrawRequested = false;
        // Create preview renderer
        this._previewRenderer = new HTMLCanvasTwoDCanvasRenderer();
        this._previewRenderer.ctx = this._previewCanvas.getContext("2d");
        this._previewRenderer.viewport.units = this.config.units;
        this._previewCanvasElem.attr("tabindex", "1");
        this.rootEntity = new TwoDEntity();
        this.rootEntity.setGroup(true);
        this.rootEntity.setPos(new gpos(0, 0));
        this.rootEntity.canvas = this;
        this.rootEntity.debugColor = "red";
        // TEST scene
        //this._debugLoadTestScene();
        // Bindings
        this.bindUIEvents();
        // Initial draw
        this.resize();
        this.redraw();
    }
    TwoDCanvas.prototype.addEntity = function (entity) {
        this.rootEntity.addChild(entity);
        return this;
    };
    TwoDCanvas.prototype.mmToPx = function (mm) {
        return this._previewRenderer.viewport.mmToPx(mm);
    };
    TwoDCanvas.prototype._debugLoadTestScene = function () {
        new TwoDEntityImage()
            .setAutoSize(true)
            .setImage("/static/file/images/burri_digitalworks_logo.png")
            .setPos(new gpos(0, 0))
            .addToCanvas(this);
        new TwoDEntityImage()
            .setAutoSize(true)
            .setImage("/static/file/images/burri_logo.svg")
            .setPos(new gpos(1000, 1000))
            .addToCanvas(this);
        new TwoDEntityRuler()
            .setPos(new gpos(2000, 1000))
            .setSize(new gsize(1000, 1000 / 4))
            .addToCanvas(this);
        new TwoDEntityRuler()
            .setPos(new gpos(1000, 2000))
            .setSize(new gsize(10000, 1000 / 4))
            .addToCanvas(this);
        new TwoDEntityMeasuringTape()
            .setPos(new gpos(3000, 3000))
            .addToCanvas(this);
        return;
        var mainEntity = new TwoDEntity().setPos(new gpos(-10, 10)).setSize(new gsize(20, 20));
        mainEntity.debugColor = "blue";
        mainEntity.setRot(Machinata.Math.degToRad(33.0));
        this.rootEntity.addChild(mainEntity);
        this.rootEntity.addChild(new TwoDEntity().setPos(new gpos(40, 50)).setSize(new gsize(30, 10)));
        var grandChild = new TwoDEntity().setPos(new gpos(40, 50)).setSize(new gsize(30, 10));
        mainEntity.addChild(grandChild);
        var greatGrandChild = new TwoDEntity().setPos(new gpos(20, 10)).setSize(new gsize(5, 8)).setRot(Machinata.Math.degToRad(256.0));
        grandChild.addChild(greatGrandChild);
        var greatGreatGrandChild = new TwoDEntity().setPos(new gpos(20, 10)).setSize(new gsize(5, 8)).setRot(Machinata.Math.degToRad(-33.0));
        greatGrandChild.addChild(greatGreatGrandChild);
        // Random entities
        for (var i = 0; i < 1000; i++) {
            var newEntity = new TwoDEntity()
                .setPos(new gpos(Machinata.Math.rnd(-1000, 1000), Machinata.Math.rnd(-1000, 1000)))
                .setRot(Machinata.Math.degToRad(Machinata.Math.rnd(-360, 360)))
                .setSize(new gsize(Machinata.Math.rnd(10, 100), Machinata.Math.rnd(10, 100)));
            this.rootEntity.addChild(newEntity);
        }
    };
    TwoDCanvas.prototype._debugDrawPoint = function (pos) {
        this._previewRenderer.ctx.save();
        {
            this._previewRenderer.ctx.fillStyle = "pink";
            this._previewRenderer.ctx.fillRect(pos.x - 1, pos.y - 1, 2, 2);
        }
        this._previewRenderer.ctx.restore();
    };
    TwoDCanvas.prototype._updatePointer = function () {
        if (this._lockedInteractionMode == TwoDCanvasInteractionMode.PAN && this._interactionMode == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grabbing");
        }
        else if (this._lockedInteractionMode == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grab");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.DRAG || this._interactionPossibility == TwoDCanvasInteractionMode.DRAG) {
            this._previewCanvasElem.css("cursor", "move");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.ROTATE || this._interactionPossibility == TwoDCanvasInteractionMode.ROTATE) {
            this._previewCanvasElem.css("cursor", "cell");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.PAN || this._interactionPossibility == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grabbing");
        }
        else {
            this._previewCanvasElem.css("cursor", "crosshair");
        }
    };
    ;
    TwoDCanvas.prototype._updateCursor = function (event) {
        if (event != null) {
            this._lastMousePos.x = this._mousePos.x;
            this._lastMousePos.y = this._mousePos.y;
            this._mousePos = new gpos(event.offsetX, event.offsetY);
        }
        this._lastCursorPos.x = this.cursorPos.x;
        this._lastCursorPos.y = this.cursorPos.y;
        this.cursorPos = new gpos((this._mousePos.x - this._previewRenderer.viewport.deviceSize.w / 2) / this._previewRenderer.viewport.drawScale() - this._previewRenderer.viewport.pos.x, (this._mousePos.y - this._previewRenderer.viewport.deviceSize.h / 2) / this._previewRenderer.viewport.drawScale() - this._previewRenderer.viewport.pos.y);
    };
    TwoDCanvas.prototype.bindUIEvents = function () {
        var self = this;
        // Redraw for responsiveness...
        $(window).resize(function () {
            self.resize();
            self.redraw();
        });
        // Right click bind
        $(this._previewCanvas).on('contextmenu', this._previewCanvas, function (e) { return false; });
        // Drag n drop interactions - drag over
        this._previewCanvas.addEventListener("dragover", function (event) {
            event.preventDefault(); // allow
            // Update cursor
            self._updateCursor(event);
        });
        // Drag n drop interactions - drop
        this._previewCanvas.addEventListener("drop", function (event) {
            event.preventDefault();
            // Image drop
            var imageData = event.dataTransfer.getData("image");
            if (imageData != null) {
                new TwoDEntityImage()
                    .setAutoSize(true)
                    .setImage(imageData)
                    .setPos(self.cursorPos)
                    .addToCanvas(self);
            }
            // Type drop
            var typeData = event.dataTransfer.getData("type");
            if (typeData != null) {
                var newEntity = new window[typeData]();
                if (newEntity != null) {
                    newEntity.setLabel(event.dataTransfer.getData("name"));
                    newEntity.setPos(self.cursorPos);
                    newEntity.addToCanvas(self);
                }
            }
        });
        // Mouse interactions - mouse down
        this._previewCanvas.addEventListener("mousedown", function (event) {
            //event.preventDefault(); // this will prevent focus, we need focus for key events
            event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            // If we are locked, dont update
            if (event.which == 1) {
                if (self._lockedInteractionMode == null) {
                    // Left mouse
                    if (self._interactionPossibility != TwoDCanvasInteractionMode.NONE) {
                        self.setInteractionMode(self._interactionPossibility);
                        if (self._interactionMode == TwoDCanvasInteractionMode.SELECT) {
                            self.selectEntity(self.hoveredEntity);
                            self.setInteractionMode(TwoDCanvasInteractionMode.DRAG);
                        }
                        else if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                            if (self.selectedEntity != null) {
                                self.selectedEntityGrabOffset = self.selectedEntity.worldPos().subtract(self.cursorPos);
                            }
                        }
                    }
                    else {
                        // Deselect
                        self.selectEntity(null);
                    }
                }
                else {
                    self.setInteractionMode(self._lockedInteractionMode);
                }
            }
            else if (event.which == 2) {
                // Middle mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PAN);
            }
            else if (event.which == 3) {
                // Right mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PAN);
            }
            else {
                // Unknown
                self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            }
            /*
            if (event.which == 1 && self._interactionMode == TwoDCanvasInteractionMode.NONE) {
                // Left mouse - select entity

                let newSelectedEntity = self.getEntityAtPosition(self.cursorPos);
                if (newSelectedEntity != null) {
                    if (self.selectedEntity != null) self.selectedEntity.onDeselected();
                    self.selectedEntityGrabOffset = newSelectedEntity.worldPos().subtract(self.cursorPos);
                    newSelectedEntity.onSelected();
                } else {
                    if (self._interactionPossibility == TwoDCanvasInteractionMode.NONE) {
                        self.selectedEntity = null; // set to null
                    }
                }
            }*/
            self.redraw();
        });
        // Mouse interactions - mouse up
        this._previewCanvas.addEventListener("mouseup", function (event) {
            event.preventDefault();
            event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            // End drag?
            if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                self.selectedEntity.snapToClosebySnapPoints();
            }
            self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            self._updatePointer();
            self.redraw();
        });
        // Mouse interactions - mouse move
        this._previewCanvas.addEventListener("mousemove", function (event) {
            event.preventDefault();
            event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            /*
            if (event.which == 0) {
                self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            } else if (event.which == 1) {
                // Left mouse
                if (self._interactionPossibility == TwoDCanvasInteractionMode.DRAGGING) {
                    self.setInteractionMode(TwoDCanvasInteractionMode.DRAGGING);
                } else if (self._interactionPossibility == TwoDCanvasInteractionMode.ROTATING) {
                    self.setInteractionMode(TwoDCanvasInteractionMode.ROTATING);
                }
            } else if (event.which == 2) {
                // Middle mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PANNING);
            } else if (event.which == 3) {
                // Right mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PANNING);
            } else {
                self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            }*/
            // If we are locked, dont update
            {
                // Move entity
                if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                    if (self.selectedEntity != null && self.selectedEntity.canMove == true) {
                        self.selectedEntity.setWorldPos(self.cursorPos.add(self.selectedEntityGrabOffset));
                    }
                }
                // Rotate entity
                if (self._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                    if (self.selectedEntity != null && self.selectedEntity.canRotate) {
                        self.selectedEntity.pointTo(self.cursorPos);
                    }
                }
                // Pan
                if (self._interactionMode == TwoDCanvasInteractionMode.PAN) {
                    self._previewRenderer.viewport.pos.x += (self._mousePos.x - self._lastMousePos.x) / self._previewRenderer.viewport.drawScale();
                    self._previewRenderer.viewport.pos.y += (self._mousePos.y - self._lastMousePos.y) / self._previewRenderer.viewport.drawScale();
                }
                if (self._interactionMode == TwoDCanvasInteractionMode.NONE && self._lockedInteractionMode == null) {
                    // Update hovered entity
                    self.hoveredEntity = self.getEntityAtPosition(self.cursorPos);
                    var newInteractionPossibility = TwoDCanvasInteractionMode.NONE;
                    // Update possibilities
                    if (self.hoveredEntity != null) {
                        if (self.hoveredEntity != self.selectedEntity) {
                            newInteractionPossibility = TwoDCanvasInteractionMode.SELECT;
                        }
                        else if (self.hoveredEntity != null && self.hoveredEntity.canMove == true) {
                            newInteractionPossibility = TwoDCanvasInteractionMode.DRAG;
                        }
                    }
                    // Test for handles
                    if (self.selectedEntity != null && self.selectedEntity.canRotate) {
                        var rotateHandle = self.selectedEntity.rotateHandlePos();
                        if (rotateHandle.dist(self.cursorPos) < self.config.rotateHandleSize / self._previewRenderer.viewport.drawScale()) {
                            newInteractionPossibility = TwoDCanvasInteractionMode.ROTATE;
                        }
                    }
                    self.setInteractionPossibility(newInteractionPossibility);
                }
            }
            self._updatePointer();
            self.redraw();
        }, false);
        // Mouse interactions - scroll wheel
        this._previewCanvas.addEventListener("wheel", function (event) {
            event.preventDefault();
            event.stopPropagation();
            var factor = 1.1;
            if (event.deltaY > 0)
                self.zoomOut(factor, true);
            else if (event.deltaY < 0)
                self.zoomIn(factor, true);
        }, false);
        // Focus event
        this._previewCanvasElem.on("focus", function () {
        });
        // Keyboard interactions - key up
        window.addEventListener("keyup", function (event) {
            if (!self._previewCanvasElem.is(":focus"))
                return;
            event.preventDefault();
            event.stopPropagation();
            // Delete
            if (event.key == "Delete") {
                if (self.selectedEntity != null) {
                    if (self.selectedEntity.deletesParent == true && self.selectedEntity.parent != null) {
                        self.selectedEntity.parent.delete();
                    }
                    else {
                        self.selectedEntity.delete();
                    }
                    self.selectedEntity = null;
                }
                self.redraw();
            }
        });
    };
    TwoDCanvas.prototype.selectEntity = function (entity) {
        var self = this;
        Machinata.debug("selectEntity: " + entity);
        if (entity != null) {
            self.selectedEntity = entity;
            self.selectedEntityGrabOffset = self.selectedEntity.worldPos().subtract(self.cursorPos);
        }
        else {
            self.selectedEntity = entity;
        }
        if (self.onEntitySelected)
            self.onEntitySelected(self, entity);
    };
    TwoDCanvas.prototype.lockInteractionMode = function (mode) {
        this._lockedInteractionMode = mode;
        this.setInteractionMode(TwoDCanvasInteractionMode.NONE);
    };
    TwoDCanvas.prototype.unlockInteractionMode = function () {
        this._lockedInteractionMode = null;
        this.setInteractionMode(TwoDCanvasInteractionMode.NONE);
    };
    TwoDCanvas.prototype.setInteractionMode = function (mode) {
        if (this._interactionMode != mode) {
            Machinata.debug("setInteractionMode: new mode: " + mode);
            this._interactionMode = mode;
            if (mode != TwoDCanvasInteractionMode.NONE) {
                this._interactionStartCursorPos = this.cursorPos.clone();
            }
            else {
                this._interactionEndCursorPos = this.cursorPos.clone();
            }
        }
    };
    TwoDCanvas.prototype.setInteractionPossibility = function (mode) {
        if (this._interactionPossibility != mode) {
            Machinata.debug("setInteractionPossibility: new possibility: " + mode);
            this._interactionPossibility = mode;
        }
    };
    TwoDCanvas.prototype.zoomIn = function (factor, fromCursor) {
        this.setZoom(this._previewRenderer.viewport.zoom * factor, fromCursor);
    };
    TwoDCanvas.prototype.zoomOut = function (factor, fromCursor) {
        this.setZoom(this._previewRenderer.viewport.zoom / factor, fromCursor);
    };
    TwoDCanvas.prototype.setZoom = function (newZoom, fromCursor) {
        if (newZoom > this.maxZoom)
            newZoom = this.maxZoom;
        if (newZoom < this.minZoom)
            newZoom = this.minZoom;
        var lastZoom = this._previewRenderer.viewport.zoom;
        var focalPos = fromCursor ? this.cursorPos : new gpos(0, 0);
        var mouseOffset = fromCursor ? this._mousePos : new gpos(0, 0);
        // Match cursor and canvas coord before and after zoom
        // Consider that the same page coordinates need to match the same canvas coordinates 
        // before and after the zoom.Then you can do some algebra starting from this equation:
        //   (pageCoords - translation) / scale = canvasCoords
        if (fromCursor) {
            var newZoomScaled = this._previewRenderer.viewport.drawScaleForZoom(newZoom);
            this._previewRenderer.viewport.pos = new gpos(-this.cursorPos.x + (this._mousePos.x - this._previewRenderer.viewport.deviceSize.w / 2) / newZoomScaled, -this.cursorPos.y + (this._mousePos.y - this._previewRenderer.viewport.deviceSize.h / 2) / newZoomScaled);
        }
        this._previewRenderer.viewport.setZoom(newZoom);
        this._updateCursor(null);
        this._updatePointer();
        this.redraw();
    };
    TwoDCanvas.prototype.zoomFit = function (factor) {
        var bounds = this.rootEntity.worldBounds();
        // Set to center
        this._previewRenderer.viewport.pos = new gpos(-bounds.center.x, -bounds.center.y);
        // Get zoom to fit
        var scaleFactor = this._previewRenderer.viewport.drawScaleForZoom(1.0); // We need this factor since our actual draw scale is different from the zoom
        var newZoomX = (this._previewRenderer.viewport.deviceSize.w / scaleFactor) / bounds.size.w;
        var newZoomY = (this._previewRenderer.viewport.deviceSize.h / scaleFactor) / bounds.size.h;
        var newZoom = Math.min(newZoomX, newZoomY);
        this.setZoom(newZoom - (newZoom * 0.01), false);
        this._updateCursor(null);
        this._updatePointer();
        this.redraw();
    };
    TwoDCanvas.prototype.resize = function () {
        this._previewRenderer.viewport.deviceSize = new gsize(this._previewCanvas.width, this._previewCanvas.height);
        this._previewRenderer.viewport.viewSize = this._previewRenderer.viewport.deviceSize.mul(1.0 / this._previewRenderer.viewport.drawScale());
    };
    TwoDCanvas.prototype.getEntityAtPosition = function (pos) {
        if (this.rootEntity.worldBounds().containsPoint(pos)) {
            var child = this.rootEntity.getChildAtPoint(pos);
            if (child != null)
                return child;
            else
                return null;
        }
        return null;
    };
    TwoDCanvas.prototype.redraw = function () {
        Machinata.debug("TwoDCanvas.redraw()");
        // TESTING
        //if (this.selectedEntity) this.selectedEntity.setRot(this.selectedEntity._rot += 0.01);
        // Call render pipeline
        // First we mark as being drawn, and if someone has requested a redraw we set that to false since we are just about to do that...
        this._isDrawing = true;
        //if (this._redrawRequested == true) this._redrawRequested = false;
        this._renderWithRenderer(this._previewRenderer);
        this._isDrawing = false;
        if (this._redrawRequested == true) {
            this.redraw();
        }
    };
    TwoDCanvas.prototype.requestRedraw = function () {
        if (this._redrawRequested == true)
            return;
        if (this._isDrawing == false) {
            this.redraw();
        }
        else {
            this._redrawRequested = true;
        }
    };
    TwoDCanvas.prototype._renderWithRenderer = function (renderer) {
        Machinata.debug("TwoDCanvas._drawWithRenderer()");
        // Stats
        renderer.statsStartDrawTime = new Date();
        renderer.statsTotalDraws = 0;
        renderer.debug = this.debug;
        // Init
        var ctx = renderer.ctx;
        // Before we touch anything, we save the context so that at the very end we can reset it again
        ctx.save(); //TODO: why does the stack not reset on getcontext?
        // Clear bg
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, renderer.viewport.deviceSize.w, renderer.viewport.deviceSize.h);
        // Translate to center
        ctx.translate(Math.round(renderer.viewport.deviceSize.w / 2), Math.round(renderer.viewport.deviceSize.h / 2));
        // Draw crosshair viewport
        if (this.debug == true) {
            var crossHairSize = 20.0;
            ctx.fillStyle = "black";
            ctx.fillRect(0, -crossHairSize / 2, 1, crossHairSize);
            ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, 1);
        }
        // Draw grid
        if (renderer.viewport.gridEnabled) {
            // To ensure pixel-perfect grid lines, we draw the grid before the view transformation...
            ctx.strokeStyle = this.config.gridLineColor;
            ctx.lineWidth = this.config.gridLineSize;
            var gridSize = renderer.viewport.gridSize;
            var pixelOffset = 0.5;
            //if (renderer.viewport.zoom > 5) gridSize = gridSize / 10; //TODO: dynamic grid size multiples of 10
            ctx.save();
            {
                // Move according to viewport (x only)
                var vptransx = renderer.viewport.pos.x * renderer.viewport.drawScale();
                var vptransy = renderer.viewport.pos.y * renderer.viewport.drawScale();
                //ctx.translate(vptransx, 0);
                var vpxo = (Math.floor(-(renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) / gridSize) + 1) * gridSize;
                ctx.beginPath();
                for (var xview = 0 + vpxo; xview <= renderer.viewport.viewSize.w + vpxo; xview += gridSize) {
                    var xdevice = Math.round(vptransx + xview * renderer.viewport.drawScale());
                    ctx.moveTo(xdevice + pixelOffset, -renderer.viewport.deviceSize.h / 2);
                    ctx.lineTo(xdevice + pixelOffset, renderer.viewport.deviceSize.h / 2);
                }
                ctx.closePath();
                ctx.stroke();
                // Undo previous move (x), and move according to viewport (y only)
                //ctx.translate(-vptransx, vptransy);
                //ctx.translate(0, vptransy);
                var vpyo = (Math.floor(-(renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2) / gridSize) + 1) * gridSize;
                ctx.beginPath();
                for (var yview = 0 + vpyo; yview <= renderer.viewport.viewSize.h + vpyo; yview += gridSize) {
                    var ydevice = Math.round(vptransy + yview * renderer.viewport.drawScale());
                    ctx.moveTo(-renderer.viewport.deviceSize.w / 2, ydevice + pixelOffset);
                    ctx.lineTo(renderer.viewport.deviceSize.w / 2, ydevice + pixelOffset);
                }
                ctx.closePath();
                ctx.stroke();
            }
            ctx.restore();
        }
        // Draw scale?
        if (renderer.viewport.scaleEnabled) {
            ctx.save();
            {
                var scaleBlocksToShow = 10; // meters
                var mmPerBlock = 1000; // 1 meter
                var blockW = mmPerBlock * renderer.viewport.drawScale();
                var blockH = 10; // in pixels
                ctx.translate(-renderer.viewport.deviceSize.w / 2, +renderer.viewport.deviceSize.h / 2);
                ctx.translate(blockH, -blockH * 2);
                for (var i = 0; i < scaleBlocksToShow; i++) {
                    var blockX = (i * blockW);
                    var blockY = 0;
                    ctx.fillStyle = i % 2 == 0 ? "black" : "white";
                    ctx.fillRect(blockX, blockY, blockW, blockH);
                }
                // Block label
                ctx.font = renderer.viewport.labelFontSize + "px " + renderer.viewport.labelFontName;
                ctx.fillStyle = "black";
                ctx.fillText(this.getMetricUnitLabel(mmPerBlock), 0, -blockH / 2);
                ctx.textAlign = "right";
                ctx.fillText(this.getMetricUnitLabel((mmPerBlock * scaleBlocksToShow)), blockW * scaleBlocksToShow, -blockH / 2);
                ctx.textAlign = "center";
                //ctx.fillText((renderer.viewport.physicalScale * 100) + ":100 @ " + renderer.viewport.dpi + "dpi", blockW * scaleBlocksToShow / 2, -blockH / 2);
                var viewportScaleB = 100;
                var viewportScaleA = (renderer.viewport.scale * viewportScaleB) * renderer.viewport.zoom;
                viewportScaleB = viewportScaleB / viewportScaleA;
                viewportScaleA = (renderer.viewport.scale * viewportScaleB) * renderer.viewport.zoom;
                viewportScaleA = Math.round(viewportScaleA);
                viewportScaleB = Math.round(viewportScaleB);
                ctx.fillText(viewportScaleA + ":" + viewportScaleB, blockW * scaleBlocksToShow / 2, -blockH / 2);
                // Border
                ctx.lineWidth = 1.0;
                ctx.strokeStyle = "black";
                ctx.strokeRect(0, 0, blockW * scaleBlocksToShow, blockH);
            }
            ctx.restore();
        }
        // Zoom
        ctx.scale(renderer.viewport.drawScale(), renderer.viewport.drawScale());
        // Translate (pos)
        ctx.translate(renderer.viewport.pos.x, renderer.viewport.pos.y);
        // Draw crosshair origin
        if (this.debug == true) {
            var crossHairSize = 20.0 / renderer.viewport.drawScale();
            var crossHairWidth = 1.0 / renderer.viewport.drawScale();
            ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
            ctx.fillStyle = "blue";
            ctx.fillRect(0, -crossHairSize / 2, crossHairWidth, crossHairSize);
            ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, crossHairWidth);
        }
        // Draw cursor
        if (this.debug == true) {
            var crossHairSize = 20.0 / renderer.viewport.drawScale();
            var crossHairWidth = 1.0 / renderer.viewport.drawScale();
            ctx.save();
            {
                ctx.translate(this.cursorPos.x, this.cursorPos.y);
                ctx.fillStyle = "red";
                ctx.fillRect(0, -crossHairSize / 2, crossHairWidth, crossHairSize);
                ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, crossHairWidth);
            }
            ctx.restore();
        }
        // Draw viewport bounds
        if (this.debug == true) {
            ctx.save();
            {
                var bounds = renderer.viewport.worldBounds();
                bounds.size.w = bounds.size.w - 10.0 / renderer.viewport.drawScale();
                bounds.size.h = bounds.size.h - 10.0 / renderer.viewport.drawScale();
                ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
                ctx.fillStyle = "black";
                ctx.strokeRect(bounds.center.x - bounds.size.w / 2, bounds.center.y - bounds.size.h / 2, bounds.size.w, bounds.size.h);
            }
            ctx.restore();
        }
        // Begin redraw chain
        ctx.save();
        {
            this.rootEntity._drawEntity(renderer);
        }
        ctx.restore();
        ctx.save();
        {
            this.rootEntity._drawUI(renderer);
        }
        ctx.restore();
        // Final restore
        ctx.restore();
        // Stats
        renderer.statsEndDrawTime = new Date();
        renderer.statsTotalDrawTimeMS = renderer.statsEndDrawTime.getTime() - renderer.statsStartDrawTime.getTime();
        renderer.statsTotalDrawTimeFPS = 1000 / renderer.statsTotalDrawTimeMS;
        if (renderer.debug) {
            console.log("  rendered in " + renderer.statsTotalDrawTimeMS + " ms (" + renderer.statsTotalDrawTimeFPS + " fps)");
        }
        // Debug overlay
        if (renderer.debug) {
            ctx.save();
            {
                var text = "INFO";
                text += "\n" + "cursor: " + this.cursorPos.x + " " + this.cursorPos.y;
                text += "\n" + "zoom: " + renderer.viewport.zoom;
                text += "\n" + "draw scale: " + renderer.viewport.drawScale();
                text += "\n" + "vp-tl: " + (renderer.viewport.pos.x - renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y - renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-tr: " + (renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y - renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-bl: " + (renderer.viewport.pos.x - renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-br: " + (renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2);
                text += "\n" + "i-mode: " + this._interactionMode;
                text += "\n" + "i-pos: " + this._interactionPossibility;
                text += "\n" + "selected: " + this.selectedEntity;
                text += "\n" + "hover: " + this.hoveredEntity;
                text += "\n" + "";
                text += "\n" + "STATS";
                text += "\n" + "ms: " + renderer.statsTotalDrawTimeMS;
                text += "\n" + "fps: " + renderer.statsTotalDrawTimeFPS;
                text += "\n" + "draws: " + renderer.statsTotalDraws;
                ctx.textBaseline = "top";
                ctx.fillStyle = "black";
                ctx.font = "10px Helvetica";
                ctx.fillMultilineText(text, 10, 10);
            }
            ctx.restore();
        }
    };
    TwoDCanvas.prototype.exportPDF = function () {
        // Create a HTMLCanvas renderer
        var renderer = new PDFCanvasTwoDCanvasRenderer();
        renderer.viewport.units = this.config.units;
        //TODO: modify viewport as wanted
        var ctx = renderer.createDrawingContext();
        // Call render pipeline
        this._renderWithRenderer(renderer);
        // Setup a download for PDF
        var mimetype = "application/pdf";
        var filename = "export.pdf";
        //convert your PDF to a Blob and save to file
        ctx.stream.on('finish', function () {
            var blob = ctx.stream.toBlob('application/pdf');
            Machinata.Data.exportBlob(blob, mimetype, filename);
        });
        ctx.end();
    };
    TwoDCanvas.prototype.exportPNG = function () {
        // Create a HTMLCanvas renderer
        var renderer = new HTMLCanvasTwoDCanvasRenderer();
        //TODO: modify viewport as wanted
        renderer.viewport.units = this.config.units;
        renderer.viewport.fitBounds(this.rootEntity.worldBounds());
        renderer.createDrawingContext();
        // Call render pipeline
        this._renderWithRenderer(renderer);
        // Setup a download for PNG
        var mimetype = "image/png";
        var filename = "export.png";
        renderer.canvas.toBlob(function (blob) {
            Machinata.Data.exportBlob(blob, mimetype, filename);
        }, mimetype);
    };
    TwoDCanvas.prototype.getMetricUnitLabel = function (mm, precision) {
        if (precision === void 0) { precision = null; }
        function applyPrecision(val) {
            if (precision == null)
                return val;
            return val.toFixed(precision);
        }
        // Convert to m
        var m = mm / 1000;
        if (m < 1.0) {
            var cm = mm / 10;
            if (cm < 1.0) {
                return applyPrecision(mm) + "mm";
            }
            return applyPrecision(cm) + "cm";
        }
        return applyPrecision(m) + "m";
    };
    return TwoDCanvas;
}());
Machinata.TwoD.Canvas = TwoDCanvas;
