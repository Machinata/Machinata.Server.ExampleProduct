declare class TwoDEntityMeasuringTape extends TwoDEntity {
    a: TwoDEntity;
    b: TwoDEntity;
    _tapeSize: number;
    constructor();
    draw(renderer: TwoDCanvasRenderer): void;
}
