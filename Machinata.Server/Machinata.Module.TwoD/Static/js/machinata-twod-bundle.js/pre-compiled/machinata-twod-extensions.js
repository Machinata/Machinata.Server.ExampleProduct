Object.defineProperty(CanvasRenderingContext2D.prototype, 'fillMultilineText', {
    value: function (text, x, y) {
        var lines = text.split("\n");
        var fontSize = parseInt(this.font.split("px")[0]);
        for (var i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});
Object.defineProperty(canvas2pdf.PdfContext.prototype, 'fillMultilineText', {
    value: function (text, x, y) {
        var lines = text.split("\n");
        var fontSize = parseInt(this.font.split("px")[0]);
        for (var i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});
