/// <type>class</type>
var TwoDCanvasConfig = /** @class */ (function () {
    function TwoDCanvasConfig() {
        this.rotateHandleSize = 5.0;
        this.rotateHandleLineSize = 2.0;
        this.rotateHandleColor = "red";
        this.selectedBorderSize = 2.0;
        this.selectedBorderColor = "red";
        this.hoveredBorderSize = 1.0;
        this.hoveredBorderColor = "gray";
        this.gridLineSize = 1.0;
        this.gridLineColor = "rgba(0,0,0,0.2)";
        this.snapPointSize = 8.0;
        this.snapPointColor = "gray";
        this.snapPointSnappingDistance = 20;
        this.units = "mm";
    }
    return TwoDCanvasConfig;
}());
