var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDCanvasRenderer = /** @class */ (function () {
    function TwoDCanvasRenderer() {
        this.viewport = new TwoDCanvasViewport();
        this.debug = true;
        this.drawLabels = true;
    }
    TwoDCanvasRenderer.prototype.setShapeStyle = function () {
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.viewport.drawScale();
    };
    TwoDCanvasRenderer.prototype.drawLabel = function (entity, text, x, y, alignment) {
        var drawLabel = true;
        if (this.drawLabels == false) {
            // Only draw labels on hover/select
            if (entity.canvas.selectedEntity != entity && entity.canvas.hoveredEntity != entity)
                drawLabel = false;
        }
        if (!drawLabel)
            return;
        var ctx = this.ctx;
        ctx.save();
        {
            ctx.textAlign = alignment;
            ctx.textBaseline = "middle";
            ctx.font = (this.viewport.labelFontSize / this.viewport.drawScale()) + "px " + this.viewport.labelFontName;
            var meas = ctx.measureText(text);
            ctx.fillStyle = "rgba(0,0,0,0.8)";
            var padding = 2 / this.viewport.drawScale();
            var w = meas.width;
            var h = meas.actualBoundingBoxAscent + meas.actualBoundingBoxDescent;
            ctx.fillRect(x - w / 2 - padding, y - h / 2 - padding, w + padding + padding, h + padding + padding);
            ctx.fillStyle = "white";
            ctx.fillText(text, x, y);
        }
        ctx.restore();
    };
    return TwoDCanvasRenderer;
}());
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var HTMLCanvasTwoDCanvasRenderer = /** @class */ (function (_super) {
    __extends(HTMLCanvasTwoDCanvasRenderer, _super);
    function HTMLCanvasTwoDCanvasRenderer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HTMLCanvasTwoDCanvasRenderer.prototype.createDrawingContext = function () {
        // Create the canvas
        var canvas = document.createElement('canvas');
        canvas.width = this.viewport.deviceSize.w;
        canvas.height = this.viewport.deviceSize.h;
        this.canvas = canvas;
        // Set the drawing context
        var ctx = canvas.getContext('2d');
        this.ctx = ctx;
        return ctx;
    };
    return HTMLCanvasTwoDCanvasRenderer;
}(TwoDCanvasRenderer));
/// <summary>
/// Calling fill and then stroke consecutively only executes fill
/// Some canvas 2d context methods are not implemented yet(e.g.setTransform and arcTo)
/// See https://github.com/dankrusi/canvas2pdf
/// </summary>
/// <type>class</type>
var PDFCanvasTwoDCanvasRenderer = /** @class */ (function (_super) {
    __extends(PDFCanvasTwoDCanvasRenderer, _super);
    function PDFCanvasTwoDCanvasRenderer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PDFCanvasTwoDCanvasRenderer.prototype.createDrawingContext = function () {
        //Create a new PDF canvas context.
        var ctx = new canvas2pdf.PdfContext(blobStream());
        this.ctx = ctx;
        return ctx;
    };
    return PDFCanvasTwoDCanvasRenderer;
}(TwoDCanvasRenderer));
