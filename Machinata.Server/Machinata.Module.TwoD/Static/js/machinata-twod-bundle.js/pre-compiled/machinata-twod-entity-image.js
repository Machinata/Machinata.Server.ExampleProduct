var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityImage = /** @class */ (function (_super) {
    __extends(TwoDEntityImage, _super);
    function TwoDEntityImage() {
        var _this = _super.call(this) || this;
        _this._imageURL = null;
        _this._imageLoaded = false;
        _this._autoSize = false;
        var self = _this;
        _this._imageURL = null;
        _this._imageLoaded = false;
        _this.image = new Image();
        _this.image.onload = function () {
            self._imageLoaded = true;
            console.log("TwoDEntityImage: image loaded: " + self.image.src);
            if (self._autoSize == true) {
                self.setSize(new gsize(self.image.width, self.image.height));
            }
            if (self.canvas != null)
                self.canvas.requestRedraw();
        };
        // Properties
        _this.props.push(new TwoDEntityProp()
            .title("{text.twod.image}")
            .type("image")
            .defaultValue(null)
            .getter(function (entity) { return entity._imageURL; })
            .setter(function (entity, val) { entity.setImage(val); }));
        return _this;
    }
    TwoDEntityImage.prototype.setAutoSize = function (autoSize) {
        this._autoSize = autoSize;
        return this;
    };
    TwoDEntityImage.prototype.setImage = function (url) {
        this._imageLoaded = false;
        this._imageURL = url;
        this.image.src = url;
        return this;
    };
    TwoDEntityImage.prototype.draw = function (renderer) {
        var ctx = renderer.ctx;
        if (this._imageLoaded == true) {
            ctx.drawImage(this.image, -this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
        }
    };
    return TwoDEntityImage;
}(TwoDEntity));
Machinata.TwoD.EntityImage = TwoDEntityImage;
