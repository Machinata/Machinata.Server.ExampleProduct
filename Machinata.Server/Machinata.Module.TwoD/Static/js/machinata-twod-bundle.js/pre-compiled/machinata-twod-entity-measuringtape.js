var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityMeasuringTape = /** @class */ (function (_super) {
    __extends(TwoDEntityMeasuringTape, _super);
    function TwoDEntityMeasuringTape() {
        var _this = _super.call(this) || this;
        _this._tapeSize = 100;
        var self = _this;
        self._group = true;
        self.canRotate = false;
        self.canMove = false;
        //self.setSize(new gsize(0, this._tapeSize));
        self.a = new TwoDEntityCircle();
        self.a.setSize(new gsize(_this._tapeSize, _this._tapeSize));
        self.a.canRotate = false;
        self.a.deletesParent = true;
        _this.addChild(self.a);
        self.b = new TwoDEntityCircle();
        self.b.setSize(new gsize(_this._tapeSize, _this._tapeSize));
        self.b.canRotate = false;
        self.b.setPos(new gpos(1000, 0));
        self.b.deletesParent = true;
        _this.addChild(self.b);
        return _this;
    }
    TwoDEntityMeasuringTape.prototype.draw = function (renderer) {
        var ctx = renderer.ctx;
        ctx.strokeStyle = "black";
        ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
        ctx.beginPath();
        ctx.moveTo(this.a._pos.x, this.a._pos.y);
        ctx.lineTo(this.b._pos.x, this.b._pos.y);
        ctx.closePath();
        ctx.stroke();
        var dist = this.a._pos.dist(this.b._pos);
        var labelPos = this.b._pos.add(this.a._pos).mul(0.5);
        renderer.drawLabel(this, this.canvas.getMetricUnitLabel(dist, 2), labelPos.x, labelPos.y, "center");
    };
    return TwoDEntityMeasuringTape;
}(TwoDEntity));
Machinata.TwoD.EntityMeasuringTape = TwoDEntityMeasuringTape;
