declare class TwoDEntityRuler extends TwoDEntity {
    constructor();
    setLength(length: number): void;
    draw(renderer: TwoDCanvasRenderer): void;
}
