declare enum TwoDCanvasInteractionMode {
    NONE = "NONE",
    DRAG = "DRAG",
    PAN = "PAN",
    ROTATE = "ROTATE",
    SELECT = "SELECT"
}
declare class TwoDCanvas {
    _previewCanvasElem: any;
    _previewCanvas: HTMLCanvasElement;
    _previewRenderer: TwoDCanvasRenderer;
    _isDrawing: boolean;
    _redrawRequested: boolean;
    debug: boolean;
    rootEntity: TwoDEntity;
    selectedEntity: TwoDEntity;
    selectedEntityGrabOffset: gpos;
    config: TwoDCanvasConfig;
    hoveredEntity: TwoDEntity;
    onEntitySelected: Function;
    onEntityDeleted: Function;
    onEntityMoved: Function;
    onEntityRotated: Function;
    maxZoom: number;
    minZoom: number;
    cursorPos: gpos;
    _mousePos: gpos;
    _lastMousePos: gpos;
    _lastCursorPos: gpos;
    _interactionMode: TwoDCanvasInteractionMode;
    _interactionPossibility: TwoDCanvasInteractionMode;
    _lockedInteractionMode: TwoDCanvasInteractionMode;
    _interactionStartCursorPos: gpos;
    _interactionEndCursorPos: gpos;
    constructor(canvasElem: any);
    addEntity(entity: TwoDEntity): TwoDCanvas;
    mmToPx(mm: number): number;
    _debugLoadTestScene(): void;
    _debugDrawPoint(pos: gpos): void;
    _updatePointer(): void;
    _updateCursor(event: MouseEvent): void;
    bindUIEvents(): void;
    selectEntity(entity: TwoDEntity): void;
    lockInteractionMode(mode: TwoDCanvasInteractionMode): void;
    unlockInteractionMode(): void;
    setInteractionMode(mode: TwoDCanvasInteractionMode): void;
    setInteractionPossibility(mode: TwoDCanvasInteractionMode): void;
    zoomIn(factor: number, fromCursor: boolean): void;
    zoomOut(factor: number, fromCursor: boolean): void;
    setZoom(newZoom: any, fromCursor: boolean): void;
    zoomFit(factor: number): void;
    resize(): void;
    getEntityAtPosition(pos: gpos): TwoDEntity;
    redraw(): void;
    requestRedraw(): void;
    _renderWithRenderer(renderer: TwoDCanvasRenderer): void;
    exportPDF(): void;
    exportPNG(): void;
    getMetricUnitLabel(mm: number, precision?: number): string;
}
