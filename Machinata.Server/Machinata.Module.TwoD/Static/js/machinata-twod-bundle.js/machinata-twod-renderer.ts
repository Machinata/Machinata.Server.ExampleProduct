
/// <summary>
/// 
/// </summary>
/// <type>class</type>
abstract class TwoDCanvasRenderer {

    ctx: CanvasRenderingContext2D;
    viewport: TwoDCanvasViewport = new TwoDCanvasViewport();

    statsStartDrawTime: Date;
    statsEndDrawTime: Date;
    statsTotalDrawTimeMS: number;
    statsTotalDrawTimeFPS: number;
    statsTotalDraws: number;

    debug: boolean = true;

    drawLabels: boolean = true;

    abstract createDrawingContext(): any;

    setShapeStyle() {
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.viewport.drawScale();
    }

    drawLabel(entity: TwoDEntity, text: string, x: number, y: number, alignment: CanvasTextAlign) {
        let drawLabel = true;
        if (this.drawLabels == false) {
            // Only draw labels on hover/select
            if (entity.canvas.selectedEntity != entity && entity.canvas.hoveredEntity != entity) drawLabel = false;
        }
        if (!drawLabel) return;
        

        let ctx = this.ctx;

        ctx.save(); {
            ctx.textAlign = alignment;
            ctx.textBaseline = "middle";
            ctx.font = (this.viewport.labelFontSize / this.viewport.drawScale()) + "px " + this.viewport.labelFontName;
            
            var meas = ctx.measureText(text);
            ctx.fillStyle = "rgba(0,0,0,0.8)";
            var padding = 2 / this.viewport.drawScale();
            var w = meas.width;
            var h = meas.actualBoundingBoxAscent + meas.actualBoundingBoxDescent;
            ctx.fillRect(x - w / 2 - padding, y - h / 2 - padding, w + padding + padding, h + padding + padding);
            ctx.fillStyle = "white";
            ctx.fillText(text, x, y);
        } ctx.restore();
    }
}

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class HTMLCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {

    canvas: HTMLCanvasElement;

    createDrawingContext(): any {
        // Create the canvas
        var canvas = document.createElement('canvas');
        canvas.width = this.viewport.deviceSize.w;
        canvas.height = this.viewport.deviceSize.h;
        this.canvas = canvas;

        // Set the drawing context
        var ctx = canvas.getContext('2d');
        this.ctx = ctx;
        return ctx;
    }
}

/// <summary>
/// Calling fill and then stroke consecutively only executes fill
/// Some canvas 2d context methods are not implemented yet(e.g.setTransform and arcTo)
/// See https://github.com/dankrusi/canvas2pdf
/// </summary>
/// <type>class</type>
class PDFCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {

    canvas: HTMLCanvasElement;

    createDrawingContext(): any {
        //Create a new PDF canvas context.
        var ctx = new canvas2pdf.PdfContext(blobStream());
        this.ctx = ctx;
        return ctx;
    }
}