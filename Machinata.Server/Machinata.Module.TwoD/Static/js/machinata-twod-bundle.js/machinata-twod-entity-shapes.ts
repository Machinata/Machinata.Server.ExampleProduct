

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityCircle extends TwoDEntity {


    fillColor: string = "black";

    constructor() {
        super();
        let self = this;
    }

    draw(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        ctx.ellipse(0, 0, this._size.w / 2, this._size.h / 2, 0, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    }

}
Machinata.TwoD.EntityCircle = TwoDEntityCircle;
