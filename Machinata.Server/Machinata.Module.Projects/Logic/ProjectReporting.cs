using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Logic {



    public static class ProjectReporting {



        public static IEnumerable<ProjectCost> GetLongRunningTimeSheetEntries(ModelContext db, TimeSpan maxTime) {
            var longOpen = db.ProjectCosts().
                Where(pc => pc.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet 
                         && pc.TimeRange != null && pc.TimeRange.End == null
                         && pc.TimeRange.Start != null 
                         && (DateTime.UtcNow - pc.TimeRange.Start >= maxTime));


            return longOpen;
        }


        public static IEnumerable<Project> GetProjectsWithBudgetConsumption(ModelContext db, decimal minConsumption, decimal maxConsumption) {
            var projects = db.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Include(nameof(Project.Business))
                .Where(p => p.ProjectType == Project.ProjectTypes.Project && p.Completed == false && p.Archived == false && p.Business != null).ToList();
            var result = new List<Project>();

            foreach(var project in projects) {
                var budget = project.CalculateBudget(ProjectBudget.BudgetTypes.External);
                if (budget != null && budget.BudgetUsed.HasValue && budget.BudgetUsed >= minConsumption && budget.BudgetUsed < maxConsumption) {
                    result.Add(project);
                }
            }

            return result;
        }


        //public static IEnumerable<ProjectCost> (ModelContext db, TimeSpan maxTime) {
        //    var longOpen = db.ProjectCosts().
        //        Where(pc => pc.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet
        //                 && pc.TimeRange != null && pc.TimeRange.End == null
        //                 && pc.TimeRange.Start != null
        //                 && (DateTime.UtcNow - pc.TimeRange.Start >= maxTime));
        //    return longOpen;

        //    // todo long closed

        //}



    }
}
