using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Logic {
    public static class PageTemplateExtensions {

        /// <summary>
        /// Inserts a list of project costs
        /// Filters based on the request filters: budgets, billed, users, costtypes, worktypes
        /// Autohides columns based on the filters
        /// </summary>
        public static void InsertFilteredProjectCosts(this PageTemplateHandler handler, Project project, bool insertListing, FormBuilder form = null, FormBuilder formTotal = null, string link = null) {

            if (form == null) {
                form = new FormBuilder(Forms.Admin.LISTING)
                    .Include(nameof(ProjectCost.Created))
                    .Include(nameof(ProjectCost.WorkType));
            }
            if (formTotal == null) {
                formTotal = new FormBuilder(Forms.Admin.TOTAL);
            }

            var costsOfProject = project.GetAllCosts().Select(c => c.Id);
            var costsUnfiltered = handler.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(c => costsOfProject.Contains(c.Id))
                .ToList()
                .AsQueryable();

            var costs = costsUnfiltered;

            var budgetsFilter = handler.Params.String("budgets", null);
            var billedFilter = handler.Params.BoolNullable("billed", null);
            var userFilter = handler.Params.StringArray("users", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
            var workTypeFilter = handler.Params.StringArray("worktypes", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
            var costTypeFilter = handler.Params.String("costtype", null);
         

            // TODO: date filtering


            IEnumerable<User> filteredUsers = new List<User>();
            IEnumerable<ProjectWorkType> filteredWorkTypes = new List<ProjectWorkType>();


            // Budgets
            if (budgetsFilter == "internal") {
                form.Exclude(nameof(ProjectCost.CostPerUnit));
                form.Exclude(nameof(ProjectCost.Costs));
                form.Include(nameof(ProjectCost.CostPerUnitInternal));
            } else if (budgetsFilter == "external") {
                form.Exclude(nameof(ProjectCost.CostPerUnitInternal));
                form.Exclude(nameof(ProjectCost.CostsInternal));
            }

            // Billed?
            if (billedFilter == true) {
                costs = costs.Where(c => c.Billed != null);
            } else if (billedFilter == false) {
                costs = costs.Where(c => c.Billed == null);
                form.Exclude(nameof(ProjectCost.Billed));
            }

            // Users
            if (userFilter.Any()) {
                costs = costs.Where(c => c.User != null && userFilter.Contains(c.User.PublicId));
                filteredUsers = costs.Select(w => w.User);
                form.Exclude(nameof(ProjectCost.User));
            }

            // Worktypes
            if (workTypeFilter.Any()) {
                costs = costs.Where(c => c.WorkType != null && workTypeFilter.Contains(c.WorkType.PublicId));
                filteredWorkTypes = costs.Select(c => c.WorkType);
                form.Exclude(nameof(ProjectCost.WorkType));
            }

            // Costtype
            if (!string.IsNullOrEmpty(costTypeFilter)) {
                var filter = Core.Util.EnumHelper.ParseEnum<ProjectCost.ProjectCostTypes>(costTypeFilter);
                costs = costs.Where(c => c.ProjectCostType == filter);
                form.Exclude(nameof(ProjectCost.ProjectCostType));
            }
            

            DateTime? minDate = null;
            DateTime? maxDate = null;
            if (costs.Any()) {
                minDate = costs.Where(p => p.TimeRange != null && p.TimeRange.Start != null).Min(p => p.TimeRange.Start);
                maxDate = costs.Where(p => p.TimeRange != null && p.TimeRange.End != null).Max(p => p.TimeRange.End);
            }



            
            // Listing
            if (insertListing == true) {
                handler.Template.InsertEntityList(
                    entities: costs,
                    variableName: "entities",
                        form: form,
                        link: link,
                        total: formTotal
                );
            }
          

            // Filter
            handler.Template.InsertTemplates("user-filters", costsUnfiltered.Where(c => c.User != null).Select(c => c.User).Distinct(), "costs.filter", InsertFilterVariables);
            handler.Template.InsertTemplates("worktype-filters", costsUnfiltered.Where(c => c.WorkType != null).Select(c => c.WorkType).Distinct(), "costs.filter", InsertFilterVariables);

            // Query Param
            handler.Template.InsertVariable("query-string", handler.Context.Request.QueryString.ToString());

            // Vars
            handler.Template.InsertVariables("project", project);
            handler.Template.InsertVariable("budgets", string.IsNullOrEmpty(budgetsFilter) ? "all" : budgetsFilter);
            handler.Template.InsertVariable("internal-budget", (budgetsFilter == "internal" || string.IsNullOrEmpty(budgetsFilter)) ? "true" : "false");
            handler.Template.InsertVariable("external-budget", (budgetsFilter == "external" || string.IsNullOrEmpty(budgetsFilter)) ? "true" : "false");

            handler.Template.InsertVariable("min-date",  Core.Util.Time.ToDefaultTimezoneDateString(minDate));
            handler.Template.InsertVariable("max-date", Core.Util.Time.ToDefaultTimezoneDateString(maxDate));
            handler.Template.InsertVariable("billed", billedFilter.HasValue ? billedFilter.Value.ToString().ToLowerInvariant() : "all");
            handler.Template.InsertVariable("billed-raw", billedFilter.HasValue ? billedFilter.Value.ToString().ToLowerInvariant() : "");
            handler.Template.InsertVariable("costtype", !string.IsNullOrEmpty(costTypeFilter) ? costTypeFilter : "all");
            handler.Template.InsertVariable("costtype-raw", !string.IsNullOrEmpty(costTypeFilter) ? costTypeFilter : "");
            handler.Template.InsertVariable("user", filteredUsers.Any() ? filteredUsers.FirstOrDefault().PublicId : "all");
            handler.Template.InsertVariable("user-raw", filteredUsers.Any() ? filteredUsers.FirstOrDefault().PublicId : "");
            handler.Template.InsertVariable("worktype", filteredWorkTypes.Any() ? filteredWorkTypes.FirstOrDefault().PublicId : "all");
            handler.Template.InsertVariable("worktype-raw", filteredWorkTypes.Any() ? filteredWorkTypes.FirstOrDefault().PublicId : "");

        }


        private static void InsertFilterVariables(ProjectWorkType workType, PageTemplate template) {
            template.InsertVariable("filter.name", "worktypes");
            template.InsertVariable("filter.title", workType.Name);
            template.InsertVariable("filter.id", workType.PublicId);
        }

        private static void InsertFilterVariables(User user, PageTemplate template) {
            template.InsertVariable("filter.name", "users");
            template.InsertVariable("filter.title", user.Name);
            template.InsertVariable("filter.id", user.PublicId);
        }

    }
}
