using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects {
    public class Config {

        public readonly static int ProjectsPruneCostsMinSeconds = Core.Config.GetIntSetting("ProjectsPruneCostsMinSeconds");
        public readonly static double ProjectsDefaultVatRate = Core.Config.GetDoubleSetting("ProjectsDefaultVatRate");
        public readonly static string ProjectsDefaultWorkTypeGroup = Core.Config.GetStringSetting("ProjectsDefaultWorkTypeGroup");
        public readonly static bool ProjectsFinanceIntegrationEnabled = Core.Config.GetBoolSetting("ProjectsFinanceIntegrationEnabled");

        public const string BUSINESS_COLOR_PROPERTY_KEY = "Business_Color";

    }
}
