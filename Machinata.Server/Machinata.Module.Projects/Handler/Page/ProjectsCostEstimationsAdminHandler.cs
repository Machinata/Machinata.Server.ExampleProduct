
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.View;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsCostEstimationsAdminHandler : AdminPageTemplateHandler {

      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        #region Virtual Methods
        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();
            this.Template.InsertVariable("projects.finance-enabled", Config.ProjectsFinanceIntegrationEnabled ? "finance-enabled" : "");
        }
        #endregion

     

        [RequestHandler("/admin/projects/cost-estimations")]
        public void CostEstimations() {

            var estimations = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.ParentId == null && p.ProjectType == Project.ProjectTypes.CostEstimation)
                .OrderBy(p => p.Business.Name);
            estimations = this.Template.Filter(estimations, this, nameof(Project.Name));
            estimations = this.Template.Paginate(estimations, this, null);

            this.Template.InsertEntityList(
                entities: estimations,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/cost-estimations/cost-estimation/{entity.path}"
                );


            // Templates
            var templates = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.ParentId == null && p.ProjectType == Project.ProjectTypes.CostEstimationTemplate)
                .OrderBy(p => p.Business.Name);


            this.Template.InsertEntityList(
                entities: templates,
                variableName: "templates",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/cost-estimations/cost-estimation/{entity.path}"
                );

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add("cost-estimations");

        }

        [RequestHandler("/admin/projects/cost-estimations/create")]
        public void ProjectCreate() {

            this.RequireWriteARN();

            var entity = new Project();
            entity.ProjectType = Project.ProjectTypes.CostEstimation;

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE)
                .Hidden("projecttype",Project.ProjectTypes.CostEstimation.ToString()),
                apiCall : "/api/admin/projects/create",
                onSuccess: "/admin/projects/cost-estimations/cost-estimation/{project.path}"
                );

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add("cost-estimations");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/{path}")]
        public void CostEstimationtView(string path) {
            // Entity
            var entity = this.DB.Projects()
                .Include(nameof(Project.Children))
                .Where(p=>p.ProjectType == Project.ProjectTypes.CostEstimation || p.ProjectType == Project.ProjectTypes.CostEstimationTemplate)
                .GetByPath(path);
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users) + "." + nameof(ProjectCost.User))
                .Include(nameof(Project.Children))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostEstimation || p.ProjectType == Project.ProjectTypes.CostEstimationTemplate)
                .GetRootByPath(path);

            // Props
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

    
              
            // Sub Projs
            this.Template.InsertTemplates(
              entities: entity.Children.AsQueryable(),
              variableName: "subprojects",
              templateName: "project.subproject"
              );

            // Budget
            if(entity.Budget.HasValue) {
                // External
                {
                    var externalTemplate = this.Template.LoadTemplate("project.budget");
                    externalTemplate.InsertPropertyList("budget.form", entity.CalculateBudget(ProjectBudget.BudgetTypes.External), new FormBuilder(Forms.Admin.VIEW));
                    externalTemplate.InsertVariable("type", ProjectBudget.BudgetTypes.External);
                    this.Template.InsertTemplate("budget.external", externalTemplate);
                }

                // Internal
                {
                    var internalTemplate = this.Template.LoadTemplate("project.budget");
                    internalTemplate.InsertPropertyList("budget.form", entity.CalculateBudget(ProjectBudget.BudgetTypes.Internal), new FormBuilder(Forms.Admin.VIEW));
                    internalTemplate.InsertVariable("type", ProjectBudget.BudgetTypes.Internal);
                    this.Template.InsertTemplate("budget.internal", internalTemplate);
                }


            } else {
                this.Template.InsertVariable("budget.external", "");
                this.Template.InsertVariable("budget.internal", "");
            }
            

            // Costs
            this.Template.InsertEntityList(
              entities: entity.GetAllCosts().OrderByDescending(c=>c.TimeRange.Start).Take(20).AsQueryable(), // Note: this query alone will not include all the users, but since we have them in the context they will automically be fully loaded
              variableName: "costs",
              form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(ProjectCost.Created)),
              link: "/admin/projects/cost-estimations/cost-estimation/cost/{entity.project-path}/" + "{entity.public-id}",
              total: null
            );

            // Invoices
            this.Template.InsertEntityList(
             entities: entity.GetAllInvoicesFast().Where(o=>o.Status < Finance.Model.Invoice.InvoiceStatus.Paid).OrderByDescending(i=>i.Created).AsQueryable(),
             variableName: "invoices",
             form: new FormBuilder(Forms.Admin.LISTING),
             link: "/admin/projects/cost-estimations/cost-estimation/invoice/" + entity.Path + "/" +  "{entity.public-id}"
           );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, entity);

        }


        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/{path}/budgets")]
        public void ProjectBudgets(string path) {
            CostEstimationtView(path);
            this.Navigation.Add("budgets", "{text.budgets}");
        }

        public static void AddCostEstimationNavigation(NavigationBuilder navigationBuilder, Project project) {
            navigationBuilder.Add("projects");
            navigationBuilder.Add("cost-estimations");
            var p = project.GetRootProject();
            p.Include(nameof(Project.Business));
            var business = p.Business;
            navigationBuilder.Add("/admin/projects/businesses/business/" + business.PublicId, business.Name);
            AddProjectAncestorNavigation(navigationBuilder, project);
        }

        public static void AddProjectAncestorNavigation(NavigationBuilder navigationBuilder, Project project) {
            if (project.ParentId != null) {
                project.Include(nameof(Project.Parent));
                AddProjectAncestorNavigation(navigationBuilder, project.Parent);
            }
            navigationBuilder.Add("/admin/projects/cost-estimations/cost-estimation/" + project.Path, project.Name);
        }


        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/edit/{path}")]
        public void Edit(string path) {
            this.RequireWriteARN();
            // Menu items
            var entity = this.DB.Projects()
            .Include(nameof(Project.Users) +"."+ nameof(ProjectUser.User))
            .Include(nameof(Project.WorkTypes))
            .Include(nameof(Project.Parent))
            .GetByPath(path);

            var form = new FormBuilder(Forms.Admin.EDIT);
                       

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: form,
                apiCall: $"/api/admin/projects/project/{entity.PublicId}/edit",
                onSuccess: "/admin/projects/cost-estimations/cost-estimation/{path}"
                );

            // Users
            this.Template.InsertSelectionList(
                variableName: "users",
                entities: this.DB.Users().Active(),
                selectedEntities: entity.Users.Select(u=>u.User),
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/projects/project/{entity.PublicId}/"+ "toggle-user/{entity.public-id}");

            // Worktypes
            this.Template.InsertSelectionList(
                variableName: "worktypes",
                entities: this.DB.ProjectWorkTypes(),
                selectedEntities: entity.WorkTypes,
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/projects/project/{entity.PublicId}/"+ "toggle-worktype/{entity.public-id}");

            // Vars
            this.Template.InsertVariable("entity.is-root", entity.IsRootProject);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, entity);
            this.Navigation.Add("edit");
        }




        #region Task //////////////////////////////////////////////////////////////////////////////////////////////

        [RequestHandler("/admin/projects/project/create-task/{path}")]
        public void ProjectCreateTask(string path) {
            this.RequireWriteARN();

            var project = this.DB.Projects().Include(nameof(Project.Users) + "." + nameof(ProjectUser.User)).GetByPath(path);
            var entity = new ProjectTask();
            var projectUsers = project.GetProjectUsers(true).AsQueryable();
            var defaultUser = projectUsers.FirstOrDefault(p => p.User == this.User);

            // Vars
            this.Template.InsertVariables("project", project);

            // Form
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.CREATE).DropdownList("user", projectUsers, defaultUser),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/create-task",
                onSuccess: $"/admin/projects/project/task/{project.Path}/"+"{task.public-id}"
                );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("create-task", "{text.create-task}");
        }

        [RequestHandler("/admin/projects/project/task/{path}/{taskId}")]
        public void Task(string path, string taskId) {
            var project = this.DB.Projects()
            .Include(nameof(Project.Tasks) + "." + nameof(ProjectTask.User) + "." + nameof(ProjectUser.User))
           // .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
            .GetByPath(path);
            var entity = project.Tasks.AsQueryable().GetByPublicId(taskId);
        
            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("/admin/projects/project/tasks/" + project.Path, "tasks");
            this.Navigation.Add(entity.URL, entity.Title);
        }

        [RequestHandler("/admin/projects/project/task-edit/{path}/{taskId}")]
        public void TaskEdit(string path, string taskId) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.Tasks) + "." + nameof(ProjectTask.User))
                .GetByPath(path);
            var task = project.Tasks.AsQueryable().GetByPublicId(taskId);
            var projectUsers = project.GetProjectUsers(true).AsQueryable();

            // Form
            this.Template.InsertForm(
                entity: task,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT).DropdownList("user", projectUsers, task.User),
                apiCall: $"/api/admin/projects/task/{taskId}/edit",
                onSuccess: "/admin/projects/project/task/{project.path}/{task.public-id}"
                );

                
            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("/admin/projects/project/tasks/" + project.Path, "tasks");
            this.Navigation.Add(task.URL, "{text.task}: " + task.Title);
            Navigation.Add("edit");
        }

        [RequestHandler("/admin/projects/project/tasks/{path}")]
        public void ProjectTasks(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
               
                .GetByPath(path);
            var tasks = Template.Paginate(project.GetAllTasks().AsQueryable(), this, nameof(ProjectTask.Deadline));

            // Form
            this.Template.InsertEntityList(
                entities: tasks.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{entity.url}",
                loadFirstLevelReferences: true
                );

            this.Template.InsertVariables("project", project);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("tasks");
        }

        #endregion


        #region Event /////////////////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/project/event/{path}/{eventId}")]
        public void Event(string path, string eventId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Events))
                .GetByPath(path);
            var entity = project.Events.AsQueryable().GetByPublicId(eventId);
            entity.LoadFirstLevelObjectReferences();

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add(entity.URL, "{text.event}: " + entity.Title);
        }

        [RequestHandler("/admin/projects/project/event-edit/{path}/{eventId}")]
        public void EventEdit(string path, string eventId) {
            this.RequireWriteARN();
            // Menu items
            var project = this.DB.Projects().Include(nameof(Project.Events)).GetByPath(path);
            var projectEvent = project.Events.AsQueryable().GetByPublicId(eventId);


            // Form
            this.Template.InsertForm(
                entity: projectEvent,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/projects/event/{eventId}/edit",
                onSuccess: "/admin/projects/project/event/{project.path}/{event.public-id}"
                );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add(projectEvent.URL, "{text.event}: " + projectEvent.Title);
            Navigation.Add("edit");
        }


        [RequestHandler("/admin/projects/project/create-event/{path}")]
        public void ProjectCreateEvent(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects().GetByPath(path);
            var entity = new ProjectEvent();

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/create-event",
                onSuccess: "/admin/projects/project/event/{project.path}/{event.public-id}"
                );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("create-event", "{text.create-event}");
        }

        [RequestHandler("/admin/projects/project/events/{path}")]
        public void ProjectEvents(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects().GetByPath(path);
            var events = Template.Paginate(project.GetAllEvents().AsQueryable(), this,nameof(ProjectEvent.Date) );

            // List
            this.Template.InsertEntityList(
                entities: events.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{entity.url}"
                );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("events");
        }

        #endregion


        #region Cost /////////////////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/cost/{path}/{costId}")]
        public void Cost(string path, string costId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);
            var entity = project.Costs.AsQueryable().GetByPublicId(costId);
            entity.Include(nameof(ProjectCost.User));
            entity.Include(nameof(ProjectCost.Invoice));

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW),
                loadFirstLevelReferences: true
            );

            // Vars
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);
            if (entity.Invoice != null) {
                this.Template.InsertVariables("invoice", entity.Invoice);
            } 
            
            this.Template.InsertVariable("invoice.available", entity.Invoice != null);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add(entity.URL, "{text.project-cost}: " + entity.Description);
        }

        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/cost-edit/{path}/{costId}")]
        public void CostEdit(string path, string costId) {
            this.RequireWriteARN();
            // Menu items
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs)+ "." + nameof(ProjectCost.WorkType))
                .GetByPath(path);
            var projectCost = project.Costs.AsQueryable().GetByPublicId(costId);

            var workTypes = project.GetWorkTypes();
            var projectUsers = project.GetProjectUsers(true).Select(u => u.User).ToList();
            var date = projectCost.TimeRange.Start.Value;
            
            var projectCostForm = ProjectsForms.GetProjectCostFormBuilder(projectCost.ProjectCostType.ToString(),date);
            var timeSheetForm = ProjectsForms.GetTimesheetForm(workTypes, projectUsers, projectCost.User, date,projectCost.Units, projectCost.WorkType);
            var form = projectCost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet 
                ? timeSheetForm
                : projectCostForm;

            // Form
            this.Template.InsertForm(
                entity: projectCost,
                variableName: "form",
                form: form ,
                apiCall: $"/api/admin/projects/cost/{costId}/edit" + (projectCost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet? "-timesheet-cost": "-project-cost"),
                onSuccess: "/admin/projects/cost-estimations/cost-estimation/cost/{project.path}/{cost.public-id}"
            );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add(projectCost.URL, "{text.project-cost}: " + projectCost.Description);
            Navigation.Add("edit");
        }


      
        [RequestHandler("/admin/projects/project/add-timesheet-cost/{path}")]
        public void ProjectAddTimesheetCost(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.WorkTypes))
                .GetByPath(path);
            var entity = new ProjectCost();
            entity.Billable = true;
            var projectUsers = project.GetProjectUsers(true).Select(pu=>pu.User);
            var defaultUser = projectUsers.SingleOrDefault(u => u.Id == this.User.Id);
            //DateTime defaultDate = Core.Util.Time.GetDefaultTimezonesToday();
            DateTime defaultDate = DateTime.UtcNow;

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: ProjectsForms.GetTimesheetForm(project.GetWorkTypes(), projectUsers, defaultUser, defaultDate,0, project.GetWorkTypes().FirstOrDefault()),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/add-timesheet-cost",
                onSuccess: "/admin/projects/cost-estimations/cost-estimation/cost/{project.path}/{cost.public-id}"
            );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("add-timesheet-cost", "{text.add-timesheet-entry}");
        }



        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/move/{path}/{publicId}")]
        public void CostMove(string path, string publicId) {
            this.RequireWriteARN();
            // Menu items
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .GetByPath(path);
            var projectCost = project.Costs.AsQueryable().GetByPublicId(publicId);

            // Paging
            var allProjects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => !p.Archived && p.ProjectType == Project.ProjectTypes.CostEstimation)
                .OrderBy(p => p.Business.Name)
                .ThenBy(p => p.Name);

            // List
            var projects = this.Template.Paginate(
                entities: allProjects,
                handler: this,
                sortBy: null,
                sortDir: "asc",
                pageNum: 1,
                pageSize: 20);

            // Vars
            this.Template.InsertVariables("entity", projectCost);

            // Form
            this.Template.InsertSelectionList(
                entities: projects,
                variableName: "entity-list",
                form: new FormBuilder(Forms.Admin.LISTING),
                selectedEntity: project,
                loadFirstLevelReferences: true
            );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add(projectCost.URL, "{text.project-cost}: " + projectCost.Description);
            Navigation.Add("move");
        }


        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/add-project-cost/{path}")]
        public void ProjectAddProjectCost(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.WorkTypes))
                .GetByPath(path);
            var entity = new ProjectCost();
            entity.Units = 1;
            entity.Billable = true;
            DateTime defaultDate = DateTime.UtcNow;
            var projectUsers = project.GetProjectUsers();
            var workTypes = project.GetWorkTypes();
            var defaultUser = projectUsers.SingleOrDefault(u => u.Id == this.User.Id);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: ProjectsForms.GetProjectCostFormBuilder("Internal", defaultDate),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/add-project-cost",
                onSuccess: "/admin/projects/cost-estimations/cost-estimation/cost/{project.path}/{cost.public-id}"
            );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("add-project-cost");
        }

      

        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/generate-invoice/{path}")]
        public void GenerateInvoice(string path) {
            this.RequireWriteARN();

            var project = this.DB.Projects().GetByPath(path);
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .GetRootByPath(path);

            IEnumerable<ProjectCost> costsNoInvoice = project.GetUnbilledProjectCosts().OrderBy(c=>c.Id);
            this.Template.InsertSelectionList(
                variableName: "no-invoices-costs",
                entities: costsNoInvoice.AsQueryable(),
                selectedEntities: costsNoInvoice,
                form: new FormBuilder(Forms.Admin.LISTING),
                selectionAPICall: null
            );

            // Vars
            this.Template.InsertVariables("project", project);

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("create-invoice", "{text.generate-invoice}");
        }

        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/costs/{path}")]
        public void Costs(string path) {
            // Init
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .GetRootByPath(path);
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);

            // Common filtered data
            var link = "/admin/projects/cost-estimations/cost-estimation/cost/{entity.project-path}/{entity.public-id}";
          
            this.InsertFilteredProjectCosts(project, true, null, null, link); 

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add("costs/" + project.PublicId, "{text.project-costs}");
        }


        [RequestHandler("/admin/projects/project/edit-costs/{path}")]
        public void EditCosts(string path) {
            // Init
           var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);

            // Costs with related ent
            var costsOfProject = project.GetAllCosts().Select(c => c.Id);
            var costs = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(c => costsOfProject.Contains(c.Id))
                .ToList()
                .AsQueryable();

            // Form
            var form = new FormBuilder(Forms.EMPTY)
                .Include(nameof(ProjectCost.ProjectName))
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.Costs))
                .Include(nameof(ProjectCost.TimeRange))
                .Include(nameof(ProjectCost.Duration))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.Billed));


            // List
            this.Template.InsertEditableEntityList(
                variableName: "entities",
                entities: costs,
                form: form,
                editAPICall: $"/api/admin/projects/{project.PublicId}/edit-project-costs"
                );



            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add("edit-costs/" + project.PublicId, "{text.edit-project-costs}");
        }

        [RequestHandler("/admin/projects/project/hours/{path}")]
        public void Hours(string path) {
            // Init
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .GetRootByPath(path);
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);

            // Common filtered data
            var link = "/admin/projects/project/cost/{entity.project-path}/{entity.public-id}";
            this.InsertFilteredProjectCosts(project, true, null, null, link);


            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add("hours/" + project.PublicId, "{text.project-hours}");
        }

        #endregion


        #region Invoice ///////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/invoice/{path}/{invoiceId}")]
        public void Invoice(string path, string invoiceId) {
            var project = this.DB.Projects()
                .GetByPath(path);
            var entity = project.GetAllInvoicesFast().AsQueryable().GetByPublicId(invoiceId);

            var allCosts = ProjectCost.GetCostsForInvoice(entity);

            var entites = this.Template.Paginate(allCosts, this);

            // Costs
            this.Template.InsertEntityList(
                entities: entites,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{entity.url}"
                );


            // Properties
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "entity",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);
                

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            this.Navigation.Add("invoice", "{text.invoice}: " + entity.SerialId);
        }

        [RequestHandler("/admin/projects/cost-estimations/cost-estimation/invoices/{path}")]
        public void ProjectInvoices(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects().GetByPath(path);
            var invoices = Template.Paginate(project.GetAllInvoicesFast().AsQueryable(), this, nameof(Finance.Model.Invoice.DueDate));

            // List
            this.Template.InsertEntityList(
                entities: invoices.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/cost-estimations/cost-estimation/invoice/" + project.Path + "/" + "{entity.public-id}"
                );

            // Navigation
            AddCostEstimationNavigation(this.Navigation, project);
            Navigation.Add("events");
        }

        #endregion


    }
}
