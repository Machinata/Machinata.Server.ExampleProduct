
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsTimelineAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/projects/timeline")]
        public void Default() {
            // Navigation
            Navigation.Add("projects", "{text.projects}");
            Navigation.Add("timeline", "{text.timeline}");
        }

        [RequestHandler("/admin/projects/timeline/fullscreen")]
        public void Fullscreen() {
         
        }

    }
}
