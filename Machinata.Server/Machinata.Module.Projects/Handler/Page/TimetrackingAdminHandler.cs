
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;

namespace Machinata.Module.Projects.Handler {


    public class TimetrackingAdminHandler : AdminPageTemplateHandler {

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "stopwatch",
                Path = "/admin/projects/timetracking/applet",
                OnClick = "javascript:Machinata.Timetracking.createWindow();",
                Title = "{text.timetracking}",
                Sort = "500"
            });
        }

        #endregion


        [RequestHandler("/admin/projects/timetracking")]
        public void Default() {
            // Navigation
            Navigation.Add("projects", "{text.projects}");
            Navigation.Add("timetracking", "{text.timetracking}");
        }

        [RequestHandler("/admin/projects/timetracking/applet")]
        public void Applet() {

        }
        
    }
}
