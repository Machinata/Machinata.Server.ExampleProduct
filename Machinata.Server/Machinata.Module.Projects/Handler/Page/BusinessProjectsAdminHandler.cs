
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class BusinessProjectsAdminHandler : AdminPageTemplateHandler {
        

        [RequestHandler("/admin/projects/businesses")]
        public void Businesses() {
            var entities = this.DB.ActiveBusinesses().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this, nameof(Business.Name),"asc");
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/projects/businesses/business/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add("businesses", "{text.businesses}");
        }


        [RequestHandler("/admin/projects/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {
            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Projects
            var rootProjects = this.DB.Projects().Include(nameof(Project.Business)).Where(p => p.Business.Id == entity.Id && p.ParentId == null && p.ProjectType == Project.ProjectTypes.Project);
            this.Template.InsertEntityList(
            variableName: "projects",
            entities: rootProjects,
            form: new FormBuilder(Forms.Admin.LISTING),
            link: "{entity.url}");



            // CostEstimations
            // TODO NOT SUPPORTED/TESTED/IMPLMENTED YET
            //var rootEstimations = this.DB.Projects().Include(nameof(Project.Business)).Where(p => p.Business.Id == entity.Id && p.ParentId == null && p.ProjectType == Project.ProjectTypes.CostEstimation);
            //this.Template.InsertEntityList(
            //variableName: "cost-estimations",
            //entities: rootEstimations,
            //form: new FormBuilder(Forms.Admin.LISTING),
            //link: "{entity.url}");

            // Color
            var color = Project.GetColor(entity);
         
            this.Template.InsertPropertyList("business.project-color", color);
            this.Template.InsertVariable("business.project-color.color", color?.Color);
          

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"business/{publicId}", entity.Name);
        }
        
        

    }
}
