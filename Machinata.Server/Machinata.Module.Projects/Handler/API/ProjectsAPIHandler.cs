using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.View;
using System.Collections.Generic;
using Machinata.Module.Admin.View;
using Machinata.Core.Messaging;

namespace Machinata.Module.Projects.Handler {
    
        

    public class ProjectsAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Project> {

        #region Handler Policies


        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return new List<AccessPolicy>() {
                new AccessPolicy() { Name = $"Team", ARN = AccessPolicy.GetDefaultRootAccessARN("team") } // indicates a user is part of the team
                //AccessPolicy.GetDefaultAdminPolicies("projects/timetracking").FirstOrDefault() // not needed, since /projects/* covers most cases
            };
        }

        #endregion

        [RequestHandler("/api/admin/projects/create")]
        public void Create() {
            var project = new Project();
            project.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            project.ChangeName(this.DB, project.Name, false);
            project.Validate();

            // Color from Business
            project.SetColorFromBusiness();

            if (project.IsRootProject && project.Color == null) {
                project.Color = Core.Charts.ChartColors.D3_CATEGORY_10.Random();
            }

            // Default users
            project.Users = new List<ProjectUser>();
            foreach (var user in this.DB.Users().Active()) {
                // Does this user have the team ARN?
                var hasMatchingARN = user.HasMatchingARN("/team", false);
                // Register user
                if(hasMatchingARN) project.AddNewProjectUser(user);
            }

            // Default worktypes
            var worktypes = this.DB.ProjectWorkTypes().Where(wt => wt.Group == Config.ProjectsDefaultWorkTypeGroup);
            foreach (var worktype in worktypes) {
                project.WorkTypes.Add(worktype);
            }

            this.DB.Projects().Add(project);
            this.DB.SaveChanges();
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });

        }

      

        [RequestHandler("/api/admin/projects/create-sub/{parentId}")]
        public void CreateSub(string parentId) {
          
             // Parent
            var parent = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Children))
                .GetByPublicId(parentId);

            // Name
            var name = this.Params.String("name");

            // Project
            var project = new Project();
            project.Name = name ;
            project.Parent = parent;
            project.ParentId = parent.Id;
            project.Business = parent.Business;
            project.ProjectType = parent.ProjectType;
            project.ChangeName(this.DB, project.Name, false);
            project.SetColorFromBusiness();

            if (parent.TimeRange.HasValue()) {
                project.TimeRange = parent.TimeRange.Clone();
            }

            // Save
            this.DB.Projects().Add(project);
            this.DB.SaveChanges();

            // Send
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });
        }

        [RequestHandler("/api/admin/projects/project/{publicId}/delete")]
        public void Delete(string publicId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Children))
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Tasks))
                .Include(nameof(Project.Events))
                .Include(nameof(Project.Users))
                .GetByPublicId(publicId);

            // Deny deleting cascading project hierarchies
            if (project.GetAllSubProjects(false).Any()) {
                throw new BackendException("delete-error", "Please delete sub projects before");
            }

            // Costs
            foreach (var cost in project.Costs.ToList()) {
                project.Costs.Remove(cost);
                this.DB.ProjectCosts().Remove(cost);
            }

            // Tasks
            foreach (var task in project.Tasks.ToList()) {
                project.Tasks.Remove(task);
                this.DB.ProjectTasks().Remove(task);
            }

            // Events
            foreach (var projEvent in project.Events.ToList()) {
                project.Events.Remove(projEvent);
                this.DB.ProjectEvents().Remove(projEvent);
            }

            // Users
            foreach (var user in project.Users.ToList()) {
                project.Users.Remove(user);
                this.DB.ProjectUsers().Remove(user);
            }

            // Project
            this.DB.Projects().Remove(project);

            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("delete-success");

        }

        [RequestHandler("/api/admin/projects/project/{publicId}/rename")]
        public void Rename(string publicId) {
            var project = this.DB.Projects()
                .GetByPublicId(publicId);
            project.ChangeName(this.DB, this.Params.String("name"));
            this.DB.SaveChanges();

            this.SendAPIMessage("rename-success", new { Project = new { Path = project.Path, url = project.URL } });
        }

        [RequestHandler("/api/admin/projects/project/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.Set<Project>()
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Business))
                .GetByPublicId(publicId);

            // Update
            var oldBuisinessId = entity.Business?.Id;
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            var newBusinessId = entity.Business?.Id;

            // Did we change business?
            if(oldBuisinessId != newBusinessId) {
                entity.ChangeName(this.DB, entity.Name);
            }

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                PublicId = entity.PublicId,
                Path = entity.Path,
                URL = entity.URL
            });

        }

     
        [RequestHandler("/api/admin/projects/project/{publicId}/toggle-user/{userId}")]
        public void ToggleUser(string publicId,string userId) {

            var project = this.DB.Projects()
               .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
               .GetByPublicId(publicId);

            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value", false);

            var projectUser = project.Users.SingleOrDefault(u => u.User.Id == user.Id);

            if (projectUser != null && !enable ) {
                this.DB.ProjectUsers().Remove(projectUser);
            }
            else if (enable) {
                project.AddNewProjectUser(user);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("toggle-success");
        }

       

        [RequestHandler("/api/admin/projects/project/{publicId}/toggle-worktype/{worktypeId}")]
        public void ToggleWorkType(string publicId, string worktypeId) {

            var project = this.DB.Projects()
               .Include(nameof(Project.WorkTypes))
               .GetByPublicId(publicId);

            var worktype = this.DB.ProjectWorkTypes().GetByPublicId(worktypeId);
            var enable = this.Params.Bool("value", false);

            if (!enable) {
               project.WorkTypes.Remove(worktype);
            } else  {
           
                project.WorkTypes.Add(worktype);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("toggle-success");
        }

        #region Task ///////////////////////////////////////////////////////////////////////

        [RequestHandler("/api/admin/projects/project/{publicId}/create-task")]
        public void CreateTask(string publicId) {
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Tasks))
                .Include(nameof(Project.Users))
                .GetByPublicId(publicId);

            var task = new ProjectTask();
            task.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            task.Validate();
            project.Tasks.Add(task);

            // Users
            var users = project.GetProjectUsers();
            var user = users.AsQueryable().GetByPublicId(this.Params.String("user"));

            task.User = user;
        
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Task = new { PublicId = task.PublicId} , Project = new { Path = project.Path} });
        }


        [RequestHandler("/api/admin/projects/task/{publicId}/edit")]
        public void TaskEdit(string publicId) {
            var entity = this.DB.Set<ProjectTask>()
            .Include(nameof(ProjectTask.Project))
            .GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new { Task = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });

        }

        [RequestHandler("/api/admin/projects/task/{publicId}/delete")]
        public void TaskDelete(string publicId) {
            var entity = this.DB.Set<ProjectTask>()
         
            .GetByPublicId(publicId);
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");

        }

        #endregion


        #region Event ///////////////////////////////////////////////////////////////////////

        [RequestHandler("/api/admin/projects/project/{publicId}/create-event")]
        public void CreateEvent(string publicId) {
            var project = this.DB.Set<Project>()
             .Include(nameof(Project.Events))
            .GetByPublicId(publicId);

            var projectEvent = new ProjectEvent();
            projectEvent.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            projectEvent.Validate();
            project.Events.Add(projectEvent);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Event = new { PublicId = projectEvent.PublicId }, Project = new { Path = project.Path } });
        }


        [RequestHandler("/api/admin/projects/event/{publicId}/edit")]
        public void EventEdit(string publicId) {
            var entity = this.DB.Set<ProjectEvent>()
            .Include(nameof(ProjectEvent.Project))
            .GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new { Event = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });

        }

        [RequestHandler("/api/admin/projects/event/{publicId}/delete")]
        public void EventDelete(string publicId) {
            var entity = this.DB.Set<ProjectEvent>()

            .GetByPublicId(publicId);
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");

        }

        #endregion
        

        #region Costs ///////////////////////////////////////////////////////////////////////

        //[RequestHandler("/api/admin/projects/project/{publicId}/create-cost")]
        //public void CreateCost(string publicId) {
        //    var project = this.DB.Set<Project>()
        //        .Include(nameof(Project.Costs))
        //        .GetByPublicId(publicId);

        //    var projectCost = new ProjectCost();
        //    projectCost.Populate(this, new FormBuilder(Forms.Admin.CREATE));
        //    projectCost.Validate();
        //    project.Costs.Add(projectCost);

        //    this.DB.SaveChanges();

        //    // Return
        //    SendAPIMessage("create-success", new { Cost = new { PublicId = projectCost.PublicId }, Project = new { Path = project.Path } });
        //}

        [RequestHandler("/api/admin/projects/project/{publicId}/add-timesheet-cost")]
        public void AddTimesheetCost(string publicId) {
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Business))
                .GetByPublicId(publicId);

            var projectCost = new ProjectCost();
            projectCost.Business = project.Business;
            ProjectsForms.PopulateTimesheet(projectCost, this);
            project.Costs.Add(projectCost);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Cost = new { PublicId = projectCost.PublicId }, Project = new { Path = project.Path } });
        }

        

        [RequestHandler("/api/admin/projects/project/{publicId}/add-project-cost")]
        public void AddProjectCost(string publicId) {
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Business))
                .GetByPublicId(publicId);

            var projectCost = new ProjectCost();
            projectCost.Business = project.Business;
            ProjectsForms.PopulateProjectCost(projectCost, this);
            project.Costs.Add(projectCost);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Cost = new { PublicId = projectCost.PublicId }, Project = new { Path = project.Path } });
        }

        [RequestHandler("/api/admin/projects/cost/{publicId}/edit-project-cost")]
        public void EditProjectCost(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
               .Include(nameof(ProjectCost.Project))
               .Include(nameof(ProjectCost.User))
               .Include(nameof(ProjectCost.WorkType))
           .GetByPublicId(publicId);
            ProjectsForms.PopulateProjectCost(entity, this);
          
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Cost = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });
        }


        [RequestHandler("/api/admin/projects/{publicId}/edit-project-costs")]
        public void EditProjectCosts(string publicId) {
            var project = this.DB.Projects()
               .Include(nameof(Project.Costs))
               .GetByPublicId(publicId);

            var newEntities =  this.PopulateListEdit(project.Costs.AsQueryable(), createNewCost, null , enableEntityDeletion : true);

            // Adding new entities to Invoice
            foreach(var newEntity in newEntities) {
                newEntity.Project = project;
                this.DB.ProjectCosts().Add(newEntity);
            }

            // Update LineItems

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Project = new { Path = project.Path } });
        }

        private ProjectCost createNewCost() {
           return  new ProjectCost();
        }


       

        [RequestHandler("/api/admin/projects/cost/{publicId}/edit-timesheet-cost")]
        public void EditTimesheetCost(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
                .Include(nameof(ProjectCost.Project))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
            .GetByPublicId(publicId);
            ProjectsForms.PopulateTimesheet(entity, this);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Cost = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });
        }

        [RequestHandler("/api/admin/projects/cost/{publicId}/move")]
        public void CostMove(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
                .Include(nameof(ProjectCost.Project))
                .Include(nameof(ProjectCost.Business))
                .GetByPublicId(publicId);

            var destination = this.DB.Projects()
                 .Include(nameof(ProjectCost.Business))
                .GetByPublicId(this.Params.String("project"));

            entity.Project = destination;
            entity.Business = destination.Business;

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("move-success", new { Cost = new { PublicId = entity.PublicId, Path = entity.URL }, Project = new { Path = entity.Project.Path } });
        }

      

        [RequestHandler("/api/admin/projects/cost/{publicId}/delete")]
        public void CostDelete(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
                .GetByPublicId(publicId);

            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");

        }

        #endregion


        #region Invoice
        [RequestHandler("/api/admin/projects/project/{publicId}/generate-invoices")]
        public void BusinessesGenerateInvoices(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Projects().GetByPublicId(publicId);
            var business = entity.GetProjectBusiness();
            var address = business.GetBillingAddressForBusiness();

            var unbilledCosts = entity.GetUnbilledProjectCosts();
            var costIds = Params.StringArray("selected", null);
            if (costIds == null) throw new BackendException("nothing-selected", "Nothing was selected.");
            var costs = DB.ProjectCosts().GetByPublicIds(costIds);

            var sender = this.DB.OwnerBusiness();

            if (sender == null) {
                throw new BackendException("owner-business", "No IsOwner business has been defined yet.");
            }

            // check all orders are open
            if (!costs.Select(c=>c.Id).ToList().All(oo => unbilledCosts.Select(uc=>uc.Id).ToList().Contains(oo))) {
                throw new BackendException("order-wrong-status", "Not all orders are ready for invoice creation.");
            }

            // Create all the invoices and save
            var invoice = CreateInvoice(entity, sender, address, costs, Finance.Payment.PaymentMethod.Invoice, this.User, Core.Config.CurrencyDefault, business);
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("invoice-generated-success", new { Invoice = new { PublicId = invoice.PublicId} });
        }

        public static Invoice CreateInvoice(
         
            Project parentProject,
            Business sender,
            Address billingAddress,
            IQueryable<ProjectCost> costs,
            Finance.Payment.PaymentMethod method,
            User user,
            string currency,
            Business business = null
            
            ) {
            try {
                // Create the invoice and setup details
                var invoice = new Invoice();
                invoice.User = user;
                invoice.Business = business;
                invoice.BillingAddress = billingAddress;
                invoice.BillingName = billingAddress?.Name;
                invoice.BillingEmails = billingAddress?.Email;
                invoice.Sender = sender;
                invoice.SenderAddress = sender.GetBillingAddressForBusiness();
                invoice.Status = Invoice.InvoiceStatus.Created;
                invoice.PaymentMethod = method;

                // Prices /Currencies
                invoice.SubtotalCustomer = new Price(0, currency);
                invoice.TotalCustomer = new Price(0, currency);
                invoice.VATCustomer = new Price(0, currency);
                invoice.Currency = currency;

                // Title
                invoice.Title = parentProject.Name;

                var grouped = costs.GroupBy(c => c.Project).Select(c => new { Key = c.Key, Value = c.AsEnumerable() });

                // Add each order as a line item group
                foreach (var projectGroup in grouped) {
                    var project = projectGroup.Key;
                    var lineItemsGroups = project.GenerateLineItemGroups(projectGroup.Value, invoice);
                    foreach (var lineItemGroup in lineItemsGroups) {
                        invoice.LineItemGroups.Add(lineItemGroup);
                    }
                }

                // Update totals
                invoice.UpdateInvoice();
                
                return invoice;

            }
            catch (Exception e) {

                _logger.Error(e.StackTrace);
              
                throw new BackendException("invoice-error", $"Could not create invoice for {parentProject.Name}",e);
            }
        }


        [RequestHandler("/api/admin/projects/invoice/{publicId}/rollback")]
        public void InvoiceRollback(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." + (nameof(LineItemGroup.LineItems)))
                .GetByPublicId(publicId);

            var costs = ProjectCost.GetCostsForInvoice(entity);

            // Costs Vars
            foreach (var cost in costs) {

                cost.Invoice = null;
                cost.LineItem = null;
                cost.Billed = null;
            }

            // Line Item Groups
            foreach ( var group in entity.LineItemGroups.ToList()) {

                // Line Items
                foreach (var item in group.LineItems.ToList()) {
                    this.DB.DeleteEntity(item);
                }

                this.DB.DeleteEntity(group);
            }

            // Invoice
            this.DB.DeleteEntity(entity);

       

            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("invoice-rollback-success", new { Invoice = new { PublicId = entity.PublicId } });
        }

        #endregion


        [RequestHandler("/api/admin/projects/business/{publicId}/set-color")]
        public void SetBusinessColor(string publicId) {

            var business = this.DB.ActiveBusinesses().GetByPublicId(publicId);
            var color = this.Params.String("color", null);

            //if (string.IsNullOrWhiteSpace(color)) { 
            //    throw new BackendException("no-color", "No color value.");
            //}

            business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY] = color;

            this.DB.SaveChanges();

            this.SendAPIMessage("success");

        }


        #region Templates ///////////////////////////////////////////////////

        [RequestHandler("/api/admin/projects/create-template/{parentId}/{type}")]
        public void CreateTemplate(string parentId, string type) {

            // Parent, dont load anything here since recursion in copyproject has  to do it anyway
            var source = this.DB.Projects().GetByPublicId(parentId);

            // Name
            var name = this.Params.String("name");
            var typeParsed = Core.Util.EnumHelper.ParseEnum<Project.ProjectTypes>(type);

            Project project = CopyProject(source, typeParsed, name);
            // Save
            this.DB.Projects().Add(project);
            this.DB.SaveChanges();

            project.ChangeName(this.DB, project.Name, true, false);

            // Save
            this.DB.SaveChanges();

            // Send
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });
        }

        private Project CopyProject(Project source, Project.ProjectTypes type, string name) {


            throw new Exception("Not finished tested yet");
            // Load all related entities

            source.Include(nameof(Project.Business))
                  .Include(nameof(Project.Children))
                  .Include(nameof(Project.Costs))
                  .Include(nameof(Project.Users))
                  .Include(nameof(Project.WorkTypes))
                  .Include(nameof(Project.Events))
                  .Include(nameof(Project.Tasks));


            // Project
            var newProject = new Project();
            newProject.Business = source.Business;
            newProject.Context = this.DB;
            newProject.Users = new List<ProjectUser>();

            // Name is null if down in hierarchy/recursion
            if (name != null) {
                newProject.Name = name;
              
            } else {
                newProject.Name = source.Name;
            }

          
            newProject.ProjectType = type;
            
            newProject.SetColorFromBusiness();
            if (source.TimeRange.HasValue()) {
                newProject.TimeRange = source.TimeRange.Clone();
            }

            // Costs
            foreach (var cost in source.Costs) {

                var newCost = new ProjectCost();
                newCost.Project = newProject;

                var form = new FormBuilder(Forms.Admin.EMPTY);
                form.Include(nameof(ProjectCost.TimeRange));
                form.Include(nameof(ProjectCost.Description));
                form.Include(nameof(ProjectCost.ProjectCostType));
                form.Include(nameof(ProjectCost.Billable));
                form.Include(nameof(ProjectCost.Units));
                form.Include(nameof(ProjectCost.Note));
                form.Include(nameof(ProjectCost.Duration));
                form.Include(nameof(ProjectCost.WorkType));
                form.Include(nameof(ProjectCost.Costs));
                form.Include(nameof(ProjectCost.CostsInternal));
                cost.CopyValuesTo(newCost, form);


                newProject.Costs.Add(newCost);
            }

            // Tasks
            foreach (var task in source.Tasks) {

                var newTask = new ProjectTask();
                newTask.Project = newProject;

                var form = new FormBuilder(Forms.Admin.EMPTY);
                form.Include(nameof(ProjectTask.Title));
                form.Include(nameof(ProjectTask.Completed));
                form.Include(nameof(ProjectTask.Note));
                form.Include(nameof(ProjectTask.Duration));
                form.Include(nameof(ProjectTask.User));
                task.CopyValuesTo(newTask, form);

                newProject.Tasks.Add(newTask);
            }


            // Events
            foreach (var @event in source.Events) {

                var newEvent = new ProjectEvent();
                newEvent.Project = newProject;

                var form = new FormBuilder(Forms.Admin.EMPTY);
                form.Include(nameof(ProjectEvent.ProjectEventType));
                form.Include(nameof(ProjectEvent.Title));
                form.Include(nameof(ProjectEvent.Date));
                form.Include(nameof(ProjectEvent.Description));

                @event.CopyValuesTo(newEvent, form);

                newProject.Events.Add(@event);
            }

            if (source.TimeRange.HasValue()) {
                newProject.TimeRange = source.TimeRange.Clone();
            }

            // Root projects only
            if (source.IsRootProject == true) {


                // Users
                foreach (var user in source.Users) {
                    newProject.Users.Add(user);
                }

                // Worktypes
                foreach (var workType in source.WorkTypes) {
                    newProject.WorkTypes.Add(workType);
                }

            }
                
            foreach(var subProject in source.Children) {
                var child = CopyProject(subProject, type, null);
                child.Parent = newProject;
                newProject.Children = new List<Project>();
                newProject.Children.Add(child);
            }



            // Same for each subprojects each costs, tasks, events
            return newProject;
        }

        #endregion

        [RequestHandler("/api/admin/projects/list")]
        public void List() {

            var allProjects = this.DB.Projects()
               .Include(nameof(Project.Business))
               .Where(p => !p.Archived)
               .OrderBy(p => p.Business.Name)
               .ThenBy(p => p.Name).ToList();

            var form = new FormBuilder(Forms.EMPTY)
                .Include(nameof(Project.PublicId))
                .Include(nameof(Project.ShortURLPath))
                .Include(nameof(Project.BusinessFormalName))
                .Include(nameof(Project.Name));
              

            var result = allProjects.Select(c => c.GetJSON(form));

            // manually here otherwise enums will be integers
            var rawJson = Core.JSON.Serialize(new { Projects = result }, true, true, true);

            // Send raw to 
            this.SendRawAPIMessage("success", rawJson, false);


        }
    }
}
