using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using System.Collections.Generic;

namespace Machinata.Module.Projects.Handler {
    
    public class ProjectsChartsAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        #region Project Timelines 

        private TimelineChartDataTrack _createProjectTrack(TimelineChartData chart, Project project, Project rootProject, bool loadSubprojects) {
            // Create track
            var track = new TimelineChartDataTrack();
            if(project.IsRootProject) {
                track.Title = project.Business.Name + " " + project.Name;
            } else {
                track.Title = new string('↳',project.Level) +" " +  project.Name;//TODO
            }
            track.Name = track.Title;
            track.Group = project.ShortURLPath;
            track.Start = project.TimeRange.Start.Value;
            track.End = project.TimeRange.End.Value;
            track.Link = project.URL;
            track.Id = project.PublicId;
            track.Level = project.Level;
            // Color
            if(rootProject != null) {
                track.Color = rootProject.Color;
            } else {
                track.Color = project.Color;
            }
            if(project.Level > 0 && track.Color != null) {
                var numberOfColorSteps = 4;
                track.Color = Core.Util.Colors.FadedHTMLCodeForHTMLCode(track.Color, project.Level, numberOfColorSteps);
            }
            // Add events
            if(project.Events == null) {
                project.Include(nameof(Project.Events));
            }
            var events = project.Events.Where(e => e.Date.HasValue);
            foreach(var projectEvent in events) {
                var evt = new TimelineChartDataEvent();
                evt.Title = projectEvent.Title;
                evt.Date = projectEvent.Date.Value;
                evt.Link = projectEvent.URL;
                track.Events.Add(evt);
            }
            chart.Tracks.Add(track);
            // Subprojects?
            if(loadSubprojects) {
                project.Include(nameof(Project.Children));
                var subprojects = project.Children.Where(p => p.Archived == false && p.TimeRange.Start.HasValue && p.TimeRange.End.HasValue);
                foreach(var subproject in subprojects) {
                    var subtrack = _createProjectTrack(chart,subproject,rootProject,loadSubprojects);
                    subtrack.ParentId = project.PublicId;
                }
            }
            return track;
        }


        private TimelineChartDataTrack _createAbsenceTrack(TimelineChartData chart, Absence absence, bool mergeIntoSingleTrack) {

            // Only if we have daterange
            if (absence.Start == null || absence.End == null) {
                return null;
            }

            // Init
            var chartId = "ABS_" + absence.PublicId;
            var track = new TimelineChartDataTrack();

            // Setup a track for this absence
            track.Name = absence.FullTitle;
            if (mergeIntoSingleTrack) {
                track.Title = "Absences";
                track.Group = "absences";
            } else {
                track.Title = absence.FullTitle;
                track.Group = chartId;
            }
            track.Start = absence.TimeRange.Start.Value;
            track.End = absence.TimeRange.End.Value;
            track.Link = absence.URL;
            track.Id = chartId;

            // Add an event
            track.Events.Add(new TimelineChartDataEvent() {
                Date = track.Start,
                Title = absence.FullTitle,
                Link = absence.URL,
                Id = chartId + "_EVT"
            });
         
            chart.Tracks.Insert(0,track);
        
            return track;
        }

        
        private TimelineChartDataTrack _createHolidayTrack(TimelineChartData chart) {
            // Init
            var chartId = "HOL";
            var track = new TimelineChartDataTrack();
            var start = DateTime.Now;
            var end = DateTime.Now;
            if(chart.Tracks != null && chart.Tracks.Count > 0) start = chart.Tracks.Min(e => e.Start);
            if(chart.Tracks != null && chart.Tracks.Count > 0) end = chart.Tracks.Max(e => e.End);
            start = new DateTime(start.Year, 1, 1);
            end = new DateTime(end.Year, 12, 31);
            track.Start = start;
            track.End = end;
            track.Color = "transparent";
            track.Title = "Holidays";
            track.Group = "holidays";
            track.Id = chartId;

            var holidays = Core.Util.Time.GetPublicHolidays(start, end);
            foreach (var holiday in holidays) {
                // Setup a track for this absence
                track.Name = holiday.LocalName;
                // Add an event
                track.Events.Add(new TimelineChartDataEvent() {
                    Date = holiday.Date,
                    Title = holiday.LocalName,
                    Id = chartId + "_TIT"
                });

            }

            chart.Tracks.Insert(0,track);
            return track;
        }

        
        private TimelineChartDataTrack _createEventsTrack(TimelineChartData chart) {
            // Init
            var chartId = "EVT";
            var track = new TimelineChartDataTrack();
            var start = chart.Tracks.Min(e => e.Start);
            var end = chart.Tracks.Max(e => e.End);
            start = new DateTime(start.Year, 1, 1);
            end = new DateTime(end.Year, 12, 31);
            track.Name = "Events";
            track.Title = "Events";
            track.Group = "events";
            track.Id = chartId;
            track.Start = start;
            track.End = end;
            track.Color = "transparent";
            foreach(var existingTrack in chart.Tracks) {
                foreach(var existingEvent in existingTrack.Events) {
                    track.Events.Add(existingEvent);
                }
            }
            chart.Tracks.Insert(0,track);
            return track;
        }

        [RequestHandler("/api/admin/projects/chart/timeline/projects")]
        public void TimelineChartProjects() {

            // Init
            var data = new TimelineChartData();  
            bool loadEvents = this.Params.Bool("events", false);
            bool loadAbsences = this.Params.Bool("absences", false);
            bool loadSubprojects = this.Params.Bool("subprojects", false);
            bool loadHolidays = this.Params.Bool("holidays", false);
            var projectType = this.Params.Enum<Nullable<Project.ProjectTypes>>("type", null);

            // Projects
            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Events))
                .AsQueryable();

          
            // Project Type filter?
            if (projectType.HasValue == true) {
                projects = projects.Where(p => p.ProjectType == projectType);
            }

            // Apply filters
            projects = projects.Where(p => p.Archived == false && p.TimeRange.Start.HasValue && p.TimeRange.End.HasValue && p.Timeline == true);
            if(this.Params.String("business-id") != null) {
                var business = this.DB.Businesses().GetByPublicId(this.Params.String("business-id"));
                projects = projects.Where(p => p.Business.Id == business.Id);
            }

            // Subprojects
            if(!string.IsNullOrEmpty(this.Params.String("public-id"))) {
                // Get specific project
                var id = Core.Ids.Obfuscator.Default.UnobfuscateId(this.Params.String("public-id"));
                projects = projects.Where(p => p.Id == id);
            } else {
                // Get all root projects
                projects = projects.Where(p => p.ParentId == null);
            }
            
            // Order
            //projects = projects.OrderBy(p => p.Business.Name).ThenBy(p => p.Name);
            projects = projects.OrderByDescending(p => p.Sticky).ThenBy(p => p.Business.Name).ThenBy(p=>p.Name);

            // Execute query and compile
            var projectsList = projects.ToList();
            foreach(var project in projectsList){
                _createProjectTrack(data, project, project, loadSubprojects);
            }

            // Insert events
            if (loadEvents) {
               _createEventsTrack(data);
            }

            // Insert holidays
            if (loadHolidays) {
               _createHolidayTrack(data);
            }
            
            // Absences
            if (loadAbsences) {
                // Get specific absence
                var absences = this.DB.Absences().Include(nameof(Absence.User));
                foreach (var absence in absences) {
                    _createAbsenceTrack(data, absence, true);
                }
            }

            // Auto-prune data
            data.Prune();
            data.ConvertToDefaultTimezone();

            this.SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/projects/chart/timeline/absences")]
        public void Absences() {

            // Init
            var data = new TimelineChartData();

            // Absences
            var absences = this.DB.Absences()
                .Include(nameof(Absence.User))
                .OrderBy(e => e.TimeRange.Start);
            foreach (var absence in absences) {
                _createAbsenceTrack(data, absence, false);
            }

            // Insert holidays
            bool loadHolidays = this.Params.Bool("holidays", false);
            if (loadHolidays) {
                _createHolidayTrack(data);
            }

            // Auto-prune data
            data.Prune();
            data.ConvertToDefaultTimezone();

            this.SendAPIMessage("chart-data", data);
        }

        #endregion

        #region Project Budgets

        [RequestHandler("/api/admin/projects/chart/donut/project/budget/{publicId}")]
        public void DonutProjectsProjectBudget(string publicId) {
            var entity = this.DB.Projects().GetByPublicId(publicId);
            var type = this.Params.Enum<ProjectBudget.BudgetTypes>("type", ProjectBudget.BudgetTypes.External);
            var budget = entity.CalculateBudget(type);
            
            var data = new DonutChartData();
            data.Title = entity.Budget.ToString();
            
            data.Items.Add(new DonutChartDataItem() { Name = "Costs", Title = "Costs", Value = (int)budget.TotalCosts.Value });
            data.Items.Add(new DonutChartDataItem() { Name = "Remaining", Title = "Remaining", Value = (int)budget.RemainingBudget.Value });


            //var businesses = this.DB.Businesses().Select(b => new { Name = b.Name, Revenue = this.DB.Orders().Where(o => o.Business == b).ToList().Sum(o => o.TotalCustomer.Cents.Value)/100.0 });
            //businesses = businesses.OrderByDescending(g => g.Revenue).Take(10);
            //var businesses = this.DB.Businesses().Where(b => b.IsOwner == false).ToList();
            //foreach(var b in businesses) {
            //    var rev = this.DB.Invoices().Where(o => o.Business.Id == b.Id).ToList().Sum(o => o.TotalCustomer.Value.Value);
            //    data.Items.Add(new DonutChartDataItem() { Name = b.Name, Title = b.Name, Value = (int)rev });
            //}
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        #endregion

        #region Project Costs

        [RequestHandler("/api/admin/projects/chart/timeseries/project/costs/{publicId}")]
        public void BarChartProjectsProjectCosts(string publicId) {
            // Init
            var entity = this.DB.Projects().GetByPublicId(publicId);
            var costsOfProject = entity.GetAllCosts().Select(c => c.Id);
            var costs = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(e => costsOfProject.Contains(e.Id))
                .ToList()
                .AsQueryable();
            DateRange customTimeRange = null;
            if (this.Params.String("range") == "all" && costs.Any() == true) {
                customTimeRange = new DateRange(
                    costs.Where(e => e.TimeRange.Start.HasValue).Min(e => e.TimeRange.Start.Value).Date, 
                    costs.Where(e => e.TimeRange.End.HasValue).Max(e => e.TimeRange.End.Value
                ));
            }
            bool accumulated = this.Params.Bool("accumulated", false);

            // Apply filters
            {
                // Init
                var billedFilter = this.Params.BoolNullable("billed", null);
                var userFilter = this.Params.StringArray("users", new string[] { },',',StringSplitOptions.RemoveEmptyEntries);
                var workTypeFilter = this.Params.StringArray("worktypes", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
                var costTypeFilter = this.Params.String("costtype", null);
                IEnumerable<User> filteredUsers = new List<User>();
                IEnumerable<ProjectWorkType> filteredWorkTypes = new List<ProjectWorkType>();
           
                // Billed?
                if (billedFilter == true) {
                    costs = costs.Where(c => c.Billed != null);
                } else if(billedFilter == false) {
                    costs = costs.Where(c => c.Billed == null);
                }

                // Users
                if (userFilter.Any()) {
                    costs = costs.Where(c => c.User != null && userFilter.Contains(c.User.PublicId));
                    filteredUsers = costs.Select(w => w.User);
                }

                // Worktypes
                if (workTypeFilter.Any()) {
                    costs = costs.Where(c => c.WorkType != null && workTypeFilter.Contains(c.WorkType.PublicId));
                    filteredWorkTypes = costs.Select(c => c.WorkType);
                }

                // Costtype
                if (!string.IsNullOrEmpty(costTypeFilter)) {
                    var filter = Core.Util.EnumHelper.ParseEnum<ProjectCost.ProjectCostTypes>(costTypeFilter);
                    costs = costs.Where(c => c.ProjectCostType == filter);
                }
            }

            // Cache
            costs = costs.ToList().AsQueryable();

            var data = new TimeSeriesChartData();
            data.TimeFormat = Core.Config.DateFormat;
            data.Format = Core.Config.CurrencyFormat;
            data.Metric = Core.Config.CurrencyDefault;
            if(entity.Budget.HasValue) data.ZeroLine = (int)entity.Budget.Value;
            data.XAxisIsTime = false;
            if (this.Params.Bool("internal-budget",true)) {
                data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<ProjectCost>(
                    handler: this,
                    entities: costs.AsQueryable(),
                    entitiesForRange: delegate (IQueryable<ProjectCost> entities, DateTime start, DateTime end) {
                        if (accumulated == false) {
                            return entities.Where(e => e.TimeRange.Start >= start && e.TimeRange.End < end);
                        } else {
                            return entities.Where(e => e.TimeRange.End < end);
                        }
                    },
                    valueForEntities: delegate (IQueryable<ProjectCost> entities) {
                        return (double)entities.Sum(e => e.CostsInternal.Value);
                    },
                    title: accumulated ? "Internal (Accumulated)" : "Internal",
                    customTimeRange: customTimeRange
                ));
            }
            if (this.Params.Bool("external-budget", true)) {
                data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<ProjectCost>(
                    handler: this,
                    entities: costs.Where(e => e.Billable).AsQueryable(),
                    entitiesForRange: delegate (IQueryable<ProjectCost> entities, DateTime start, DateTime end) {
                        if (accumulated == false) {
                            return entities.Where(e => e.TimeRange.Start >= start && e.TimeRange.End < end);
                        } else {
                            return entities.Where(e => e.TimeRange.End < end);
                        }
                    },
                    valueForEntities: delegate (IQueryable<ProjectCost> entities) {
                        return (double)entities.Sum(e => e.Costs.Value);
                    },
                    title: accumulated ? "External (Accumulated)" : "External",
                    customTimeRange: customTimeRange
                ));
            }

            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/projects/chart/timeseries/project/hours/{publicId}")]
        public void BarChartProjectsProjectHours(string publicId) {
            // Init
            var entity = this.DB.Projects().GetByPublicId(publicId);
            var costsOfProject = entity.GetAllCosts().Select(c => c.Id);
            var costs = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(e => costsOfProject.Contains(e.Id) && e.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet && e.TimeRange.Start.HasValue && e.TimeRange.End.HasValue)
                .ToList()
                .AsQueryable();
            DateRange customTimeRange = null;
            if (this.Params.String("range") == "all") {
                customTimeRange = new DateRange(costs.Min(e => e.TimeRange.Start.Value).Date, costs.Max(e => e.TimeRange.End.Value));
            }
            bool accumulated = this.Params.Bool("accumulated", false);

            costs = ApplyFilters(costs);

            // Cache
            costs = costs.ToList().AsQueryable();

            // Compile chart data
            var data = new TimeSeriesChartData();
            data.TimeFormat = Core.Config.DateFormat;
            data.XAxisIsTime = false;
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<ProjectCost>(
                handler: this,
                entities: costs.AsQueryable(),
                entitiesForRange: delegate (IQueryable<ProjectCost> entities, DateTime start, DateTime end) {
                    if (accumulated == false) {
                        return entities.Where(e => e.TimeRange.Start >= start && e.TimeRange.End < end);
                    } else {
                        return entities.Where(e => e.TimeRange.End < end);
                    }
                },
                valueForEntities: delegate (IQueryable<ProjectCost> entities) {
                    return (double)entities.Sum(e => e.Duration.Hours);
                },
                title: accumulated ? "Hours (Accumulated)" : "Hours",
                customTimeRange: customTimeRange
            ));

            SendAPIMessage("chart-data", data);
        }

        private IQueryable<ProjectCost> ApplyFilters(IQueryable<ProjectCost> costs) {
            // Apply filters
            {
                // Init
                var billedFilter = this.Params.BoolNullable("billed", null);
                var userFilter = this.Params.StringArray("users", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
                var workTypeFilter = this.Params.StringArray("worktypes", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
                var costTypeFilter = this.Params.String("costtype", null);
                IEnumerable<User> filteredUsers = new List<User>();
                IEnumerable<ProjectWorkType> filteredWorkTypes = new List<ProjectWorkType>();

                // Billed?
                if (billedFilter == true) {
                    costs = costs.Where(c => c.Billed != null);
                } else if (billedFilter == false) {
                    costs = costs.Where(c => c.Billed == null);
                }

                // Users
                if (userFilter.Any()) {
                    costs = costs.Where(c => c.User != null && userFilter.Contains(c.User.PublicId));
                    filteredUsers = costs.Select(w => w.User);
                }

                // Worktypes
                if (workTypeFilter.Any()) {
                    costs = costs.Where(c => c.WorkType != null && workTypeFilter.Contains(c.WorkType.PublicId));
                    filteredWorkTypes = costs.Select(c => c.WorkType);
                }

                // Costtype
                if (!string.IsNullOrEmpty(costTypeFilter)) {
                    var filter = Core.Util.EnumHelper.ParseEnum<ProjectCost.ProjectCostTypes>(costTypeFilter);
                    costs = costs.Where(c => c.ProjectCostType == filter);
                }
            }

            return costs;
        }

        #endregion


        [RequestHandler("/api/admin/projects/chart/donut/project/costs/worktypes/{publicId}")]
        public void DonutProjectCostsWorktypes(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                .GetByPublicId(publicId);

            IQueryable<ProjectCost> costs = GetAllCostsOfProject(entity).Where(c => c.WorkType != null);

            costs = ApplyFilters(costs.AsQueryable());

            var externalBudget = this.Params.Bool("external-budget", true);
            var grouped = costs.GroupBy(c => c.WorkType.ToString());

            DonutChartData data = CreateGroupingDonut(externalBudget, grouped, "Worktypes");
            SendAPIMessage("chart-data", data);
        }

      

        [RequestHandler("/api/admin/projects/chart/donut/project/costs/users/{publicId}")]
        public void DonutProjectCostsUsers(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                .GetByPublicId(publicId);

            IQueryable<ProjectCost> costs = GetAllCostsOfProject(entity).Where(c=>c.User != null);
            costs = ApplyFilters(costs.AsQueryable());

            var externalBudget = this.Params.Bool("external-budget", true);
            var grouped = costs.GroupBy(c => c.User.ToString());

            DonutChartData data = CreateGroupingDonut(externalBudget, grouped, "Users");
            SendAPIMessage("chart-data", data);
        }


        [RequestHandler("/api/admin/projects/chart/donut/project/costs/projects/{publicId}")]
        public void DonutProjectCostsProjects(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Children))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                .GetByPublicId(publicId);

            var externalBudget = this.Params.Bool("external-budget", true);

            // Direct subprojects
            var subProjects = entity.Children;

            var groups = new List<IGrouping<string, ProjectCost>>();

            // Children Costs
            foreach (var subProject in subProjects) {
                subProject.Include(nameof(Project.Costs));
                IQueryable<ProjectCost> costs = subProject.Costs.AsQueryable();
                costs = ApplyFilters(costs.AsQueryable());
                var grouped = costs.GroupBy(c => c.ProjectName);
                groups.AddRange(grouped);
            }

            // Own costs
            var ownCosts = entity.Costs;
            var g = ownCosts.GroupBy(c => c.ProjectName);
            groups.AddRange(g);

            DonutChartData data = CreateGroupingDonut(externalBudget, groups, "Projects");
            SendAPIMessage("chart-data", data);
        }


        [RequestHandler("/api/admin/projects/chart/donut/project/budgets/{publicId}")]
        public void DonutProjectCostsBudgets(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                .GetByPublicId(publicId);
            var externalBudget = this.Params.Bool("external-budget", true);
            var budgetType = externalBudget ? ProjectBudget.BudgetTypes.External : ProjectBudget.BudgetTypes.Internal;

            var projects = entity.GetAllSubProjects(includeSelf: false).Where(p => p.Budget != null && p.Budget.HasValue == true);

            //// Take self if no children
            //if  (projects.Any () == false) {
            //    projects = new List<Project>() { entity };
            //}
            var data = new DonutChartData();
            data.Title = "Budgets";


            foreach (var project in projects) {
                var budget = project.Budget;
                var title = project.Name;
                var amount = budget?.Value != null ? (int)budget.Value.Value : 0;
                data.Items.Add(new DonutChartDataItem() { Name = title, Title = title, Value = amount });
            }

            data.UpdatePercentages();

            this.SendAPIMessage("chart-data", data);
        }

        /// <summary>
        /// Fill grouped costs into a donut
        /// </summary>
        /// <param name="externalBudget"></param>
        /// <param name="grouped"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        private  DonutChartData CreateGroupingDonut(bool externalBudget, IEnumerable<IGrouping<string, ProjectCost>> grouped, string title) {
            var data = new DonutChartData();
            data.Title = title;

            foreach (var costsByGroup in grouped) {
                var name = costsByGroup.Key.ToString();
                decimal groupedCosts = 0;
                if (externalBudget == true) {
                    groupedCosts = costsByGroup.Sum(c => c.Costs.Value.Value);
                } else {
                    groupedCosts = costsByGroup.Sum(c => c.CostsInternal.Value.Value);
                }

                
                data.Items.Add(new DonutChartDataItem() { Name = name, Title = name, Value = (int)groupedCosts });
            }

            data.UpdatePercentages();
            return data;
        }

        private IQueryable<ProjectCost> GetAllCostsOfProject(Project entity) {
            var costsOfProject = entity.GetAllCosts().Select(c => c.Id);
            var costs = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(e => costsOfProject.Contains(e.Id))
                .ToList()
                .AsQueryable();
            return costs;
        }
    }
}
