using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;

namespace Machinata.Module.Projects.Handler {

    public class AbsenceAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Absence> {

        [RequestHandler("/api/admin/projects/absences/create")]
        public void Create() {
            base.CRUDCreate();
        }

        [RequestHandler("/api/admin/projects/absence/{publicId}/edit")]
        public void Edit(string publicId) {
            base.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/projects/absence/{publicId}/delete")]
        public void Delete(string publicId) {
            base.CRUDDelete(publicId);
        }



    }
}
