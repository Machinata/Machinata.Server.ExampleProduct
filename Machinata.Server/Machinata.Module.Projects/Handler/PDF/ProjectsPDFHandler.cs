using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Module.Projects.Model;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsPDFHandler : PDFPageTemplateHandler {


        [RequestHandler("/pdf/projects/project/{publicId}/costs", "/admin/projects/*")]
        public void Costs(string publicId) {

            var project = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.WorkTypes))
                .GetByPublicId(publicId);

            var billingAddress = project.Business.GetBillingAddressForBusiness();
            var ownerBusinessAddress = this.DB.OwnerBusiness().GetBillingAddressForBusiness();
     

            var form = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.WorkType))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            var formTotal = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            this.InsertFilteredProjectCosts(project, true, form, formTotal);


            this.Template.Data.Replace("{line-break}", "<br/>");

            this.Template.InsertVariables("project", project);
            this.Template.InsertVariables("project.business", project.Business);
            this.Template.InsertVariable("date", DateTime.UtcNow.ToDefaultTimezoneDateTimeString());
            this.Template.InsertVariableXMLSafeWithLineBreaks("business.billing-address-breaked", billingAddress.ToString().Replace(", ", "\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("invoice.sender-address.city", ownerBusinessAddress.City);

            this.Template.InsertVariable("default-currency", Core.Config.CurrencyDefault);

            // Set footer
            this.FooterLeft = "{text.costs} " + project.Name;

            // Set filename
            this.SetFileName(project.GetPDFFileName());

        }



    }
}
