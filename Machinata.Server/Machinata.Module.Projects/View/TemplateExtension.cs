using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.View {
    public static class TemplateExtension {

        public static void InsertWeeklyEvents(this PageTemplate template, string variableName, IQueryable<ProjectEvent> entities, DateTime start, DateTime? maxDate = null) {
            //var startWeek = Core.Util.Time.StartOfWeek(start);
            var endWeek = start.EndOfWeek();
            var weeklyEvents = entities.Where(pt => pt.Date != null && (pt.Date >= start));

            var endTasks = weeklyEvents.Where(e => e.Date != null);
            var end = endWeek;

            if (maxDate.HasValue && endTasks.Any()) {
                end = new DateTime(System.Math.Min(endTasks.Max(e => e.Date.Value).Ticks, maxDate.Value.Ticks));
                weeklyEvents = weeklyEvents.Where(we => we.Date <= end);
            }
          
           

            template.InsertScheduleTable(
                  entities: weeklyEvents,
                  start: start,
                  end: end,
                  variableName: variableName,
                  templateName: "schedule-table",
                  dateSelector: (e) => (e as ProjectEvent).Date.Value,
                  getTemplateForDay: getProjectTaskDayTemplate,
                  selectedSchedule: new DateRange(start, endWeek),
                  startOfWeek: DayOfWeek.Monday,
                  endOfWeekToShow: DayOfWeek.Sunday
                  );
        }

        private static PageTemplate getProjectTaskDayTemplate(IEnumerable<ModelObject> events, DateTime deadline, PageTemplate template) {
            var dayTemplate = template.LoadTemplate("event-schedule.week.day");
            var eventTemplates = new List<PageTemplate>();

            foreach(var modelObejct in events) {
                var eventTemplate = dayTemplate.LoadTemplate("event-schedule.week.day.event");
                var eventObject = modelObejct as ProjectEvent;
                eventObject.Include(nameof(eventObject.Project));
                eventObject.Project.Include(nameof(eventObject.Project.Business));
                eventTemplate.InsertVariables("entity", eventObject);
                eventTemplate.InsertVariable("entity.business.title", eventObject.Project.Business.Name);
                eventTemplates.Add(eventTemplate);
            }

            // dayTemplate.InsertTemplates("events", events, "event-schedule.week.day.event");
            dayTemplate.InsertTemplates("events", eventTemplates);
            return dayTemplate;

           
        }

    }
}

