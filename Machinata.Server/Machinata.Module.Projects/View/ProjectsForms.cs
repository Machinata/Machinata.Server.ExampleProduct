using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Projects.Model.ProjectCost;

namespace Machinata.Module.Projects.View {
    public static class ProjectsForms {

        public static FormBuilder GetProjectCostFormBuilder(string projectCostDefault, DateTime defaultDate) {
            return new FormBuilder(Forms.Admin.EMPTY)
                .DropdownList(
                    "projectcosttype",
                    "projectcosttype",
                        new Dictionary<string, string>() { { "Internal", "Internal" }, { "External", "External" } },
                        projectCostDefault)
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.CostPerUnitInternal))
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.Note))
                .Include(nameof(ProjectCost.ProjectCostType))
                .Custom("date", "date", "date", value: defaultDate, required: true, readOnly: false);
        }

        public static FormBuilder GetTimesheetForm(IEnumerable<ProjectWorkType> workTypes, IEnumerable<User> projectUsers, User defaultUser, DateTime date, decimal units, ProjectWorkType worktype) {
            return new FormBuilder(Forms.Admin.EMPTY)
                .DropdownList("user", projectUsers, projectUsers.FirstOrDefault(pu => pu.Id == defaultUser?.Id))
                .DropdownList("worktype", workTypes, worktype)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Note))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.WorkType))
                .Custom("date", "date", "datetime", date, true, false)
                .Hidden("projectcosttype", ProjectCostTypes.Timesheet.ToString())
                .Custom("units", "units", "decimal", units, true, false);
        }


        public static void PopulateProjectCost(ProjectCost projectCost, Machinata.Core.Handler.Handler handler ) {
            projectCost.Populate(handler,
                new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.Note))
                .Include(nameof(ProjectCost.ProjectCostType))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.CostPerUnitInternal))
                );

            var startDate = handler.Params.DateTime("date", Core.Util.Time.GetDefaultTimezoneToday());
            projectCost.TimeRange = new DateRange(startDate, startDate);
            if (!projectCost.CostPerUnitInternal.HasValue) {
                projectCost.CostPerUnitInternal = projectCost.CostPerUnit.Clone();
            }
            

            projectCost.Validate();
        }

        public static void PopulateTimesheet(ProjectCost projectCost, Machinata.Core.Handler.Handler handler) {
            projectCost.Populate(handler,
                new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.Note))
                .Include(nameof(ProjectCost.WorkType)));

            if ( projectCost.WorkType == null) {
                throw new BackendException("timesheet-error", "No worktype selected");
            }

            var endDate = handler.Params.DateTime("date", Core.Util.Time.GetDefaultTimezoneToday());
            projectCost.CostPerUnit = projectCost.WorkType.CostPerUnit;
            projectCost.CostPerUnitInternal = projectCost.WorkType.CostPerUnitInternal;
            projectCost.Duration = TimeSpan.FromTicks((long)(projectCost.WorkType.GetUnitDuration().Ticks * projectCost.Units));
            projectCost.TimeRange = new DateRange(endDate - projectCost.Duration, endDate);
            projectCost.ProjectCostType = ProjectCost.ProjectCostTypes.Timesheet;
            projectCost.Billable = true;
            if (!projectCost.CostPerUnitInternal.HasValue) {
                projectCost.CostPerUnitInternal = projectCost.CostPerUnit.Clone();
            }

            projectCost.Validate();
        }


    }
}

