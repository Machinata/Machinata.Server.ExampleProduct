using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Model;
using Machinata.Core.Lifecycle;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class Project : ModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectTypes : short {
            Project = 10,
            CostEstimation = 20,
            CostEstimationTemplate = 30,
            Special = 100
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Project() {
            this.TimeRange = new DateRange();
            this.Tasks = new List<ProjectTask>();
            this.WorkTypes = new List<ProjectWorkType>();
            this.Costs = new List<ProjectCost>();
            this.Events = new List<ProjectEvent>();
            this.Budget = new Price();

            this.Notifications = new Properties();

        }

      
        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.API.LISTING)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public ProjectTypes ProjectType { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string ShortURL { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string ShortURLPath { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.Date)] 
        public DateRange TimeRange { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public ContentNode Description { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public ContentNode Notes { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [ContentTypeAttribute("color")]
        [Column]
        public string Color { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Timeline { get; set; } = true;

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Sticky { get; set; }

        [Column]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Completed { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Archived { get; set; }
        
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public Price Budget { get; set; }

        [Column]
        public Properties Notifications { get; set; }


       

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [ForeignKey("ParentId")]
        public Project Parent { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public Business Business { get; set; }
        
        [Column]
        public int? ParentId { get; set; }

        [Column]
        [InverseProperty("Parent")]
        public ICollection<Project> Children { get; set; }
        
        [Column]
        public ICollection<ProjectEvent> Events { get; set; }
        
        [Column]
        public ICollection<ProjectTask> Tasks { get; set; }
        
        [Column]
        public ICollection<ProjectWorkType> WorkTypes { get; set; }
        
        [Column]
        public ICollection<ProjectCost> Costs { get; set; }
        
        [Column]
        public ICollection<ProjectUser> Users { get; set; }
        
        #endregion
        
        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? Start {
            get {
                return this.TimeRange.Start;
            }
        }
        
        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? End {
            get {
                return this.TimeRange.End;
            }
        }
        
        [NotMapped]
        public bool Published {
            get {
                return this.Archived == false;
            }
        }
        
        [NotMapped]
        [FormBuilder]
        public bool IsRootProject {
            get {
                return this.ParentId == null;
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string Path {
            get {
                return $"{this.PublicId}/{this.ShortURLPath}";
            }
        }
        
        /// <summary>
        /// Admin url
        /// </summary>
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.System.LISTING)] 
        [NotMapped]
        public string URL {
            get {

                if (this.ProjectType == ProjectTypes.Project) {
                    return $"/admin/projects/project/{this.Path}";
                }
                if (this.ProjectType == ProjectTypes.CostEstimation || this.ProjectType == ProjectTypes.CostEstimationTemplate) {
                    return $"/admin/projects/cost-estimations/cost-estimation/{this.Path}";
                }
                throw new NotImplementedException("URL for project type not supported");
            }
        }

        [NotMapped]
        [FormBuilder]
        public int Level
        {
            get
            {
                // return this.ShortURLPath.Count(s => s == '/') - 1;
                int level = 0;
                Project project = GetParentProject(this);
                while (project != null) {
                    project = GetParentProject(project);
                    level++;
                }
                return level;
            }
        }

        public bool IsOverdue
        {
            get
            {
                return !this.Archived && !this.Completed && this.TimeRange.HasValue() && this.TimeRange.End.HasValue && this.TimeRange.End.Value < DateTime.UtcNow;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string BusinessFormalName {
            get {
                return this.Business?.FormalName;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public ProjectBudget CalculateBudget(ProjectBudget.BudgetTypes type) {
            if (this.Budget == null || this.Budget.HasValue == false) return null;

            var budget = new ProjectBudget();
            IList<ProjectCost> costs = null;

            if (type == ProjectBudget.BudgetTypes.External) {
                costs =  this.GetAllCosts().Where(c=>c.Billable).ToList();
                budget.TotalCosts = costs.Sum(c => c.Costs);

            }
            else if (type == ProjectBudget.BudgetTypes.Internal) {
                costs = this.GetAllCosts().ToList();
                budget.TotalCosts = costs.Sum(c => c.CostsInternal);
            }

            budget.TotalBudget = this.Budget;
         
            if (budget.TotalCosts == null || budget.TotalCosts.HasValue == false) {
                budget.TotalCosts = new Price(0); 
            }
            budget.TotalHours = (decimal)costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet).Sum(c => c.Duration.TotalHours);
           
            budget.RemainingBudget = budget.TotalBudget - budget.TotalCosts;

            //Estimated Budged
            if (budget.TotalHours > 0 && budget.TotalCosts != null && budget.TotalCosts.Value > 0) {
                var hourlyAverage = budget.TotalCosts.Value / budget.TotalHours;
                budget.EstimatedRemainingHours = System.Math.Round(budget.RemainingBudget.Value.Value / hourlyAverage.Value);
            }
        

            budget.OverBudget = null;
            if (budget.TotalCosts > budget.TotalBudget) budget.OverBudget = budget.TotalCosts - budget.TotalBudget;

            return budget;
        }

        public void ChangeName(ModelContext db,string newName, bool updateAllSubProjectsAlso = true, bool checkSiblingName = true) {
            // Update name
            this.Name = newName;
                  
            // Check name available
            if (checkSiblingName && this.GetSiblings(db).ToList().Any(c => c.Name == newName)) {
                throw new BackendException("name-taken", "Name is already taken by a sibling: " + newName);
            }

            // Update ShortURLs
            if (this.IsRootProject) {
                if(this.Business == null) this.Include(nameof(Project.Business));
                this.ShortURL = this.Business.ShortURL + "/" + Core.Util.String.CreateShortURLForCamelCased(newName);
                this.ShortURLPath = this.ShortURL;
            } else {
                this.ShortURL = Core.Util.String.CreateShortURLForCamelCased(newName);
                this.ShortURLPath = this.Parent.ShortURLPath + "/" + this.ShortURL;
            }
            // Update all children
            if (updateAllSubProjectsAlso) {
                this.Include(nameof(Project.Children));
                foreach (var subproject in this.Children) {
                    subproject.ChangeName(db, subproject.Name,true,false);
                }
            }
        }

        /// <summary>
        /// Gets the siblings of a project.
        /// If root project -> all root projects of same business
        /// </summary>
        /// <value>
        /// The siblings.
        /// </value>
        public IEnumerable<Project> GetSiblings(ModelContext db) {
            if (this.IsRootProject) {
                var business = GetProjectBusiness();
                return db.Projects().Where(p => p.ParentId == null && p.Business.Id == business.Id && p.Id != this.Id);
            }
            if (this.Context != null) {
                this.Include(nameof(Project.Parent));
            }
            this.Parent.Include(nameof(Project.Children));
            return this.Parent.Children.Where(p=>p.Id != this.Id);
        }


        public List<Project> GetAllSubProjects(bool includeSelf = true, params string [] includePaths) {
            this.Include(nameof(Project.Children));
            if (includePaths!= null) {
                foreach(var includePath in includePaths) {
                    this.Include(includePath);
                }
            }

            var projects = new List<Project>();
            foreach(var child in this.Children) {
                projects.AddRange( child.GetAllSubProjects(true, includePaths));
            }
            if (includeSelf) {
                projects.Add(this);
            }
          
            return projects;
            
        }

        /// <summary>
        /// Gets all costs of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectCost> GetAllCosts() {
            var projects = GetAllSubProjects(true, nameof(Project.Costs));
            return projects.SelectMany(p => p.Costs);
        }

      

        /// <summary>
        /// Gets all ubilled costs of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectCost> GetUnbilledProjectCosts() {
            // Open Orders
            return this.GetAllCosts().Where(c => c.Billed.HasValue == false && c.Billable);
        }

        /// <summary>
        /// Gets all events of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectEvent> GetAllEvents() {
            var projects = GetAllSubProjects(true, nameof(Project.Events));
            return projects.SelectMany(p => p.Events);
        }

        /// <summary>
        /// Gets all tasks of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectTask> GetAllTasks() {
            var projects = GetAllSubProjects(true, nameof(Project.Tasks));
            return projects.SelectMany(p => p.Tasks);
        }

        /// <summary>
        /// Gets the work types of the projects root project
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectWorkType> GetWorkTypes() {
            Project root = this;
            if (!this.IsRootProject) {
                root = this.GetRootProject();
            }
            root.Include(nameof(Project.WorkTypes));
            return root.WorkTypes;
        }
                
        public Project GetRootProject() {
            if (this.IsRootProject) return this;

            if (this.Parent == null) this.Include(nameof(Project.Parent));
            return this.Parent.GetRootProject();
        }

        public static Project GetParentProject(Project project) {
            if (project.IsRootProject) return null;
            if (project.Parent == null) project.Include(nameof(Project.Parent));
            return project.Parent;
        }

        /// <summary>
        /// Gets all projectusers of its root project
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectUser> GetProjectUsers(bool includeUser = false) {
            var rootProject = this.GetRootProject();
            rootProject.Include(nameof(Project.Users));
            if (includeUser) {
                foreach (var projectUser in rootProject.Users) {
                    projectUser.Include(nameof(projectUser.User));
                }
            }
            return rootProject.Users;
        }

        public Business GetProjectBusiness() {
            var rootProject = this.GetRootProject();
            if (this.Context != null) {
                rootProject.Include(nameof(Project.Business));
            }
            return rootProject.Business;
        }


        /// <summary>
        /// Gets all Invoices of this and all sub projects
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<Invoice> GetAllInvoices() {
        //    var projects = GetAllSubProjects(true, nameof(Project.Costs));
        //    var costs = projects.SelectMany(p => p.Costs).ToList();
        //    costs.ForEach(p => p.Include(nameof(ProjectCost.Invoice)));
        //    return costs.Where(c => c.Invoice != null).Select(c => c.Invoice).Distinct();
        //}

        public IEnumerable<Invoice> GetAllInvoicesFast() {
            var projects = GetAllSubProjects(true, nameof(Project.Costs));
            var costs = projects.SelectMany(p => p.Costs).ToList();
            var costIds = costs.Select(c => c.Id);
            var costsWithInvoice =  this.Context.ProjectCosts().Include(nameof(ProjectCost.Invoice)).Where(c => costIds.Contains(c.Id));

           // costs.ForEach(p => p.Include(nameof(ProjectCost.Invoice)));
            return costsWithInvoice.Where(c => c.Invoice != null).Select(c => c.Invoice).Distinct();
        }


        public void AddNewProjectUser(User user) {
            var newUser = new ProjectUser();
            newUser.User = user;
            newUser.Flags = ProjectUser.ProjectUserFlags.Subscribed;
            this.Users.Add(newUser);
        }


        // Subscribe to Invoice Status changes
        [OnApplicationStartup]
        public static void OnApplicationStartup() {
            Business.BusinessNameChangedEvent += Project.HandleBusinessNameChanged;
        }

        public static void HandleBusinessNameChanged(Business business, string oldName) {
            var projects = business.Context.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.ParentId == null);

            foreach (var project in projects) {
                project.ChangeName(business.Context, project.Name);
            }
        }

        /// <summary>
        /// Set the color of the project if business has one
        /// </summary>
        public void SetColorFromBusiness() {
            if (this.Business.Settings.Keys.Contains(Config.BUSINESS_COLOR_PROPERTY_KEY)) {
                this.Color = this.Business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString();
            }
        }

        public static ProjectColor GetColor(Business business) {
            if (business.Settings.Keys.Contains(Config.BUSINESS_COLOR_PROPERTY_KEY) && !string.IsNullOrEmpty(business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString())) {
                return  new ProjectColor() { Color = business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString() };
            }
            return new ProjectColor();
        }

        public string GetPDFFileName() {
            var name = $"{Core.Config.ProjectID}_Costs_{this.Name}";
            if (!string.IsNullOrEmpty(this.Business?.Name)) {
                name += "_" + this.Business.Name;
            }
            return name + ".pdf";
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            // Currently always Project Type
            this.ProjectType = ProjectTypes.Project;

        }

        public override CardBuilder VisualCard() {

            // Init card
            this.Include(nameof(Business));
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Business?.Name)
                .Sub(this.ProjectType)
                .Sub(this.TimeRange?.ToString(Core.Config.DateFormat))
                .Icon("star-full-outline")
                .Wide();
            if (this.IsOverdue) {
                card.BlinkSub();
            }

            card.Link("/admin/projects/project/" + this.Path);
            
            return card;
        }

        public IList<Finance.Model.LineItemGroup> GenerateLineItemGroups(IEnumerable<ProjectCost> costs, Invoice invoice) {
            this.LoadFirstLevelNavigationReferences();
            this.LoadFirstLevelObjectReferences();
            var billedDate = DateTime.UtcNow;
            var groups = new List<Finance.Model.LineItemGroup>();
            {
                // Order group
                var group = new Finance.Model.LineItemGroup();
               
                // Default Timerange
                group.TimeRange = this.TimeRange.Clone();

                // Try Timerange from first cost to last cost
                if (costs != null && costs.Any(c => c.TimeRange?.Start != null) && costs.Any(c => c.TimeRange?.End != null)) {
                    var min = costs.Where(c => c.TimeRange?.Start != null).Min(c => c.TimeRange.Start.Value);
                    var max = costs.Where(c => c.TimeRange?.End != null).Max(c => c.TimeRange.End.Value);
                    group.TimeRange = new DateRange(min, max);
                }
                
                group.Title = "{text.project} " + this.Name;
                group.Description = this.GetDescription();
                group.Category = "{text.project}";

                // Items
                foreach (var cost in  costs) {
                    cost.LoadFirstLevelNavigationReferences();
                    cost.LoadFirstLevelObjectReferences();
                    var lineItem = new Finance.Model.LineItem();
                   
                    // General
                    lineItem.CustomerItemPrice = cost.CostPerUnit.Clone();
                    lineItem.InternalItemPrice = cost.CostPerUnitInternal.Clone();
                    lineItem.Category = "{text.project-cost}";
                    lineItem.SubCategory = cost.WorkType?.Name;

                    lineItem.Quantity = cost.Units;
                    lineItem.CustomerVATRate = (decimal?)(Config.ProjectsDefaultVatRate / 100);
                    lineItem.TimeRange = cost.TimeRange?.Clone();

                    // Set totals baked in
                    lineItem.CustomerVAT = lineItem.CustomerItemPrice * lineItem.CustomerVATRate;
                    lineItem.CustomerTotal = lineItem.CustomerSubtotal + lineItem.CustomerVAT * lineItem.Quantity;

                    // Description
                    var description = Core.Config.GetStringSetting("ProjectsInvoiceGenerationLineItemText"+ cost.ProjectCostType.ToString());
                    description = description.Replace("cost.description".TemplateVar(), cost.Description);
                    if (cost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet) {
                        description = description.Replace("cost.work-type.name".TemplateVar(), cost.WorkType.Name);
                        description = description.Replace("cost.work-type.type".TemplateVar(), cost.WorkType.WorkUnit.ToString());
                        description = description.Replace("cost.duration".TemplateVar(), cost.Duration.TotalHours.ToString("0.00h"));
                        description = description.Replace("cost.user.name".TemplateVar(), cost.User?.Name);
                    }
                    description = description.Replace("cost.note".TemplateVar(), cost.Note);
                    description = description.ReplacePrefix(", ", "");
                    description = description.ReplaceSuffix(", ", "");
                    description = description.Trim();
                    lineItem.Description = description;

                    cost.LineItem = lineItem;
                    cost.Invoice = invoice;
                    cost.Billed = billedDate;
                    group.LineItems.Add(lineItem);
                }
                               
                groups.Add(group);
            }
            return groups;
        }

        /// <summary>
        /// Gets the description of the projects.
        /// </summary>
        /// <returns></returns>
        public string GetDescription() {
            this.Include(nameof(this.Description));
            if (this.Description != null && !string.IsNullOrEmpty(this.Description.ToString())) {
                return this.Description.ToString();
            }
            //If none available tries with ancestors
            //if (this.ParentId != null) {
            //    this.Include(nameof(this.Parent));
            //    return this.Parent.GetDescription();
            //}
            return null;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectExtensions {
        [ModelSet]
        public static DbSet<Project> Projects(this Core.Model.ModelContext context) {
            return context.Set<Project>();
        }
                
        public static Project GetByShortUrlPath(this IQueryable<Projects.Model.Project> query, string shortUrlPath, bool throwExceptions = true){
            var ret = query.Where(e => e.ShortURLPath == shortUrlPath).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("project-not-found", $"The project with path {shortUrlPath} could not be found.");
            return ret;
        }

        public static Project GetByPath(this IQueryable<Projects.Model.Project> query, string path, bool throwExceptions = true) {
            var publicId = path?.Split('/').FirstOrDefault();
            return query.GetByPublicId(publicId);
        }

        public static Project GetRootByPath(this IQueryable<Projects.Model.Project> query, string path, bool throwExceptions = true) {
            var rootPath = string.Join("/", path.Split('/').Skip(1).Take(2));
            return query.GetByShortUrlPath(rootPath);
        }
    }

    #endregion


    public class ProjectColor : ModelObject {

        [FormBuilder(Forms.Admin.VIEW)]
        public string Color { get; set; }
    }
}


