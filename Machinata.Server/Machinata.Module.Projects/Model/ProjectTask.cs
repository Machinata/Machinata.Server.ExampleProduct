using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using Machinata.Core.Cards;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class ProjectTask : ModelObject, IPublishedModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectTaskTypes : short {
            ToDo = 10,
            Checklist = 20,
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectTask() {

        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public bool Completed { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.CREATE)]
        [Column]
        public DateTime? Deadline { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public DateTime? DateCompleted { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Note { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public TimeSpan Duration { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public ContentNode Description { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public Project Project { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public ProjectUser User { get; set; }

        #endregion

        #region Public Properties Derived //////////////////////////////////////////////////////        

        [FormBuilder]
        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public string ProjectPath { get { return this.Project?.Path; } }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ProjectName {
            get { return this.Project?.Name; }
        }

        [FormBuilder]
        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public string URL
        {
            get
            {
                return $"/admin/projects/project/task/{this.ProjectPath}/{this.PublicId}";
            }
        }

        
        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public bool IsOverdue {
            get { return this.Deadline != null && this.Deadline < DateTime.UtcNow && this.Completed == false; }
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public bool Published {
            get {
                return this.Completed == false;
            }
        }

        public override CardBuilder VisualCard() {

            this.Include(nameof(ProjectTask.Project));
            this.Project.Include(nameof(Model.Project.Business));
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Subtitle(this.ProjectName)
                .Sub(this.Project?.Business?.Name)
                .Icon("flash")
                .Wide()
                .Link($"/admin/projects/project/task/{this.ProjectPath}/" + $"{this.PublicId}");
            if (this.IsOverdue) {
                card.BlinkSub();
            }

            return card;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectTaskExtensions {
        [ModelSet] 
        public static DbSet<ProjectTask> ProjectTasks(this Core.Model.ModelContext context) {
            return context.Set<ProjectTask>();
        }
     }

    public static class IQueryableProjectTaskExtenions {
        public static IQueryable<T> GetByUser<T>(this IQueryable<T> query, Core.Model.ModelContext context, User user) where T : ProjectTask {
            IQueryable<int> projectUsers = context.ProjectUsers().GetByUser(user, false).Select(pu => pu.Id);
            query = query.Where(wt => projectUsers.Contains(wt.User.Id));
            return query;
        }
    }


    #endregion

}
