using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using Machinata.Core.Cards;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class ProjectEvent : ModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectEventTypes : short {
            Start = 1,
            End = 2,
            Milestone = 10,
            Meeting = 20,
            Deadline = 30,
            Deliverable = 40,
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectEvent() {

        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public ProjectEventTypes ProjectEventType { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public DateTime? Date { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [CascadeDelete]
        [Column]
        public ContentNode Description { get; set; }
        
        #endregion
        
        #region Public Navigation Properties //////////////////////////////////////////////////////        
        
        [Column]
        public Project Project { get; set; }

        #endregion

        #region Public Properties Derived //////////////////////////////////////////////////////        
        
        public bool Published {
            get {
                if (this.Date.HasValue) {
                    return this.Date.Value.Date >= DateTime.UtcNow.Date;
                } else {
                    return true;
                }
                
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string ProjectPath { get { return this.Project?.Path; } }
        
        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string URL {
            get {
                return $"/admin/projects/project/event/{this.ProjectPath}/{this.PublicId}";
            }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ProjectName
        {
            get { return this.Project?.Name; }
        }

        [FormBuilder]
        public string Name
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Title)) {
                    return this.ToString();
                }
                return this.Title;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public bool IsOverdue
        {
            get { return this.Date != null && this.Date < DateTime.UtcNow && ( this.ProjectEventType == ProjectEventTypes.Deadline || this.ProjectEventType == ProjectEventTypes.Milestone); }
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        

        public override CardBuilder VisualCard() {
            this.Include(nameof(ProjectEvent.Project));
            this.Project.Include(nameof(Model.Project.Business));
            var card = new Core.Cards.CardBuilder(this)
              .Title(this.Title)
              .Sub(this.Project?.Business?.Name)
              .Icon("flag")
              .Wide()
              .Link($"/admin/projects/project/event/{this.ProjectPath}/" + $"{this.PublicId}");
            if (this.IsOverdue) {
                card.BlinkSub();
            }
            if (this.Date.HasValue) {
                card.Subtitle(this.Date.Value.ToDefaultTimezoneDateTimeString());
            }
            return card;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectEventExtensions {
        [ModelSet]
        public static DbSet<ProjectEvent> ProjectEvents(this Core.Model.ModelContext context) {
            return context.Set<ProjectEvent>();
        }
    }

    #endregion

}
