using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class ProjectUser : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Enums /////////////////////////////////////////////////////////////////////////////

        [Flags]
        public enum ProjectUserFlags {
            Subscribed,
            Manager,
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectUser() {

        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ProjectUserFlags Flags { get; set; }
        
        #endregion
        
        #region Public Navigation Properties //////////////////////////////////////////////////////        
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
     //   [FormBuilder(Forms.Admin.LISTING)]
        public User User { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Project Project { get; set; }
        
        [Column]
        public ICollection<ProjectTask> Tasks { get; set; }

        #endregion


        #region Derived Properties //////////////////////////////////////////////////////        


        [FormBuilder(Forms.Admin.LISTING)]
        public string Username
        {
            get
            {
                if (this.Context != null) {
                    this.Include(nameof(ProjectUser.User));
                }
                return this.User?.Username;
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        public override string ToString() {
            if (this.Context != null) {
                this.Include(nameof(ProjectUser.User));
            }
            return this.User?.ToString();
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectUserExtensions {
        [ModelSet]
        public static DbSet<ProjectUser> ProjectUsers(this Core.Model.ModelContext context) {
            return context.Set<ProjectUser>();
        }
    }

    public static class IQueryableProjectUserExtenions {
        public static IQueryable<T> GetByUser<T>(this IQueryable<T> query, User user, bool throwException = true) where T : ProjectUser {
            var projectUsers = query.Where(pu => pu.User.Id == user.Id);
            return projectUsers;
        }
    }

    #endregion

}
