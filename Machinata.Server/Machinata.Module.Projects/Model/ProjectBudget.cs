using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Model;
using System.Globalization;

namespace Machinata.Module.Projects.Model {

    /// <summary>
    /// Helper class that represents a projects budget vs spend/costs.
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    [Serializable()]
    public partial class ProjectBudget : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////

        public enum BudgetTypes : short {
            External = 10,
            Internal = 20
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        #endregion
        
        #region Public Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price TotalBudget { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price TotalCosts { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public decimal TotalHours { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price RemainingBudget { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public decimal? RemainingHours {
            get {
                if (TotalCosts > TotalBudget || TotalCosts == null || TotalCosts.Value == 0 || this.TotalHours == 0) return null;
                // Estimatation based on current budget
                var costPerHour = TotalCosts.Value / TotalHours;
                return (int)(RemainingBudget.Value / costPerHour);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price AverageCostPerHour {
            get {
                if (TotalCosts == null || TotalCosts.Value == 0 || this.TotalHours == 0 ) return null;
                var costPerHour = TotalCosts.Value / TotalHours;
                return new Price((int)costPerHour, TotalCosts.Currency);
            }
        }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string PercentUsed {
            get {
                if (this.TotalBudget.HasValue && this.TotalBudget.Value != 0) {
                    var p = (this.TotalCosts.Value / this.TotalBudget.Value).Value;
                    return p.ToString("P", CultureInfo.InvariantCulture);
                }
                return null;
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price OverBudget { get; set; }

        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.LISTING)]
        public BudgetTypes Type { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public decimal? EstimatedRemainingHours { get; set; }

        /// <summary>
        /// TODO MERGE WITH PERCENTAGE USED GETTER
        /// </summary>
        public decimal? BudgetUsed {
            get {
                if (this.TotalBudget.HasValue && this.TotalBudget.Value != 0) {
                    var p = (this.TotalCosts.Value / this.TotalBudget.Value).Value;
                    return p;
                }
                return null;
            }
        }



        #endregion


    }
    
}
