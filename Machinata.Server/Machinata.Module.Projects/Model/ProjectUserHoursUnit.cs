using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Model;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace Machinata.Module.Projects.Model {

    /// <summary>
    /// Helper class that represents a the working hours of a user by days
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    [Serializable()]
    public class ProjectUserHoursUnit : ModelObject {


        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion



        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectUserHoursUnit(User user) {
            this.User = user;
        }

        #endregion

        #region Public Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.EXPORT)]
        public string Name { get { return this.User?.Name; } }
            
        public Dictionary<int, double> DayHours = new Dictionary<int, double>();

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public User User { get; set; }


        #endregion


        /// <summary>
        /// Creates a List of ProjectUserHours per user, with all the working hours summed up per day (local time)
        /// </summary>
        /// <param name="db"></param>
        /// <param name="start">start date of report in UTC</param>
        /// <param name="end">end date of report in UTC</param>
        /// <param name="users"></param>
        /// <param name="excludeOwnerBusiness">exlude the hours of the owner busines</param>
        /// <returns></returns>
        public static List<ProjectUserHoursUnit> CreateDailyHoursReport(ModelContext db, DateTime start, DateTime end, bool excludeOwnerBusiness, string monthName) {

            // This is expensiv but cannont query with ProjectCost.TimeRage ??
            var projectCosts = db.ProjectCosts()
               .Include(nameof(ProjectCost.Project))
               .Include(nameof(ProjectCost.User))
               .Include(nameof(ProjectCost.Business))
               .Where(pc => pc.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet
                         && pc.User != null).ToList();

            // Project Costs in time range
            projectCosts = projectCosts.Where(pc =>
                            pc.TimeRange != null
                            && pc.TimeRange.End != null
                            && pc.TimeRange.Start >= start
                            && pc.TimeRange.End < start.AddMonths(2) // Cap for performance
                            ).ToList();

            // Owner Business?
            var ownerBusiness = db.OwnerBusiness();
            if (excludeOwnerBusiness == true && ownerBusiness != null) {
                projectCosts = projectCosts.Where(pc => pc.Business != ownerBusiness).ToList();
            }

            // Dynamically create user list by available costs
            var users = projectCosts.Select(pc =>pc.User).DistinctBy(u=>u.Id).Where(u => u.IsSuperUser == false);


            var result = new List<ProjectUserHoursUnit>();
            var usedCosts = new List<ProjectCost>();

            var startDayLocalTime = Time.ConvertToDefaultTimezone(start).Day;
            var endDayLocalTime = Time.ConvertToDefaultTimezone(end).Day;
            foreach (var user in users) {
                      
                var unit = new ProjectUserHoursUnit(user);
                for (int day = startDayLocalTime; day <= endDayLocalTime; day++) {
       
                    var costs = projectCosts.Where(
                        pc => pc.User == user 
                        && Time.ConvertToDefaultTimezone(pc.TimeRange.Start.Value).Day == day
                        && pc.TimeRange.Start.Value <= end
                        ).ToList();

      

                    // Book keeping
                    usedCosts.AddRange(costs);

                    // Sum up
                    unit.DayHours[day] = costs.Sum(c => c.Duration.TotalHours);

        

                }

                result.Add(unit);
            }


            // Debuggin / Testing
            var unusedCosts = (projectCosts.Except(usedCosts)).Where(pc => pc.TimeRange.Start.Value <= end);


            _logger.Info("Unused costs in rapport: ");
            foreach (var cost in unusedCosts) {
                _logger.Info(cost.URL);
            }

            _logger.Info("spanning multiple days costs in rapport: ");
            foreach (var cost in usedCosts.Where(pc => Time.ConvertToDefaultTimezone(pc.TimeRange.End.Value).Date != Core.Util.Time.ConvertToDefaultTimezone(pc.TimeRange.Start.Value).Date)) {
                _logger.Info(cost.URL);
            }


            // Check results
            var allCostsSum = projectCosts.Where(pc => pc.TimeRange.Start.Value <= end && pc.User.IsSuperUser == false).Sum(p => p.Units);
            var resultsSum = result.Sum(r => r.DayHours.Values.Sum());

           if (System.Math.Abs((double)allCostsSum - resultsSum) > 1) {
                //throw new BackendException("hours-error", "There is an error in the hour calculations in the month");

              
            }
            _logger.Info("Project Hours of: " + monthName);
            _logger.Info("Costs in time range: " + allCostsSum);
            _logger.Info("Costs in report: " + resultsSum);

            return result;
        }

        public static byte[] ExportXLSX(IEnumerable<ProjectUserHoursUnit> units, string name) {

            XLSX xlsx = new XLSX(name);

            foreach (var unit in units) {
                var row = new Dictionary<string, string>();
                row.Add("Name", unit.Name);

                foreach (var day in unit.DayHours) {
                    row.Add(day.Key.ToString(), day.Value.ToString("0.00"));
                }

                xlsx.AddRow(row);
            }

            return xlsx.GetContent();
        }

    }


}
