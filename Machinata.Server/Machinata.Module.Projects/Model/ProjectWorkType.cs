using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class ProjectWorkType : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        
        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum WorkUnitTypes : short {
            Hourly = 1,
            Daily = 2,
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectWorkType() {
            this.CostPerUnit = new Price();
            this.CostPerUnitInternal = new Price();
        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Group { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Description { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public WorkUnitTypes WorkUnit { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public Price CostPerUnit { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Price CostPerUnitInternal { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<Project> Projects { get; set; }

        [NotMapped]
        public string URL { get {
                return "/admin/projects/worktypes/worktype/" + this.PublicId;
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public TimeSpan GetUnitDuration() {
            if (this.WorkUnit == WorkUnitTypes.Daily) {
                return new TimeSpan(24, 0, 0);
            }
            if(this.WorkUnit == WorkUnitTypes.Hourly) {
                return new TimeSpan(1, 0, 0);
            }
            throw new Exception($"{this.WorkUnit} not supported yet.");
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        public override string ToString() {
            return $"{this.Name}: {this.WorkUnit}";
        }

      
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectWorkTypeExtensions {
        [ModelSet]
        public static DbSet<ProjectWorkType> ProjectWorkTypes(this Core.Model.ModelContext context) {
            return context.Set<ProjectWorkType>();
        }
    }

    #endregion

}
