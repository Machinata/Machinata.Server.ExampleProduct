using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {
    /// <summary>
    /// 
    /// See https://developers.google.com/maps/documentation/geocoding/start
    /// </summary>
    /// <seealso cref="Machinata.Module.Services.Providers.GeocodingProvider" />
    public class GoogleGeoCodingProvider : GeoCodingProvider {

        public override GeoCodingData DataForAddress(string address) {
            // https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
            /*
            {
               "results" : [
                  {
                     "address_components" : [
                        {
                           "long_name" : "1600",
                           "short_name" : "1600",
                           "types" : [ "street_number" ]
                        },
                        {
                           "long_name" : "Amphitheatre Pkwy",
                           "short_name" : "Amphitheatre Pkwy",
                           "types" : [ "route" ]
                        },
                        {
                           "long_name" : "Mountain View",
                           "short_name" : "Mountain View",
                           "types" : [ "locality", "political" ]
                        },
                        {
                           "long_name" : "Santa Clara County",
                           "short_name" : "Santa Clara County",
                           "types" : [ "administrative_area_level_2", "political" ]
                        },
                        {
                           "long_name" : "California",
                           "short_name" : "CA",
                           "types" : [ "administrative_area_level_1", "political" ]
                        },
                        {
                           "long_name" : "United States",
                           "short_name" : "US",
                           "types" : [ "country", "political" ]
                        },
                        {
                           "long_name" : "94043",
                           "short_name" : "94043",
                           "types" : [ "postal_code" ]
                        }
                     ],
                     "formatted_address" : "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA",
                     "geometry" : {
                        "location" : {
                           "lat" : 37.4224764,
                           "lng" : -122.0842499
                        },
                        "location_type" : "ROOFTOP",
                        "viewport" : {
                           "northeast" : {
                              "lat" : 37.4238253802915,
                              "lng" : -122.0829009197085
                           },
                           "southwest" : {
                              "lat" : 37.4211274197085,
                              "lng" : -122.0855988802915
                           }
                        }
                     },
                     "place_id" : "ChIJ2eUgeAK6j4ARbn5u_wAGqWA",
                     "types" : [ "street_address" ]
                  }
               ],
               "status" : "OK"
            }
            */
            var key = Core.Config.GetStringSetting("GoogleGeoCodingAPIKey");
            var addressParam = address.Replace("\n", ",").Replace(" ", "+");
            var url = $"https://maps.googleapis.com/maps/api/geocode/json?address={addressParam}&key={key}";
            var json = Core.Util.HTTP.GetJSON(url);
            return _parseJSONResults(json);
        }

        public override GeoCodingData DataForLatLon(string lat, string lon) {
            // https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
            var key = Core.Config.GetStringSetting("GoogleGeoCodingAPIKey");
            var url = $"https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lon}&key={key}";
            var json = Core.Util.HTTP.GetJSON(url);
            return _parseJSONResults(json);
        }

        private GeoCodingData _parseJSONResults(JToken json) {
            GeoCodingData data = new GeoCodingData();
            //TODO: handle multiple results?
            data.Formatted = json["results"][0]["formatted_address"].ToString();
            data.Lat = json["results"][0]["geometry"]["location"]["lat"].ToString();
            data.Lon = json["results"][0]["geometry"]["location"]["lng"].ToString();
            //TODO: addresses
            return data;
        }
    }
}
