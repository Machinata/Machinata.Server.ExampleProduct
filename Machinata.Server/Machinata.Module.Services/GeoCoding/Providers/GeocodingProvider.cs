using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {

    public class GeoCodingData {
        public string Lat;
        public string Lon;
        public string Formatted;
        public Address AddressLong;
        public Address AddressShort;
        
        public double LatAsDouble() {
            return double.Parse(Lat);
        }
        public double LonAsDouble() {
            return double.Parse(Lon);
        }
    }

    public abstract class GeoCodingProvider {

        public abstract GeoCodingData DataForAddress(string address);

        public abstract GeoCodingData DataForLatLon(string lat, string lon);

    }
}
