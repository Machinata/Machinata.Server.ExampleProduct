using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {

    /// <summary>
    /// See https://fixer.io/
    /// </summary>
    public class FixerExchangeRateProvider : ExchangeRateProvider {

        public override ExchangeRateData DataForCurrency(string baseCurrency) {
            // https://api.fixer.io/latest?base=USD
            /*
            {
              "base": "USD",
              "date": "2018-03-01",
              "rates": {
                "AUD": 1.2937,
                "BGN": 1.6069,
                "BRL": 3.2651,
                "CAD": 1.2862,
                "CHF": 0.94643,
                "CNY": 6.3503,
                "CZK": 20.897,
                "DKK": 6.1186,
                "EUR": 0.82163,
                "GBP": 0.7273,
                "HKD": 7.827,
                "HRK": 6.1195,
                "HUF": 257.81,
                "IDR": 13803,
                "ILS": 3.4834,
                "INR": 65.175,
                "ISK": 101.64,
                "JPY": 106.79,
                "KRW": 1087.4,
                "MXN": 18.927,
                "MYR": 3.9285,
                "NOK": 7.9369,
                "NZD": 1.3858,
                "PHP": 51.959,
                "PLN": 3.4387,
                "RON": 3.8266,
                "RUB": 56.839,
                "SEK": 8.3132,
                "SGD": 1.3269,
                "THB": 31.57,
                "TRY": 3.8152,
                "ZAR": 11.93
              }
            }
            */
            var url = $"http://data.fixer.io/latest?access_key={Config.FixerApiAccessKey}";
            var json = Core.Util.HTTP.GetJSON(url);
         
            var responseBase = json["base"].ToString();
            var date = json["date"].ToString();
            var rates = json["rates"].ToObject<JObject>();

            decimal baseConvert = 1;
            if (responseBase != baseCurrency) {
                baseConvert = decimal.Parse(rates[baseCurrency].ToString());
            
            }
            ExchangeRateData data = new ExchangeRateData();
            data.Currency = baseCurrency;

            foreach (var rate in rates) {
                var val = decimal.Parse(rate.Value.ToString(), System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent);
                data.Rates[rate.Key] = Math.Round( val / baseConvert,6);
               
            }
           
            return data;
        }
        
    }
}
