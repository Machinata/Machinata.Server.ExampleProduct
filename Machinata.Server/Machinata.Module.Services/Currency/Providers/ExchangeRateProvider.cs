using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {

    public class ExchangeRateData {
        public string Currency;
        public string Date;
        public Dictionary<string,decimal> Rates = new Dictionary<string, decimal>();
    }

    public abstract class ExchangeRateProvider {

        public abstract ExchangeRateData DataForCurrency(string baseCurrency);

    }
}
