using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Tasks {

    public class CurrencyRateUpdateTask : Core.TaskManager.Task {
        public override void Process() {

            // Get and set the exchange rate for the default currency, for all supported currencies...
            Log($"Finding exchange rates for default currency {Core.Config.CurrencyDefault}...");
            var exchangeRates = Machinata.Module.Services.ExchangeRate.Default.DataForCurrency(Core.Config.CurrencyDefault);
            // Validate
            if (exchangeRates.Currency.ToUpper() != Core.Config.CurrencyDefault.ToUpper()) throw new BackendException("invalid-exchangerates","Could not get exchange rates for "+Core.Config.CurrencyDefault);
            foreach(var rate in exchangeRates.Rates) {
                Log($"  {Core.Config.CurrencyDefault} to {rate.Key}: {rate.Value}");
            }
            foreach(var currency in Core.Config.CurrencySupported) {
                // Don't do self
                if (currency == Core.Config.CurrencyDefault) continue;
                // Key
                var from = Core.Config.CurrencyDefault.ToUpper();
                var to = currency.ToUpper();
                var key = $"CurrencyConversionRate{from}to{to}";
                Log($"Finding exchange rate {from} to {to}...");
                // Find exchage rate
                var rates = exchangeRates.Rates.Where(kvp => kvp.Key.ToUpper() == to);
                if(rates.Count() != 1) throw new BackendException("missing-exchangerates",$"Could not get exchange rate for {from} to {to}!");
                var rate = rates.Select(kvp => kvp.Value).SingleOrDefault();
                Log($"  Exchange rate {from} to {to}: {rate}");
                Log($"  Setting {key} to rate {rate}");
                // Set value
                Core.Config.Dynamic.Set(key, rate.ToString());
            }

        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1d";
            return config;
        }
    }
}
