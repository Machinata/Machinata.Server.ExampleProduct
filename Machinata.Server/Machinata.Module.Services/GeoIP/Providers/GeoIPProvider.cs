using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {

    public class GeoIPData {
        public string IP;
        public string Lat;
        public string Lon;
        public string MetroCode;
        public string Timezone;
        public Address Address;
    }

    public abstract class GeoIPProvider {

        public abstract GeoIPData DataForIP(string ip);

    }
}
