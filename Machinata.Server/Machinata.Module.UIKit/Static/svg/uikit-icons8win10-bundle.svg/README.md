﻿## Adding new Icons

 - Go to https://icons8.com/icons/windows and login
 - Under Collections (bottom left) make sure to select "Nerves UI-Kit"
 - Use the search icons to find your icon you need (make sure you stay within the icon style "Windows 10")
 - Add the icon to the collection
 - Download all the icons using SVG, 50px, "ZIP with individual SVGs"
 - Open the zip file and place the new icon(s) into the bundle folder (make sure the files are set to Copy If Newer/Content, validate that the file name matches the other files)
