
/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {

    Machinata.Responsive.registerResponsiveDefinitions([
        {
            "id": "uikit-coverpage-full",
            "selector": ".ui-coverpage.option-full",
            "max-height": "Machinata.Math.makeEven({window.height} - {header.height})",
        },
        {
            "id": "uikit-coverpage-video",
            "selector": ".ui-coverpage .media.video .video-container",
            "aspect-ratio": "16/9",
            "cover": "fill",
        },
        {
            "id": "uikit-coverpage-half",
            "selector": ".ui-coverpage.option-half",
            "max-height": "Machinata.Math.makeEven({window.height} - {header.height})/2",
        },
        {
            "id": "uikit-coverpage-fold",
            "selector": ".ui-coverpage.option-show-fold",
            "max-height": "Machinata.Math.makeEven({window.height} - {header.height} - 32)",
        },
        {
            "id": "uikit-layout-fullbleed",
            "selector": ".ui-layout-fullbleed",
            "margin-left": "Math.floor(-({body.width}-{container.width})/2)",
            "margin-right": "Math.floor(-({body.width}-{container.width})/2)",
        },
        {
            "id": "uikit-genericpage-video-16-9",
            "selector": ".ui-genericpage .video.option-16-9",
            "max-width": "{parent.width}",
            "aspect-ratio": "16/9",
        }
    ]);

});