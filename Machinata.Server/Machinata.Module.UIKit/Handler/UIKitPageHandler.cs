using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Builder;

namespace Machinata.Module.UIKit.Handler {


    public abstract class UIKitPageHandler : Core.Handler.PageTemplateHandler {

        public override string PackageName {
            get {
                // We must use the UI-Kit package name so that all templates get merged to the same package...
                return "Machinata.Module.UIKit";
            }
        }

        public override string FallbackPackageName {
            get {
                // We let UIKit handlers use their own namespace for packages (ie templates) but the fallback should be 
                // to the UIKit since there are standard templates those handlers will want to use...
                return "Machinata.Module.UIKit";
            }
        }


    }
}