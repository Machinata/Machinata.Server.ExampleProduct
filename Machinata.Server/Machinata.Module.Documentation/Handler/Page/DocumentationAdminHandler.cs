using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Lifecycle;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Documentation.Extensions;
using Machinata.Core.Documentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Documentation.Handler {


    public class DocumentationAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("documentation");
        }

        #endregion


        #region Menu Item
               
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "document",
                Path = "/admin/documentation",
                Title = "{text.documentation}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/documentation")]
        public void Default() {
            IEnumerable<DocumentationPackage> packages = DocumentationPackage.GetDocumentations(this.DB);

            this.Template.InsertEntityList(
                variableName: "entities",
                entities: packages.AsQueryable(),
                form: null,
                link: "/admin/documentation/packages/package/{entity.public-id}");

            this.Template.InsertVariable("year", DateTime.Now.Year);

            // Navigation
            this.Navigation.Add("documentation", "{text.documentation}");
        }

        [RequestHandler("/admin/documentation/packages/package/{name}")]
        public void Package(string name) {
            IEnumerable<DocumentationPackage> docs = DocumentationPackage.GetDocumentations(this.DB);
            var document = docs.FirstOrDefault(d => d.Name == name);

            var namespaces = document.GetRootNamespaces();

            // Package
            this.Template.InsertVariable("package.credits", document.Credits?.Replace("{year}", DateTime.Now.Year.ToString()));
            this.Template.InsertVariables(
                variableName: "package",
                entity: document,
                form: new FormBuilder()
             );

            // Items
            this.Template.InsertEntityList(
              variableName: "items",
              entities: namespaces.AsQueryable(),
              form: new Core.Builder.FormBuilder(Forms.EMPTY).Include(nameof(DocumentationItem.Namespace)),
              link: $"/admin/documentation/packages/package/{name}" +"/namespace/{entity.namespace-url}");


            // Automatic redirect if only one namespace in package...
            if (namespaces.Count() == 1) {
                this.Context.Response.Redirect($"/admin/documentation/packages/package/{name}" + $"/namespace/{namespaces.FirstOrDefault().NamespaceURL}", false);
                return;
            }

            // Navigation
            this.Navigation.Add("documentation", "{text.documentation}");
            this.Navigation.Add("packages/package/" + name, name );
        }


        [RequestHandler("/admin/documentation/packages/package/{packageName}/namespace/{name}")]
        public void Namespace(string packageName, string name) {
            IEnumerable<DocumentationPackage> docs = DocumentationPackage.GetDocumentations(this.DB);
            var document = docs.FirstOrDefault(d => d.Name == packageName);

            var items = document.GetItemsForNamesapceURL(name);

            var namespaces = document.GetChildNamespaceURLs(name, true);

            // Package
            this.Template.InsertVariable("package.credits", document.Credits?.Replace("{year}", DateTime.Now.Year.ToString()));
            this.Template.InsertVariables(
                variableName: "package",
                entity: document,
                form: new FormBuilder()
             );

            // Namespaces Overview
            this.Template.InsertEntityList(
              variableName: "namespaces",
              entities: namespaces.AsQueryable(),
              form: new Core.Builder.FormBuilder(Forms.EMPTY).Include(nameof(DocumentationItem.Namespace)),
               link: $"/admin/documentation/packages/package/{packageName}" + "/namespace/{entity.namespace-url}");

            // Items Overview
            this.Template.InsertEntityList(
              variableName: "items",
              entities: items.AsQueryable(),
              form: null,
              link: $"/admin/documentation/packages/package/{packageName}" + "/item/{entity.full-name-url}");


            var insertCode = this.Params.Bool("show-source", false);
            this.Template.InsertDocumentationItemsTemplates("root",name, document, true);
            
            // Navigation
            this.Navigation.Add("documentation", "{text.documentation}");
            this.Navigation.Add("packages/package/" + packageName, packageName);


            this.Navigation.AddNamespaceNavigation("/admin/documentation/packages/package", packageName, name);

        }

       /*
        [RequestHandler("/admin/documentation/packages/package/{packageName}/item/{name}")]
        public void Item(string packageName, string name) {
            IEnumerable<DocumentationPackage> docs = DocumentationPackage.GetDocumentations(this.DB);
            var document = docs.FirstOrDefault(d => d.Name == packageName);

            var item = document.Items.FirstOrDefault(i => i.FullNameURL == name);
                    
            // Entity
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: item
             );

            // Summary html (markdown converted)
            this.Template.InsertVariableUnsafe("entity.summary-html", item.Summary.MarkdownToHtml());
            this.Template.InsertVariables("entity", item);


            // Navigation
            this.Navigation.Add("documentation", "{text.documentation}");
            this.Navigation.Add("packages/package/" + packageName, packageName);
            this.Navigation.AddNamespaceNavigation("/admin/documentation/packages/package", packageName, name, "namespace");
        }*/

     
       
    }
}
