using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Core.Model.CoreTypes;

namespace Machinata.Module.Courses.Model {

    [Serializable()]
    [ModelClass]
    [Table("CoursesCourses")]
    public partial class Course : ModelObject, IPublishedModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////



        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Course() {
            this.Classes = new List<Class>();
        }

        #endregion

        // TODO IMPLEMENT Formbuilder.Order
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        [Display]
        public string TitleForListing
        {
            get
            {
                return this.GetTitle(null);
            }
        }

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        [ContentType(ContentNode.NODE_TYPE_TITLE)]
        public ContentNode Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string ShortURL { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public decimal DurationDays { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ContentNode Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public Price Price { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public Price DiscountPrice { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public int? Level { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public bool Published { get; set; } = true;
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Archived { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<Curriculum> Curricula { get; set; }

        [Column]
        public ICollection<Curriculum> Extended { get; set; }
        
        [Column]
        public ICollection<Class> Classes { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public virtual CourseType Type { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder()]
        public string CourseTypeName
        {
            get
            {
                return this.Type?.Name;
            }
        }

        [NotMapped]
        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        public string DurationTitle {
            get {
                return Core.Util.Time.HumanReadableDurationForDays(this.DurationDays);
            }
        }

       

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelCreating]
        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
            
            modelBuilder.Entity<Curriculum>().HasMany(c => c.Courses)
                .WithMany(c => c.Curricula).Map(m=>m.ToTable("CoursesCurriculumCourses"));
            
            modelBuilder.Entity<Curriculum>().HasMany(c => c.Extended)
              .WithMany(c => c.Extended).Map(m=>m.ToTable("CoursesCurriculumExtendedCourses"));

            
        }
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Gets the translated text as a string.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public string GetTitle(string language) {
            this.Title?.IncludeContent();
            var ret = this.Title?.GetExactTranslationForLanguageOrDefault(language)?.Title;
            //if (ret == null) {
            //    return this.TitleOld;
            //}
            return ret;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {
            var language = Core.Config.LocalizationAdminLanguage;
            var card = new Core.Cards.CardBuilder(this)
                .Icon("mortar-board")
                .Title(this.GetTitle(language))
                .Wide();
            return card;
        }

       
        public override string ToString() {
            return $"{this.GetTitle(Core.Config.LocalizationDefaultLanguage)}: L:{this.Level}";
        }
    


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextCoursesExtensions {
        [ModelSet]
        public static DbSet<Course> Courses(this Core.Model.ModelContext context) {
            return context.Set<Course>();
        }
    }

    #endregion

}
