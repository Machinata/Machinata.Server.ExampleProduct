using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Module.Courses.Model {

    [Serializable()]
    [ModelClass]
    [Table("CoursesCourseTypes")]
    public partial class CourseType : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
      
        #region Constructors //////////////////////////////////////////////////////////////////////

        public CourseType() {
            
        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }
        
    
        
        #endregion
        
        #region Public Navigation Properties //////////////////////////////////////////////////////        
        
        [Column]
        public ICollection<Course> Course { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Derived Properties //////////////////////////////////////////////////

        public string GetShortUrl(bool toLower) {

            if (string.IsNullOrWhiteSpace(this.Name)) {
                throw new Exception($"Course Type {this.PublicId} has no name.");
            }
            return Core.Util.String.CreateShortURLForName(this.Name, toLower);

        }



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override string ToString() {
            return string.IsNullOrWhiteSpace(this.Name) ? base.ToString() : this.Name;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectWorkTypeExtensions {
        [ModelSet]
        public static DbSet<CourseType> CourseTypes(this Core.Model.ModelContext context) {
            return context.Set<CourseType>();
        }
    }

    #endregion

}
