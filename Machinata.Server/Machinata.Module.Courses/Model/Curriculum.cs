using System;

using System.Linq;
   
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;

using System.Collections.Generic;
using System.Data.Entity;
using Machinata.Core.Cards;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Courses.Model {

    [Serializable()]
    [ModelClass]
    [Table("CoursesCurriculums")]
    public partial class Curriculum : ModelObject, IPublishedModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Curriculum() {

        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string ShortURL { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public decimal DurationDays { get; set; }

        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ContentNode Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public Price Price { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public int? Level { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public bool Published { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Archived { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<Course> Courses { get; set; }

        [Column]
        public ICollection<Course> Extended { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public virtual CourseType Type { get; set; }

        #endregion


        #region Derived Properties ////////////////////////////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        public string DurationTitle {
            get {
                return Core.Util.Time.HumanReadableDurationForDays(this.DurationDays);
            }
        }

        #endregion//////////

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Gets the translated text as a string.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public string GetDescription(string language) {
            this.Description?.IncludeContent();
            var ret = this.Description?.TranslationForLanguage(language)?.Summary;
            return ret;
        }
        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        public override CardBuilder VisualCard() {
            var card = new Core.Cards.CardBuilder(this)
                .Icon("document-text")
                .Title(this.Title)
                .Wide();
            return card;
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }





    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectCostExtensions {
        [ModelSet]
        public static DbSet<Curriculum> Curricula(this Core.Model.ModelContext context) {
            return context.Set<Curriculum>();
        }
    }

    #endregion

}
