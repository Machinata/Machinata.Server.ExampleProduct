using System;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Machinata.Module.Courses.Model {

    [Serializable()]
    [ModelClass]
    [Table("CoursesClasses")]
    public partial class Class : ModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

       

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Class() {
            this.TimeRange = new DateRange();
        }

        #endregion
        // TODO IMPLEMENT Formbuilder.Order
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public string TitleForListing
        {
            get
            {
                return this.GetTitle(Core.Config.LocalizationDefaultLanguage);
            }
        }

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [ContentType(ContentNode.NODE_TYPE_TITLE)]
        [Column]
        public ContentNode Title { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string ClassID { get; set; }
                   
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public DateRange TimeRange { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public decimal DurationDays { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public int Capacity { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public int Booked { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool Published { get; set; } = false;

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool Timeline { get; set; } = false;

        /// <summary>
        /// KETS: Price can override the Courses price
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price Price { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        



        [Column]
        [Required]
        public Course Course { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        [DataType(DataType.Date)]
        public DateTime? Start {
            get {
                return this.TimeRange.Start;
            }
        }

        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        [DataType(DataType.Date)]
        public DateTime? End {
            get {
                return this.TimeRange.End;
            }
        }
        
        [FormBuilder]
        [NotMapped]
        public string TimeRangeTitle {
            get {
                if(this.TimeRange.Start?.Date == this.TimeRange.End?.Date && this.TimeRange.Start?.Date != null) {
                    // Same day, show start time
                    return Core.Util.Time.ToHumanReadableDateString(Core.Util.Time.ConvertToDefaultTimezone(this.TimeRange.Start.Value))
                        + ", " + Core.Util.Time.ConvertToDefaultTimezone(this.TimeRange.Start.Value).ToShortTimeString()
                        + "-" + Core.Util.Time.ConvertToDefaultTimezone(this.TimeRange.End.Value).ToShortTimeString();
                } else {
                    // Different days
                    return this.TimeRange.ToHumanReadableDateString();
                }
            }
        }

        [FormBuilder]
        [NotMapped]
        public string OneDayClass
        {
            get
            {
                if (this.TimeRange.Start?.Date == this.TimeRange.End?.Date && this.TimeRange.Start?.Date != null) {
                    // Same day
                    return "one-day";
                } else {
                    // Different days
                    return "multiple-days";
                }
            }
        }


        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string DurationTitle {
            get {
                return Core.Util.Time.HumanReadableDurationForDays(this.DurationDays);
            }
        }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string AvailableTitle {
            get {
                if (this.Available == 1)    return this.Available + " {text.available-spot}";
                else                        return this.Available + " {text.available-spots}";
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public int Available { get { return this.Capacity - this.Booked; } }


        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string CourseId
        {
            get
            {
                if (this.Context != null) {
                    this.Include(nameof(this.Course));
                }
                return this.Course?.PublicId;
            }
        }

        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public string CourseTitle
        {
            get
            {
                return this.Course?.GetTitle(Core.Config.LocalizationDefaultLanguage);
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Gets the translated text as a string.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public string GetTitle(string language) {
            this.Title?.IncludeContent();
            var ret = this.Title?.GetExactTranslationForLanguageOrDefault(language)?.Title;
            //if (ret == null) {
            //    return this.TitleOld;
            //}
            return ret;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
            var language = Core.Config.LocalizationAdminLanguage;
            this.Include(nameof(this.Course));
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.GetTitle(language))
                .Subtitle(this.Course.GetTitle(language))
                .Sub(this.TimeRange.ToString())
                .Icon("pencil")
                .Link("/admin/courses/course/"+ this.CourseId + "/class/" + this.PublicId )
                .Wide();
                
            return card;
        }

        public override string ToString() {
            return $"{this.Course?.GetTitle(null)}: {this.GetTitle(null)}";
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextClassesExtensions {

        [ModelSet]
        public static DbSet<Class> Classes(this Core.Model.ModelContext context) {
            return context.Set<Class>();
        }
                
       
    }

    #endregion

}
