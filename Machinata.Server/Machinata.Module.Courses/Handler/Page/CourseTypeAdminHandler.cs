
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Courses.Handler {


    public class CourseTypeAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("courses");
        }

        #endregion

        [RequestHandler("/admin/courses/types")]
        public void Default() {
            var entities = this.DB.CourseTypes().AsQueryable();
            entities = this.Template.Paginate(entities, this);

            this.Template.InsertEntityList(
                entities: entities,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/type/{entity.public-id}"
                );
            this.Navigation.Add("courses");
            this.Navigation.Add("types");
        }

    

        [RequestHandler("/admin/courses/types/create")]
        public void Create() {
            var entity = new CourseType();

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall : "/api/admin/courses/types/create",
                onSuccess: "/admin/courses/types/type/{course-type.public-id}"
                );

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("types");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/courses/types/type/{publicId}")]
        public void View(string publicId) {
            // Entity
            var entity = this.DB.CourseTypes().GetByPublicId(publicId);

            // Props
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "entity",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("types");
            this.Navigation.Add("type/" + entity.PublicId, entity.Name);

        }

      

        [RequestHandler("/admin/courses/types/type/{publicId}/edit")]
        public void Edit(string publicId) {
            // Menu items

            var entity = this.DB.CourseTypes()
                    .GetByPublicId(publicId);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/courses/type/{entity.PublicId}/edit",
                onSuccess: $"/admin/courses/types/type/{entity.PublicId}"
                );

            Navigation.Add("courses");
            Navigation.Add("types");
            Navigation.Add("type/" + entity.PublicId, entity.Name);
            Navigation.Add("edit");
        }
        
    }
}
