
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Finance.Handler {


    public class CurriculumAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("courses");
        }

        #endregion
        

        [RequestHandler("/admin/courses/curricula")]
        public void Default() {
            // Menu items?
          
            // Curricula
            var curricula = this.Template.Paginate(this.DB.Curricula(), this);
            this.Template.InsertEntityList(
                    variableName: "curricula",
                    entities: curricula,
                    link: "/admin/courses/curricula/curriculum/{entity.public-id}",
                    loadFirstLevelReferences: true);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("curricula");
        }

        [RequestHandler("/admin/courses/curricula/curriculum/{publicId}")]
        public void Curriculum(string publicId) {

            // Courses
            var curriculum = this.DB.Curricula()
                .Include(nameof(Courses.Model.Curriculum.Description))
                .Include(nameof(Courses.Model.Curriculum.Courses))
                .Include(nameof(Courses.Model.Curriculum.Extended))
                .Include(nameof(Courses.Model.Curriculum.Type))
                .GetByPublicId(publicId);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: curriculum,
                    form: new FormBuilder(Forms.Admin.VIEW)
                    );
            this.Template.InsertVariables("entity", curriculum);

            var allCourses = this.DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes) + "." + nameof(Courses.Model.Class.Title))
                .Include(nameof(Courses.Model.Course.Description))
                .Include(nameof(Courses.Model.Course.Title))
                .OrderBy(c => c.Level);

            // Courses
            this.Template.InsertSelectionList(
                     variableName: "selection-list",
                     entities: allCourses,
                     selectedEntities: curriculum.Courses,
                     selectionAPICall: "/api/admin/courses/curriculum/" + publicId + "/toggle-course/{entity.public-id}",
                     loadFirstLevelReferences: true);

            // Extended Courses
            this.Template.InsertSelectionList(
                     variableName: "extended-list",
                     entities: allCourses,
                     selectedEntities: curriculum.Extended,
                     selectionAPICall: "/api/admin/courses/curriculum/" + publicId + "/toggle-extended/{entity.public-id}",
                     loadFirstLevelReferences: true);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("curricula");
            this.Navigation.Add("curriculum/" + publicId, "{text.curriculum}" + ": " + curriculum.Title);
        }

        [RequestHandler("/admin/courses/curricula/create")]
        public void Create() {
            this.RequireWriteARN();

            // Course
            var course = new Curriculum();

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: course,
                  apiCall: "/api/admin/courses/curricula/create",
                  onSuccess: "{page.navigation.prev-path}"
                  );

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("curricula");
            this.Navigation.Add("create");

        }

        [RequestHandler("/admin/courses/curricula/curriculum/{publicId}/edit")]
        public void CurriculumEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Curricula().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/courses/curriculum/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("curricula");
            this.Navigation.Add("curriculum/" + entity.PublicId, "{text.curriculum}" + ": " + entity.Title);
            this.Navigation.Add("edit");

        }
    }
}
