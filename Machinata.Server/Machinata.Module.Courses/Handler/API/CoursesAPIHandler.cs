
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Admin.Handler {


    public class CoursesAPIHandler : CRUDAdminAPIHandler<Courses.Model.Course> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        [RequestHandler("/api/admin/courses/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/courses/course/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/courses/course/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/courses/course/{publicId}/add-class")]
        public void AddClass(string publicId) {

            var course = this.DB.Courses()
                .Include(nameof(Course.Title))
                .Include(nameof(Course.Classes) + "." + nameof(Class.Title))
                .GetByPublicId(publicId);

            var title = this.Params.String("title");

            var entity = new Class();
            entity.Price = new Price();
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.Capacity = Courses.Config.CoursesClassCapacityDefault;

            // Add to course
            course.Classes.Add(entity);

            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("create-success", new { Class = new { entity.PublicId } });
        }

        protected override void CreatePopulate(Course entity) {
            if (string.IsNullOrWhiteSpace(entity.ShortURL)) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.GetTitle(Core.Config.LocalizationDefaultLanguage));
            }
            base.CreatePopulate(entity);
        }






    }
}
