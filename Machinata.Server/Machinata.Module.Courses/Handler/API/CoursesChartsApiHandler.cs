using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Courses.Model;
using System.Collections.Generic;

namespace Machinata.Module.Courses.Handler {

    public class ProjectsChartsAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        private TimelineChartDataTrack _createClassTrack(TimelineChartData chart, Courses.Model.Class classObject) {
            var language = Core.Config.LocalizationAdminLanguage;

            // Create track
            var track = new TimelineChartDataTrack();

            track.Title = classObject.Course.GetTitle(language) + " " + classObject.GetTitle(language);

            track.Name = track.Title;
            // if (classObject.Course.Type.TimeRangeType == CourseType.CourseTypeTimeRangeType.MultipleDays) {
            if (false) {
                track.Start = classObject.TimeRange.Start.Value.ToDefaultTimezone().Date.ToUniversalTime();
                track.End = classObject.TimeRange.End.Value.ToDefaultTimezone().Date.ToUniversalTime();
            } else {
                track.Start = classObject.TimeRange.Start.Value;
                track.End = classObject.TimeRange.End.Value;
            }
         
            track.Link = $"/admin/courses/course/{classObject.CourseId}/class/{classObject.PublicId}";
            track.Id = classObject.PublicId;
            track.Level = classObject.Course.Level.HasValue ? classObject.Course.Level.Value : 0;
            track.Color = ChartColors.D3_CATEGORY_10[classObject.Course.Id % ChartColors.D3_CATEGORY_10.Length];

            // Add events
            var events = getEvents(classObject);

            foreach (var projectEvent in events) {
                var evt = new TimelineChartDataEvent();
                evt.Title = projectEvent.Title;
                evt.Date = projectEvent.Date;
                //evt.Link = projectEvent.URL;
                track.Events.Add(evt);
            }
            chart.Tracks.Add(track);

            return track;
        }

        private IEnumerable<ClassEvent> getEvents(Class classObject) {
            int? day = null;
            var title = classObject.GetTitle(null);
            var result = new List<ClassEvent>();
            if (title.Contains("Montag")) {
                day = 1;
            } else if (title.Contains("Dienstag")) {
                day = 2;
            } else if (title.Contains("Mittwoch")) {
                day = 3;
            } else if (title.Contains("Donnerstag")) {
                day = 4;
            } else if (title.Contains("Freitag")) {
                day = 5;
            } else if (title.Contains("Samstag")) {
                day = 6;
            } else if (title.Contains("Sonntag")) {
                day = 7;
            }
            if (day.HasValue == false) {

                //if (title.Contains("Mo-Fr")) {
                //    DateTime d = classObject.Start.Value.Date.AddHours(24); // utc fake
                //    int offset = 0;
                //    while (d <= classObject.End.Value.AddDays(1)) {
                //        result.Add(new ClassEvent() { Title = (d.DayOfWeek).ToString().Substring(0, 3), Date = d });

                //        if (offset != 5) {
                //            d = d.AddDays(1);
                //        } else {
                //            d = d.AddDays(3);
                //        }
                //        offset++;
                //    }
                //}

                return result;
            }
            DateTime date = classObject.Start.Value.Date;
            while (date <= classObject.End.Value) {
                result.Add(new ClassEvent() { Title = ((DayOfWeek)(day % 7)).ToString().Substring(0,3), Date = date });
                date = date.AddDays(7);
            }


            return result;


        }

        public class ClassEvent {
            public string Title { get; set; }
            public DateTime Date { get; set; }
        }

        [RequestHandler("/api/admin/courses/chart/timeline/classes-by-course")]
        public void Projects() {

            // Init
            var data = new TimelineChartData();
          
            var coursesOrg = this.DB.Courses()
                .Include(nameof(Course.Classes) + "." + nameof(Class.Title))
                .Include(nameof(Course.Title))
                .Include(nameof(Course.Type))
                .ToList();

            // fake sort
            foreach(var course in coursesOrg) {
                if (course.Level.HasValue == false) {
                    course.Level = 5;
                }
            }
            var courses = coursesOrg.OrderBy(p => p.Level);

            // Execute query and compile
            var coursesList = courses.ToList();
            foreach (var course in coursesList) {
                foreach (var c in course.Classes.Where(c=>c.Published).OrderBy(c=>c.Start)) {
                    _createClassTrack(data, c);
                }
            }


            // Auto-prune data
            data.Prune();
            data.ConvertToDefaultTimezone();

            this.SendAPIMessage("chart-data", data);
        }



    }
}