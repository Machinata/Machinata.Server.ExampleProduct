
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Admin.Handler {


    public class CurriculaAPIHandler : CRUDAdminAPIHandler<Courses.Model.Curriculum> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        [RequestHandler("/api/admin/courses/curricula/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/courses/curriculum/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/courses/curriculum/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/courses/curriculum/{publicId}/toggle-course/{toggleId}")]
        public void ToggleCourse(string publicId, string toggleId) {
            this.CRUDToggleSelection<Courses.Model.Course>(publicId, toggleId, nameof(Courses.Model.Curriculum.Courses));
        }


        [RequestHandler("/api/admin/courses/curriculum/{publicId}/toggle-extended/{toggleId}")]
        public void ToggleExtendedCourse(string publicId, string toggleId) {
            this.CRUDToggleSelection<Courses.Model.Course>(publicId, toggleId, nameof(Courses.Model.Curriculum.Extended));
        }

        protected override void CreatePopulate(Curriculum entity) {
            if (string.IsNullOrWhiteSpace(entity.ShortURL)) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Title);
            }
            base.CreatePopulate(entity);
        }


    }
}
