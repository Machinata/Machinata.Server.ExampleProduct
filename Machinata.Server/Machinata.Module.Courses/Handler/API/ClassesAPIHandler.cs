
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Admin.Handler {


    public class ClassesAPIHandler : CRUDAdminAPIHandler<Courses.Model.Class> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        [RequestHandler("/api/admin/courses/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/courses/class/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/courses/class/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        protected override void EditPopulate(Class entity) {
            entity.Include(nameof(Class.Course)); // hack for required field
            base.EditPopulate(entity);
        }

    }
}
