using Machinata.Core.StructuredData;
using Machinata.Core.Templates;
using Machinata.Module.Courses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Courses.Extensions {
    public static class TemplateExtensions {

        /// <summary>
        /// Inserts the course structured data based on a Course
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="course">The course.</param>
        public static void InsertStructuredData(this PageTemplate template, Course course) {
            if (course.Context != null && course.Description == null) {
                course.Include(nameof(course.Description));
            }
            var courseSD = new CourseStructuredData();
            courseSD.Name = course.GetTitle(template.Language);
            var description = "";
            // Description Limit is 60 chars
            if (string.IsNullOrEmpty(course.Description?.ToString()) == false) {
                description = Core.Util.String.CreateSummarizedText(course.Description.ToString(), 60);
            }

            courseSD.Description = description;
            courseSD.Provider = OrganizationStructuredData.LoadDefault(true, template.Handler.DB);
            template.StructuredData.Add(courseSD);
        }

        /// <summary>
        /// Inserts the course structured data based on a Curriculum
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="curriculum">The curriculum.</param>
        public static void InsertStructuredData(this PageTemplate template, Curriculum curriculum) {
            if (curriculum.Context != null && curriculum.Description == null) {
                curriculum.Include(nameof(curriculum.Description));
            }
            var courseSD = new CourseStructuredData();
            courseSD.Name = curriculum.Type + " " + curriculum.Title;
            var description = "";
            // Description display limit on google ist 60 chars
            if (curriculum.Description != null) {
                description = curriculum.GetDescription(template.Language);
            }

            courseSD.Description = description;
            courseSD.Provider = OrganizationStructuredData.LoadDefault(true, template.Handler.DB);
            template.StructuredData.Add(courseSD);
        }

        /// <summary>
        /// Insert multiple course structured data based on Curriculums
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="curriculums">The curriculums.</param>
        public static void InsertStructuredData(this PageTemplate template, IEnumerable<Curriculum> curriculums) {
            try { 
            foreach (var curriculum in curriculums) {
                template.InsertStructuredData(curriculum);
                }
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Events Structured Data Error", "Machinata.Module.Courses", null, template?.Handler, e);
            }
        }


    }
}
