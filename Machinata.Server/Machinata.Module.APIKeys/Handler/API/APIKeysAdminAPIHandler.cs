
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler {


    public class APIKeysAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<APIKey> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("api");
        }

        #endregion


        [RequestHandler("/api/admin/api/keys/create")]
        public void Create() {
            var entity = this.CRUDCreate();
            entity.Enabled = true;
            this.DB.SaveChanges();
        }

        [RequestHandler("/api/admin/api/keys/key/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/api/keys/key/{publicId}/delete")]
        public void Delete(string publicId) {
            this.CRUDDelete(publicId);
        }

    }
}
