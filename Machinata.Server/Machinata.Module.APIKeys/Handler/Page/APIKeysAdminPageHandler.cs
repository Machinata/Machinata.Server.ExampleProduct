using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using System.Collections.Generic;
using Machinata.Core.Builder;
using Machinata.Core.Util;

namespace Machinata.Module.Admin.Handler {


    public class APIKeysAdminPageHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("api");
        }

        #endregion

        #region Menu 
        
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "key",
                Path = "/admin/api",
                Title = "{text.api}",
                Sort = "500"
            });
        }

        #endregion


        public override void DefaultNavigation() {
            base.DefaultNavigation();
            
            // Navigation
            this.Navigation.Add("api", "{text.api}");
        }

        [RequestHandler("/admin/api")]
        public void Default() {
            // API keys
            var apiKeys = this.DB.APIKeys()
                .Include(nameof(APIKey.User))
                .Active()
                .OrderByDescending(e => e.Id);
            this.Template.InsertEntityList(
                variableName: "api-keys", 
                entities: apiKeys, 
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/api/keys/key/{entity.public-id}"
            );
            // API Users
            var apiUsers = apiKeys
                .Select(e => e.User)
                .Distinct()
                .OrderByDescending(e => e.Name);
            this.Template.InsertEntityList(
                variableName: "api-users", 
                entities: apiUsers,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/users/user/{entity.public-id}"
            );
        }


        #region Keys

        [RequestHandler("/admin/api/keys")]
        public void KeysDefault() {
            // API keys
            var entities = this.DB.APIKeys().Include(nameof(APIKey.User)).OrderByDescending(e => e.Id);
            this.Template.InsertEntityList(
                variableName: "api-keys", 
                entities: entities, 
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/api/keys/key/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("keys", "{text.keys}");
        }
        
        [RequestHandler("/admin/api/keys/create")]
        public void KeysCreate() {
            // Form
            this.Template.InsertForm(
                variableName: "form",
                entity: new APIKey(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/api/keys/create",
                onSuccess: "/admin/api/keys/key/{api-key.public-id}/edit"
            );
            // Navigation
            this.Navigation.Add("keys", "{text.keys}");
            this.Navigation.Add("create", "{text.create}");
        }
        
        [RequestHandler("/admin/api/keys/key/{publicId}")]
        public void KeysKeyView(string publicId) {
            // Entity
            var entity = this.DB.APIKeys()
                .Include(nameof(APIKey.User))
                .GetByPublicId(publicId);
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW)
            );
            // Navigation
            this.Navigation.Add("keys", "{text.keys}");
            this.Navigation.Add("key/"+entity.PublicId, entity.Title);
        }
        
        [RequestHandler("/admin/api/keys/key/{publicId}/edit")]
        public void KeysKeyEdit(string publicId) {
            // Entity
            var entity = this.DB.APIKeys()
                .Include(nameof(APIKey.User))
                .GetByPublicId(publicId);
            this.Template.InsertForm(
                variableName: "form",
                apiCall: $"/api/admin/api/keys/key/{publicId}/edit",
                entity: entity,
                onSuccess: $"/admin/api/keys/key/{publicId}",
                form: new FormBuilder(Forms.Admin.EDIT)
             );
            // Navigation
            this.Navigation.Add("keys", "{text.keys}");
            this.Navigation.Add("key/"+entity.PublicId, entity.Title);
            this.Navigation.Add("edit", "{text.edit}");
        }

        #endregion

    }
}
