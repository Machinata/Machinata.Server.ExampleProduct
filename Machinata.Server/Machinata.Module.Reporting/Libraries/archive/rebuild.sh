#!/bin/sh


#echo ""
#echo "============================================================="
#echo "Pre-Building vega..."
#echo "============================================================="
#read -p "Press enter to continue"

#cd vega
#yarn
#yarn build
#cd ..



echo ""
echo "============================================================="
echo "Applying vega-patches/legend.js.patch to Vega..." 
echo "============================================================="
echo "Note: if the patch does not apply, it means it has already been patched"
read -p "Press enter to continue"

# Note: patches must have the diff line removed
# Note: patches must have the index line removed
# Note: patches must have relative file paths in the --- and +++ line
cd vega
echo "Applying legend.js.patch..."
git apply --verbose --no-index ../nerves/vega-patches/legend.js.patch

echo "Applying canvas-index.js.patch..."
git apply --verbose --no-index ../nerves/vega-patches/canvas-index.js.patch

echo "Applying canvas-package.json.patch..."
git apply --verbose --no-index ../nerves/vega-patches/canvas-package.json.patch

echo "Applying canvas-nodeCanvas.js.patch..."
git apply --verbose --no-index ../nerves/vega-patches/canvas-nodeCanvas.js.patch

cd ..
echo "Patch done"



echo ""
echo "============================================================="
echo "Building bmpi/d3-format..." 
echo "============================================================="
read -p "Press enter to continue"

cd bmpi/d3-format
yarn
yarn build
cd ..
cd ..



echo ""
echo "============================================================="
echo "Removing vega/node_modules/d3-format..."
echo "============================================================="
read -p "Press enter to continue"

rm -rf vega/node_modules/d3-format

echo ""
echo "============================================================="
echo "Installing bmpi/d3-format into vega/node_modules/"
echo "============================================================="
read -p "Press enter to continue"

cp -r bmpi/d3-format vega/node_modules



echo ""
echo "============================================================="
echo "Building vega..."
echo "============================================================="
read -p "Press enter to continue"

cd vega
yarn
yarn build
cd ..



sh util-copy-over.sh

echo ""
echo "============================================================="
echo "========================*** DONE *** ========================"
echo "============================================================="