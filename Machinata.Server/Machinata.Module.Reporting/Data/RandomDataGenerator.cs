﻿using Machinata.Core.Util;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Reporting.Model.Report;

namespace Machinata.Module.Reporting.Data {
    public class RandomDataGenerator {

        public static Report ChartTypes() {

            Report report = CreateReport("Chart Type Examples");

            {
                var section = CreateSectionNode("Line Columns");
                section.Children.Add(new LineColumnNode().LoadSampleData());
                report.AddToBody(section);
               
            }
            {
                var section = CreateSectionNode("Donut");
                section.Children.Add(new DonutNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Pie");
                section.Children.Add(new PieNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("HBar");
                section.Children.Add(new HBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("VBar");
                section.Children.Add(new VBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Gauges");
                section.Children.Add(new GaugeNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Symboled VBars");
                section.Children.Add(new SymboledVBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Offline Map");
                section.Children.Add(new OfflineMapNode().LoadSampleData());
                report.AddToBody(section);
            }

            return report;

        }

      

      

        private static SectionNode CreateSectionNode(string title) {
            var chapter = new SectionNode();
            chapter.MainTitle = new TranslatableString(title, "BR_PO_Title");
            chapter.CommentC = "CHAPTER";
            chapter.Comment1 = "This section node is ignored since the style is null and Machinata.Reporting.Node.SectionNode.defaults.ignoreIfStyleIsNull is not true";
            chapter.Theme = "blue";
            chapter.Children = new List<Node>();

            var infoNode = new InfoNode();
            infoNode.Message = $"This is the chapter " + title;
            chapter.Children.Add(infoNode);
            return chapter;
        }

     

     

      

       

    }
}
