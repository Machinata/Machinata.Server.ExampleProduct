﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class CategorySerie {

        public CategorySerie() {
            this.Facts = new List<CategoryFact>();
            this.Title = new TranslatableString(null);
        }

        [JsonProperty("facts")]
        public List<CategoryFact> Facts { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("colorShade")]
        public string ColorShade { get; set; }


        public static IEnumerable<CategoryFact> CreateRandomCategorieFacts(int bars, double minValue, double maxValue) {
            var random = new System.Random();
            for (int i = 1; i <= bars; i++) {
                var val = random.NextDouble() * (maxValue - minValue) + minValue;
                yield return new CategoryFact((decimal)val, val.ToString("P"), "Label " + i, "number", ".0%", "percent: 1");
            }
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieSymboledFacts(double minValue, double maxValue) {

            var facts = new List<CategoryFact>();

            {
                var min = Core.Util.Math.RandomDouble(minValue, maxValue);
                var minFact = new CategoryFact((decimal)min, min.ToString("P"), "Label min", "number", ".0%", "percent: 1");
                minFact.SetSymbolType(SymbolTypes.Min);
                facts.Add(minFact);
            }
            {
                var max = Core.Util.Math.RandomDouble(minValue, maxValue);
                var maxFact = new CategoryFact((decimal)max, max.ToString("P"), "Label max", "number", ".0%", "percent: 1");
                maxFact.SetSymbolType(SymbolTypes.Max);
                facts.Add(maxFact);
            }
            {
                var line = Core.Util.Math.RandomDouble(minValue, maxValue);
                var lineFact = new CategoryFact((decimal)line, line.ToString("P"), "Label line", "number", ".0%", "percent: 1");
                lineFact.SetSymbolType(SymbolTypes.Line);
                facts.Add(lineFact);
            }
            {
                var diamond = Core.Util.Math.RandomDouble(minValue, maxValue);
                var diamondFact = new CategoryFact((decimal)diamond, diamond.ToString("P"), "Label diamond", "number", ".0%", "percent: 1");
                diamondFact.SetSymbolType(SymbolTypes.Dot);
                facts.Add(diamondFact);
            }

            return facts;
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieFactsAddingToOne(int count) {
            var random = new System.Random();
            var total = 0.0;
            for (int i = 1; i <= count; i++) {
                var val = random.NextDouble() / count * 1.5;
                total += val;
                if (i == count && total < 1) {
                    val = 1 - total;
                }
                yield return new CategoryFact((decimal)val, val.ToString("P"), "Label " + i, "number", ".0%", "percent: 1");
            }
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieFactsAddingToOne(IEnumerable<string> categories) {
            var random = new System.Random();
            var total = 0.0;
            var count = categories.Count();
            int i = 0;
            foreach (var category in categories) {
                var val = random.NextDouble() / count * 1.5;
                total += val;
                if (i == count && total < 1) {
                    val = 1 - total;
                }
                i++;
                yield return new CategoryFact((decimal)val, val.ToString("P"), category + i, "number", ".0%", "percent: 1");
            }
        }

        /// <summary>
        /// Co
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IEnumerable<GeoFact> CreateRandomGeoFacts(int count) {
            for (int i = 0; i < count; i++ ) {
                var lat = Core.Util.Math.RandomDouble(-90, 90);
                var lon = Core.Util.Math.RandomDouble(-180, 180);
                yield return new GeoFact() {X = (decimal)lat, Y = (decimal)lon };
            }
        }

        public static IEnumerable<GeoFact> CreateAddressGeoFacts(int count) {

            var facts = new List<GeoFact>();

            facts.Add(new GeoFact() { Address = "Räffelstrasse, Zürich" , Link = new Link("https://nerves.ch", "Nerves GmnbH")});
            facts.Add(new GeoFact() { Address = "Brahmstrasse, Zürich", Link = new Link("https://google.ch") });
            facts.Add(new GeoFact() { Address = "Bundeshaus Bern", Link = new Link("https://admin.ch") });
            facts.Add(new GeoFact() { Address = "Bärengraben, Bern" });
            facts.Add(new GeoFact() { Address = "Herisau"});
            facts.Add(new GeoFact() { Address = "Kentishtown"});
            facts.Add(new GeoFact() { Address = "Sydney Australia"});

            return facts.Take(count);
        }



    }
}
