﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [TypeConverter(typeof(TranslatableStringConverter))]
    [JsonObject]
    public class TranslatableString {

        public TranslatableString(string resolved) {
            this.Resolved = resolved;
        }

        public TranslatableString(string resolved, string label) {
            this.Resolved = resolved;
            this.Label = label;
        }

        [JsonProperty("resolved")]
        public string Resolved { get; set; }

        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        public static implicit operator TranslatableString(string v) {
            return new TranslatableString(v);
        }

    }

    #region TranslatableStringConverter

    public class TranslatableStringConverter : TypeConverter {


        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            if (value is string) {
                return new TranslatableString((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }
        }

    }



    #endregion
}
