﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class LayoutNode : ChartNode {
      
        [JsonProperty("slotChildren")]
        public Dictionary<string, int> SlotChildren { get; set; }
    }
}
