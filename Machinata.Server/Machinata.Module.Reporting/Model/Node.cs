﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {


    /// <summary>
    /// Should this be ReportNode?
    /// 
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_nodes_.reportnode.html
    /// </summary>
    public abstract class Node {

        /*
        "enabled": true,
    "screen": true,
    "chrome": "solid",
    "mainTitle": { "resolved": "Line and Column Example" },
    "subTitle": { "resolved": "only lines (sparse, forced y-axis values)" },
        */

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("supportsToolbar", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SupportsToolbar;

        [JsonProperty("chrome", NullValueHandling = NullValueHandling.Ignore)]
        public string Chrome;

        [JsonProperty("mainTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString MainTitle { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString SubTitle { get; set; }

        [JsonProperty("nodeType")]
        public string NodeType { get { return this.GetType().Name; } }

        [JsonProperty("children")]
        public List<Node> Children { get; set; }

        [JsonProperty("style")]
        public string Style { get; set; }


        public virtual Node LoadSampleData() {
            throw new NotImplementedException($"'{this.GetType()}' does not implement LoadSampleData() yet");
        }

        public virtual JObject GetJObject() {
            return JObject.FromObject(this);
        }
    }
}
