﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class Serie {

        public Serie() {
            this.Facts = new List<Fact>();
            this.Title = new TranslatableString(null);
        }

    

        [JsonProperty("facts")]
        public List<Fact> Facts { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("chartType")]
        public string ChartType { get; set; }

        [JsonProperty("colorShade")]
        public string ColorShade { get; set; }

        public static List<Fact> GenerateFacts(int count, long start, long end, double min, double max, Random random = null) {
            var facts = new List<Fact>();
            if (random == null) {
                random = new System.Random();
            }
            var timeFrame = (end - start) / count;
            for (int i = 1; i <= count; i++) {
                var value = random.NextDouble() * (max - min) + min;
                var date = start + (i - 1) * timeFrame;
                var fact = new Fact();
                fact.X = date;
                fact.Y = (decimal?)value;
                facts.Add(fact);
            }
            return facts;
        }


        public static List<Fact> GenerateAccumulatedFacts(int count, long start, long end, double min, double max, Random random = null) {
            var facts = new List<Fact>();
            if (random == null) {
                random = new System.Random();
            }
            var timeFrame = (end - start) / count;
            var currentValue = min;
            for (int i = 1; i <= count; i++) {
                var value = (random.NextDouble() * (max - min) + min) / count * 2.5;
                var date = start + (i - 1) * timeFrame;
                var fact = new Fact();
                fact.X = date;

                currentValue += value;
                fact.Y = (decimal?)currentValue;
                facts.Add(fact);
            }
            return facts;
        }




    }
}
