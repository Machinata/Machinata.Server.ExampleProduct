﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    ///https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.offlinemapnode.html
    /// </summary>
    public class OfflineMapNode : ChartNode {

        [JsonProperty("projection")]
        public MapProjection Projection { get; set; }

        [JsonProperty("facts")]
        public List<CategoryFact> Facts { get; set; }


        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public OfflineMapNode LoadSampleData(int numberSegements = 7) {
            this.MainTitle = "Offline Map Node  (random)";
            this.SubTitle = "";
            var random = new Random();
            IEnumerable<string> categories = new List<string>() { "CAD", "USD", "RUB", "CHF", "EUR", "GBP", "JPY", "AUD", "ZAR", "BRL", "INR", "BRL", "CNY", "Other" };
            this.Facts = CategorySerie.CreateRandomCategorieFactsAddingToOne(categories).ToList();
            return this;
        }

        public class MapProjection {

            [JsonProperty("center")]
            public decimal[] Center { get; set; } = new decimal[] { 0, 40 };

            [JsonProperty("scale")]
            //scale: string | number = "auto"
            public decimal Scale { get; set; }

            [JsonProperty("zoom")]
            //scale: string | number = "auto"
            public decimal Zoom { get; set; } = 1;
        }


    }

  
}
