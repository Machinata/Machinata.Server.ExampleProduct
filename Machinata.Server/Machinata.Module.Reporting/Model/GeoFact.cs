﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class GeoFact : Fact {

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("link")]
        public Link Link { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; } 

        [JsonProperty("lat-lng")]
        public string LatLong {
            get {
                if (this.X != null && this.Y != null) {
                    return this.X + "," + this.Y;
                }
                return null;
            }
        }
    }
}
