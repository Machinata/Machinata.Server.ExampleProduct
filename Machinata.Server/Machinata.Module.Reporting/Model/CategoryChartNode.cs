﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.categorychartnode.html
    /// </summary>
    public abstract class CategoryChartNode : ChartNode {
     
        [JsonProperty("series")]
        public List<CategorySerie> Series { get; set; }
    }
}
