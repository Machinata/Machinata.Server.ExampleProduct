﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class VBarNode : CategoryChartNode {

        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public VBarNode LoadSampleData(int bars = 15, double minValue = -0.04, double maxValue = 0.1) {
          
            this.MainTitle = "V-Bar Test  (random)";
            this.SubTitle = "";
            this.Series = new List<CategorySerie>();
            var serie = new CategorySerie();

            serie.Id = "serie1";
            serie.Title = "Title";
            serie.ColorShade = "bright";
            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFacts(bars, minValue, maxValue));
            this.Series.Add(serie);
            return this;
        }
    }
}
