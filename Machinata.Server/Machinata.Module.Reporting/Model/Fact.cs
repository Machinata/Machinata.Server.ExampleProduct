﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class Fact {

        [JsonProperty("x")]
        public decimal? X { get; set; }

        [JsonProperty("y")]
        public decimal? Y { get; set; }

        [JsonProperty("z", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Z { get; set; }

        /*
                            "x": 1547251200000
        "y": 0.0030853684588241226,
        "z": 0.0030853684588241226,
        */

    }
}
