﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class DonutNode : CategoryChartNode {

        [JsonProperty("legendLabelsShowValue")]
        public bool LegendLabelsShowValue { get; set; }

        [JsonProperty("legendLabelsShowCategory")]
        public bool LegendLabelsShowCategory { get; set; }

        [JsonProperty("sliceLabelsShowValue")]
        public bool SliceLabelsShowValue { get; set; }

        [JsonProperty("sliceLabelsShowCategory")]
        public bool SliceLabelsShowCategory { get; set; }

        [JsonProperty("legendColumns")]
        public int LegendColumns { get; internal set; }


        public override Node LoadSampleData() {
            this.LegendLabelsShowValue = true;
            this.LegendLabelsShowCategory = true;
            this.SliceLabelsShowValue = false;
            this.SliceLabelsShowCategory = true;
            this.LegendColumns = 2;
            this.MainTitle = "Donut test";
            this.SubTitle = "Show value in legend, show category in slices";
            this.Series = new List<CategorySerie>();

            var serie = new CategorySerie();
            serie.Id = "s0";
            serie.Title = "Series title";

            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFactsAddingToOne(8));
            this.Series.Add(serie);
            return this;





        }
    }
}
    