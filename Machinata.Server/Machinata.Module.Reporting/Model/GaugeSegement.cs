﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class GaugeSegement {

        public GaugeSegement() {
            this.Title = new TranslatableString(null);
        }

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("tooltip")]
        public TranslatableString Tooltip { get; set; }
    }
}
