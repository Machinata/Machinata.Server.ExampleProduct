﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class Axis {

        [JsonProperty("orient", NullValueHandling = NullValueHandling.Ignore)]
        public string Orient { get; set; }

        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("formatType", NullValueHandling = NullValueHandling.Ignore)]
        public string FormatType { get; set; }

        [JsonProperty("values", NullValueHandling = NullValueHandling.Ignore)]
        public List<decimal> Values { get; set; }


        /*
        "orient": "left",
                "format": ".2%",
                "formatType": "number"
        */

    }
}
