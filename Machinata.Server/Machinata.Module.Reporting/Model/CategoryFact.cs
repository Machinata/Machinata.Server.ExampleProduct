﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class CategoryFact {
        public CategoryFact(decimal val, string resolved, string category, string formatType = null, string format = null, string formattingHint = null) {
            this.Value = val;
            this.Resolved = resolved;
            this.Category = category;
            this.FormatType = formatType;
            this.Format = format;
            this.FormattingHint = formattingHint;

        }

        [JsonProperty("val")]
        public decimal? Value { get; set; }

        [JsonProperty("resolved")]
        public string Resolved { get; set; }

        [JsonProperty("category")]
        public TranslatableString Category { get; set; }

        [JsonProperty("formatType")]
        public string FormatType { get;  set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("formattingHint")]
        public string FormattingHint { get; set; }

        [JsonProperty("symbolType")]
        public string SymbolType {
            get {
                return _symbolType?.ToString()?.ToLowerInvariant();
            }
        }



        /// <summary>
        /// For newtonsoft serizalization reasons
        /// </summary>
        /// <param name="type"></param>
        public void SetSymbolType (SymbolTypes type) {
            _symbolType = type;
        }

        [JsonIgnore]
        private SymbolTypes? _symbolType;
    }

    // Where should this actually 
    public enum SymbolTypes {
       
        Dot,
      
        Line,
        
        Max,
       
        Min
    }
}
