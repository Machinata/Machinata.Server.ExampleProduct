﻿using Machinata.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class Report {

        [JsonProperty("reportId")]
        public string ReportId { get; set; }

        [JsonProperty("reportTitle")]
        public ReportTitleClass ReportTitle { get; set; }

        [JsonProperty("body")]
        public Node Body { get; set; }

        /// <summary>
        /// Creates a new report with title and body
        /// </summary>
        /// <returns></returns>
        public static Report CreateReport(string title) {
            var report = new Report();
            report.ReportId = "rep-20190430-PO_001-1";
            report.ReportTitle = new ReportTitleClass();
            report.ReportTitle.Translations["*"] = title;
            report.Body = new NoOpNode();
            report.Body.Children = new List<Node>();
            return report;
        }


        public void AddToBody(Node node) {
            this.Body.Children.Add(node);
        }

    }

    public class ReportTitleClass {
        [JsonProperty("translations")]
        public Dictionary<string, string> Translations = new Dictionary<string, string>();
    }

}
