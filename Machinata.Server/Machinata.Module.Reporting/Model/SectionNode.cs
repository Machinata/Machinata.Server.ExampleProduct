﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class SectionNode : Node {

        [JsonProperty("_commentC")]
        public string CommentC { get; set; }
        
        [JsonProperty("_comment1")]
        public string Comment1 { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; } // move up?
        public string Note { get; internal set; }
    }
}
