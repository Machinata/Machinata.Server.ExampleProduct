﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.gaugenode.html
    /// </summary>
    public class GaugeNode : ChartNode {

        [JsonProperty("segments")]
        public List<GaugeSegement> Segments { get; set; }

        [JsonProperty("status")]
        public GaugeStatusClass Status { get; set; }


        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public GaugeNode LoadSampleData(int numberSegements = 7) {
            this.MainTitle = "Gauge Node  (random)";
            this.SubTitle = "";
            var random = new Random();
            var value = random.Next(0, numberSegements);
            this.Status = new GaugeStatusClass() { Title = "Segmented Gauge Test", Value = value };
            this.Segments = GenerateSegements(numberSegements, value);
            return this;
        }


        private static List<GaugeSegement> GenerateSegements(int numberOfSegements, int active) {
            var segs = new List<GaugeSegement>();

            for (int i = 1; i <= numberOfSegements; i++) {
                var seg = new GaugeSegement();
                seg.Active = i <= active;
                seg.Title = i.ToString();
                seg.Tooltip = $"{i} / {numberOfSegements}";
                segs.Add(seg);
            }

            return segs;
        }





    }

    public class GaugeStatusClass {

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("value")]
        public decimal Value { get; set; }
    }
}
