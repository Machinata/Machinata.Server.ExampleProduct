﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class SerieGroup {

        public SerieGroup() {
            this.xAxis = new Axis();
            this.yAxis = new Axis();
            this.Series = new List<Serie>();
        }

        /*
        "id": "lines",
        "xAxis": {
                "format": "%d.%m.%y",
                "formatType": "time"
            },
            "yAxis": {
                "orient": "left",
                "format": ".2%",
                "formatType": "number"
            },
        */

        public Axis xAxis { get; set; }
        public Axis yAxis { get; set; }

        [JsonProperty("series")]
        public List<Serie> Series { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
