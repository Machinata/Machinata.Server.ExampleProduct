﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public abstract class GraphNode : ChartNode {

        [JsonProperty("seriesGroups")]
        public List<SerieGroup> SeriesGroups { get; set; }

    }
}
