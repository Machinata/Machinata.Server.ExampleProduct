﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class PieNode : DonutNode {

        public override Node LoadSampleData() {
            this.LegendLabelsShowValue = true;
            this.LegendLabelsShowCategory = true;
            this.SliceLabelsShowValue = false;
            this.SliceLabelsShowCategory = true;
            this.LegendColumns = 2;
            this.MainTitle = "Pie test";
            this.SubTitle = "Show value in legend, show category in slices";

            this.Series = new List<CategorySerie>();

            var serie = new CategorySerie();
            serie.Id = "s0";
            serie.Title = "Series title";
            serie.Facts = CategorySerie.CreateRandomCategorieFactsAddingToOne(5).ToList();


            this.Series.Add(serie);
            return this;
        }
    }

}
