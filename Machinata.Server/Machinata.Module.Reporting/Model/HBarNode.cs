﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class HBarNode : CategoryChartNode {

        public override Node LoadSampleData() {
            return LoadSampleData();
        }
        public HBarNode LoadSampleData(int bars = 5, double minValue = -5, double maxValue = 5) {

            this.MainTitle = "H-Bar Test  (random)";
            this.SubTitle = "";
            this.Series = new List<CategorySerie>();
            var serie = new CategorySerie();
            serie.Id = "serie1";
            serie.Title = "Portfolio";
            serie.ColorShade = "bright";
            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFacts(bars, minValue, maxValue));
            this.Series.Add(serie);
            return this;
        }

    }
}

