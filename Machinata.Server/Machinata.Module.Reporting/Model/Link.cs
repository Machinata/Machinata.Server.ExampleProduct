﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {




    public class Link {

        public Link(string url, string tooltip = null) {
            this.Tooltip = tooltip;
            this.URL = url;
        }

        [JsonProperty("tooltip")]
        public TranslatableString Tooltip { get; set; }

        [JsonProperty("chapterId")]
        public string ChapterId { get; set; }
        [JsonProperty("nodeId")]
        public string NodeId { get; set; }
        [JsonProperty("target")]
        public string Target { get; set; }
        [JsonProperty("url")]
        public string URL { get; set; }



    }


}
