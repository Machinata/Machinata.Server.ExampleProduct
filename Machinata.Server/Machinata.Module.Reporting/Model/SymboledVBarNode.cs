﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.symboledvbarnode.html
    /// </summary>
    public class SymboledVBarNode : CategoryChartNode {

        public override Node LoadSampleData() {
       

       
           
            this.MainTitle = "Symbols Test";
            this.SubTitle = "";
            this.Series = new List<CategorySerie>();


            {
                var serie = new CategorySerie();
                serie.Id = "serie1";
                serie.Title = "Title";
                serie.ColorShade = "bright";
                serie.Facts.AddRange(CategorySerie.CreateRandomCategorieSymboledFacts(0, 0.01));
                this.Series.Add(serie);
            }

            {
                var serie = new CategorySerie();
                serie.Id = "serie2";
                serie.Title = "Title2";
                serie.ColorShade = "bright";
                serie.Facts.AddRange(CategorySerie.CreateRandomCategorieSymboledFacts(0, 0.01));
                this.Series.Add(serie);
            }


            {
                var serie = new CategorySerie();
                serie.Id = "serie3";
                serie.Title = "Title3";
                serie.ColorShade = "bright";
                serie.Facts.AddRange(CategorySerie.CreateRandomCategorieSymboledFacts(0, 0.01));
                this.Series.Add(serie);
            }



            return this;
        }



    }
}
