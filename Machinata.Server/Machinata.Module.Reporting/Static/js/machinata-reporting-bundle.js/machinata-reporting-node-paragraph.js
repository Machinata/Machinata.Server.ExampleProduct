


/// <summary>
/// Basic paragraph node for displaying a block of text.
/// The node style will drive the look the paragraph.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ParagraphNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults = {};

/// <summary>
/// By default ParagraphNode have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults.supportsDashboard = false;

/// <summary>
/// The main body resolvable text to display.
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults.content = null;

/// <summary>
/// The lead resolvable text to display ontop of the main body.
/// Typically, the design will place this much larger than main text body.
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults.lead = null;

/// <summary>
/// If true, adds a shortcut tool for copying the text to clipboard.
/// </summary>
Machinata.Reporting.Node.ParagraphNode.defaults.addCopyToClipboardTool = false;

Machinata.Reporting.Node.ParagraphNode.init = function (instance, config, json, nodeElem) {

    // Init texts
    var leadTextType = "h3";
    var contentTextType = "p";
    //if (json.style == "overviewParagraphStyle") {
    //    leadTextType = "h2";
    //    contentTextType = "h3";
    //}
    var leadText = Machinata.Reporting.Text.resolve(instance,json["lead"]);
    var contentText = Machinata.Reporting.Text.resolve(instance,json["content"]);

    // Create lead element
    var containerElem = $("<div class='body node-bg'></div>");
    if (leadText != null) {
        var elem = $("<" + leadTextType + "></" + leadTextType + ">");
        elem.addClass("lead");
        elem.text(leadText);
        containerElem.append(elem);
    }

    // Create content element
    if (contentText != null) {
        var paragraphs = contentText.split("\n");
        $.each(paragraphs, function (index, p) {
            var elem = $("<" + contentTextType + "></" + contentTextType + ">");
            elem.addClass("content");
            elem.text(p);
            containerElem.append(elem);
        });
    }

    nodeElem.append(containerElem);

    // Tools
    if (json.addCopyToClipboardTool == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.copy-to-clipboard"), "copy", function () {
            Machinata.UI.copyElementToClipboard(containerElem);
            Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied.text')).show();
        });
    }

};
Machinata.Reporting.Node.ParagraphNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








