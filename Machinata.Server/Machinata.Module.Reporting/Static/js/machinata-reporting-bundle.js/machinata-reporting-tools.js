/// <summary>
/// Collection of tools that supplement a report.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Tools = {};

/// <summary>
/// Converts the given value to pixels using ```Machinata.Reporting.Config.layoutingDPI```.
/// </summary>
Machinata.Reporting.Tools.convertMillimetersToPixels = function (config,millimeters) {
    var inches = millimeters * (1.0 / 25.4);
    return Machinata.Reporting.Tools.convertInchesToPixels(config,inches);
};

/// <summary>
/// Converts the given value to pixels using ```Machinata.Reporting.Config.layoutingDPI```.
/// </summary>
Machinata.Reporting.Tools.convertInchesToPixels = function (config,inches) {
    if (config == null) throw ("Machinata.Reporting.Tools.convertInchesToPixels: config is null");
    if (config.layoutingDPI == null) throw ("Machinata.Reporting.Tools.convertInchesToPixels: config.layoutingDPI is null");
    var dpi = config.layoutingDPI;
    var px = inches * dpi;
    return px;
};

/// <summary>
/// Toggles fullscreen for a node by opening a fullscreen report for the nodes screen number.
/// </summary>
Machinata.Reporting.Tools.toggleFullscreenForNode = function (instance,nodeElem) {
    // Find screen number
    var screenNumber = nodeElem.attr("data-screen");
    if ($(document).fullScreen() == false) {
        if (screenNumber == "") screenNumber = null;
        if (screenNumber != null) screenNumber = parseInt(screenNumber);
        Machinata.Reporting.Tools.openFullscreenReport(instance,screenNumber);
    } else {
        Machinata.Reporting.Tools.exitFullscreenLayout(instance);
    }
};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Reporting.Tools.openFullscreenReport = function (instance,screenNumber, title, subtitle) {
    //Machinata.Reporting.Tools.openFullscreenReportViaIframe(instance,screenNumber, title, subtitle);
    Machinata.Reporting.Tools.openFullscreenReportViaInlineElement(instance, screenNumber, title, subtitle);
};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Reporting.Tools.openFullscreenReportViaInlineElement = function (instance, screenNumber, title, subtitle) {


    // Fallback title to report tile
    if (title == null) title = instance.config.reportTitle;

    // Find screen number
    if (screenNumber == null) screenNumber = 0;

    // Create fullscreen UI
    var fullscreenContainer = $("<div class='machinata-reporting-fullscreen-container machinata-reporting-styling'></div>");
    instance.elems.fullscreen = fullscreenContainer;

    // Read progress
    var reportContainer = $("<div class='machinata-reporting-report'></div>");
    var reportContainerId = Machinata.UI.autoUIDElem(reportContainer);
    fullscreenContainer.append(reportContainer);

    // Read progress
    var fullscreenReadProgress = $("<div class='machinata-reporting-readprogress'><div class='progress'></div></div>");
    fullscreenContainer.append(fullscreenReadProgress);

    // Toolbar
    var fullscreenToolbar = Machinata.Reporting.buildToolbar(instance, title, "");
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    Machinata.Reporting.addToolToToolbar(instance, fullscreenContainer, fullscreenToolbar, Machinata.Reporting.Text.translate(instance, "reporting.exit-fullscreen"), "close", function () {
        Machinata.Reporting.Tools.exitFullscreenLayout(instance);
    });
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    fullscreenToolbar.find(".title,.subtitle").css("user-select","none");
    fullscreenToolbar.find(".subtitle").addClass("machinata-reporting-attribute-currentscreen");
    fullscreenContainer.append(fullscreenToolbar);

    // Controls
    {
        var controlElem = $('<div class="machinata-reporting-screen-controls prev"></div>');
        controlElem.append(Machinata.Reporting.buildIcon(instance, "chevron-left"));
        controlElem.appendTo(fullscreenContainer);
    }
    {
        var controlElem = $('<div class="machinata-reporting-screen-controls next"></div>');
        controlElem.append(Machinata.Reporting.buildIcon(instance, "chevron-right"));
        controlElem.appendTo(fullscreenContainer);
    }

    // Register in body
    $("body").append(fullscreenContainer);

    // Init a fullscreen version of the report
    // This is a new clone from the initConfig of the calling report
    var fullScreenConfig = Machinata.Util.extend({}, instance.initConfig, {
        profile: "fullscreen",
        screens: [screenNumber],
        chapters: "*",
        contentSelector: "#" + reportContainerId
    });
    var fullscreenInstance = Machinata.Reporting.init(fullScreenConfig, function (instance, config) {
        // Success...
        // Initial UI update
        Machinata.Reporting.Tools._updateFullscreenUI(instance);
    });

    // Bind some tools
    fullscreenContainer.find(".machinata-reporting-screen-controls.next").click(function () {
        Machinata.Reporting.Tools.nextScreen(fullscreenInstance);
    });
    fullscreenContainer.find(".machinata-reporting-screen-controls.prev").click(function () {
        Machinata.Reporting.Tools.prevScreen(fullscreenInstance);
    });

    // Bind some events
    $(document).bind("fullscreenchange", function () {
        if ($(document).fullScreen() == false) {
            Machinata.Reporting.Tools.exitFullscreenLayout(instance);
        }
    });

    // Enter fullscreen
    fullscreenContainer.fullScreen(true);

};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
/// <deprecated/>
/// <hidden/>
Machinata.Reporting.Tools.openFullscreenReportViaIframe = function (instance,screenNumber, title, subtitle) {
    throw "Machinata.Reporting.Tools.openFullscreenReportViaIframe is no longer supported"

    /*
    // Fallback title to report tile
    if (title == null) title = instance.config.reportTitle;

    // Find screen number
    if (screenNumber == null) screenNumber = 0;

    // Create fullscreen UI
    var fullscreenContainer = $("<div class='machinata-reporting-fullscreen-container'><iframe></iframe></div>");
    var screenURL = instance.config.url;
    screenURL = Machinata.updateQueryString("screen", screenNumber, screenURL);
    screenURL = Machinata.updateQueryString("profile", "fullscreen", screenURL);
    screenURL = Machinata.updateQueryString("chapter", "*", screenURL);
    fullscreenContainer.find("iframe").attr("src", screenURL);

    // Read progress
    var fullscreenReadProgress = $("<div class='machinata-reporting-readprogress'><div class='progress'></div></div>");
    fullscreenContainer.append(fullscreenReadProgress);

    // Toolbar
    var fullscreenToolbar = Machinata.Reporting.buildToolbar(instance, title, "");
    var totalScreens = instance.totalScreens;
    if (totalScreens == 0) totalScreens = 1;
    fullscreenContainer.find("iframe").on("load", function () {
        var newURL = this.contentWindow.location.href;
        var newScreen = Machinata.queryParameterFromURL("screen", newURL);
        var currentScreen = parseInt(newScreen);
        currentScreen++;

        fullscreenToolbar.find(".subtitle").text(currentScreen + " / " + totalScreens);
        var progress = (currentScreen / totalScreens) * 100.0;
        fullscreenReadProgress.find(".progress").css("width", progress + "%");
    });
    Machinata.Reporting.addToolToToolbar(instance, fullscreenContainer, fullscreenToolbar, Machinata.Reporting.Text.translate(instance, "reporting.exit-fullscreen"), "close", function () {
        Machinata.Reporting.Tools.exitFullscreenLayout();
    });
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    fullscreenToolbar.find(".subtitle").text((screenNumber + 1) + " / " + totalScreens);
    fullscreenContainer.append(fullscreenToolbar);
    $("body").append(fullscreenContainer);

    // Enter fullscreen
    fullscreenContainer.fullScreen(true);

    // Set focus
    setTimeout(function () {
        fullscreenContainer.find("iframe").focus();
    }, 1000);*/
};


/// <summary>
/// Forces a exit of fullscreen mode
/// </summary>
Machinata.Reporting.Tools.exitFullscreenLayout = function (instance) {
    Machinata.debug("Machinata.Reporting.Tools.exitFullscreenLayout");
    if (instance != null && instance.elems.fullscreen != null) {
        var fullscreenContainer = instance.elems.fullscreen;
        fullscreenContainer.fullScreen(false);
        fullscreenContainer.remove();
        instance.elems.fullscreen = null;
    }
};

/// <summary>
/// Internal method for updating the fullscreen UI (when the report is opened in fullscreen)
/// </summary>
/// <hidden/>
Machinata.Reporting.Tools._updateFullscreenUI = function (instance) {
    // Init
    var fullscreenContainer = instance.elems.content.closest(".machinata-reporting-fullscreen-container");
    var totalScreens = instance.totalScreens;
    if (totalScreens == 0) totalScreens = 1;
    var progress = ((instance.currentScreen+1) / totalScreens) * 100.0;
    fullscreenContainer.find(".machinata-reporting-readprogress .progress").css("width", progress + "%");

    // Update screen number UI
    fullscreenContainer.find(".machinata-reporting-attribute-currentscreen")
        .text((instance.currentScreen + 1) + " / " + totalScreens);

    // Update next/prev buttons
    if (instance.currentScreen >= instance.totalScreens - 1) {
        fullscreenContainer.find(".machinata-reporting-screen-controls.next").hide();
    } else {
        fullscreenContainer.find(".machinata-reporting-screen-controls.next").show();
    }
    if (instance.currentScreen <= 0) {
        fullscreenContainer.find(".machinata-reporting-screen-controls.prev").hide();
    } else {
        fullscreenContainer.find(".machinata-reporting-screen-controls.prev").show();
    }
};

/// <summary>
/// Changes the report to the given screen number.
/// </summary>
Machinata.Reporting.Tools.changeScreen = function (instance, screenNumber) {
    Machinata.debug("Machinata.Reporting.Tools.changeScreen:", screenNumber);

    if (instance.screenNumber >= instance.totalScreens - 1) throw "Machinata.Reporting.Tools.changeScreen: screen " + screenNumber + " is not valid"; // sanity
    if (instance.screenNumber <= 0) throw "Machinata.Reporting.Tools.changeScreen: screen " + screenNumber + " is not valid"; // sanity

    // Update screen state to reflect the new screen number
    instance.currentScreen = screenNumber;
    instance.screensToShow = [screenNumber];
    
    // Update actual report to reflect the current screen state
    Machinata.Reporting.Tools._updateFullscreenUI(instance);
    Machinata.Reporting.buildReport(instance, instance.config);
}

/// <summary>
/// Moves to the next screen.
/// This is only supported in fullscreen mode.
/// </summary>
Machinata.Reporting.Tools.nextScreen = function (instance) {
    if (instance.currentScreen >= instance.totalScreens - 1) return; // sanity
    Machinata.Reporting.Tools.changeScreen(instance, instance.currentScreen + 1);
};

/// <summary>
/// Moves to the previous screen.
/// This is only supported in fullscreen mode.
/// </summary>
Machinata.Reporting.Tools.prevScreen = function (instance) {
    if (instance.currentScreen <= 0) return; // sanity
    Machinata.Reporting.Tools.changeScreen(instance, instance.currentScreen - 1);
};


/// <summary>
/// Scrolls to an element, automatically taking into account the header
/// </summary>
Machinata.Reporting.Tools.scrollTo = function (instance, elem, duration) {
    if (duration == null) duration = 0;
    // Compile options for scrollto
    var opts = {};
    if (instance.elems.header != null) {
        opts.offset = { top: instance.elems.header.height() * -1.5, left: 0 };
    }
    Machinata.UI.scrollTo(elem, duration, opts);
};




/// <summary>
/// Opens/focuses/drilldowns using a link JSON object.
/// 
/// When linking to nodes within the same report, the browser will automatically scroll to the node (if it is already on the page).
/// If the given node happens to be a chapter, instead of a scroll to that node the chapter will be automatically selected.
///
/// ## Link JSON
/// The link object can have the following properties:
///  - ```tooltip```: resolvable JSON text which is used the mouse-hover tooltip or accessibility
///  - ```nodeId```: the id of a node within the same report to link to (string)
///  - ```chapterId```: the id of a chapter within the same report to link to (string)
///  - ```url```: fully qualified URL to an external page or report (string)
///  - ```target```: if defined, will open a new browser window (string)
///  - ```drilldown```: Drilldown on node information, for example invoking a table to focus on a very specific row (JSON object)
/// </summary>
/// <example>
/// ### Example with node focus
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
/// }
/// ```
/// ### Example with chapter change
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "chapterId": "rep-20190710-chapterxyz"
/// }
/// ```
/// ### Example with external link
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Go to Nerves.ch"
///     },
///     "url": "https://nerves.ch",
///     "target": "_blank"
/// }
/// ```
/// ### Example with drilldown for table
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Drilldown on AAA >10 years"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
///     "drilldown": {
///         "filters": [
///             { 
///                 "columnId": "rating",
///                 "value": "AAA"
///             } ,
///             { 
///                 "columnId": "maturityBucket",
///                 "value": ">10 years"
///             } 
///         ]
///     }
/// }
/// ```
/// </example>
Machinata.Reporting.Tools.openLink = function (instance,linkJSON) {
    if (linkJSON == null) return;

    // Which mode?
    if (linkJSON.url != null) {
        // URL
        if (linkJSON.target != null) {
            window.open(linkJSON.url, linkJSON.target);
        } else {
            window.location = linkJSON.url;
        }
    } else if (linkJSON.elementId != null) {
        // HTML element drilldown
        Machinata.Reporting.Tools.drilldownOnElement(instance,linkJSON);
    } else if (linkJSON.nodeId != null) {
        // Node drilldown
        Machinata.Reporting.Tools.drilldownOnNode(instance,linkJSON);
    } else if (linkJSON.chapterId != null) {
        // Chapter
        Machinata.Reporting.Tools.changeChapter(instance,linkJSON.chapterId);
    }

};


/// <summary>
/// Changes to a ```chapterId``` in the current report via the default handler.
/// </summary>
Machinata.Reporting.Tools.changeChapter = function (instance,chapterId) {
    if (Machinata.Reporting.Handler.changeChapter != null) Machinata.Reporting.Handler.changeChapter(instance,chapterId);
    else Machinata.Reporting.Tools.changeChapterViaURLUpdate(instance,chapterId); // default
};

/// <summary>
/// Changes to a ```chapterId``` in the current report.
/// </summary>
Machinata.Reporting.Tools.changeChapterViaURLUpdate = function (instance,chapterId) {
    // Find matching chapter go to that url
    Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
        if (chapterJSON.id == chapterId) {
            window.location = chapterJSON.chapterURL;
            return;
        }
    });
};

/// <summary>
/// Changes to a ```chapterId``` in the current report.
/// </summary>
Machinata.Reporting.Tools.changeChapterViaInlineUpdate = function (instance, chapterId) {
    // Just rebuild the report set to the new chapter
    instance.config.chapters = [chapterId];
    instance.config.initialChapter = chapterId;
    Machinata.Reporting.buildReport(instance, instance.config);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.showReportDates = function (instance, catalog, mandate, report, defaultDate, title) {
    if (title == null) title = Machinata.Reporting.Text.translate(instance, 'reporting.change-date');
    var diag = Machinata.messageDialog(title, '');
    var datePickerElem = $("<div class='ui-datepicker'></div>");
    if (report != null && defaultDate == null) defaultDate = report.date;
    datePickerElem.datepicker({
        defaultDate: defaultDate,
        prevText: "",
        nextText: "",
        onSelect: function (dateText, inst) {
            var date = datePickerElem.datepicker("getDate");
            var report = mandate.dateToReportMap[date];
            Machinata.goToPage(report.urlResolved);
        },
        beforeShowDay: function (date) {
            // See https://api.jqueryui.com/datepicker/#option-beforeShowDay
            // Init
            var selectable = false;
            var classes = "";
            var tooltip = Machinata.Reporting.Text.translate(instance,"reporting.no-data-for-this-date"); 
            // Data available?
            if (mandate.dateToReportMap[date] != null) {
                selectable = true;
                //tooltip = mandate.dateToReportMap[date].dateResolved; //BUG: jquery ui bug
                tooltip = null;
            }
            return [selectable, classes, tooltip];
        }
    });
    diag.elem.append(datePickerElem);
    diag.cancelButton();
    diag.show();
};

/// <summary>
/// A universal routine to make any table sortable.
/// </summary>
Machinata.Reporting.Tools.makeTableSortable = function (instance,tableElem) {
    var allowReset = true;
    if (instance.config != null) allowReset = instance.config.allowTableSortResets;
    // See https://mottie.github.io/tablesorter/docs/example-widget-filter.html#filter-placeholder for more options
    tableElem.tablesorter({
        sortReset: allowReset, // third click on the header will reset column to default - unsorted
        widthFixed: true,
        widgets: ['filter', 'staticRow'],
        widgetOptions: {
            //filter_placeholder: { search: "{text.reporting.filter-placeholder}", select: '' }
        }
    });
    tableElem.on('filterEnd filterReset', function () {
        var c = this.config,
            fr = c.filteredRows;
        if (fr === 0) {
            c.$table.find('.no-results').remove();
            c.$table.append([
                // "remove-me" class added to rows that are not to be included 
                // in the cache when updating
                '<tr class="no-results remove-me" role="alert" aria-live="assertive">',
                '<td colspan="' + c.columns + '">' + Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results") + ' <a class="reset-action">' + Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.reset") +'</a></td>',
                '</tr>'
            ].join(''));
            c.$table.find("a.reset-action").click(function () {
                c.$table.trigger('filterReset');
            });
        } else {
            c.$table.find('.no-results').remove();
        }
    });
    // Remap tablefilter classes to original headers
    tableElem.find("tr.tablesorter-filter-row input").each(function () {
        var index = parseInt($(this).attr("data-column"));
        var matchingColumn = tableElem.find("tr.tablesorter-headerRow th").eq(index);
        if (matchingColumn.hasClass("type-text")) $(this).parent().addClass("type-text");
    });
    tableElem.find(".tablesorter-filter-row .tablesorter-filter.disabled")
        .parent()
        .addClass("disabled");
    tableElem.find(".tablesorter-filter-row .tablesorter-filter:not(.disabled)")
        .attr("title", Machinata.Reporting.Text.translate(instance, "reporting.nodes.search-filter"));
};

/// <summary>
/// Performs a drilldown operation on a table. Usually this is not called directly, but rather
/// indirectly using a drilldown on a node/element using a link JSON.
/// </summary>
Machinata.Reporting.Tools.drilldownOnTable = function (tableElem, drilldownJSON) {

    // Get the column indexes
    var columns = tableElem.find("tr.header.columns th");
    if (columns.length == 0) return; // abort
    var filterData = [];
    Machinata.Util.each(columns, function (index, elem) {
        var columnId = $(elem).attr("data-col-id");
        var filterToApply = '';
        // Search for match
        Machinata.Util.each(drilldownJSON.filters, function (index, filter) {
            if (filter.columnId == columnId) {
                filterToApply = "'" + filter.value + "'"; //TODO: escape '
            }
        });
        filterData.push(filterToApply);
    });

    // Apply filter
    //TODO: what if this table hasn't been sorted/filtered yet?
    $.tablesorter.setFilters(tableElem, filterData, true);

    // Show
    tableElem.show();

};

/// <summary>
/// Scrolls to a html element, automatically taking into account the header
/// </summary>
Machinata.Reporting.Tools.drilldownOnElement = function (instance,linkJSON) {
    var targetElement = null;
    if (linkJSON.elementId != null) targetElement = $("#" + linkJSON.elementId);

    if (linkJSON.drilldown != null) {
        Machinata.Reporting.Tools.drilldownOnTable(targetElement, linkJSON.drilldown);
    }

    Machinata.Reporting.Tools.scrollTo(instance, targetElement, 1200);
};

/// <summary>
/// Scrolls to a node element, automatically taking into account the header
/// </summary>
Machinata.Reporting.Tools.drilldownOnNode = function (instance,linkJSON) {

    var targetNode = null;
    if (linkJSON.nodeId != null) targetNode = $("#" + linkJSON.nodeId);

    if (targetNode.length == 0) {
        // Node is not on page, get its link
        //console.log(linkJSON);
        //console.log(instance.idToNodeJSONLookupTable);
        var nodeJSON = Machinata.Reporting.Node.getJSONByID(instance,linkJSON.nodeId);
        if (nodeJSON == null) return;
        // Is this node actually a chapter?
        if (nodeJSON.chapter == true) {
            // Change chapter
            Machinata.Reporting.Tools.changeChapter(instance,linkJSON.nodeId);
        } else {
            // Focus
            linkJSON.url = nodeJSON.url;
            linkJSON.nodeId = null;
            Machinata.Reporting.Tools.openLink(instance,linkJSON);
        }
    } else {
        // Node is on page, just scroll to it...
        if (linkJSON.drilldown != null) {
            var tableElem = targetNode.find("table");
            if (tableElem.length == 1) {
                Machinata.Reporting.Tools.drilldownOnTable(tableElem, linkJSON.drilldown);
            }
        }
        Machinata.Reporting.Tools.scrollTo(instance, targetNode, 1200);
    }
};



/// <summary>
/// Opens the print composer built-in UI.
/// </summary>
Machinata.Reporting.Tools.openPrintComposer = function (instance) {
    Machinata.Reporting.PrintComposer.UI.open(instance);
}


/// <summary>
/// Opens the browser print dialog
/// </summary>
Machinata.Reporting.Tools.openBrowserPrintDialog = function (instance) {
    window.print();
}

/// <summary>
/// 
/// </summary>
/// <deprecated/>
Machinata.Reporting.Tools.openPrintPage = function (instance, nodeInfosOrChapter) {
    throw "Machinata.Reporting.Tools.openPrintPage is deprecated!";
}

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForSection = function (instance,nodeInfos) {
    var screenNumber = nodeInfos.nodeScreen;
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", screenNumber, printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", "*", printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForChapter = function (instance, chapter) {
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", "", printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", chapter, printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForReport = function (instance, reportId) {
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", "", printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", "*", printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};


/// <summary>
/// For the given report, shows a menu of various printing options.
/// Typically such options include the following:
///  - Print entire report
///  - Print current chapter
///  - View print composer
/// </summary>
Machinata.Reporting.Tools.printReport = function (instance) {
    
    var opts = {};
    if (Machinata.Reporting.Handler.openPrintPageForReport != null) {
        opts["printreport"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-report");
    }
    if (Machinata.Reporting.Handler.openPrintPageForChapter != null && instance.chapters != null && instance.chapters.length > 1 && instance.config.automaticallyFoldChaptersIntoPages == true) {
        opts["printchapter"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-chapter");
    }
    if (Machinata.Reporting.Handler.openPrintComposer != null && Machinata.Reporting.PrintComposer.hasData(instance) == true) {
        opts["viewcomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-view-composer");
    }
    if (Object.keys(opts).length == 0) console.warn("Machinata.Reporting.Tools.printReport: no handlers have been registered for printing. See Machinata.Reporting.Handler.printReport for more information.");
    Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-report.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print-report.message"), opts)
        .okay(function (id, val) {
            if (id == "printchapter") {
                Machinata.Reporting.Handler.openPrintPageForChapter(instance,instance.config.chapters[0]);
            } else if (id == "viewcomposer") {
                Machinata.Reporting.Handler.openPrintComposer(instance);
            } else  {
                Machinata.Reporting.Handler.openPrintPageForReport(instance, instance.reportData == null ? null : instance.reportData.reportId);
            }
        })
        .show();
};

/// <summary>
/// For the given node, shows a menu of various printing options.
/// Typically such options include the following:
///  - Print entire report
///  - Print current chapter
///  - Print this section
///  - Add to print composer
///  - View print composer
/// </summary>
Machinata.Reporting.Tools.printNode = function (instance, infosJSON, nodeJSON) {

    var opts = {};
    // Report
    if (Machinata.Reporting.Handler.openPrintPageForReport != null) {
        opts["printreport"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-report");
    }
    // Chapter
    if (Machinata.Reporting.Handler.openPrintPageForChapter != null && instance.chapters != null && instance.chapters.length > 1) {
        opts["printchapter"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-chapter");
    }
    // Section
    if (Machinata.Reporting.Handler.openPrintPageForSection != null) {
        opts["printsection"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-section");
    }
    // Add to composer
    if (Machinata.Reporting.Handler.openPrintComposer != null) {
        opts["addtocomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-add-composer");
    }
    // View composer
    if (Machinata.Reporting.Handler.openPrintComposer != null && Machinata.Reporting.PrintComposer.hasData(instance) == true) {
        opts["viewcomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-view-composer");
    }
    if (Object.keys(opts).length == 0) console.warn("Machinata.Reporting.Tools.printReport: no handlers have been registered for printing. See Machinata.Reporting.Handler.printNode for more information.");
    // Show dialog with all options
    Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-node.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print.node.message"), opts)
        .okay(function (id, val) {
            if (id == "addtocomposer") {
                Machinata.Reporting.PrintComposer.addScreen(instance, infosJSON);
            } else if (id == "printsection") {
                Machinata.Reporting.Handler.openPrintPageForSection(instance, infosJSON);
            } else if (id == "printchapter") {
                Machinata.Reporting.Handler.openPrintPageForChapter(instance,nodeJSON.chapterId);
            } else if (id == "viewcomposer") {
                Machinata.Reporting.Handler.openPrintComposer(instance);
            } else {
                Machinata.Reporting.Handler.openPrintPageForReport(instance, instance.reportData == null ? null : instance.reportData.reportId);
            }
        })
        .show();
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.copyElementToClipboard = function (tempElem, title, message, useTemporaryDom) {
    if (useTemporaryDom == true) {
        Machinata.UI.copyElementToClipboardInTemporaryDOM(tempElem);
    } else {
        Machinata.UI.copyElementToClipboard(tempElem);
    }
    setTimeout(function () {
        Machinata.messageDialog(title, message).show();
    }, 100);
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.generateThemeCode = function (config, type) {
    var shades = config.themeColorShadeNames;
    var ret = "";
    ret += "// Machinata.Reporting.Tools.generateThemeCode(\"" + type + "\")\n";
    ret += "//   type: " + type + "\n";

    Machinata.Util.each(Machinata.Reporting._themes, function (key, val) {
        ret += "\n";
        for (var i = 0; i < shades.length; i++) {
            var shade = shades[i];
            var color = val[i];
            var textColor = null;
            var textOnTheme = Machinata.Reporting._themes["text-on-" + key];
            if (textOnTheme != null) textColor = textOnTheme[i];
            if (color == null) continue;
            ret += "/* Theme: " + key + ", Shade: " + shade + " */\n";
            ret += ".machinata-reporting-themable.theme-" + key + " .theme-fill-" + shade + " { fill: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + key + " .theme-stroke-" + shade + " { stroke: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + key + " .theme-bg-" + shade + " { background-color: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + key + " .theme-color-" + shade + " { color: " + color + "; }  \n";
            if (textColor != null) ret += ".machinata-reporting-themable.theme-" + key + " .theme-text-" + shade + " { color: " + textColor + "; }  \n";

        }
    });
    return ret;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.lineBreakString = function (text, opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.appendEllipsisOnLastLine == null) opts.appendEllipsisOnLastLine = true;
    if (opts.wordSeparator == null) opts.wordSeparator = " ";
    if (opts.lineSeparator == null) opts.lineSeparator = "\n";
    if (opts.ellipsis == null) opts.ellipsis = Machinata.Reporting.Config.textTruncationEllipsis;
    if (opts.returnArray == null) opts.returnArray = false;
    var words = text.split(opts.wordSeparator);
    var lines = [];
    var currentLine = null;
    // Sanity
    if (text == null) return null;
    if (text == "") return "";
    for (var w = 0; w < words.length; w++) {
        // Init
        var word = words[w];
        var newCurrentLine = null;
        // Create a new line
        if (currentLine == null) newCurrentLine = word;
        else newCurrentLine = currentLine + opts.wordSeparator + word;
        //alert("newCurrentLine: " + newCurrentLine);
        if (newCurrentLine.length > opts.maxCharsPerLine) {
            // Push current
            //alert("pushing: " + currentLine);
            lines.push(currentLine);
            // New line with current word
            currentLine = word;
        } else {
            // Accept, continue...
            currentLine = newCurrentLine;
        }
    }
    // Push final line
    //alert("pushing: " + currentLine);
    lines.push(currentLine);
    // Rebuild
    var ret = null;
    for (var l = 0; l < lines.length; l++) {
        // Init
        var line = lines[l];
        // Validate
        if (opts.maxLines != null && l >= (opts.maxLines)) {
            if (opts.returnArray == true) {
                if (opts.appendEllipsisOnLastLine == true) {
                    ret[ret.length - 1] = ret[ret.length - 1] + opts.ellipsis;
                } else {
                    ret.push(opts.ellipsis);
                }
            } else {
                if (opts.appendEllipsisOnLastLine == true) {
                    ret = ret + opts.ellipsis;
                } else {
                    ret = ret + opts.lineSeparator + opts.ellipsis;
                }
            }
            break;
        }
        // Append
        if (opts.returnArray == true) {
            if (ret == null) ret = [];
            ret.push(line);
        } else {
            if (ret == null) ret = line;
            else ret = ret + opts.lineSeparator + line;
        }
        
    }
    return ret;
};


/// <summary>
/// Creates a title using the givens string with variables, using the avaiable data.
/// You can use the following variables:
/// - ```{mandate.id}```
/// - ```{mandate.title}```
/// - ```{report.id}```
/// - ```{report.title}```
/// - ```{report.subtitle}```
/// - ```{report.name}```
/// - ```{report.date-raw}```
/// - ```{report.date-resolved}```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Tools.createTitleString(instance, config, "ACME AG Report {report.portfolio-id} {report.date-raw} {report.title}");
/// ```
/// </example>
Machinata.Reporting.Tools.createTitleString = function (instance, config, text) {
    // Grab template variables from various sources
    if (instance.catalogCurrentMandate != null) {
        text = text.replaceAll("{mandate.id}", instance.catalogCurrentMandate.id);
        text = text.replaceAll("{mandate.title}", instance.catalogCurrentMandate.title);
    }
    if (instance.catalogCurrentReport != null) {
        text = text.replaceAll("{report.id}", instance.catalogCurrentReport.id);
        text = text.replaceAll("{report.title}", config.reportTitle);
        text = text.replaceAll("{report.subtitle}", config.reportSubtitle);
        text = text.replaceAll("{report.name}", config.reportName);
        text = text.replaceAll("{report.date-raw}", instance.catalogCurrentReport.dateRaw);
        text = text.replaceAll("{report.date-resolved}", instance.catalogCurrentReport.dateResolved);
        if (instance.catalogCurrentReport.parameters !=  null) text = text.replaceAll("{report.portfolio-id}", instance.catalogCurrentReport.parameters.portfolioId);
    }
    // Cleanup
    text = text.replaceAll("{mandate.id}", "");
    text = text.replaceAll("{mandate.title}", "");
    text = text.replaceAll("{report.id}", "");
    text = text.replaceAll("{report.title}", "");
    text = text.replaceAll("{report.subtitle}", "");
    text = text.replaceAll("{report.name}", "");
    text = text.replaceAll("{report.date-resolved}", "");
    text = text.replaceAll("{report.date-raw}", "");
    text = text.replaceAll("{report.portfolio-id}", "");
    //TODO: trim out missing variables separators...
    text = text.replace(/  +/g, ' ');
    text = text.replace(/ - - +/g, ' ');
    return text;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.measureStringApproximately = function (config, text, fontSize, fontName) {
    // Init
    if (text == null) return 0;
    if (fontSize == null) fontSize = config.textSize; // fallback


    var approxLen = text.length * fontSize * config.fontSizeToCharacterWidthRatio;
    return approxLen;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.bindTableUI = function (instance,elements) {
    // Table filters
    elements.filter(".machinata-reporting-table.option-filter").each(function () {
        var tableElem = $(this);
        Machinata.Reporting.Tools.makeTableSortable(instance,tableElem);
    });
    elements.filter(".machinata-reporting-table.option-sort").each(function () {
        var tableElem = $(this);
        tableElem.tablesorter({
            widgets: ['staticRow'],
        });
    });

    // Sticky table headers
    elements.filter(".machinata-reporting-table.option-auto-sticky").each(function () {
        var tableElem = $(this);
        if (tableElem.find(".table-row").length > 5) {
            tableElem.stickyTableHeaders({
                cacheHeaderHeight: true,
                fixedOffset: $('#header')
            });
            tableElem.on("enabledStickiness.stickyTableHeaders", function () {
                $(this).addClass("header-is-sticky");
            });
            tableElem.on("disabledStickiness.stickyTableHeaders", function () {
                $(this).removeClass("header-is-sticky");
            });
        }
    });

    // Jump to top
    elements.find(".table-header .icon-arrow.up").click(function () {
        var tableElem = $(this).closest(".ui-table");
        Machinata.UI.scrollTo(tableElem, 600, { offset: -60 });
    });

    // Clickable rows
    elements.find("tr.option-link").click(function () {
        var url = $(this).attr("href");
        if (url == null || url == "") return;
        Machinata.goToPage(url);
    });
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.bindTooltipsUI = function (instance, elements) {
    elements.tooltip({
        classes: {
            "ui-tooltip": "machinata-reporting-tooltip"
        },
        track: true,
        show: {
            delay: 0,
            effect: "none"
        },
        hide: {
            delay: 0,
            effect: "none"
        },
        position: {
            my: "center bottom-40",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>").addClass("arrow").addClass(feedback.vertical).appendTo(this);
            }
        },
        open: function (e, ui) {
            if (document.fullscreenElement) {
                ui.tooltip.appendTo($(document.fullscreenElement));
            }
        }
    });
};



/// <summary>
/// </summary>
Machinata.Reporting.Tools.askAQuestionViaEmail = function (instance, infosJSON, nodeJSON) {
    var toEmail = instance.config.askAQuestionViaEmailDefaultEmail;
    var toName = instance.config.askAQuestionViaEmailDefaultName;
    var subject = instance.config.askAQuestionViaEmailDefaultSubject;
    var body = instance.config.askAQuestionViaEmailDefaultBody;
    function replaceVariables(str) {
        str = str.replace("{report.title}", infosJSON.reportTitle);
        str = str.replace("{report.subtitle}", infosJSON.reportSubtitle);
        str = str.replace("{report.url}", infosJSON.reportURL);
        str = str.replace("{node.title}", infosJSON.nodeTitle);
        str = str.replace("{node.subtitle}", infosJSON.nodeSubtitle);
        str = str.replace("{node.url}", infosJSON.nodeURL);
        str = str.replace("{to.email}", toEmail);
        str = str.replace("{to.name}", toName);
        return str;
    }
    subject = replaceVariables(subject);
    body = replaceVariables(body);
    window.open("mailto:" + toEmail + "?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(body));
};



/// <summary>
/// </summary>
Machinata.Reporting.Tools.shareNodeByCopyingLink = function (instance, infosJSON, nodeJSON) {
    
    var diag = Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.share"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.share.description"));
    diag.elem.append($("<p class='node-link' style='font-size:0.8em;word-break:break-all;'></p>").text(infosJSON.nodeURL));
    diag.button(Machinata.Reporting.Text.translate(instance, "reporting.nodes.share.copy"), "copy", function () {
        Machinata.Reporting.Tools.copyElementToClipboard(diag.elem.find(".node-link"), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.share.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.share.copied.text'));
        diag.close();
    },false,false);
    diag.okayButton();
    diag.show();

};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences = function (json) {
    var seen = new WeakSet();
    function getCircularReplacer(key, value) {
        if (key == "dataSource") return;
        if (key == "initConfig") return;
        if (key == "parent") return;
        if (key == "domElem") return;
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    }
    return JSON.stringify(instance.reportData, getCircularReplacer, 2);
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.getCalibrationJSONForFont = function (config, font) {

    // Relevant string measurement code in Vega:
    // https://github.com/vega/vega/blob/master/packages/vega-scenegraph/src/util/canvas/context.js
    // https://github.com/vega/vega/blob/master/packages/vega-canvas/index.js
    // https://github.com/vega/vega/tree/master/packages/vega-scenegraph/src/util
    // https://github.com/vega/vega/blob/master/packages/vega-scenegraph/src/util/text.js#L133
    // See also
    // https://github.com/vega/vega/issues/2396

    console.log("Machinata.Reporting.Tools.getCalibrationJSONForFont:");

    var fontString = "9px " + font;
    var measureString = "abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
    //measureString = "14,000,000";
    console.log("  "+"fontString: "+fontString);
    console.log("  " + "measureString: " + measureString);

    try {

        var canvas = Machinata.Imaging.createDrawingCanvas(1, 1);

        
        console.log(canvas);
        var context = canvas.getContext('2d');
        console.log(context);
        context.font = fontString;
        var measure = context.measureText(measureString);
        console.log("  " + "measured dims are:", measure);




    } catch (error) {
        console.error(error);
    }
};



/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration = function (themeJSON) {

    // Web / default (from theme)
    Machinata.Reporting.Config.assetsPath = themeJSON["assets-path"];
    Machinata.Reporting.Config.iconsBundle = themeJSON["icons-bundle"];

    Machinata.Reporting.Config.padding = themeJSON["padding"];

    Machinata.Reporting.Config.lineSize = themeJSON["line-size"];
    Machinata.Reporting.Config.graphLineSize = themeJSON["graph-line-size"];
    Machinata.Reporting.Config.graphSymbolSize = themeJSON["graph-symbol-size"];
    Machinata.Reporting.Config.domainLineSize = themeJSON["domain-line-size"];
    Machinata.Reporting.Config.verticalBarMaxSize = themeJSON["vertical-bar-max-size"];
    Machinata.Reporting.Config.horizontalBarMaxSize = themeJSON["horizontal-bar-max-size"];
    Machinata.Reporting.Config.textTruncationEllipsis = themeJSON["text-truncation-ellipsis"];

    Machinata.Reporting.Config.textFont = themeJSON["text-font"];
    Machinata.Reporting.Config.labelFont = themeJSON["label-font"];
    Machinata.Reporting.Config.axisFont = themeJSON["axis-font"];
    Machinata.Reporting.Config.headlineFont = themeJSON["headline-font"];
    Machinata.Reporting.Config.legendFont = themeJSON["legend-font"];
    Machinata.Reporting.Config.subtitleFont = themeJSON["subtitle-font"];
    Machinata.Reporting.Config.numberFont = themeJSON["number-font"];

    Machinata.Reporting.Config.textSize = themeJSON["text-size"];
    Machinata.Reporting.Config.subtitleSize = themeJSON["subtitle-size"];
    Machinata.Reporting.Config.chartTextSize = themeJSON["chart-text-size"];
    Machinata.Reporting.Config.labelSize = themeJSON["label-size"];
    Machinata.Reporting.Config.axisSize = themeJSON["axis-size"];
    Machinata.Reporting.Config.h1Size = themeJSON["h1-size"];
    Machinata.Reporting.Config.h2Size = themeJSON["h2-size"];
    Machinata.Reporting.Config.h3Size = themeJSON["h3-size"];
    Machinata.Reporting.Config.h4Size = themeJSON["h4-size"];

    Machinata.Reporting.Config.legendSize = themeJSON["legend-size"];
    Machinata.Reporting.Config.legendPadding = themeJSON["legend-padding"];
    Machinata.Reporting.Config.legendSymbolSize = themeJSON["legend-symbol-size"];
    Machinata.Reporting.Config.legendSymbolStroke = themeJSON["legend-symbol-stroke"];
    Machinata.Reporting.Config.legendColumnPadding = themeJSON["legend-column-padding"];
    Machinata.Reporting.Config.legendRowPadding = themeJSON["legend-row-padding"];

    Machinata.Reporting.Config.gridColor = themeJSON["grid-color"];
    Machinata.Reporting.Config.solidColor = themeJSON["solid-color"];
    Machinata.Reporting.Config.darkColor = themeJSON["dark-color"];
    Machinata.Reporting.Config.pageColor = themeJSON["page-color"];
    Machinata.Reporting.Config.titleColor = themeJSON["title-color"];
    Machinata.Reporting.Config.highlightColor = themeJSON["highlight-color"];
    Machinata.Reporting.Config.domainLineColor = themeJSON["domain-line-color"];

    Machinata.Reporting.Config.mediumContentWidthSize = themeJSON["medium-content-width-size"];
    Machinata.Reporting.Config.fontSizeToCharacterWidthRatio = themeJSON["font-size-to-character-width-ratio"];

    Machinata.Reporting.Config.axisBottomShowTicks = themeJSON["axis-bottom-show-ticks"];
    Machinata.Reporting.Config.axisBottomTickSize = themeJSON["axis-bottom-tick-size"];


    Machinata.Reporting.Config.layoutMinWidth = themeJSON["reporting-layout-min-width"];
    Machinata.Reporting.Config.layoutMaxWidth = themeJSON["reporting-layout-max-width"];


    // Webprint profile
    Machinata.Reporting.Profiles.Configs["webprint"].layoutMinWidth = themeJSON["reporting-layout-min-width-webprint"];
    Machinata.Reporting.Profiles.Configs["webprint"].layoutMaxWidth = themeJSON["reporting-layout-max-width-webprint"];


    // Print profile (from theme)

    Machinata.Reporting.Profiles.Configs["print"].textFont = themeJSON["text-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].labelFont = themeJSON["label-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisFont = themeJSON["axis-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].headlineFont = themeJSON["headline-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendFont = themeJSON["legend-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].subtitleFont = themeJSON["subtitle-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].numberFont = themeJSON["number-font-print"];

    Machinata.Reporting.Profiles.Configs["print"].textSize = themeJSON["text-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].subtitleSize = themeJSON["subtitle-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].chartTextSize = themeJSON["chart-text-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].labelSize = themeJSON["label-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisSize = themeJSON["axis-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h1Size = themeJSON["h1-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h2Size = themeJSON["h2-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h3Size = themeJSON["h3-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h4Size = themeJSON["h4-size-print"];

    Machinata.Reporting.Profiles.Configs["print"].padding = themeJSON["padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].fontSizeToCharacterWidthRatio = themeJSON["font-size-to-character-width-ratio-print"];
    Machinata.Reporting.Profiles.Configs["print"].lineSize = themeJSON["line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].gridLineSize = themeJSON["grid-line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].graphLineSize = themeJSON["graph-line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].domainLineSize = themeJSON["domain-line-size-print"];
    

    Machinata.Reporting.Profiles.Configs["print"].axisLabelSeparation = themeJSON["padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisLabelPadding = themeJSON["padding-print"] / 2;
    Machinata.Reporting.Profiles.Configs["print"].axisTargetTickCount = 6;
    Machinata.Reporting.Profiles.Configs["print"].verticalBarMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].horizontalBarMaxSize = themeJSON["horizontal-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].graphSymbolSize = themeJSON["graph-symbol-size-print"];

    Machinata.Reporting.Profiles.Configs["print"].legendSize = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendFont = themeJSON["legend-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendPadding = themeJSON["legend-padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendSymbolSize = themeJSON["legend-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendSymbolStroke = themeJSON["legend-symbol-stroke-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendColumnPadding = themeJSON["legend-column-padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendRowPadding = themeJSON["legend-row-padding-print"];

    Machinata.Reporting.Profiles.Configs["print"].gridColor = themeJSON["grid-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].solidColor = themeJSON["solid-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].darkColor = themeJSON["dark-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].pageColor = themeJSON["page-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].titleColor = themeJSON["title-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].highlightColor = themeJSON["highlight-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].domainLineColor = themeJSON["domain-line-color-print"];

    Machinata.Reporting.Profiles.Configs["print"].axisBottomShowTicks = themeJSON["axis-bottom-show-ticks-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisBottomTickSize = themeJSON["axis-bottom-tick-size-print"];

    // Print (generic customizations)
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["LineColumnNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["LineColumnNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["ScatterPlotNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["ScatterPlotNode"]["scatterPlotSymbolSize"] = themeJSON["graph-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelLimitLines = 4;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelLimitChars = 8;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelAddSpaceBeforeValue = false;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].satelliteLabelLimitLines = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].satelliteLabelLimitChars = 20;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["sliceLabelsSize"] = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["legendPositionPortrait"] = "bottom-left";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["legendPositionLandscape"] = "left-top";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["sliceLabelsSize"] = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["legendPositionPortrait"] = "bottom-left";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["legendPositionLandscape"] = "left-top";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["GaugeNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["GaugeNode"]["statusArcLineSize"] = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["MultiGaugeNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["MultiGaugeNode"]["statusArcLineSize"] = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["VBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["VBarNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].barMaxSize = themeJSON["horizontal-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].fillChartSpace = false;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].autoSnapXAxisTickCount = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"] * 2;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"].symbolFactSize = themeJSON["graph-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["OfflineMapNode"] = {};
}