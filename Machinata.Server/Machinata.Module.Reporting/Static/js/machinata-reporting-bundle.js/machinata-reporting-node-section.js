


/// <summary>
/// Inserts a basic title element (h1,h2,h3...).
/// These SectionNodes serve as a way to logically split a report into 
/// different chapters and sections, and can be used to build an overview or table-of-contents.
///
/// ## Title Level
/// To support different types of readers and print-modes, the SectionNode
/// will automatically create the appropriate hN-element based on it's ```titleLevel``` 
/// int setting or the style name. If the style name for example contains ```h1```, a html node
/// ```<h1/>``` will be inserted.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.SectionNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults = {};
Machinata.Reporting.Node.SectionNode.defaults.supportsToolbar = false;

/// <summary>
/// If ```true```, the SectionNode is ignored if it has no ```style``` or the ```style``` is ```null```.
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.ignoreIfStyleIsNull = false;

/// <summary>
/// If set, defines the level of the h1,h2,h3... html element.
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.titleLevel = null;


Machinata.Reporting.Node.SectionNode.init = function (instance, config, json, nodeElem) {

    // Init
    var styleName = null;
    if (json.styles != null && json.styles.length > 0) styleName = json.styles[0];

    // Ignore?
    if (json.ignoreIfStyleIsNull == true && styleName == null) return;

    // Derive the title level (h1,h2,h3...) from the style
    if (styleName != null) {
        if (styleName.toLowerCase().contains("h1")) json.titleLevel = 1;
        else if (styleName.toLowerCase().contains("h2")) json.titleLevel = 2;
        else if (styleName.toLowerCase().contains("h3")) json.titleLevel = 3;
        else if (styleName.toLowerCase().contains("h4")) json.titleLevel = 4;
        else if (styleName.toLowerCase().contains("h5")) json.titleLevel = 5;
        else if (styleName.toLowerCase().contains("h6")) json.titleLevel = 6;
    }
    if (json.titleLevel == null) json.titleLevel = 1; // fallback

    // Create H html element
    var titleElem = $("<h" + json.titleLevel + "></h" + json.titleLevel + ">");
    titleElem.addClass("section-title");
    titleElem.text(Machinata.Reporting.Text.resolve(instance,json.mainTitle));
    titleElem.attr("id", "toc_" + Machinata.Reporting.uid());
    if (styleName != null) titleElem.addClass(styleName);
    nodeElem.append(titleElem);

};
Machinata.Reporting.Node.SectionNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








