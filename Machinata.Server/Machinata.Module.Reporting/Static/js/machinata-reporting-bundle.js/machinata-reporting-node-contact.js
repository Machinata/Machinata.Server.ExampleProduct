


/// <summary>
/// Node for displaying contact information or user information.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ContactNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults = {};


/// <summary>
/// By default ContactNode have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.chrome = "solid";

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// If true, a round photo is generated and shown until the user interacts with the node.
/// By default ContactNode have a round style.
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.roundPhoto = true;

/// <summary>
/// By default ContactNode have a ```W_RoundPhoto``` style.
/// Available styles:
///  - ```W_RoundPhoto```
///  - ```W_SquarePhoto```
/// Note: styles are automatically applied as needed depending on the ```roundPhoto``` setting.
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.style = "W_RoundPhoto";

/// <summary>
/// No tools for ContactNode
/// </summary>
Machinata.Reporting.Node.ContactNode.defaults.addStandardTools = false;


/// <summary>
/// The contact JSON object that contains the data to display.
/// Possible properties:
///  - ```photo```: a resolvable image asset
///  - ```name```: a resolvable text (first and last name)
///  - ```jobTitle```: a resolvable text
///  - ```phone```: a resolvable text (will be automatically linked)
///  - ```email```: a resolvable text (will be automatically linked)
///  - ```website```: a resolvable text (will be automatically linked)
/// </summary>
/// <example>
/// ```
/// "contact": {
///     "photo": {
///         "filename": "QRCodeTest.png",
///         "mimeType": "image/png",
///         "encoding": "base64",
///         "data": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVAQMAAACT2TfVAAAABlBMVEUAAAD///+l2Z/dAAAAAnRSTlP//8i138cAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABcSURBVAiZY2Bcws5Qq/udwdVUnMF1LRCXijPUpn5nYAxhZ/jP/p9BZ44/g9De8wwHg58zXHvfzsBoGs/wn0+dgdHqPEPtI3sG1x52Btf8dgbX6+UMtZHxDIzu/AAr6RmSv9DdfQAAAABJRU5ErkJggg=="
///     },
///     "name": { "resolved": "Frederick Patel" },
///     "jobTitle": { "resolved": "Senior Analyst, Credit Suisse" },
///     "phone": { "resolved": "+41 44 123 45 61" },
///     "email": { "resolved": "frederick.patel@credit-suisse.com" }
///     "website": { "resolved": "credit-suisse.com" }
/// }
/// ```
/// </example>
Machinata.Reporting.Node.ContactNode.defaults.contact = null;


Machinata.Reporting.Node.ContactNode.init = function (instance, config, json, nodeElem) {
    // Container
    var containerElem = $("<div class='node-bg option-fullsize-content '></div>");
    if (json.roundPhoto == true) nodeElem.addClass("style-W_RoundPhoto");
    else nodeElem.addClass("style-W_SquarePhoto");

    // Profile photo
    if (json.contact.photo) {
        var photoShimElem = $("<div class='photo shim'></div>"); //Note: this shim is used to get the proper sizing, unfortunately this was not possible CSS only
        var photoElem = $("<div class='photo'></div>");
        photoElem.css("background-image", "url(" + Machinata.Reporting.Asset.resolve(json.contact.photo) + ")");
        // Bind interaction
        photoElem.hover(function () {
            photoShimElem.attr("orig-width", photoShimElem.css("width"));
            photoShimElem.attr("orig-height", photoShimElem.css("height"));
            photoElem.css("width", "100%");
            photoElem.css("height", "100%");
            photoElem.addClass("expand");
        }, function () {
            photoElem.css("width", photoShimElem.attr("orig-width"));
            photoElem.css("height", photoShimElem.attr("orig-height"));
            photoElem.removeClass("expand");
        });
        // Append
        containerElem.append(photoShimElem);
        containerElem.append(photoElem);
    }

    // Bottom
    var bottomElem = $("<div class='option-bottom-content option-padded-content'></div>").appendTo(containerElem);
    if (json.contact.name != null) {
        $("<div class='name main-property'></div>").text(Machinata.Reporting.Text.resolve(instance,json.contact.name)).appendTo(bottomElem);
    }
    if (json.contact.jobTitle != null) {
        $("<div class='jobTitle sub-property'></div>").text(Machinata.Reporting.Text.resolve(instance,json.contact.jobTitle)).appendTo(bottomElem);
    }
    if (json.contact.phone != null) {
        var txt = Machinata.Reporting.Text.resolve(instance,json.contact.phone);
        $("<a class='phone sub-property'></a>").text(txt).appendTo(bottomElem).attr("href", "tel:" + txt);
    }
    if (json.contact.email != null) {
        var txt = Machinata.Reporting.Text.resolve(instance,json.contact.email);
        $("<a class='email sub-property'></a>").text(txt).appendTo(bottomElem).attr("href", "mailto:" + txt);
    }
    if (json.contact.website != null) {
        var txt = Machinata.Reporting.Text.resolve(instance,json.contact.website);
        $("<a class='website sub-property'></a>").text(txt).appendTo(bottomElem).attr("href", "https://" + txt);
    }

    nodeElem.append(containerElem);


};
Machinata.Reporting.Node.ContactNode.resize = function (instance, config, json, nodeElem) {
    // If we use round mode, update the size depending on the node bg
    // Unfortunately we cannot do this in CSS since the height can't be derived from the width
    // We need a fixed size for the CSS transition to work on the hover
    if (json.roundPhoto == true) {
        var photoW = nodeElem.find(".node-bg").width() * 0.42;
        var photoH = photoW;
        nodeElem.find(".photo").width(photoW).height(photoH);
    }
};
Machinata.Reporting.Node.ContactNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








