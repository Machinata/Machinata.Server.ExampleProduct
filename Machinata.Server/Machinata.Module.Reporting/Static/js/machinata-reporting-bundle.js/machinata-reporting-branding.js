


/// <summary>
/// Allows the storage and retrieving of entire branding configurations (including handlers).
/// This is useful if you want to select a branding configuration at runtime.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Branding = {};

/// <summary>
/// Storage for all the registered configuraitons.
/// </summary>
/// <hidden/>
Machinata.Reporting.Branding._registeredBrandingConfigurations = {};

/// <summary>
/// Registeres a branding configuration routine by name. ```callback``` is a function which
/// will be executed when the branding configuration is requisted by ```loadBrandingConfiguration()```.
/// </summary>
Machinata.Reporting.Branding.registerBrandingConfiguration = function (name, callback) {
    Machinata.Reporting.Branding._registeredBrandingConfigurations[name] = callback;
};

/// <summary>
/// Loads a branding configuration routine by name.
/// </summary>
Machinata.Reporting.Branding.loadBrandingConfiguration = function (name) {
    var callback = Machinata.Reporting.Branding._registeredBrandingConfigurations[name];
    if (callback == null) throw "Cannot load the branding configuration '"+name+"' as it has not been registered. Are you sure the proper configruation file has been included or required?";
    callback();
};