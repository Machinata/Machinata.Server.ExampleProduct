


/// <summary>
/// For each child in ```children```, this node will create a toggle interface that 
/// can switch between the nodes.
/// 
/// ## Toggle Title
/// Children nodes can contain the additional JSON property ```toggleTitle```. If this is defined,
/// then this is used for the tab title. Furthermore, the ToggleNode property ```toggleTitles``` can also be defined
/// to map a toggle title for each child node. If no title is provided, we fallback to the the node's standard ```mainTitle```
/// property. 
/// Defining a toggle title is good practice to ensure that tabs/lists always have short, concice titles.
///
/// ## Chrome
/// Unless explicitly set, the ToggleNode will automatically detect its chrome based on the childrens
/// chrome. This is important to ensure that the tab-bar chrome matches the toggle nodes.
/// 
/// ## UX
/// Toggles should always contain nodes with similar data and UI sizes. It is best practice to make
/// sure that toggle children do not cause the layout to jump around too much. For charts use the same
/// layout sizes, for tables that can differ in row-count, make sure the row counts are similar. For nodes
/// that take up more space than single screen-height, using an alternate to ToggleNode is recommended.
///
/// ## Screens
/// You should never break children nodes of a ToggleNode into different screens. A toggle node may be a screen itself, 
/// but it should not contain any further screens.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ToggleNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults = {};

/// <summary>
/// This node does not support being added to dashboards...
/// Toggle children should be added to dashboards instead.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.supportsDashboard = false;

/// <summary>
/// Toolbars are provided by children nodes...
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.supportsToolbar = false;

/// <summary>
/// If set to a node id, this property will set the initial tab that is active.
/// If ```null``` or unset, the first child node will be set as the active tab.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.initialTab = null;

/// <summary>
/// When ```true```, a tab bar is automatically added at the bottom of the ToggleNode
/// that allows the toggling of nodes.
///
/// By default this is ```true```.
///
/// Note: either the ```tabSelection```, ```listSelection``` or ```menuSelection``` must be enabled in order
/// to allow for a user to toggle between nodes.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.tabSelection = true;

/// <summary>
/// When ```true```, a select list is automatically added at the top of the ToggleNode
/// that allows the toggling of nodes.
///
/// By default this is ```false```. 
///
/// Can be combined with ```tabSelection: true```, however
/// the usability of having both tab selection and list selection is questionable.
///
/// Note: either the ```tabSelection```, ```listSelection``` or ```menuSelection``` must be enabled in order
/// to allow for a user to toggle between nodes.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.listSelection = false;


/// <summary>
/// When ```true```, a menu (tab menu) is automatically added to the node's menubar on the top of the node.
///
/// By default this is ```false```. 
///
/// Should NOT be combined with ```tabSelection: true```.
///
/// Note: either the ```tabSelection```, ```listSelection``` or ```menuSelection``` must be enabled in order
/// to allow for a user to toggle between nodes.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.menuSelection = false;

/// <summary>
/// When ```true```, a menu (tab menu) is displayed in it's soft version.
///
/// By default this is ```false```. 
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.useSoftTabMenuForMenuSelection = false;

/// <summary>
/// If defined, this array of translatable texts defines the toggle titles to use for the children
/// nodes. The array length must match that of the children number.
/// </summary>
/// <example>
/// ```
/// "toggleTitles": [
///     {
///         "resolved": "Child 1 Title"
///     },
///     {
///         "resolved": "Child 2 Title"
///     }
/// ]
/// ```
/// </example>
Machinata.Reporting.Node.ToggleNode.defaults.toggleTitles = null;


/// <summary>
/// When ```true```, this node's content is automatically exploded.
/// By default this is ```false```, but the ```webprint``` profile has a 
/// built-in override on this setting for ```true```.
/// </summary>
Machinata.Reporting.Node.ToggleNode.defaults.explodeChildren = false;

/// <summary>
/// Shows the current tab for ```tabId```, automatically updating any related toggle UI.
/// </summary>
Machinata.Reporting.Node.ToggleNode.showTab = function (instance, config, json, nodeElem, tabId) {
    // Init
    var childrensElem = nodeElem.data("children");
    var selectElem = nodeElem.data("select");
    var menuElem = nodeElem.data("menubar");
    var tabsElem = nodeElem.data("subtoolbar");
    // Hide others
    childrensElem.children().hide();
    if (tabsElem != null) tabsElem.find(".tab").removeClass("selected");
    if (menuElem != null) menuElem.find(".tab").removeClass("selected");
    // Show target node
    var targetNodeElem = childrensElem.find("[data-node-id=" + tabId+"]");
    targetNodeElem.find(".node-config-placeholder").append(nodeElem.data("config-elem"));
    targetNodeElem.show();
    if (targetNodeElem.length == 0) console.warn("Machinata.Reporting.Node.ToggleNode.showTab: tab "+tabId+" doesn't exist.");
    // Select tab
    if (tabsElem != null) tabsElem.find(".tab[data-target-id='" + tabId + "']").addClass("selected");
    // Select menu button
    if (menuElem != null) menuElem.find(".tab[data-target-id='" + tabId + "']").addClass("selected");
    // Select list option
    if (selectElem != null) {
        selectElem.val(tabId);
    }
    // Register
    json._state.currentTab = tabId;
    // Make sure childrens is visible
    childrensElem.show();
    // Call responsive update
    //Machinata.Responsive.updateResponsiveElements(); // deprecated
    // Call redraw
    //targetNodeElem.trigger("machinata-reporting-redraw-node");
    targetNodeElem.trigger("machinata-reporting-redraw-node-and-children"); // we now include all children, since they never got drawn in the first place
};
Machinata.Reporting.Node.ToggleNode.init = function (instance, config, json, nodeElem) {

    // Allow the toggle node?
    if (json.explodeChildren == true) {
        nodeElem.addClass("exploded");
        return;
    }

    // Init
    var self = this;

    // Bar selector?
    if (json.menuSelection == true) {
        nodeElem.data("menubar", Machinata.Reporting.buildMenubar(instance, { softMenuBar: json.useSoftTabMenuForMenuSelection }).appendTo(nodeElem));
        nodeElem.addClass("menu-selection");
    }

    // Childrens container
    var childrensElem = $("<div class='children'></div>").appendTo(nodeElem);
    childrensElem.hide();
    nodeElem.data("children", childrensElem);

    // List selector?
    if (json.listSelection == true) {
        var configurationElem = $("<div class='node-config machinata-reporting-config'></div>");
        var selectWrapperElem = $('<div class="machinata-reporting-select-wrapper"><select class="machinata-reporting-select"></select></div>');
        selectWrapperElem.append(Machinata.Reporting.buildIcon(instance,"chevron-down"));
        configurationElem.append(selectWrapperElem);
        var selectElem = selectWrapperElem.find("select");
        selectElem.on("change", function () {
            var targetId = selectElem.val();
            self.showTab(instance, config, json, nodeElem, targetId);
        });
        nodeElem.data("select", selectElem);
        nodeElem.data("config-elem", configurationElem);
        nodeElem.addClass("list-selection");
    }

    // Subtoolbar
    if (json.tabSelection == true) {
        // Create the sub tooltab for tabs
        var toolbarElem = Machinata.Reporting.buildToolbar(instance);
        toolbarElem.addClass("option-small");
        toolbarElem.addClass("machinata-reporting-subtoolbar");
        toolbarElem.addClass("toggle-toolbar");
        nodeElem.append(toolbarElem);
        nodeElem.data("subtoolbar", toolbarElem);
        nodeElem.addClass("tab-selection");
    }

    // Create all tabs...
    $.each(json.children, function (index, childJSON) {
        // Get title
        var toggleTitle = Machinata.Reporting.Text.resolve(instance,childJSON.toggleTitle);
        if (toggleTitle == null && json.toggleTitles != null) {
            // Try toggle node lookup
            if (json.toggleTitles.length >= index) {
                var titleFromParent = Machinata.Reporting.Text.resolve(instance,json.toggleTitles[index]);
                if (titleFromParent != null) toggleTitle = titleFromParent;
            }
        }
        if (toggleTitle == null) {
            // Fallback to mainTitle
            toggleTitle = Machinata.Reporting.Text.resolve(instance,childJSON.mainTitle);
        }
        // Add sub tab for this node
        if (json.tabSelection == true) {
            var tabElem = Machinata.Reporting.addSubTab(instance, nodeElem, toggleTitle, null, function (clickedElem) {
                var targetId = clickedElem.attr("data-target-id");
                self.showTab(instance, config, json, nodeElem, targetId);
            });
            tabElem.attr("data-target-id", childJSON.id);
        }
        // Menu item?
        if (json.menuSelection == true) {
            var buttonElem = Machinata.Reporting.addMenubarButton(instance, nodeElem, toggleTitle, function (clickedElem) {
                var targetId = clickedElem.attr("data-target-id");
                self.showTab(instance, config, json, nodeElem, targetId);
            }, { softMenuBar: json.useSoftTabMenuForMenuSelection });
            buttonElem.addClass("tab").attr("data-target-id", childJSON.id);
        }
        // Select option?
        if (json.listSelection == true) {
            nodeElem.data("select").append($("<option></option>").attr("value", childJSON.id).text(toggleTitle));
        }
        // Register the initial tab, if not manually set
        if (json.initialTab == null) {
            json.initialTab = childJSON.id;
        }
    });
};
Machinata.Reporting.Node.ToggleNode.postBuild = function (instance, config, json, nodeElem) {
    // Allow the toggle node?
    if (json.explodeChildren == true) {
        return;
    }

    // Automatically set the chrome (if not set manually)
    // This needs to be done after the children have been built, since we need to inherit defaults
    if (json.chrome == null) {
        var firstChildChrome = nodeElem.find(".machinata-reporting-node").first().attr("data-chrome");
        json.chrome = firstChildChrome;
        nodeElem.addClass("chrome-" + json.chrome);
        nodeElem.attr("chrome", json.chrome);
        if (nodeElem.data("subtoolbar") != null) nodeElem.data("subtoolbar").addClass("option-" + json.chrome);
    }
    // List selector?
    if (json.listSelection == true) {
        nodeElem.find(".children .machinata-reporting-node").each(function () {
            var childNodeElem = $(this);
            var childToolbarElem = childNodeElem.find(".machinata-reporting-toolbar");
            var configurationElem = $("<div class='node-config-placeholder'></div>").insertAfter(childToolbarElem);
        });
    }
    // Initial tab
    // BUG: Vega doesn't like this here, the bounds calculations are off
    // Moved to draw below...
    //this.showTab(instance, config, json, nodeElem, json.initialTab);
    //BUG: tabs with children that invoke a redraw will prematurely call the draw and thus the initial showTab before the entire children tree is built
    // Mark as built 
    json._state.childrenBuilt = true;
};
Machinata.Reporting.Node.ToggleNode.draw = function (instance, config, json, nodeElem) {
    // Allow the toggle node?
    if (json.explodeChildren == true) {
        return;
    }

    // Initial draw
    // FIX: If not tab has been displayed before, then we show the first at this stage...
    //BUG: tabs with children that invoke a redraw will prematurely call the draw and thus the initial showTab before the entire children tree is built
    if (json._state.currentTab == null && json._state.childrenBuilt == true) this.showTab(instance, config, json, nodeElem, json.initialTab);  
};








