
/// <summary>
/// Provides CommonJS module loading support
/// </summary>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

Machinata.Reporting = {};
if (typeof module === 'object' && typeof module.exports === 'object') {
	// CommonJS
	module.exports = Machinata.Reporting;
}