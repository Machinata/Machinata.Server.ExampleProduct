/// <summary>
/// This nodes provides various means for creating dynamic and fixed layouts. A LayoutNode
/// should act solely as a container, and not define any specific ```chrome``` or invoke a 
/// toolbar.
///
/// ## Dynamic System
/// With it's default dynamic behaviour, the LayoutNode drives the CSS stack to begin and 
/// dynamic block system where each child can define it's own style and size.
///
/// ## Slot System
/// When using the slot system, the LayoutNode's style defines a pre-defined layout where
/// each of the LayoutNode's children is assigned a slot and adjusted (layouted) according
/// to the layout. Each layout must be registered with the ```Machinata.Reporting.registerLayout```
/// function.
///
/// ## Registering Layouts
/// See ```Machinata.Reporting.registerLayout```.
/// </summary>
/// <type>class</type>
/// <example>
/// ### Dynamic System (without registration)
/// ```
/// {
///     "nodeType": "LayoutNode",
///     "children": [
///         {
///             "nodeType": "PlaceholderNode",
///             "style": "XYZ",
///             "mainTitle": {
///                 "resolved": "Left node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "style": "ABC",
///             "mainTitle": {
///                 "resolved": "Center node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "style": "123",
///             "mainTitle": {
///                 "resolved": "Right node"
///             }
///         }
///     ]
/// }
/// ```
/// ### Slot System (layout registration required)
/// ```
/// {
///     "nodeType": "LayoutNode",
///     "style": "CS_W_Overview_PanelTrailerSmall",
///     "slotChildren": {
///         "{left}": 0,
///         "{center}": 1,
///         "{right}": 2
///     },
///     "children": [
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Left node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Center node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Right node"
///             }
///         }
///     ]
/// }
/// ```
/// </example>
Machinata.Reporting.Node.LayoutNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LayoutNode.defaults = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LayoutNode.defaults.addStandardTools = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.LayoutNode.init = function (instance, config, json, nodeElem) {

    var LAYOUT_DEBUG_ENABLED = false; // will be deprecated

    // Init
    var nodeRequiresChildrenResort = false;
    var styleName = null;
    if (json.styles != null && json.styles.length > 0) styleName = json.styles[0];

    // Get layout
    if (LAYOUT_DEBUG_ENABLED) console.log(Machinata.Reporting._layouts);
    if (LAYOUT_DEBUG_ENABLED) console.log("searching layout " + json.style);
    var layoutJSON = null;
    if (styleName != null) layoutJSON = Machinata.Reporting.getLayout(styleName);
    if (layoutJSON != null) {
        if (LAYOUT_DEBUG_ENABLED) console.log("got layout " + layoutJSON.name);
    }

    // Flatten slotChildren into actual child
    // Note: we treat styles a bit special here, instead of overwriting if set by the layout, we add them (merge them in)
    $.each(json.children, function (index, childJSON) {
        // Find matching reference (by id) in slotChildren
        if (childJSON.additionalCSSClasses == null) childJSON.additionalCSSClasses = "";
        childJSON.additionalCSSClasses += " layout-child";
        $.each(json.slotChildren, function (key, value) {
            if (value == index) {
                // We have a match
                // Register directly in child json
                if (LAYOUT_DEBUG_ENABLED) console.log("found slot match " + key + " index " + value);
                childJSON.slotNumber = value;
                childJSON.slotKey = key;
                childJSON.slotClass = key.replace("{", "").replace("}", "");
                childJSON.additionalCSSClasses += " slot-child slot-" + childJSON.slotNumber + " slot-" + childJSON.slotClass;
                // Additional styles from self?
                if (childJSON.style != null) childJSON.additionalCSSClasses += " style-" + childJSON.style;
                if (childJSON.styles != null) {
                    for (var si = 0; si < childJSON.styles.length; si++) {
                        childJSON.additionalCSSClasses += " style-" + childJSON.styles[si];
                    }
                }
                // Merge in layout configuration
                if (layoutJSON != null) {
                    // Additional style from layout?
                    if (layoutJSON.style != null) childJSON.additionalCSSClasses += " style-" + layoutJSON.style;
                    if (layoutJSON.styles != null) {
                        for (var si = 0; si < layoutJSON.styles.length; si++) {
                            childJSON.additionalCSSClasses += " style-" + layoutJSON.styles[si];
                        }
                    }
                    // Find mathing slot
                    $.each(layoutJSON.children, function (layoutChildIndex, layoutChildJSON) {
                        if (LAYOUT_DEBUG_ENABLED) console.log("compare " + layoutChildJSON.slotKey + " w " + childJSON.slotKey);
                        if (layoutChildJSON.slotKey == childJSON.slotKey) {
                            // Match!
                            // Merge layoutChildJSON in our childJSON...
                            $.extend(childJSON, layoutChildJSON);
                            childJSON.slotIndex = layoutChildIndex;
                            if (layoutChildJSON.style != null) childJSON.additionalCSSClasses += " style-" + layoutChildJSON.style;
                            if (layoutChildJSON.styles != null) {
                                for (var si = 0; si < layoutChildJSON.styles.length; si++) {
                                    childJSON.additionalCSSClasses += " style-" + layoutChildJSON.styles[si];
                                }
                            }
                            if (LAYOUT_DEBUG_ENABLED) console.log("found matching layout child slot " + layoutChildJSON.slotKey);
                            if (LAYOUT_DEBUG_ENABLED) console.log("   final index is " + childJSON.slotIndex);
                            if (layoutChildIndex != index) {
                                if (LAYOUT_DEBUG_ENABLED) console.log("   index mismatch! will require resort");
                                nodeRequiresChildrenResort = true;
                            }
                        }
                    });
                }
                // Further properties...
                
                return;
            }
        });
    });

    // Resort, only if needed
    if (nodeRequiresChildrenResort == true) {
        json.children.sort(function (a, b) {
            return a.slotIndex > b.slotIndex;
        });
    }

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LayoutNode.postBuild = function (instance, config, json, nodeElem) {
    // For floating layouts we need this...
    nodeElem.append($("<div class='clear'></div>"));
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LayoutNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








