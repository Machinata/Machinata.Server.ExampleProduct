


/// <summary>
/// 
/// Note: This node should not be used in production.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.VegaTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaTestNode.defaults = {};

/// <summary>
/// We have a solid chrome by default.
/// </summary>
Machinata.Reporting.Node.VegaTestNode.defaults.chrome =  "solid";



/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaTestNode.getVegaSpec = function (instance, config, json, nodeElem) {
    
    return {
        "data": [
            {
                "name": "table",
                "values": [
                    { "x": 0, "y": 28000000, "c": 0 }, { "x": 0, "y": 55000000, "c": 1 },
                    { "x": 1, "y": 43000000, "c": 0 }, { "x": 1, "y": 91000000, "c": 1 },
                    { "x": 2, "y": 81000000, "c": 0 }, { "x": 2, "y": 53000000, "c": 1 },
                    { "x": 3, "y": 19000000, "c": 0 }, { "x": 3, "y": 87000000, "c": 1 },
                    { "x": 4, "y": 52000000, "c": 0 }, { "x": 4, "y": 48000000, "c": 1 },
                    { "x": 5, "y": 24000000, "c": 0 }, { "x": 5, "y": 49000000, "c": 1 },
                    { "x": 6, "y": 87000000, "c": 0 }, { "x": 6, "y": 66000000, "c": 1 },
                    { "x": 7, "y": 17000000, "c": 0 }, { "x": 7, "y": 27000000, "c": 1 },
                    { "x": 8, "y": 68000000, "c": 0 }, { "x": 8, "y": 16000000, "c": 1 },
                    { "x": 9, "y": 49000000, "c": 0 }, { "x": 9, "y": 15000000, "c": 1 }
                ],
                "transform": [
                    {
                        "type": "stack",
                        "groupby": ["x"],
                        "sort": { "field": "c" },
                        "field": "y"
                    }
                ]
            }
        ],

        "scales": [
            {
                "name": "x",
                "type": "band",
                "range": "width",
                "domain": { "data": "table", "field": "x" }
            },
            {
                "name": "y",
                "type": "linear",
                "range": "height",
                "nice": true, "zero": true,
                "domain": { "data": "table", "field": "y1" }
            },
            {
                "name": "color",
                "type": "ordinal",
                "range": "category",
                "domain": { "data": "table", "field": "c" }
            }
        ],

        "axes": [
            {
                "orient": "bottom",
                "scale": "x",
                "zindex": 1
            },
            {
                "orient": "left",
                "scale": "y",
                "zindex": 1,
                "translate": 0
            }
        ],

        "marks": [
            {
                "type": "rect",
                "from": { "data": "table" },
                "encode": {
                    "enter": {
                        "x": { "scale": "x", "field": "x" },
                        "width": { "scale": "x", "band": 1, "offset": -1 },
                        "y": { "scale": "y", "field": "y0" },
                        "y2": { "scale": "y", "field": "y1" },
                        "fill": { "scale": "color", "field": "c" }
                    },
                    "update": {
                        "fillOpacity": { "value": 1 }
                    },
                    "hover": {
                        "fillOpacity": { "value": 0.5 }
                    }
                }
            }
        ]
    };
};
Machinata.Reporting.Node.VegaTestNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};
Machinata.Reporting.Node.VegaTestNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.VegaTestNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.VegaTestNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







