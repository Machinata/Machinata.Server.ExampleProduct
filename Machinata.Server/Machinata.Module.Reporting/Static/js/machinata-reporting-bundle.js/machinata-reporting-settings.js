

/// <summary>
/// User settings abstraction layer.
/// Different provider implementations can be used.
/// The provider can be set using the configuration ```Machinata.Reporting.Config.userSettingsProvider```.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.UserSettings = {};


/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.UserSettings._getImpl = function (instance) {
    var impl = Machinata.Reporting.UserSettings.Providers[instance.config.userSettingsProvider];
    if (impl == null) throw "The user settings provider " + instance.config.userSettingsProvider + " does not exist.";
    return impl;
};

/// <summary>
/// Gets a global value for a user for the given key. Such global settings span all reports, nodes and alter the behaviour of the reporting
/// across all reports.
/// This method forwards to the currently configured providers implementation.
/// Recommended storage key: ```global_{key}```
/// </summary>
Machinata.Reporting.UserSettings.getGlobalValue = function (instance, key) {
    return Machinata.Reporting.UserSettings._getImpl(instance).getGlobalValue(instance, key);
};

/// <summary>
/// Sets a global value for a user for the given key. 
/// This method forwards to the currently configured providers implementation.
/// Values can be any object.
/// Recommended storage key: ```global_{key}```
/// </summary>
Machinata.Reporting.UserSettings.setGlobalValue = function (instance, key, val) {
    Machinata.Reporting.UserSettings._getImpl(instance).setGlobalValue(instance, key, val);
};

/// <summary>
/// Gets a report specific value for a user for the given key. 
/// This method forwards to the currently configured providers implementation.
/// Recommended storage key: ```report_{instance.reportData.reportId}_{key}```
/// Note: A loaded report can be assumed.
/// </summary>
Machinata.Reporting.UserSettings.getReportValue = function (instance, key) {
    if (instance.reportData == null || instance.reportData.reportId == null) throw "Machinata.Reporting.UserSettings.getGlobalValue: instance.reportData.reportId cannot be null!";
    return Machinata.Reporting.UserSettings._getImpl(instance).getReportValue(instance, key);
};

/// <summary>
/// sets a report specific value for a user for the given key. 
/// This method forwards to the currently configured providers implementation.
/// Values can be any object.
/// Recommended storage key: ```report_{instance.reportData.reportId}_{key}```
/// Note: A loaded report can be assumed.
/// </summary>
Machinata.Reporting.UserSettings.setReportValue = function (instance, key, val) {
    if (instance.reportData == null || instance.reportData.reportId == null) throw "Machinata.Reporting.UserSettings.setGlobalValue: instance.reportData.reportId cannot be null!";
    Machinata.Reporting.UserSettings._getImpl(instance).setReportValue(instance, key, val);
};

/// <summary>
/// Gets a node specific value for a user for the given key and nodeJSON.
/// This method forwards to the currently configured providers implementation.
/// Recommended storage key: ```node_{nodeJSON.id}_{key}```
/// Note: A loaded report cannot be assumed - it is possible that nodes are displayed without a report (containerless).
/// </summary>
Machinata.Reporting.UserSettings.getNodeValue = function (instance, nodeJSON, key) {
    if (nodeJSON == null || nodeJSON.id == null) throw "Machinata.Reporting.UserSettings.getNodeValue: nodeJSON.id cannot be null!";
    return Machinata.Reporting.UserSettings._getImpl(instance).getNodeValue(instance, key);
};

/// <summary>
/// Sets a node specific value for a user for the given key and nodeJSON.
/// This method forwards to the currently configured providers implementation.
/// Recommended storage key: ```node_{nodeJSON.id}_{key}```
/// Note: A loaded report cannot be assumed - it is possible that nodes are displayed without a report (containerless).
/// </summary>
Machinata.Reporting.UserSettings.setNodeValue = function (instance, nodeJSON, key, val) {
    if (nodeJSON == null || nodeJSON.id == null) throw "Machinata.Reporting.UserSettings.setNodeValue: nodeJSON.id cannot be null!";
    return Machinata.Reporting.UserSettings._getImpl(instance).setNodeValue(instance, key, val);
};


/// <summary>
/// Namespace for all data provider implementations.
/// 
/// Each provider must implement at least the following:
///  - ```setGlobalValue(instance, key, val)```
///  - ```getGlobalValue(instance, key)```
///  - ```setReportValue(instance, key, val)```
///  - ```getReportValue(instance, key)```
///  - ```setNodeValue(instance, key, val)```
///  - ```getNodeValue(instance, key)```
/// 
/// Providers can be registered via a object set on ```Machinata.Reporting.UserSettings.Providers```.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.UserSettings.Providers.MyProvider = {
///    setGlobalValue: function(instance, key, val) {
///        // TODO
///    }
///    getGlobalValue: function(instance, key) {
///        // TODO
///        return null;
///    }
///    ...
/// };
/// ```
/// </example>
/// <type>namespace</type>
Machinata.Reporting.UserSettings.Providers = {};



/// <summary>
/// A user settings storage impelementation using the browsers web storage API (via Machinata.setCache).
/// This provider can be used for basic functionality, but cannot be relied upon for saving critical data, as the 
/// data is stored to the user's browser session without any gaurantee.
/// Note that on most user environments, browser storage is not cross-device.
/// </summary>
/// <type>class</type>
Machinata.Reporting.UserSettings.Providers.WebStorage = {};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.setGlobalValue = function (instance, key, val) {
    Machinata.setCache("reporting_"+key, val);
};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.getGlobalValue = function (instance, key) {
    return Machinata.getCache("reporting_" + key);
};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.setReportValue = function (instance, key, val) {
    Machinata.Reporting.UserSettings.Providers.WebStorage.setGlobalValue(instance, "report_" + instance.reportData.reportId+"_"+key, val);
};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.getReportValue = function (instance, key) {
    return Machinata.Reporting.UserSettings.Providers.WebStorage.getGlobalValue(instance, "report_" + instance.reportData.reportId + "_" + key);
};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.setNodeValue = function (instance, nodeJSON, key, val) {
    Machinata.Reporting.UserSettings.Providers.WebStorage.setGlobalValue(instance, "node_" + instance.reportData.reportId + "_" + key, val);
};

/// <summary>
/// </summary>
Machinata.Reporting.UserSettings.Providers.WebStorage.getNodeValue = function (instance, nodeJSON, key) {
    return Machinata.Reporting.UserSettings.Providers.WebStorage.getGlobalValue(instance, "node_" + instance.reportData.reportId + "_" + key);
};






