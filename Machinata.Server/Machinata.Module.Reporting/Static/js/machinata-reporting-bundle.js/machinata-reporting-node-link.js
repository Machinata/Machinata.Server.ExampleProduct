


/// <summary>
/// Node for displaying a link to a external or embedded resource.
/// Link Nodes don't have toolbars, and are used to supplement a visual node (as a child) as a trailer.
/// The node's mainTitle defines the text to display.
/// A Link Node must have either a ```attachment``` (embedded resource) or a ```link```.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.LinkNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults = {};


/// <summary>
/// By default LinkNode have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.supportsDashboard = false;

/// <summary>
/// No tools for LinkNode
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.addStandardTools = false;

/// <summary>
/// No toolbar for LinkNode
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.supportsToolbar = false;

/// <summary>
/// Marks the attachment as a file download.
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.downloadAttachmentToBrowser = false;


/// <summary>
/// The attachment JSON asset object that contains the data for the attachment. 
/// </summary>
/// <example>
/// ```
/// "attachment": {
///     "filename": "CS_PaperReport_long-v2.pdf",
///     "mimeType": "application/pdf",
///     "encoding": "base64",
///     "data": "JVBERi0xLjcNJeLjz9MNCjE2MyAwIG9iag08PC9MaW5lYXJ..."
/// }
/// ```
/// </example>
Machinata.Reporting.Node.LinkNode.defaults.attachment = null;

/// <summary>
/// The link JSON for the external resource. See Machinata.Reporting.Node.Defaults.link.
/// </summary>
Machinata.Reporting.Node.LinkNode.defaults.link = null;


/// <summary>
/// </summary>
Machinata.Reporting.Node.LinkNode.init = function (instance, config, json, nodeElem) {
    // Container
    var linkElem = $("<a class='machinata-reporting-button option-link'></a>");
    linkElem.text(Machinata.Reporting.Text.resolve(instance,json.mainTitle));
    if (json.attachment != null) {
        linkElem.attr("title", json.attachment.filename);
        if (json.downloadAttachmentToBrowser == true) {
            linkElem.append(Machinata.Reporting.buildIcon(instance,"download"));
        } else {
            linkElem.append(Machinata.Reporting.buildIcon(instance,"arrow-right"));
        }
    } else {
        if(json.link.tooltip != null) linkElem.attr("title", Machinata.Reporting.Text.resolve(instance,json.link.tooltip));
        linkElem.append(Machinata.Reporting.buildIcon(instance,"arrow-right"));
    }

    // Link interaction
    linkElem.click(function (event) {
        event.preventDefault();
        if (json.attachment != null) {
            // Attachment
            if (json.downloadAttachmentToBrowser == true) {
                Machinata.Data.exportDataAsBlob(
                    json.attachment.data,
                    json.attachment.mimeType,
                    json.attachment.filename
                );
            } else {
                Machinata.Data.openDataInNewWindow(
                    json.attachment.data,
                    json.attachment.mimeType,
                    json.attachment.filename
                );
            }
        } else {
            // Link
            Machinata.Reporting.Tools.openLink(instance,json.link);
        }
    });
    
    nodeElem.append(linkElem);


};
Machinata.Reporting.Node.LinkNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








