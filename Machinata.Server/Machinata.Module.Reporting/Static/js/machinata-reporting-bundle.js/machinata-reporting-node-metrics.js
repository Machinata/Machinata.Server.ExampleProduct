/// <summary>
/// A generic node for displaying different formats of metrics (key/values) in flexible
/// grid or tabular formats. Items in the metrics node can be grouped (for example to create
/// a grid of items where each group becomes a row). Metrics nodes always build the items the same,
/// but allow for CSS to steer exactly how they are displayed to the user, and more importantly
/// allow for dynamic layouts that adjust to the screen size (for example, collapsing a 3x3 grid into a 3x9
/// layout).
///
/// ## Metrics Items
/// Each group (in ```itemGroups```) contains items with the following properties:
///  - ```label```: resolvable text that labels this item
///  - ```value```: resolvable text representing the item value
///  - ```subValue```: resolvable text representing a additional value (usually for comparison)
///  - ```subDelta```: delta object that shows a difference value, typically colored with an arrow icon
///  - ```alert```: alert object that shows a either a alert flag or a alert icon
///  - ```number```: if defined, the item will automatically include the number
///  - ```date```: if defined, the item will automatically generate a calendar stub, Unix Epoch Milliseconds format 
///  - ```icon```: if defined, the item will automatically generate a icon stub, registered icon (string), see ```Machinata.Reporting.icon()```
///  - ```link```: if defined, the item will clickable and display a link arrow, link JSON object (See Machinata.Reporting.Tools.openLink for more details) 
/// 
/// An item's design usually revolves around the item value. This means that in most cases the value will be
/// the largest component.
/// 
/// ## Numbering
/// To create a numbered list, just set the ```number``` property for each item.
/// 
/// ## Delta Object
/// Format for specifying a difference value. Can have the following properties:
///  - ```label```: resolvable text that labels this delta
///  - ```icon```: registered icon (string), typically ```trend-up``` or ```trend-down```
///  - ```color```: registered color (string), typically ```green``` or ```red```
/// 
/// ## Alert Object
/// Format for specifying a item alert. Either a flag is displayed or an icon is displayed.
/// In most cases specifying a color is not necessary.
/// Can have the following properties:
///  - ```label```: resolvable text that labels this alert (usually as a tooltip), optional
///  - ```flag```: boolean indicating to show a alert flag
///  - ```icon```: registered icon (string), typically ```alert``` or ```info```
///  - ```color```: registered color (string), typically ```green``` or ```red```, optional
///
/// </summary>
/// <example>
/// ## Overview Example with Deltas
/// ```
/// {
///     "nodeType": "MetricsNode",
///     "style": "CS_W_Block2x2",
///     "chrome": "dark",
///     "mainTitle": {
///         "resolved":  "Mandate information"
///     },
///     "subTitle": {
///         "resolved":  "Fixed income active"
///     },
///     "itemGroups": [
///         {
///             "items": [
///                 {
///                     "label": { "resolved":  "Investment Grade Global II" },
///                     "value": { "resolved":  "153.28m" },
///                     "subLabel": { "resolved":  "Total Assets in USD" }
///                 }
///             ]
///         },
///         {
///             "items": [
///                 {
///                     "label": { "resolved": "MTD return" },
///                     "value": { "resolved": "2.06%" },
///                     "subValue": { "resolved": "bench 2.19%" }
///                     "subDelta": {
///                         "icon": "trend-up",
///                         "color": "green",
///                         "label": { "resolved": "delta 0.41%" }
///                     }
///                 },
///                 {
///                     "label": { "resolved":  "YTD return" },
///                     "value": { "resolved":  "4.55%" },
///                     "subValue": { "resolved":  "bench 6.19%" }
///                     "subDelta": {
///                         "icon": "trend-up",
///                         "color": "red",
///                         "label": { "resolved": "delta 0.66%" }
///                     }
///                 }
///             ]
///         },
///         {
///             "items": [
///                 {
///                     "label": { "resolved": "Average duration, years" },
///                     "value": { "resolved": "5.8" }
///                 },
///                 {
///                     "label": { "resolved":  "Average rating, S&P" },
///                     "value": { "resolved":  "A+" }
///                 }
///             ]
///         }
///     ]
/// }
/// ```
/// </example>
/// <type>class</type>
Machinata.Reporting.Node.MetricsNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults = {};

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// By default MetricsNodes don't have the standard tools.
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults.addStandardTools = false;

/// <summary>
/// By default MetricsNodes have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults.chrome = "solid";

/// <summary>
/// If ```true```, the user can easily copy the text to clipboard.
/// By default ```true```.
/// Note: Experimental
/// </summary>
Machinata.Reporting.Node.MetricsNode.defaults.addCopyToClipboardTool = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.createDeltaElement = function (instance, config, deltaJSON, className) {
    var elem = $("<div class='delta'></div>").addClass(className);
    if (deltaJSON.color != null) elem.addClass("color-" + deltaJSON.color);
    if (deltaJSON.label != null) {
        var text = Machinata.Reporting.Text.resolve(instance,deltaJSON.label);
        elem.append($("<span class='delta-label'></span>").text(text));
    }
    if (deltaJSON.icon != null) {
        //elem.append($("<span class='icon'></span>").addClass(Machinata.Reporting.icon(deltaJSON.icon)));
        elem.append(Machinata.Reporting.buildIcon(instance,deltaJSON.icon).addClass("delta-icon"));
    }
    return elem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.createAlertElement = function (instance, config, alertJSON, className) {
    var elem = $("<div class='alert'></div>").addClass(className);
    if (alertJSON.icon != null) {
        elem.addClass("option-icon");
        //elem.append($("<span class='icon'></span>").addClass(Machinata.Reporting.icon(alertJSON.icon)));
        elem.append(Machinata.Reporting.buildIcon(instance,alertJSON.icon));
    } else {
        elem.addClass("option-flag");
    }
    if (alertJSON.color != null) elem.addClass("color-" + alertJSON.color);
    if (alertJSON.label != null) elem.attr("title", Machinata.Reporting.Text.resolve(instance,alertJSON.label));
    return elem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.buildGrid = function (instance, config, json, containerElem) {
    // Container
    if (containerElem == null) containerElem = $("<div></div>");

    // Pre-process
    var maxColumns = 0;
    var maxRows = 0;
    var node_hasIcon = false;
    var node_hasDate = false;
    var node_hasSpan = false;
    var node_hasLink = false;
    var node_hasAlert = false;
    var node_hasValue = false;
    var node_hasLabel = false;
    var node_hasSubValue = false;
    var node_hasSubLabel = false;
    var node_hasSubDelta = false;
    var node_hasNumber = false;
    var node_isKeyValue = true;
    var layout = "";
    $.each(json.itemGroups, function (index, groupJSON) {
        var groupLayout = "1x" + groupJSON.items.length;
        $.each(groupJSON.items, function (index, itemJSON) {
            if (groupJSON.items.length > maxColumns) maxColumns = groupJSON.items.length;
        });
        layout += "-" + groupLayout;
    });
    maxRows = json.itemGroups.length;

    var itemsElem = $("<table class='items'></table>");
    var groupNumber = 0;
    var itemsCount = 1;
    $.each(json.itemGroups, function (index, groupJSON) {
        groupNumber++;
        var groupElem = $("<td class='group'></td>").addClass("group-" + groupNumber).appendTo(itemsElem);
        var itemNumber = 0;
        groupElem.addClass("layout-1x" + groupJSON.items.length);
        $.each(groupJSON.items, function (index, itemJSON) {
            // Create item

            var item_hasIcon = false;
            var item_hasDate = false;
            var item_hasSpan = false;
            var item_hasLink = false;
            var item_hasAlert = false;
            var item_hasValue = false;
            var item_hasLabel = false;
            var item_hasSubValue = false;
            var item_hasSubLabel = false;
            var item_hasSubDelta = false;
            var item_hasNumber = false;
            var item_isKeyValue = true;
            itemNumber++;
            var itemElem = $("<td class='item'></td>").addClass("item-" + itemNumber);

            // Link
            if (itemJSON.link != null) {
                item_hasLink = true;
                node_hasLink = true;
                itemElem.addClass("is-link");
                //var elem = $("<div class='link-icon'></div>").addClass(Machinata.Reporting.icon("arrow-right"));
                var elem = Machinata.Reporting.buildIcon(instance,"arrow-right").addClass("link-icon");
                itemElem.click(function () {
                    Machinata.Reporting.Tools.openLink(instance,itemJSON.link);
                });
                //itemElem.append($("<div class='link-bg'></div>"));
                itemElem.append(elem);
            }

            // UIs
            if (itemJSON.alert != null) {
                item_hasAlert = true;
                node_hasAlert = true;
                var elem = Machinata.Reporting.Node.MetricsNode.createAlertElement(instance, config, itemJSON.alert, null);
                itemElem.append(elem);
            }
            if (itemJSON.icon != null) {
                item_hasNumber = true;
                node_hasNumber = true;
                //var elem = $("<div class='icon tag'></div>").addClass(Machinata.Reporting.icon(itemJSON.icon));
                //var elem = Machinata.Reporting.buildIcon(instance,itemJSON.icon).addClass("tag");
                var elem = $("<div class='icon tag'></div>").append(Machinata.Reporting.buildIcon(instance,itemJSON.icon));
                itemElem.append(elem);
            }
            if (itemJSON.number != null) {
                item_hasIcon = true;
                node_hasIcon = true;
                var elem = $("<div class='number theme-number-font tag'></div>");
                elem.text(itemJSON.number);
                itemElem.append(elem);
            }
            if (itemJSON.date != null) {
                item_hasDate = true;
                node_hasDate = true;
                var elem = $("<div class='date tag'></div>");
                var monthText = d3.timeFormat("%b")(itemJSON.date);
                var dayText = d3.timeFormat("%d")(itemJSON.date);
                elem.append($("<div class='month'></div>").text(monthText));
                elem.append($("<div class='day'></div>").text(dayText));
                itemElem.append(elem);
            }

            // Texts
            if (itemJSON.label != null) {
                item_hasLabel = true;
                node_hasLabel = true;
                var text = Machinata.Reporting.Text.resolve(instance,itemJSON.label);
                itemElem.append($("<div class='label'></div>").text(text));
            }
            if (itemJSON.value != null) {
                item_hasValue = true;
                node_hasValue = true;
                var text = Machinata.Reporting.Text.resolve(instance,itemJSON.value);
                itemElem.append($("<div class='value theme-number-font'></div>").text(text));
            }
            if (itemJSON.subValue != null) {
                item_hasSubValue = true;
                node_hasSubValue = true;
                var text = Machinata.Reporting.Text.resolve(instance,itemJSON.subValue);
                itemElem.append($("<div class='subValue theme-number-font'></div>").text(text));
            }
            if (itemJSON.subLabel != null) {
                item_hasSubLabel = true;
                node_hasSubLabel = true;
                var text = Machinata.Reporting.Text.resolve(instance,itemJSON.subLabel);
                itemElem.append($("<div class='subLabel'></div>").text(text));
            }
            if (itemJSON.subDelta != null) {
                item_hasSubDelta = true;
                node_hasSubDelta = true;
                itemElem.addClass("has-subdelta");
                var elem = Machinata.Reporting.Node.MetricsNode.createDeltaElement(instance, config, itemJSON.subDelta, "subdelta");
                itemElem.append(elem);
            }

            itemElem.append($("<div class='line'></div>"));

            // Helper classes
            item_isKeyValue = (item_hasValue == true && item_hasLabel == true);
            if (item_isKeyValue == true) itemElem.addClass("is-key-value");
            else itemElem.addClass("not-key-value");
            if (item_hasAlert == true) itemElem.addClass("has-alert");
            else itemElem.addClass("no-alert");
            if (item_hasValue == true) itemElem.addClass("has-value");
            else itemElem.addClass("no-value");
            if (item_hasLabel == true) itemElem.addClass("has-label");
            else itemElem.addClass("no-label");
            if (item_hasSubValue == true) itemElem.addClass("has-subvalue");
            else itemElem.addClass("no-subvalue");
            if (item_hasSubLabel == true) itemElem.addClass("has-sublabel");
            else itemElem.addClass("no-sublabel");
            if (item_hasSubDelta == true) itemElem.addClass("has-subdelta");
            else itemElem.addClass("no-subdelta");
            if (item_hasNumber == true) itemElem.addClass("has-number");
            else itemElem.addClass("no-number");
            if (item_hasDate == true) itemElem.addClass("has-date");
            else itemElem.addClass("no-date");
            if (item_hasIcon == true) itemElem.addClass("has-icon");
            else itemElem.addClass("no-icon");

            itemElem.appendTo(groupElem);

            if (item_isKeyValue == false) node_isKeyValue = false;

        });

        // Do we need a virtual column span?
        if (groupJSON.items.length < maxColumns && groupJSON.items.length == 1) {
            groupElem.find(".item").attr("colspan", 10).addClass("option-spanned"); //TODO: why is colspan set to 10?
            node_hasSpan = true;
        }
    });

    // Helper classes
    if (node_hasSpan == true) {
        containerElem.addClass("has-span");
    } else {
        itemsElem.addClass("is-grid");
        itemsElem.addClass("grid-" + maxColumns + "x" + maxRows);
    }
    itemsElem.addClass("max-columns-" + maxColumns);
    itemsElem.addClass("max-rows-" + maxRows);
    if (node_isKeyValue == true) itemsElem.addClass("is-key-value");
    else itemsElem.addClass("not-key-value");
    if (node_hasAlert == true) itemsElem.addClass("has-alert");
    else itemsElem.addClass("no-alert");
    if (node_hasValue == true) itemsElem.addClass("has-value");
    else itemsElem.addClass("no-value");
    if (node_hasLabel == true) itemsElem.addClass("has-label");
    else itemsElem.addClass("no-label");
    if (node_hasSubValue == true) itemsElem.addClass("has-subvalue");
    else itemsElem.addClass("no-subvalue");
    if (node_hasSubLabel == true) itemsElem.addClass("has-sublabel");
    else itemsElem.addClass("no-sublabel");
    if (node_hasSubDelta == true) itemsElem.addClass("has-subdelta");
    else itemsElem.addClass("no-subdelta");
    if (node_hasNumber == true) itemsElem.addClass("has-number");
    else itemsElem.addClass("no-number");
    if (node_hasDate == true) itemsElem.addClass("has-date");
    else itemsElem.addClass("no-date");
    if (node_hasIcon == true) itemsElem.addClass("has-icon");
    else itemsElem.addClass("no-icon");

    containerElem.addClass("layout" + layout);
    containerElem.addClass("items-count-" + itemsCount);
    containerElem.append(itemsElem);

    return containerElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.buildTable = function (instance, config, json) {
    
    var itemsElem = $("<table></table>");
    $.each(json.itemGroups, function (index, groupJSON) {
        var itemNumber = 0;
        $.each(groupJSON.items, function (index, itemJSON) {
            // Create item
            var itemElem = $("<tr></tr>");

            // Link
            if (itemJSON.link != null) {

            }

            // UIs
            if (itemJSON.alert != null) {
                
            }
            if (itemJSON.icon != null) {
                
            }
            if (itemJSON.number != null) {
                $("<td></td>").text(itemJSON.number).appendTo(itemElem);
            }
            if (itemJSON.date != null) {
                var monthText = d3.timeFormat("%b")(itemJSON.date);
                var dayText = d3.timeFormat("%d")(itemJSON.date);
                $("<td></td>").text(monthText + " " + dayText).appendTo(itemElem);
            }

            // Texts
            if (itemJSON.label != null){
                $("<td></td>").text(Machinata.Reporting.Text.resolve(instance,itemJSON.label)).appendTo(itemElem);
            }
            if (itemJSON.value != null){
                $("<td></td>").text(Machinata.Reporting.Text.resolve(instance,itemJSON.value)).appendTo(itemElem);
            }
            if (itemJSON.subLabel != null){
                $("<td></td>").text(Machinata.Reporting.Text.resolve(instance,itemJSON.subLabel)).appendTo(itemElem);
            }
            if (itemJSON.subValue != null){
                $("<td></td>").text(Machinata.Reporting.Text.resolve(instance,itemJSON.subValue)).appendTo(itemElem);
            }
            if (itemJSON.subDelta != null){
                $("<td></td>").text(Machinata.Reporting.Text.resolve(instance,itemJSON.subDelta.label)).appendTo(itemElem);
            }
             
            itemElem.appendTo(itemsElem);
        });
    });

    return itemsElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.init = function (instance, config, json, nodeElem) {
    var self = this;

    // Container  
    var faderElem = $("<div class='bottom-fader option-overflow-only option-bottom-content '></div>").appendTo(nodeElem);
    var containerElem = $("<div class='sizer node-bg option-fullsize-content'></div>").appendTo(nodeElem);

    // Grid
    Machinata.Reporting.Node.MetricsNode.buildGrid(instance, config, json, containerElem);

    // Tools
    if (json.addCopyToClipboardTool == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.copy-to-clipboard"), "copy", function () {
            var tempGridElem = self.buildTable(instance, config, json);
            Machinata.Reporting.Tools.copyElementToClipboard(tempGridElem, Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied.grid'), true);
        });
    }

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MetricsNode.draw = function (instance, config, json, nodeElem) {
    // Overflow?
    var sizerElem = nodeElem.find(".sizer");
    var itemsElem = nodeElem.find(".items");
    if (itemsElem.height() > sizerElem.height()) {
        nodeElem.addClass("is-overflowing");
    } else {
        nodeElem.removeClass("is-overflowing");
    }
};








