


/// <summary>
/// A user-customizable dashboard consisting of multiple child nodes.
/// Uses LayoutNode as the underlying layout driver, but adds a layer
/// of dashboard board functionality over the top.
/// Note: NOT FOR PRODUCTION USE (current POC)
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.DashboardNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DashboardNode.defaults = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DashboardNode.defaults.addStandardTools = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.DashboardNode.init = function (instance, config, json, nodeElem) {
    Machinata.Reporting.Node.LayoutNode.init(instance, config, json, nodeElem);
    nodeElem.addClass("type-LayoutNode");
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DashboardNode.postBuild = function (instance, config, json, nodeElem) {
    Machinata.Reporting.Node.LayoutNode.postBuild(instance, config, json, nodeElem);

    
    // Initialize Packery
    var grid = nodeElem.packery({
        itemSelector: '.layout-child',
        gutter: 0,
        columnWidth: 0,
        rowHeight: 0,
        transitionDuration: 0 // for the first transition we dont want an animation
    });

    // Make all items draggable
    var items = grid.find('.layout-child');
    items.draggable({
        stop: function () {
            // When the user finishes a drag, we need to
            nodeElem.packery('layout');
        }
    });

    // Bind drag events to Packery
    grid.packery('bindUIDraggableEvents', items);

    // Bind events
    grid.on('layoutComplete', function (event, items) {
    });
    grid.on('dragItemPositioned', function (event, draggedItem) {
    });
    grid.on('fitComplete', function (event, item) {
    });
    grid.on('removeComplete', function (event, removedItems) {
    });

    // Initial packery update
    // Note: to ensure that the browser dom building has enough time, we split
    // this out to a different thread task via a timeout
    setTimeout(function () {
        nodeElem.packery('layout');
        nodeElem.packery({
            transitionDuration: '0.4s'
        });
    }, 1);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DashboardNode.draw = function (instance, config, json, nodeElem) {
    Machinata.Reporting.Node.LayoutNode.draw(instance, config, json, nodeElem);

    // Further packery updates
    // Note: to ensure that the browser dom resizing has enough time, we split
    // this out to a different thread task via a timeout
    setTimeout(function () {
        nodeElem.packery('layout');
    }, 1);
};

















