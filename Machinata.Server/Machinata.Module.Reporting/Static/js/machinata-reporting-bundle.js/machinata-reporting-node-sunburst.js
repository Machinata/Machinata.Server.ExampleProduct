


/// <summary>
/// </summary>
/// <example>
/// See ```example-report-packs.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.LineColumnNode</inherits>
Machinata.Reporting.Node.SunburstNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults = {};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.insertLegend = true;


/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.legendColumns = 3;

/// <summary>
/// Sets the correct legend data source for the automatic legend.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.legendDataSource = "factsLegend";

Machinata.Reporting.Node.SunburstNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return {
        "data": [
          {
              "name": "facts",
              "values": null
          },
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          },
          {
              "name": "factsPartitioned",
              "source": "facts",
              "transform": [
                {
                    "type": "stratify",
                    "key": "id",
                    "parentKey": "parent"
                },
                {
                    "type": "partition",
                    "field": json.weightingKey,
                    "sort": {"field": json.weightingKey},
                    "size": [{ "signal": "2 * PI" }, { "signal": "min(width,height) / 2" }],
                    "as": ["a0", "r0", "a1", "r1", "depth", "children"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "factsPartitioned",
              "transform": [
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum." + json.shadingKey
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "colorResolved",
                    "expr": "datum.depth == 0 ? scale('grayByShade','gray3') : ( datum.depth == 1 ? scale('color',datum.id) :  scale('color',datum.parent) )  "
                },
              ]
          },
          {
              "name": "factsLegend",
              "source": "factsResolved",
              "transform": [
                    { "type": "filter", "expr": "datum.depth == 1" },
                    {
                        "type": "aggregate",
                        "groupby": [json.shadingKey, "colorResolved", "key"],
                    }

              ]
          }
        ],
        "signals": [
            
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "factsPartitioned", "field": json.shadingKey },
                "range": { "scheme": json.theme }
            },
            {
                "name": "colorFill",
                "type": "quantize",
                "domain": [0,1],
                "range": { "scheme": "infographic" },
                "reverse": true
            },
            {
                "name": "legendColor",
                "type": "ordinal",
                "domain": { "data": "factsLegend", "field": "key" },
                "range": { "data": "factsLegend", "field": "colorResolved" },
            },
            {
                "name": "legendLabel",
                "type": "ordinal",
                "domain": { "data": "factsLegend", "field": "key" },
                "range": { "data": "factsLegend", "field": json.shadingKey }
            },
            {
                "name": "grayColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "id" },
                "range": { "scheme": "gray" }
            },
            {
                "name": "infographicColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "id" },
                "range": { "scheme": "infographic" }
            },
            {
                "name": "colorByShade",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": json.theme }
            },
            {
                "name": "grayByShade",
                "type": "ordinal",
                "domain": { "data": "grayShades", "field": "data" },
                "range": { "scheme": "gray" }
            },
        ],
        "marks": [
            {
                "type": "arc",
                "from": { "data": "factsResolved" },
                "encode": {
                    "enter": {
                        "x": { "signal": "width / 2" },
                        "y": { "signal": "height / 2" },
                        "fill": { "signal": "datum.colorResolved" },

                        "opacity": { "signal": "datum.depth == 0 ? 0 : datum.depth == 1 ? 1.0 : 1/datum.depth" }
                        //"tooltip": { "signal": "datum.name + (datum.size ? ', ' + datum.size + ' bytes' : '')" }
                    },
                    "update": {
                        "startAngle": { "field": "a0" },
                        "endAngle": { "field": "a1" },
                        "innerRadius": { "field": "r0" },
                        "outerRadius": { "field": "r1" },
                        "stroke": { "value": "white" },
                        "strokeWidth": { "value": 1 },
                        "zindex": { "value": 0 },
                    },
                    "hover": {
                        "stroke": { "value": "red" },
                        "strokeWidth": { "value": 2 },
                        "zindex": { "value": 1 }
                    }
                }
            }
          /*{
              "type": "symbol",
              "from": { "data": "factsPacked" },
              "encode": {
                  "enter": {
                      "shape": { "value": "circle" },
                      "tooltip": { "signal": "datum.titleResolved + (datum." + json.weightingKey + " ? ': '+format(datum." + json.weightingKey + ",'.2s')+', '+datum.weight+', '+datum." + json.shadingKey + " : '')" }
                  },
                  "update": {
                      "x": { "field": "x" },
                      "y": { "field": "y" },
                      "size": { "signal": "4 * datum.r * datum.r" },
                      "stroke": { "value": "black" },
                      "strokeWidth": { "value": 0 },
                      "fill": { "signal": "datum.colorResolved" },
                  },
                  "hover": {
                      //"strokeWidth": { "value": 2 },
                  }
              }
          }*/
        ]
    };
};
Machinata.Reporting.Node.SunburstNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    //TODO: temporary patch of fact data to make it relative
    if (false) {
        var map = {};
        for (var i = 0; i < json.facts.length; i++) {
            var fact = json.facts[i];
            map[fact.id] = fact;
        }
        for (var i = 0; i < json.facts.length; i++) {
            var fact = json.facts[i];
            if (fact.parent == null) continue;
            var parent = map[fact.parent];
            fact.marketValueRelative = (fact.marketValue / parent.marketValue);
        }
        console.log(json.facts);
    }
    spec["data"][0].values = json.facts;
};
Machinata.Reporting.Node.SunburstNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.SunburstNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);

    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsLegend');
    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsResolved');
};
Machinata.Reporting.Node.SunburstNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








