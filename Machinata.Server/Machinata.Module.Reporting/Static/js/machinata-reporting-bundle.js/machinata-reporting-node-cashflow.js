


/// <summary>
/// Cash-Flow Graphic for Private Client Partners (exclusive).
///
/// ### Bucket Titles
/// The bucket titles are automatically line-breaked, 
/// but you can also define your own using the \n character. A bucket title should never
/// be more than two lines.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.PCPCashFlowGraphicNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults = {};

/// <summary>
/// This is a infographic, thus ```light```.
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_4x4];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.titleSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.titleLineHeight = 1.2;


/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.labelSize = 12;


/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.valueSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.valueLargeSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.symbolSize = 18;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.symbolFontWeight = "bold";

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.strokeDash = "4,4";

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.titleMaxCharsPerLine = 16;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.graphicAlignment = "middle";

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.beginAndEndBucketsAlignLeft = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.bucketFactHeight = 60;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.bucketFactPaddingMultiplier = 1;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.defaults.chartBorderMultiplier = 0.5;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PCPCashFlowGraphicNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return {
        "data": [
            {
                "name": "buckets",
                "values": null
            },
            {
                "name": "bucketsResolved",
                "source": "buckets",
                "transform": [
                    
                ]
            },
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          }
        ],
        "signals": [
            {
                "name": "chartPadding",
                /*"_comment": "NOTE: padding is reserved by vega (will cause strange behaviour)",*/
                "init": config.padding
            },
            {
                "name": "chartBorder",
                /*"_comment": "NOTE: padding is reserved by vega (will cause strange behaviour)",*/
                "init": config.padding * json.chartBorderMultiplier
            },
            {
                "name": "titleSize",
                "init": json.titleSize
            },
            {
                "name": "titleLineHeight",
                "init": "titleSize * "+json.titleLineHeight // default
            },
            {
                "name": "labelSize",
                "init": json.labelSize
            },
            {
                "name": "valueSize",
                "init": json.valueSize
            },
            {
                "name": "valueLargeSize",
                "init": json.valueLargeSize
            },
            {
                "name": "symbolSize",
                "init": json.symbolSize
            },
            {
                "name": "symbolFontWeight",
                "init": "'" + json.symbolFontWeight + "'"
            },
            {
                "name": "highlightBGColor",
                "init": "scale('colorByShade','mid')"
            },
            {
                "name": "highlightFGColor",
                "init": "scale('colorByShadeText','mid')"
            },
            {
                "name": "solidBGColor",
                "init": "scale('colorByShade','gray3')"
            },
            {
                "name": "solidFGColor",
                "init": "scale('colorByShadeText','gray3')"
            },
            {
                "name": "borderedBGColor",
                "init": "'transparent'"
            },
            {
                "name": "borderedFGColor",
                "init": "'black'"
            },
            {
                "name": "graphicAlignment",
                "init": "'" + json.graphicAlignment + "'"
            },
            {
                "name": "numberOfBuckets",
                "init": json._numberOfBuckets
            },
            {
                "name": "numberOfBucketsWithoutSymbol",
                "init": json._numberOfBucketsWithoutSymbol
            },
            {
                "name": "numberOfBucketsWithSymbol",
                "init": json._numberOfBucketsWithSymbol
            },
            {
                "name": "maxBucketFacts",
                "init": json._maxBucketFacts
            },
            {
                "name": "symbolConnectionWidth",
                "update": "80",
            },
            {
                "name": "symbolRadius",
                "update": "13",
            },
            {
                "name": "bucketFactHeight",
                "update": json.bucketFactHeight,
            },
            {
                "name": "bucketFactPaddingMultiplier",
                "update": json.bucketFactPaddingMultiplier,
            },
            {
                "name": "widthNeededForSymbols",
                "update": "numberOfBucketsWithSymbol * symbolConnectionWidth",
            },
            {
                "name": "bucketWidthExcludingSymbol",
                "update": "(width - widthNeededForSymbols - chartBorder) / numberOfBuckets",
            },
            {
                "name": "bucketHeight",
                "update": "bucketFactHeight * (maxBucketFacts+1)",
            },
        ],
        "scales": [
            {
                "name": "colorByShade",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": json.theme }
            },
            {
                "name": "colorByShadeText",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": "text-on-"+json.theme }
            },
            
            
        ],
        "marks": [
            {
                "clip": true, // makes sure that when we scale it doesnt push open the chart size
                "type": "group",
                "encode": {
                    "update": {
                        "width": { "signal": "width" },
                        "height": { "signal": "height" }
                    }
                },
                "marks": [
                    {
                        /*"_comment": "BUCKETS GROUPS",*/
                        "type": "group",
                        "from": { "data": "bucketsResolved" },
                        "signals": [
                            {
                                /*"_comment": "Expose the bucket datum to all marks...",*/
                                "name": "bucketData",
                                "update": "parent",
                            },
                        ],
                        "data": [
                            {
                                /*"_comment": "Since vega can't hide groups or marks, we need this data selector to filter our buckets that dont have symbols (for the symbol connection)",*/
                                "name": "bucketSymbol",
                                "source": "bucketsResolved",
                                "transform": [
                                    {
                                        "type": "filter",
                                        "expr": "datum._bucketIndex == bucketData._bucketIndex && datum.bucketSymbol != null"
                                    }
                                ]
                            }
                        ],
                        "encode": {
                            "enter": {
                                // DEBUGGING
                                //"tooltip": { "signal": "'bucket:'+datum.titleResolved" },
                                //"stroke": { "value": "rgba(255,0,0,1.0)" },
                                //"fill": { "value": "rgba(255,0,0,0.1)" },
                            },
                            "update": {
                                "x": { "signal": "bucketWidthExcludingSymbol * datum._bucketIndex + (datum._bucketSymbolsPreceedingThisBucket * symbolConnectionWidth) + chartBorder/2" },
                                "width": { "signal": "datum.bucketSymbol == null ? (bucketWidthExcludingSymbol) : (symbolConnectionWidth + bucketWidthExcludingSymbol)" },
                                "y": { "signal": " graphicAlignment == 'middle' ? ((height-bucketHeight)/2) : (chartPadding)" },
                                "height": { "signal": "bucketHeight" }
                            }
                        },
                        "marks": [
                            {
                                /*"_comment": "BUCKET SYMBOL CONNECTION",*/
                                "type": "group",
                                "from": { "data": "bucketSymbol" },
                                "encode": {
                                    "enter": {
                                        // DEBUGGING
                                        //"stroke": { "value": "rgba(0,255,0,1.0)" },
                                        //"fill": { "value": "rgba(0,255,0,0.1)" },
                                    },
                                    "update": {
                                        "x": { "signal": "0" },
                                        "width": { "signal": "symbolConnectionWidth" },
                                        "y": { "signal": "bucketFactHeight * 1 " },
                                        "height": { "signal": "bucketFactHeight - chartBorder" }
                                    }
                                },
                                "signals": [
                                    {
                                        /*"_comment": "shortcut signal...",*/
                                        "name": "groupWidth",
                                        "update": "symbolConnectionWidth",
                                    },
                                    {
                                        /*"_comment": "shortcut signal...",*/
                                        "name": "groupHeight",
                                        "update": "bucketFactHeight - chartBorder",
                                    },
                                ],
                                "marks": [
                                    {
                                        "type": "rule",
                                        "encode": {
                                            "enter": {
                                                "stroke": { "signal": "borderedFGColor" },
                                                "strokeDash": { "value": json.strokeDash },
                                                "strokeWidth": { "value": config.lineSize },
                                            },
                                            "update": {
                                                "x": { "signal": "0" },
                                                "x2": { "signal": "groupWidth/2 - symbolRadius" },
                                                "y": { "signal": "groupHeight/2" },
                                                "y2": { "signal": "groupHeight/2" }
                                            }
                                        },
                                    },
                                    {
                                        "type": "rule",
                                        "encode": {
                                            "enter": {
                                                "stroke": { "signal": "borderedFGColor" },
                                                "strokeDash": { "value": json.strokeDash },
                                                "strokeWidth": { "value": config.lineSize }, 
                                            },
                                            "update": {
                                                "x": { "signal": "groupWidth/2 + symbolRadius" },
                                                "x2": { "signal": "groupWidth" },
                                                "y": { "signal": "groupHeight/2" },
                                                "y2": { "signal": "groupHeight/2" }
                                            }
                                        },
                                    },
                                    {
                                        "type": "arc",
                                        "encode": {
                                            "enter": {
                                                "stroke": { "signal": "borderedFGColor" },
                                                "strokeDash": { "value": json.strokeDash },
                                                "strokeWidth": { "value": config.lineSize },
                                                "startAngle": { "signal": "-PI/2 + 0" },
                                                "endAngle": { "signal": "-PI/2 + PI*2" },
                                                "outerRadius": { "signal": "symbolRadius" },
                                            },
                                            "update": {
                                                "xc": { "signal": "groupWidth/2" },
                                                "yc": { "signal": "groupHeight/2" },
                                            }
                                        },
                                    },
                                    {
                                        "type": "text",
                                        "encode": {
                                            "enter": {
                                                "fontSize": { "signal": "symbolSize" },
                                                "fontWeight": { "signal": "symbolFontWeight" },
                                                "text": { "signal": "bucketData.bucketSymbol" },
                                                "baseline": { "value": "middle" },
                                                "align": { "value": "center" },
                                                "fill": { "signal": "highlightBGColor" },
                                            },
                                            "update": {
                                                "xc": { "signal": "groupWidth/2" },
                                                "yc": { "signal": "groupHeight/2" },
                                            }
                                        },
                                    }
                                ]
                            },
                            {
                                /*"_comment": "BUCKET CONTENTS",*/
                                "type": "group",
                                "data": [
                                    {
                                        "name": "bucketFacts",
                                        "source": "bucketsResolved",
                                        "transform": [
                                            {
                                                "type": "flatten",
                                                "fields": [
                                                    "facts"
                                                ],
                                                "as": [
                                                    "fact"
                                                ]
                                            },
                                            {
                                                "type": "filter",
                                                "expr": "datum.fact._bucketIndex == bucketData._bucketIndex"
                                            }
                                        ]
                                    }
                                ],
                                "encode": {
                                    "enter": {
                                        // DEBUGGING
                                        //"stroke": { "value": "rgba(0,0,255,1.0)" },
                                        //"fill": { "value": "rgba(0,0,255,0.1)" },
                                        //"tooltip": { "signal": "parent.titleResolved" },
                                    },
                                    "update": {
                                        "x": { "signal": "bucketData.bucketSymbol == null ? 0 : symbolConnectionWidth" },
                                        "width": { "signal": "bucketWidthExcludingSymbol" },
                                        "y": { "signal": "0" },
                                        "height": { "signal": "bucketHeight" }
                                    }
                                },
                                "marks": [
                                    {
                                        /*"_comment": "BUCKET TITLE (label)",*/
                                        "type": "text",
                                        "font": config.subtitleFont,
                                        "encode": {
                                            "enter": {
                                                "text": { "signal": "bucketData.titleResolved" },
                                                "fill": { "signal": "highlightBGColor" },

                                                "align": { "signal": "bucketData.textAlign == 'right' ? 'right' : 'left'" },
                                                "fontSize": { "signal": "titleSize" },
                                                "baseline": { "value": "bottom" },
                                                // DEBUGGING
                                                //"stroke": { "value": "rgba(255,0,0,1.0)" },
                                                //"fill": { "value": "rgba(255,0,0,0.1)" },
                                                //"tooltip": { "signal": "bucketData.titleResolved.length" },
                                            },
                                            "update": {
                                                "x": { "signal": "bucketData.textAlign == 'right' ? (bucketWidthExcludingSymbol) : 0" },
                                                "y": { "signal": "bucketFactHeight * 1  - (chartPadding*bucketFactPaddingMultiplier) - ((bucketData.titleResolved.length-1)*titleLineHeight)" },
                                            }
                                        }
                                    },
                                    {
                                        /*"_comment": "BUCKET FACT ITEMS",*/
                                        "type": "group",
                                        "from": { "data": "bucketFacts" },
                                        "encode": {
                                            "enter": {
                                                "stroke": { "signal": "highlightBGColor" },
                                                "strokeWidth": { "signal": "datum.fact.factStyle == 'bordered' ? chartBorder/2 : 0" },
                                                "fill": [
                                                    { "test": "datum.fact.factStyle == 'solid'", "signal": "highlightBGColor" },
                                                    { "test": "datum.fact.factStyle == 'default'", "signal": "solidBGColor" },
                                                    { "value": null }
                                                ],
                                                // DEBUGGING
                                                //"stroke": { "value": "rgba(255,0,0,1.0)" },
                                                //"fill": { "value": "rgba(255,0,0,0.1)" },
                                            },
                                            "update": {
                                                "x": { "signal": "0" },
                                                "width": { "signal": "bucketWidthExcludingSymbol" },
                                                "y": { "signal": "bucketFactHeight * (datum.fact._factIndex + 1)" },
                                                "height": { "signal": "bucketFactHeight - chartBorder" }
                                            }
                                        },
                                        "signals": [
                                            {
                                                /*"_comment": "shortcut signal...",*/
                                                "name": "groupWidth",
                                                "update": "bucketWidthExcludingSymbol",
                                            },
                                            {
                                                /*"_comment": "shortcut signal...",*/
                                                "name": "groupHeight",
                                                "update": "bucketFactHeight - chartBorder",
                                            },
                                        ],
                                        "marks": [
                                            {
                                                /*"_comment": "FACT LABEL",*/
                                                "type": "text",
                                                "encode": {
                                                    "enter": {
                                                        "text": { "signal": "parent.fact.title.resolved" },
                                                        "baseline": { "value": "top" },
                                                        "fontSize": { "signal": "labelSize" },
                                                        "align": { "signal": "bucketData.textAlign == 'right' ? 'right' : 'left'" },
                                                        "fill": [
                                                            { "test": "parent.fact.factStyle == 'solid'", "signal": "highlightFGColor" },
                                                            { "test": "parent.fact.factStyle == 'default'", "signal": "solidFGColor" },
                                                            { "signal": "solidFGColor" }
                                                        ],
                                                    },
                                                    "update": {
                                                        "x": { "signal": "bucketData.textAlign == 'right' ? (groupWidth-chartPadding*bucketFactPaddingMultiplier) : (0+chartPadding*bucketFactPaddingMultiplier)" },
                                                        "y": { "signal": "chartPadding*bucketFactPaddingMultiplier" },
                                                    }
                                                }
                                            },
                                            {
                                                /*"_comment": "FACT VALUE",*/
                                                "type": "text",
                                                "font": config.numberFont,
                                                "encode": {
                                                    "enter": {
                                                        "text": { "signal": "parent.fact.resolved" },
                                                        "baseline": { "value": "top" },
                                                        "fontSize": { "signal": "parent.fact._factIndex == 0 ? valueLargeSize : valueSize" },
                                                        "fontWeight": { "signal": "parent.fact._factIndex == 0 ? 'bold' : null" },
                                                        "align": { "signal": "bucketData.textAlign == 'right' ? 'right' : 'left'" },
                                                        "fill": [
                                                            { "test": "parent.fact.factStyle == 'solid'", "signal": "highlightFGColor" },
                                                            { "test": "parent.fact.factStyle == 'default'", "signal": "highlightBGColor" },
                                                            { "signal": "highlightBGColor" }
                                                        ],
                                                    },
                                                    "update": {
                                                        "x": { "signal": "bucketData.textAlign == 'right' ? (groupWidth-chartPadding*bucketFactPaddingMultiplier) : (0+chartPadding*bucketFactPaddingMultiplier)" },
                                                        "y": { "signal": "labelSize + chartPadding*bucketFactPaddingMultiplier + chartBorder" },
                                                    }
                                                }
                                            },
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
          
        ]
    };
};
Machinata.Reporting.Node.PCPCashFlowGraphicNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert Buckets
    spec["data"][0].values = json.buckets;
};

Machinata.Reporting.Node.PCPCashFlowGraphicNode.init = function (instance, config, json, nodeElem) {
    // Process data
    json._numberOfBuckets = json.buckets.length;
    json._numberOfBucketsWithSymbol = 0;
    json._numberOfBucketsWithoutSymbol = 0;
    json._maxBucketFacts = 0;
    for (var b = 0; b < json.buckets.length; b++) {
        var bucket = json.buckets[b];
        bucket._bucketIndex = b;
        bucket._bucketSymbolsPreceedingThisBucket = json._numberOfBucketsWithSymbol;
        if (bucket.bucketStyle == null) {
            if (b == 0) bucket.bucketStyle = "begin";
            else if (b == json.buckets.length-1) bucket.bucketStyle = "end";
            else bucket.bucketStyle = "operator";
        }
        if (bucket.textAlign == null) {
            if (json.beginAndEndBucketsAlignLeft == true) {
                if (bucket.bucketStyle == "begin" || bucket.bucketStyle == "end") bucket.textAlign = "left";
                else bucket.textAlign = "right";
            } else {
                bucket.textAlign = "right";
            }
        }
        if (bucket.bucketSymbol != null) json._numberOfBucketsWithSymbol++;
        else json._numberOfBucketsWithoutSymbol++;
        json._maxBucketFacts = Math.max(json._maxBucketFacts, bucket.facts.length);

        bucket.titleResolved = Machinata.Reporting.Text.resolve(instance, bucket.title);
        if (bucket.titleResolved.indexOf("\n") != -1) {
            // Includes line breaks already
            // Split into array
            bucket.titleResolved = bucket.titleResolved.split("\n");
        } else {
            bucket.titleResolved = Machinata.Reporting.Tools.lineBreakString(bucket.titleResolved, {
                maxCharsPerLine: json.titleMaxCharsPerLine,
                maxLines: 2,
                wordSeparator: " ",
                ellipsis: config.textTruncationEllipsis,
                appendEllipsisOnLastLine: true,
                returnArray: true
            });
        }
        


        for (var f = 0; f < bucket.facts.length; f++) {
            var fact = bucket.facts[f];
            fact._bucketIndex = bucket._bucketIndex;
            fact._factIndex = f;
            if (fact.factStyle == null) {
                if (bucket.bucketStyle == "begin" || bucket.bucketStyle == "end") {
                    fact.factStyle = "bordered";
                } else {
                    if (f == 0) fact.factStyle = "solid";
                    else fact.factStyle = "default";
                }
            }
        }
    }
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PCPCashFlowGraphicNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PCPCashFlowGraphicNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







