/// <summary>
/// Print Composer utility methods and data store.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.PrintComposer = {};


/// <summary>
/// Returns true if the user has added any screens for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.hasData = function (instance) {
    var data = Machinata.Reporting.PrintComposer.getData(instance);
    if (data == null || data.screens == null || data.screens.length == 0) return false;
    return true;
}

/// <summary>
/// Gets the printcomposer data for the current report (if any).
/// </summary>
Machinata.Reporting.PrintComposer.getData = function (instance) {
    //var key = "printcomposer_" + instance.reportData.reportId;
    //return Machinata.getCache(key);
    return Machinata.Reporting.UserSettings.getReportValue(instance,"printcomposer");
};


/// <summary>
/// Sets the printcomposer data for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.setData = function (instance,data) {
    //var key = "printcomposer_" + instance.reportData.reportId;
    //return Machinata.setCache(key, data);
    Machinata.Reporting.UserSettings.setReportValue(instance,"printcomposer",data);
};

/// <summary>
/// Resets all print composer data for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.resetData = function (instance) {
    Machinata.Reporting.PrintComposer.setData(instance,null);
};

/// <summary>
/// Helper function for sorting arrays of screens (nodeInfos)
/// </summary>
/// <hidden/>
Machinata.Reporting.PrintComposer._sortByScreenNumber = function (a, b) {
    return ((a.nodeScreen < b.nodeScreen) ? -1 : ((a.nodeScreen > b.nodeScreen) ? 1 : 0));
};

/// <summary>
/// Adds a screen for the current report via a nodeInfos object.
/// If the screen is already added, the add is ignored.
/// </summary>
Machinata.Reporting.PrintComposer.addScreen = function (instance, nodeInfos) {
    // INit
    var data = Machinata.Reporting.PrintComposer.getData(instance);
    if (data == null) data = {
        title: instance.config.reportTitle
    };
    if (data.screens == null) data.screens = [];

    // Already added?
    var alreadyExists = false;
    $.each(data.screens, function (index, screenJSON) {
        if (screenJSON.nodeScreen == nodeInfos.nodeScreen) alreadyExists = true;
    });

    // Add screen (migrated to node infos format)
    if (alreadyExists == false) {
        data.screens.push(nodeInfos);
        data.screens.sort(Machinata.Reporting.PrintComposer._sortByScreenNumber);
        Machinata.Reporting.PrintComposer.setData(instance, data); // save
    }
    //data.screens.push({
    //    screen: json.screenNumber,
    //    title: Machinata.Reporting.Text.resolve(instance,json.mainTitle),
    //    subtitle: Machinata.Reporting.Text.resolve(instance,json.subTitle)
    //});

    // Browse to?
    Machinata.yesNoDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-composer.added.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print-composer.added.message"))
        .okay(function () {
            Machinata.Reporting.Handler.openPrintComposer(instance);
        }).show();

    return data;
};


/// <summary>
/// Removes a screen via a screen number for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.removeScreen = function (instance, screenNumber) {
    var data = Machinata.Reporting.PrintComposer.getData(instance);
    var newScreens = [];
    $.each(data.screens, function (index, screenJSON) {
        if (screenJSON.nodeScreen != screenNumber) newScreens.push(screenJSON);
    });
    data.screens = newScreens;
    // Save
    Machinata.Reporting.PrintComposer.setData(instance,data);
    return data;
};


/// <summary>
/// Returns all the screens as an array of nodeInfos for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.getScreens = function (instance) {
    var data = Machinata.Reporting.PrintComposer.getData(instance);
    if (data == null) return null;
    return data.screens;
};

/// <summary>
/// Returns all the screens as an array of ints for the current report.
/// </summary>
Machinata.Reporting.PrintComposer.getScreensAsIntArray = function (instance) {
    var screens = Machinata.Reporting.PrintComposer.getScreens(instance);
    if (screens == null) return null;
    var intScreens = [];
    for (var i = 0; i < screens.length; i++) {
        intScreens.push(screens[i].nodeScreen);
    }
    return intScreens;
};


/// <summary>
/// A built-in basic UI for the print composer.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.PrintComposer.UI = {};

/// <summary>
/// </summary>
Machinata.Reporting.PrintComposer.UI.init = function (config) {

    // Patch config
    config.automaticallyFoldChaptersIntoPages = false;
    config.buildContents = false; // we don't need the report contents
    config.automaticallyInsertChapterNavigation = false; // we dont need the chapter nav


    // Init report
    Machinata.Reporting.init(config, function (instance, config) {

        // Add a nav item for print composer
        if (Machinata.Reporting.Handler.addNavigationItem != null) Machinata.Reporting.Handler.addNavigationItem(instance, "Print Composer", "");

        // Init print composer...
        var elem = $(instance.config.printComposerSelector);

        // Get data
        var data = Machinata.Reporting.PrintComposer.getData(instance);

        // Debug
        Machinata.debug("Machinata.Reporting.PrintComposer.init:");
        Machinata.debug(data);

        if (data == null || data.screens == null || data.screens.length == 0) {
            // No Screens
            elem.find(".machinata-printcomposer-nocontent").show();
            elem.find(".machinata-printcomposer-tools").hide();
            elem.find(".machinata-printcomposer-screens").hide();
        } else {
            // Have screens
            elem.find(".machinata-printcomposer-nocontent").hide();

            // Compile listing
            var previewsByScreenNumber = {};
            $.each(data.screens, function (index, screenJSON) {
                // Validate
                if (previewsByScreenNumber[screenJSON.nodeScreen] != null) return;
                // Init preview elem
                var previewElem = $("<div class='item machinata-reporting-toolbar option-gray'><label class='title theme-headline-font'></label><label class='subtitle'></label><div class='tools'><div class='tool tool-remove'/></div></div></div>");
                previewElem.attr("data-screen", screenJSON.nodeScreen);
                previewElem.find(".tool-remove").append(Machinata.Reporting.buildIcon(instance, "close")); // <div class='tool tool-remove icon-csam-fn-small-close' data-hint='Remove item'></div>
                previewElem.find(".title").text(screenJSON.nodeTitle);
                previewElem.find(".subtitle").text(screenJSON.nodeSubtitle);
                previewElem.find(".tool-remove").click(function () {
                    // Remove elem
                    var updatedData = Machinata.Reporting.PrintComposer.removeScreen(instance, screenJSON.nodeScreen);
                    // Update UI
                    previewElem.remove();
                    if (updatedData.screens.length == 0) {
                        Machinata.reload();
                    }
                });

                // Register
                previewsByScreenNumber[screenJSON.nodeScreen] = previewElem;
                elem.find(".machinata-printcomposer-screens").append(previewElem);
            });
            Machinata.UI.bind(elem.find(".machinata-printcomposer-screens"));
        }

        // Bind actions
        elem.find(".machinata-printcomposer-action-reset").click(function () {
            Machinata.confirmDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-composer.reset.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print-composer.reset.message"))
                .okay(function () {
                    Machinata.Reporting.PrintComposer.resetData(instance);
                    Machinata.reload();
                })
                .show();
        });
        elem.find(".machinata-printcomposer-action-preview").click(function () {
            var reportURL = Machinata.updateQueryString("screen", "printcomposer", instance.config.url);
            reportURL = Machinata.updateQueryString("profile", "webprint", reportURL);
            reportURL = Machinata.updateQueryString("chapter", "*", reportURL);
            reportURL = Machinata.updateQueryString("print-dialog", "true", reportURL);
            Machinata.openPage(reportURL);
        });

    });


};

/// <summary>
/// </summary>
Machinata.Reporting.PrintComposer.UI.open = function (instance) {
    //TODO: is this really the best method in getting the report url?
    var reportURL = Machinata.updateQueryString("ui", "printcomposer", instance.config.url);
    Machinata.openPage(reportURL);
};
