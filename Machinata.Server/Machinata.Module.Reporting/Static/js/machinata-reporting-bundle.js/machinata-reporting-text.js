

/// <summary>
/// Reporting text tools for resolving texts with formats and getting the proper translation.
/// Note: It is highly recommend to always provide reports JSON with fully resolved text.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Text = {};

/// <summary>
/// Get the resolved text for a JSON object.
/// It is highly recommend to always provide reports JSON with fully resolved text.
/// </summary>
/// <example>
/// Example JSON:
/// ```
/// "myResolvableText": {
///     "translations": {
///         "en": "View Page",
///         "de": "Seite Ansehen",
///         "*": "TRANS: View Page"
///     },
///     "resolved": "View Page"
/// }
/// ```
/// </summary>
Machinata.Reporting.Text.resolve = function (instance, json) {
    // Sanity
    if (instance == null) throw "Machinata.Reporting.Text.resolve: instance cannot be null";
    if (instance.config == null) throw "Machinata.Reporting.Text.resolve: instance.config cannot be null";

    // Get the language to use
    var language = instance.config.contentLanguage;

    // Get the resolved text
    if (json == null) return null;
    if (json["resolved"] != null) return json["resolved"];
    if (json["translations"] != null) {
        if (json["translations"][language] != null) return json["translations"][language];
        if (json["translations"]["*"] != null) return json["translations"]["*"];
    }
    if (json["text"] != null) return json["text"];
    return null;
};





/// <summary>
/// Looks up the translation for a given identifier for the 'instance.config.interfaceLanguage' 
/// Uses 'en' as a fallback
/// If no translation can be found this will return [TRANS:{translationId}.{language}]
/// </summary>
Machinata.Reporting.Text.translate = function (instance, translationId) {
    // Sanity
    if (instance == null) throw "Machinata.Reporting.Text.resolve: instance cannot be null";
    if (instance.config == null) throw "Machinata.Reporting.Text.resolve: instance.config cannot be null";

    // Get the language to use
    var language = instance.config.interfaceLanguage;
    var fallbackLanguage = "en";

    // Get the entry
    var translations = Machinata.Reporting.Text.TRANSLATIONS["categories"]["global"]["translations"];
    var entry = translations[translationId];

    // Get the translation 
    var translation = null;
    if (entry != null) {
        translation = entry[language];

        // Fallback Language
        if (translation == null || translation == "") {
            translation = entry[fallbackLanguage];
        }
    }

    // Nothing Found
    if (translation == null || translation == "") {

        translation = "[TRANS:" + translationId + "." + language + "]";
    }
    
    return translation;
};




