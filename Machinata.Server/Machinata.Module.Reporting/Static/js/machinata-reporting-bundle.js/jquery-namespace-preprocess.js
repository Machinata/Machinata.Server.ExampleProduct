
/// <summary>
/// Shortcut multi-platform handle for the namespace for globals.
/// </summary>
/// <hidden/>
var MACHINATA_GLOBAL = (typeof global !== 'undefined') ? global : window;


(function ($, jQuery) { 
