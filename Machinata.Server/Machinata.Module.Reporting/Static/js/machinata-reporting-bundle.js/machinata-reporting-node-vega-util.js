/// <summary>
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VegaNode.Util = {};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForGroupedSeries = function (instance, config, json, seriesGroups, opts) {
    var legendMap = {};
    var legendData = [];
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            var serieKey = seriesGroup.id + "_" + serie.id;
            var serieTitle = Machinata.Reporting.Text.resolve(instance, serie.title);
            var serieSymbol = "square"; // fallback
            if (serie.chartType == "line") serieSymbol = "line";
            if (serie.chartType == "area") serieSymbol = "square";
            var legendDatum = {
                key: serieKey,
                title: serieTitle,
                symbol: serieSymbol
            };
            legendMap[serieKey] = legendDatum;
            if (opts != null && opts.reverseOrder == true) {
                legendData.splice(0, 0, legendDatum); // insert
            } else {
                legendData.push(legendDatum); // push
            }
        }
    } 
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries = function (instance, config, json, series, opts) {
    var legendData = [];
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        var serieKey = serie.id; 
        var serieTitle = Machinata.Reporting.Text.resolve(instance, serie.title); 
        var legendDatum = {
            key: serieKey,
            title: serieTitle
        };
        legendData.push(legendDatum);
    }
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForSeriesFacts = function (instance, config, json, series, opts) {
    var legendMap = {};
    var legendData = [];
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        for (var fi = 0; fi < serie.facts.length; fi++) {
            var fact = serie.facts[fi];
            var factTitle = Machinata.Reporting.Text.resolve(instance, fact.category);
            var factKey = factTitle; //TODO
            if (legendMap[factKey] == null) {
                var legendDatum = {
                    key: factKey,
                    title: factTitle
                };
                if (fact.symbolType != null) legendDatum.symbol = fact.symbolType;
                legendMap[factKey] = legendDatum;
                legendData.push(legendDatum);
            }
        }
    }
    json.legendData = legendData;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.getNiceDomain = function (domain) {
    return d3.scaleLinear().domain([domain[0], domain[1]]).nice().domain();
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.getNiceStep = function (step) {
    return d3.scaleLinear().domain([0, step]).nice().domain()[1];
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis = function (instance, config, json, series, opts) {
    // Init
    Machinata.debug("Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis:");
    Machinata.debug("  " + "node id: " + json.id);
    if (opts == null) opts = {};
    if (opts.axisKey == null) throw "The axisKey must be set (usually xAxis or yAxis) for Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis!";
    if (opts.stackedSeries == null) opts.stackedSeries = false; // Note: when we say we want to stack series, we want to stack each serie ontop of each other whithin a category
    if (opts.tickCount == null) opts.tickCount = config.axisTargetTickCount; // Fallback

    // Get min/max
    var minVal = null; // Facts min/max values
    var maxVal = null; // Facts min/max values
    var serieMinTotalVal = null; // Series total min/max values
    var serieMaxTotalVal = null; // Series total min/max values
    var categoryMinTotalVal = null; // Categories total min/max values
    var categoryMaxTotalVal = null; // Categories total min/max values
    var categories = {};
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        serie.totalValue = null; // tracks the total value of a serie
        if (serie[opts.axisKey] == null) serie[opts.axisKey] = {};
        for (var fi = 0; fi < serie.facts.length; fi++) {
            var fact = serie.facts[fi];
            // Set axis format?
            if (fact.format != null && serie[opts.axisKey].format == null) {
                Machinata.debug("  "+"axis format was null, setting based on first fact format");
                console.log("  "+"axis format was null, setting based on first fact format");
                serie[opts.axisKey].format = fact.format;
            }
            if (fact.formatType != null && serie[opts.axisKey].formatType == null) {
                Machinata.debug("  " + "axis formatType was null, setting based on first fact format");
                serie[opts.axisKey].formatType = fact.formatType;
            }
            if (fact.val != null) {
                // Validate 
                if (opts.stackedSeries == true && fact.val < 0) throw "A stacked series cannot have negative values."
                // Track fact min/max
                if (fact.val < minVal || minVal == null) minVal = fact.val;
                if (fact.val > maxVal || maxVal == null) maxVal = fact.val;
                // Track series total
                if (serie.totalValue == null) serie.totalValue = 0;
                // Track categories total
                var category = categories[fact.category.resolved];
                if (category == null) {
                    category = { "key": fact.category.resolved,"totalValue": 0};
                    categories[category.key] = category;
                }
                // Track the stacking value (this makes drawing later very easy and cheap)
                fact.stackedValA = category.totalValue; // pre value
                category.totalValue += fact.val; 
                serie.totalValue += fact.val;
                fact.stackedValB = category.totalValue; // post value
            }
        }
        if (serie.totalValue != null && (serie.totalValue < serieMinTotalVal || serieMinTotalVal == null)) serieMinTotalVal = serie.totalValue;
        if (serie.totalValue != null && (serie.totalValue > serieMaxTotalVal || serieMaxTotalVal == null)) serieMaxTotalVal = serie.totalValue;
    }

    // Stacked? If so the min / max should reflect the series total min max
    if (opts.stackedSeries == true) {
        
        // Get the min/max over all categories
        Machinata.Util.each(categories, function (key,category) {
            if (category.totalValue != null && (category.totalValue < categoryMinTotalVal || categoryMinTotalVal == null)) categoryMinTotalVal = category.totalValue;
            if (category.totalValue != null && (category.totalValue > categoryMaxTotalVal || categoryMaxTotalVal == null)) categoryMaxTotalVal = category.totalValue;
        });
        Machinata.debug("  " + "calc categoryMinTotalVal/categoryMaxTotalVal: " + categoryMinTotalVal + "/" + categoryMaxTotalVal);
        minVal = categoryMinTotalVal;
        maxVal = categoryMaxTotalVal;
    }

    // Debug out
    Machinata.debug("  " + "calc min/max: " + minVal + "/" + maxVal);

    // Balance?
    if (opts.balance == true) {
        var minMaxAbsVal = Math.max(Math.abs(minVal), Math.abs(maxVal));
        if (minVal < 0 && maxVal > 0) {
            minVal = -minMaxAbsVal;
            maxVal = +minMaxAbsVal;
        } else {
            minVal = Math.min(0, minVal);
            maxVal = Math.max(0, maxVal);
        }
        Machinata.debug("  " + "balanced min/max: " + minVal + "/" + maxVal);

    }

    // Snap to zero?
    // We do so if either both min/max are >0, or min/max < 0
    if (opts.snapToZero == true) {
        if (minVal >= 0 && maxVal >= 0) {
            minVal = 0;
        } else if (minVal < 0 && maxVal < 0) {
            maxVal = 0;
        }
        Machinata.debug("  " + "zero-snap min/max: " + minVal + "/" + maxVal);
    }

    // Add margin
    if (opts.margin != null) {
        if (minVal < 0) minVal = minVal + (minVal * opts.margin);
        if (maxVal > 0) maxVal = maxVal + (maxVal * opts.margin);
        Machinata.debug("  " + "margined min/max: " + minVal + "/" + maxVal);
    }

    // Use D3's nice algorithm to snap the domain to nice round values...
    if (opts.niceDomain == true) {
        // See https://github.com/d3/d3-scale/blob/master/src/nice.js
        // See https://github.com/d3/d3-scale/blob/v2.2.2/README.md#continuous_nice
        var d3NiceDomain = d3.scaleLinear().domain([minVal, maxVal]).nice().domain();
        minVal = d3NiceDomain[0];
        maxVal = d3NiceDomain[1];
        Machinata.debug("  " + "niced min/max: " + minVal + "/" + maxVal);
    }


    // Set min max on axis's
    Machinata.debug("  " + "new min/max: " + minVal + "/" + maxVal);
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        if (serie[opts.axisKey] == null) serie[opts.axisKey] = {};
        // We only update the min/max if it has not already been explicitly set...
        if (serie[opts.axisKey].minValue == null) serie[opts.axisKey].minValue = minVal;
        if (serie[opts.axisKey].maxValue == null) serie[opts.axisKey].maxValue = maxVal;
        if (serie[opts.axisKey].formatType == null) serie[opts.axisKey].formatType = "number"; //fallback
    }

    // Set tick values, set formatting
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        var axis = serie[opts.axisKey];
        if (axis.minValue != null && axis.maxValue != null) {
            if (axis.requestedMinValue == null) axis.requestedMinValue = axis.minValue;
            if (axis.requestedMaxValue == null) axis.requestedMaxValue = axis.maxValue;
            Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues(axis, opts.tickCount, opts.niceDomain, opts.extendToDomain);
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis(axis);
        }
    }
    
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis = function (instance, config, json, opts) {
    //TODO: @dankrusi: make this more generic
    //TODO: @dankrusi: refactor to options params

    // Init
    Machinata.debug("Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis:");
    Machinata.debug("  "+"node id: "+json.id);

    // Pre process
    // Here we track all the min/max, and track the absolute min/max across all series
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        if (seriesGroup.yAxis == null) seriesGroup.yAxis = {}; // Make sure the axis exists
        var minValue = null;
        var maxValue = null;
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            for (var fi = 0; fi < serie.facts.length; fi++) {
                var fact = serie.facts[fi];
                if (fact.y != null && (fact.y < minValue || minValue == null)) minValue = fact.y;
                if (fact.y != null && (fact.y > maxValue || maxValue == null)) maxValue = fact.y;
            }
        }
        Machinata.debug("  " + "orig " + seriesGroup.id + ": ");
        Machinata.debug("  " + "  " + "min: " + seriesGroup.yAxis.minValue);
        Machinata.debug("  " + "  " + "max: " + seriesGroup.yAxis.maxValue);
        Machinata.debug("  " + "calc " + seriesGroup.id + ": ");
        Machinata.debug("  " + "  " + "min: " + minValue);
        Machinata.debug("  " + "  " + "max: " + maxValue);

        // Snap to zero?
        // We do so if either both min/max are >0, or min/max < 0
        if (json.autoSnapYAxisSnapToZero == true) {
            if (minValue >= 0 && maxValue >= 0) {
                minValue = 0;
            } else if (minValue < 0 && maxValue < 0) {
                maxValue = 0;
            }
            Machinata.debug("  " + "snapped to zero " + seriesGroup.id + ": ");
            Machinata.debug("  " + "  " + "min: " + minValue);
            Machinata.debug("  " + "  " + "max: " + maxValue);
        }

        // Add margin
        if (json.autoSnapYAxisMargin != null) {
            if (minValue < 0) minValue = minValue * (1.0 + json.autoSnapYAxisMargin);
            if (maxValue > 0) maxValue = maxValue * (1.0 + json.autoSnapYAxisMargin);

            Machinata.debug("  " + "margined " + seriesGroup.id + ": ");
            Machinata.debug("  " + "  " + "margin: " + json.autoSnapYAxisMargin);
            Machinata.debug("  " + "  " + "min: " + minValue);
            Machinata.debug("  " + "  " + "max: " + maxValue);
        }

        // Use D3's nice algorithm to snap the domain to nice round values...
        if (json.autoSnapYAxisNiceDomain == true) {
            // See https://github.com/d3/d3-scale/blob/master/src/nice.js
            // See https://github.com/d3/d3-scale/blob/v2.2.2/README.md#continuous_nice
            var domain = Machinata.Reporting.Node.VegaNode.Util.getNiceDomain([minValue, maxValue]);
            minValue = domain[0];
            maxValue = domain[1];
            Machinata.debug("  " + "niced " + seriesGroup.id + ": ");
            Machinata.debug("  " + "  " + "min: " + minValue);
            Machinata.debug("  " + "  " + "max: " + maxValue);
        }

        // Register, for now
        seriesGroup.yAxis.calcMinValue = minValue;
        seriesGroup.yAxis.calcMaxValue = maxValue;
        seriesGroup.yAxis.calcBandwidth = maxValue - minValue;

    }

    // Apply the strategy...
    if (json.autoSnapYAxisStategy == "proportional") {

        // Proportional strategy...

        // Track top/bottom min/max ratios (we use the upper value)
        var maxTopRatio = 0;
        var maxBottomRatio = 0;
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            if (seriesGroup.yAxis.preBandwidth != 0) {
                maxTopRatio = Math.max(maxTopRatio, seriesGroup.yAxis.calcMaxValue * (1) / seriesGroup.yAxis.calcBandwidth);
                maxBottomRatio = Math.min(maxBottomRatio, seriesGroup.yAxis.calcMinValue * (1) / seriesGroup.yAxis.calcBandwidth);
            }
        }
        // Apply each axis domain according to the max ratios
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            if (seriesGroup.yAxis.calcBandwidth != 0) {
                seriesGroup.yAxis.strategyMinValue = maxBottomRatio * seriesGroup.yAxis.calcBandwidth;
                seriesGroup.yAxis.strategyMaxValue = maxTopRatio * seriesGroup.yAxis.calcBandwidth;
            }
        }

    } else {

        throw "The auto snap strategy " + json.autoSnapYAxisStategy + " is not supported! Please check yout autoSnapYAxisStategy setting.";

    }

    // Check each group to see if a yaxis min/max is missing
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];

        // Don't apply a strategy min/max value if one has been manually set...
        if (seriesGroup.yAxis.minValue != null && seriesGroup.yAxis.maxValue != null) {
            seriesGroup.yAxis.requestedMinValue = seriesGroup.yAxis.minValue;
            seriesGroup.yAxis.requestedMaxValue = seriesGroup.yAxis.maxValue;
            continue;
        }

        // Set the axis value to those provided by the strategy
        seriesGroup.yAxis.minValue = seriesGroup.yAxis.strategyMinValue;
        seriesGroup.yAxis.maxValue = seriesGroup.yAxis.strategyMaxValue;
        seriesGroup.yAxis.requestedMinValue = seriesGroup.yAxis.strategyMinValue;
        seriesGroup.yAxis.requestedMaxValue = seriesGroup.yAxis.strategyMaxValue;
        Machinata.debug("  " + "strategy " + seriesGroup.id + ": ");
        Machinata.debug("  " + "  " + "min: " + seriesGroup.yAxis.strategyMinValue);
        Machinata.debug("  " + "  " + "max: " + seriesGroup.yAxis.strategyMaxValue);
    }

    // Set tick values
    if (true) {

        // Reference: d3 ticks() source: https://github.com/d3/d3-array/blob/master/src/ticks.js
        Machinata.debug("will set ticks manually:")
        var tickCount = json.autoSnapYAxisTickCount;
        if (tickCount == null) tickCount = config.axisTargetTickCount; //TODO: @dankrusi what about if we want responsive?
        var tries = 0;
        var maxTries = 10;
        var currentAxisBoundFitScaleFactor = 1.0;
        while (tries < maxTries) {
            tries++;
            Machinata.debug("  "+"try", tries);
            var allAxisWithinRequestedBounds = true;
            for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
                var axis = json.seriesGroups[sgi].yAxis;
                if (sgi == 0) {
                    Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues(axis, tickCount, json.autoSnapYAxisNiceDomain);
                } else {
                    var refAxis = json.seriesGroups[0].yAxis;
                    var axisBandwidth = (axis.requestedMaxValue - axis.requestedMinValue);
                    var refAxisBandwidth = (refAxis.requestedMaxValue - refAxis.requestedMinValue);
                    var bandwidthFactor = (axisBandwidth / refAxisBandwidth) * currentAxisBoundFitScaleFactor;
                    //console.log("axisBandwidth", axisBandwidth);
                    //console.log("refAxisBandwidth", refAxisBandwidth);
                    //console.log("bandwidthFactor", bandwidthFactor);
                    axis.values = [];
                    for (var ti = 0; ti < refAxis.values.length; ti++) {
                        var refAxisTickValue = refAxis.values[ti];
                        var tickValue = refAxisTickValue * bandwidthFactor;
                        //console.log("tick", ti, refAxisTickValue, tickValue)
                        axis.values.push(tickValue);
                    }
                    Machinata.debug("  " +"refAxis values",refAxis.values);
                    Machinata.debug("  " +"axis values",axis.values);
                }
                // Register
                axis.minValue = axis.values[0];
                axis.maxValue = axis.values[axis.values.length - 1];
                // Check if within bounds
                if (axis.minValue > axis.requestedMinValue || axis.maxValue < axis.requestedMaxValue) {
                    allAxisWithinRequestedBounds = false;
                    Machinata.debug("  " +"axis " + sgi + " does not fit bounds:");
                } else {
                    Machinata.debug("  " +"axis " + sgi + " fits bounds:");

                }
                Machinata.debug("  " + "requested", axis.requestedMinValue, axis.requestedMaxValue);
                Machinata.debug("  " + "current", axis.minValue, axis.maxValue);
                
            }
            if (allAxisWithinRequestedBounds == false) {
                Machinata.debug("  " +"not allAxisWithinRequestedBounds!");
                currentAxisBoundFitScaleFactor += 0.05;
            } else {
                Machinata.debug("  " + "allAxisWithinRequestedBounds! Finishing...");
                break;
            }
        }

        
    }

    // Automatically set the axis format?
    if (true) {
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var axis = json.seriesGroups[sgi].yAxis;
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis(axis);
        }
    }

};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues = function (axis, tickCount, niceDomain, extendToDomain) {
    Machinata.debug("  " + "automatically set axis tick values:");
    if (axis.tickCount != null) tickCount = axis.tickCount;
    var scale = d3.scaleLinear().domain([axis.minValue, axis.maxValue]);
    Machinata.debug("  " + "raw scale", scale.domain());
    if (niceDomain == true) scale = scale.nice(tickCount);
    Machinata.debug("  " + "niced scale", scale.domain());
    var ticks = scale.ticks(tickCount);
    Machinata.debug("  " + "ticks", ticks);
    if (axis.requestedMinValue != null && axis.requestedMaxValue != null && extendToDomain != false) axis.values = Machinata.Reporting.Node.VegaNode.Util.extendTicksToDomain(ticks, axis.requestedMinValue, axis.requestedMaxValue);
    else axis.values = ticks;
    Machinata.debug("  " + "values", axis.values);
    Machinata.debug("  " + "ticks domain", axis.values[0], axis.values[axis.values.length - 1]);

    return axis.values;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers = function (facts) {
    var fullNumberFormat = d3.format(".0%");
    for (var i = 0; i < facts.length; i++) {
        var fact = facts[i];
        if (fact.format != null && fact.format.endsWith("%")) {
            // If the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%), 
            // we use the facts full formatting, otherwise we use a standard full number percent format
            // that is simplified (ie 43% instead of 43.04%)...
            if ((fact.val <= -0.01 || fact.val >= 0.01) && fact.val <= 0.995 && fact.val != 1) fact.resolved = fullNumberFormat(fact.val);
        }
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis = function (axis) {
    // We only support number formats
    if (axis.formatType == "number" && axis.format != null && axis.values != null) {
        Machinata.debug("Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis:");
        if (axis.format.startsWith(".") && axis.format.endsWith("%")) {
            // Percent formatting ".5%"
            Machinata.debug("  " + "automatically formatting percent axis...");
            var minValue = axis.values[0];
            var maxValue = axis.values[axis.values.length-1];
            var tickCount = axis.values.length;
            var stepWidth = (maxValue - minValue) / (tickCount-1);
            var decPoints = -Math.ceil(Math.log10(stepWidth)); // from bmpi https://banknotes.atlassian.net/secure/RapidBoard.jspa?rapidView=7&projectKey=NGR&modal=detail&selectedIssue=NGR-817&assignee=5cf767844ddcda0e7cb95b49
            if (isFinite(decPoints) == false) decPoints = 0; // make sure this never fails
            Machinata.debug("  old format", axis.format);
            decPoints--; // TODO: @bmpi: for some reason this needs to be subracted one...?
            if (decPoints < 0) decPoints = 0; // make sure this never fails
            axis.format = "." + (decPoints) + "%";
            Machinata.debug("  new format", axis.format);
            axis.formatSource = "Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis";
        } else if (axis.format.startsWith(".") && axis.format.endsWith("f")) {
            // Decimal formatting ".f%"
            Machinata.debug("  " + "automatically formatting decimal axis...");
            var minValue = axis.values[0];
            var maxValue = axis.values[axis.values.length - 1];
            var tickCount = axis.values.length;
            var stepWidth = (maxValue - minValue) / (tickCount - 1);
            var decPoints = -Math.ceil(Math.log10(stepWidth)); // from bmpi https://banknotes.atlassian.net/secure/RapidBoard.jspa?rapidView=7&projectKey=NGR&modal=detail&selectedIssue=NGR-817&assignee=5cf767844ddcda0e7cb95b49
            if (isFinite(decPoints) == false) decPoints = 0; // make sure this never fails
            Machinata.debug("  old format", axis.format);
            decPoints++; // TODO: @bmpi: for some reason this needs to be added one...?
            if (decPoints < 0) decPoints = 0; // make sure this never fails
            axis.format = "." + (decPoints) + "f";
            Machinata.debug("  new format", axis.format);
            axis.formatSource = "Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis";
        } else {
            Machinata.debug("  " + "unsupported automatic format:"+axis.format);
        }
    }
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.extendTicksToDomain = function (ticks, requestedMin, requestedMax) {
    // Sanity
    if (ticks.length < 2) return ticks; // we need at least two ticks
    // Init
    var tries = 0;
    var maxTries = 4;
    while (tries < maxTries) {
        tries++;
        var minValue = ticks[0];
        var maxValue = ticks[ticks.length - 1];
        var step = ticks[1] - ticks[0]; // Note: this will have a floating point error on small numbers
        var stepNiced = Machinata.Reporting.Node.VegaNode.Util.getNiceStep(step);
        if (minValue > requestedMin) {
            if (tries == 1) Machinata.debug("Automatically extending axis ticks:");
            Machinata.debug(" adding to front:", step, stepNiced);
            ticks.unshift(minValue - stepNiced);
        } if (maxValue < requestedMax) {
            if (tries == 1) Machinata.debug("Automatically extending axis ticks:");
            Machinata.debug("  adding to back", step, stepNiced);
            ticks.push(maxValue + stepNiced);
        } else {
            return ticks;
        }
    }
    return ticks;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis = function (instance, config, json, axisDefinition) {
    if (axisDefinition.orient == "bottom" && axisDefinition.ticks == null && config.axisBottomShowTicks == true) {
        axisDefinition.ticks = true;
        axisDefinition.tickSize = config.axisBottomTickSize
    }
    if (axisDefinition.orient == "bottom" && json.xAxisLabelAngle != null) {
        axisDefinition.labelAngle = json.xAxisLabelAngle; // Angle in degrees of axis tick labels
        axisDefinition.labelAlign = "right"; // Horizontal text alignment of axis tick labels, overriding the default setting for the current axis orientation.
        axisDefinition.labelOverlap = "greedy"; // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
        axisDefinition.labelSeparation = 0; // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled.
    }
    return axisDefinition;
};