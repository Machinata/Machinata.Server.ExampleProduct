

/// <summary>
/// Handler namespace.
/// The handlers let you configure and bind a custom user interface
/// for different reporting functionality. Typically, these handlers
/// handle things happening outside of the report.
/// For example: a fullscreen button located at the top right of the page.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Handler = {};  

/// <summary>
/// A handler called when a specific profile is loaded.
/// This allows you to prepare a page specifically for the profile, if needed.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.setupForProfile = function(profile) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.setupForProfile = null;

/// <summary>
/// A handler for adding a page tool that has to do with the report being displayed.
/// For example, such tools may include report attachments, date change, print composer, etc....
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addPageTool = function(instance, title, icon, action, tooltip, addSeparator) { 
///     // instance >> reporting instance
///     // title >> string, tool title
///     // icon >> string, icon id
///     // action >> click hander, called when tool is pressed - you must bind this handler to a press event of the tool
///     // tooltip >> string, tool tooltip
///     // addSeparator >> bool, true if a separator should be added
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.addPageTool = null;

/// <summary>
/// A handler called when a user would like to change the reporting date.
/// You can use the default supplied version: 
/// ```Machinata.Reporting.Tools.showReportDates```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.changeReportingDate = function(instance, catalogJSON, currentMandateJSON, currentReportJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.changeReportingDate = null;

/// <summary>
/// A handler that is called whenever the user would like to change chapter. This allows you to customize the behaviour of a chapter
/// a chapter change, depending on the integration.
///
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.changeChapterViaURLUpdate```
///  - ```Machinata.Reporting.Tools.changeChapterViaInlineUpdate```
///
/// Note: If you use a custom integration, you must make sure that absolute URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.changeChapter = function(instance, chapterId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.changeChapter = null; 

/// <summary>
/// A handler for when the user wishes to ask a question regarding a specifc node or section.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.askAQuestionViaEmail```
///
/// Note: If you use a custom integration, you must make sure that absolute URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.askAQuestion = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.askAQuestion = null;

/// <summary>
/// A handler for when the user wishes to add a specific node to their dashboard.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// In the ```infosJSON``` of special interest are the properties ```nodeSupportedDashboardSizes``` (see ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```),
/// ```nodeCurrentPixelSize``` (see ```Machinata.Reporting.Node.compileNodeInfos```), and
/// ```nodeCurrentBlockSize``` (see ```Machinata.Reporting.Node.compileNodeInfos```).
/// 
/// Here it is best practice to add the node to the dashboard as one of the supported dashboard sizes that best matches the current block size.
/// This ensures that the user has the closest representation of the node on the dashboard as it was on the report when the user originally requested to add
/// it to the dashboard.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addToDashboard = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.addToDashboard = null;

/// <summary>
/// A handler for when the user wishes to add a specific node to a presentation.
/// Typically, this will be the entire screen for the current node.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addToPresentation = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.addToPresentation = null;

/// <summary>
/// A handler for sharing a specific node and it's URL.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.shareNodeByCopyingLink```
///
/// Note: If you use a custom integration, you must make sure that absolute URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.shareNode = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.shareNode = null;


/// <summary>
/// A handler for printing a specific report.
/// Typically, such a handler should display a list of common
/// printing options, such as:
///  - Print entire report
///  - Print current chapter
///  - View print composer
///
/// The options displayed depend on which of the following corresponding handlers are implemented:
///  - Machinata.Reporting.Handler.openPrintPageForChapter
///  - Machinata.Reporting.Handler.openPrintPageForReport
///  - Machinata.Reporting.Handler.openPrintComposer
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version which automatically builds the UI and correct user options:
///  - ```Machinata.Reporting.Tools.printNode```
///
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.printNode = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.printReport = Machinata.Reporting.Tools.printReport;

/// <summary>
/// A handler for printing a specific node. 
/// Typically, such a handler should display a list of common
/// printing options, such as:
///  - Print entire report
///  - Print current chapter
///  - Print this section
///  - Add to print composer
///  - View print composer
///
/// The options displayed depend on which of the following corresponding handlers are implemented:
///  - Machinata.Reporting.Handler.openPrintPageForSection
///  - Machinata.Reporting.Handler.openPrintPageForChapter
///  - Machinata.Reporting.Handler.openPrintPageForReport
///  - Machinata.Reporting.Handler.openPrintComposer
/// 
/// You can use the default supplied version which automatically builds the UI and correct user options: 
///  - ```Machinata.Reporting.Tools.printReport```
///
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.printReport = function(instance) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.printNode = Machinata.Reporting.Tools.printNode;

/// <summary>
/// A handler for printing a specific section (screen) via the nodeInfos screen number.
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForSection = function(instance,nodeInfos) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForSection = null;

/// <summary>
/// A handler for printing a specific chapter via the chapterId (nodeId of the chapter).
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForChapter = function(instance,chapterId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForChapter = null;

/// <summary>
/// A handler for printing an entire report via a reportId. The reportId will always match the currently loaded report of the instance.
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForReport = function(instance,reportId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForReport = null;


/// <summary>
/// A handler for displaying the print composer and it's data to the user.
/// 
/// See ```Machinata.Reporting.PrintComposer``` for more details.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForReport = function(instance,reportId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintComposer = null;

/// <summary>
/// A handler that can change the default icon ids for icon keys. This allows one
/// to use a custom icon set (or icon library) with differing icon ids than the default
/// shipped version.
/// You must support the mapping as defined by dictionary ```Machinata.Reporting.Config.iconNameToBundleIdMapping```.
/// By default this handler is setup to use the current configuration ```iconNameToBundleIdMapping```.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.getIconByKey = function(instance,key) {
///    // return string
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.getIconByKey = function (instance,key) {
    return instance.config.iconNameToBundleIdMapping[key];
};

/// <summary>
/// </summary>
Machinata.Reporting.Handler.addNavigationItem = null;

/// <summary>
/// </summary>
Machinata.Reporting.Handler.addNavigationSibling = null;

/// <summary>
/// A handler to allow customization of chapter navigation.
/// This is usefull if you want to provide a custom way for users to navigation chapters.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addChapterNavigation = function(instance, infosJSON, chapterNodeJSON) {
///    // add your own UI using the infos and chapter JSON
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.addChapterNavigation = null; 

/// <summary>
/// A handler to allow customization of table UI/UX. Typically the default supplied handler should be used.
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.bindTableUI```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.bindTableUI = function(instance,elements) {
///    // elements is a jquery selector of tables
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.bindTableUI = null;

/// <summary>
/// A handler to allow customization of tooltips. Typically the default supplied handler should be used.
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.bindTooltipsUI```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.bindTooltipsUI = function(instance,elements) {
///    // elements is a jquery selector for elements (and their children) for which tooltips should be registered
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.bindTooltipsUI = null;
