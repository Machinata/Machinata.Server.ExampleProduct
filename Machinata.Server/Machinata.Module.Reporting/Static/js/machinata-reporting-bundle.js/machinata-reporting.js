/// <summary>
/// This package is part of the Machinata Core JS library.
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") throw "Machinata Core JS (machinata-core-bundle.js) must be loaded."


/// <summary>
/// Library for dynamically building interactive online reports including powerfull charting framework.
/// 
/// ## Documentation
/// This JS library is completely documented inline. This means you can read the documentation directly
/// within the source code. Alternatively, you can browse the online version at 
/// https://nerves.ch/documentation/machinata-reporting-bundle.js
///
/// ## Dependencies
/// This library has the following dependencies: 
///  - machinata-core-bundle.js
/// 
/// ## Structure
/// Reporting is split into these main blocks:
/// 
///  1. Configuration
///  2. Data
///  3. Index and Building
///  4. Nodes
/// 
/// ## Configuration
/// System for configuring the reporting engine.
/// 
/// ## Data
/// Tools for loading data from different sources, definition of common data format for layouting reports and providing chart data.
/// 
/// ## Index and Building
/// System for automatically compiling the desired and configured report or sub-report based on the full report.
/// 
/// ## Nodes
/// Building blocks that make up a report, including everything from tables to paragraphs to charts.
/// 
/// ## Catalogs and Content
/// The reporting framework not only will display a report, but can also manage the interface and display of catalogs of reports. 
/// These modes can even be combined to create a self-sustainable reporting UI based off of JSON files.
///
/// ## Report Lifecycle
/// Typically the routine cycle of a report should look like this:
/// - Machinata.Reporting.init()
///    - Machinata.Reporting.Data.loadData() >> Load Catalog JSON (if any)
///    - Machinata.Reporting.Data.loadData() >> Load Report JSON
///    - Machinata.Reporting.buildIndex() >> Recursively called
///        - Machinata.Reporting.buildIndex()
///            - Machinata.Reporting.buildIndex()
///                - ...
///    - Machinata.Reporting.buildContents() >> Recursively called
///        - Machinata.Reporting.Node.build()
///            - Machinata.Reporting.Node.ImplementationXYZ.preBuild()
///            - Machinata.Reporting.Node.ImplementationXYZ.init()
///            - Machinata.Reporting.Node.ImplementationXYZ.postBuild()
///        - Machinata.Reporting.buildContents()
///            - Machinata.Reporting.Node.build()
///                - Machinata.Reporting.Node.ImplementationXYZ.preBuild()
///                - Machinata.Reporting.Node.ImplementationXYZ.init()
///                - Machinata.Reporting.Node.ImplementationXYZ.postBuild()
///        - ...
///    - Machinata.Reporting.Node.ImplementationXYZ.draw() >> Draw is called stacked when everything is built
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - ...
///
/// ## Required HTML
/// To get a report injected into your page, you must include the following element and call the init() routine:
/// ```
/// <div class="machinata-reporting-report">
/// </div>
/// <script>
///     var config = {
///         // Your config...
///     };
///     Machinata.ready(function () {
///         Machinata.Reporting.init(config);
///     });
/// </script>
/// ```
/// </summary>
/// <namespace>Machinata</namespace>
/// <name>Machinata.Reporting</name>
/// <type>namespace</type>




/* ======== Variables ========================================================== */



/// <summary>
/// Indicates that a cold start is about to happen.
/// This allows some processes to be skipped on either a cold or warm start to increase performance.
/// </summary>
Machinata.Reporting._coldStart = true;

/// <summary>
/// The UID tracker for reporting nodes. This gets automatically increment as a UID is consumed.
/// </summary>
Machinata.Reporting._uid = 0;

/// <summary>
/// Stores the currently available and registered themes.
/// A mapping from theme key/id to array of colors.
/// </summary>
Machinata.Reporting._themes = {};

/// <summary>
/// Stores the currently available and registered layouts.
/// A mapping from layout key/id to the layout JSON definition.
/// </summary>
Machinata.Reporting._layouts = {};








/* ======== Helper Functions ========================================================== */


/// <summary>
/// Generate a new (unique) reporting UID.
/// </summary>
Machinata.Reporting.uid = function() {
    Machinata.Reporting._uid++;
    return "machinata_reporting_autoid_"+Machinata.Reporting._uid;
};



/* ========  Functions ========================================================== */


/// <summary>
/// Automaticall merges in the given config parameters together with the default configs and the profile configs.
/// This method will also automatically then load the config, thus it should be your final stop for setting up a configuration.
/// </summary>
Machinata.Reporting.mergeAndLoadConfigWithDefaults = function (config, profile) {

    // Load the branding defaults configuration, if any
    if (config != null && config["brandingConfiguration"] != null) {
        Machinata.Reporting.Branding.loadBrandingConfiguration(config["brandingConfiguration"]);
    }

    // Create a new config
    var ret = null;
    var profileConfig = Machinata.Reporting.Profiles.Configs[profile];
    if (profileConfig != null) {
        Machinata.debug("Machinata.Reporting.mergeAndLoadConfigWithDefaults: " + profile);
        ret = Machinata.Util.extend({}, Machinata.Reporting.Config, profileConfig, config);
    } else {
        ret = Machinata.Util.extend({}, Machinata.Reporting.Config, config);
    }
    ret["profile"] = profile;

    // Load the config
    Machinata.Reporting.loadConfig(config);

    // Return the newly loaded and merged config...
    return ret;
};


/// <summary>
/// Applies any of the needed configuration parameters that are not used on-the-fly. If 
/// the configuration has changed for any reason, you should call this method again to ensure
/// the new configuration is properly applied.
/// </summary>
Machinata.Reporting.loadConfig = function (config) {
    Machinata.debug("Machinata.Reporting.loadConfig");
    // Set locales
    if (config.numberFormatLocaleDefinition != null) {
        Machinata.debug("  " + "setting custom numberFormatLocaleDefinition");
        vega.formatLocale(config.numberFormatLocaleDefinition);
        d3.formatLocale(config.numberFormatLocaleDefinition);
    }
    if (config.timeFormatLocaleDefinition != null) {
        Machinata.debug("  " + "setting custom timeFormatLocaleDefinition");
        vega.timeFormatLocale(config.timeFormatLocaleDefinition);
        d3.timeFormatLocale(config.timeFormatLocaleDefinition);
    }
};

/// <summary>
/// Creates empty instance object with all the empty handles needed to represent a instance of a report.
/// </summary>
/// <hidden/>
Machinata.Reporting._createEmptyInstance = function() {
    var instance = {
        uid: Machinata.Reporting.uid(),
        config: null, // Holds the currently merged and applied config JSON.
        catalogData: null, // Holds the currently compiled catalog data (if any).
        reportData: null, // Holds the currently compiled report data (if any).
        rootNode: null, // Holds the current root node of the report.
        catalogCurrentMandate: null, // If using a catalog, stores the currently selected mandate JSON.
        catalogCurrentReport: null, // If using a catalog, stores the currently selected report JSON.
        idToNodeElementLookupTable: {}, // A map from node id to node DOM element.
        idToNodeJSONLookupTable: {}, // A map from node id to node JSON.
        screensToShow: null, // The screens to be displayed. int array
        screens: [], // The discovered screens in the report. Node JSON array
        chapters: [], // The discovered chapters in the report. Node JSON array
        currentScreen: null, // The currently displayed screen (if set)
        totalScreens: null, // The total number of screens.
        focusKey: null, // Parameter from query string (key) which defines a node or section to focus on.
        elems: {
            content: null, // The main report content nodes (root of all reporting content).
            catalog: null, // The catalog UI, if any.
            header: null, // The header of the page, if any. This can be set once globally at any time the DOM is loaded.
            navigation: null, // The navigation of the page, if any. This can be set once globally at any time the DOM is loaded.
            fullscreen: null, // The current fullscreen container for a sub-report, if any
        }
    };
    return instance;
};

/// <summary>
/// Initialize a report, build its toc and content, and create all underlying content and nodes.
/// This will return a instance object with all the relavant handles.
/// </summary>
Machinata.Reporting.init = function (config, onSuccess) {

    // Init and reset
    Machinata.Debugging.startTimer("report-init");

    // Instance object for all relavant handles...
    var instance = Machinata.Reporting._createEmptyInstance();

    // Cold start registration
    var thisInitIsColdStart = Machinata.Reporting._coldStart;
    Machinata.Reporting._coldStart = false;

    // Machinata core settings
    Machinata.Dialogs.MAX_DIALOG_WIDTH = 480;
    Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES = "machinata-reporting-dialog";

    // Resolve the profile
    var profileName = config.profile || "web";
    if (Machinata.hasQueryParameter("profile")) {
        profileName = Machinata.queryParameter("profile");
    }

    // Create merged config
    instance.initConfig = Machinata.Util.cloneJSON(config);
    config = Machinata.Reporting.mergeAndLoadConfigWithDefaults(config, profileName);
    instance.config = config;

    // Apply some config values from query string...
    if (Machinata.queryParameter("prevent-caching") == "true") {
        config.preventDataCaching = true;
    }

    // Cache some selectors
    instance.elems.header = $(config.headerSelector);
    instance.elems.navigation = $(config.navigationSelector);
    instance.elems.content = $(config.contentSelector);
    instance.elems.catalog = $(config.catalogSelector);

    // Allow dom context to get the instance via the content node (machinata-reporting-report)
    instance.elems.content.data("instance", instance);

    // Register the report class
    instance.elems.content.addClass("machinata-reporting-report");

    // Clear DOM
    // Note: migrated to buildReport
    //if (thisInitIsColdStart == false) {
    //    instance.elems.content.children().filter(".machinata-reporting-node").remove();
    //}

    // Initial layout
    Machinata.Reporting.Responsive.updateReportLayout(instance);

    // Url?
    if (config.url == null) {
        config.url = window.location.href;
        config.url = Machinata.removeQueryParameter("chapter", config.url);
        config.url = Machinata.removeQueryParameter("focus", config.url);
        config.url = Machinata.removeQueryParameter("ui", config.url);
    }

    // Profile?
    if (Machinata.Reporting.Handler.setupForProfile != null) Machinata.Reporting.Handler.setupForProfile(config.profile);
    instance.elems.content.addClass("machinata-reporting-profile-"+config.profile);
    // Note: the body classes are still needed by webprint...
    if ($("body").attr("data-machinata-reporting-profile") == null) {
        $("body").addClass("machinata-reporting-profile-" + config.profile);
        $("body").attr("data-machinata-reporting-profile", config.profile);
    }

    // Cache some parameters
    instance.focusKey = Machinata.queryParameter("focus");
    if (instance.focusKey == "") instance.focusKey = null;

    // Show loading in nav
    var rootNavItemElem = null;
    if (config.automaticallyInsertChapterNavigation == true && Machinata.Reporting.Handler.addNavigationItem != null) rootNavItemElem = Machinata.Reporting.Handler.addNavigationItem(instance, "Loading...", "");
    else rootNavItemElem = $("<div/>");

    // Debugging
    Machinata.debug("Machinata.Reporting.init:");

    // Injected CSS
    if (config.injectThemeCSSIntoPage == true) {
        // Already injected?
        var cssElem = $("#machinata-reporting-css");
        if (cssElem.length == 0) {
            // Add a inline stylesheet into page
            cssElem = $("<style id='machinata-reporting-css'>" + Machinata.Reporting.Tools.generateThemeCode(config,"css")+"</style>");
            cssElem.appendTo($("head"));
        }
    }

    // Build UI: register the container and styles to reporting
    instance.elems.content.addClass("machinata-reporting-styling");
    instance.elems.content.addClass("machinata-reporting-container");

    

    // Init screen selector
    if (config.screens == "auto") {
        Machinata.debug("    screens is auto:");
        var screensQueryString = Machinata.queryParameter("screen"); 
        if (screensQueryString == null || screensQueryString == "") {
            instance.screensToShow = null;
            Machinata.debug("        no screens in query, fallback to all");
        } else if (screensQueryString == "printcomposer") {
            Machinata.debug("        screens from printcomposer");
            config.screens = "printcomposer";
            // Note: we now have to do this later since we need the report data
            //instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
        } else {
            Machinata.debug("        screens from query: " + screensQueryString);
            var screenSegsString = screensQueryString.split(",");
            instance.screensToShow = [];
            for (var i = 0; i < screenSegsString.length; i++) {
                instance.screensToShow.push(parseInt(screenSegsString[i]));
            }
        }
    } else if (config.screens == "printcomposer") {
        // We have to wait until all data is loaded...?
        //instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
    } else if (config.screens == "*") {
        // Just show all...
        instance.screensToShow = null;
    } else if (config.screens != null && Array.isArray(config.screens)) {
        // Array of screen numbers
        instance.screensToShow = config.screens;
    } else if (config.screens != null && !isNaN(config.screens)) {
        // Single screen number
        instance.screensToShow = [parseInt(config.screens)];
    } else if (config.screens != null && Machinata.Util.isString(config.screens) && config.screens.indexOf(",") != -1) {
        // Comma separated list
        var screenSegsString = config.screens.split(",");
        instance.screensToShow = [];
        for (var i = 0; i < screenSegsString.length; i++) {
            instance.screensToShow.push(parseInt(screenSegsString[i]));
        }
    }
    if (instance.screensToShow != null) instance.currentScreen = instance.screensToShow[0];

    // Debugging
    Machinata.debug("    chapters: " + config.chapters);
    Machinata.debug("    screensToShow: " + instance.screensToShow);
    Machinata.debug("    currentScreen: " + instance.currentScreen);
    Machinata.debug("    initialChapter: " + config.initialChapter);

    // Cache some attributes needed by other modules
    //instance.elems.content.attr("data-title", json.title);
    //instance.elems.content.attr("data-subtitle", json.subtitle);

    //TODO: build and show loader, expose common loading routines...

    // Figure out catalog source
    if (config.catalogSource == "auto") {
        config.catalogSource = Machinata.queryParameter("catalog");
        if (config.catalogSource == "") config.catalogSource = null;
    }

    Machinata.Debugging.stopTimer("report-init");
    
    // Load catalog data
    Machinata.Debugging.startTimer("catalog-download");
    if (config.loadCatalog == false) config.catalogSource = null;
    Machinata.Reporting.Data.loadData(instance, config.catalogSource, function (catalogData) {
        // Success - catalog loaded
        Machinata.Debugging.stopTimer("catalog-download");

        // Register data singleton instance
        instance.catalogData = catalogData;

        // Compile catalog and it's UI
        if (catalogData != null) {
            Machinata.Reporting.Catalog.processCatalog(instance, config, catalogData);
        }
        if (config.automaticallyBuildCatalogUI == true && catalogData != null) {
            Machinata.Reporting.Catalog.compileCatalogUI(instance, config, catalogData);
        }

        // Set dataSource if missing
        if (config.dataSource == "auto") {
            config.dataSource = Machinata.queryParameter("data");
            if (config.dataSource == "") config.dataSource = Machinata.Reporting.Config.dataSource;
        }

        // Add chapter in nav
        var chapterNavItemElem = null;
        if (config.automaticallyInsertChapterNavigation == true && Machinata.Reporting.Handler.addNavigationItem != null) chapterNavItemElem = Machinata.Reporting.Handler.addNavigationItem(instance, "Loading...", "");
        else chapterNavItemElem = $("<div/>");

        // Load report data - at this point we can't proceed without the index and meta data
        if (config.loadReport == false) config.dataSource = null;
        Machinata.Debugging.startTimer("report-download");
        Machinata.Reporting.Data.loadData(instance, config.dataSource, function (reportData) {
            // Success - data loaded
            Machinata.Debugging.stopTimer("report-download");
            Machinata.Debugging.startTimer("report-preinit");

            // Register data singleton instance
            instance.reportData = reportData;

            // Validate we even have anything
            if (reportData == null) {
                // Rollback the navigation loader...
                var sepElem = rootNavItemElem.prev().prev();
                var posElem = rootNavItemElem.prev();
                sepElem.remove();
                posElem.remove();
                rootNavItemElem.remove();
                return;
            }

            // If we want printcomposer screen, we can get them now since we now know the reportid
            if (config.screens == "printcomposer") {
                instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
            }

            // Apply custom config from data
            if (reportData["config"] != null) {
                config = Machinata.Util.extend({}, config, reportData["config"]);
                instance.config = config;
                Machinata.Reporting.loadConfig(config); // we unfortunately need to reload the config...
            }

            // Cache some common attributes
            config.reportName = Machinata.Reporting.Text.resolve(instance,reportData["reportName"]);
            if (reportData.reportTitle != null) config.reportTitle = Machinata.Reporting.Text.resolve(instance,reportData.reportTitle);
            if (reportData.reportSubtitle != null) config.reportSubtitle = Machinata.Reporting.Text.resolve(instance,reportData.reportSubtitle);
            if (reportData.reportBadge != null) config.reportBadge = reportData.reportBadge;
            Machinata.debug("    reportName: " + config.reportName);
            Machinata.debug("    reportTitle: " + config.reportTitle);
            Machinata.debug("    reportSubtitle: " + config.reportSubtitle);

            // Debug dom elem?
            if (Machinata.DebugEnabled == true) {
                //TODO: re-enable debug features
                //instance.elems.content.append(Machinata.UI.createDebugPanel("Build", Machinata.buildInfo()));
                //instance.elems.content.append(Machinata.UI.createDebugPanel("Report", config));
            }

            // Initial UI update (for report meta data)
            $(".machinata-reporting-attribute-report-title").text(config.reportTitle);
            $(".machinata-reporting-attribute-report-subtitle").text(config.reportSubtitle);
            window.document.title = window.document.title.replace("Loading...", config.reportTitle);
            rootNavItemElem.find(".link").text(config.reportTitle);
            if (instance.catalogCurrentMandate != null) {
                rootNavItemElem.find(".link").attr("href", instance.catalogCurrentMandate.catalogURLResolved);
            } else {
                rootNavItemElem.find(".link").attr("href", config.url);
            }

            // UI update for data status
            if (config.reportBadge != null) {
                var badgeElem = $(".machinata-reporting-attribute-report-badge");
                if (config.reportBadge.color != null) badgeElem.addClass("option-" + config.reportBadge.color);
                badgeElem.text(Machinata.Reporting.Text.resolve(instance,config.reportBadge.label));
                if (config.reportBadge.tooltip != null) badgeElem.attr("title", Machinata.Reporting.Text.resolve(instance,config.reportBadge.tooltip));
                badgeElem.addClass("has-content");
            } else {
                $(".machinata-reporting-attribute-report-badge").remove();
            }

            // Set window title
            // Here we use the config reportWindowTitle, unless we are in webprint, in which case we try to use the filename...
            if (config.reportWindowTitle != null) {
                var windowTitleTemplateString = config.reportWindowTitle;
                if (config.profile == Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY && reportData != null && reportData.fileName != null) {
                    windowTitleTemplateString = reportData.fileName;
                }
                var windowTitle = Machinata.Reporting.Tools.createTitleString(instance, config, windowTitleTemplateString);
                if (windowTitle != null) window.document.title = windowTitle;
            }
            
            // Build index recursively
            Machinata.Debugging.stopTimer("report-preinit");
            Machinata.Debugging.startTimer("report-buildindex");
            instance.rootNode = reportData["body"];
            Machinata.debug("Machinata.Reporting.buildIndex:");
            Machinata.Reporting.buildIndex(instance, config, instance.rootNode, null);
            Machinata.Debugging.stopTimer("report-buildindex");

            // Validate we have at least one chapter
            //if (instance.chapters.length == 0) {
            //    Machinata.debug("   **Setting default chapter");
            //    instance.chapters.push(rootNode.children[0]);
            //}

            // Show what happend
            Machinata.debug("    index complete!");
            Machinata.debug("    totalScreens: " + instance.totalScreens);
            Machinata.debug("    totalChapters: " + instance.chapters.length);

            // Page tools
            if (config.automaticallyAddPageTools == true) {
                Machinata.Reporting.initPageTools(instance);
            }

            // Auto-chapter?
            if (config.chapters == "auto") {
                config.chapters = [];
                // Do we have from query param?
                if (Machinata.hasQueryParameter("chapter")) {
                    config.chapters.push(Machinata.queryParameter("chapter"));
                } else if (config.initialChapter != null) {
                    if (typeof config.initialChapter == 'number') {
                        config.chapters.push(instance.chapters[config.initialChapter - 1].id);
                    } else {
                        config.chapters.push(config.initialChapter);
                    }
                } else {
                    if (instance.chapters.length > 0) {
                        config.chapters.push(instance.chapters[0].id);
                    }
                }
            }

            // Load the chapter title, if we have selected only a single chapter
            if (config.chapters != null && config.chapters.length == 1) {
                Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                    if (config.chapters[0] == chapterJSON.id) {
                        if (config.reportChapter == null) {
                            config.reportChapter = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                        }
                    }
                });
            }
            if (config.reportChapter == null) {
                config.reportChapter == ""; //fallback
            }

            // Update some attributes that might have only come up after build index
            $(".machinata-reporting-attribute-report-disclaimer").text(config.reportDisclaimer);
            $(".machinata-reporting-attribute-report-legal-entity").text(config.reportLegalEntity);
            $(".machinata-reporting-attribute-report-chapter").text(config.reportChapter);
            $(".machinata-reporting-attribute-catalog-url").attr("href", config.catalogURL);


            // Create chapters UI
            if (config.automaticallyFoldChaptersIntoPages == true) {
                $(config.chapterSelector).each(function () {
                    var chapterTabsElem = $(this);
                    var numChapters = 0;
                    Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                        var chapterTitle = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                        // Create chapter button
                        var chapterButtonElem = $("<li></li>").attr("data-chapter", chapterJSON.chapterId).append($("<a></a>").text(chapterTitle));
                        chapterTabsElem.append(chapterButtonElem);
                        chapterButtonElem.find("a").attr("href", chapterJSON.chapterURL);
                        // Manage selection
                        if (config.chapters != null && config.chapters.length == 1) {
                            if (config.chapters[0] == chapterJSON.id) {
                                chapterButtonElem.find("a").addClass("is-active");
                            }
                        }
                        numChapters++;
                    });
                    if (numChapters <= 1) chapterTabsElem.hide();
                    // Autobalance the tabs
                    chapterTabsElem.find("li").css("width", (100 / numChapters) + "%");
                });
            }

            if (config.automaticallyInsertChapterNavigation && chapterNavItemElem != null) {
                Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                    var chapterTitle = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                    // Manage selection
                    if (config.chapters != null && config.chapters.length == 1) {
                        if (config.chapters[0] == chapterJSON.id) {
                            chapterNavItemElem.find(".link").text(chapterTitle);
                        }
                    }
                    // Register sibling
                    if (instance.chapters.length == 1) return;
                    if (Machinata.Reporting.Handler.addNavigationSibling != null) {
                        var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, chapterTitle, chapterJSON.chapterURL);
                    }
                });
                if (instance.chapters != null && instance.chapters.length == 0) {
                    chapterNavItemElem.remove(); //TODO: what about the slash in CS?
                }
            } else {
                //TODO: handle alternative?
            }

            // Build report content

            // Build content recursively
            if (config.buildContents == true) {
                Machinata.Debugging.startTimer("report-buildcontents");
                Machinata.Reporting.buildReport(instance, config);
                Machinata.Debugging.stopTimer("report-buildcontents");
            }


            Machinata.Debugging.startTimer("report-postinit");

            // Initial focus?
            if (instance.focusKey != null) {
                var linkJSON = {
                    nodeId: instance.focusKey
                };
                Machinata.Reporting.Tools.drilldownOnNode(instance,linkJSON);
            }

            // Fullscreen profile?
            if (config.profile == Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY) {
                // Bind arrow keys (only for fullscreen)
                $(document).keydown(function (e) {
                    if (e.which == 37) {
                        // LEFT
                        e.preventDefault();
                        Machinata.Reporting.Tools.prevScreen(instance);
                    } else if (e.which == 39) {
                        // RIGHT
                        e.preventDefault();
                        Machinata.Reporting.Tools.nextScreen(instance);
                    }

                });
            }

            // Webprint profile?
            if (config.profile == Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY) {
                if (config.automaticallyOpenBrowserPrintDialog == true || Machinata.queryParameter("print-dialog") == "true") {
                    setTimeout(function () {
                        Machinata.Reporting.Tools.openBrowserPrintDialog(instance);
                    }, 300); // seems the browser needs some breathing room so that it can properly detect the margins etc...
                }
            }

            // Insert a dom element for headless and automated systems to detect (like screenshot services)
            instance.elems.content.append("<div class='machinata-reporting-init-complete'/>");

            // Success
            if (onSuccess) onSuccess(instance,config);
            
            Machinata.Debugging.stopTimer("report-postinit");

            // Insert stats?
            if (Machinata.queryParameter("stats") == "true") {
                instance.elems.content.prepend(Machinata.Debugging.createTimersDebugPanel("Stats").addClass("option-margined"));
            }

            // Notice?
            if (reportData != null && reportData["reportNotice"] != null) {
                var key = "noticeshowed_" + reportData.reportId;
                if (Machinata.getCache(key) == null) {
                    setTimeout(function () {
                        Machinata.setCache(key, true);
                        Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.notice"), Machinata.Reporting.Text.resolve(instance,reportData["reportNotice"])).show();

                    }, 600);
                }
            }

        });

    });

    // Cold start UI bindings
    // These happend immediately and must remain valid throughout multiple inits
    if (thisInitIsColdStart == true) {

        // Bind tooltips via handler, if any
        if (Machinata.Reporting.Handler.bindTooltipsUI != null) {
            Machinata.Reporting.Handler.bindTooltipsUI(instance, $(".machinata-reporting-styling")); // bind to root of document
        }

        // Register window resize events
        $(window).resize(function () {
            $(".machinata-reporting-report").each(function () {
                Machinata.Reporting.Responsive.updateForContainerSize($(this).data("instance"));
            });
        });

        // Bind resizes
        /*
        Machinata.Responsive.onResize(function () {
            // Note: this is called before onResize
            // Update all report sizes
            $(".machinata-reporting-report").each(function () {
                Machinata.Reporting.Responsive.updateReportLayout($(this).data("instance"));
            });
        });
        Machinata.Responsive.onResized(function () {
            // Note: this is called after onResize
            // Update all nodes
            TODO: should we cache this selector?
            EXPENSIVE:
            $(".machinata-reporting-node").trigger("machinata-reporting-redraw-node");
        });*/

        // Readprogress
        $(".machinata-reporting-readprogress.option-autoscroll").each(function () {
            var elem = $(this);
            var progressElem = elem.find(".progress");
            var subtract = 0;
            if ($("#footer").length != 0) subtract += $("#footer").height();
            if ($("#legal").length != 0) subtract += $("#legal").height();
            $(window).scroll(function () {
                var h = document.documentElement,
                    b = document.body,
                    st = 'scrollTop',
                    sh = 'scrollHeight';
                var percent = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight - subtract) * 100;
                if (percent == 0) elem.addClass("is-zero");
                else elem.removeClass("is-zero");
                progressElem.css("width", percent + "%");
            });
        });

    }


    return instance;

};

/// <summary>
/// This function updates and binds all the page tools that reside outside of the machinata-reporting main container.
/// </summary>
Machinata.Reporting.initPageTools = function (instance) {
    // General page tools
    // Fullscreen
    if (Machinata.Reporting.Handler.addPageTool != null && instance.totalScreens > 0) {
        Machinata.Reporting.Handler.addPageTool(instance, "View fullscreen", "fullscreen", function () {
            Machinata.Reporting.Tools.openFullscreenReport(instance);
        });
    }
    
    // Resources
    if (instance.reportData != null && instance.reportData.reportResources != null && Machinata.Reporting.Handler.addPageTool != null) {
        Machinata.Util.each(instance.reportData.reportResources, function (index, resourceJSON) {
            Machinata.Reporting.Handler.addPageTool(
                instance,
                Machinata.Reporting.Text.resolve(instance,resourceJSON.tooltip),
                resourceJSON.icon,
                function () {
                    if (resourceJSON.link != null) {
                        // Link
                        Machinata.Reporting.Tools.openLink(instance,resourceJSON.link);
                    } else if (resourceJSON.attachment != null) {
                        // Attachment
                        if (resourceJSON.downloadAttachmentToBrowser == true) {
                            Machinata.Data.exportDataAsBlob(
                                resourceJSON.attachment.data,
                                resourceJSON.attachment.mimeType,
                                resourceJSON.attachment.filename
                            );
                        } else {
                            Machinata.Data.openDataInNewWindow(
                                resourceJSON.attachment.data,
                                resourceJSON.attachment.mimeType,
                                resourceJSON.attachment.filename
                            );
                        }
                    }
            });
        });
    }
    // Printing (only if we have screens)
    if (Machinata.Reporting.Handler.addPageTool != null && instance.screens != null && instance.screens.length > 0) {
        if (Machinata.Reporting.Handler.printReport != null) {
            Machinata.Reporting.Handler.addPageTool(instance, Machinata.Reporting.Text.translate(instance, "reporting.printing.print"), "print", function () {
                Machinata.Reporting.Handler.printReport(instance);
            });
        }
    }
    // Change date
    // NOTE: The calendar change date tool must be the last tool to be added!
    if (Machinata.Reporting.Handler.addPageTool != null && instance.catalogCurrentMandate != null && Machinata.Reporting.Handler.changeReportingDate != null) {
        // Add the calendar tool
        Machinata.Reporting.Handler.addPageTool(instance, Machinata.Reporting.Text.translate(instance, "reporting.change-date"), "calendar", function () {
            Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, instance.catalogCurrentMandate, instance.catalogCurrentReport);
        });
        // Add subtitle as text tool
        if (instance.config.insertReportSubtitleAsPageTool == true && instance.reportData != null && instance.reportData.reportSubtitle != null) {
            var subtitle = Machinata.Reporting.Text.resolve(instance, instance.reportData.reportSubtitle);
            Machinata.Reporting.Handler.addPageTool(instance, subtitle, null, function () {
                Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, instance.catalogCurrentMandate, instance.catalogCurrentReport);
            }, Machinata.Reporting.Text.translate(instance, "reporting.change-date"),false);
        }
    }
    
};


/// <summary>
/// Recursivley builds the index of the report. This especially has to do with figuring
/// out the logical screen numbers for a report, which allows one to seperate
/// the chapters out in regular display, but still speak of univeral screen numbers
/// across the entire report.
/// </summary>
Machinata.Reporting.buildIndex = function (instance, config, json, parent) {
    // Re-init
    instance.screens = [];

    // Recursively build index
    Machinata.Reporting._buildIndexRecursively(instance, config, json, parent);

    // Register num screens
    instance.totalScreens = instance.screens.length;

    return json;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting._buildIndexRecursively = function (instance, config, json, parent) {

    if (json == null) json = instance.rootNode;

    if (json.enabled == false) return;

    // ID
    if (json.id == null) json.id = Machinata.Reporting.uid();
    instance.idToNodeJSONLookupTable[json.id] = json;

    // Tags?
    if (json.tags != null) {
        for (var i = 0; i < json.tags.length; i++) {
            var tag = json.tags[i];
            if (tag == config.tagDisclaimer) {
                json.exclude = true;
                Machinata.debug("    Report_Disclaimer tag, excluding");
                config.reportDisclaimer = Machinata.Reporting.Text.resolve(instance,json.content);
            } else if (tag == config.tagLegalEntity) {
                json.exclude = true;
                Machinata.debug("    Report_LegalEntity tag, excluding");
                config.reportLegalEntity = Machinata.Reporting.Text.resolve(instance,json.content);
            } else if (tag == config.tagScreen) {
                Machinata.debug("    Screen tag, marking as screen");
                json.screen = true;
            } else if (tag == config.tagDarkChrome) {
                Machinata.debug("    Dark tag, setting chrome to dark");
                json.chrome = "dark";
            } else if (tag == config.tagLightChrome) {
                Machinata.debug("    Light tag, setting chrome to dark");
                json.chrome = "light";
            } else if (tag == config.tagSolidChrome) {
                Machinata.debug("    Solid tag, setting chrome to solid");
                json.chrome = "solid";
            } else if (tag == config.tagWebPrintOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY) json.exclude = true;
            } else if (tag == config.tagWebOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_WEB_KEY) json.exclude = true;
            } else if (tag == config.tagPrintOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_PRINT_KEY) json.exclude = true;
            } else if (tag == config.tagFullscreenOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY) json.exclude = true;
            }
        }
    }

    // Level
    if (parent == null) {
        json.level = 0;
        json.screenNumber = null;
        json.chapterId = null;
    } else {
        json.level = parent.level + 1;
        json.screenNumber = parent.screenNumber;
        json.chapterId = parent.chapterId;
    }

    // Chapter book keeping
    if (json.level == config.chapterLevel && json.nodeType == "SectionNode") {
        if(json.mainTitle != null) {
            json.chapterId = json.id;
            json.chapter = true; // helper marker
            if (config.headless == false) {
                json.chapterURL = Machinata.updateQueryString("chapter", json.chapterId, config.url);
                json.chapterURL = Machinata.removeQueryParameter("focus", json.chapterURL);
                json.chapterURL = Machinata.removeQueryParameter("screen", json.chapterURL);
                json.chapterURL = Machinata.removeQueryParameter("ui", json.chapterURL);
            }
            instance.chapters.push(json);
            Machinata.debug("    new chapter: " + json.chapterId);
            if (Machinata.Reporting.Handler.addChapterNavigation != null) {
                var infos = {};
                infos.chapterURL = json.chapterURL;
                infos.chapterId = json.chapterId;
                infos.chapterTitle = Machinata.Reporting.Text.resolve(instance,json.mainTitle);
                Machinata.Reporting.Handler.addChapterNavigation(instance, infos, json);
            }
        }
    }


    // Screen number book keeping
    if (json.level == config.screenLevel && config.automaticallyGenerateScreenNumbers == true) {
        json.screenNumber = instance.screens.length;
        instance.screens.push(json);
        Machinata.debug("    new screen: " + json.screenNumber);
    } else if(json.screen == true) {
        json.screenNumber = instance.screens.length;
        instance.screens.push(json);
        Machinata.debug("    new screen: " + json.screenNumber);
    }

    // Inherit theme
    if (json.theme == null && parent != null) json.theme = parent.theme;

    // Validate theme
    if (json.theme != null && Machinata.Reporting.getTheme(json.theme) == null) {
        console.warn("The theme '"+json.theme+"' for node "+json.id+" is not valid.");
        json.theme = "default";
    }

    // URL
    if (config.headless == false && json.url == null) {
        json.url = Machinata.updateQueryString("focus", json.id, config.url);
        json.url = Machinata.updateQueryString("chapter", json.chapterId, json.url);
    }

    // Debug
    Machinata.debug("    id: " + json.id);
    Machinata.debug("        level: " + json.level);
    Machinata.debug("        screen: " + json.screenNumber);

    
    //TODO: fullscreen API: migrate to buildContent
    /*
    // Matches?
    if (instance.screensToShow != null) {
        var matches = false;
        for (var i = 0; i < instance.screensToShow.length; i++) {
            if (json.screenNumber == instance.screensToShow[i]) {
                matches = true;
                Machinata.debug("    ***matches " + instance.screensToShow[i]);
                break;
            }
        }
        if (matches == false) {
            json.exclude = true;
            Machinata.debug("    no match, excluded");
        } else {
            json.exclude = false;
            Machinata.debug("    ***match!");
        }
    }
    */

    // Children
    Machinata.Util.each(json.children, function (index, childJSON) {
        Machinata.Reporting._buildIndexRecursively(instance, config, childJSON, json);
        // Inherit child exclude to make sure titles trickle up on screen matching
        //TODO: fullscreen API: migrate to buildContent
        //if (instance.screensToShow != null && childJSON.exclude == false) {
        //    json.exclude = false;
        //}
    });
};

/// <summary>
/// Builds a report for the configuration
/// </summary>
Machinata.Reporting.buildReport = function (instance, config) {

    //var reportData = Machinata.Util.cloneJSON(instance.rootNode);
    var reportData = instance.rootNode;

    // Recursive routine to update the screen inclusion
    // Note: this is not as straight forward as one might think, as the screen
    // flag will trickle back up to the root node to allow a screen to include a section title
    // (ie setting screen=true on a subnode will automatically select all the parent nodes to ensure the subnode is included)
    function _updateScreenInclusion(instance, config, json, parent) {
        if (instance.screensToShow != null) {
            // Matches?
            var matches = false;
            for (var i = 0; i < instance.screensToShow.length; i++) {
                if (json.screenNumber == instance.screensToShow[i]) {
                    matches = true;
                    Machinata.debug("    ***matches " + instance.screensToShow[i]);
                    break;
                }
            }
            if (matches == false) {
                json.exclude = true;
                Machinata.debug("    no match, excluded");
            } else {
                json.exclude = false;
                Machinata.debug("    ***match!");
            }
        } else {
            json.exclude = false;
        }
        // Recursively update each child, based on the same logic
        Machinata.Util.each(json.children, function (index, childJSON) {
            // Inherit child exclude to make sure titles trickle up on screen matching
            _updateScreenInclusion(instance, config, childJSON, json);
            // If the child was marked as include, mark self as include as well
            if (childJSON.exclude == false) {
                json.exclude = false;
            }
        });
    };
    _updateScreenInclusion(instance, config, reportData, null);
    
    // Remove all existing within content selector
    instance.elems.content.children().filter(".machinata-reporting-node").remove();

    // Build content recursively
    if (config.buildContents == true) {
        Machinata.Debugging.startTimer("report-buildcontents");
        Machinata.Reporting.buildContents(instance, config, reportData, null);
        Machinata.Debugging.stopTimer("report-buildcontents");
    }

    // Bind tooltips via handler, if any
    if (Machinata.Reporting.Handler.bindTooltipsUI != null) {
        Machinata.Reporting.Handler.bindTooltipsUI(instance, $(".machinata-reporting-styling")); 
    }

    // Bind new responsive elements
    //Machinata.Debugging.startTimer("report-responsive");
    //Machinata.Responsive.detectNewResponsiveElements();
    //Machinata.Responsive.updateResponsiveElements();
    //Machinata.Debugging.stopTimer("report-responsive");

    // Trigger initial redraw
    //EXPENSIVE:
    Machinata.Debugging.startTimer("report-firstdraw");
    //instance.elems.content.find(".machinata-reporting-node").trigger("machinata-reporting-redraw-node");
    Machinata.Reporting.Responsive.updateForContainerSize(instance);
    Machinata.Debugging.stopTimer("report-firstdraw");
};

/// <summary>
/// Recursivley builds the actual contents of the report, making sure
/// to respect any configuration regarding screen or chapter selection.
/// </summary>
Machinata.Reporting.buildContents = function (instance, config, json, parent, insertionElem) {

    // Disabled? If so, skip
    if (json.enabled == false) return;
    if (json.exclude == true) return;

    //json = Machinata.Util.cloneJSON(json); // <<< very expensive

    // ID sanity - not all nodes come from a built index, some are built on the fly, so we 
    // assign an id if not already
    if (json.id == null) {
        json.id = Machinata.Reporting.uid();
    }
    instance.idToNodeJSONLookupTable[json.id] = json;

    Machinata.debug("Machinata.Reporting.buildContents:");
    Machinata.debug("    type: " + json.nodeType);
    Machinata.debug("    id: " + json.id);
    Machinata.debug("    level: " + json.level);

    // Include via chapters param?
    // Note: we only exclude if we are past the chapters level (since we need the root node for a chapter)
    var include = true;
    if (json.level >= config.chapterLevel && config.chapters != null && config.chapters != "*" && config.chapters.length != 0) {
        include = false;
        for (var i = 0; i < config.chapters.length; i++) {
            if (config.chapters[i] == json.chapterId) {
                include = true;
                Machinata.debug("    excluding due to chapter mismatch!");
                break;
            }
        }
    }
    if (include == false) {
        return;
    }

    // Parent elem
    // If we dont specify the insertion point, we get the childrens elem from the parent json dom elem
    var parentElem = null;
    if (insertionElem != null) parentElem = insertionElem;
    if (parent != null && parentElem == null) {
        if (parent.domElem != null && parent.domElem.data("children") != null) {
            parentElem = parent.domElem.data("children");
        } else {
            parentElem = parent.domElem;
        }
    }
    
    if (parentElem == null) parentElem = instance.elems.content;

    // Create node elem
    var sizerElem = null;
    var nodeElem = null;
    if (config.useSizerParentElemForNodes == true) {
        sizerElem = $("<div class='machinata-reporting-node-sizer'></div>").appendTo(parentElem);
        nodeElem = Machinata.Reporting.Node.build(instance, config, json, sizerElem);
    } else {
        nodeElem = Machinata.Reporting.Node.build(instance, config, json, parentElem);
    }
    json.domElem = nodeElem;

    // Children...
    if (json.processChildren != false) { // could be null
        Machinata.Util.each(json.children, function (index, childJSON) {
            if (childJSON.nodeType == "StopProcessNode") return false; // For debugging only, special node type that stops the tree from resolving...
            Machinata.Reporting.buildContents(instance, config, childJSON, json);
        });
    }

    // Post build?
    var impl = Machinata.Reporting.Node[json.nodeType];
    if (impl != null && impl.postBuild != null) {
        impl.postBuild(instance, config, json, nodeElem);
    }

    // Clearer
    //$("<div class='machinata-reporting-section-end'></div>").addClass("level-" + level).appendTo(sectionElem);

    // Focus target?
    //TODO
    //if (instance.focusKey != null && json.key != null && instance.focusKey == json.key) {
    //    Machinata.Reporting.Elems.focusElement = sectionElem;
    //    Machinata.debug("***found focus element (section): " + json.key);
    //};

    return nodeElem;
};

/// <summary>
/// Registers multiple layouts at the same time by providing an key value map of layouts
/// where each key is the layout name and the value is the layout JSON.
/// See ```Machinata.Reporting.registerLayout```
/// </summary>
Machinata.Reporting.registerLayouts = function (layoutsJSON) {
    Machinata.debug("Machinata.Reporting.registerLayouts:");
    Machinata.Util.each(layoutsJSON, function (key, val) {
        Machinata.debug("  "+key);
        Machinata.Reporting.registerLayout(key, val);
    });
};


/// <summary>
/// Registers a new layout with the layouting system. Layouts registered here
/// a primarily used by the LayoutNode, though any component in the reporting system
/// may make use of layouts.
/// 
/// The layoutJSON contains an array of children definitions called ```children```. This
/// property is used to define (extend, overwrite) each slot child's properties according. Typically this
/// achieved by defining the ```style``` and ```chrome``` of each child slot. Each slot definition must also
/// contain the ```slotKey``` property that defines the name of the slot.
/// </summary>
/// <example>
/// ```
/// ### Layout JSON (definition)
/// var layoutJSON = {
///     "style": "CS_CustomStyleToAddToLayout",
///     "children": [
///         {
///             "slotKey": "{left}",
///             "style": "CS_W_Block1x1"
///         },
///         {
///             "slotKey": "{center}",
///             "style": "CS_W_Block2x1"
///         },
///         {
///             "slotKey": "{right}",
///             "style": "CS_W_Block1x1"
///         }
///     ]
/// };
/// Machinata.Reporting.registerLayout("CS_W_Overview_PanelTrailerSmall",layoutJSON);
/// ```
///
/// ### Layout Node (usage)
/// ```
/// {
///     "nodeType": "LayoutNode",
///     "style": "CS_W_Overview_PanelTrailerSmall",
///     "slotChildren": {
///         "{left}": 0,
///         "{center}": 1,
///         "{right}": 2
///     },
///     "children": [
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Left node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Center node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Right node"
///             }
///         }
///     ]
/// }
/// ```
/// </example>
Machinata.Reporting.registerLayout = function (name, layoutJSON) {
    // Register
    layoutJSON.name = name;
    Machinata.Reporting._layouts[name] = layoutJSON;
};

/// <summary>
/// </summary>
Machinata.Reporting.getLayout = function (name) {
    return Machinata.Reporting._layouts[name];
};



/// <summary>
/// Register a new global theme for the report. This should be called before the ```init()``` method.
/// Each theme used by the node property ```theme``` should be registered here so that
/// charting libraries can access the themes.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.registerTheme('blue', ['#003868', '#004c97', '#0072ce', '#adc8e9', '#dadada', '#a8a8a7']); // dark,mid,bright,light,gray3,gray4 
/// ```
/// </example>
Machinata.Reporting.registerTheme = function (name, colors) {
    // Register
    Machinata.Reporting._themes[name] = colors;
    // Register with Vega
    vega.scheme(name, colors);
};

/// <summary>
/// </summary>
Machinata.Reporting.getTheme = function (name) {
    return Machinata.Reporting._themes[name];
};

/// <summary>
/// </summary>
Machinata.Reporting.getThemeColorByIndex = function (name, index) {
    var theme = Machinata.Reporting._themes[name];
    var c = theme[(index % theme.length)];
    return c;
};

/// <summary>
/// </summary>
Machinata.Reporting.getThemeColorByShade_DEPRECATED = function (name, shade) {
    // dark,mid,bright,light,gray3,gray4 
    if(shade == "dark") return Machinata.Reporting._themes[name][0];
    if (shade == "mid") return Machinata.Reporting._themes[name][1];
    if (shade == "bright") return Machinata.Reporting._themes[name][2];
    if (shade == "light") return Machinata.Reporting._themes[name][3];
    if (shade == "gray") return Machinata.Reporting._themes["gray"][0];
    if (shade == "gray1") return Machinata.Reporting._themes["gray"][0];
    if (shade == "gray2") return Machinata.Reporting._themes["gray"][1];
    if (shade == "gray3") return Machinata.Reporting._themes["gray"][2];
    if (shade == "gray4") return Machinata.Reporting._themes["gray"][3];
    if (shade == "gray5") return Machinata.Reporting._themes["gray"][4];
    if (shade == "gray6") return Machinata.Reporting._themes["gray"][5];
};



/// <summary>
/// Get a full icon class through the standardized icon key.
/// Currently the following standard icons exist:
///  - ```alert```
///  - ```info```
///  - ```trend-up```
///  - ```trend-down```
///  - ```fullscreen```
///  - ```print```
///  - ```check```
///  - ```copy```
///  - ```filter```
///  - ```export```
///  - ```download```
///  - ```report```
///  - ```arrow-left```
///  - ```arrow-right```
/// </summary>
Machinata.Reporting.icon = function (instance,key) {
    if (Machinata.Reporting.Handler.getIconByKey == null) return null;
    return Machinata.Reporting.Handler.getIconByKey(instance,key);
};

Machinata.Reporting.buildIcon = function (instance, key) {
    if (instance.config.iconsBundle == null) throw "Machinata.Reporting.buildIcon: iconsBundle cannot be null";
    var iconsBundle = instance.config.iconsBundle.replace("{" + "assets-path" + "}", instance.config.assetsPath);
    var iconName = Machinata.Reporting.Handler.getIconByKey(instance,key);
    var iconURL = iconsBundle + "#" + iconName;
    return Machinata.UI.buildSVGIcon(iconName, iconURL).addClass("icon-"+key);
};

/// <summary>
/// Creates a toolbar and returns the element.
/// </summary>
Machinata.Reporting.buildToolbar = function (instance, title, subtitle) {
    var toolbarElem = $('<div class="machinata-reporting-toolbar DEPRECATED_ui-toolbar">');
    //if (config.chrome != null) toolbarElem.addClass("option-" + config.chrome);
    if (title != null) {
        var titleLabelElem = $('<label class="title theme-headline-font"></label>').text(title);
        toolbarElem.append(titleLabelElem);
        Machinata.UI.autoShowTextAsTooltipOnTruncated(titleLabelElem);
    }
    if (subtitle != null) {
        var subtitleLabelElem = $('<label class="subtitle"></label>').text(subtitle);
        toolbarElem.append(subtitleLabelElem);
        Machinata.UI.autoShowTextAsTooltipOnTruncated(subtitleLabelElem);
    }
    toolbarElem.append($('<div class="tabs"></div>'));
    toolbarElem.append($('<div class="tools"></div>'));
    return toolbarElem;
};


/// <summary>
/// Creates a toolbar and returns the element.
/// </summary>
Machinata.Reporting.buildMenubar = function (instance, options) {
    if (options == null) options = {};
    var menubarElem = $('<div class="machinata-reporting-menubar"><ul class="tabs"></ul></div>');
    if (options.softMenuBar == true) menubarElem.addClass("option-soft");
    else menubarElem.addClass("option-hard");
    return menubarElem;
};


/// <summary>
/// Creates a subtoolbar and returns the element.
/// </summary>
/// <deprecated/>
Machinata.Reporting.buildSubToolbar = function (instance, options) {
    var toolbarElem = $('<div class="machinata-reporting-subtoolbar DEPRECATED_ui-toolbar option-small">');
    //if (config.chrome != null) toolbarElem.addClass("option-" + config.chrome);
    toolbarElem.append($('<div class="tabs"></div>'));
    toolbarElem.append($('<label class="hint"></label>'));
    toolbarElem.append($('<div class="tools"></div>'));
    return toolbarElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.addMenubarButton = function (instance, elem, title, action, options) {
    
    if (elem == null) return; //TODO: handle better?
    if (options == null) options = {};

    var additionalClasses = null;
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    
    // Get the nodes menubar
    var menubarElem = elem.data("menubar");
    if (menubarElem == null) {
        // Create a new menubar
        menubarElem = Machinata.Reporting.buildMenubar(instance,options);
        elem.data("menubar", menubarElem); 
    }
    if (menubarElem != null) {
        // Create new button
        var buttonElem = $("<li><a></a></li>");
        buttonElem.click(function () { action(buttonElem); });
        buttonElem.find("a").text(title);
        // Add to menubar
        menubarElem.find(".tabs").append(buttonElem);
        return buttonElem;
    } else {
        return null;
    }
};


/// <summary>
/// Adds a tool to the node toolbar.
///     action: click handler
///     options: space seperated string of options (classes)
/// </summary>
Machinata.Reporting.addTool = function (instance, elem, title, icon, action, options) {
    //if (instance.config["suppress-tools"] == true) return; // DEPRECATED: seems unused
    if (elem == null) return; //TODO: handle better?

    var additionalClasses = null;
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var ownerId = elem.attr("id");
    if (elem.hasClass("option-parent-toolbar")) {
        elem = elem.parent().closest(".machinata-reporting-node");
        additionalClasses = "belongs-to-child-node";
    }
    var toolbarElem = elem.data("toolbar");
    if (toolbarElem != null) {
        var toolElem = Machinata.Reporting.addToolToToolbar(instance, elem, toolbarElem, title, icon, action, options);
        toolElem.attr("data-owner-id", ownerId);
        toolElem.addClass(additionalClasses);
        return toolElem;
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addSubTool = function (instance, elem, title, icon, action, options) {
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var toolbarElem = elem.data("subtoolbar");
    if (toolbarElem != null) {
        return Machinata.Reporting.addToolToToolbar(instance, elem, toolbarElem, title, icon, action, options);
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addSubTab = function (instance, elem, title, icon, action, options) {
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var toolbarElem = elem.data("subtoolbar");
    if (toolbarElem == null) {
        // Not yet created, create it
        alert("no toolbar yet");
    }
    if (toolbarElem != null) {
        return Machinata.Reporting.addTabToToolbar(instance, elem, toolbarElem, title, icon, action, options);
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addToolToToolbar = function (instance, elem, toolbarElem, title, icon, action, options) {
    // Icon
    var toolElem = $("<div class='tool'></div>").appendTo(toolbarElem.find(".tools"));
    toolElem.append(Machinata.Reporting.buildIcon(instance,icon));
    toolElem.click(function () { action(toolElem); });
    toolbarElem.addClass("option-has-tool");
    if (options != null) toolElem.addClass(options);
    if (toolbarElem == elem.data("toolbar")) elem.addClass("option-has-tool");
    if (toolbarElem == elem.data("subtoolbar")) elem.addClass("option-has-subtool");
    // Title interaction
    if (title != null) {
        toolElem.attr("title", title);
    }
    // Bookkeeping
    toolbarElem.attr("data-num-tools", toolbarElem.find(".tools .tool").length);
    return toolElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.addTabToToolbar = function (instance, elem, toolbarElem, title, icon, action, options) {
    // Icon
    var tabElem = $("<div class='tab'></div>").text(title).appendTo(toolbarElem.find(".tabs"));
    tabElem.click(function () { action(tabElem); });
    if (options != null) tabElem.addClass(options);
    toolbarElem.addClass("option-has-tab");
    if (toolbarElem == elem.data("toolbar")) elem.addClass("option-has-tab");
    if (toolbarElem == elem.data("subtoolbar")) elem.addClass("option-has-subtab");
    return tabElem;
};















