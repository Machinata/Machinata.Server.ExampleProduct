


/// <summary>
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ComponentNode = {};
Machinata.Reporting.Node.ComponentNode.defaults = {
    supportsToolbar: false,
};

Machinata.Reporting.Node.ComponentNode.init = function (instance, config, json, nodeElem) {

    // Flatten slotChildren into actual child
    $.each(json.children, function (index, childJSON) {
        // Find matching reference (by id) in slotChildren
        $.each(json.slotChildren, function (key, value) {
            if (value == index) {
                // We have a match
                // Register directly in child json
                childJSON.slotNumber = value;
                childJSON.slotKey = key;
                childJSON.slotClass = key.replace("{", "").replace("}", "");
                childJSON.additionalCSSClasses = "slot-child slot-"+childJSON.slotNumber + " slot-" + childJSON.slotClass;
                return;
            }
        });
    });


};
Machinata.Reporting.Node.ComponentNode.postBuild = function (instance, config, json, nodeElem) {
    nodeElem.append($("<div class='clear'></div>"));
};
Machinata.Reporting.Node.ComponentNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








