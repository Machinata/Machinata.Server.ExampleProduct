

/// <summary>
/// Specific reactjs Profile Code
/// </summary>
/// <hidden/>


/// <summary>
/// Enable strict JS
/// </summary>
/// <hidden/>
"use strict";

// Turn off define and require loading for all vendor plugins in this file...
const define = null;
const require = null;

// ES-Lint is entirely disabled for this bundle. Vega/D3 cause too many troubles...
/* eslint-disable */

// Most of the CommonJS/AMD/UMD loaders cause lint errors, so unfortunately we must disable this lint rule...
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-globals */
