

/// <summary>
/// Data loading abstraction layer.
/// Different provider implementations can be used.
/// The provider can be set using the configuration ```Machinata.Reporting.Config.dataProvider```.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Data = {};




/// <summary>
/// Gets a piece of data for a given source using the configured provider.
/// The ```dataSource``` is always a string, however the provider defines in what format. 
/// </summary>
Machinata.Reporting.Data.loadData = function (instance, dataSource, onSuccess) {

    // Init
    if (dataSource == null) {
        onSuccess(null);
        return;
    }

    // Get the provider implementaiton
    Machinata.debug("Machinata.Reporting.Data.loadData: ");
    Machinata.debug("   dataProvider: " + instance.config.dataProvider);
    Machinata.debug("   dataSource: " + dataSource);
    var impl = Machinata.Reporting.Data.Providers[instance.config.dataProvider];
    if (impl == null) throw "The data provider " + instance.config.dataProvider + " does not exist.";

    // Pass to implementation
    impl.loadData(instance, dataSource, onSuccess);
    
};





/// <summary>
/// Namespace for all data provider implementations.
/// 
/// Each provider must implement at least the following:
///  - ```loadData(instance, dataSource, onSuccess)```
/// 
/// Providers can be registered via a object set on ```Machinata.Reporting.Data.Providers```.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Data.Providers.MyProvider = {
///    loadData: function(instance, dataSource, onSuccess) {
///        // Load data...
///        // TODO
///        // Call success callback
///        onSuccess(data);
///    }
/// };
/// ```
/// </example>
/// <type>namespace</type>
Machinata.Reporting.Data.Providers = {};



/// <summary>
/// A very simple and lightweight data provider for retrieving JSON files via HTTP requests.
/// The ```dataSource``` is loaded directly from the browser as a URL, thus any ```dataSource```
/// while using this provider can either be a remote resource via a absolute URL, a local URL
/// absolute to the server or relative to the currently loaded HTML document.
/// </summary>
/// <example>
/// ### Absolute Remote URL
///  - Loaded URL: ```https://remote.server/view/report```
///  - dataSource: ```Machinata.Reporting.Data.loadData("https://other.server/data/report/abc-xyz.json?auth-token=ABCXYZ")```
/// ### Absolute URL to Loaded Page
///  - Loaded URL: ```https://remote.server/view/report```
///  - dataSource: ```Machinata.Reporting.Data.loadData("/data/report/abc-xyz.json?auth-token=ABCXYZ")```
/// ### Relative URL to Loaded Page
///  - Loaded URL: ```https://remote.server/view/report```
///  - dataSource: ```Machinata.Reporting.Data.loadData("../../data/report/abc-xyz.json?auth-token=ABCXYZ")```
/// </example>
/// <type>class</type>
Machinata.Reporting.Data.Providers.HTTP = {};

/// <summary>
/// </summary>
Machinata.Reporting.Data.Providers.HTTP.loadData = function (instance, dataSource, onSuccess) {
    // Cache bust
    if (instance.config != null && instance.config.preventDataCaching == true) {
        var guid = Machinata.guid();
        dataSource = Machinata.updateQueryString("cache-buster", guid, dataSource);
    }
    $.ajax({
        dataType: "json",
        url: dataSource,
        success: function (data, textStatus, jqXHR) {
            onSuccess(data);
        },
        error: function (xhr, status, errorThrown) {
            Machinata.apiError("load-data-failed", "Could not load data from "+dataSource+": "+errorThrown);
        }
    });
};







/// <summary>
/// A very simple data provider for retrieving JSON data via memory.
/// The ```dataSource``` is the JSON object that is already loaded in memory.
/// Note: This provider will create a copy of the incoming data, and has a performance hit because of this.
/// </summary>
/// <example>
/// ```Machinata.Reporting.Data.loadData({ ...your JSON data... })```
/// </example>
/// <type>class</type>
Machinata.Reporting.Data.Providers.Memory = {};

/// <summary>
/// </summary>
Machinata.Reporting.Data.Providers.Memory.loadData = function (instance, dataSource, onSuccess) {
    //var dataCopy = Machinata.Util.cloneJSON(dataSource);
    // Note: since the data is coming from in memory, to disable side-effects we need a clean copy so that the report in theory can be loaded several times in one DOM without a changed state
    //TODO: @dan is there a faster way we can do this?
    //WARNING: this is very expensive
    var dataCopy = JSON.parse(JSON.stringify(dataSource)); 
    onSuccess(dataCopy);
};

