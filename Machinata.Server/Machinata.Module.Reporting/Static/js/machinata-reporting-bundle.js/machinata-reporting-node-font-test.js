


/// <summary>
/// 
/// Note: This node should not be used in production.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.FontTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults = {
    chrome: "solid"
};

/// <summary>
/// We have a solid chrome by default.
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// By default ```auto```. Defines the layout-dpi size of the blocks in the ruler.
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.blockSize = 'auto';

/// <summary>
/// By default ```auto```. Defines the number of blocks/ticks to show in the ruler.
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.numberOfBlocks = "auto";

/// <summary>
/// The units to work with. Can be ether ```px```, ```mm```, or ```in```.
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.units = "px";

/// <summary>
/// If ```true```, will show the font in different sizes.
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.showTextSizes = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.textSizesMin = 8;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.metricsTest = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.testText = "abcdefghijklmnop ABCDEFGHIJKLMNOP 0123456789 !@#$%^&*(),'\";:";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.defaults.fontConfigToTest = "text";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FontTestNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var EMPTY_GROUP = { "type": "group" };
    var font = config[json.fontConfigToTest + "Font"];
    var size = config[json.fontConfigToTest + "Size"];
    if (json.fontConfigToTest == "number") {
        size = config.chartTextSize;
    }
    if (json.fontConfigToTest == "system") {
        font = "sans-serif"
        size = config.chartTextSize;
    }
    var measurement = {};
    if (json.metricsTest == true) {
        var fontString = size + "px " + font;
        var canvas = Machinata.Imaging.createDrawingCanvas(1, 1);
        var context = canvas.getContext('2d');
        context.font = fontString;
        measurement = context.measureText(json.testText);
        console.log("measured dims are:", measurement);
    }
    return {
        "data": [
          
        ],
        "scales": [
        ],
        "signals": [
            
        ],
        "marks": [
            json.metricsTest==true ? EMPTY_GROUP : {
                "type": "group",
                /*"_comment": "CROPPER, DISPLAY FONT",*/
                "encode": {
                    "enter": {
                        "clip": { "value": true },
                    },
                    "update": {
                        "width": { "signal": "width" },
                        "height": { "signal": "height" }
                    }
                },
                "marks": [
                    {
                        "type": "text",
                        "encode": {
                            "enter": {
                                "align": { "value": "center" },
                                "baseline": { "value": "top" },
                                "font": { "value": font },
                                "fontSize": { "signal": size },
                            },
                            "update": {
                                "x": { "signal": "width/2" },
                                "y": { "signal": (size * 0.5) },
                                "text": { "value": json.fontConfigToTest + ", " + size + "px (" + font + ")" },
                            }
                        }
                    },
                    {
                        "type": "text",
                        "encode": {
                            "enter": {
                                "align": { "value": "center" },
                                "baseline": { "value": "top" },
                                "font": { "value": font },
                                "fontSize": { "signal": size },
                            },
                            "update": {
                                "x": { "signal": "width/2" },
                                "y": { "signal": (size * 2.5) },
                                "text": { "value": json.testText },
                            }
                        }
                    },
                    {
                        "type": "text",
                        "encode": {
                            "enter": {
                                "align": { "value": "center" },
                                "baseline": { "value": "top" },
                                "font": { "value": font },
                                "fontSize": { "signal": size * 10 },
                            },
                            "update": {
                                "x": { "signal": "width/2" },
                                "y": { "signal": (size * 4.5) },
                                "text": { "value": json.testText.split(' ') },
                            }
                        }
                    },
                ]
            },
            json.metricsTest==false ? EMPTY_GROUP : {
                "type": "group",
                /*"_comment": "CROPPER, DISPLAY METRICS",*/
                "encode": {
                    "enter": {
                        "clip": { "value": true },
                    },
                    "update": {
                        "width": { "signal": "width" },
                        "height": { "signal": "height" }
                    }
                },
                "marks": [
                    {
                        "type": "text",
                        "encode": {
                            "enter": {
                                "align": { "value": "center" },
                                "baseline": { "value": "top" },
                                "font": { "value": font },
                                "fontSize": { "signal": size },
                            },
                            "update": {
                                "x": { "signal": "width/2" },
                                "y": { "signal": (size * 0.5) },
                                "text": { "value": json.fontConfigToTest + ", " + size + "px (" + font + ")" },
                            }
                        }
                    },
                    {
                        "type": "text",
                        "encode": {
                            "enter": {
                                "align": { "value": "center" },
                                "baseline": { "value": "middle" },
                                "font": { "value": font },
                                "fontSize": { "signal": size },
                            },
                            "update": {
                                "x": { "signal": "width/2" },
                                "y": { "signal": "height/2" },
                                "text": { "value": json.testText },
                            }
                        }
                    },
                    {
                        "type": "rect",
                        "encode": {
                            "enter": {
                                "fill": { "value": "" },
                                "stroke": { "value": "red" },
                                "strokeWidth": { "value": 1 }
                            },
                            "update": {
                                "xc": { "signal": "width/2" },
                                "yc": { "signal": "height/2" },
                                "width": { "signal": measurement.width },
                                "height": { "signal": measurement.actualBoundingBoxAscent + measurement.actualBoundingBoxDescent }
                            }
                        }
                    },
                    {
                        "type": "rule",
                        "encode": {
                            "enter": {
                                "stroke": { "value": "green" },
                                "strokeWidth": { "value": 1 }
                            },
                            "update": {
                                "x": { "signal": "width/2 - "+measurement.width/2 },
                                "x2": { "signal": "width/2 + " + measurement.width/2 },
                                "y": { "signal": "height/2 + " + (-(measurement.actualBoundingBoxAscent + measurement.actualBoundingBoxDescent) / 2 + measurement.actualBoundingBoxAscent) },
                                "y2": { "signal": "height/2 + " + (-(measurement.actualBoundingBoxAscent + measurement.actualBoundingBoxDescent) / 2 + measurement.actualBoundingBoxAscent) }
                            }
                        }
                    }
                ]
            }
        ]
    };
};
Machinata.Reporting.Node.FontTestNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};
Machinata.Reporting.Node.FontTestNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.FontTestNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.FontTestNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







