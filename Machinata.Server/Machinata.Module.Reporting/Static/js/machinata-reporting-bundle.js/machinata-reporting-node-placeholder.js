/// <summary>
/// Inserts a bare-bones node with a resolved placeholder text from ```text```.
/// This node can be used to design a report which is missing some components
/// or has not yet been fully designed.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.PlaceholderNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PlaceholderNode.defaults = {
    chrome: "solid",
    supportsToolbar: true,
    addStandardTools: false
};

/// <summary>
/// The resolved text to display in the node. If not text is provided, then the default
/// text ```Placeholder``` is displayed.
/// </summary>
Machinata.Reporting.Node.PlaceholderNode.defaults.text = null;

Machinata.Reporting.Node.PlaceholderNode.init = function (instance, config, json, nodeElem) {
       
    // Create text with vertical alignment
    var tableElem = $("<div class='vertical-aligner node-bg'><div class='vertical-alignee text'></div></div>");
    var textToShow = Machinata.Reporting.Text.resolve(instance,json.text);
    if (textToShow == null) textToShow = "Placeholder";
    tableElem.find(".text").text(textToShow);
    nodeElem.append(tableElem);

};
Machinata.Reporting.Node.PlaceholderNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here
};








