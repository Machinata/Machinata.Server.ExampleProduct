


/// <summary>
/// Basic node for displaying a block of rich text.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.RichTextNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.RichTextNode.defaults = {};

/// <summary>
/// By default RichTextNode have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.RichTextNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.RichTextNode.defaults.supportsDashboard = false;

/// <summary>
/// The main resolvable richt-text to display in HTML format.
/// The HTML must be fully qualified HTML (for example, a ```img``` should always be contained in a ```p``` and without any styling information.
/// Supported HTML tags:
///  - ```h1```: title heading
///  - ```h2```: subtitle heading
///  - ```h3```: heading
///  - ```p```: paragraph
///  - ```img```: inline image
///  - ```ul```: un-ordered list
///  - ```ol```: ordered list
///  - ```li```: list item (requires a list parent)
///  - ```a```: text link (renders with underline)
///  - ```b```/```strong```: strong (renders bold)
///  - ```em```: emphasis (renders italic, nesting not supported)
///  - ```u```: underline (not recommended, confuses users with link)
///  - ```i```: idiomatic text (renders italic)
/// </summary>
/// <example>
/// ### Texts
/// ```
/// <h1>Title</h1>
/// <h2>Subtitle</h2>
/// <p>Paragraph text in long form.</p>
/// <p>Second paragraph text in long form.</p>
/// ```
/// ### Images
/// ```
/// <p><img src="data:image/png;base64,iVBORw0KGgoAAA"/></p>
/// ```
/// ### Links
/// ```
/// <p><a href="/more/information">More information</a></p>
/// ```
/// ### Lists
/// ```
/// <ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>
/// ```
/// </example>
Machinata.Reporting.Node.RichTextNode.defaults.html = null;

/// <summary>
/// If ```true```, the user can easily copy the text to clipboard.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.RichTextNode.defaults.addCopyToClipboardTool = true;

/// <summary>
/// If ```true```, the ```html``` property must be fully-qualified HTML, or even just plain text.
/// It is highly recommended to set this to ```true``` to ensure a safe and secure injection of markup code into the page.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.RichTextNode.defaults.requireFullyQualifiedHTML = false;

Machinata.Reporting.Node.RichTextNode.init = function (instance, config, json, nodeElem) {

    // Init
    var html = Machinata.Reporting.Text.resolve(instance,json["html"]);

    // Create body with html
    var containerElem = $("<div class='body'></div>");
    if (html != null) {
        if (json.requireFullyQualifiedHTML == false) {
            // Detect if contains HTML, if so set the node html to value, otherwise set as text
            var isHTML = Machinata.String.isHTML(html);
            if (isHTML == true) {
                containerElem.html(html);
            } else {
                containerElem.text(html);
            }
        } else {
            // Create new node, then append to dom
            containerElem.append($(html));
        }
    }
    nodeElem.append(containerElem);

    // Tools
    if (json.addCopyToClipboardTool == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.copy-to-clipboard"), "copy", function () {
            Machinata.Reporting.Tools.copyElementToClipboard(containerElem, Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied.text'));
        });
    }
};
Machinata.Reporting.Node.RichTextNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








