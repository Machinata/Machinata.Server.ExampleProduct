

/// <summary>
/// Reporting asset tools for resolving assets so that they are accessible within the report.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Asset = {};




/// <summary>
/// Get the resolved asset for a JSON object.
///
/// Note: assets that are encoded and embedded into the report JSON must adhere to the browsers limitations for blobs and data-URIs. 
/// In practice this means that files should not be larger than 2MB.
///
/// ## Images
/// Image assets can be directly encoded as a base64. This allows for image assets to be embedded in the
/// report JSON. Image assets can also be provided as a URL, either absolute (fully qualified) or relative.
/// </summary>
/// <example>
/// ### Example Embedded Asset:
/// ```
/// "photo": {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "base64",
///     "data": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVAQMAAACT2TfVAAAABlBMVEUAAAD///+l2Z/dAAAAAnRSTlP//8i138cAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABcSURBVAiZY2Bcws5Qq/udwdVUnMF1LRCXijPUpn5nYAxhZ/jP/p9BZ44/g9De8wwHg58zXHvfzsBoGs/wn0+dgdHqPEPtI3sG1x52Btf8dgbX6+UMtZHxDIzu/AAr6RmSv9DdfQAAAABJRU5ErkJggg=="
/// }
/// ```
/// ### Example Downloadable Asset:
/// ```
/// "photo": {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "base64",
///     "data": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVAQMAAACT2TfVAAAABlBMVEUAAAD///+l2Z/dAAAAAnRSTlP//8i138cAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABcSURBVAiZY2Bcws5Qq/udwdVUnMF1LRCXijPUpn5nYAxhZ/jP/p9BZ44/g9De8wwHg58zXHvfzsBoGs/wn0+dgdHqPEPtI3sG1x52Btf8dgbX6+UMtZHxDIzu/AAr6RmSv9DdfQAAAABJRU5ErkJggg=="
/// }
/// ```
/// ### Example Linked Asset:
/// ```
/// "photo": {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "url",
///     "data": "https://nerves.ch/content/image/GYssOw/Nerves-New-Business-Card.jpg"
/// }
/// ```
/// </example>
Machinata.Reporting.Asset.resolve = function (json) {
    if (json == null) return null;
    
    // Special handling for encoding types?
    if (json.encoding == "url") {
        return json.data;
    } else if(json.encoding == "base64") {
        return "data:" + json.mimeType + ";base64," + encodeURIComponent(json.data);
    } else {
        return null;
    }

    // Fallback to resolved version
    return json.resolved;
};




