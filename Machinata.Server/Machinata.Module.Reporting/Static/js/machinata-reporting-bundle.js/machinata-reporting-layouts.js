

/// <summary>
/// Reporting Layouts for nodes.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Layouts = {};

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.COLUMNS_2 = "COLUMNS_2";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.COLUMNS_4 = "COLUMNS_4";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.COLUMNS_1 = "COLUMNS_1";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_1x1 = "Block1x1";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_2x1 = "Block2x1";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_1x2 = "Block1x2";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_2x2 = "Block2x2";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_4x2 = "Block4x2";

/// <summary>
/// </summary>
Machinata.Reporting.Layouts.BLOCK_4x4 = "Block4x4";



/// <summary>
/// </summary>
Machinata.Reporting.Layouts.calculateSizeForBlockOrColumn = function (instance, config, blockOrColumnName, containerWidth, containerHeight) {
    // Init
    var ret = {width: null, height: null}; // number
    containerWidth = Machinata.Math.makeEven(containerWidth); // Make sure we have a nicely divisible number to start with
    containerHeight = Machinata.Math.makeEven(containerHeight); // Make sure we have a nicely divisible number to start with

    // Switch on block or column name
    if (blockOrColumnName == Machinata.Reporting.Layouts.COLUMNS_2) {
        ret.width = containerWidth / 2;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.COLUMNS_4) {
        ret.width = containerWidth / 4;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.COLUMNS_1) {
        ret.width = containerWidth;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_1x1) {
        ret.width = (containerWidth / 4) * 1; // blocks are always on the 4 column grid
        ret.height = ret.width;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_1x2) {
        ret.width = (containerWidth / 4) * 1; // blocks are always on the 4 column grid
        ret.height = (containerWidth / 4) * 2; // blocks are always on the 4 column grid
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_2x1) {
        ret.width = (containerWidth / 4) * 2; // blocks are always on the 4 column grid
        ret.height = (containerWidth / 4) * 1; // blocks are always on the 4 column grid
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_2x2) {
        ret.width = (containerWidth / 4) * 2; // blocks are always on the 4 column grid
        ret.height = ret.width;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_4x4) {
        ret.width = (containerWidth / 4) * 4; // blocks are always on the 4 column grid
        ret.height = ret.width;
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_2x1) {
        ret.width = (containerWidth / 4) * 2; // blocks are always on the 4 column grid
        ret.height = (containerWidth / 4) * 1; // blocks are always on the 4 column grid
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_4x1) {
        ret.width = (containerWidth / 4) * 4; // blocks are always on the 4 column grid
        ret.height = (containerWidth / 4) * 1; // blocks are always on the 4 column grid
    } else if (blockOrColumnName == Machinata.Reporting.Layouts.BLOCK_4x2) {
        ret.width = (containerWidth / 4) * 4; // blocks are always on the 4 column grid
        ret.height = (containerWidth / 4) * 2; // blocks are always on the 4 column grid
    } else {
        console.warn("Machinata.Reporting.Layouts.calculateSizeForBlockOrColumn: the block or column name " + blockOrColumnName +" is not recognized. Please use a valid block or column name given by Machinata.Reporting.Layouts constants.");
    }

    //console.log(containerWidth + "x" + containerHeight,">>", ret);
    return ret; 
};




/// <summary>
/// Default layouts shipped with the core reporting framework.
/// These are automatically registered.
/// </summary>
Machinata.Reporting.Layouts.DEFAULT_LAYOUTS = {
    "CS_W_Two_Columns": {
        "style": "CS_W_Two_Columns",
        "children": [
            {
                "slotKey": "{left}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_2"
                    },
                    "tablet": {
                        "width": "COLUMNS_1"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            },
            {
                "slotKey": "{right}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_2"
                    },
                    "tablet": {
                        "width": "COLUMNS_1"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            }
        ]
    },
    "CS_W_Four_Columns": {
        "style": "CS_W_Four_Columns",
        "children": [
            {
                "slotKey": "{a}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_4"
                    },
                    "tablet": {
                        "width": "COLUMNS_2"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            },
            {
                "slotKey": "{b}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_4"
                    },
                    "tablet": {
                        "width": "COLUMNS_2"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            },
            {
                "slotKey": "{c}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_4"
                    },
                    "tablet": {
                        "width": "COLUMNS_2"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            },
            {
                "slotKey": "{d}",
                "responsiveSize": {
                    "default": {
                        "width": "COLUMNS_4"
                    },
                    "tablet": {
                        "width": "COLUMNS_2"
                    },
                    "mobile": {
                        "width": "COLUMNS_1"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelTopA": {
        "children": [
            {
                "slotKey": "{mandateInformation}",
                "style": "CS_W_Block2x2",
                "chrome": "dark",
                "addStandardTools": false,
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x2"
                    },
                    "tablet": {
                        "size": "Block4x4"
                    },
                    "mobile": {
                        "size": "Block4x4"
                    }
                },
            },
            {
                "slotKey": "{contact}",
                "style": "CS_W_Block1x1",
                "addStandardTools": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{riskStatus}",
                "style": "CS_W_Block1x1",
                "addStandardTools": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{performanceChart}",
                "style": "CS_W_Block2x1",
                "addStandardTools": false,
                "disableExport": true,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelTopB": {
        "children": [
            {
                "slotKey": "{mandateInformation}",
                "styles": ["CS_W_Block2x2", "CS_W_MetricsFirstItemDark"],
                "chrome": "solid",
                "addStandardTools": false,
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x2"
                    },
                    "tablet": {
                        "size": "Block4x4"
                    },
                    "mobile": {
                        "size": "Block4x4"
                    }
                },
            },
            {
                "slotKey": "{contact}",
                "style": "CS_W_Block1x1",
                "addStandardTools": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{riskStatus}",
                "style": "CS_W_Block1x1",
                "addStandardTools": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{performanceChart}",
                "style": "CS_W_Block2x1",
                "addStandardTools": false,
                "disableExport": true,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelText": {
        "style": "CS_W_Overview_FullWidth",
        "children": [
            {
                "slotKey": "{text}",
                "style": "CS_W_RichText_Overview",
                "chrome": "light"
            }
        ]
    },
    "CS_W_Overview_PanelMap": {
        "style": "CS_W_Overview_FullWidth",
        "children": [
            {
                "slotKey": "{map}",
                "chrome": "light"
            }
        ]
    },
    "CS_W_Overview_PanelLargeBlocksA": {
        "children": [
            {
                "slotKey": "{left}",
                "style": "CS_W_Block2x2",
                "responsiveSize": {
                    "default": {
                        "size": "Block2x2"
                    },
                    "tablet": {
                        "size": "Block4x4"
                    },
                    "mobile": {
                        "size": "Block4x4"
                    }
                },
            },
            {
                "slotKey": "{right}",
                "style": "CS_W_Block2x2",
                "responsiveSize": {
                    "default": {
                        "size": "Block2x2"
                    },
                    "tablet": {
                        "size": "Block4x4"
                    },
                    "mobile": {
                        "size": "Block4x4"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelMixedBlocksA": {
        "children": [
            {
                "slotKey": "{left}",
                "style": "CS_W_Block2x2",
                "responsiveSize": {
                    "default": {
                        "size": "Block2x2"
                    },
                    "tablet": {
                        "size": "Block4x4"
                    },
                    "mobile": {
                        "size": "Block4x4"
                    }
                },
            },
            {
                "slotKey": "{rightTop}",
                "style": "CS_W_Block2x1",
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            },
            {
                "slotKey": "{rightBottom}",
                "style": "CS_W_Block2x1",
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelTrailerBlocksA": {
        "children": [
            {
                "slotKey": "{left}",
                "styles": ["CS_W_Block1x1"],
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{center}",
                "styles": ["CS_W_Block2x1"],
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            },
            {
                "slotKey": "{right}",
                "styles": ["CS_W_Block1x1"],
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelTrailerBlocksB": {
        "children": [
            {
                "slotKey": "{left1}",
                "styles": ["CS_W_Block1x1"],
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{left2}",
                "style": "CS_W_Block1x1",
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{right}",
                "style": "CS_W_Block2x1",
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block2x1"
                    },
                    "tablet": {
                        "size": "Block4x2"
                    },
                    "mobile": {
                        "size": "Block4x2"
                    }
                },
            }
        ]
    },
    "CS_W_Overview_PanelTrailerBlocksC": {
        "children": [
            {
                "slotKey": "{left}",
                "styles": ["CS_W_Block1x1"],
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{middleLeft}",
                "style": "CS_W_Block1x1",
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{middleRight}",
                "style": "CS_W_Block1x1",
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            },
            {
                "slotKey": "{right}",
                "style": "CS_W_Block1x1",
                "addCopyToClipboardTool": false,
                "responsiveSize": {
                    "default": {
                        "size": "Block1x1"
                    },
                    "tablet": {
                        "size": "Block2x2"
                    },
                    "mobile": {
                        "size": "Block2x2"
                    }
                },
            }
        ]
    },
};
Machinata.Reporting.registerLayouts(Machinata.Reporting.Layouts.DEFAULT_LAYOUTS);