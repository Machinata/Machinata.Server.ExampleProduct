


/// <summary>
/// 
/// Note: This node should not be used in production.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.CalibrationTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults = {};

/// <summary>
/// We have a solid chrome by default.
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// By default ```auto```. Defines the layout-dpi size of the blocks in the ruler.
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.blockSize = 'auto';

/// <summary>
/// By default ```auto```. Defines the number of blocks/ticks to show in the ruler.
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.numberOfBlocks = "auto";

/// <summary>
/// The units to work with. Can be ether ```px```, ```mm```, or ```in```.
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.units = "px";

/// <summary>
/// If ```true```, will show the font in different sizes.
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.showTextSizes = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.textSizesMin = 8;

/// <summary>
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.defaults.textSizesMax = 40;

/// <summary>
/// </summary>
Machinata.Reporting.Node.CalibrationTestNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var ruleSize = 10;
    var blockSizePX = 10; // default
    var blockSizeInUnits = 10; // default
    var numberOfBlocks = 10; // default
    var widthInUnitsSignal = "width";
    var heightInUnitsSignal = "height";
    if (json.unit == "px") {
        if (json.blockSize == null || json.blockSize == "auto") blockSizeInUnits = 10;
        if (json.numberOfBlocks == null || json.numberOfBlocks == "auto") numberOfBlocks = 10;
        blockSizePX = blockSizeInUnits;
    } else if (json.unit == "mm") {
        if (json.blockSize == null || json.blockSize == "auto") blockSizeInUnits = 10;
        if (json.numberOfBlocks == null || json.numberOfBlocks == "auto") numberOfBlocks = 10;
        blockSizePX = Machinata.Reporting.Tools.convertMillimetersToPixels(config,blockSizeInUnits);
        widthInUnitsSignal = "width / " + Machinata.Reporting.Tools.convertMillimetersToPixels(config,1);
        heightInUnitsSignal = "height / " + Machinata.Reporting.Tools.convertMillimetersToPixels(config,1);
    } else if (json.unit == "in") {
        if (json.blockSize == null || json.blockSize == "auto") blockSizeInUnits = 1 / 8;
        if (json.numberOfBlocks == null || json.numberOfBlocks == "auto") numberOfBlocks = 8;
        blockSizePX = Machinata.Reporting.Tools.convertInchesToPixels(config,blockSizeInUnits);
        widthInUnitsSignal = "width / " + Machinata.Reporting.Tools.convertInchesToPixels(config,1);
        heightInUnitsSignal = "height / " + Machinata.Reporting.Tools.convertInchesToPixels(config,1);
    }

    var textSizes = [];
    var totalTextSize = 0;
    if (json.showTextSizes == true) {
        for (var s = json.textSizesMin; s <= json.textSizesMax; s += 2) {
            totalTextSize += s;
            textSizes.push({
                size: s,
                offset: totalTextSize
            });
        }
    }

    return {
        "data": [
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "blocks",
              "transform": [
                  { "type": "sequence", "start": 0, "stop": numberOfBlocks, "as": "index" }
              ]
          },
          {
              "name": "textSizes",
              "values": textSizes
          },
        ],
        "scales": [
        ],
        "signals": [
            {
                "name": "drawSize",
                "update": "min(width,height)",
            },
            {
                "name": "centerX",
                "update": "width/2",
            },
            {
                "name": "centerY",
                "update": "height/2",
            },
            {
                "name": "widthInUnits",
                "update": widthInUnitsSignal,
            },
            {
                "name": "heightInUnits",
                "update": heightInUnitsSignal,
            }
        ],
        "marks": [
            {
                "type": "group",
                /*"_comment": "CROPPER",*/
                "marks": []
            },
            {
                "type": "group",
                /*"_comment": "FONT CALIBRATION TEXT FONT",*/
                "marks": [
                    {
                        "type": "text",
                        "from": { "data": "textSizes" },
                        "encode": {
                            "enter": {
                                "align": { "value": "left" },
                                "baseline": { "value": "top" },
                                "font": { "value": config.textFont },
                                "fontSize": { "signal": "datum.size" },
                            },
                            "update": {
                                "x": { "signal": ruleSize/2 },
                                "y": { "signal": "20+datum.offset" },
                                "text": { "signal": "datum.size + 'px' + (datum.size == " + config.chartTextSize + " ? ' default' : '')" },
                            }
                        }
                    },
                ]
            },
            {
                "type": "group",
                /*"_comment": "FONT CALIBRATION NUMBER FONT",*/
                "marks": [
                    {
                        "type": "text",
                        "from": { "data": "textSizes" },
                        "encode": {
                            "enter": {
                                "align": { "value": "right" },
                                "baseline": { "value": "top" },
                                "font": { "value": config.numberFont },
                                "fontSize": { "signal": "datum.size" },
                            },
                            "update": {
                                "x": { "signal": "width - "+ruleSize / 2 },
                                "y": { "signal": "20+datum.offset" },
                                "text": { "signal": "(datum.size == " + config.chartTextSize + " ? 'default ' : '') + datum.size + 'px'" },
                            }
                        }
                    },
                ]
            },
          {
              "type": "group",
              /*"_comment": "RULER CALIBRATION BLOCKS",*/
              "encode": {
                  "update": {
                      "x": { "signal": "centerX" },
                      "y": { "signal": "centerY - ("+blockSizePX+" * data('blocks').length)/2" },
                  }
              },
              "marks": [
                  {
                      "type": "group",
                      "from": { "data": "blocks" },
                      "encode": {
                          "update": {
                              "xc": { "signal": "0" },
                              "yc": { "signal": "datum.index * " + blockSizePX },
                          }
                      },
                      "marks": [
                          {
                              "type": "rect",
                              "encode": {
                                  "update": {
                                      "fill": { "signal": "parent.index % 2 == 0 ? 'black' : 'lightgray'" },
                                      
                                      "width": { "value": blockSizePX },
                                      "height": { "value": blockSizePX }
                                  }
                              }
                          },
                          {
                              "type": "text",
                              "encode": {
                                  "enter": {
                                      "dx": { "value": blockSizePX * 1.5 },
                                      "dy": [
                                          {
                                              "test": "parent.index == 0",
                                              "value": 0,
                                          },
                                          {
                                              "test": "parent.index == data('blocks').length-1",
                                              "value": blockSizePX,
                                          },
                                          {
                                              "value": 0
                                          }
                                      ],
                                      "align": { "value": "left" },
                                      "baseline": { "value": "middle" },
                                      "fontSize": { "value": 10 },
                                  },
                                  "update": {
                                      "text": [
                                          {
                                              "test": "parent.index == 0",
                                              "signal": "(parent.index * " + blockSizePX + ") + '"+json.unit+"'",
                                          },
                                          {
                                              "test": "parent.index == data('blocks').length-1",
                                              "signal": "((parent.index+1) * " + blockSizeInUnits + ") + '" + json.unit + "' + ' / ' + format((parent.index+1) * " + blockSizePX + ",',.2f') + 'px' + ' @ ' + " + config.layoutingDPI + "+ 'dpi'",
                                          },
                                          {
                                              "value": null
                                          }
                                      ],
                                  }
                              }
                          }
                      ]
                  },
              ]
          },
          {
              "type": "text",
              "encode": {
                  "enter": {
                      "dx": { "value": ruleSize/2 },
                      "dy": { "value": ruleSize/2 },
                      "align": { "value": "left" },
                      "baseline": { "value": "top" },
                      "fontSize": { "value": 10 },
                  },
                  "update": {
                      "text": {
                          "signal": "format(widthInUnits,',.2f') + 'x' + format(heightInUnits,',.2f') + '" + json.unit + "' + ' / ' + width + 'x' + height + 'px' + ' @ ' + " + config.layoutingDPI + "+ 'dpi'"
                      }
                  }
              }
          },
          {
              "type": "group",
              /*"_comment": "TOP LEFT CROP MARKS",*/
              "marks": [
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": 0 },
                              "y": { "signal": 0 },
                              "x2": { "signal": ruleSize },
                              "y2": { "signal": 0 }
                          }
                      }
                  },
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": 0 },
                              "y": { "signal": 0 },
                              "x2": { "signal": 0 },
                              "y2": { "signal": ruleSize }
                          }
                      }
                  }
              ]
          },
          {
              "type": "group",
              /*"_comment": "BOTTOM LEFT CROP MARKS",*/
              "marks": [
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": 0 },
                              "y": { "signal": "height-0.5" },
                              "x2": { "signal": ruleSize },
                              "y2": { "signal": "height-0.5" }
                          }
                      }
                  },
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": 0 },
                              "y": { "signal": "height-0.5" },
                              "x2": { "signal": 0 },
                              "y2": { "signal": "height-0.5-" + ruleSize }
                          }
                      }
                  }
              ]
          },
          {
              "type": "group",
              /*"_comment": "TOP RIGHT CROP MARKS",*/
              "marks": [
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": "width" },
                              "y": { "signal": 0 },
                              "x2": { "signal": "width-"+ruleSize },
                              "y2": { "signal": 0 }
                          }
                      }
                  },
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": "width" },
                              "y": { "signal": 0 },
                              "x2": { "signal": "width" },
                              "y2": { "signal": ruleSize }
                          }
                      }
                  }
              ]
          },
          {
              "type": "group",
              /*"_comment": "BOTTOM RIGHT CROP MARKS",*/
              "marks": [
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": "width" },
                              "y": { "signal": "height-0.5" },
                              "x2": { "signal": "width-" + ruleSize },
                              "y2": { "signal": "height-0.5" }
                          }
                      }
                  },
                  {
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "strokeWidth": { "value": 1 }
                          },
                          "update": {
                              "x": { "signal": "width" },
                              "y": { "signal": "height-0.5" },
                              "x2": { "signal": "width" },
                              "y2": { "signal": "height-0.5-" + ruleSize }
                          }
                      }
                  }
              ]
          }
        ]
    };
};
Machinata.Reporting.Node.CalibrationTestNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};
Machinata.Reporting.Node.CalibrationTestNode.init = function (instance, config, json, nodeElem) {

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.CalibrationTestNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.CalibrationTestNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};













