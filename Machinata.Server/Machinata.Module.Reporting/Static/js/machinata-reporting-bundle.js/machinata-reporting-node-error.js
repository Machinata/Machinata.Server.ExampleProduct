


/// <summary>
/// Displays an error to the user. This should only be used if the report (or a part) 
/// has failed and should only reach the end user on  staging/testing environment.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ErrorNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults = {};

/// <summary>
/// No toolbar needed. By default ```false```.
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults.supportsToolbar = false;

/// <summary>
/// The error message to display.
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults.error = null;

/// <summary>
/// The error code to include (optional).
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults.code = null;

/// <summary>
/// The message to display to the sure (optional).
/// This is a resolved text.
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults.message = null;

/// <summary>
/// The style of the error node to use. If ```null``` (the default), a general
/// styled error node is created to ensure visibility (using a standard debug panel),
/// otherwise the style drives the design of the error node.
/// </summary>
Machinata.Reporting.Node.ErrorNode.defaults.style = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ErrorNode.init = function (instance, config, json, nodeElem) {
    // We have two types of errors, plain generated debug panel nodes, and styled error nodes
    if (json.styles == null || json.styles.length == 0) {
        // No style - build a standard debug panel
        var errorOpts = {};
        if (json.code != null) errorOpts["Code"] = json.code;
        if (json.error != null) errorOpts["Error"] = json.error;
        var msgToShow = "Error: " + json.error;
        if (json.message != null) msgToShow = Machinata.Reporting.Text.resolve(instance,json.message);
        var errorElem = Machinata.UI.createDebugPanel(msgToShow, errorOpts).addClass("option-error");
        nodeElem.append(errorElem);
    } else {
        // With style - build a simple dom to be styled via css...
        var bgElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
        var messageElem = $("<div class='message'></div>").text(Machinata.Reporting.Text.resolve(instance,json.message)).appendTo(bgElem);
        if (json.code != null) var codeElem = $("<div class='code'></div>").text(json.code).appendTo(bgElem);
        if (json.error != null) var errorElem = $("<div class='error'></div>").text(json.error).appendTo(bgElem);
    }

};
Machinata.Reporting.Node.ErrorNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








