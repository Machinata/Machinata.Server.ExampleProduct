


/// <summary>
/// ## Axis's
/// Scaling Engine: ```Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis```
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.VBarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults = {};

/// <summary>
/// By default charts are ```solid```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.chrome = "solid";
/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.legendColumns = 2;

/// <summary>
/// Defines the amount of padding (or spacing) for bar-groups (in percent relative to bandwidth).
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barBandPadding = 0.2;

/// <summary>
/// Defines the maximum width of a bar in pixels. If there is not enough space, then the bar may be
/// smaller.
/// By default ```Machinata.Reporting.Config.verticalBarMaxSize```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barMaxSize = Machinata.Reporting.Config.verticalBarMaxSize;

/// <summary>
/// When ```true```, the Y-Axis domain is automatically adjusted through different mechanisms
/// to make the axis' as pretty and nice as possible.
/// 
/// Note: If a series contains a yAxis definitions with a minValue
/// and maxValue, then the autoSnap is not performed on this series.
///
/// By default ```true```.
///  
/// See also:
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisSnapToZero
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisNiceDomain
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisMargin
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisBalance
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxis = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, snaps the axis' domain min value to zero. This only happens
/// if all values are either ```> 0``` or all values are ```< 0```.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisSnapToZero = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, rounds the axis' domain to nice numbers.
/// Uses D3's domain scale ```nice()```.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisNiceDomain = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, defines the margin to add to the axis domain
/// in decimal percent.
/// By default ```0.05```, or 5%.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisMargin = 0.05;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, this feature will automaticall balance the domain on negative and positive. 
/// Only applies to domains that span from negative to positive.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisBalance = true;

/// <summary>
/// When enabled, the bars will automatically stack within their categories. 
/// All fact values must be positive, 
/// or the chart will not render.
/// If ```autoSnapYAxis``` is enabled, the y-axis domain will automatically be adjusted to account
/// for the new limits. If it is disabled, you must ensure that the y-axis domain accounts for largest
/// category total.
/// By default ```false``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeries = false;

/// <summary>
/// If ```stackedSeries``` is ```true```, then the multiple defines the total width of each stacked bar
/// relative to what a single bar width would take. This ensures a consistent design while allowing for a more
/// practical bar width in stacked mode.
/// By default ```4``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesBarWidthMultiple = 4;

/// <summary>
/// By default we use a ```greedy``` overlap strategy.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.axisLabelOverlap = "greedy";

/// <summary>
/// Defines the maximum characters a x-axis label can have before is line-breaked.
/// Note that x-axis labels are allowed maximum two lines.
/// By default ```10```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisLabelLimitChars = 12;

/// <summary>
/// The maximumum number of lines a label can span. If the maximum lines exceed, the 
/// ```xAxisLabelLimitChars``` is automatically postfixed.
/// By default ```2```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisLabelLimitLines = 3;

/// <summary>
/// </summary>
Machinata.Reporting.Node.VBarNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var stackedSeriesBandwidthSize = json.stackedSeriesBarWidthMultiple;
    var stackedSeriesFakeDomain = [];
    for (var i = 0; i < stackedSeriesBandwidthSize; i++) stackedSeriesFakeDomain.push(i);
    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "domain",
              "values": null
          },

            {
                "name": "empty",
                "values": []
            },
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          },
          {
              "name": "facts",
              "source": "series",
              "transform": [
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "yAxisFormatType",
                      "expr": "(datum.yAxis && datum.yAxis.formatType) ? datum.yAxis.formatType : 'number'" // default
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "yAxisFormat",
                      "expr": "(datum.yAxis && datum.yAxis.format) ? datum.yAxis.format : '.1%'" // default
                  },
                {
                    "type": "flatten",
                    "fields": ["facts"],
                    "as": ["fact"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [

                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id" //TODO
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "category",
                    "expr": "datum.fact.category"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "val",
                    "expr": "datum.fact.val"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "categoryResolved",
                    "expr": "datum.category.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "xAxisCategoryResolved",
                      "expr": "datum.categoryLineBreaked != null ? datum.categoryLineBreaked : datum.category.resolved"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "valResolved",
                    "expr": "datum.title != null ? (datum.titleResolved + ': ' + datum.fact.resolved) : (datum.fact.resolved)"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "groupResolved",
                    "expr": "datum.group ? datum.group.resolved : datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "serieId",
                    "expr": "datum.id"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "colorResolved",
                    "expr": "datum.colorShade != null ? scale('colorByShade',datum.colorShade) : scale('color',datum.serieId)"
                  }
              ]
          }
        ],
        "signals": [
            
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme }
            },
            {
                "name": "colorByShade",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": json.theme }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormat",
                "type": "ordinal",
                "domain": ["yAxisFormat"],
                "range": { "data": "factsResolved", "field": "yAxisFormat" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormatType",
                "type": "ordinal",
                "domain": ["yAxisFormatType"],
                "range": { "data": "factsResolved", "field": "yAxisFormatType" }
            },
          {
              /*"_comment": "This is the main y scale bandwidth for groups of bars WITHOUT padding",*/
              "name": "yScaleNoPadding",
              "type": "band",
              "domain": {
                  "data": "factsResolved",
                  "field": "categoryResolved"
              },
              "range": "height",
              "padding": 0
          },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "serieId" },
              "range": { "data": "factsResolved", "field": "colorResolved" },
          },
            {
                // Provide the proper label for the legend
                "name": "legendLabel",
                "type": "ordinal",
                "domain": { "data": "factsResolved", "field": "serieId" },
                "range": { "data": "factsResolved", "field": "titleResolved" }
            },
            {
                // Provide the proper label for the x-axis
                "name": "xScaleLabel",
                "type": "ordinal",
                "domain": { "data": "factsResolved", "field": "categoryResolved" },
                "range": { "data": "factsResolved", "field": "xAxisCategoryResolved" }
            },
            {
                "name": "xScale",
                "type": "band",
                "domain": {
                    "data": "factsResolved",
                    "field": "categoryResolved"
                },
                "padding": json.barBandPadding,
                //"padding": json.xScalePadding,
                "range": "width",
                "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                "zero": true,
                "nice": true
                //"nice": { "signal": "scale('yAxisTickCount','yAxisTickCount')" }
                //"nice": true // setting nice to true will cause the axis domain to snap to a value, leading to a side affect that two axis' no longer align, setting it to false will cause the top/bottom of the bars/lines to hang over the edge of the yaxis
            },
            {
                "name": "yScale",
                "type": "linear",
                "domain": {
                    "data": "factsResolved",
                    "field": "val"
                },
                //"domainMin": {
                //    "signal": "factsDomainMin",
                //},
                //"domainMax": {
                //    "signal": "factsDomainMax",
                //},
                "domainMin": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.minValue : null",
                },
                "domainMax": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.maxValue : null",
                },
                "padding": config.yAxisLabelPadding,
                "range": "height",
                "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                "zero": true,
                "nice": true
                //"nice": { "signal": "scale('yAxisTickCount','yAxisTickCount')" }
                //"nice": true // setting nice to true will cause the axis domain to snap to a value, leading to a side affect that two axis' no longer align, setting it to false will cause the top/bottom of the bars/lines to hang over the edge of the yaxis
            },
            {
                "name": "xScaleBar",
                /*"_comment": "Defines the individual bar scale for each category. If we are stacking the series, then we don't care about how many series we have, but we still use this scale to create a multiple of the size of what a single bar will look like...",*/
                "type": "band",
                "range": "width",
                "domain": json.stackedSeries == true ? stackedSeriesFakeDomain : { "data": "factsResolved", "field": "key" }
            }
            
        ],
        "axes": [     
            Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                "orient": "bottom",
                "scale": "xScale",
                "domain": false,
                //"format": { "signal": "if(scale('xAxisFormat','xAxisFormat') == null, ' ', scale('xAxisFormat','xAxisFormat'))" }, // If the format is null, use empty string ' ', otherwise 
                //"formatType": { "signal": "scale('xAxisFormatType','xAxisFormatType')" },
                //"values": { "signal": "data('series')[0].xAxis.values" },
                //"title": (json.showXAxisTitle == true ? { "signal": "data('series')[0].xAxis.title != null ? data('series')[0].xAxis.title.resolved : null" } : null), // Vega can't not show a title other than if we set it to null hard, thus we doe this before compiling the spec (with an extra runtime check for safety)
                //"grid": json.showXAxisGrid,
                //"labelOverlap": true, // tells vega to remove labels that overlap
                "encode": {
                    "labels": {
                        "update": {
                            "text": { "signal": "scale('xScaleLabel',datum.value)" }
                        }

                    }
                }
            }),
            {
                "orient": "left",
                "scale": "yScale",
                "tickSize": 0,
                "tickRound": true,
                //"labelPadding": 4,
                "format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                "formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                "zindex": 1,
                "grid": true,
                "domain": false
            }
        ],
        "marks": [
          {
              /*"_comment": "BARS GROUPS",*/
              "type": "group",
              "zindex": 2,
              "from": {
                  "facet": {
                      "data": "factsResolved",
                      "name": "facet",
                      "groupby": "categoryResolved"
                  }
              },
              "encode": {
                  "update": {
                      "x": { "scale": "xScale", "field": "categoryResolved" }
                  }
              },
              "signals": [
                    { "name": "numCategories", "update": "domain('xScaleBar').length" },
                    {
                        /*"_comment": "This signal limits the bar size by automatically limiting the total space allowed",*/
                        "name": "width",
                        "update": "(bandwidth('xScale') / numCategories) > " + json.barMaxSize + " ? numCategories*" + json.barMaxSize + " : bandwidth('xScale')"
                    },
                    { "name": "fullWidth", "update": "bandwidth('xScale')" }
              ],
              "scales": [
                {
                    "name": "seriesPos",
                    "type": "band",
                    "range": "width",
                    "domain": { "data": "factsResolved", "field": "key" }
                }
              ],
              "marks": [
                {
                    /*"_comment": "SINGLE BARS - side by side",*/
                      "name": "bars",
                      "from": { "data": json.stackedSeries != true ? "facet" : "empty" },
                    "type": "rect",
                    "encode": {
                        "enter": {
                            "fill": { "field": "colorResolved" },
                            "tooltip": {"signal": "datum.valResolved"},
                        },
                        "update": {
                            "x": { "signal": "scale('seriesPos',datum.key) + (fullWidth-width)/2" },
                            "width": { "scale": "seriesPos", "band": 1 },
                            "y": { "scale": "yScale", "field": "val" },
                            "y2": { "scale": "yScale", "value": 0 }
                        }
                    }
                  },
                  {
                      /*"_comment": "STACKED BARS",*/
                      "name": "bars_stacked",
                      "from": { "data": json.stackedSeries == true ? "facet" : "empty" },
                      "type": "rect",
                      "encode": {
                          "enter": {
                              "fill": { "field": "colorResolved" },
                              "tooltip": { "signal": "datum.valResolved" },
                          },
                          "update": {
                              "x": { "signal": "scale('seriesPos',datum.key)*0 + (fullWidth-width)/2" },
                              "width": { "signal": "width" },
                              "y": { "signal": "scale('yScale',datum.fact.stackedValA)" },
                              "y2": { "signal": "scale('yScale',datum.fact.stackedValB)" },
                          }
                      }
                  }
              ]
          },
          {
              /*"_comment": "Y-ZERO RULE",*/
              "name": "yzero",
              "zindex": 3,
              "type": "rule",
              "encode": {
                  "enter": {
                      "stroke": { "value": config.domainLineColor },
                      "strokeWidth": { "value": config.domainLineSize }
                  },
                  "update": {
                      "x": { "value": 0 },
                      "x2": { "signal": "width" },
                      "y": { "signal": "scale('yScale',0)" }
                  }
              }
          }
        ]
    };
};
Machinata.Reporting.Node.VBarNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.series;
};

Machinata.Reporting.Node.VBarNode.init = function (instance, config, json, nodeElem) {

    // Convert serie titles into multilines...
    if (json.xAxisLabelLimitChars != null && json.xAxisLabelLimitChars > 0) {
        Machinata.Util.each(json.series, function (index, jsonSerie) {
            Machinata.Util.each(jsonSerie.facts, function (index, jsonFact) {
                var catResolved = Machinata.Reporting.Text.resolve(instance,jsonFact.category);
                var catLineBreaked = null;
                catLineBreaked = Machinata.Reporting.Tools.lineBreakString(catResolved, {
                    maxCharsPerLine: json.xAxisLabelLimitChars,
                    maxLines: json.xAxisLabelLimitLines,
                    wordSeparator: " ",
                    ellipsis: config.textTruncationEllipsis,
                    appendEllipsisOnLastLine: true,
                    returnArray: true
                });
                jsonFact.categoryLineBreaked = catLineBreaked;
            });
        });
    }

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries(instance, config, json, json.series);

    // Use our own axis snapping tool?
    if (json.autoSnapYAxis == true) {
        Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis(
            instance, 
            config,
            json,
            json.series,
            {
                axisKey: "yAxis",
                snapToZero: json.autoSnapYAxisSnapToZero,
                niceDomain: json.autoSnapYAxisNiceDomain,
                margin: json.autoSnapYAxisMargin,
                balance: json.autoSnapYAxisBalance,
                stackedSeries: json.stackedSeries,
                tickCount: json.autoSnapYAxisTickCount
            }
        );
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.VBarNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.VBarNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







