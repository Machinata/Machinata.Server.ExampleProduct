/// <summary>
/// A specialized infographic that shows a planet surrounded by a series of satellites.
/// If the node is used in tight space, one must make sure to configure the label settings to ensure that
/// the text is properly truncated.
///
/// Currently only two series are officially supported: 
///  * Serie 1: The planet (one fact)
///  * Serie 2: The satellites (one to six facts)
///
/// Planet labels are included in the center of the planet in bold, while satellite labels are included on the 
/// outside of the satellite and autmatically aligned away from the planet. These labels are automatically line-breaked
/// and/or truncated using the following settings:
///  * ```planetLabelLimitPixels```
///  * ```planetLabelLimitChars```
///  * ```planetLabelLimitLines```
///  * ```planetLabelLimitEllipsis```
///  * ```satelliteLabelLimitPixels```
///  * ```satelliteLabelLimitChars```
///  * ```satelliteLabelLimitLines```
///  * ```satelliteLabelLimitEllipsis```
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.CoreSatelliteNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults = {};

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Donuts and pies have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.supportsToolbar = true;

/// <summary>
/// The chart never has a legend.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.insertLegend = false;

/// <summary>
/// If ```true```, sorts the facts automatically. Otherwise the facts are displayed in the order
/// of the data structure...
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.sortFacts = false;

/// <summary>
/// The pixel limit (width) of a planet label. If the label exceeds, the ```planetLabelLimitEllipsis```
/// is automatically postfixed.
/// This setting is a fallback incase the automatic line breaking functions do not succeed due to long words.
/// By default ```300```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.planetLabelLimitPixels = 300;

/// <summary>
/// The number of characters allowed on the label per line. If the label exceeds this character
/// count, the label is automatically split into lines.
/// By default ```8```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.planetLabelLimitChars = 8;

/// <summary>
/// The maximumum number of lines a label can span. If the maximum lines exceed, the 
/// ```planetLabelLimitEllipsis``` is automatically postfixed.
/// By default ```8```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.planetLabelLimitLines = 8;

/// <summary>
/// The ellipsis string to use.
/// By default ```Machinata.Reporting.Config.textTruncationEllipsis```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.planetLabelLimitEllipsis = Machinata.Reporting.Config.textTruncationEllipsis;

/// <summary>
/// Adds a line-break space before the planet value.
/// By default ```true``` for web, ```false``` for print.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.planetLabelAddSpaceBeforeValue = true;

/// <summary>
/// The pixel limit (width) of a planet label. If the label exceeds, the ```satelliteLabelLimitEllipsis```
/// is automatically postfixed.
/// This setting is a fallback incase the automatic line breaking functions do not succeed due to long words.
/// By default ```300```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.satelliteLabelLimitPixels = 300;

/// <summary>
/// The number of characters allowed on the label per line. If the label exceeds this character
/// count, the label is automatically split into lines.
/// By default ```24```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.satelliteLabelLimitChars = 24;

/// <summary>
/// The maximumum number of lines a label can span. If the maximum lines exceed, the 
/// ```satelliteLabelLimitEllipsis``` is automatically postfixed.
/// By default ```3```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.satelliteLabelLimitLines = 3;

/// <summary>
/// The ellipsis string to use.
/// By default ```Machinata.Reporting.Config.textTruncationEllipsis```.
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.defaults.satelliteLabelLimitEllipsis = Machinata.Reporting.Config.textTruncationEllipsis;


/// <summary>
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Helper function (macro) to calculate multi-line offset
    function calculateMultilineOffset(numberLinesSignal, fontSize) {
        // "-(parent.labelResolvedLineBreaked.length*" + config.labelSize*1.5 + ")/2 + (" + config.labelSize*1.5/2 + ")"
        var lineHeight = fontSize * 1.2; // this seems to be the line height after some testing
        return "( +(" + lineHeight / 2 + ") -(" + numberLinesSignal + "*" + lineHeight + ")/2 )";
    }
    // Create the sort (collection) transform. If no sort is given, we just provide an empty transform
    var collectTransform = {"type": "collect"};
    if (json.sortFacts != false && json.sortFacts != null) {
        collectTransform["sort"] = {
            "field": "fact.val",
            "order": [json.sortFacts]
        };
    }
    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "seriesResolved",
              "source": "series",
              "transform": [
                  {
                      "type": "flatten",
                      "fields": ["facts"],
                      "as": ["fact"]
                  }
              ]
          },
          {
              "name": "factsResolved",
              "source": "seriesResolved",
              "transform": [
                  { "type": "formula", "initonly": true, "as": "key", "expr": "datum.fact.category.resolved" },
                  { "type": "formula", "initonly": true, "as": "valResolved", "expr": "datum.fact.resolved" },
                  { "type": "formula", "initonly": true, "as": "tooltipResolved", "expr": "datum.fact.category.resolved + ': ' + datum.valResolved" },
                  { "type": "formula", "initonly": true, "as": "labelResolved", "expr": "datum.fact.category.resolved" },
                  { "type": "formula", "initonly": true, "as": "labelResolvedLineBreaked", "expr": "datum.fact.categoryLineBreaked" },
                  { "type": "formula", "initonly": true, "as": "serieSize", "expr": "(scale('serieSize',datum.serieIndex)) * (1/datum.serieTotal) * 0.4" },
                  { "type": "formula", "initonly": true, "as": "serieOffset", "expr": "(datum.serieSize*4.2)*datum.serieIndex" },
                  { "type": "formula", "initonly": true, "as": "factAngle", "expr": "(datum.fact.factIndex/datum.fact.factTotal)*(360) + (360/datum.fact.factTotal)/2" },
                  collectTransform
              ]
          },

          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          },
        ],
        "signals": [
            {
                "name": "drawSize",
                "update": "min(width,height)",

            },
            {
                "name": "centerX",
                "update": "width/2",
            },
            {
                "name": "centerY",
                "update": "height/2",
            }
        ],
        "scales": [

          {
              "name": "colorByShade",
              "type": "ordinal",
              "domain": { "data": "colorShades", "field": "data" },
              "range": { "scheme": json.theme }
          },
          {
              "name": "grayColor",
              "type": "ordinal",
              "domain": { "data": "grayShades", "field": "data" },
              "range": { "scheme": "gray" }
          },
          {
              "name": "serieColor",
              "type": "ordinal",
              "domain": { "data": "seriesResolved", "field": "serieIndex" },
              "range": { "scheme": json.theme }
          },
          {
              "name": "serieArcColor",
              "type": "ordinal",
              "domain": { "data": "seriesResolved", "field": "serieIndex" },
              "range": { "scheme": "gray" }
          },
          {
              "name": "serieTextColor",
              "type": "ordinal",
              "domain": { "data": "seriesResolved", "field": "serieIndex" },
              "range": { "scheme": "text-on-" + json.theme }
          },
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "key" },
              "range": { "scheme": json.theme }
          },
          {
              "name": "sliceTextColor",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "key" },
              "range": { "scheme": "text-on-" + json.theme }
          },
          {
              "name": "serieSize",
              "type": "linear",
              "domain": { "data": "seriesResolved", "field": "serieIndex" },
              "range": [1.0,0.5]
          },
        ],

        "marks": [
            {
                "name": "planetSeriesLines",
                /*"_comment": "We have to draw these first in th background of all series",*/
                "type": "group",
                "from": {
                    "facet": {
                        "data": "factsResolved",
                        "name": "facetSeries",
                        "groupby": "id"
                    }
                },
                "marks": [
                    {
                        "name": "planetLines",
                        /*"_comment": "This group has a single line from a to b. The line must be in a group, otherwise the lines are connected.",*/
                        "type": "group",
                        "from": {
                            "data": "facetSeries",
                        },
                        "marks": [
                            {
                                "name": "planetLine",
                                /*"_comment": "Planet line from planet center to center...",*/
                                "type": "rule",
                                "encode": {
                                    "enter": {
                                        "stroke": {
                                            "signal": "scale('grayColor','gray3')"
                                        },
                                        "strokeWidth": {
                                            "value": (config.padding / 2)
                                        }
                                    },
                                    "update": {
                                        "x": {
                                            "signal": "centerX"
                                        },
                                        "y": {
                                            "signal": "centerY"
                                        },
                                        "x2": {
                                            "signal": "centerX + (drawSize*parent.serieOffset)*cos((parent.factAngle-90)*(PI/180))"
                                        },
                                        "y2": {
                                            "signal": "centerY + (drawSize*parent.serieOffset)*sin((parent.factAngle-90)*(PI/180))"
                                        }
                                    }
                                }
                            },
                        ]
                    }
                ]
            },
          {
              "name": "planetSeries",
              "type": "group",
              "from": {
                  "facet": {
                      "data": "factsResolved",
                      "name": "facetSeries",
                      "groupby": "id"
                  }
              },
              "marks": [
                  {
                      "name": "planets",
                      /*"_comment": "This group moves to the center point of the planet based on its angle",*/
                      "type": "group",
                      "from": {
                          "data": "facetSeries",
                      },
                      "encode": {
                          "update": {
                              "xc": {
                                  "signal": "centerX + (drawSize*datum.serieOffset)*cos((datum.factAngle-90)*(PI/180))"
                              },
                              "yc": {
                                  "signal": "centerY + (drawSize*datum.serieOffset)*sin((datum.factAngle-90)*(PI/180))"
                              }
                          }
                      },
                      "marks": [
                          {
                              "name": "planetSphereBG",
                              /*"_comment": "Planet circle bg...",*/
                              "type": "arc",
                              "encode": {
                                  "enter": {
                                      "fill": {
                                          "signal": "scale('grayColor','gray3')"
                                      },
                                      "startAngle": {
                                          "signal": "0"
                                      },
                                      "endAngle": {
                                          "signal": "2*PI"
                                      },
                                      "xc": {
                                          "signal": "0"
                                      },
                                      "yc": {
                                          "signal": "0"
                                      },
                                      "innerRadius": {
                                          "signal": "0"
                                      },
                                      "tooltip": {
                                          "signal": "parent.tooltipResolved"
                                      }
                                  },
                                  "update": {
                                      "outerRadius": {
                                          "signal": "drawSize*parent.serieSize + " + (config.padding / 2)
                                      }
                                  }
                              }
                          },
                          {
                              "name": "planetSphereArcs",
                              /*"_comment": "Planet circle arc bg...",*/
                              "type": "arc",
                              "encode": {
                                  "enter": {
                                      "fill": {
                                          "signal": "scale('grayColor','gray4')"
                                          //"signal": "scale('serieColor',parent.serieIndex)"
                                          //"signal": "scale('serieArcColor',parent.serieIndex)"
                                      },
                                      "startAngle": {
                                          "signal": "0"
                                      },
                                      "endAngle": {
                                          "signal": "2*PI * parent.fact.val"
                                      },
                                      "xc": {
                                          "signal": "0"
                                      },
                                      "yc": {
                                          "signal": "0"
                                      },
                                      "innerRadius": {
                                          "signal": "0"
                                      },
                                      "tooltip": {
                                          "signal": "parent.tooltipResolved"
                                      }
                                  },
                                  "update": {
                                      "outerRadius": {
                                          "signal": "drawSize*parent.serieSize + "+(config.padding/2)
                                      }
                                  }
                              }
                          },
                          {
                              "name": "planetSpheres",
                              /*"_comment": "Planet circles...",*/
                              "type": "arc",
                              "encode": {
                                  "enter": {
                                      "fill": {
                                          "signal": "scale('serieColor',parent.serieIndex)"
                                      },
                                      "startAngle": {
                                          "signal": "0"
                                      },
                                      "endAngle": {
                                          "signal": "2*PI"
                                      },
                                      "xc": {
                                          "signal": "0"
                                      },
                                      "yc": {
                                          "signal": "0"
                                      },
                                      "innerRadius": {
                                          "signal": "0"
                                      },
                                      "tooltip": {
                                          "signal": "parent.tooltipResolved"
                                      }
                                  },
                                  "update": {
                                      "outerRadius": {
                                          "signal": "drawSize*parent.serieSize"
                                      }
                                  }
                              }
                          },
                          {
                              "name": "planetInsideLabels",
                              /*"_comment": "Text labels ontop of circle",*/
                              "type": "text",
                              "encode": {
                                  "enter": {
                                      "text": { "signal": "parent.serieIndex == 0 ? parent.labelResolvedLineBreaked : parent.valResolved" },
                                      "xc": { "value": 0 },
                                      "yc": { "value": 0 },
                                      "dy": { "signal": "parent.serieIndex != 0 ? 0 : "+calculateMultilineOffset("parent.labelResolvedLineBreaked.length",config.labelSize) },
                                      "align": { "value": "center" },
                                      "baseline": { "value": "middle" },
                                      "fill": { "signal": "scale('serieTextColor',parent.serieIndex)" },
                                      //"stroke": { "signal": "scale('serieColor',parent.serieIndex)" }, doesnt work, stroke is inside text
                                      //"strokeWidth": { "value": 1 },
                                      "font": { "value": config.labelFont },
                                      "fontSize": { "value": config.labelSize },
                                      "fontWeight": { "value": "bold" },// TODO??
                                      "tooltip": {
                                          "signal": "parent.tooltipResolved"
                                      } 
                                  },
                                  "update": {

                                  }
                              }
                          },
                          {
                              "name": "planetOutsideLabels",
                              /*"_comment": "Text labels next to circle",*/
                              "type": "text",
                              "encode": {
                                  "enter": {
                                      "text": { "signal": "parent.serieIndex == 0 ? null : parent.labelResolvedLineBreaked" },
                                      "dy": { "signal": "parent.serieIndex == 0 ? 0 : " + calculateMultilineOffset("parent.labelResolvedLineBreaked.length", config.textSize) },
                                      "yc": { "value": 0 },
                                      "fontSize": { "value": config.chartTextSize },
                                      "align": { "signal": "(parent.factAngle >= 180 ? 'right' : 'left')" },
                                      "baseline": { "value": "middle" },
                                      "limit": { "value": json.planetLabelLimitPixels },
                                      "ellipsis": { "value": json.planetLabelLimitEllipsis },
                                      "tooltip": {
                                          "signal": "parent.tooltipResolved"
                                      }
                                  },
                                  "update": {

                                      "xc": { "signal": "  (drawSize*parent.serieSize+"+(config.padding*1.5)+")  * (parent.factAngle >= 180 ? -1 : 1)" },
                                  }
                              }
                          }
                      ]
                  }
              ]
          }
        ]
    };
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.series;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.CoreSatelliteNode.init = function (instance, config, json, nodeElem) {
    // Pre-index series
    for (var s = 0; s < json.series.length; s++) {
        json.series[s].serieIndex = s;
        json.series[s].serieTotal = json.series.length;
        for (var f = 0; f < json.series[s].facts.length; f++) {
            json.series[s].facts[f].factIndex = f;
            json.series[s].facts[f].factTotal = json.series[s].facts.length;
            // Create string array for planet label
            var catResolved = Machinata.Reporting.Text.resolve(instance,json.series[s].facts[f].category);
            var categoryLineBreaked = null;
            if (s == 0) {
                // Planet label...
                categoryLineBreaked = Machinata.Reporting.Tools.lineBreakString(catResolved, {
                    maxCharsPerLine: json.planetLabelLimitChars,
                    maxLines: json.planetLabelLimitLines,
                    wordSeparator: " ",
                    ellipsis: json.planetLabelLimitEllipsis,
                    appendEllipsisOnLastLine: true,
                    returnArray: true
                });
                if (categoryLineBreaked == null) categoryLineBreaked = [];
                // Add spacer line break
                // Add value
                if (json.planetLabelAddSpaceBeforeValue == true && categoryLineBreaked.length < (json.planetLabelLimitLines)) categoryLineBreaked.push(" ");
                categoryLineBreaked.push(json.series[s].facts[f].resolved);
            } else {
                // Satellite label...
                categoryLineBreaked = Machinata.Reporting.Tools.lineBreakString(catResolved, {
                    maxCharsPerLine: json.satelliteLabelLimitChars,
                    maxLines: json.satelliteLabelLimitLines,
                    wordSeparator: " ",
                    ellipsis: json.planetLabelLimitEllipsis,
                    appendEllipsisOnLastLine: true,
                    returnArray: true
                });
            }
            // Register
            json.series[s].facts[f].categoryLineBreaked = categoryLineBreaked;
        }
    }
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.CoreSatelliteNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.CoreSatelliteNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








