/// <summary>
/// A very simple node that displays a info message for testing and demonstration
/// purposes. A user on a production system should not see this.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.InfoNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults = {};

/// <summary>
/// This node does not support a toolbar.
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.supportsToolbar = false;

/// <summary>
/// Defines the type of info. Can either be ```'info'``` or ```'warning'```.
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.infoType = "info";

/// <summary>
/// The info message to show (string).
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.message = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.chrome = "solid";


Machinata.Reporting.Node.InfoNode.init = function (instance, config, json, nodeElem) {

    var msg = json.message;
    //msg = Machinata.Reporting.Text.resolve(instance,msg);

    var bgElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
    var messageElem = $("<div class='message'></div>").text(msg).appendTo(bgElem);
    if (json.infoType == "warning") messageElem.prepend(Machinata.Reporting.buildIcon(instance,"alert"));
    else messageElem.prepend(Machinata.Reporting.buildIcon(instance,"info"));

};
Machinata.Reporting.Node.InfoNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








