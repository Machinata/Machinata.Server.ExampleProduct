


/// <summary>
/// A table node using HTML elements.
/// Supports filters, sticky headers, sorting and automatic grouping and merging of headers.
///
/// ## Sorting Rows
/// By default all tables can be sorted by clicking on the column headers. If a table has nested rows, then
/// only the first level is sorted. Row dimension groups always stay in place, regardless of the sort. This
/// means that all sorting happens within each row dimension group.
///
/// ## Filterting Rows
/// You can include a simple row search & filter tool by setting ```allowRowFiltering``` to ```true```.
/// Note that totals are not updated when filtering.
///
/// ## Nested Rows
/// Rows can be nested by including an additional array of dimension elements as ```childRows``` for each row 
/// dimension element. For nested rows, especially when ```nestedRowsAreInteractive``` is ```true```, each row
/// can be preconfigured as expanded or collapsed through it's ```expanded``` property.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VerticalTableNode = {};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults = {};

/// <summary>
/// By default, tables have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.supportsDashboard = false;

/// <summary>
/// Defines the minimum number of rows that the table must have before the header
/// automatically becomes sticky.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.stickyHeadersMinRows = 5;

/// <summary>
/// If enabled, the user may configure the table columns through a basic configuration UI.
/// If you would like a column to be initially hidden, you can set the property ```display``` to
/// ```false``` on a column.
/// You can use this setting together with column ```importance``` and the node setting ```maxColumnImportanceForDisplay``` to
/// automatically set a grouped set of columns that are initially displayed to the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowColumnConfiguration = false;

/// <summary>
/// If enabled, the user may filter the table row. Row filtering should only be enabled
/// on large tables where searching for cells makes sense. Tables with calculated totals 
/// and row filtering may provide a confusing (and inaccurate) view. You cannot row filter
/// nested tables.
/// By default this is ```false```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowRowFiltering = false;

/// <summary>
/// If enabled, the user may sort the table rows using the column headers. 
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowRowSorting = true;

/// <summary>
/// If not null, header groups are only displayed if they meet this minimum count.
/// By default this is ```2``` (meaning that a single group is not shown).
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.headerGroupsMinimumRequired = 2;

/// <summary>
/// If true, header groups are not shown if they have no labels. This takes into account empty-string group labels as well 
/// (meaning that if all header groups have empty string labels, they will not be shown).
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.headerGroupsHideIfNoLabels = true;

/// <summary>
/// If true, row group headers are not shown if they have no labels. This takes into account empty-string group labels as well 
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.rowGroupHeadersHideIfNoLables = true;

/// <summary>
/// If true, header groups are spaced using DOM table cells.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.insertHeaderSpacerCells = false;

/// <summary>
/// If an integer, each column or column group is automatically evaluated to and it's ```display``` value is automatically
/// set based on if it has a lower or equal ```importance``` than ```maxColumnImportanceForDisplay```.
/// If ```null```, this property has no effect.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.maxColumnImportanceForDisplay = null;

/// <summary>
/// If ```true```, nested rows are able to be collapsed and expanded by the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.nestedRowsAreInteractive = true;

/// <summary>
/// If ```true```, grouped rows with header facts are able to be collapsed and expanded by the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.groupedRowsAreInteractive = true;

/// <summary>
/// If ```true```, the user can easily copy the table to clipboard.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.addCopyToClipboardTool = true;

/// <summary>
/// Common helper routing for building a HTML table with the given table JSON data.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.buildTable = function (instance, config, json, nodeElem, interactive, useResolvedValues) {

    // Create table elem
    var tableElem = $("<table class='machinata-reporting-table'></table>");
    var tableHeadElem = $("<thead class='table-head'></thead>").appendTo(tableElem);

    // Init
    var allGroupLabelsEmpty = true;
    var columnIdToGroupMap = {}; // contains a map from column ids to their groups
    var groupNumber = 0;
    var dataRequiresStatsCompilation = false;
    var didAddRowLabel = false; // set to true if any row contains a label
    if (useResolvedValues == null) useResolvedValues = true; // default behaviour

    // Preflight for column selection
    $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {
        // Validate importance
        if (json.maxColumnImportanceForDisplay != null && groupJSON.importance != null && groupJSON.importance > json.maxColumnImportanceForDisplay) {
            if (groupJSON.display == null) groupJSON.display = false;
        }
        $.each(groupJSON.dimensionElements, function (index, columnJSON) {
            // Validate importance
            if (json.maxColumnImportanceForDisplay != null && columnJSON.importance != null && columnJSON.importance > json.maxColumnImportanceForDisplay) {
                if (columnJSON.display == null) columnJSON.display = false;
            }
        });
    });

    // Header groups
    var headerGroupsElem = $("<tr class='header groups'></tr>");

    // Placeholder for labels
    var headerGroupsLabelElem = $("<th class='label'></th>").appendTo(headerGroupsElem);
    $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {

        // Don't display?
        if (groupJSON.display == false) {
            return;
        }

        groupNumber++;

        // Spacer
        if (groupNumber > 1 && json.insertHeaderSpacerCells == true) {
            $("<th class='spacer'>&#160;</th>").appendTo(headerGroupsElem);
        }
        // Create th elem
        var headerElem = $("<th></th>").appendTo(headerGroupsElem);
        headerElem.attr("colspan", groupJSON.dimensionElements.length);
        headerElem.attr("data-grp-id", groupJSON.id);
        headerElem.addClass("group-type-" + groupJSON.groupType);
        if (groupJSON.valueType != null) headerElem.addClass("type-" + groupJSON.valueType);
        var groupText = Machinata.Reporting.Text.resolve(instance,groupJSON.name);
        if (groupText != null && groupText != "") allGroupLabelsEmpty = false;
        headerElem.text(groupText);
        headerElem.addClass("importance-" + groupJSON.importance);
    });
    // Add the header groups?
    // We only add header groups if the minimum number is reached and if they all have a label
    var addHeaderGroups = true;
    if (json.headerGroupsMinimumRequired != null && json.columnDimension.dimensionGroups.length < json.headerGroupsMinimumRequired) {
        addHeaderGroups = false;
    }
    if (json.headerGroupsHideIfNoLabels == true && allGroupLabelsEmpty == true) {
        addHeaderGroups = false;
    }
    if(addHeaderGroups == true) headerGroupsElem.appendTo(tableHeadElem);

    // Headers
    var headerRowElem = $("<tr class='header columns'></tr>").appendTo(tableHeadElem);
    var rowTemplateElem = $("<tr class='table-row'></tr>");
    groupNumber = 0;

    // Placeholder for labels
    var headerLabelElem = $("<th class='label'></th>").appendTo(headerRowElem);//if (json.rowHeaderName != null) headerLabelElem.text(Machinata.Reporting.Text.resolve(instance,json.rowHeaderName));
    if (json.rowHeaderName != null) headerLabelElem.text(Machinata.Reporting.Text.resolve(instance,json.rowHeaderName));
    $("<td class='label' data-col-id='LABEL'></th>").appendTo(rowTemplateElem);
    $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {

        // Don't display?
        if (groupJSON.display == false) {
            return;
        }

        groupNumber++;

        // Spacer
        if (groupNumber > 1 && json.insertHeaderSpacerCells == true) {
            $("<th class='spacer'>&#160;</th>").appendTo(headerRowElem);
            $("<td class='spacer'>&#160;</th>").appendTo(rowTemplateElem);
        }
        // Columns
        $.each(groupJSON.dimensionElements, function (index, columnJSON) {

            // Don't display?
            if (columnJSON.display == false) {
                return;
            }

            // Register in map
            columnIdToGroupMap[columnJSON.id] = groupJSON;

            // Create th elem
            var headerElem = $("<th></th>").appendTo(headerRowElem);
            var title = Machinata.Reporting.Text.resolve(instance,columnJSON.name);
            headerElem.addClass("column");
            headerElem.addClass("importance-" + columnJSON.importance);
            headerElem.addClass("type-" + columnJSON.valueType);
            headerElem.addClass("group-type-" + groupJSON.groupType);
            headerElem.attr("data-col-id", columnJSON.id);
            headerElem.text(title);
            
            // Create td elem (for template)
            var tdElem = $("<td class='data'></td>").appendTo(rowTemplateElem);
            tdElem.attr("data-col-id", columnJSON.id);
            tdElem.addClass("type-" + columnJSON.valueType);
            tdElem.attr("data-type", columnJSON.valueType);

            // Special handling for grouped columns (such as bar charts
            if (groupJSON.groupType == "bars") {
                headerElem.text("");
                if (index == 0) {
                    headerElem.attr("colspan", groupJSON.dimensionElements.length);
                    tdElem.attr("colspan", groupJSON.dimensionElements.length);
                    headerElem.attr("data-th-grp-id", groupJSON.id);
                    tdElem.attr("data-td-grp-id", groupJSON.id);
                    dataRequiresStatsCompilation = true;
                } else {
                    headerElem.remove();
                    tdElem.remove();
                }
            }
        });

    });

    // Stats compilation?
    var tableStatsMinValue = {};
    var tableStatsMaxValue = {};
    if (dataRequiresStatsCompilation == true) {
        $.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {
            $.each(groupJSON.dimensionElements, function (rowIndex, rowJSON) {

                // ROW

                $.each(rowJSON.facts, function (index, factJSON) {

                    // COLUMN
                    if (tableStatsMinValue[factJSON.col] == null || factJSON.val < tableStatsMinValue[factJSON.col]) {
                        tableStatsMinValue[factJSON.col] = factJSON.val;
                    }
                    if (tableStatsMaxValue[factJSON.col] == null || factJSON.val > tableStatsMaxValue[factJSON.col]) {
                        tableStatsMaxValue[factJSON.col] = factJSON.val;
                    }
                    var columnGroupJSON = columnIdToGroupMap[factJSON.col];
                    if (columnGroupJSON != null) {
                        if (tableStatsMinValue[columnGroupJSON.id] == null || factJSON.val < tableStatsMinValue[columnGroupJSON.id]) {
                            tableStatsMinValue[columnGroupJSON.id] = factJSON.val;
                        }
                        if (tableStatsMaxValue[columnGroupJSON.id] == null || factJSON.val > tableStatsMaxValue[columnGroupJSON.id]) {
                            tableStatsMaxValue[columnGroupJSON.id] = factJSON.val;
                        }
                    }
                    
                });
            });
        });
    }

    // Row groups
    groupNumber = 0;
    json.numRows = 0;
    $.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {

        // Register and create new tbody group
        groupNumber++;
        var tbodyElem = $("<tbody class='table-body'></tbody>").appendTo(tableElem);
        tbodyElem.addClass("importance-" + groupJSON.importance);
        tbodyElem.addClass("group-" + groupNumber);

        // Initial group header
        var groupName = Machinata.Reporting.Text.resolve(instance,groupJSON.name);
        var headerGroupRowJSON = null;
        if ((groupName != null && groupName != "") || json.rowGroupHeadersHideIfNoLables == false) {
            // Create header row
            var rowElem = createRow(json.numRows, groupJSON, 0, null, null);
            rowElem.addClass("group-header");
            rowElem.addClass("static"); // for tablesorter widget
            // Extract label
            var labelElem = rowElem.find("[data-col-id='LABEL']");
            if (labelElem.length == 0) {
                labelElem = rowElem.find("td").first(); // if no label, fallback to first cell
            } else {
                didAddRowLabel = true;
            }
            // Calculate colspan
            var colSpan = 1;
            var tdi = 1;
            var tdElems = rowElem.find("td");
            while (tdi < tdElems.length) {
                tdi++;
                var tdElem = tdElems.eq(tdi);
                if (tdElem.hasClass("has-definition")) {
                    break;
                } else {
                    tdElem.addClass("mark-for-deletion");
                    colSpan++;
                }
            }
            rowElem.find("td.mark-for-deletion").remove();
            if (colSpan > 1) labelElem.attr("colspan", colSpan);
            if (json.groupedRowsAreInteractive == true && groupJSON.dimensionElements.length > 0) {
                // Register class
                rowElem.addClass("has-child-rows");
                tableElem.addClass("has-nested-rows");
                // Make clickable
                if (interactive == true) {
                    if (groupJSON.expanded == false) {
                        rowElem.addClass("collapsed");
                    } else {
                        rowElem.addClass("expanded");
                    }
                    rowElem.find("td").first().prepend(Machinata.Reporting.buildIcon(instance,"expand")).prepend(Machinata.Reporting.buildIcon(instance,"collapse"));
                    rowElem.addClass("toggable");
                    rowElem.click(function () {
                        Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, $(this));
                    });
                }
            }
            // Register
            headerGroupRowJSON = groupJSON;
        }

        // Row create helper function
        function createRow(rowIndex, rowJSON, rowIndent, parentRowJSON, parentRowElem) {
            // ROW
            //if (rowIndex > 300) return; //TODO: @dan allow a dynamic UI for loading lots of rows

            var rowElem = rowTemplateElem.clone().appendTo(tbodyElem);
            rowElem.attr("data-row-id", rowJSON.id);
            rowElem.addClass("importance-" + rowJSON.importance);
            json.numRows++;
            
            // Parent or header?
            if (parentRowJSON != null && parentRowElem != null) {
                // Has a parent row
                rowElem.addClass("is-child-row");
                rowElem.addClass("tablesorter-childRow"); // for tablesorter widget
                rowElem.attr("data-parent-row-id", parentRowJSON.id);
                json.allowRowFiltering = false; // do not allow row filtering!
            } else if (json.groupedRowsAreInteractive == true && headerGroupRowJSON != null) {
                // Has header group
                rowElem.attr("data-parent-row-id", headerGroupRowJSON.id);
                if (groupJSON.expanded == false) {
                    rowElem.addClass("hidden");
                }
            }

            //TODO: 
            //if (rowJSON["class"] != null) rowElem.addClass("class-" + rowJSON["class"]);
            //if (rowJSON["class"] == "total") rowElem.addClass("static").attr("data-row-index", "100%");

            // Row label
            var label = Machinata.Reporting.Text.resolve(instance,rowJSON.name);
            if (label != null && label != "") {
                didAddRowLabel = true;
                var labelElem = rowElem.find("[data-col-id='LABEL']"); //TODO: this is probably a performance bottleneck
                labelElem.text(label);
                labelElem.attr("data-text", rowJSON.sort || label);
                labelElem.attr("data-sort", rowJSON.sort || label);
            }

            // Indent?
            if (rowIndent == null) rowIndent = 0;
            if (rowJSON.indent != null) rowIndent += rowJSON.indent;
            if (rowIndent > 0) {
                rowElem.addClass("indent-" + rowIndent);
            }

            // Link?
            if (interactive == true && rowJSON.link != null) {
                rowElem.addClass("has-link");
                rowElem.attr("title", Machinata.Reporting.Text.resolve(instance,rowJSON.link.tooltip));
                rowElem.click(function () {
                    if (rowJSON.link.drilldown != null) {
                        tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                        rowElem.addClass("selected-drilldown");
                    }
                    Machinata.Reporting.Tools.openLink(instance,rowJSON.link);
                });
            }

            // Row data columnms (facts)
            $.each(rowJSON.facts || rowJSON.headerFacts, function (index, factJSON) {

                // COLUMN

                // Find matching column
                // Normally, each fact has it's own column, but some are virtually combined into a single column as a group (such as bar charts)
                var dataElem = null;
                var columnGroupJSON = columnIdToGroupMap[factJSON.col]; // gets the column group defintion, if any
                if (columnGroupJSON != null && columnGroupJSON.groupType == "bars") {
                    dataElem = rowElem.find("[data-td-grp-id='" + columnGroupJSON.id + "']");
                    
                    // Chart cell

                    if (columnGroupJSON.groupType == "bars") {
                        // Lookup theme
                        var columnName = factJSON.col;
                        var shade = config.themeColorShadeNames[0]; //fallback
                        $.each(columnGroupJSON.dimensionElements, function (index, columnJSON) {
                            if (columnJSON.id == factJSON.col) {
                                if (columnJSON.colorShade != null) {
                                    shade = columnJSON.colorShade;
                                } else {
                                    // Cycle all shades
                                    shade = Machinata.Reporting.Config.themeColorShadeNames[index % Machinata.Reporting.Config.themeColorShadeNames.length];
                                }
                                /* DEPRECATED:
                                // Do we have exactly two?
                                if (columnGroupJSON.dimensionElements.length == 2) {
                                    // Use neg/pos color shades
                                    columnName = Machinata.Reporting.Text.resolve(instance,columnJSON.name);
                                    if (index == 0) {
                                        if (factJSON.val < 0) shade = config.themeNegativePositiveShadeNames[0];
                                        else shade = config.themeNegativePositiveShadeNames[1];
                                    } else {
                                        if (factJSON.val < 0) shade = config.themeNegativePositiveGrayShadeNames[0];
                                        else shade = config.themeNegativePositiveGrayShadeNames[1];
                                    }
                                } else {
                                    // Cycle all shades
                                    shade = Machinata.Reporting.Config.themeColorShadeNames[index % Machinata.Reporting.Config.themeColorShadeNames.length];
                                }*/
                            }
                        });
                        // Get the relative value according to min max
                        var min = tableStatsMinValue[columnGroupJSON.id];
                        var max = tableStatsMaxValue[columnGroupJSON.id];
                        // Balance min/max
                        if (min < 0 && max > 0) {
                            if (Math.abs(min) < Math.abs(max)) min = max * -1;
                            else if (Math.abs(min) > Math.abs(max)) max = min * -1;
                        }
                        var val = factJSON.val;
                        var pPos = 0;
                        var pNeg = 0;
                        if (val > 0) pPos = val / max;
                        if (val < 0) pNeg = val / min;
                        var barsElem = $("<div class='balanced-bars'></div>");
                        var tooltip = columnName + ": " + factJSON.resolved;//todo col
                        // Positive bar
                        if (max >= 0) {
                            var barPositiveElem = Machinata.UI.microBar(pPos, tooltip).addClass("positive");
                            barPositiveElem.find(".bar").addClass("theme-bg-" + shade);
                            barsElem.append(barPositiveElem);
                            barsElem.addClass("has-positive");
                        }
                        // Negative bar
                        if (min < 0) {
                            var barNegativeElem = Machinata.UI.microBar(pNeg, tooltip).addClass("negative");
                            barNegativeElem.find(".bar").addClass("theme-bg-" + shade);
                            barsElem.append(barNegativeElem);
                            barsElem.addClass("has-negative");
                        }

                        dataElem.append(barsElem);
                    }
                } else {

                    // Text cell

                    // Find matching fact column and apply data
                    dataElem = rowElem.find("[data-col-id='" + factJSON.col + "']"); //TODO: this is probably a performance bottleneck
                    if (useResolvedValues == true) {
                        // Resolved values
                        dataElem.text(factJSON.resolved);
                    } else {
                        // Use raw value
                        if (factJSON.val != null) {
                            dataElem.text(factJSON.val);
                        } else {
                            dataElem.text(factJSON.resolved); // fallback, not all cells have a value
                        }
                    }
                    dataElem.attr("data-text", factJSON.sort || factJSON.resolved);
                    dataElem.attr("data-sort", factJSON.sort || factJSON.val);

                    // Overwrite type?
                    if (factJSON.valueType != null) {
                        dataElem.addClass("type-" + factJSON.valueType);
                        dataElem.attr("data-type", factJSON.valueType);
                    }

                    // Tooltip?
                    if (factJSON.tooltip != null) {
                        dataElem.attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.tooltip));
                    }

                }

                // Mark as having explicit fact defintion
                dataElem.addClass("has-definition");

                // Link?
                if (interactive == true && factJSON.link != null) {
                    dataElem.addClass("has-link");
                    dataElem.attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.link.tooltip));
                    dataElem.click(function () {
                        if (factJSON.link.drilldown != null) {
                            tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                            dataElem.addClass("selected-drilldown");
                        }
                        Machinata.Reporting.Tools.openLink(instance,factJSON.link);
                    });
                }

                // Add alert?
                if (factJSON.alert != null) {
                    dataElem.addClass("has-icon");
                    var icon = factJSON.alert.icon;
                    if (icon == null) icon = "alert"; // fallback
                    //dataElem.prepend($("<span class='icon alert " + Machinata.Reporting.icon(icon) + "'></span>").attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.alert.hint)));
                    dataElem.prepend(Machinata.Reporting.buildIcon(instance,icon).addClass("alert").attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.alert.hint)));
                }

                // Add icon?
                if (factJSON.icon != null) {
                    dataElem.addClass("has-icon");
                    if (factJSON.icon == "trend") {
                        if (factJSON.val > 0) {
                            dataElem.addClass("green-number");
                            //dataElem.append("<span class='icon " + Machinata.Reporting.icon("trend-up") + "'></span>");
                            dataElem.append(Machinata.Reporting.buildIcon(instance,"trend-up"));
                        } else {
                            dataElem.addClass("red-number");
                            //dataElem.append("<span class='icon " + Machinata.Reporting.icon("trend-down") + "'></span>");
                            dataElem.append(Machinata.Reporting.buildIcon(instance,"trend-down"));
                        }
                    } else {
                        //dataElem.append("<span class='icon " + Machinata.Reporting.icon(factJSON.icon) + "'></span>");
                        dataElem.append(Machinata.Reporting.buildIcon(instance,factJSON.icon));
                    }
                }
            });

            // Nested rows?!
            if (rowJSON.childRows != null) {
                if (rowJSON.childRows.length > 0) {
                    // Register class
                    rowElem.addClass("has-child-rows");
                    tableElem.addClass("has-nested-rows");
                    // Make clickable
                    if (interactive == true && json.nestedRowsAreInteractive == true) {
                        rowElem.addClass("toggable");
                        rowElem.find("td").first().prepend(Machinata.Reporting.buildIcon(instance,"expand")).prepend(Machinata.Reporting.buildIcon(instance,"collapse"));
                        rowElem.click(function () {
                            Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, $(this));
                        });
                    }
                }
                $.each(rowJSON.childRows, function (childRowIndex, childRowJSON) {
                    createRow(childRowIndex, childRowJSON, rowIndent + 1, rowJSON, rowElem); //TODO: what do we do about the index?
                });

                Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, rowElem, rowJSON.expanded != false);
            }

            return rowElem;
        }

        // Process each row
        $.each(groupJSON.dimensionElements, function (rowIndex, rowJSON) {

            var rowElem = createRow(rowIndex, rowJSON);

            // Last in group
            if (rowIndex == groupJSON.dimensionElements.length - 1 && groupIndex != json.rowDimension.dimensionGroups.length - 1) {
                rowElem.addClass("last-in-group");
            }
            
        });
    });

    // Post-processing

    // Did we never add a label?
    if (didAddRowLabel == false) {
        tableElem.find("td.label,th.label").remove();
    }

    return tableElem;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal = function (tableElem, rowElem, show) {

    // Helper method for recursion...
    function _toggleAllChildrenRows(rowElem, show) {
        var selector = "[data-parent-row-id='" + rowElem.attr("data-row-id") + "']";
        var childRowElems = tableElem.find(selector);
        if (show == true) {
            childRowElems.removeClass("hidden");
        } else {
            childRowElems.addClass("hidden");
        }
        // Recurse
        childRowElems.each(function () {
            if (show == true && $(this).hasClass("collapsed")) return;
            _toggleAllChildrenRows($(this), show);
        });
    }

    // Parent state
    if (show == null) show = !rowElem.hasClass("expanded");
    if (show == true) {
        rowElem.addClass("expanded");
        rowElem.removeClass("collapsed");
    } else {
        rowElem.removeClass("expanded");
        rowElem.addClass("collapsed");
    }

    // Initial toggle
    _toggleAllChildrenRows(rowElem, show);
};



/// <summary>
/// Toggles a set of table columns or groups on the table.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.toggleColumn = function (instance, config, json, nodeElem, colId, grpId, show) {
    if (show == null) show = true; //TODO: @dan

    // Init selection
    var tableElem = nodeElem.data("table");
    var elemsCols = null;
    var elemsGrps;
    if (colId != null) elemsCols =tableElem.find("[data-col-id='" + colId + "']");
    if (grpId != null) elemsGrps = tableElem.find("[data-grp-id='" + grpId + "']");

    // Toggle
    if (show == true) {
        if (elemsCols != null) elemsCols.show();
        if (elemsGrps != null) elemsGrps.show();
    } else {
        if (elemsCols != null) elemsCols.hide();
        if (elemsGrps != null) elemsGrps.hide();
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.rebuild = function (instance, config, json, nodeElem) {
    // Already exists?
    if (nodeElem.data("table") != null) {
        nodeElem.data("table").remove();
    }

    // Create table elem
    var tableElem = this.buildTable(instance, config, json, nodeElem, true, true);
    nodeElem.append(tableElem);
    nodeElem.data("table", tableElem);

    // Make sortable and filterabile
    // See https://mottie.github.io/tablesorter/docs/ for more documentation
    // Filter plugin: https://mottie.github.io/tablesorter/docs/example-node-filter.html
    // Static row plugin: https://mottie.github.io/tablesorter/docs/example-node-static-row.html
    if (json.allowRowSorting == true) {
        //console.log("makeTableSortable");
        Machinata.Reporting.Tools.makeTableSortable(instance,tableElem);
    }

    // Make sticky, but only if it makes sense
    if (json.stickyHeadersMinRows != null && json.numRows > json.stickyHeadersMinRows && config.profile != "webprint") {
        tableElem.stickyTableHeaders({
            cacheHeaderHeight: true,
            fixedOffset: $('#header')
        });
        tableElem.on("enabledStickiness.stickyTableHeaders", function () {
            $(this).addClass("header-is-sticky");
        });
        tableElem.on("disabledStickiness.stickyTableHeaders", function () {
            $(this).removeClass("header-is-sticky");
        });
    }

    return tableElem;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.init = function (instance, config, json, nodeElem) {
    var self = this;
        
    // Initial rebuild
    Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem);
    

    // Export
    Machinata.Reporting.Node.registerExportOptions(instance, config, json, nodeElem, {
        "xls": "Excel",
        "csv": "CSV",
    });


    // Copy
    if (json.addCopyToClipboardTool == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.copy-to-clipboard"), "copy", function () {
            // Create a raw copy of table
            //TODO: this doesnt include the filters sorts - do we want that?
            //TODO: LibreOffice doesnt copy table nicely
            var tempTableElem = self.buildTable(instance, config, json, nodeElem, false, true);
            Machinata.Reporting.Tools.copyElementToClipboard(tempTableElem, Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied.table'), true);
        });
    }

    // Filter
    var filterToolElem = null;
    if (json.allowRowFiltering == true) {
        filterToolElem = Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.search-filter"), "filter", function (toolElem) {
            nodeElem.data("table").toggleClass("option-filter");
            if (nodeElem.data("table").hasClass("option-filter")) {
                toolElem.addClass("active");
            } else {
                nodeElem.data("table").trigger('filterReset');
                toolElem.removeClass("active");
            }
        }, "fullscreen-supported");
    }

    // Column selection
    if (json.allowColumnConfiguration == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.configure"), "configure", function () {
            // Create a UI for each column
            var diag = Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, 'reporting.nodes.configure'), '');
            diag.elem.append($("<p></p>").text(Machinata.Reporting.Text.translate(instance, 'reporting.tables.select-columns')));
            var checkboxesElem = $("<div></div>");
            var checkboxesCount = 0;
            $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {
                $.each(groupJSON.dimensionElements, function (index, columnJSON) {
                    var checkboxId = Machinata.Reporting.uid();
                    var checkboxUIElem = $('<div class="machinata-reporting-checkbox"><input type="checkbox"/><label></label></div>');
                    checkboxUIElem.find("label").append(Machinata.Reporting.buildIcon(instance,"check"));
                    checkboxUIElem.find("label")
                        .attr("for", checkboxId)
                        .append(Machinata.Reporting.Text.resolve(instance,columnJSON.name));
                    if(columnJSON.display != false) checkboxUIElem.find("input").attr("checked", "checked")
                    checkboxUIElem.find("input")
                        .attr("id", checkboxId)
                        .attr("data-grp-id", groupJSON.id)
                        .attr("data-col-id", columnJSON.id)
                        .on("change", function () {
                            //TODO: limit max checked columns?
                            var checked = $(this).is(':checked');
                            columnJSON.display = checked;
                        });
                    checkboxesElem.append(checkboxUIElem);
                    checkboxesCount++;
                });
            });
            if (checkboxesCount > 8) {
                checkboxesElem.css("column-count", "2");
                checkboxesElem.css("column-gap", config.padding+"px");
            }
            diag.elem.append(checkboxesElem);
            diag.cancelButton();
            diag.okayButton();
            diag.okay(function () {
                // User wants an update - call rebuild
                Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem);
            });
            diag.show();
        });
    }


    // Do we have preset highlight?
    if (config.highlight != null) {
        config.highlight = config.highlight.toLowerCase();
        nodeElem.data("table").find("td").each(function () {
            var dataElem = $(this);
            var dataElemSearch = dataElem.text().toLowerCase();
            if (dataElemSearch.indexOf(config.highlight) != -1) {
                dataElem.addClass("selected-highlight");
            }
        });
    }
    /*
    if (config.highlights != null && config.highlights.length > 0) {
        // Compile filter array (needed by tablesorter)
        var highlightsArray = [];
        $.each(data.header, function (index, headerJSON) {
            // Find matching value in config
            var searchValue = null;
            $.each(config.highlights, function (index, highlightJSON) {
                if (highlightJSON.key == headerJSON.name) {
                    searchValue = highlightJSON.value;
                }
            });
            // Register 
            highlightsArray[index] = searchValue;
        });
        alert(JSON.stringify(highlightsArray));
        // Apply filter
        tableElem.trigger("search", highlightsArray);
    }*/

    // Do we have preset sorts?
    //TODO: Support this on the data side as well
    if (json.sorts != null) {
        // Compile filter array (needed by tablesorter)
        var sortArray = []; // For example, [[1,0], [2,0]]
        // Find matching value in config
        for (sortKey in config.sorts) {
            // Register 
            var sortDir = 0;
            if (config.sorts[sortKey] == "asc") sortDir = 0;
            if (config.sorts[sortKey] == "desc") sortDir = 1;
            // The value contains an array of instructions for per-column sorting and direction in the format: 
            // [[columnIndex, sortDirection], ... ] where columnIndex is a zero-based index for your columns left-to-right and sortDirection is 0 for Ascending and 1 for Descending. A valid argument that sorts ascending first by column 1 and then column 2 looks like: [[0,0],[1,0]].
            // Note: the above documentation is misleading: if you have groups, then each group offsets the column by one.
            // Instead of using the column header index, we just go look it up
            var columnHeaderElem = nodeElem.data("table").find("th[data-name='" + sortKey + "']");
            sortArray.push([parseInt(columnHeaderElem.attr("data-column")), sortDir]);
        }
        // Apply sort
        nodeElem.data("table").trigger('sorton', [sortArray]); // From documentation: $( 'table' ).trigger( 'sorton', [ [[0,0],[2,0]] ] );
    }

    // Do we have preset filters?
    if (json.filters != null) {
        // Compile filter array (needed by tablesorter) (needs to be prefilled with null)
        var filterArray = [];
        nodeElem.data("table").find("th").each(function () {
            filterArray[parseInt($(this).attr("data-column"))] = null;
        });
        // Find matching value in config
        for (filterKey in json.filters) {
            filterValue = json.filters[filterKey];
            // Instead of using the column header index, we just go look it up
            var columnHeaderElem = nodeElem.data("table").find("th[data-name='" + filterKey + "']");
            filterArray[parseInt(columnHeaderElem.attr("data-column"))] = filterValue;
        }
        // Apply filter
        //BUG: bug in tablesorter plugin, requires delay
        setTimeout(function () {
            $.tablesorter.setFilters(nodeElem.data("table"), filterArray, true);
        }, 100);
        // Toggle toolbar element
        nodeElem.data("table").addClass("option-filter");
        filterToolElem.addClass("active");
    }

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    var self = this;
    if (format == "xls") {
        var tempTableElem = self.buildTable(instance, config, json, nodeElem, false, false);
        Machinata.Data.exportTableAsExcel(tempTableElem, filename);
        return true;
    }
    if (format == "csv") {
        var tempTableElem = self.buildTable(instance, config, json, nodeElem, false, false);
        Machinata.Data.exportTableAsCSV(tempTableElem, filename);
        return true;
    }
    return false;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};









