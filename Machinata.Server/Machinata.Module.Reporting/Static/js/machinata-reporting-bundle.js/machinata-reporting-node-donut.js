


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.DonutNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults = {};

/// <summary>
/// Donuts and pies have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.insertLegend = true;

/// <summary>
/// Defines the inner radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.innerRadius = 0.5;

/// <summary>
/// Defines the outer radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.outerRadius = 1.0;

/// <summary>
/// If ```true```, displays labels inside the slices with a slide angle larger than ```sliceLablesMinimumDegrees```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabels = true;

/// <summary>
/// If ```true```, includes the slice value in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsShowValue = true;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsShowCategory = false;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.legendLabelsShowValue = false;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the minimum slice angle required for the label to display.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsMinimumDegrees = 36;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the offset from the outer edge in pixels.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsOffsetPixels = 4;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the font size in pixels for the labels.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsSize = Machinata.Reporting.Config.chartTextSize;

/// <summary>
/// If ```true```, sorts the facts automatically.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sortFacts = false;

/// <summary>
/// If enabled, if the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%),
/// we use the facts full formatting (```fact.format```), otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// For this setting to work, the fact format specified (```fact.format```) must be of type percent (ie '.2%').
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.automaticallyFormatEdgeNumbers = true;


Machinata.Reporting.Node.DonutNode.getVegaSpec = function (instance, config, json, nodeElem) {



    // Create the sort (collection) transform. If no sort is given, we just provide an empty transform
    var collectTransform = {"type": "collect"};
    if (json.sortFacts != false && json.sortFacts != null) {
        collectTransform["sort"] = {
            "field": "fact.val",
            "order": [json.sortFacts]
        };
    }
    // Create slice label template
    var sliceLabelTemplate = "''";
    if (json.sliceLabels == true && json.sliceLabelsShowCategory == true) sliceLabelTemplate += " + datum.fact.category.resolved";
    if (json.sliceLabels == true && json.sliceLabelsShowCategory == true && json.sliceLabelsShowValue == true) sliceLabelTemplate += " + ': '";
    if (json.sliceLabels == true && json.sliceLabelsShowValue == true) sliceLabelTemplate += " + datum.valResolved";

    // Create legend label template
    var legendLabelTemplate = "''";
    if (json.legendLabelsShowCategory == true) legendLabelTemplate += " + datum.fact.category.resolved";
    if (json.legendLabelsShowCategory == true && json.legendLabelsShowValue == true) legendLabelTemplate += " + ': '";
    if (json.legendLabelsShowValue == true) legendLabelTemplate += " + datum.valResolved";

    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "seriesResolved",
              "source": "series",
              "transform": [
                  {
                      "type": "flatten",
                      "fields": ["facts"],
                      "as": ["fact"]
                  }
              ]
          },
          {
              "name": "facts",
              "source": "seriesResolved",
              "transform": [
                  { "type": "formula", "initonly": true, "as": "key", "expr": "datum.fact.category.resolved" },
                  { "type": "formula", "initonly": true, "as": "valResolved", "expr": "datum.fact.resolved" },
                  { "type": "formula", "initonly": true, "as": "tooltipResolved", "expr": "datum.fact.category.resolved + ': ' + datum.valResolved" },
                  { "type": "formula", "initonly": true, "as": "labelResolved", "expr": legendLabelTemplate }, // for legend
                  { "type": "formula", "initonly": true, "as": "sliceLabelResolved", "expr": sliceLabelTemplate },
                  collectTransform,
                {
                    "type": "pie",
                    "field": "fact.val",
                    "startAngle": 0,
                    "endAngle": 6.29,
                    "sort": false
                }
              ]
          }
        ],
        "signals": [
            {
                "name": "drawZeroLine",
                "init": "scale('color',data('facts')[0].key) == scale('color',data('facts')[length(data('facts'))-1].key)",
            },
            {
                "name": "innerRadius",
                "update": "(min(width,height) / 2)*" + json.innerRadius,
                
            },
            {
                "name": "outerRadius",
                "update": "(min(width,height) / 2)*" + json.outerRadius,
            }
        ],
        "scales": [
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "facts", "field": "key" },
              "range": { "scheme": json.theme }
          },
          {
              "name": "sliceTextColor",
              "type": "ordinal",
              "domain": { "data": "facts", "field": "key" },
              "range": { "scheme": "text-on-" + json.theme }
          },
            {
                // Provide the proper label for the legend
                "name": "legendLabel",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "key" },
                "range": { "data": "facts", "field": "labelResolved" }
            },
          {
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "facts", "field": "key" },
              "range": { "scheme": json.theme }
          },
        ],

        "marks": [
          {
              "name": "markSlices",
              "type": "arc",
              "from": { "data": "facts" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "color", "field": "key" },
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "field": "startAngle" },
                      "endAngle": { "field": "endAngle" },
                      "tooltip": { "field": "tooltipResolved" },
                      "padAngle": { "value": 0 },
                      "innerRadius": { "signal": "innerRadius" },
                      "outerRadius": { "signal": "outerRadius" },
                      "cornerRadius": { "value": 0 }
                  }
              }
          },
          {
              "name": "markSliceLabels",
              "type": "text",
              "from": {
                  "data": "facts"
              },
              "encode": {
                  "enter": {
                      "text": {
                          // Only show the label if it meets the requirements of a min angle
                          "signal": "if(" + json.sliceLabels + " == false || ((datum['endAngle'] - datum['startAngle'])*(180/PI) < " + json.sliceLabelsMinimumDegrees + "), '', datum.sliceLabelResolved)"
                      },
                      "fill": { "scale": "sliceTextColor", "field": "key" },

                      "fontSize": { "value": json.sliceLabelsSize },
                      "align": {
                          // This depends on whether the label is on the left right side
                          "signal": "if(datum['endAngle']*(180/PI) < 180, 'right', 'left')"
                      },
                      "baseline": {
                          // This depends on whether the label is on the left right side
                          "signal": "if(datum['endAngle']*(180/PI) < 180, 'bottom', 'top')"
                      },
                      "dx": {
                          // This depends on whether the label is on the left right side
                          "signal": "if(datum['endAngle']*(180/PI) < 180, -" + json.sliceLabelsOffsetPixels + ", " + json.sliceLabelsOffsetPixels + ")"
                      },
                      "dy": {
                          // This depends on whether the label is on the left right side
                          "signal": "if(datum['endAngle']*(180/PI) < 180, -" + json.sliceLabelsOffsetPixels + ", " + json.sliceLabelsOffsetPixels + ")"
                      },
                  },
                  "update": {
                      "tooltip": { "field": "tooltipResolved" },
                      "x": {
                          "signal": "width / 2"
                      },
                      "y": {
                          "signal": "height / 2"
                      },
                      "radius": {
                          "signal": "(min(width,height) / 2)*1"
                      },
                      "theta": {
                          "signal": "datum['endAngle']"
                      },
                      "angle": {
                          // The angle is the endAngle value, but we need to add 90 degrees depending on whether its on the left/right side
                          "signal": "if(datum['endAngle']*(180/PI) < 180, (datum['endAngle']*(180/PI))-90, (datum['endAngle']*(180/PI))+90)"
                      }
                  }
              }
          },
          {
              "type": "rect",
              "encode": {
                  "enter": {
                      "fill": {
                          "value": (json.chrome == "dark" ? config.darkColor : (json.chrome == "solid" ? config.solidColor : config.whiteColor))
                      },
                      "opacity": {
                          "signal": "drawZeroLine == true ? 1 : 0"
                      }
                  },
                  "update": {
                      
                      "x": {
                          "signal": "width / 2"
                      },
                      "y": {
                          "signal": "0"
                      },
                      "width": {
                          "signal": config.domainLineSize
                      },
                      "height": {
                          "signal": "(outerRadius - innerRadius) + 0" //  + config.domainLineSize
                      },
                      
                  }
              }
          }
        ]
    };
};

Machinata.Reporting.Node.DonutNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.series;
};


Machinata.Reporting.Node.DonutNode.init = function (instance, config, json, nodeElem) {
    // Create legend
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeriesFacts(instance, config, json, json.series);

    // Normalize facts to rounded percentages, but only if fact is very edge of boundaries...
    if (json.automaticallyFormatEdgeNumbers == true) {
        for (var si = 0; si < json.series.length; si++) {
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers(json.series[si].facts);
        }
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.DonutNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.DonutNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








