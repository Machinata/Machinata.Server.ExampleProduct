
/// <summary>
/// ## Axis's
/// Scaling Engine: ```Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis```
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.HBarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults = {};

/// <summary>
/// By default charts are ```solid```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.legendColumns = 2;

/// <summary>
/// Defines the main yScale band padding for the bars, in decimal-percent.
/// Default is ```0.3```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.bandPaddingPercent = 0.3;

/// <summary>
/// Defines the the width ratio the labels on the left side should use.
/// Default is ```0.5```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.labelRatio = 0.5;

/// <summary>
/// Defines the maximum height of a bar in pixels. If there is not enough space, then the bar may be
/// smaller.
/// By default this is ```Machinata.Reporting.Config.horizontalBarMaxSize```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.barMaxSize = Machinata.Reporting.Config.horizontalBarMaxSize;

/// <summary>
/// If not zero, a bar with a non-zero value is snapped to this size (in pixels).
/// By default ```3```.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.barMinValueWidth = 3;

/// <summary>
/// When ```true```, the X-Axis domain is automatically adjusted through different mechanisms
/// to make the axis' as pretty and nice as possible. 
///
/// Note: If a series contains a xAxis definitions with a minValue
/// and maxValue, then the autoSnap is not performed on this series.
///
/// By default ```true```.
///  
/// See also:
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisSnapToZero
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisNiceDomain
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisMargin
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisBalance
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxis = true;

/// <summary>
/// When ```autoSnapXAxis``` is ```true```, snaps the axis' domain min value to zero. This only happens
/// if all values are either ```> 0``` or all values are ```< 0```.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisSnapToZero = true;

/// <summary>
/// When ```autoSnapXAxis``` is ```true```, rounds the axis' domain to nice numbers.
/// Uses D3's domain scale ```nice()```.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisNiceDomain = true;

/// <summary>
/// By default ```false```
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisExtendToDomain = false;

/// <summary>
/// When ```autoSnapXAxis``` is ```true```, defines the margin to add to the axis domain
/// in decimal percent.
/// By default ```0.05```, or 5%.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisMargin = 0.10;

/// <summary>
/// When ```autoSnapXAxis``` is ```true```, this feature will automaticall balance the domain on negative and positive. 
/// Only applies to domains that span from negative to positive.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisBalance = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.autoSnapXAxisTickCount = 5;

/// <summary>
/// When ```fillChartSpace``` is ```false```, and there is enough space, the bars
/// are automatically stacked from top to bottom, not filling the entire chart space. 
/// If there is not enough space, then the bars are fit to the
/// chart area.
/// By default ```true``` on web, ```false``` for print.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.fillChartSpace = true;

/// <summary>
/// By default we use a ```greedy``` overlap strategy.
/// </summary>
Machinata.Reporting.Node.HBarNode.defaults.axisLabelOverlap = "greedy";


/// <summary>
/// </summary>
Machinata.Reporting.Node.HBarNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var ROUND = (config.roundToNearestFullPixel ? "round" : ""); // template for rounding function to use (ie non if not requested by config)
    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          },
          {
              "name": "facts",
              "source": "series",
              "transform": [
                {
                    "type": "flatten",
                    "fields": ["facts"],
                    "as": ["fact"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [

                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id" // serie id
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "category",
                      "expr": "datum.fact.category"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "label",
                      "expr": "datum.fact.label"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "val",
                    "expr": "datum.fact.val"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "categoryResolved",
                    "expr": "datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "valResolved",
                      "expr": "datum.title != null ? (datum.title.resolved + ': ' + datum.fact.resolved) : (datum.fact.resolved)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "labelResolved",
                      "expr": "datum.label != null ? (datum.label.resolved) : (null)"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "groupResolved",
                    "expr": "datum.group ? datum.group.resolved : datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "formatType",
                    "expr": "datum.fact.formatType ? datum.fact.formatType : 'number'" // default
                },
                /*{
                    "type": "formula",
                    "initonly": true,
                    "as": "format_OLD",
                    "expr": "datum.fact.format ? datum.fact.format : '.1%'" 
                },*/
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "format",
                    "expr": "(data('series')[0].xAxis != null && data('series')[0].xAxis.format != null) ? data('series')[0].xAxis.format : '.1%'"  // TODO: @bmpi, default fallback
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "serieId",
                    "expr": "datum.id"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "colorResolved",
                      "expr": "datum.colorShade != null ? scale('colorByShade',datum.colorShade) : scale('color',datum.serieId)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "textColorResolved",
                      "expr": "datum.colorShade != null ? scale('colorByShadeText',datum.colorShade) : scale('colorText',datum.serieId)"
                  }
              ]
          },
          {
              "name": "factsCategories",
              "source": "factsResolved",
              "transform": [
                {
                    "type": "aggregate",
                    "groupby": ["groupResolved"],
                    "groupby": ["groupResolved","categoryResolved"]
                }
              ]
          }
        ],
        "signals": [
            {
                "name": "xScaleRightLabelFlush",
                /*"_comment": "If we have minus to positive, use this label flush",*/
                //"init": "if(data('series')[0].xAxis.minValue < 0 && data('series')[0].xAxis.maxValue > 0, " + json.xAxisLabelFlushPixels + ",   false  )"
                //"init": "true" // Old method deprecated to ensure labels are ALWAYS flush
                "init": "false" // Old method deprecated to ensure labels are ALWAYS flush
            },
            {
                "name": "numCategories",
                "update": "domain('factsByCategoryScale').length" //TODO
            },
            {
                "name": "numSeries",
                "update": "domain('factsByKeyScale').length"
            },
            {
                "name": "heightRaw",
                "update": "height"
            },
            {
                "name": "chartPadding",
                "init": config.padding
            },
            {
                "name": "heightPerBar",
                "update": json.barMaxSize
            },
            {
                "name": "heightPerCategory",
                "update": "heightPerBar * numSeries"
            },
            {
                "name": "heightForAxis",
                "update": Math.round(config.axisLabelPadding + (config.axisSize) + config.axisLabelPadding)
            },
            {
                "name": "heightNeeded",
                /*"_comment": "In theory, this should return the space needed for the maximum bar height. This emulates the way the vega scales use the inner/outer padding",*/
                "update": "round( heightPerCategory * (numCategories)* (1.0 + " + json.bandPaddingPercent + " + " + json.bandPaddingPercent +") + heightForAxis )"
            },
            {
                "name": "heightAdjusted",
                /*"_comment": "If our heightNeeded is less than the height, we use that. Otherwise we set the max height of the drawing area...",*/
                "update": (
                    json.fillChartSpace == true ?
                        "heightRaw" // fillChartSpace == true, the heightAdjusted is just the total area
                        :
                        "(heightNeeded+heightForAxis > heightRaw) ? heightRaw : heightNeeded-heightForAxis" // fillChartSpace == false
                )
            },
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme }
            },
            {
                "name": "colorText",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": "text-on-" +json.theme }
            },
            {
                "name": "colorByShade",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": json.theme }
            },
            {
                "name": "colorByShadeText",
                "type": "ordinal",
                "domain": { "data": "colorShades", "field": "data" },
                "range": { "scheme": "text-on-" + json.theme }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisFormat",
                "type": "ordinal",
                "domain": ["xAxisFormat"],
                "range": { "data": "factsResolved", "field": "format" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisFormatType",
                "type": "ordinal",
                "domain": ["xAxisFormatType"],
                "range": { "data": "factsResolved", "field": "formatType" }
            },
            {
                "name": "factsByCategoryScale",
                "type": "band",
                "range": { "signal": "[0,height]" },
                "domain": { "data": "factsResolved", "field": "categoryResolved" }
            },
            {
                "name": "factsByKeyScale",
                "type": "band",
                "range": { "signal": "[0,height]" },
                "domain": { "data": "factsResolved", "field": "key" }
            },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "serieId" },
              "range": { "data": "factsResolved", "field": "colorResolved" },
          },
            {
                // Provide the proper label for the legend
                "name": "legendLabel",
                "type": "ordinal",
                "domain": { "data": "factsResolved", "field": "serieId" },
                "range": { "data": "factsResolved", "field": "titleResolved" }
            },
        ],
        "marks": [
            /*{
                "type": "group",
                //"_comment": "DEBUG GROUP FOR HEIGHT ADJUSTED",
                "encode": {
                    "update": {
                        "opacity": { "signal": "0.4" },
                        "width": { "signal": "width/3" },
                        "height": { "signal": "heightAdjusted" },
                        "fill": { "signal": "'red'" }
                    }
                }
            },
            {
                "type": "group",
                //"_comment": "DEBUG GROUP FOR HEIGHT ADJUSTED",
                "encode": {
                    "update": {
                        "opacity": { "signal": "0.4" },
                        "width": { "signal": "width/2" },
                        "height": { "signal": "heightRaw" },
                        "fill": { "signal": "'yellow'" }
                    }
                }
            },*/
            // Begin wrapper group (only used for signals - we need a signal namespace that has the legend already subtracting the drawing area) (except legend)
            {
                "type": "group",
                /*"_comment": "DEBUG GROUP FOR WRAPPER",*/
                "signals": [
                    
                ],
                "encode": {
                    "update": {
                        "width": { "signal": "width" },
                        "height": { "signal": "height" },
                        // DEBUGGING:
                        //"fill": { "signal": "'yellow'" }
                    }
                },
                "marks": [
                    /*{
                        "type": "group",
                        "encode": {
                            "update": {
                                "opacity": { "signal": "0.4" },
                                "width": { "signal": "width/5" },
                                "height": { "signal": "height" },
                                "fill": { "signal": "'blue'" }
                            }
                        }
                    },*/
                    // Begin sizer group (sizes all master groups) (except legend)
                    {
                        "type": "group",
                        /*"_comment": "SIZER GROUP - this is used to clip the height of the contained groups, allowing the legend to appear at the bottom but the axis' snapped to this group",*/
                        "encode": {
                            "update": {
                                "width": { "signal": "width" },
                                "height": { "signal": "heightAdjusted > heightRaw ? heightRaw : heightAdjusted" },
                                // DEBUGGING:
                                //"fill": { "signal": "heightNeeded > height ? 'green' : null" },
                                //"tooltip": { "signal": "'heightNeeded: '+heightNeeded + ', heightAdjusted: '+heightAdjusted + ',heightPerBar:'+heightPerBar + ',heightPerCategory:'+heightPerCategory + ',height:'+height  + ',numCategories:'+numCategories  + ',numSeries:'+numSeries" }
                            }
                        },
                        "signals": [
                            {
                                /*"_comment": "This signal re-limits the height size (again) because the axis beneath will cause a reflow/recalculation in fillChartSpace == false mode (growing). If fillChartSpace is true, then the height is ignored (via a signal name rename)...",*/
                                "name": json.fillChartSpace == false ? "height" : "height_unused",
                                "update": "heightAdjusted > height ? height : heightAdjusted" // make sure we dont go bigger than height
                            },
                            {
                                /*"_comment": "This signal limits the bar size by automatically limiting the total space allowed",*/
                                "name": "categoryHeight",
                                "update": "(bandwidth('yScale') / numSeries) > " + json.barMaxSize + " ? numSeries*" + json.barMaxSize + " : bandwidth('yScale')"
                            },
                        ],
                        "scales": [
                            {
                                /*"_comment": "This is the main y scale bandwidth for groups of bars with padding",*/
                                "name": "yScale",
                                "type": "band",
                                "domain": {
                                    "data": "factsResolved",
                                    "field": "categoryResolved"
                                },
                                "range": "height",
                                "round": true,
                                "paddingInner": json.bandPaddingPercent,
                                "paddingOuter": json.bandPaddingPercent / 2
                            },
                            {
                                /*"_comment": "This is the main y scale bandwidth for groups of bars WITHOUT padding",*/
                                "name": "yScaleNoPadding",
                                "type": "band",
                                "domain": {
                                    "data": "factsResolved",
                                    "field": "categoryResolved"
                                },
                                "range": "height",
                                "padding": 0
                            },
                        ],
                        "marks": [
                            /*{
                                "type": "group",
                                "_comment": "DEBUG GROUP FOR HEIGHT WITHIN DRAWING AREA",
                                "encode": {
                                    "update": {
                                        "opacity": { "signal": "0.4" },
                                        "width": { "signal": "width/4" },
                                        "height": { "signal": "height" },
                                        "fill": { "signal": "'green'" },
                                        "tooltip": { "signal": "'height:' + height + ', heightRaw: ' + heightRaw + ', heightAdjusted: ' + heightAdjusted + ', heightNeeded: ' + heightNeeded" }
                                    }
                                }
                            },*/
                            // Begin master groups (LEFT, RIGHT, OVERLAY)
                            {
                                "type": "group",
                                /*"_comment": "This is the OVERLAY horizontal rules group",*/
                                "encode": {
                                    "enter": {
                                        //"fill": { "value": "yellow" },
                                    },
                                    "update": {
                                        "x": { "value": 0, },
                                        "width": { "signal": "width" },
                                        "height": { "signal": "height" }
                                    }
                                },
                                "scales": [
                                    {
                                        "name": "xScaleOverlay",
                                        "type": "linear",
                                        "round": true,
                                        "domain": [0, 1],
                                        "range": [0, { "signal": "width" }],
                                    }
                                ],
                                "axes": [
                                    Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                                        /*"_comment": "This is an fake axis with the same formatting as the right side axis to make sure we have exactly the same dimensions",*/
                                        "orient": "bottom",
                                        "scale": "xScaleOverlay",
                                        "values": ['INVALID_VALUE'],
                                        "labelFontWeight": json.xAxisLabelFontWeight,
                                        "labelOpacity": 0,
                                        "domain": false,
                                    })
                                ],
                                "marks": [
                                    {
                                        "name": "yzero",
                                        /*"_comment": "bottom rule that spans from left to right",*/
                                        "zindex": 2,
                                        "type": "rule",
                                        "encode": {
                                            "enter": {
                                                "stroke": { "value": config.domainLineColor },
                                                "strokeWidth": { "value": config.domainLineSize }
                                            },
                                            "update": {
                                                "x": { "value": 0 },
                                                "x2": { "signal": "width" },
                                                "y": { "signal": "round(height)" + (config.domainLineSize == 1 ? "-0.5" : "") } // -0.5 odd vega bug?
                                            }
                                        }
                                    },
                                    {
                                        "type": "group",
                                        "from": {
                                            "facet": {
                                                "data": "factsCategories",
                                                "name": "facetGroups",
                                                "groupby": "groupResolved"
                                            }
                                        },
                                        "encode": {
                                            "update": {
                                                "y": {
                                                    "scale": "yScale",
                                                    "field": "groupResolved"
                                                }
                                            }
                                        },
                                        "signals": [
                                            {
                                                "name": "height",
                                                "update": "bandwidth('yScale')"
                                            }
                                        ],
                                        "scales": [
                                            {
                                                "name": "groupPos",
                                                "type": "band",
                                                "range": "height",
                                                "round": true,
                                                "domain": {
                                                    "data": "facetGroups",
                                                    "field": "groupResolved"
                                                }
                                            }
                                        ],
                                        "marks": [
                                            {

                                                "name": "xzero",
                                                "from": {
                                                    "data": "facetGroups"
                                                },
                                                "zindex": 2,
                                                "type": "rule",
                                                "encode": {
                                                    "enter": {
                                                        "stroke": {
                                                            "value": config.gridColor
                                                        },
                                                    },
                                                    "update": {

                                                        "opacity": {
                                                            "signal": "scale('yScaleNoPadding',datum.categoryResolved) == 0 ? 0 : 1"
                                                        },
                                                        //"tooltip": {
                                                        //    "signal": "scale('yScaleNoPadding',datum.categoryResolved)"
                                                        //},
                                                        "x": {
                                                            "value": 0
                                                        },
                                                        "x2": {
                                                            "signal": "width"
                                                        },
                                                        "y": {
                                                            //"signal": "height + bandwidth('yScale')*0.1"
                                                            //"signal": "floor(0 - bandwidth('yScale')*" + (json.bandPaddingPercent / 2) + ")"
                                                            //"signal": "-ceil(bandwidth('yScale'))*0.3"
                                                            "signal": ROUND + "( -bandwidth('yScaleNoPadding')/2 * " + json.bandPaddingPercent + " ) + 0.5" // +0.5 odd vega bug?
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "type": "group",
                                /*"_comment": "This is the LEFT side labels group",*/
                                "encode": {
                                    "enter": {
                                        //"fill": { "value": "blue" },
                                    },
                                    "update": {
                                        "x": { "value": 0, },
                                        //"x2": { "signal": "width", },
                                        "width": { "signal": ROUND + "( width*" + json.labelRatio + " )" },
                                        "height": { "signal": "height" }
                                    }
                                },
                                "signals": [
                                    {
                                        "name": "widthAdjusted",
                                        "update": ROUND + "( width*" + json.labelRatio + " )"
                                    }
                                ],
                                "scales": [
                                    {
                                        "name": "xScaleLeft",
                                        "type": "linear",
                                        "round": true,
                                        "domain": [0, 1],
                                        "range": [0, { "signal": "width*" + json.labelRatio }],
                                    }
                                ],
                                "axes": [
                                    Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                                        /*"_comment": "This is an fake axis with the same formatting as the right side axis to make sure we have exactly the same dimensions",*/
                                        "orient": "bottom",
                                        "scale": "xScaleLeft",
                                        "values": ['INVALID_VALUE'],
                                        "labelFontWeight": json.xAxisLabelFontWeight,
                                        "labelOpacity": 0,
                                        "domain": false,
                                    })
                                ],
                                "marks": [
                                    {
                                        "type": "group",
                                        "from": {
                                            "facet": {
                                                "data": "factsCategories",
                                                "name": "facetGroups",
                                                "groupby": "groupResolved"
                                            }
                                        },
                                        "encode": {
                                            "update": {
                                                "y": {
                                                    "signal": ROUND + "( scale('yScale',datum.groupResolved) )",
                                                    //"scale": "yScale",
                                                    //"field": "groupResolved"
                                                }
                                            }
                                        },
                                        "signals": [
                                            {
                                                "name": "height",
                                                "update": ROUND + "( bandwidth('yScale') )"
                                            }
                                        ],
                                        "scales": [
                                            {
                                                "name": "groupPos",
                                                "type": "band",
                                                "range": "height",
                                                "domain": {
                                                    "data": "facetGroups",
                                                    "field": "groupResolved"
                                                }
                                            }
                                        ],
                                        "marks": [
                                            {
                                                "name": "labels",
                                                "from": {
                                                    "data": "facetGroups"
                                                },
                                                "type": "text",
                                                "encode": {
                                                    "enter": {
                                                        "baseline": {
                                                            "value": "middle"
                                                        },
                                                        "text": {
                                                            "signal": "datum.categoryResolved"
                                                        },
                                                        "ellipsis": { "value": config.textTruncationEllipsis }
                                                    },
                                                    "update": {
                                                        "y": {
                                                            "signal": ROUND + "( scale('groupPos',datum.groupResolved) + bandwidth('yScale')/2 )",
                                                            //"scale": "groupPos",
                                                            //"field": "groupResolved"
                                                        },
                                                        "height": {
                                                            "scale": "groupPos",
                                                            "band": 1
                                                        },
                                                        "x": {
                                                            "scale": "xScaleLeft",
                                                            "value": 0
                                                        },
                                                        "limit": {
                                                            "signal": "widthAdjusted - "+config.padding
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "type": "group",
                                /*"_comment": "This is the RIGHT side bars group",*/
                                "encode": {
                                    "enter": {
                                        //"fill": { "value": "red" },
                                    },
                                    "update": {
                                        "x": { "signal": ROUND + "( width*" + json.labelRatio + " )", },
                                        //"x2": { "signal": "width", },
                                        "width": { "signal": ROUND + "( width * (1-" + json.labelRatio + ") )" },
                                        "height": { "signal": "height" }
                                    }
                                },
                                "signals": [
                                    {
                                        "name": "widthAdjusted",
                                        "update": ROUND + "( width * (1-" + json.labelRatio + ") )"
                                    }
                                ],
                                "scales": [
                                    {
                                        "name": "xScaleRight",
                                        "type": "linear",
                                        "domain": {
                                            "data": "factsResolved",
                                            "field": "val"
                                        },
                                        "domainMin": {
                                            "signal": "data('series')[0].xAxis != null ? data('series')[0].xAxis.minValue : null",
                                        },
                                        "domainMax": {
                                            "signal": "data('series')[0].xAxis != null ? data('series')[0].xAxis.maxValue : null",
                                        },
                                        "padding": config.padding, // Expands the scale domain to accommodate the specified number of pixels on each of the scale range. The scale range must represent pixels for this parameter to function as intended. Padding adjustment is performed prior to all other adjustments, including the effects of the zero, nice, domainMin, and domainMax properties.
                                        "range": [0, { "signal": "width * (1-" + json.labelRatio+")" }],
                                        "round": true, // If true, rounds numeric output values to integers. Helpful for snapping to the pixel grid.
                                        "zero": true, // Boolean flag indicating if the scale domain should include zero. The default value is true for linear, sqrt and pow, and false otherwise.
                                        "nice": false // If true, there is a bug when the domain spans from minus to plus. Extends the domain so that it starts and ends on nice round values. This method typically modifies the scale’s domain, and may only extend the bounds to the nearest round value. Nicing is useful if the domain is computed from data and may be irregular. For example, for a domain of [0.201479…, 0.996679…], a nice domain might be [0.2, 1.0]. Domain values set via domainMin and domainMax (but not domainRaw) are subject to nicing. Using a number value for this parameter (representing a desired tick count) allows greater control over the step size used to extend the bounds, guaranteeing that the returned ticks will exactly cover the domain.
                                    }
                                ],
                                "axes": [
                                    Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                                        /*"_comment": "This is the actual axis with the values",*/
                                        "orient": "bottom",
                                        "scale": "xScaleRight",
                                        //"labelPadding": 0,
                                        //"labelOverlap": "parity", // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
                                        //"labelSeparation": json.xAxisLabelSeparation, // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled. ≥ 5.0
                                        //"labelFontWeight": json.xAxisLabelFontWeight,
                                        "labelFlush": { "signal": "xScaleRightLabelFlush" }, // Indicates if labels at the beginning or end of the axis should be aligned flush with the scale range. If a number, indicates a pixel distance threshold: labels with anchor coordinates within the threshold distance for an axis end-point will be flush-adjusted. If true, a default threshold of 1 pixel is used. Flush alignment for a horizontal axis will left-align labels near the beginning of the axis and right-align labels near the end. For vertical axes, bottom and top text baselines will be applied instead.
                                        "labelBound": false, //{ "signal": "xScaleRightLabelBound" }, // Indicates if labels should be hidden if they exceed the axis range. If false (the default) no bounds overlap analysis is performed. If true, labels will be hidden if they exceed the axis range by more than 1 pixel. If this property is a number, it specifies the pixel tolerance: the maximum amount by which a label bounding box may exceed the axis range.
                                        "values": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.values : null" }, // Indicates if labels at the beginning or end of the axis should be aligned flush with the scale range. If a number, indicates a pixel distance threshold: labels with anchor coordinates within the threshold distance for an axis end-point will be flush-adjusted. If true, a default threshold of 1 pixel is used. Flush alignment for a horizontal axis will left-align labels near the beginning of the axis and right-align labels near the end. For vertical axes, bottom and top text baselines will be applied instead.
                                        "domain": false,
                                        "format": { "signal": "scale('xAxisFormat','xAxisFormat')" },
                                        "formatType": { "signal": "scale('xAxisFormatType','xAxisFormatType')" }
                                    })
                                ],
                                "marks": [

                                    {
                                        "type": "group",
                                        "from": {
                                            "facet": {
                                                "data": "factsResolved",
                                                "name": "facetGroups",
                                                "groupby": "categoryResolved"
                                            }
                                        },
                                        "encode": {
                                            "update": {
                                                "y": {
                                                    "signal": ROUND + "( scale('yScale',datum.categoryResolved) )",
                                                    //"scale": "yScale",
                                                    //"field": "categoryResolved"
                                                }
                                            }
                                        },
                                        "signals": [
                                            {
                                                "name": "height",
                                                "update": "categoryHeight"
                                            },
                                            {
                                                "name": "fullHeight",
                                                "update": "bandwidth('yScale')"
                                            },
                                            {
                                                "name": "barMinValueWidth",
                                                "update": json.barMinValueWidth,
                                            },
                                        ],
                                        "scales": [
                                            {
                                                "name": "seriesPos",
                                                "type": "band",
                                                "range": "height",
                                                "round": true,
                                                "domain": {
                                                    "data": "factsResolved",
                                                    "field": "key"
                                                }
                                            }
                                        ],
                                        "marks": [
                                            {
                                                "name": "bars",
                                                /*"_comment": "This displays the actual bar using the group fill, but can also contain children such as the fact label",*/
                                                "from": {
                                                    "data": "facetGroups"
                                                },
                                                "type": "group",
                                                "signals": [
                                                    
                                                ],
                                                "encode": {
                                                    "enter": {
                                                        "tooltip": {
                                                            "signal": "datum.valResolved"
                                                            //"signal": "'DEBUG: ' + xScaleRightLabelFlush",
                                                            //"signal": "'DEBUG: ' + scale('seriesPos',datum.id) + '/' + bandwidth('seriesPos') + '/'+ datum.id + '/'+ datum.valResolved",
                                                            //"signal": "'DEBUG: '+(scale('xScaleRight',datum.val)-scale('xScaleRight',0))"
                                                            //"signal": "'DEBUG: '+scale('xAxisFormatType','xAxisFormatType')"
                                                        },
                                                        "fill": {
                                                            "signal": "datum.colorResolved"
                                                        }
                                                    },
                                                    "update": {
                                                        "y": {
                                                            "signal": ROUND + "( scale('seriesPos',datum.id) + (fullHeight-height)/2 )"
                                                        },
                                                        "height": {
                                                            "signal": ROUND + "( bandwidth('seriesPos') )"
                                                            //"scale": "seriesPos",
                                                            //"band": 1
                                                        },
                                                        "x": {
                                                            "signal": ROUND + "( scale('xScaleRight',0) )"
                                                            //"scale": "xScaleRight",
                                                            //"value": 0
                                                        },
                                                        "width": [
                                                            {
                                                                "test": "datum.val < 0 && (scale('xScaleRight',datum.val)-scale('xScaleRight',0)) > -barMinValueWidth",
                                                                "value": -json.barMinValueWidth
                                                            },
                                                            {
                                                                "test": "datum.val > 0 && (scale('xScaleRight',datum.val)-scale('xScaleRight',0)) < +barMinValueWidth",
                                                                "value": json.barMinValueWidth
                                                            },
                                                            {
                                                                "signal": ROUND + "( (scale('xScaleRight',datum.val)-scale('xScaleRight',0)) )"
                                                            }
                                                        ]
                                                    }
                                                },
                                                "marks": [
                                                    {
                                                        "type": "text",
                                                        "encode": {
                                                            
                                                            "enter": {
                                                                "tooltip": {
                                                                    "signal": "parent.valResolved"
                                                                },
                                                                "baseline": {
                                                                    "value": "middle"
                                                                },
                                                                "text": {
                                                                    "signal": "parent.labelResolved"
                                                                },
                                                                "fill": {
                                                                    "signal": "parent.textColorResolved"
                                                                },
                                                                "align": {
                                                                    "signal": "parent.val < 0 ? 'right' : 'left'"
                                                                },
                                                            },
                                                            "update": {
                                                                "x": {
                                                                    "signal": "parent.val < 0 ? (-chartPadding/2) : (chartPadding/2)"
                                                                },
                                                                "y": {
                                                                    "signal": "( bandwidth('seriesPos')/2 )"
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        "name": "xzero",
                                        "zindex": 2,
                                        "type": "rule",
                                        "encode": {
                                            "enter": {
                                                "stroke": {
                                                    "value": config.domainLineColor
                                                },
                                                "strokeWidth": {
                                                    "value": config.domainLineSize
                                                }
                                            },
                                            "update": {
                                                "x": {
                                                    "signal": "scale('xScaleRight',0)" + (config.domainLineSize == 1 ? "-0.5" : "") // -0.5 odd vega bug?
                                                    //"scale": "xScaleRight",
                                                    //"value": 0
                                                },
                                                "y": {
                                                    "value": 0
                                                },
                                                "y2": {
                                                    "signal": "height"
                                                }
                                            }
                                        }
                                    }
                                ]
                            },
                        ]
                    },
                ]
            }
        ]
    };
};
Machinata.Reporting.Node.HBarNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.series;
};
Machinata.Reporting.Node.HBarNode.init = function (instance, config, json, nodeElem) {

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries(instance, config, json, json.series);

    // Use our own axis snapping tool?
    if (json.autoSnapXAxis == true) {
        Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis(
            instance, 
            config,
            json,
            json.series,
            {
                axisKey: "xAxis",
                snapToZero: json.autoSnapXAxisSnapToZero,
                niceDomain: json.autoSnapXAxisNiceDomain,
                extendToDomain: json.autoSnapXAxisExtendToDomain,
                margin: json.autoSnapXAxisMargin,
                balance: json.autoSnapXAxisBalance,
                tickCount: json.autoSnapXAxisTickCount,
            }
        );
    }


    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.HBarNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.HBarNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







