


/// <summary>
/// Provides infrastructure for loading different configuration profiles. Each profile can override the 
/// default configuration settings. Furthermore, the profile can be used to drive specific UI interactions.
/// When a profile is set, the report main DOM element and the body DOM element will have ```profile-xzy``` class
/// registered to it.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Profiles = {};

/// <summary>
/// Standardized ```web``` profile used for regular display on a website.
/// </summary>
/// <type>constant</type>
Machinata.Reporting.Profiles.PROFILE_WEB_KEY = "web";

/// <summary>
/// Standardized ```webprint``` profile used for printing a report within the website.
/// </summary>
/// <type>constant</type>
Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY = "webprint";

/// <summary>
/// Standardized ```print``` profile used for generating print-documents.
/// </summary>
/// <type>constant</type>
Machinata.Reporting.Profiles.PROFILE_PRINT_KEY = "print";

/// <summary>
/// Standardized ```fullscreen``` profile used for viewing a report in fullscreen.
/// </summary>
/// <type>constant</type>
Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY = "fullscreen";

/// <summary>
/// A dictionary of profile configurations. 
/// If registered, the profile configuration will override the default configuration.
/// </summary>
/// <type>variable</type>
Machinata.Reporting.Profiles.Configs = {};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEB_KEY] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEB_KEY]["nodeDefaults"] = {};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY]["nodeDefaults"] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY]["nodeDefaults"]["ToggleNode"] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY]["nodeDefaults"]["ToggleNode"]["explodeChildren"] = true;


/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["graphicAlignment"] = "top";
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["titleSize"] = 20;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["titleLineHeight"] = 1.15;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["labelSize"] = 10;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["valueSize"] = 20;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["valueLargeSize"] = 20;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["symbolSize"] = 18;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["strokeDash"] = "3,3";
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["bucketFactHeight"] = 54;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["bucketFactPaddingMultiplier"] = 2;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY]["nodeDefaults"]["PCPCashFlowGraphicNode"]["chartBorderMultiplier"] = 1.0;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY].medium = "paper";
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY].calibrationUnits = "mm";
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY].roundToNearestFullPixel = true;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY].layoutingDPI = 96;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_PRINT_KEY].mediumContentWidthSize = 680;


/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY] = {};
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["automaticallyFoldChaptersIntoPages"] = false;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["automaticallyAddPageTools"] = false;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["automaticallyInsertCatalogNavigation"] = false;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["automaticallyInsertChapterNavigation"] = false;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["automaticallyBuildCatalogUI"] = false;
Machinata.Reporting.Profiles.Configs[Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY]["nodeDefaults"] = {};




