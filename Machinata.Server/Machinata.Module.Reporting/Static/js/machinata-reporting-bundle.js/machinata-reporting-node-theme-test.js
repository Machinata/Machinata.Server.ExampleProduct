


/// <summary>
/// A very basic node to test themes. This node will generate a pallet for the theme including
/// a text overlay in the proper theme shade overlay color.
/// 
/// Note: This node should not be used in production.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.ThemeTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults = {};

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults.chrome = "solid";

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var shadeNamesToUse = config.themeColorShadeNames;
    if (json.theme == "gray") {
        shadeNamesToUse = config.themeGrayShadeNames;
    }
    return {
        "data": [
          {
              "name": "colorShades",
                "values": shadeNamesToUse
          },
        ],
        "scales": [
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "colorShades", "field": "data" },
              "range": { "scheme": json.theme }
          },
          {
              "name": "textColor",
              "type": "ordinal",
              "domain": { "data": "colorShades", "field": "data" },
              "range": { "scheme": "text-on-" + json.theme }
          },
          {
              "name": "xscale",
              "type": "band",
              "domain": { "data": "colorShades", "field": "data" },
              "range": "width",
              "padding": 0.0,
              "round": true
          }
        ],

        "marks": [
          {
              "type": "rect",
              "from": { "data": "colorShades" },
              "encode": {
                  "enter": {
                      "x": { "scale": "xscale", "field": "data" },
                      "width": { "scale": "xscale", "band": 1 },
                      "y": { "signal": "height" },
                      "y2": { "value": 0 }
                  },
                  "update": {
                      "fill": { "scale": "color", "field": "data" }
                  }
              }
          },
          {
              "type": "text",
              "from": { "data": "colorShades" },
              "encode": {
                  "enter": {
                      "x": { "signal": "scale('xscale',datum.data) + bandwidth('xscale')/2" },
                      "y": { "signal": "height/2" },
                      "align": { "value": "center" },
                      "baseline": { "value": "middle" },
                      "fontWeight": { "value": "bold" },
                      "fontSize": { "value": 14 },
                  },
                  "update": {
                      "fill": { "scale": "textColor", "field": "data" },
                      "text": { "signal": "['"+json.theme+"' + ' ' + datum.data , scale('color',datum.data)]" }
                  }
              }
          }
        ]
    };
};
Machinata.Reporting.Node.ThemeTestNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};
Machinata.Reporting.Node.ThemeTestNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ThemeTestNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ThemeTestNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







