


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.DonutNode</inherits>
Machinata.Reporting.Node.PieNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PieNode.defaults = {};

/// <summary>
/// Donuts and pies have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.insertLegend = true;

/// <summary>
/// Defines the inner radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.innerRadius = 0.0;

/// <summary>
/// Defines the outer radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.outerRadius = 1.0;

/// <summary>
/// If ```true```, displays labels inside the slices with a slide angle larger than ```sliceLablesMinimumDegrees```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabels = true;

/// <summary>
/// If ```true```, includes the slice value in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsShowValue = true;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsShowCategory = false;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.legendLabelsShowValue = false;


/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the minimum slice angle required for the label to display.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsMinimumDegrees = 36;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the offset from the outer edge in pixels.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsOffsetPixels = 4;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the font size in pixels for the labels.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsSize = Machinata.Reporting.Config.chartTextSize;

/// <summary>
/// If ```true```, sorts the facts automatically.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sortFacts = false;

/// <summary>
/// If enabled, if the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%),
/// we use the facts full formatting (```fact.format```), otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// For this setting to work, the fact format specified (```fact.format```) must be of type percent (ie '.2%').
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.automaticallyFormatEdgeNumbers = true;


Machinata.Reporting.Node.PieNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["DonutNode"].getVegaSpec(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].applyVegaData(instance, config, json, nodeElem, spec);
};
Machinata.Reporting.Node.PieNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







