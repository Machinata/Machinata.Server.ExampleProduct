/// <summary>
/// This abstract Node type provides the infrastructure needed for displaying
/// and managing Vega-based chart Nodes.
/// This Node implementation also provides some useful helper functions, such as
/// createVegaLegend(), for doing mundane Vega tasks. Through the applyGlobalVegaSpec(),
/// implementing Nodes only need to provide the Node-specific Vega specs - common Vega
/// spec stuff such as config and styles are provided by VegaNode automatically.
/// Nodes inheriting the VegaNode should call the parent methods before executing their own.
///
/// ## Chart Data
/// Data for the vega charts are (for obvious reasons) not integrated into the specs.
/// The data is automatically bound in using the ```Machinata.Reporting.Node.VegaNode.applyVegaData``` interface.
/// Here the data from the node JSON is bound into the spec when needed.
///
/// ### JSON Date/Times
/// Date/times should always be provided in Unix Epoch Milliseconds long format.
///
/// ### Themes
/// All Vega-Nodes must have a valid theme registered for it.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VegaNode = {};

/// <summary>
/// Supported defaults for VegaNode Node types.
/// Note that due to the nature of JavaScript these are actually not automatically
/// inherited to implementing Nodes - you must redefine them.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults = {};

/// <summary>
/// If true, a legend will automatically be inserted via createVegaLegend().
/// To use the automatic legend, you must implement in the vega spec the 
/// following scales which use the mark group key to resolve:
///     legendLabel
///     legendColor
/// Optionally, the vega spec data set 'legendSelected' can be provided to
/// create a legend that is interactive (toggable).
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.insertLegend = false;

/// <summary>
/// For charts that support legends, this defines the number of columns.
/// Implementing chart types will define their own default settings.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendColumns = 1;

/// <summary>
/// For charts that support legends, this property optionally defines the 
/// minimum number of rows (in space) the legend should occupy.
/// This is especially useful for aligning the size of the chart area across multiple charts if
/// the legend data varies (here it is recommended to find the common ```legendColumns``` and ```legendMinRows```
/// that suits all the charts).
/// By default ```null``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendMinRows = null;

/// <summary>
/// For charts that support legends, this property defines the position of the chart legend.
/// Supported positions:
///  - ```left-top```: Place the legend to the top-left of the chart, in the top corner.
///  - ```right-top```: Place the legend to the right of the chart, in the top corner.
///  - ```top-left```: Place the legend above the top of the chart, on the left side.
///  - ```bottom-left```: Place the legend below the bottom of the chart, on the left side.
/// By default ```bottom``.
/// If the chart node does not define this setting, then the global default ```Machinata.Reporting.Config.legendPosition``` is used.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPosition = "bottom";

/// <summary>
/// For render modes that have fixed chart dimensions (such as print), the legend position
/// can differ depending on the orientation using the ```legendPositionPortrait``` 
/// and ```legendPositionLandscape``` properties.
/// See ```legendPosition``` for supported positions.
/// By default ```null``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPositionPortrait = null;

/// <summary>
/// For render modes that have fixed chart dimensions (such as print), the legend position
/// can differ depending on the orientation using the ```legendPositionPortrait``` 
/// and ```legendPositionLandscape``` properties.
/// See ```legendPosition``` for supported positions.
/// By default ```null``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPositionLandscape = null;

/// <summary>
/// For charts that support legends, if ```legendSiblingsAutoBalance``` is not explicitly ```false``` then
/// chart legends are automatically balanced with regards to their siblings (ie charts next to 
/// the current chart).
/// When using this feature, you do not have to provide ```legendColumns``` or ```legendMinRows```,
/// as this feature will automatically set these settings. You can optionally set ```legendColumns``` to
/// force a specific look.
/// Currently this only works if the following properties are defined as well:
///  - ```legendSiblingsExpectedMinItems```
///  - ```legendSiblingsExpectedMaxItems```
/// By default ```true``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsAutoBalance = true;

/// <summary>
/// For charts that support legends and are auto-balanced, this property
/// hints to how the legend should be sized. 
/// This property must equal the minimum number of legend items across all
/// neighboring siblings (including itself).
/// By default ```null``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsExpectedMinItems = null;

/// <summary>
/// For charts that support legends and are auto-balanced, this property
/// hints to how the legend should be sized. 
/// This property must equal the maximum number of legend items across all
/// neighboring siblings (including itself).
/// By default ```null``.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsExpectedMaxItems = null;

/// <summary>
/// Applies the common vega spec for all VegaNode implementations.
/// You do not need to call this from an implementing node, this is done automatically by the init.
/// </summary>
Machinata.Reporting.Node.VegaNode.applyGlobalVegaSpec = function (instance, config, json, nodeElem, spec) {
    // Init
    var self = this;
    // General
    spec["$schema"] = "https://vega.github.io/schema/vega/v5.json";
    spec["padding"] = config.chartPadding; // fixes some small inaccuraces in font measuring, especially in nodejs environment
    spec["autosize"] = {
        "type": "fit"
    };
    // Signals
    if (spec["signals"] == null) spec["signals"] = [];
    spec["signals"].push({
        "name": "widthInPercentOfMedium",
        "update": "width / " + config.mediumContentWidthSize
    });
    spec["signals"].push({
        "name": "isLayoutedOnHalfMediumWidth",
        "update": "widthInPercentOfMedium < 0.6"
    });
    // Default configs
    spec["config"] = {
        "mark": {
            "font": config.textFont,
            "fontSize": config.chartTextSize
        },
        "text": {
            "font": config.textFont,
            "fontSize": config.chartTextSize
        },
        "style": {
            "text": {
                "font": config.textFont,
                //"fontWeight": "bold",
                "fontSize": config.chartTextSize
            },
            "subtitle": {
                "font": config.subtitleFont,
                //"fontWeight": "bold",
                "fontSize": config.subtitleSize
            },
            "label": {
                "font": config.labelFont,
                //"fontWeight": "bold",
                "fontSize": config.labelSize
            },
            "number": {
                "font": config.numberFont
            }
        },
        "axis": {
            "ticks": false,
            "tickWidth": config.gridLineSize,
            "offset": 0,
            "gridOpacity": 1.0,
            "gridColor": config.gridColor,
            "gridWidth": config.gridLineSize,
            "labelLimit": json.axisLabelLimit || config.axisLabelLimit, // The maximum allowed length in pixels of axis tick labels.
            "labelSeparation": json.axisLabelSeparation || config.axisLabelSeparation, // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled. ≥ 5.0
            "labelOverlap": json.axisLabelOverlap || config.axisLabelOverlap, // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
            "labelPadding": json.axisLabelPadding || config.axisLabelPadding, // The padding in pixels between labels and ticks.
            "labelFlushOffset": 0, // Indicates the number of pixels by which to offset flush-adjusted labels (default 0). For example, a value of 2 will push flush-adjusted labels 2 pixels outward from the center of the axis. Offsets can help the labels better visually group with corresponding axis ticks.
            "labelFont": config.axisFont,
            "labelFontSize": config.axisSize,
            "labelFontWeight": config.axisLabelFontWeight,
            "titleFont": config.textFont,
            "titleFontSize": config.chartTextSize,
            "titleFontWeight": "normal"
        },
        "legend": {
            "labelFont": config.legendFont,
            "labelFontSize": config.legendSize,
            "labelColor": (json.chrome == "dark" ? "white" : null),
            "padding": 0,
            "layout": {
                //"bounds": "full"
            }
        }
    };
    return spec;
};


/// <summary>
/// Applies the common vega legend spec for VegaNode implementations that use the automatic legend.
/// You do not need to call this from an implementing node, this is done automatically by the init.
/// </summary>
Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec = function (instance, config, json, nodeElem, spec) {
    var self = this;
    var insertLegend = json.insertLegend;

    // All legend data "empty", or no legend data at all? 
    // If so, we turn off the legend
    if (json.insertLegend == true && json.legendData == null) {
        insertLegend = false; // no legend data
    } if (json.insertLegend == true && json.legendData != null) {
        var allEmpty = true;
        for (var i = 0; i < json.legendData.length; i++) {
            if (json.legendData[i].title != null && json.legendData[i].title != "") {
                allEmpty = false;
                break;
            }
        }
        if (allEmpty == true) {
            insertLegend = false;
        }
    }

    if (insertLegend == true) {

        Machinata.debug("Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec");
        Machinata.debug("  " + "node id:" + json.id);

        json.legendIsInteractive = false; // This feature is deprecated

        // Auto orient?
        //TODO @dan

        

        var actualLegendItemCount = json.legendData.length;

        // Auto balance?
        if (json.legendSiblingsAutoBalance != false && json.legendSiblingsExpectedMaxItems != null && json.legendMinRows == null) {
            // Set columns, if not explicitly set.
            if (json.legendColumns == null) {
                json.legendColumns = 1; // fallback
                if (json.legendSiblingsExpectedMaxItems > 2) json.legendColumns = 2;
                if (json.legendSiblingsExpectedMaxItems > 8) json.legendColumns = 3;
            }
            // Set rows
            json.legendMinRows = Math.ceil(json.legendSiblingsExpectedMaxItems / json.legendColumns);
        }


        // Apply min rows
        // Here we add fake data to ensure that we reach the minimum number of rows
        //TODO: incorporate columns
        if (json.legendMinRows != null) {
            var columns = (json.legendColumns || 1);
            while (json.legendData.length < json.legendMinRows * columns) {
                Machinata.debug("  " + "Adding blank legend item for min rows: " + json.legendMinRows);
                json.legendData.push({
                    key: "BLANK_" + json.legendData.length,
                    symbol: "blank"
                });
            }
        }

        // Clip columns down if not needed
        if (json.legendMinRows == null) {
            if (json.legendColumns > 1 && actualLegendItemCount == 1) {
                json.legendColumns = 1;
            }
        }

        // Compile an array of all legend keys (we need this for our custom scales)
        var legendKeys = [];
        for (var i = 0; i < json.legendData.length; i++) {
            // Apply default type
            if (json.legendData[i].symbol == null) json.legendData[i].symbol = "square";
            // Register the key
            legendKeys.push(json.legendData[i].key);
        }

        // Legend data definitions
        spec["data"].push({
            "name": "legendData",
            "values": json.legendData
        });
        spec["data"].push({
            "name": "legendSelected",
            "on": []
        });

        // Legend scale definitions
        spec["scales"].push({
            "name": "legendDataScale",
            "type": "ordinal",
            "domain": legendKeys,
            "range": legendKeys
        });

        // Legend position
        var legendWidthAllottedSpace = 1.0;
        var legendHeightAllottedSpace = 1.0;
        var legendForceColumns = null;
        var legendForceRows = null;
        function getLegendOrient(key) {
            // First, test for the key (ie legendPositionPortrait)
            var legendOrient = Machinata.Reporting.Config[key]; // fallback
            if (json[key] != null) legendOrient = json[key];
            else if (config[key] != null) legendOrient = config[key];
            // If nothing, fallback to legendPosition
            if (legendOrient == null) {
                legendOrient = Machinata.Reporting.Config.legendPosition; // fallback
                if (json.legendPosition != null) legendOrient = json.legendPosition;
                else if (config.legendPosition != null) legendOrient = config.legendPosition;
            }
            // Convert to vega position
            if (legendOrient == "left-top") {
                legendOrient = "left";
                legendForceColumns = 1;
                legendWidthAllottedSpace = 0.35;
            } else if (legendOrient == "right-top") {
                legendOrient = "right";
                legendForceColumns = 1;
                legendWidthAllottedSpace = 0.35;
            } else if (legendOrient == "top-left") {
                legendOrient = "top";
            } else if (legendOrient == "bottom-left") {
                legendOrient = "bottom";
            } else {
                legendOrient = "bottom";
            }
            return legendOrient;
        }
        var legendOrient = getLegendOrient("legendPosition");
        if (json.width != null && json.height != null) {
            if ((json.width / json.height) > 1.0) legendOrient = getLegendOrient("legendPositionLandscape");
            else legendOrient = getLegendOrient("legendPositionPortrait");
        }
        if (legendForceColumns != null) json.legendColumns = legendForceColumns;
        

        // Label max width
        var labelMaxWidthSignal = "(" + config.legendLabelMaxWidth + ")";
        if (true) {
            // Automatically clip by reverse engineering the vega spacing used for the legend
            // We provide this as a signal, since on web the charts are responsive...
            var columnsSafe = 1;
            if (json.legendColumns != null) columnsSafe = json.legendColumns; // legendColumns is not always defined
            var legendSymbolSpace = (columnsSafe) * (config.legendSymbolSize + config.legendSymbolStroke + + config.legendSymbolStroke);
            var legendColumnPaddingSpace = (columnsSafe - 1) * (config.legendColumnPadding);
            // Javascript calculation
            
            if (json.width != null && json.height != null) {
                var labelMaxWidthPrecomputed = Math.min(config.legendLabelMaxWidth, Math.floor((((json.width * legendWidthAllottedSpace) - legendSymbolSpace - legendColumnPaddingSpace) / json.legendColumns) - (0.5 * config.legendSymbolSize)));
                //console.log(labelMaxWidthPrecomputed);
                labelMaxWidthSignal = "(" + labelMaxWidthPrecomputed + ")";
            } else {
                // Vega signal of same calculation
                labelMaxWidthSignal = "min( " + config.legendLabelMaxWidth + ", floor( ((width*" + legendWidthAllottedSpace + " - " + legendSymbolSpace + " - " + legendColumnPaddingSpace + ") / " + columnsSafe + ") - (0.5 * " + config.legendSymbolSize + ") ) )";
            }
        }

        // Create legend spec
        if (spec["legends"] == null) spec["legends"] = [];
        spec["legends"].push({
            "fill": "legendDataScale", //legendDataScale
            "direction": "vertical",
            "orient": legendOrient, // Note: bottom-left doesnt work, only suppported is bottom
            //"orient": { "signal": legendOrientSignal}, // Note: signals dont work properly... Note: bottom-left doesnt work, only suppported is bottom
            "gridAlign": "none",
            "symbolType": { "signal": " data('legendData')[datum.index].symbol == 'dot' ? 'circle' : (data('legendData')[datum.index].symbol == 'diamond' ? 'diamond' : 'square')   " },
            "symbolSize": (config.legendSymbolSize * config.legendSymbolSize), // The area in pixels of the symbols bounding box. Note that this value sets the area of the symbol; the side lengths will increase with the square root of this value.
            "symbolStrokeColor": null,
            "symbolStrokeWidth": config.legendSymbolStroke,
            "labelLimit": { "signal": labelMaxWidthSignal}, // precompiled signal
            "columns": json.legendColumns,
            "columnPadding": config.legendColumnPadding,
            "rowPadding": config.legendRowPadding,
            "padding": config.legendPadding,
            "clipHeight": config.legendSymbolSize + config.legendSymbolStroke, // Note: the stroke exceeds the symbol bounds since the vega stroke always pushes out from the boundary
            "encode": {
                "title": {
                    "update": {

                    }
                },
                "labels": {
                    "name": "legendLabel",
                    "interactive": true,
                    "update": {
                        "opacity": [
                          { "test": "!length(data('legendSelected')) || indata('legendSelected', 'value', datum.value)", "value": 1 },
                          { "value": 0.2 }
                        ],
                        "text": {
                            //"signal": "data('legendData')[datum.index].title" // use pre computed title
                            "signal": "scale('legendLabel',datum.value)" // use label scale with key
                            //"scale": "legendLabel", "field": "value" // use key (datum value)
                        }
                    }
                },
                "symbols": {
                    "name": "legendSymbol",
                    "interactive": true,
                    "update": {
                        "opacity": [
                          { "test": "!length(data('legendSelected')) || indata('legendSelected', 'value', datum.value)", "value": 1 },
                          { "value": 0.2 }
                        ],
                        "fill": [
                            {
                                "test": "data('legendData')[datum.index].symbol == 'line'",
                                "value": "transparent"
                            },
                            {
                                "scale": "legendColor",
                                "field": "value"
                            },
                        ],
                        "stroke": [
                            {
                                "scale": "legendColor",
                                "field": "value"
                            },
                        ],
                    }
                },
                "legend": {
                }
            }
        });

    } else {

        // Some charts rely on the legend datas to exist...
        spec["data"].push({
            "name": "legendSelected",
            "on": []
        });
    }
};

/// <summary>
/// Returns a JSON vega spec. The returned JSON should not have any data values, as these
/// are bound in by applyVegaData(). When defining a data source, use ```"values": null```.
/// Every VegaNode implemention must implement this method. 
/// </summary>
Machinata.Reporting.Node.VegaNode.getVegaSpec = function (instance, config, json, nodeElem) { };

/// <summary>
/// Applys the data from the node JSON into the vega spec (since the spec is 'dataless').
/// For example, if your node as a array property 'facts', you would implement
///     ```spec["data"][0].values = json.facts;```
/// Every VegaNode implemention must implement this method. 
///
/// TODO: DEPRECATE!!!
///
/// </summary>
Machinata.Reporting.Node.VegaNode.applyVegaData = function (instance, config, json, nodeElem, spec) { };

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.compileVegaSpec = function (instance, config, json, nodeElem) {
    // Get implementation
    var self = this;
    var impl = Machinata.Reporting.Node[json.nodeType];

    // Get spec and apply data
    var spec = impl.getVegaSpec(instance, config, json, nodeElem);
    impl.applyVegaData(instance, config, json, nodeElem, spec);

    // Apply global spec and parse
    self.applyGlobalVegaSpec(instance, config, json, nodeElem, spec);
    self.applyLegendVegaSpec(instance, config, json, nodeElem, spec);
    
    return spec;
};

/// <summary>
/// Sets up the node for containing a responsive Vega view, including parsing and compiling the spec,
/// and registering common functionality such as tooltips.
/// Every VegaNode implemention must call this method at the begginning of its own
/// init(). Typically, a VegaNode implementation does not need to do anything since
/// most is handled by the getVegaSpec() method.
/// </summary>
Machinata.Reporting.Node.VegaNode.init = function (instance, config, json, nodeElem) {

    // Basic validation
    if (config == null) throw "Machinata.Reporting.Node.VegaNode.init: config JSON cannot be null (parameter config)!";
    if (json == null) throw "Machinata.Reporting.Node.VegaNode.init: node JSON cannot be null (parameter json)!";
    if (json.theme == null) throw "Machinata.Reporting.Node.VegaNode.init: node theme cannot be null (json.theme)! Please make sure to set a theme.";

    // Validate theme
    if (json.theme != null && Machinata.Reporting.getTheme(json.theme) == null) {
        console.warn("The theme '" + json.theme + "' for node " + json.id + " is not valid.");
        json.theme = "default";
    }

    // Headless?
    if (config.headless == true && nodeElem == null) {
        //nodeElem = $("<div></div>").appendTo($("body"));
        return;
    }

    // Non-headless validation
    if (nodeElem == null) throw "Machinata.Reporting.Node.VegaNode.init: node Elem cannot be null (parameter nodeElem)!";

    // Tools
    Machinata.Reporting.Node.registerExportOptions(instance, config, json, nodeElem, {
        "svg": "SVG",
        "png": "PNG",
        //"jpeg": "JPG", // vega doesnt support this
    });

    // Get spec and apply data
    var spec = Machinata.Reporting.Node.VegaNode.compileVegaSpec(instance, config, json, nodeElem);
    var parsedVegaSpec = vega.parse(spec);
    //nodeElem.data("vega-parsed-specs", parsedVegaSpec);

    // Create container element
    nodeElem.addClass("type-VegaNode");
    var vegaContainer = $("<div class='node-bg vegaview'></div>");
    nodeElem.append(vegaContainer);

    // Create vega view
    var vegaView = new vega.View(parsedVegaSpec, {
        renderer: 'svg',
        container: vegaContainer[0],
        responsive: false, // has no effect
        hover: true,
    });
    if (Machinata.DebugEnabled == true) vegaView.logLevel(vega.Info);


    // Register custom tooltip handler
    // Our custom sanitize allows us to build tabular tooltips. We don't use the standard mechanism since
    // it is not possible to modify the keys in runtime (it must be a string)
    if (true) {
        var tooltipOptions = {
            theme: 'custom',
            sanitize: function (value) {
                return String(value)
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/\[colon\]/g, ':&nbsp;')
                    .replace(/\[title\]/g, '<div class="title">')
                    .replace(/\[\/title\]/g, '</div>')
                    .replace(/\[items\]/g, '<table>')
                    .replace(/\[\/items\]/g, '</table>')
                    .replace(/\[key\]/g, '<td class="key">')
                    .replace(/\[\/key\]/g, ':&nbsp;</td>')
                    .replace(/\[val\]/g, '<td class="value">')
                    .replace(/\[\/val\]/g, '</td>')
                    .replace(/\[item\]/g, '<tr>')
                    .replace(/\[\/item\]/g, '</tr>');
            }
        };
        var tooltipHandler = new vegaTooltip.Handler(tooltipOptions);
        vegaView.tooltip(tooltipHandler.call);
    }

    // Register with node
    nodeElem.data("vega-container", vegaContainer);
    nodeElem.data("vega-view", vegaView);
};



/// <summary>
/// Draws the vega view.
/// Every VegaNode implemention must call this method at the begginning of its own
/// draw().
/// </summary>
Machinata.Reporting.Node.VegaNode.draw = function (instance, config, json, nodeElem) {
    var VERBOSE_VEGA_DRAW_LOGGING = false;

    // Extract vega elements
    var vegaContainer = nodeElem.data("vega-container");
    if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "container size ==>",vegaContainer.width(), vegaContainer.height());
    // Update the view
    var vegaView = nodeElem.data("vega-view");
    if (nodeElem.data("vega-firstdraw") != false) {
        // First draw
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vega-firstdraw == true");
        nodeElem.data("vega-firstdraw", false);
        vegaView
            .width(vegaContainer.width())
            .height(vegaContainer.height())
            .run();
        
    } else {
        // Subsequent draws (resize)
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vega-firstdraw ==>", false, "vegaView.width ==> ", vegaView.width());
        vegaView
            .width(vegaContainer.width())
            .height(vegaContainer.height())
            .resize() // subsequent draws we call the vega view resize...
            .run();
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vegaView.width ==> ", vegaView.width(), "width signal ==>", vegaView.signal("width"));
    }

    
    
};

/// <summary>
/// Outputs a dataset to the console.
/// Note: Machinata.DebugEnabled must be enabled
/// </summary>
Machinata.Reporting.Node.VegaNode.debugOutputDataSet = function (nodeElem, dataset) {
    if (Machinata.DebugEnabled == true) {
        console.log(nodeElem.data("vega-view").data(dataset));
    }
};




/// <summary>
/// Helper method to export common chart formats.
/// </summary>
Machinata.Reporting.Node.VegaNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    var self = this;

    if (format == "svg" || format == "png") {
        var vegaView = nodeElem.data("vega-view");
        // generate a PNG snapshot and then download the image
        var promise = vegaView.toImageURL(format).then(function (url) {
            var link = document.createElement('a');
            link.setAttribute('href', url);
            link.setAttribute('target', '_blank');
            link.setAttribute('download', filename);
            link.dispatchEvent(new MouseEvent('click'));
        });
        // Note: catch() is not supported by YUI Javascript Compressor!
        //}).catch(function (err) {
        //    Machinata.messageDialog("Export", "There was an error exporting the chart.").show()
        //});


        return true;
    }
    return false;
};




