/// <summary>
/// Collection of tools specifically designed for operating the Reporting module
/// in a Containerless mode, where individual nodes are rendered very specifically to parts of
/// a page without the context of a report.
/// 
/// ## Containerless Example on ReactJS
/// ```
/// 
/// 
/// // Our node to test
/// var gaugeJSON = {
///     "nodeType": "GaugeNode",
///     "theme": "magenta",
///     "width": 800,
///     "height": 600,
///     "theme": "magenta",
///     "status": {
///         "value": 1.6,
///         "title": { "resolved": "Moderate" },
///         "tooltip": { "resolved": "1.6 / 5" }
///     },
///     "segments": [
///         {
///             "active": true,
///             "title": { "resolved": "Low" }
///         },
///         {
///             "active": true,
///             "title": { "resolved": "Moderate" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Medium" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Enhanced" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "High" }
///         }
///     ]
/// };
/// 
/// TODO
/// ```
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Containerless = {};


/// <summary>
/// If ```true``` is passed, verbose debug output is enabled.
/// </summary>
Machinata.Reporting.Containerless.enableVerboseOutput = function (enabled) {
    Machinata.DebugEnabled = enabled;
};


/// <summary>
/// Returns a merged (and loaded) configuration according to the specified ```profile```, with the 
/// config setting ```Containerless``` set to ```true```.
/// Any custom configuration must be bassed at this point to ensure that it loaded and applied properly.
/// </summary>
Machinata.Reporting.Containerless.getContainerlessConfigForProfile = function (profile, config) {
    // Init config, with containerless=true
    if (profile == null) profile = "web";
    if (config == null) {
        config = {};
    }
    config["containerless"] = true;

    // Create merged default version
    Machinata.debug("Machinata.Reporting.Containerless.getContainerlessConfigForProfile: "+profile);
    config = Machinata.Reporting.mergeAndLoadConfigWithDefaults(config, profile); // here we merge in all the configs, including the profile
    return config;
};

/// <summary>

/// </summary>
Machinata.Reporting.Containerless.buildNode = function (config, nodeJSON, containerElem) {
    // Sanity
    if (config == null) throw new "Machinata.Reporting.Containerless.buildNode: config is null, it must be initialized via Machinata.Reporting.Containerless.getContainerlessConfigForProfile().";
    if (nodeJSON == null) throw new "Machinata.Reporting.Containerless.buildNode: nodeJSON is null, it be a valid drawable node.";
    if (containerElem == null) throw new "Machinata.Reporting.Containerless.buildNode: containerElem is null, it be a valid jquery selector.";
    if (containerElem.length == 0) throw new "Machinata.Reporting.Containerless.buildNode: containerElem is an empty selector.";
    if (containerElem.parent().length == 0) throw new "Machinata.Reporting.Containerless.buildNode: containerElem appears to not be in the DOM (it has no parent node).";

    // Register the reporting css namespace to container
    containerElem.addClass("machinata-reporting-styling");

    // Create an instance to work with
    var instance = Machinata.Reporting._createEmptyInstance();
    instance.config = config;

    // Build the node and contents
    var nodeElem = Machinata.Reporting.buildContents(instance, config, nodeJSON, null, containerElem);
    nodeElem.addClass("container-fullsize");

    // Return the built node
    return nodeElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Containerless.redrawNode = function (nodeElem) {
    nodeElem.trigger("machinata-reporting-redraw-node");
};

/// <summary>
/// </summary>
Machinata.Reporting.Containerless.cleanupNode = function (nodeElem) {
    
};