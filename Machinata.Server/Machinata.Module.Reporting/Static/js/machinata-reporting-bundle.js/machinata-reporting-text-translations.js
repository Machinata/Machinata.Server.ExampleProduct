﻿

/// <summary>
/// </summary>
Machinata.Reporting.Text.TRANSLATIONS = {
    "categories": {
        "global": {
            "translations": {
                "reporting.nodes.fullscreen": {
                    "en": "Enter fullscreen",
                    "de": "Vollbild anzeigen"
                },
                "reporting.nodes.print": {
                    "en": "Add to print composer",
                    "de": "Zum Print Composer hinzufügen"

                },
                "reporting.nodes.configure": {
                    "en": "Configure",
                    "de": "Konfigurieren"

                },
                "reporting.nodes.copy-to-clipboard": {
                    "en": "Copy to clipboard",
                    "de": "In die Zwischenablage kopieren"
                },
                "reporting.nodes.copied": {
                    "en": "Copied",
                    "de": "Kopiert"
                },
                "reporting.nodes.copied.text": {
                    "en": "The text has been copied to your clipboard. You can paste it into an Email, Word or Word.",
                    "de": "Der Text wurde in Ihre Zwischenablage kopiert. Sie können ihn in eine E-Mail, ein Word oder eine Word-Datei einfügen."
                },
                "reporting.nodes.copied.table": {
                    "en": "The table has been copied to your clipboard. You can paste it into an Email, Word or Excel.",
                    "de": "Die Tabelle wurde in Ihre Zwischenablage kopiert. Sie können sie in eine E-Mail, Word oder Excel einfügen."
                },
                "reporting.nodes.copied.grid": {
                    "en": "The grid has been copied to your clipboard. You can paste it into an Email, Word or Excel.",
                    "de": "Das Gitter wurde in Ihre Zwischenablage kopiert. Sie können sie in eine E-Mail, Word oder Excel einfügen."
                },
                "reporting.nodes.search-filter": {
                    "en": "Search & filter",
                    "de": "Suchen & Filtern"
                },
                "reporting.nodes.export": {
                    "en": "Export",
                    "de": "Export"
                },
                "reporting.nodes.export.what-format": {
                    "en": "What format would you like to export?",
                    "de": "Welches Format möchten Sie exportieren?"
                },
                "reporting.nodes.export.format-no-available": {
                    "en": "This data format is currently not available.",
                    "de": "Dieses Datenformat ist derzeit nicht verfügbar"
                },
                "reporting.nodes.ask-question": {
                    "en": "Ask a question",
                    "de": "Eine Frage stellen"
                },

                "reporting.nodes.add-to-dashboard": {
                    "en": "Add to dashboard",
                    "de": "Zum Dashboard hinzufügen"
                },
                "reporting.nodes.add-to-presentation": {
                    "en": "Add to presentation",
                    "de": "Zur Präsentation hinzufügen"

                },
                "reporting.nodes.share": {
                    "en": "Copy link & share",
                    "de": "Link kopieren & freigeben"
                },
                "reporting.nodes.share.copy": {
                    "en": "Copy link",
                    "de": "Link kopieren"
                },
                "reporting.nodes.share.description": {
                    "en": "You can share a direct link to this part of the report using:",
                    "de": "Sie können einen direkten Link zu diesem Teil des Berichts teilen:"
                },
                "reporting.nodes.share.copied": {
                    "en": "Link copied",
                    "de": "Link kopiert"
                },
                "reporting.nodes.share.copied.text": {
                    "en": "The link has been copied to your clipboard",
                    "de": "Der Link wurde in Ihre Zwischenablage kopiert"
                },
                "reporting.exit-fullscreen": {
                    "en": "Exit Fullscreen",
                    "de": "Vollbild beenden"
                },
                "reporting.loading-text": {
                    "en": "Loading...",
                    "de": "Laden..."
                },
                "reporting.your-catalog": {
                    "en": "Your Portfolios",
                    "de": "Ihre Portfolios"
                },
                "reporting.your-reports": {
                    "en": "Reports",
                    "de": "Berichte"
                },
                "reporting.view-all": {
                    "en": "View all",
                    "de": "Alle anzeigen"
                },
                "reporting.view-all-dates": {
                    "en": "View all dates",
                    "de": "Alle Daten anzeigen"
                },
                "reporting.view-report": {
                    "en": "View report",
                    "de": "Bericht anzeigen"
                },
                "reporting.view-latest-report": {
                    "en": "View latest report",
                    "de": "Neuesten Bericht ansehen"
                },
                "reporting.show-all-reports": {
                    "en": "Show all reports",
                    "de": "Alle Berichte anzeigen"
                },
                "reporting.choose-date": {
                    "en": "Choose reporting date",
                    "de": "Berichtsdatum wählen"
                },
                "reporting.change-date": {
                    "en": "Change reporting date",
                    "de": "Berichtsdatum ändern"
                },
                "reporting.total-row": {
                    "en": "Total",
                    "de": "Total"
                },
                "reporting.notice": {
                    "en": "Notice",
                    "de": "Hinweis"
                },
                "reporting.filter-no-results": {
                    "en": "No results for filters.",
                    "de": "Keine Ergebnisse für Filter."
                },
                "reporting.filter-no-results.reset": {
                    "en": "Reset",
                    "de": "Zurücksetzen"
                },
                "reporting.filter-placeholder": {
                    "en": "Search",
                    "de": "Suche"
                },
                "reporting.printing.print": {
                    "en": "Print",
                    "de": "Drucken"
                },
                "reporting.printing.option-report": {
                    "en": "Print entire report",
                    "de": "Gesamten Bericht drucken"
                },
                "reporting.printing.option-chapter": {
                    "en": "Print current chapter",
                    "de": "Aktuelles Kapitel drucken"
                },
               
                "reporting.printing.option-view-composer": {
                    "en": "View print composer",
                    "de": "Print Composer anzeigen"
                },
                "reporting.printing.option-section": {
                    "en": "Print this section",
                    "de": "Diesen Abschnitt drucken"
                },
                "reporting.printing.option-add-composer": {
                    "en": "Add to print composer",
                    "de": "Zum Print Composer hinzufügen"
                },
                "reporting.printing.print-composer.added.title": {
                    "en": "Item added",
                    "de": "Element hinzugefügt"
                },
                "reporting.printing.print-composer.added.message": {
                    "en": "Would you like to go to the print composer now?",
                    "de": "Wollen Sie jetzt zu dem Print Composer gehen?"
                },
                "reporting.printing.print-composer.reset.title": {
                    "en": "Reset print composer",
                    "de": "Print Composer zurücksetzen"
                },
                "reporting.printing.print-composer.reset.message": {
                    "en": "Are you sure you want to reset the print composer?",
                    "de": "Sind Sie sicher, dass Sie den Print Composer zurücksetzen wollen?"
                },
                "reporting.printing.print-report.title": {
                    "en": "Print report",
                    "de": "Bericht drucken"
                },
                "reporting.printing.print-report.message": {
                    "en": "You can print an entire report or just the current chapter. What would you like to do?",
                    "de": "Sie können einen ganzen Bericht oder nur das aktuelle Kapitel ausdrucken. Was möchten Sie tun?"
                },
                "reporting.printing.print-node.title": {
                    "en": "Print section",
                    "de": "Abschnitt drucken"
                },
                "reporting.printing.print.node.message": {
                    "en": "You can print a single section in your browser or add it to the print composer to create a custom report. You can also print an entire report or just the current chapter. What would you like to do?",
                    "de": "Sie können einen einzelnen Abschnitt in Ihrem Browser ausdrucken oder ihn dem Print Composer hinzufügen, um einen benutzerdefinierten Bericht zu erstellen. Sie können auch einen ganzen Bericht oder nur das aktuelle Kapitel drucken. Was möchten Sie tun?"
                },
                "reporting.tables.select-columns": {
                    "en": "Select which columns you would like to see:",
                    "de": "Wählen Sie aus, welche Spalten Sie sehen möchten:"
                },
                "reporting.nodes.select-columns": {
                    "en": "Select which columns you would like to see:",
                    "de": "Wählen Sie aus, welche Spalten Sie sehen möchten:"
                },
                "reporting.no-data-for-this-date": {
                    "en": "No data available for this date",
                    "de": "Keine Daten für dieses Datum verfügbar"
                }


            },

        }
    }
}
