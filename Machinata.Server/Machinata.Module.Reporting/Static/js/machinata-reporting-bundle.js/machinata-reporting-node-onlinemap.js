/// <summary>
/// A very simple node that displays a info message for testing and demonstration
/// purposes. A user on a production system should not see this.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.OnlineMapNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults = {};

/// <summary>
/// This node supports a toolbar.
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.tileLayerAPIKey = null;



Machinata.Reporting.Node.OnlineMapNode.init = function (instance, config, json, nodeElem) {
    console.log("--------------------");
    console.log(nodeElem);
    // Validate Core JS Maps
    if (Machinata.Maps == null) throw "Machinata.Maps has not be loaded. Please include the Machinata Core Maps JS library machinata-maps-bundle.js to have ";

    // Validate that a API key has been explicitly set in reporting config
    if (json.tileLayerAPIKey == null) throw "No Tile Layer API Key configured"; 

    // Create UI
    //var bgElem = $("<div class='node-bg'></div>").appendTo($(".ui-card"));
    var mapElem = $("<div class='map' style='width:100%;height:400px;'></div>").appendTo(nodeElem);

    nodeElem.data("map-elem", mapElem);

    // Force our api key
    Machinata.Maps.DEFAULT_MAPS_ACCESS_TOKEN = json.tileLayerAPIKey;

    

};
Machinata.Reporting.Node.OnlineMapNode.postBuild = function (instance, config, json, nodeElem) {

    var mapElem = nodeElem.data("map-elem");
    // Create the map...
    var mapOptions = {
        minZoom: 3, // zoomed out
        //maxZoom: 7, // zoomed in
        doubleClickZoom: true,
        scrollWheelZoom: true
    };

    var options = {};
    options["providerOptions"] = mapOptions;

    var map = Machinata.Maps.createMap(mapElem, options);
    nodeElem.data("map", map);
   
};

Machinata.Reporting.Node.OnlineMapNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
    var map = nodeElem.data("map");
    var useCluster = json["useCluster"];
    var jitter = json["jitter"];

    // Fill into coordinates
    var markers = [];
    for (i = 0; i < json.facts.length; i++) {
        var fact = json.facts[i];

        // unwrap link url
        if (fact.link != null) {
            fact["link"] = fact.link.url;
        }

        markers.push(fact);

    }


    
    Machinata.Maps.addMarkersToMap(map, markers, useCluster, jitter);
};









