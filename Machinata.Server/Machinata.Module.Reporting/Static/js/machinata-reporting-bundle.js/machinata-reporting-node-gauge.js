


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.GaugeNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults = {};

/// <summary>
/// ```solid``` chrome by default.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsDashboard = true;

/// <summary>
/// We do support a toolbar.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsToolbar = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Legends are not necessary by default.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.innerRadius = 0.5;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.outerRadius = 1.0;

/// <summary>
/// By default we use the ```magenta``` theme.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.theme = "magenta";

/// <summary>
/// Exports are not necessary.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.disableExport = true;

/// <summary>
/// The stroke width of the status arc line and it's arrow head, in pixels.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.statusArcLineSize = 2; // was 4


Machinata.Reporting.Node.GaugeNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var theme = Machinata.Reporting.getTheme(json.theme);
    var gray = Machinata.Reporting.getTheme("gray");
    return {
        "data": [
          {
              "name": "status",
              "values": null,
          },
          {
              "name": "segments",
              "values": null,
              "transform": [
                {
                    "type": "identifier",
                    "as": "index",
                },
                {
                    "type": "formula",
                    "as": "value",
                    "expr": "1"
                },
                {
                    "type": "formula",
                    "as": "color",
                    "expr": "datum.active == true ? 1 : 4"
                },
                {
                    "type": "formula",
                    "as": "colorShade",
                    "expr": "datum.active == true ? 'dark' : 'gray3'"
                },
                {
                    "type": "formula",
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                },
                {
                    "type": "pie",
                    "field": "value",
                    "startAngle": 0,
                    "endAngle": { "signal": "PI * 2" },
                    "sort": false
                }
              ]
          },
          {
              "name": "colorShades",
              "values": config.themeColorShadeNames
          },
          {
              "name": "grayShades",
              "values": config.themeGrayShadeNames
          },
        ],
        "scales": [
          {
              "name": "grayColorByShade",
              "type": "ordinal",
              "domain": { "data": "grayShades", "field": "data" },
              "range": { "scheme": "gray" }
          },
          {
              "name": "colorByShade",
              "type": "ordinal",
              "domain": { "data": "colorShades", "field": "data" },
              "range": { "scheme": json.theme }
          }
        ],

        "marks": [
          {
              "type": "arc",
              "from": { "data": "segments" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "colorByShade", "field": "colorShade" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "field": "startAngle" },
                      "endAngle": { "field": "endAngle" },
                      "tooltip": { "field": "titleResolved" },
                      //"padAngle": { "value": 0.03 },
                      "innerRadius": { "signal": "(min(width,height) / 2)*" + json.innerRadius },
                      "outerRadius": { "signal": "(min(width,height) / 2)*" + json.outerRadius },
                      "strokeCap": { "value": "square" }
                  }
              }
          },
          {
              "type": "arc",
              "from": { "data": "segments" },
              //"transform": [
              //      { "type": "filter", "expr": "datum.index < 3" },
              //],
              "encode": {
                  "enter": {
                      "stroke": { "signal": "datum.index == 1 ? 'transparent' : scale('grayColorByShade','gray1')" },
                      "strokeWidth": { "value": json.statusArcLineSize }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "field": "startAngle" },
                      "endAngle": { "field": "startAngle" },
                      "tooltip": { "field": "resolved" },
                      //"padAngle": { "value": 0.03 },
                      "innerRadius": { "signal": "(min(width,height) / 2)*" + json.innerRadius + " - " + (json.statusArcLineSize / 2) },
                      "outerRadius": { "signal": "(min(width,height) / 2)*" + json.outerRadius + " + " + (json.statusArcLineSize / 2) },
                      "strokeCap": { "value": "square" }
                  }
              }
          },
          {
              "type": "arc",
              "from": { "data": "status" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "colorByShade", "value": "bright" },
                      "tooltip": { "signal": "if(datum.tooltip, datum.tooltip.resolved, null)" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "value": 0.0 },
                      "endAngle": { "signal": "(data('status')[0].value / length(data('segments')))*(PI*2)" },
                      "innerRadius": { "signal": "(min(width,height) / 2)*" + (json.innerRadius + 0.1) },
                      "outerRadius": { "signal": "(min(width,height) / 2)*" + (json.outerRadius - 0.1) }
                  }
              }
          },
          {
              "type": "arc",
              "from": { "data": "status" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "grayColorByShade", "value": 'gray2' },
                      "tooltip": { "signal": "if(datum.tooltip, datum.tooltip.resolved, null)" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "value": 0 },
                      "endAngle": { "signal": "(data('status')[0].value*0.99 / length(data('segments')))*(PI*2)" },
                      "innerRadius": { "signal": "(min(width,height) / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " -" + (json.statusArcLineSize / 2) },
                      "outerRadius": { "signal": "(min(width,height) / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " +" + (json.statusArcLineSize / 2) }
                  }
              }
          },
          {
              "type": "symbol",
              "encode": {
                  "enter": {
                      "stroke": { "scale": "grayColorByShade", "value": 'gray2' },
                      "strokeWidth": { "value": (json.statusArcLineSize) },
                      "shape": { "value": "M-1,1L0,0L-1,-1" },
                      "size": { "signal": "(min(width,height) / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " *4.0" }, // at normal res should be about 1000
                      "tooltip": { "signal": "if(datum.tooltip, datum.tooltip.resolved, null)" }
                  },
                  "update": {
                      "angle": { "signal": "(data('status')[0].value*0.99 / length(data('segments')))*360" },
                      "x": { "signal": "width/2 + sin((data('status')[0].value*0.99 / length(data('segments')))*(PI*2))*  (min(width,height) / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                      "y": { "signal": "height/2 - cos((data('status')[0].value*0.99 / length(data('segments')))*(PI*2))*  (min(width,height) / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                  }
              }
          },
          {
              "type": "text",
              "style": "text",
              "encode": {
                  "enter": {
                      "fontWeight": { "value": "bold" },
                      "text": { "signal": "data('status')[0].title.resolved" },
                      "align": { "value": "center" },
                      "baseline": { "value": "middle" },
                  },
                  "update": {
                      "fontSize": { "signal": "max(  (min(width,height)/2)*0.1 ,  " + config.chartTextSize + "  )" }, // Dynamically size the font, but make sure we meet the minimum font size requirement
                      "x": { "signal": "width/2" },
                      "y": { "signal": "height/2" },
                  }
              }
          }
        ]
    };
};
Machinata.Reporting.Node.GaugeNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][1].values = json.segments;
    spec["data"][0].values = [json.status];
};

Machinata.Reporting.Node.GaugeNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.GaugeNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.GaugeNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







