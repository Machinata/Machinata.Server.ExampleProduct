using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;

namespace Machinata.Module.Mailing.Tests {


    [TestClass]
    public class MailingContactAddressTests {
        
        [TestInitialize]
        public void TestInit() {
            Core.Localization.Text.LoadAllLocalizationTextsFromFiles();
        }

        private void _testStructuredAddress(string inputString, Core.Model.Address address) {
            // Prepare structured address to test against...
            var mailingContact = new Model.MailingContact();
            mailingContact.Properties[Model.MailingContact.PROPERTIES_KEY_ADDRESS] = inputString;
            var structedAddr = mailingContact.StructuredAddress;
            // Do all asserts
            Assert.AreEqual(address.Address1, structedAddr.Address1, "Address1");
            Assert.AreEqual(address.Address2, structedAddr.Address2, "Address2");
            Assert.AreEqual(address.City, structedAddr.City, "City");
            Assert.AreEqual(address.ZIP, structedAddr.ZIP, "ZIP");
            Assert.AreEqual(address.Country, structedAddr.Country, "Country");
            Assert.AreEqual(address.CountryCode, structedAddr.CountryCode, "CountryCode");
        }
        
        [TestMethod]
        public void Mailing_Contact_Address_StructuredAddress_CH() {
            
            _testStructuredAddress(
                "Steinstrasse 27, c/o Micha Aprile, 8003 Zurich, Switzerland",
                new Core.Model.Address() {
                Address1 = "Steinstrasse 27",
                Address2 = "c/o Micha Aprile",
                ZIP = "8003",
                City = "Zurich",
                CountryCode = "ch"
            });
            
            _testStructuredAddress(
                "Strategic Management and Innovation, WEV H 309, Weinbergstr. 56, 8006 Zürich",
                new Core.Model.Address() {
                Address1 = "Strategic Management and Innovation",
                Address2 = "WEV H 309, Weinbergstr. 56",
                ZIP = "8006",
                City = "Zürich"
            });
            
            _testStructuredAddress(
                "Hochschule für Gestaltung und Kunst FHNW, Institut Visuelle Kommunikation, Freilager-Platz 1, Postfach, 4002 Basel",
                new Core.Model.Address() {
                Address1 = "Hochschule für Gestaltung und Kunst FHNW",
                Address2 = "Institut Visuelle Kommunikation, Freilager-Platz 1, Postfach",
                ZIP = "4002",
                City = "Basel"
            });
            
            
        }
        
        [TestMethod]
        public void Mailing_Contact_Address_StructuredAddress_USA() {
            
            _testStructuredAddress(
                "50 Goddard Ave., Brookline, MA 02445, USA",
                new Core.Model.Address() {
                Address1 = "50 Goddard Ave.",
                ZIP = "02445",
                City = "Brookline",
                StateCode = "MA",
                CountryCode = "us"
            });
            
            
        }

        [TestMethod]
        public void Mailing_Contact_Address_StructuredAddress_EU() {
            
            
            _testStructuredAddress(
                "Sint Catharinastraat 44B, 5611JD Eindhoven, The Netherlands",
                new Core.Model.Address() {
                Address1 = "Sint Catharinastraat 44B",
                ZIP = "5611JD",
                City = "Eindhoven",
                CountryCode = "nl"
            });
            
            
        }

        [TestMethod]
        public void Mailing_Contact_Address_StructuredAddress_UK() {
            

            _testStructuredAddress(
                "9919 Church Street, Bromley, BR64 3AU, UK",
                new Core.Model.Address() {
                Address1 = "9919 Church Street",
                ZIP = "BR64 3AU",
                City = "Bromley",
                CountryCode = "uk"
            });
            
        }

        [TestMethod]
        public void Mailing_Contact_Address_StructuredAddress_DE() {
            
            _testStructuredAddress(
                "Brüningstrasse 28, 61350 Bad Homburg v.d.H., Germany",
                new Core.Model.Address() {
                Address1 = "Brüningstrasse 28",
                ZIP = "61350",
                City = "Bad Homburg v.d.H.",
                CountryCode = "de"
            });
            
        }

    }
}
