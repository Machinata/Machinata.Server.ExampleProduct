
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;


namespace Machinata.Module.Engage.Handler {


    public class AdminPageHandler : PageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("engage");
        }

        #endregion

        #region Menu 

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "hand-cursor",
                Path = "/admin/engage",
                Title = "{text.engage}",
                Sort = "500"
            });
        }

        #endregion


        [RequestHandler("/admin/engage")]
        public void Default() {
           
            // Navigation
            this.Navigation.Add("engage");
        }

       
    }
}
