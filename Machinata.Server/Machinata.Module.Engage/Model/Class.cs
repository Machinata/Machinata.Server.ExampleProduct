using System;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Machinata.Module.Engage.Model {

    [Serializable()]
    [ModelClass]
    public partial class Template : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

       

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Template() {
           
        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }
     

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        



      
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
       

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


       
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextTemplateExtensions {

        //[ModelSet]
        //public static DbSet<Class> Classes(this Core.Model.ModelContext context) {
        //    return context.Set<Class>();
        //}
                
       
    }

    #endregion

}
