

/// <summary>
/// A standard info text.
/// </summary>
Machinata.Dashboard.Widgets.Types["info"] = {
    init: function (dashboard, elem, config) {
        // Tools
        if(config["can-be-removed"] != false) Machinata.Dashboard.Widgets.addRemoveTool(elem, "Remove");
        if (config.settings["URL"] != null) Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        // Content
        var textElem = $("<div></div>");
        if (config.settings["Text"] != null) textElem.append($("<h2></h2>").text(config.settings["Text"]));
        if (config.settings["Subtext"] != null) textElem.append($("<h3></h3>").text(config.settings["Subtext"]).addClass(config.settings["Text"] == null ? "spaced-top" : ""));
        if (config.settings["Columns"] != null) textElem.addClass("ui-column-content");
        elem.data("contents").append(textElem);
        if (config.settings["TrailerText"] != null) {
            elem.data("contents").append($("<a class='button button--small button--tertiary'></a>").text(config.settings["TrailerText"]).append($("<span class='icon icon-arrow'></span>")));
            elem.data("contents").find("a.button").click(function () {
                // Popup?
                if (config.settings["TrailerLinkIsPopup"] == true) {
                    Machinata.showPageAsDialog(config.settings["TrailerLink"]);
                } else {
                    url = config.settings["TrailerLink"];
                    if (url == null) url = config.settings["URL"];
                    url = url.replace("{current-page}", Machinata.getPagePath());
                    Machinata.goToPage(url);
                }
            });
        }

    }
};
