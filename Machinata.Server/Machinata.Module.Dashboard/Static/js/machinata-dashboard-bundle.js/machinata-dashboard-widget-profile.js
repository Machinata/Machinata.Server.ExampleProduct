

/// <summary>
/// A profile with key information.
/// </summary>
Machinata.Dashboard.Widgets.Types["profile"] = {
    init: function (dashboard, elem, config) {
        elem.addClass("type-meta");
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], "icon-csam-fn-small-settings1", "Settings");
        // Content
        elem.addClass("option-contents-padding");
        elem.data("contents").append($("<h3 class='title'></h3>").text(config.settings["FirstName"] + " " + config.settings["LastName"]));
        //elem.data("contents").append($("<p class='subtitle'></h3>").text("Key Information"));

        var bottom = $("<div class='bottom'></div>");
        var metaTable = $("<div class='table'></div>").appendTo(bottom);
        {
            var row = $("<div class='row'></div>").appendTo(metaTable);
            row.append($("<div class='key'></div>").text("CSAM ID"));
            row.append($("<div class='value'></div>").text(config.settings["CSAMId"]));
        }
        {
            var row = $("<div class='row'></div>").appendTo(metaTable);
            row.append($("<div class='key'></div>").text("Company"));
            row.append($("<div class='value'></div>").text(config.settings["Company"]));
        }
        {
            var row = $("<div class='row'></div>").appendTo(metaTable);
            row.append($("<div class='key'></div>").text("Last Login"));
            row.append($("<div class='value'></div>").html(config.settings["LastLoginDate"] + " " + config.settings["LastLoginTime"] + "<br/>" + config.settings["LastLoginLocation"]));
        }
        elem.data("contents").append(bottom);

        //Machinata.Dashboard.Widgets.setGlobalLink(elem, config.settings["URL"]);
    }
};
