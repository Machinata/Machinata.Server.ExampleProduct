

/// <summary>
/// Shortcut widget type with large icon and global link.
/// </summary>
Machinata.Dashboard.Widgets.Types["shortcut"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        // Content
        var contents = $("<div class='vertical-aligner'><div class='vertical-alignee'><div class='shortcut-icon'></div></div></div>");
        contents.find(".shortcut-icon").addClass(config.settings["Icon"]);
        elem.data("contents").append(contents);
        Machinata.Dashboard.Widgets.setGlobalLink(elem, config.settings["URL"]);
    }
};



Machinata.Dashboard.Widgets.Types["reportfullscreen"] = {
    init: function (dashboard, elem, config) {
        elem.addClass("type-shortcut");
        // Tools
        Machinata.Dashboard.Widgets.addTool(elem, config.settings["Hint"], "icon-csam-fn-small-scan", function () {
            Machinata.Reporting.Tools.openFullscreenReport();
        });
        // Content
        var contents = $("<div class='vertical-aligner'><div class='vertical-alignee'><div class='shortcut-icon'></div></div></div>");
        contents.find(".shortcut-icon").addClass(config.settings["Icon"]);
        elem.data("contents").append(contents);
        //Machinata.Dashboard.Widgets.setGlobalLink(elem, config.settings["URL"]);
    }
};


