


/// <summary>
/// Contact/profile widget.
/// </summary>
Machinata.Dashboard.Widgets.Types["contact"] = {
    init: function (dashboard, elem, config) {
        elem.addClass("type-meta");
        // Tools
        if (config.settings["URL"] != null) Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        //if (config.settings["URLCall"] != null) Machinata.Dashboard.Widgets.addLinkSubTool(elem, config.settings["URLCall"], "icon-csam-fn-small-phone", "Call");
        //if (config.settings["URLEmail"] != null) Machinata.Dashboard.Widgets.addLinkSubTool(elem, config.settings["URLEmail"], "icon-csam-fn-small-mail", "Email");
        //if (config.settings["URLChat"] != null) Machinata.Dashboard.Widgets.addLinkSubTool(elem, config.settings["URLChat"], "icon-csam-fn-small-comment", "Chat");
        //if (config.settings["URLQuestion"] != null) Machinata.Dashboard.Widgets.addLinkSubTool(elem, config.settings["URLQuestion"], "icon-csam-fn-small-comment", "Ask a question");
        // Content
        elem.addClass("option-contents-padding");
        // Bottom infos
        var bottomElem = $("<div class='bottom'></p>");
        bottomElem.append($("<p class='name ui-no-bottom-margin'></p>").text(config.settings["FirstName"] + " " + config.settings["LastName"]));
        bottomElem.append($("<p class='meta ui-no-bottom-margin'></p>")
            .append($("<a class='text-link'></a>").text(config.settings["Phone"]).attr("href", config.settings["URLCall"]))
            .append($("<a class='text-link'></a>").text(config.settings["Email"]).attr("href", config.settings["URLEmail"]))
            .append($("<div></div>").text(config.settings["Title"]))
        );
        elem.data("contents").append(bottomElem);

        // Photo
        var photoElemShim = $("<div class='profile-photo shim'></div>"); //Note: this shim is used to get the proper sizing, unfortunately this was not possible CSS only
        var photoElem = $("<div class='profile-photo'></div>").css("background-image", "url('" + config.settings["Photo"] + "'");
        photoElem.hover(function () {
            photoElemShim.attr("orig-size", photoElemShim.css("width"));
            photoElem.css("width", "100%");
            photoElem.css("height", "100%");
            photoElem.addClass("expand");
        }, function () {
            photoElem.css("width", photoElemShim.attr("orig-size"));
            photoElem.css("height", photoElemShim.attr("orig-size"));
            photoElem.removeClass("expand");
        });
        elem.data("contents").append(photoElemShim);
        elem.data("contents").append(photoElem);

        //Machinata.Dashboard.Widgets.setGlobalLink(elem, config.settings["URL"]);
    }
};

