

/// <summary>
/// Guage for displaying a level indicator.
/// </summary>
Machinata.Dashboard.Widgets.Types["guage"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        elem.addClass("option-contents-padding");

        var guage = $("<div class='ui-guage option-vertical'>");
        var statuses = $("<div class='items'>").appendTo(guage);
        var itemsConfig = config.settings["Items"];
        for (var i = 0; i < itemsConfig.length; i++) {
            var itemConfig = itemsConfig[i];
            var itemWrapper = $("<div class='item-wrapper'></div>");
            //selector = $("<div class='selector'></div>").appendTo(itemWrapper);
            //if (itemConfig["Selected"] == true) selector.addClass("option-selected");
            var item = $("<div class='item'></div>").appendTo(itemWrapper);
            if (itemConfig["Selected"] == true) item.addClass("option-selected");
            item.append($("<div class='indicator'></div>"));
            item.append($("<div class='pusher'></div>"));
            item.append($("<div class='value'></div>").text(itemConfig["Value"]));
            item.append($("<div class='title'></div>").text(itemConfig["Title"]));
            item.append($("<div class='subtitle'></div>").text(itemConfig["SubTitle"]));
            item.append($("<div class='tab'></div>").addClass("bg-" + itemConfig["Color"]));
            item.addClass("bg-" + itemConfig["Color"]);
            item.addClass("fg-" + itemConfig["Color"]);
            item.find(".value,.title").addClass("fg-" + itemConfig["Color"]);
            statuses.append(itemWrapper);
        }

        elem.data("contents").append(guage);
    }
};
