/// <summary>
/// Collection of tools that supplement a dashboard.
/// </summary>
Machinata.Dashboard.Tools = {};


/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Dashboard.Tools.openFullscreen = function (url,title) {
    // Create fullscreen UI
    var fullscreenContainer = $("<div class='bb-dashboard-fullscreen-container'><iframe></iframe</div>");
    fullscreenContainer.find("iframe").attr("src", url);
    // Toolbar
    var fullscreenToolbar = Machinata.Reporting.buildToolbar({ chrome: "light" }, title, "");
    fullscreenContainer.find("iframe").on("load", function () {
        
    });
    Machinata.Reporting.addToolToToolbar(fullscreenContainer, {}, fullscreenToolbar, "Exit Fullscreen", "icon-csam-fn-small-scan", function () {
        Machinata.Dashboard.Tools.exitFullscreen();
    });
    fullscreenToolbar.addClass("bb-dashboard-fullscreen-toolbar");
    fullscreenToolbar.find(".subtitle").text("");
    fullscreenContainer.append(fullscreenToolbar);
    $("body").append(fullscreenContainer);
    // Enter fullscreen
    fullscreenContainer.fullScreen(true);
};


/// <summary>
/// Forces a exit of fullscreen mode
/// </summary>
Machinata.Dashboard.Tools.exitFullscreen = function () {
    var fullscreenContainer = $(".bb-dashboard-fullscreen-container");
    fullscreenContainer.fullScreen(false);
    fullscreenContainer.remove();
};




/// <summary>
/// Scrolls to a item
/// </summary>
Machinata.Dashboard.Tools.showItemByName = function (name) {
    var item = $(".bb-dashboard-item[data-item-name='" + name + "']");
    Machinata.UI.scrollTo(item, 1000, {offset:-80});
};