

/// <summary>
/// Dashboard Widgets namespace for everything widget related.
/// A dashboard widget is a single item on the dashboard, where
/// each item has a type, its corresponding implementation, and its
/// data.
/// </summary>
Machinata.Dashboard.Widgets = {};

/// <summary>
/// Main object where all the custom implemenations are mapped to.
/// </summary>
Machinata.Dashboard.Widgets.Types = {};

/// <summary>
/// A global store for all widget data. This enables a quick lookup if needed 
/// from an external module.
/// </summary>
Machinata.Dashboard.Widgets.data = {};

/// <summary>
/// Register data for a widget.
/// </summary>
Machinata.Dashboard.Widgets.registerData = function (config) {
    var key = config["id"];
    if (config["public-id"] != null) {
        key = config["public-id"];
        config["id"] = config["public-id"];
    }
    Machinata.Dashboard.Widgets.data[key] = config;
};


/// <summary>
/// Initializes a widget. This method will automatically grab the implementation
/// and execute the implementation routines.
/// </summary>
Machinata.Dashboard.Widgets.init = function (dashboard, elem, config) {
    // Register json and dashboard
    if (config == null) return;
    elem.data("config", config);
    elem.data("id", config["id"]);
    elem.data("type", config["type"] || config["dashboard-item-type"]);
    elem.data("dashboard", dashboard);
    elem.data("toolbar", elem.find(".bb-dashboard-toolbar"));
    elem.data("subtoolbar", elem.find(".bb-dashboard-subtoolbar"));
    elem.data("contents", elem.find(".bb-dashboard-contents"));

    // Initial toolbar state
    elem.data("toolbar").find(".title").text(config["title"]);
    elem.data("toolbar").find(".subtitle").text(config["subtitle"]);
    if (config["title"] == "") elem.addClass("option-notoolbar");

    // Add type and theme as class
    elem.addClass("type-" + elem.data("type"));
    elem.addClass("theme-" + config["theme"]);

    if (config["theme"] != "") {
        elem.data("toolbar").addClass("option-" + config["theme"]);
        elem.data("subtoolbar").addClass("option-" + config["theme"]);
    }

    if (config["icon"] != null && config["icon"] != "") {
        elem.data("toolbar").find(".section-icon").addClass(config["icon"]);
        elem.data("toolbar").addClass("option-section-icon");
    }

    // Resize?
    if (config["supported-sizes"] != null && config["supported-sizes"].split(",").length > 1) {
        if (!dashboard.hasClass("option-no-resize")) {
            Machinata.Dashboard.Widgets.addResizeSubTool(elem, "Resize");
        }
    }

    // Add to dashboard?
    if (config["can-be-added-to-dashboards"] == true || config["supports-dashboard"] == true) {
        Machinata.Dashboard.Widgets.addAddToDashboardSubTool(elem, "Add to dashboard");
    }

    // Remove?
    if (config["can-be-removed"] || dashboard.hasClass("option-removable")) {
        Machinata.Dashboard.Widgets.addRemoveSubTool(elem, "Remove");
    }

    // Init routine for widget type
    var widgetImpl = Machinata.Dashboard.Widgets.getImpl(elem);
    if (widgetImpl != null) {
        widgetImpl.init(dashboard, elem, config);
    } else {
        console.log("Warning: no dashboard implementation for " + elem.data("type"));
    }
    elem.data("impl", widgetImpl);

    // Add to dashboard?
    /*
    if (config["can-be-added-to-dashboards"] == true || config["supports-dashboard"] == true) {
        // Do we have a subtool? If so we add the add to dashboard to that, otherwise we add it to the main toolbar
        if (elem.hasClass("option-has-subtool")) {
            Machinata.Dashboard.Widgets.addAddToDashboardSubTool(elem, "Add to dashboard");
        } else {
            Machinata.Dashboard.Widgets.addAddToDashboardTool(elem, "Add to dashboard");
        }
    }*/

    //if (config.size == "large-wide-growing") {
    //    elem.after($("<div class='clear'></div>"));
    //}


    // Info?
    if (config["info"] != null) {
        Machinata.Dashboard.Widgets.addTool(elem, config["info"]["title"], "icon-csam-fn-small-info", function () {
            if (config["info"]["page"] != null) {
                // Show inline page
                Machinata.showPageAsDialog(config["info"]["page"]);
            } else {
                // Show text
                Machinata.messageDialog(config["info"]["title"], config["info"]["text"]).show();
            }
        });
    }

    // Register with UI framework
    Machinata.UI.bind(elem);

};

/// <summary>
/// Gets a implementation for the given widget element.
/// </summary>
Machinata.Dashboard.Widgets.getImpl = function (elem) {
    return Machinata.Dashboard.Widgets.Types[elem.data("type")];
};

/// <summary>
/// Adds a tool to the widget toolbarElem.
///     action: a click handler.
///     position: use 'last' to position it last
/// </summary>
Machinata.Dashboard.Widgets.addTool = function (elem, title, icon, action, toolbarElem, position) {
    var config = elem.data("config");
    if (toolbarElem == null) toolbarElem = elem.data("toolbar");
    // Suppressed?
    if (elem.data("config")["suppress-tools"] == true) return;
    // Icon
    var toolElem = $("<div class='tool " + icon + "'></div>");
    if(position == "last") {
        toolElem.prependTo(toolbarElem.find(".tools"));
    } else {
        toolElem.appendTo(toolbarElem.find(".tools"));
    }
    
    if (config["TabId"] != null) toolElem.attr("data-tab", config["TabId"]);
    toolElem.click(action);
    elem.addClass("option-has-tool");
    if (Machinata.Dashboard.Config.useTooltipsOnTools == true) toolElem.attr("title", title);
    if (toolbarElem == elem.data("subtoolbar")) elem.addClass("option-has-subtool");
    // Title interaction
    if (title != null && Machinata.Dashboard.Config.useHintsOnTools == true) {
        title = title.replace("\n", "<br/>");
        title = title.replace("{title}", config["title"]);
        toolElem.hover(function () {
            toolbarElem.find(".hint").html(title);
            toolbarElem.addClass("option-show-hint");
        }, function () {
            toolbarElem.removeClass("option-show-hint");
        });
    }
    return toolElem;
};

/// <summary>
/// Adds a tool to the subtoolbar.
/// action: a click handler.
/// </summary>
Machinata.Dashboard.Widgets.addSubTool = function (elem, title, icon, action) {
    return Machinata.Dashboard.Widgets.addTool(elem, title, icon, action, elem.data("subtoolbar"));
};

/// <summary>
/// Adds a tab to the widget subToolbarElem.
/// action: a click handler.
/// </summary>
Machinata.Dashboard.Widgets.addTab = function (elem, title, action) {
    var config = elem.data("config");
    toolbarElem = elem.data("subtoolbar");
    toolbarElem.addClass("option-has-tab");
    // Suppressed?
    if (elem.data("config")["suppress-tabs"] == true) return;
    var tabsElem = toolbarElem.find(".tabs");
    if (tabsElem.length == 0) tabsElem = $("<div class='tabs'></div>").prependTo(toolbarElem);
    // Tab
    var tabElem = $("<div class='tab'></div>").text(title).appendTo(toolbarElem.find(".tabs"));
    tabElem.click(action);
    elem.addClass("option-has-tab");
    elem.addClass("option-has-subtool"); // for easier compatibility
    return tabElem;
};

Machinata.Dashboard.Widgets.addRemoveTool = function (elem, hint) {
    var toolElem = Machinata.Dashboard.Widgets.addTool(elem, hint, "icon-csam-fn-small-cross", function () {
        var dashboard = elem.data("dashboard");
        Machinata.Dashboard.confirmRemoveItem(dashboard, elem);
    });
    toolElem.addClass("tool-remove");
    // Do we already have this as a subtool? If so, we remove the subtool (ie we upgrade)
    elem.find(".bb-dashboard-subtoolbar .tool-remove").remove();
};
Machinata.Dashboard.Widgets.addRemoveSubTool = function (elem, hint) {
    var toolElem = Machinata.Dashboard.Widgets.addSubTool(elem, hint, "icon-csam-fn-small-cross", function () {
        var dashboard = elem.data("dashboard");
        Machinata.Dashboard.confirmRemoveItem(dashboard, elem);
    });
    toolElem.addClass("tool-remove");
};


Machinata.Dashboard.Widgets.addResizeSubTool = function (elem, hint) {
    Machinata.Dashboard.Widgets.addSubTool(elem, hint, "icon-csam-fn-small-scan", function () {
        Machinata.Dashboard.cycleSize(elem.data("dashboard"), elem);
    });
};

Machinata.Dashboard.Widgets.addAddToDashboardTool = function (elem, hint) {
    Machinata.Dashboard.Widgets.addTool(elem, hint, "icon-csam-ed1-large-builiding-blocks", function () {
        // Init
        var config = elem.data("config");
        var dashboard = elem.data("dashboard");
        // Source and base link from dashboard
        var source = dashboard.attr("data-source");
        var baseURL = dashboard.attr("data-base-url");
        // Call helper method
        Machinata.Dashboard.addExistingDashboardItemToDashboard(config, source, baseURL);
    });
};

Machinata.Dashboard.Widgets.addAddToDashboardSubTool = function (elem, hint) {
    Machinata.Dashboard.Widgets.addSubTool(elem, hint, "icon-csam-ed1-large-builiding-blocks", function () {
        // Init
        var config = elem.data("config");
        var dashboard = elem.data("dashboard");
        // Source and base link from dashboard
        var source = dashboard.attr("data-source");
        var baseURL = dashboard.attr("data-base-url");
        // Call helper method
        Machinata.Dashboard.addExistingDashboardItemToDashboard(config, source, baseURL);
    });
};

/// <summary>
/// Adds a standard link tool.
/// </summary>
Machinata.Dashboard.Widgets.addLinkTool = function (elem, url, icon, title, toolbar) {
    if (icon == null) icon = "icon-csam-fn-small-arrow-right";
    Machinata.Dashboard.Widgets.addTool(elem, title, icon, function () {
        url = url.replace("{current-page}", Machinata.getPagePath());
        Machinata.goToPage(url);
    }, toolbar);
};

/// <summary>
/// Adds a standard share tool.
/// </summary>
Machinata.Dashboard.Widgets.addShareTool = function (elem, url, icon, title, toolbar) {
    if (icon == null) icon = "icon-csam-fn-small-share";
    if (title == null) title = "Share";
    Machinata.Dashboard.Widgets.addTool(elem, title, icon, function () {
        url = url.replace("{current-page}", Machinata.getPagePath());
        //TODO: refactor this out
        alert("NOT SUPPORTED");
    }, toolbar);
};

Machinata.Dashboard.Widgets.addLinkSubTool = function (elem, url, icon, title) {
    Machinata.Dashboard.Widgets.addLinkTool(elem, url, icon, title, elem.data("subtoolbar"));
};

/// <summary>
/// Makes the entire widget clickable.
/// </summary>
Machinata.Dashboard.Widgets.setGlobalLink = function (elem, url) {
    elem.addClass("option-global-link");
    elem.find(".sizer").click(function () {
        Machinata.goToPage(url);
    });
};

/// <summary>
/// Call the implementaiton resize routine for a widget element.
/// </summary>
Machinata.Dashboard.Widgets.onResize = function (elem) {
    var widgetImpl = elem.data("impl");
    if (widgetImpl != null && widgetImpl.onResize != null) widgetImpl.onResize(elem);
};




































































