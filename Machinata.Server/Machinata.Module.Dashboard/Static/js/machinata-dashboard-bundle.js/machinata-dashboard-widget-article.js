

/// <summary>
/// Article widget with date, title, summary and image.
/// </summary>
Machinata.Dashboard.Widgets.Types["article"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        //Machinata.Dashboard.Widgets.addTool(elem, "Favorite", "icon-csam-fn-small-favourite", function () {
        //    //TODO: refactor out
        //    CSAM.Articles.favorite(config.settings["ShortURL"]);
        //});
        Machinata.Dashboard.Widgets.addSubTool(elem, "Bookmark", "icon-csam-fn-small-bookmark", function () {
            CSAM.Articles.bookmark(config.settings["ShortURL"]);
        });

        // Init
        var dateMonth = config.settings["DateMonth"];
        var dateDay = config.settings["DateDay"];
        if ((dateMonth == null || dateMonth == "") && config.settings["Date"] != null) {
            var date = Machinata.dateFromString(config.settings["Date"], "dd.mm.yy"); //TODO format??
            dateMonth = Machinata.Time.getShortMonth(date);
            dateDay = Machinata.formattedDate(date, 'dd');
        }
        // Text Content
        var textElem = $("<div class='text'></div>");
        var dateElem = $("<div class='date'></div>").attr("data-date", config.settings["Date"]);
        dateElem.append($("<div class='month'></div>").text(dateMonth));
        dateElem.append($("<div class='day ui-number'></div>").text(dateDay));
        textElem.append(dateElem);
        var subjectElem = $("<div class='subject'></div>").text(config.settings["Title"]).appendTo(textElem);
        var introElem = $("<div class='intro'></div>").text(config.settings["Summary"]).appendTo(textElem);
        introElem.append("<br/>").append($("<a></a>").text("Read article").attr("href", config.settings["URL"]));
        // Media Content
        var mediaElem = $("<div class='media'></div>");
        var imgElem = $("<div class='image'></div>").css("background-image", "url(" + config.settings["Image"] + ")");
        mediaElem.append(imgElem);
        // Compile DOM
        elem.data("contents").append(textElem);
        elem.data("contents").append(mediaElem);
    }
};
