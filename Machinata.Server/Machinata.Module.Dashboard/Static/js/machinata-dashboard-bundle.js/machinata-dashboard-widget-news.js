


/// <summary>
/// A news article widget.
/// </summary>
Machinata.Dashboard.Widgets.Types["news"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, "More Insights");
        Machinata.Dashboard.Widgets.addRemoveSubTool(elem, "Remove");
        elem.addClass("option-bottom-fader");
        // Content
        var itemsConfig = config.settings["Articles"];
        var listing = $("<div class='ui-listing'></div>");
        for (var i = 0; i < itemsConfig.length; i++) {
            var itemConfig = itemsConfig[i];
            var item = $("<a class='ui-listing-item'></a>");
            item.attr("href", itemConfig["URL"]);
            item.append(
                $("<div class='ui-microtoolbar'></div>")
                    .append("<span class='label'>" + itemConfig["Date"] + "</span>")
                    .append("<span class='tool icon-csam-fn-small-bookmark'></span>")
                    .append("<span class='tool icon-csam-fn-small-like'></span>")
            );
            item.append($("<img class='image'/>").attr("src", itemConfig["Image"] + "?crop=400x200"));
            item.append($("<div class='title'></div>").text(itemConfig["Title"]));
            item.append($("<div class='text'></div>").text(Machinata.String.createSummarizedText(itemConfig["Text"], 80)));
            item.appendTo(listing);
        }
        listing.appendTo(elem.data("contents"));
    }
};

