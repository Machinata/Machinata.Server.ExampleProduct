

/// <summary>
/// A fund overview.
/// TODO: refactor this out to CSAM namespace.
/// </summary>
Machinata.Dashboard.Widgets.Types["fund"] = {
    init: function (dashboard, elem, config) {
        // Tools
        if(config.settings["URL"] != null) Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        elem.addClass("type-metrics");
        elem.addClass("option-grid-topborder");
        // Sub tools
        Machinata.Dashboard.Widgets.addSubTool(elem, "Bookmark", "icon-csam-fn-small-bookmark", function () {
            CSAM.Funds.bookmark(config.settings["Fund"]["ISIN"]);
        });
        Machinata.Dashboard.Widgets.addSubTool(elem, "Compare", "icon-csam-fn-small-transfer", function () {
            CSAM.Funds.compare(config.settings["Fund"]["ISIN"]);
        });
        if (config.settings["Fund"]["IFAEnabled"] == true) {
            Machinata.Dashboard.Widgets.addSubTool(elem, "Buy fund", "icon-csam-fn-small-tasks", function () {
                CSAM.Funds.trade(config.settings["Fund"]["ISIN"]);
            });
        }
        if (config.settings["Fund"]["ChangeShareClassEnabled"] == true) {
            Machinata.Dashboard.Widgets.addTool(elem, "Change share class", "icon-csam-fn-small-settings1", function () {
                CSAM.Funds.changeShareClass(config.settings["Fund"]["ISIN"]);
            });
        }
        if (config.settings["Fund"]["SocialShareEnabled"] == true) {
            Machinata.Dashboard.Widgets.addTool(elem, "Copy link and share", "icon-csam-fn-small-share", function () {
                CSAM.Funds.share(config.settings["Fund"]["ISIN"]);
            });
        }
        // Init
        var topElem = $("<div class='top'></div>");
        var bottomElem = $("<div class='bottom'></div>");
        // Top
        if (!String.isNullOrEmpty(config.settings["Title"])) topElem.append($("<h3 class='title'></h3>").text(config.settings["Title"]));
        if (!String.isNullOrEmpty(config.settings["Fund"]["ShareClassInceptionDate"])) topElem.append($("<p class=''></p>").html("Share class inception date<br/>" + config.settings["Fund"]["ShareClassInceptionDate"]));
        // Large NAV
        if (!String.isNullOrEmpty(config.settings["Fund"]["PerformanceYTD"])) {
            $("<div class='items option-columns-1'><div class='group'><div class='item'><div class='title'>Performance YTD</div><div class='value ui-number'></div></div></div></div>")
                .appendTo(bottomElem)
                .find(".value")
                .text(config.settings["Fund"]["PerformanceYTD"])
                .append($("<span class='ui-symbol'>%</span>"));
        }
        // Items
        if (config.settings["Items"] != null) {
            var metaGrid = Machinata.Dashboard.Widgets.Types["metrics"].buildItemsGrid(dashboard, elem, config, 2);
            bottomElem.append(metaGrid);
        }
        // Performance
        var bottomItemsGroup = $("<div class='items option-columns-3'><div class='group'></div></div>")
            .appendTo(bottomElem)
            .find(".group");
        if (config.settings["Fund"]["Performance1Yr"] != null) {
            $("<div class='item'><div class='title'>1 Yr</div><div class='value ui-number'><span class='ui-symbol'>%</span></div></div>")
                .appendTo(bottomItemsGroup)
                .find(".value")
                .text(config.settings["Fund"]["Performance1Yr"])
                .append($("<span class='ui-symbol'>%</span>"));
        }
        if (config.settings["Fund"]["Performance3Yr"] != null) {
            $("<div class='item'><div class='title'>3 Yr</div><div class='value ui-number'><span class='ui-symbol'>%</span></div></div>")
                .appendTo(bottomItemsGroup)
                .find(".value")
                .text(config.settings["Fund"]["Performance3Yr"])
                .append($("<span class='ui-symbol'>%</span>"));
        }
        if (config.settings["Fund"]["Performance5Yr"] != null) {
            $("<div class='item'><div class='title'>5 Yr</div><div class='value ui-number'><span class='ui-symbol'>%</span></div></div>")
                .appendTo(bottomItemsGroup)
                .find(".value")
                .text(config.settings["Fund"]["Performance5Yr"])
                .append($("<span class='ui-symbol'>%</span>"));
        }
        // Compile DOM
        elem.data("contents").append(topElem);
        elem.data("contents").append(bottomElem);
    },
    supportedSizes: function (config) {
        return "large-square,small-tall";
    },
};

