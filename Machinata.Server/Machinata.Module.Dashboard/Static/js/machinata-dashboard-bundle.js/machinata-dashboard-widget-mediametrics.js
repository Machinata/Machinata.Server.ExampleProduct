

/// <summary>
/// A metrics grid with media at the top.
/// TODO: refactor this out to CSAM namespace.
/// </summary>
Machinata.Dashboard.Widgets.Types["mediametrics"] = {
    init: function (dashboard, elem, config) {
        // Tools
        //Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        elem.addClass("type-metrics");
        elem.addClass("option-grid-topborder");
        // Init
        var topElem = $("<div class='top'></div>");
        var bottomElem = $("<div class='bottom'></div>");
        // Content
        var imageElem = $("<div class='media image'></div>").css("background-image","url('"+config.settings["Image"]+"')").appendTo(topElem);
        // Grid
        var metaGrid = Machinata.Dashboard.Widgets.Types["metrics"].buildItemsGrid(dashboard, elem, config, 2);
        // Layout
        bottomElem.append(metaGrid);
        elem.data("contents").append(topElem);
        elem.data("contents").append(bottomElem);
        this.onResize(elem);
    },
    onResize: function (elem) {
        var top = elem.data("contents").find(".top");
        var bottom = elem.data("contents").find(".bottom");
        top.height(elem.data("contents").height()-bottom.height());
    },
    supportedSizes: function (config) {
        return "large-square,small-tall";
    },
};

