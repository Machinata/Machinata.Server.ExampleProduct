


/// <summary>
/// 
/// </summary>
Machinata.Dashboard.Widgets.Types["chart"] = {
    init: function (dashboard, elem, config) {
        // Init
        elem.addClass("option-contents-padding");

        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);

        // Create the corresponding chart config for the Reporting module.
        //TODO: clean this up and align common data format so no translation is needed.
        var chartConfig = {};
        chartConfig["type"] = config.settings["ChartType"];
        chartConfig["data"] = config.settings["ChartData"];
        chartConfig["theme"] = config.settings["ChartTheme"];
        //chartConfig["no-legend"] = true;
        //chartConfig["no-x-axis"] = true;
        //chartConfig["no-y-axis"] = true;
        //if (config.settings["ChartLegend"] == true) chartConfig["no-legend"] = false;
        //if (config.settings["ChartXAxis"] == true) chartConfig["no-x-axis"] = false;

        if (config.size == "large-half" || config.size == "small-square") {
            chartConfig["no-y-axis"] = true;
        }
        if (config.size == "small-square") {
            chartConfig["no-x-axis"] = true;
        }

        // Create the chart using the reporting module
        var chartElem = Machinata.Reporting.buildChart(elem.data("contents"), chartConfig);
        if (config.theme != null) elem.addClass("option-" + config.theme);

    },
    onResize: function (elem) {
        // Make sure the chart is resized when the dashboard widget resizes...
        Machinata.Reporting.redrawChart(elem.find(".bb-reporting-chart"));
    },
    supportedSizes: function (config) {
        // Forward to Reporting JS implementation
        // Get the implementation and call its supported sizes method (if any)
        var implType = config.settings["ChartType"];
        var impl = Machinata.Reporting.Charts[implType];
        if (impl["supportedDashboardSizes"] != null) {
            return impl["supportedDashboardSizes"]();
        }
        return null;
    },
};