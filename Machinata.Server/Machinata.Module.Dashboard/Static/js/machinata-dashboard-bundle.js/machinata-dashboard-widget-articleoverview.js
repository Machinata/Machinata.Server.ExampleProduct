

/// <summary>
/// TODO: refactor this out to CSAM namespace.
/// </summary>
Machinata.Dashboard.Widgets.Types["articleoverview"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addTool(elem, "Share", "icon-csam-fn-small-share", function () {
            CSAM.Articles.share(Machinata.getPagePath());
        });
        Machinata.Dashboard.Widgets.addTool(elem, "Follow", "icon-csam-fn-small-favourite", function () {
            //CSAM.Articles.favorite(config.settings["ShortURL"]);
            CSAM.notSupported();
        });
        Machinata.Dashboard.Widgets.addSubTool(elem, "Add to bookmarks", "icon-csam-fn-small-bookmark", function () {
            CSAM.Articles.bookmark(config.settings["ShortURL"]);
        });
        // Superclass
        elem.addClass("type-mediametrics");
        Machinata.Dashboard.Widgets.Types["mediametrics"].init(dashboard, elem, config);
        this.onResize(elem);
    },
    onResize: function (elem) {
        Machinata.Dashboard.Widgets.Types["mediametrics"].onResize(elem);
    },
    supportedSizes: function (config) {
        return "large-square,small-tall";
    },
};

