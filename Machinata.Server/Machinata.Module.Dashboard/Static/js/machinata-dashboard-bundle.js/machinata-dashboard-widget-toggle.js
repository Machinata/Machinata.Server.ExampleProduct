


/// <summary>
/// Toggle widget
/// </summary>
Machinata.Dashboard.Widgets.Types["toggle"] = {
    init: function (dashboard, elem, config) {
        
        var items = config.settings["Items"];

        // Helper function
        function showTab(targetId) {
            // Find and toggle contents
            elem.data("contents").find(".tab-contents").hide();
            var targetContents = elem.data("contents").find(".tab-contents[data-tab='" + targetId + "']");
            targetContents.show();
            elem.data("toolbar").find("label.title").text(targetContents.data("title"));
            elem.data("toolbar").find("label.subtitle").text(targetContents.data("subtitle"));
            // Toogle tools
            elem.data("toolbar").find(".tools .tool").hide();
            elem.data("toolbar").find(".tools .tool[data-tab='" + targetId + "']").show();
            // Find tab
            elem.data("subtoolbar").find(".tabs .tab").removeClass("selected");
            var tabElem = elem.data("subtoolbar").find(".tabs .tab[data-tab='" + targetId + "']");
            tabElem.addClass("selected");
        }

        for (var i = 0; i < items.length; i++) {
            // Init
            var itemConfig = items[i];
            itemConfig["settings"] = itemConfig["Settings"]; //TODO backwards compat
            itemConfig["TabId"] = Machinata.UI.getElemUID("dashboarditemtab");

            // Create all sub-widgets
            var impl = Machinata.Dashboard.Widgets.Types[itemConfig["Type"]];
            var itemElem = $("<div class='tab-contents'></div>");
            itemElem.attr("data-tab", itemConfig["TabId"]);
            itemElem.data("title", itemConfig["Title"]);
            itemElem.data("subtitle", itemConfig["Subtitle"]);
            itemElem.data("contents", itemElem);
            itemElem.data("config", itemConfig);
            itemElem.data("toolbar", elem.data("toolbar"));
            itemElem.data("subtoolbar", elem.data("subtoolbar"));
            impl.init(dashboard, itemElem, itemConfig);
            elem.data("contents").append(itemElem);
            // Add tab item
            var tabElem = Machinata.Dashboard.Widgets.addTab(elem, itemConfig["Tab"], function () {
                showTab($(this).attr("data-tab"));
            });
            tabElem.attr("data-tab", itemConfig["TabId"]);
        }
        // Initial state
        showTab(items[0]["TabId"]);
    },
    onResize: function (elem) {
        
    }
};