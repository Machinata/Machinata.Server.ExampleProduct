

/// <summary>
/// Machinata Dashboard JS Library
/// Library for displaying and managing dynamic dashboards.
/// </summary>
Machinata.Dashboard = {};

Machinata.Dashboard.Config = {};
Machinata.Dashboard.Config.useTooltipsOnTools = true;
Machinata.Dashboard.Config.useHintsOnTools = true;
Machinata.Dashboard.Config.deleteAPICall = "/api/csam/portal/cockpit/dashboard/{public-id}/delete";
Machinata.Dashboard.Config.saveAPICall = "/api/csam/portal/cockpit/dashboard/{public-id}/save";



/// <summary>
/// Initializes a dashboard with its element. The selector must be a dashboard node with all dashboard items
/// (widgets) as its children.
/// </summary>
Machinata.Dashboard.init = function (dashboard, data, group) {

    // Do we have data?
    if (data != null) {
        // Create all the items
        for (var i = 0; i < data.length; i++) {
            // Init
            var item = data[i];

            // Validate
            if (item.enabled == false) continue;
            if (group != null && item.group != group) continue;

            // Unique id
            if (item.id == null) {
                item.id = Machinata.UI.getElemUID("dashboard_item");
            }
            // Register data
            Machinata.Dashboard.Widgets.registerData(item);
            // Create html element
            var dashboardItemHTML = " \
                <div class='ui-dashboard-item bb-dashboard-item'> \
                   <div class='sizer'> \
                       <div class='ui-toolbar bb-dashboard-toolbar'> \
                           <div class='section-icon'></div> \
                           <label class='title'></label> \
                           <label class='subtitle'></label> \
                           <label class='hint'></label> \
                           <div class='tools'></div> \
                       </div> \
                       <div class='ui-dashboard-contents bb-dashboard-contents'></div> \
                       <div class='ui-bottom-fader'></div> \
                       <div class='ui-toolbar option-small bb-dashboard-subtoolbar'> \
                           <label class='title'></label> \
                           <label class='hint'></label> \
                           <div class='tools'></div> \
                       </div> \
                   </div> \
               </div>";
            var dashboardItem = $(dashboardItemHTML);
            dashboardItem.addClass("option-"+ item["size"]);
            dashboardItem.attr("data-item-type", item["type"]);
            dashboardItem.attr("data-item-size", item["size"]);
            dashboardItem.attr("data-item-sizes", item["supported-sizes"]);
            dashboardItem.attr("data-item-id", item["id"]);
            dashboard.append(dashboardItem);
        }
        dashboard.append($("<div class='clear'></div>"));
        Machinata.UI.bind(dashboard.find(".bb-dashboard-item"));
    } else {
        // Data is loaded from the dashboard nodes...
    }

    // Initialize Packery
    var grid = dashboard.packery({
        itemSelector: '.bb-dashboard-item',
        gutter: 0,
        columnWidth: dashboard.find('.bb-dashboard-column-width')[0],
        rowHeight: dashboard.find('.bb-dashboard-row-height')[0],
    });

    // Make all items draggable
    var items = null;
    if (dashboard.hasClass("option-draggable")) {
        items = grid.find('.bb-dashboard-item').draggable({
            stop: function () {
                Machinata.debug("Machinata.Dashboard: draggable.stop");
                Machinata.Dashboard.updateLayout(dashboard);
            }
        });
    } else {
        items = grid.find('.bb-dashboard-item');
    }
    Machinata.Dashboard.registerItems(dashboard,items);

    // Bind drag events to Packery
    grid.packery('bindUIDraggableEvents', items);

    // Bind events
    grid.on('layoutComplete', function (event, items) {
        Machinata.debug("Machinata.Dashboard: packery.layoutComplete: " + items.length);
        for (var i = 0; i < items.length; i++) {
            var elem = $(items[i].element);
            Machinata.Dashboard.Widgets.onResize(elem);
        }
    });
    grid.on('dragItemPositioned', function (event, draggedItem) {
        Machinata.debug("Machinata.Dashboard: packery.dragItemPositioned: " + draggedItem.length);
    });
    grid.on('fitComplete', function (event, item) {
        Machinata.debug("Machinata.Dashboard: packery.fitComplete: " + item.length);
    });
    grid.on('removeComplete', function (event, removedItems) {
        Machinata.debug("Machinata.Dashboard: packery.removeComplete: " + removedItems.length);
    });

    //setTimeout(function () {
        Machinata.Responsive.detectNewResponsiveElements();
        Machinata.Responsive.updateResponsiveElements();
        Machinata.Dashboard.updateLayout(dashboard,false);
    //}, 1);
    
};

/// <summary>
/// Force the packery layout to update.
/// </summary>
Machinata.Dashboard.updateLayout = function (dashboard, save) {
    Machinata.debug("Machinata.Dashboard: updateLayout");
    setTimeout(function () {
        // Update packery layout
        dashboard.packery('layout');
        // Do API save
        if (save != false) {
            setTimeout(function () {
                Machinata.Dashboard.save(dashboard);
            }, 500);
        }
    }, 10);
};

/// <summary>
/// Cycle the size of a single dashboard widget.
/// </summary>
Machinata.Dashboard.cycleSize = function (dashboard, elem) {
    // Calculate new size
    var sizes = elem.attr("data-item-sizes").split(',');
    var current = elem.attr("data-item-size");
    var ii = 0;
    for (var i = 0; i < sizes.length; i++) {
        if (sizes[i] == current) {
            ii = i;
            break;
        }
    }
    ii++;
    if (ii >= sizes.length) ii = 0;
    var newSize = sizes[ii];
    // Ask for change
    Machinata.Dashboard.changeSize(dashboard, elem, newSize);
};


/// <summary>
/// Set the size of a dashboard widget.
/// </summary>
Machinata.Dashboard.changeSize = function (dashboard, elem, newSize) {
    // Remove old class
    var oldClass = "option-" + elem.attr("data-item-size");
    var newClass = "option-" + newSize;
    elem.removeClass(oldClass).addClass(newClass).attr("data-item-size", newSize);
    //dashboard.packery('shiftLayout');
    Machinata.Responsive.detectNewResponsiveElements();
    Machinata.Responsive.updateResponsiveElements();
    Machinata.Dashboard.updateLayout(dashboard);
};

Machinata.Dashboard.removeItems = function (dashboard, elems) {
    elems.each(function () {
        var elem = $(this);
        //elem.remove();
    });
    dashboard.packery('remove', elems);
    Machinata.Dashboard.updateLayout(dashboard);
};


Machinata.Dashboard.confirmRemoveItem = function (dashboard, elem) {
    var config = elem.data("config");
    var itemDescription = "";
    if (config["title"] != null) itemDescription += " " + config["title"];
    if (config["subtitle"] != null) itemDescription += " " + config["subtitle"];
    itemDescription = itemDescription.trim();
    var title = "Remove item";
    var message = "Are you sure you want to remove '" + itemDescription + "' from " + dashboard.attr("data-name") + "?";
    Machinata.confirmDialog(title, message)
        .okay(function () {
            Machinata.Dashboard.removeItems(elem.data("dashboard"), elem);
        })
        .show();
};


/// <summary>
/// Whenever a item is added to the dashboard, you must call this method
/// to ensure it is properly initialized and registered with the underlying
/// system.
/// </summary>
Machinata.Dashboard.registerItems = function (dashboard,elems) {
    elems.each(function () {
        var elem = $(this);
        // Get id
        var id = elem.attr("data-item-id");
        var json = Machinata.Dashboard.Widgets.data[id];
        Machinata.Dashboard.Widgets.init(dashboard, elem, json);
    });
    if (dashboard.hasClass("option-animate-in") == true) {
        var i = 0;
        elems.each(function () {
            var elem = $(this);
            setTimeout(function () {
                elem.addClass("animate-in");
            }, i * 50);
            i++;
        });
    }
};

Machinata.Dashboard.save = function (dashboard) {
    //TODO: support saving on either API or cookie
    var call = dashboard.attr("data-api-call");
    if (call == null) return; //TODO: save to browser cache
    Machinata.debug("Machinata.Dashboard: saving...");
    // Create data package
    var data = {};
    var items = dashboard.packery('getItemElements');
    for(var i = 0; i < items.length; i++) {
        var item = $(items[i]);
        var iid = item.attr("data-item-id");
        data["item_" + iid + "_size"] = item.attr("data-item-size");
        data["item_" + iid + "_sort"] = i;
    }
    // Generate preview
    var previewCanvas = Machinata.Dashboard.generatePreview(dashboard);
    data["preview"] = previewCanvas[0].toDataURL();
    previewCanvas.remove();
    // Do API call
    Machinata.apiCall(call)
        .data(data)
        .success(function (message) {
            Machinata.debug("Machinata.Dashboard: save complete!");
            // Reload?
            if (message.data["require-reload"] == true) {
                Machinata.reload();
            }
        })
        .send();
};



Machinata.Dashboard.deleteDashboardById = function (publicId, confirm) {
    if (confirm != false) {
        // Show confirmation
        Machinata.confirmDialog("Delete dashboard", "Are you sure you want to delete this dashboard?")
        .okay(function () {
            Machinata.Dashboard.deleteDashboardById(publicId, false);
        })
        .show();
    } else {
        // Delete via API call
        var call = Machinata.Dashboard.Config.deleteAPICall.replace("{public-id}", publicId);
        Machinata.apiCall(call)
            .success(function (message) {
                Machinata.reload();
            })
            .send();
    }
};


Machinata.Dashboard.generatePreview = function (dashboard) {
    var width = 400;
    var height = 0;
    var items = dashboard.packery('getItemElements');
    var scale = width / dashboard.width();
    var textsize = Math.floor(width * 0.08);
    var textpadding = 0.2;
    // Caclulate height
    for (var i = 0; i < items.length; i++) {
        var item = $(items[i]);
        var yy = item.position().top + item.height();
        if (yy > height) height = yy;
    }
    height = height * scale;
    Machinata.debug("Machinata.Dashboard.generatePreview");
    Machinata.debug("  width: " + width);
    Machinata.debug("  height: " + height);
    Machinata.debug("  scale: " + scale);
    Machinata.debug("  textsize: " + textsize);
    // Draw
    var canvas = $("<canvas></canvas>").attr("width", width).attr("height", height).hide().appendTo($("body"));
    var ctx = canvas[0].getContext("2d");
    ctx.font = "bold "+textsize + "px Arial";
    for (var i = 0; i < items.length; i++) {
        // Get item and properties
        var item = $(items[i]);
        var p = item.position();
        var xx = p.left * scale;
        var yy = p.top * scale;
        var id = item.attr("data-item-id");
        var json = Machinata.Dashboard.Widgets.data[id];
        // Draw box
        ctx.fillStyle = "black";
        ctx.fillRect(xx, yy, item.width() * scale, item.height() * scale);
        // Draw text
        ctx.fillStyle = "white";
        ctx.fillText(json["short-title"], xx + textsize * textpadding, yy + textsize * (1.2 - textpadding));
    }
    // Return
    return canvas;
};

Machinata.Dashboard.createNewDashboard = function (config, source, baseURL) {
    // New dashboard!
    Machinata.inputDialog("New dashboard", "Please enter the name of the new dashboard.", "", "Dashboard name")
        .input(function (val) {
            if (val == null || val == "") return;
            // Do API call for new dashboard
            Machinata.apiCall("/api/csam/portal/cockpit/create-dashboard") //TODO config
                .data({ name: val })
                .success(function (message) {
                    Machinata.goToPage("/portal/cockpit/" + message.data["short-url"]);
                })
                .send();
        })
        .show();
};

Machinata.Dashboard.addExistingDashboardItemToDashboard = function (config, source, baseURL) {
    Machinata.debug("Machinata.Dashboard.addExistingDashboardItemToDashboard:");

    // Create converted dashboard item...
    var newConfig = Machinata.Data.deepClone(config);

    // Shift title/subtitle down one level to include source
    if (source != null && source != "") {
        // We have a source
        newConfig.title = source;
        newConfig.subtitle = config.title;
        if (config.subtitle != null) newConfig.subtitle += ", " + config.subtitle;
    } else {
        // No source
        newConfig.title = config.title;
        newConfig.subtitle = config.subtitle;
    }

    // Alter URL to make absolute
    if (newConfig.settings != null && newConfig.settings["URL"] != null && newConfig.settings["URL"] != "") {
        var url = newConfig.settings["URL"];
        url = url.replace("{current-page}", baseURL);
        if (url[0] != "/" && !url.startsWith("http")) url = baseURL + url;
        newConfig.settings["URL"] = url;
    }

    // Set the default supported sizes
    if (newConfig["supported-sizes-are-final"] != true) newConfig["supported-sizes"] = "large-square,large-half,small-square"; // default
    if (newConfig["size"] == "large-wide") newConfig["supported-sizes"] += ",large-wide";

    // Let the implementation alter the new config
    var impl = Machinata.Dashboard.Widgets.Types[config.type];
    if (impl["supportedSizes"] != null) {
        var customSizes = impl["supportedSizes"](config);
        if (customSizes != null) {
            newConfig["supported-sizes"] = customSizes;
        }
    }

    // Debug
    Machinata.debug("  title: " + config.title + " >> " + newConfig.title);
    Machinata.debug("  subtitle: " + config.subtitle + " >> " + newConfig.subtitle);
    Machinata.debug("  baseURL: " + baseURL);
    Machinata.debug("  newConfig:");
    Machinata.debug(newConfig);


    // Do API call
    Machinata.apiCall("/api/csam/portal/cockpit/dashboards") //TODO config
        .data(config)
        .success(function (message) {
            var itemTitle = config.title;
            if (itemTitle == null) itemTitle = "this widget";
            var opts = {};
            var dashboardIdToPublicId = {};
            $.each(message.data, function (index, item) {
                opts[item["short-url"]] = item.name;
                dashboardIdToPublicId[item["short-url"]] = item["public-id"];
            });
            opts["-"] = "-";
            opts["new"] = "Create new dashboard";
            Machinata.optionsDialog('Add to dashboard', 'Which dashboard would you like to add ' + itemTitle + ' to?', opts)
                .okay(function (id, val) {
                    
                    if (id == "new") {
                        // New dashboard!
                        Machinata.inputDialog("New dashboard", "Please enter the name of the new dashboard.", "", "Dashboard name")
                            .input(function (val) {
                                if (val == null || val == "") return;
                                // Do API call for new dashboard
                                Machinata.apiCall("/api/csam/portal/cockpit/create-dashboard") //TODO config
                                    .data({ name: val })
                                    .success(function (message) {
                                        Machinata.Dashboard._addExistingDashboardItemToDashboardViaAPICall(
                                            newConfig,
                                            message.data["public-id"],
                                            "/portal/cockpit/" + message.data["short-url"]
                                        );
                                    })
                                    .send();
                            })
                            .show();

                    } else {

                        // Existing dashboard!
                        var publicId = dashboardIdToPublicId[id];
                        Machinata.Dashboard._addExistingDashboardItemToDashboardViaAPICall(
                            newConfig,
                            publicId,
                            "/portal/cockpit/" + id
                        );
                    }
                })
                .show();
        })
        .send();
};

Machinata.Dashboard._addExistingDashboardItemToDashboardViaAPICall = function (config, publicId, successLink) {
    // Convert settings for API call
    config["settings"] = JSON.stringify(config["settings"]);
    // Do API call
    Machinata.apiCall("/api/csam/portal/cockpit/dashboard/" + publicId + "/add-item")
        .data(config)
        .success(function (message) {
            Machinata.yesNoDialog('Item added', 'Would you like to go your dashboard now?')
                .okay(function () {
                    Machinata.goToPage(successLink);
                }).show();
        })
        .send();
};