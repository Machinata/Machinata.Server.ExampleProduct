

/// <summary>
/// A standard multi-line text.
/// </summary>
Machinata.Dashboard.Widgets.Types["text"] = {
    init: function (dashboard, elem, config) {
        // Tools
        if (config.settings["URL"] != null) Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        // Content
        

        // Init
        var textElem = $("<div class='text ui-column-content'></div>");
        if (config.settings["columns"] == 1) textElem.addClass("option-one-column");
        if (config.settings["columns"] == 2) textElem.addClass("option-two-column");
        if (config.settings["columns"] == 3) textElem.addClass("option-three-column");
        if (config.settings["columns"] == 4) textElem.addClass("option-four-column");


        if (config.settings["title"] != null) {
            var pElem = $("<p class='title'></p>").text(config.settings["title"]);
            elem.data("contents").append(pElem);
        }

        // Insert each paragraph
        $.each(config.settings["paragraphs"], function (index, paragraph) {
            // Title
            if (paragraph.title != null) textElem.prepend($("<p class='title avoid-column-break'></p>").text(paragraph.title));
            // P with HTML and subtitle
            var pElem = $("<p class=''></p>").text(paragraph.text);
            if (config.settings["balance"] == false) pElem.addClass("avoid-column-break");
            if (paragraph.subtitle != null) pElem.prepend($("<span class='subtitle'></span><br/>").text(paragraph.subtitle));
            if (paragraph.html != null) pElem.append($("<span class='html'></span>").html(paragraph.html));
            textElem.append(pElem);
        });
        elem.data("contents").append(textElem);
        dashboard.packery();
    }
};
