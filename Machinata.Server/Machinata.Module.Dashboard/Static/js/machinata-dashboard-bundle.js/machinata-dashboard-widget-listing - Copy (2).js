

/// <summary>
/// Listing widget with items.
/// </summary>
Machinata.Dashboard.Widgets.Types["listing"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        // Content
        elem.addClass("type-listing");
        elem.addClass("option-bottom-fader");
        var listing = $("<div class='ui-inbox'></div>");
        var itemsConfig = config.settings["Items"];
        for (var i = 0; i < itemsConfig.length; i++) {
            // Init
            var itemConfig = itemsConfig[i];
            var summarizedText = Machinata.String.createSummarizedText(itemConfig["Text"], 60);
            var itemURL = itemConfig["URL"];
            itemURL = itemURL.replace("{parent-url}", config.settings["URL"]);
            itemURL = itemURL.replace("{current-page}", Machinata.getPagePath());
            // Build elements
            var itemElem = $("<a class='ui-inbox-item'></a>");
            itemElem.attr("href", itemURL);
            var iconElem = $("<div class='icon text-icon'></div>").text(itemConfig["Number"]);
            var textElem = $("<div class='text'></div>");
            textElem.append($("<div class='subject'></div>").text(itemConfig["Title"]));
            textElem.append($("<div class='message'></div>").text(summarizedText));
            // Trend?
            if (itemConfig["Trend"] == "up") {
                textElem.find(".message").append($("<span class='trend-icon icon-csam-fn-small-trend-up-white'></span>"));
            }
            if (itemConfig["Trend"] == "down") {
                textElem.find(".message").append($("<span class='trend-icon icon-csam-fn-small-trend-down-white'></span>"));
            }
            // Color?
            if (itemConfig["Color"] != null) {
                textElem.find(".message").addClass("ui-" + itemConfig["Color"]+"-text");
            }
            // Compile DOM
            itemElem.append(iconElem);
            itemElem.append(textElem);
            itemElem.append($("<div class='clear'></div>"));
            itemElem.appendTo(listing);
        }
        listing.appendTo(elem.data("contents"));
    }
};
