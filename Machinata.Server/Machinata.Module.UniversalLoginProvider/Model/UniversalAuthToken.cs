using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.UniversalLoginProvider.Model {

    [Serializable()]
    [ModelClass]
    [Table("UniversalLoginProviderUniversalAuthTokens")]
    public partial class UniversalAuthToken : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public UniversalAuthToken() {
            // Parameterless constructure is required for reflection...

        }
        
        // Subscribe to User changes
        static UniversalAuthToken() {
            _logger.Info("User.AuthorizationChanged += new User.AuthorizationChangedDelegate(HandleUserAuthorizationChanged);");
            User.PropertiesChanged += new User.PropertiesChangedDelegate(HandleUserPropertiesChanged);
            User.AuthorizationChanged += new User.AuthorizationChangedDelegate(HandleUserAuthorizationChanged);
        }



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [ForeignKey("UserId")]
        [FormBuilder]
        public User User { get; set; } 

        [Column]
        [FormBuilder]
        public int UserId { get; set; }

        [Column]
        [FormBuilder]
        public AuthToken ProviderAuthToken { get; set; } 

        [Column]
        [FormBuilder]
        public string ProviderTokenHash { get; set; }

        [Column]
        [FormBuilder]
        public string SyncKey { get; set; }

        [Column]
        [FormBuilder]
        public string SourceServer { get; set; }
        
        [Column]
        [FormBuilder]
        public string SourceIP { get; set; } 

        [Column]
        [FormBuilder]
        public string SourceTokenHash { get; set; }

        [Column]
        [FormBuilder]
        public string SourceTokenIP { get; set; } 
        
        [Column]
        [FormBuilder]
        public string SourceTokenUserAgent { get; set; } 

        [Column]
        [FormBuilder]
        public DateTime Expires { get; set; } 

        [Column]
        [FormBuilder]
        public bool Valid { get; set; } 

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static void UpdateAllSourcesForUserChange(ModelContext db, User user) {
            // Find all universalauthtokens for this user that are still valid
            var now = DateTime.UtcNow;
            var uaTokens = db.UniversalAuthTokens()
                .Include(nameof(UniversalAuthToken.ProviderAuthToken))
                .Where(e => e.UserId == user.Id)
                .Where(e => e.Expires > now)
                .ToList();

            // Group by source servers
            foreach(var sourceServerGroup in uaTokens.GroupBy(e => e.SourceServer)) {
                try {

                    //TODO: check if any original master tokens have been disabled valid==false
                    string syncKey = null;
                    List<object> tokenData = new List<object>();
                    foreach(var uaToken in sourceServerGroup) {
                        if (uaToken.ProviderAuthToken.Valid == false) uaToken.Valid = false;

                        // Note: never reveal our provider hash/id/etc
                        tokenData.Add(new {
                            Valid = uaToken.Valid,
                            SourceTokenHash = uaToken.SourceTokenHash,
                            Expires = uaToken.Expires
                        });
                        if (syncKey == null) syncKey = uaToken.SyncKey;
                    }

                    // Compile a datastructure for this server

                    // Make the api call
                    var endpoint = sourceServerGroup.Key;
                    if (Core.Config.IsTestEnvironment) endpoint = endpoint.Replace("https://", "http://");
                    var apiCall = new Core.API.APICall("/api/account/request-sync", endpoint);
                    var syncJSON = user.CreateUniversalLoginPropertiesJSONForSync(db);
                    //TODO: add parameters for sync


                    //TODO: implement the recieving api call
                    
                    apiCall.WithParameter("source", Core.Config.ServerURL);
                    apiCall.WithParameter("username", user.Username);
                    apiCall.WithParameter("sync-data", Core.JSON.Serialize(syncJSON));
                    apiCall.WithParameter("token-data", Core.JSON.Serialize(tokenData));
                    apiCall.WithParameter("synckey", syncKey);

                    apiCall.Send();

                    // Call was success - now we sync anything that might need to be changed locally
                    //TODO: is there anything?
                    
                } catch(Exception e) {
                    _logger.Warn(e, "Could not update source server " + sourceServerGroup.Key + " with sync data: "+e.Message);
                }
            }

            // Commit to db
            db.SaveChanges();
        }

        #endregion

        #region Override Methods //////////////////////////////////////////////////////////////////
        
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion
        
        #region Event Handlers ///////////////////////////////////////////////////////////////////
        
        private static void HandleUserPropertiesChanged(User user) {
            UpdateAllSourcesForUserChange(user.Context, user);
        }

        private static void HandleUserAuthorizationChanged(User user) {
            UpdateAllSourcesForUserChange(user.Context, user);
        }

        #endregion

      
    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextUniversalAuthTokenExtenions {
        public static DbSet<UniversalAuthToken> UniversalAuthTokens(this Core.Model.ModelContext context) {
            return context.Set<UniversalAuthToken>();
        }
        public static IQueryable<UniversalAuthToken> UniversalAuthTokens(this Core.Model.User user) {
            return user.Context.UniversalAuthTokens().Where(o => o.User.Id == user.Id);
        }
    }

    #endregion
}
