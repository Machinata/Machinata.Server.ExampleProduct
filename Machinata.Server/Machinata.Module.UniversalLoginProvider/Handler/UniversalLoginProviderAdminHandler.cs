
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core;

namespace Machinata.Module.Admin.Handler {


    public class UniversalLoginProviderAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("universal-login");
        }

        #endregion
        /*
        [RequestHandler("/admin/tickets")]
        public void Default() {
            // Paginate and insert list
            var entities = this.DB.SupportTickets().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/tickets/ticket/{entity.public-id}", true);

            // Navigation
            this.Navigation.Add("tickets","{text.tickets}");
        }
        
        [RequestHandler("/admin/tickets/create")]
        public void Create() {
            this.RequireWriteARN();

            this.Template.InsertForm(
                variableName: "form",
                entity: new SupportTicket(),
                form: new FormBuilder(Forms.Admin.CREATE)
                    .Custom("Message","message","text", null)
                    .Button("send",Forms.Button.SEND),
                apiCall: "/api/tickets/create",
                onSuccess: "/admin/tickets/ticket/{ticket.public-id}"
            );

            // Navigation
            this.Navigation.Add("tickets","{text.tickets}");
            this.Navigation.Add("create","{text.create}");
        }

        [RequestHandler("/admin/tickets/create-test")]
        public void CreateTest() {
            this.RequireWriteARN();

            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/tickets/ticket")]
        public void SupportTicketByGuid() {
            // Guid
            var guid = this.Params.String("guid");

            // Grab Ticket 
            var ticket = this.DB.SupportTickets().GetByGuid(guid);

            // Process
            ProcessTicket(ticket);

        }

        [RequestHandler("/admin/tickets/ticket/{publicId}")]
        public void SupportTicket(string publicId) {
            // Grab Ticket 
            var ticket = DB.SupportTickets().GetByPublicId(publicId);
            
            // Process
            ProcessTicket(ticket);
        }

        private void ProcessTicket(SupportTicket ticket) {
            ticket.LoadFirstLevelNavigationReferences();

            // Ticket
            this.Template.InsertVariables("support-ticket", ticket);
            var form = new FormBuilder(Forms.Admin.VIEW);
            if (Core.JSON.GetDepth(ticket.Infos?.Data) > 3) {
                form.Exclude(nameof(ticket.Infos));
            }
            this.Template.InsertPropertyList("ticket", ticket, form, loadFirstLevelReferences: true);

            // Templates for Extensions
            var extensionData = ticket.LoadExtensionData();
            var extensions = extensionData.Item2.Select(ed => extensionData.Item1.GetExtensionTemplate(this.Template, ed.Key, ed.Value, ticket));
            this.Template.InsertTemplates("extension", extensions);


            // Messages
            var messages = DB.SupportMessages().Include("User").Where(st => st.TicketId == ticket.Id).OrderByDescending(m => m.Id);
            messages = this.Template.Paginate(messages, this);
            this.Template.InsertEntityList("messages", messages, new FormBuilder(Forms.Admin.LISTING).Include("Created"), "/admin/tickets/ticket/" + ticket.PublicId + "/message/{entity.public-id}");

            // Customer Support Response
            ticket.Status = Ticketing.Model.SupportTicket.TicketStatus.Processed; // preset to processed
            this.Template.InsertForm(
                variableName: "admin-reponse",
                entity: ticket,
                form: new FormBuilder("admin-response")
                    .Custom("Message", "message", "text", null, true),
                apiCall: $"/api/admin/tickets/{ticket.PublicId}/create-message",
                onSuccess: "{page.navigation.current-path}");

            // Test Customer Response
            this.Template.InsertForm(
               variableName: "customer-response",
               entity: new SupportMessage(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: $"/api/tickets/{ticket.PublicId}/create-message",
               onSuccess: "{page.navigation.current-path}");

            // Response enabled?
            this.Template.InsertVariable("response-enabled", Module.Ticketing.Config.SupportTicketCustomerSupportResponseEnabled);

            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            string title = ticket.GetTicketTitle();
            this.Navigation.Add($"ticket/{ticket.PublicId}", "{text.ticket}: " + title);
        }



        private static string GetMessageTitle(SupportMessage message) {
            var title = message.PublicId;
            if (!string.IsNullOrEmpty(message.Message )) {
                title = Core.Util.String.CreateSummarizedText(message.Message, 20,true,true);
            }
            return title;
        }

        [RequestHandler("/admin/tickets/ticket/{ticketId}/message/{publicId}")]
        public void SupportMessage(string ticketId,string publicId) {
            var message = DB.SupportMessages().GetByPublicId(publicId);
            message.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", message);
            this.Template.InsertPropertyList("entity", message, new FormBuilder(Forms.Admin.VIEW));
            message.Include(nameof(message.Ticket));

            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            this.Navigation.Add($"ticket/{ticketId}", message.Ticket.GetTicketTitle());
            var title = GetMessageTitle(message);
            this.Navigation.Add($"message/{publicId}", "{text.message}: " + title);
        }

        [RequestHandler("/admin/tickets/ticket/{publicId}/edit")]
        public void TicketEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.SupportTickets().GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/tickets/ticket/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );

            entity.LoadFirstLevelNavigationReferences();
          
            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            string title = entity.GetTicketTitle();
            this.Navigation.Add($"ticket/{publicId}", title);
           
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/tickets/ticket/{publicId}/edit-extension/{extensionId}")]
        public void ExtensionEdit(string publicId, string extensionId) {
            this.RequireWriteARN();

            var entity = DB.SupportTickets().GetByPublicId(publicId);
            var extensionData = entity.LoadExtensionData();
            var extensionEntity = extensionData.Item2[extensionId];
            extensionData.Item1.InsertExtensionEdit(this.Template, extensionId, entity, extensionEntity);
            entity.LoadFirstLevelNavigationReferences();

            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            string title = entity.GetTicketTitle();
            this.Navigation.Add($"ticket/{publicId}", title);

            this.Navigation.Add($"edit-extension/{extensionId}", "{text.edit}");
        }

        [RequestHandler("/admin/tickets/ticket/{ticketId}/message/{publicId}/edit")]
        public void SupportMessageEdit(string ticketId, string publicId) {
            var ticket = DB.SupportTickets().Include("Messages").GetByPublicId(ticketId);
            var entity = ticket.Messages.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/tickets/message/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );


            // Navigation
            this.Navigation.Add("tickets", "{text.tickets}");
            this.Navigation.Add($"ticket/{ticketId}", entity.Ticket.GetTicketTitle());
            var title = GetMessageTitle(entity);
            this.Navigation.Add($"message/{publicId}", "{text.message}: " + title);
            this.Navigation.Add($"edit", "{text.edit}");
        }*/

    }
}
