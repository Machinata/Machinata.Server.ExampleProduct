
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using Machinata.Module.UniversalLoginProvider.Model;

namespace Machinata.Module.UniversalLoginProvider.Handler {


    public class UniversalLoginProviderApiHandler : APIHandler {

        public override string PackageName
        {
            get
            {
                return "Machinata.Module.UniversalLoginProvider";
            }
        }



        [RequestHandler("/api/universal-login/get-sync-data", AccessPolicy.PUBLIC_ARN)]
        public void Sync() {
            if (!Core.Config.IsTestEnvironment) throw new NotImplementedException();
            
            // Init
            string username = this.Params.String("username");
            var user = this.DB.Users().Include(nameof(User.AuthTokens)).GetByUsername(username);
            
            // Validate the authenticity of the request


            //TODO: universalauthtokens
            var syncData = user.CreateUniversalLoginPropertiesJSONForSync(this.DB);

            // Success
            SendAPIMessage("get-sync-data-success", new {
                SyncData = syncData
            });
        }

        [RequestHandler("/api/universal-login/login", AccessPolicy.PUBLIC_ARN)]
        public void Login() {

            //TODO: validate we are on HTTPS
            // We never expose this API on HTTP...
            
            string username = this.Params.String("username");
            string password = this.Params.String("password");
            string duration = this.Params.String("duration");
            string ip = this.Params.String("ip"); // this is the client ip (trying to login on source)
            string useragent = this.Params.String("useragent"); // this is the client UA (trying to login on source)
            string source = this.Params.String("source");
            string synckey = this.Params.String("synckey");

            //TODO: validate all fields...

            //TODO: validate the domain is acceppted

            // Validate the duration is accepted
            // We don't accept year, this way the maximum time a login lies around unsynced is reduced
            if(duration != "month" && duration != "day" && duration != "hour") {
                throw new BackendException("invalid-duration","The duration provided is not valid for a universal login.");
            }

            // Create login locally
            // Note: we never! reveal the token hash on the provider!
            var authToken = Core.Model.User.LoginUserWithUsernameAndPasswordOnLocalMachine(this.DB, this.Context, username, password, duration);

            // If we are here - that means that we successfully logged in with the remote credentials on the local machine
            
            // Now before we return, we do some bookkeeping for this universal login
            var sourceTokenHash = AuthToken.GenerateHash(); // This will be the defined hash for the local auth token on the source
            var universalToken = new UniversalAuthToken();
            universalToken.Expires = authToken.Expires;
            universalToken.User = authToken.User;
            universalToken.SyncKey = synckey;
            universalToken.ProviderAuthToken = authToken;
            universalToken.ProviderTokenHash = authToken.Hash;
            universalToken.SourceServer = source;
            universalToken.SourceIP = Core.Util.HTTP.GetRemoteIP(this.Context);
            universalToken.SourceTokenHash = sourceTokenHash;
            universalToken.SourceTokenIP = ip;
            universalToken.SourceTokenUserAgent = useragent;
            universalToken.Valid = true;
            this.DB.UniversalAuthTokens().Add(universalToken);
            this.DB.SaveChanges();

            // Create a return object with all the properties needed for the login and sync
            var propertiesJSON = authToken.User.CreateUniversalLoginPropertiesJSONForSync(this.DB);
            propertiesJSON["auth-token"] = Core.JSON.FromObject(new {
                Hash = sourceTokenHash,  // this is the hash for the source (caller) to use
                Created = authToken.Created,
                Expires = authToken.Expires,
                Duration = duration
            });

            // Success
            SendAPIMessage("login-success", propertiesJSON);
        }
        




    }
}
