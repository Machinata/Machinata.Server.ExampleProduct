
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Ticketing.Handler {


    public class TemplatesAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Templates.Model.Template> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("templates");
        }

        #endregion


        [RequestHandler("/api/admin/templates/template/create")]
        public void Create() {
            CRUDCreate("entity");
        }

        [RequestHandler("/api/admin/templates/template/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/templates/template/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        
        

        
    }
}
