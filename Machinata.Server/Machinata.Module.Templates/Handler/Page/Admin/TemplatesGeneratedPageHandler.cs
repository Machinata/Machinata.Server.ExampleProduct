
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core;
using Machinata.Module.Templates.Model;

namespace Machinata.Module.Admin.Handler {


    public class TemplatesGeneratedPageHandler : AdminPageTemplateHandler {
        
        

        

        
        [RequestHandler("/admin/templates/generated/{guid}", AccessPolicy.PUBLIC_ARN)]
        public void Generated(string guid) {

            //TODO: verify access

            // Init
            var entity = DB.Templates()
                .Include(nameof(Templates.Model.Template.Business))
                .Include(nameof(Templates.Model.Template.User))
                .SingleOrDefault(e => e.GUID == guid);
            var impl = entity.GetImplementation();
            

            // Implementation template
            this.Template.ChangeTemplate("generated." + entity.TemplateType.ReplaceSuffix("TemplateImplementation","").ToLower());
            this.Template.InsertVariables("entity", entity);
            impl.InsertAssetGenerationVariables(this, this.Template);
            

        }

    }
}
