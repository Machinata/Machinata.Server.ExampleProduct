using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Templates.Logic {
    
    public class HTML2PDFTemplateImplementation : TemplateImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public HTML2PDFTemplateImplementation(Model.Template template) : base(template) {
            
        }

        #endregion

        
        public override void InsertAssetGenerationVariables(Core.Handler.Handler handler, PageTemplate template) {
            

            // Insert html file
            if (!string.IsNullOrEmpty(this.Template.TemplateFile)) {
                var filepath = Core.Data.DataCenter.DownloadFile(handler.DB, this.Template.TemplateFile);
                var fileContents = File.ReadAllText(filepath);
                template.InsertVariableUnsafe("template-file-contents", fileContents);
            }

            base.InsertAssetGenerationVariables(handler, template);
        }

        public override TemplateOutputType GetOutputType() {
            return TemplateOutputType.PDF;
        }

        public override Dictionary<string, string> GetDefaultSettings() {
            var ret = new Dictionary<string, string>();
            return ret;
        }
        
    }
    

    
}
