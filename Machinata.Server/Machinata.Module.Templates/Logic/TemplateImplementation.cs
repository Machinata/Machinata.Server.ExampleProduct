using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Templates;

namespace Machinata.Module.Templates.Logic {
    
    public abstract class TemplateImplementation {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        private Model.Template _template;

        public enum TemplateOutputType {
            HTML,
            PDF
        }
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public TemplateImplementation(Model.Template template) {
            _template = template;
        }

        #endregion

        public Model.Template Template {
            get {
                return _template;
            }
        }
        
        public abstract Dictionary<string, string> GetDefaultSettings();

        public abstract TemplateOutputType GetOutputType();

        public virtual void InsertAssetGenerationVariables(Core.Handler.Handler handler, PageTemplate template) {
            // Variables
            if (this.Template.TemplateVariables != null) {
                foreach (var variable in this.Template.TemplateVariables?.Split(',')) {
                    var segs = variable.Split('=');
                    string defaultValue = null;
                    if (segs.Length > 1) defaultValue = segs[1];
                    var variableName = segs[0].ToLower();
                    var variableValue = handler.Params.String(variableName);
                    template.InsertVariable("variable." + variableName, variableValue);
                    template.InsertVariableURISafe("variable." + variableName+"-uri-safe", variableValue);
                }
            }
        }
        

        public virtual string GetInstructions() {
            return "";
        }

        public virtual string GetNotes() {
            return "";
        }

        public virtual void LoadDefaultSettingsAndMerge() {
            // Make sure all defaults are added
            var defaults = GetDefaultSettings();
            foreach(var keyval in defaults) {
                if(!this.Template.TemplateSettings.Keys.Contains(keyval.Key)) {
                    this.Template.TemplateSettings[keyval.Key] = keyval.Value;
                }
            }
            // Remove any not of this type...
            var toRemove = new List<string>();
            foreach(var key in this.Template.TemplateSettings.Keys) {
                if(!defaults.Keys.Contains(key)) {
                    toRemove.Add(key);
                }
            }
            foreach (var key in toRemove) this.Template.TemplateSettings.Delete(key);
        }


        public static List<Type> GetImplementations() {
            return Core.Reflection.Types.GetMachinataTypes(typeof(TemplateImplementation))
                .Where(e => e.IsAbstract == false)
                .ToList();
        }
        
    }
    

    
}
