using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Machinata.Core.Util;

namespace Machinata.Module.Templates.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("TemplatesTemplate")]
    public partial class Template : ModelObject, IEnabledModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public Template() {
            this.TemplateSettings = new Properties();
            if (string.IsNullOrEmpty(this.GUID)) {
                this.GUID = Guid.NewGuid().ToString();
            }
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Title { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Enabled { get; set; } 
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string GUID { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        [MaxLength(256)]
        public string TemplateType { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Properties TemplateSettings { get; set; } 

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.Upload)]
        public string TemplateFile { get; set; } 

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string TemplateVariables { get; set; } 

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Business Business { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public User User { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        public string PublicURL {
            get {
                return Core.Config.PublicURL + "/admin/templates/template/" + this.PublicId + "/generate?guid=" + this.GUID;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        public Logic.TemplateImplementation GetImplementation() {
            if (this.TemplateType == null || this.TemplateType == "") return null;
            var type = Logic.TemplateImplementation.GetImplementations().SingleOrDefault(e => e.Name == this.TemplateType);
            var obj = Core.Reflection.Types.CreateInstance<Logic.TemplateImplementation>(type, this);
            return obj;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////
        
        public bool Published {
            get {
                return this.Enabled;
            }
        }

        public override CardBuilder VisualCard() {
            var card = new Core.Cards.CardBuilder(this)
                  .Title(this.Title)
                  .Subtitle(this.Business.Name)
                  .Sub(this.TemplateType?.ReplaceSuffix("TemplateImplementation",""))
                  .Icon("media-stop-outline");
            return card;
        }

        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSignExtenions {

        public static DbSet<Template> Templates(this Core.Model.ModelContext context) {
            return context.Set<Template>();
        }
    }

    #endregion
}
