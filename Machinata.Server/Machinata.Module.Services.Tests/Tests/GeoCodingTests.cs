using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Machinata.Module.Services.Tests {


    [TestClass]
    public class GeoCodingTests {

        private void _validateData(
                Machinata.Module.Services.Providers.GeoCodingProvider provider,
                string address,
                double expectedLat,
                double expectedLon
            ) {
            var data = provider.DataForAddress(address);
            Assert.AreEqual(data.LatAsDouble(),expectedLat,0.001);
            Assert.AreEqual(data.LonAsDouble(),expectedLon,0.001);
        }

        // dont use credits in (CI)
        [Ignore]
        public void Services_GeoCoding_Default_Raeffelstrasse() {
            _validateData(
                Machinata.Module.Services.GeoCoding.Default, 
                "Raeffelstrasse 24, 8045 Zurich, Switzerland", 
                47.363552, // returns 47.3636813 
                8.512972); // return 8.5129903 (delta 0.0001)
        }

        // dont use credits in (CI)
        [Ignore]
        public void Services_GeoCoding_GoogleGeoCoding_Raeffelstrasse() {
             _validateData(
                new Machinata.Module.Services.Providers.GoogleGeoCodingProvider(), 
                "Raeffelstrasse 24, 8045 Zurich, Switzerland", 
                47.363552, // returns 47.3636813 
                8.512972); // return 8.5129903 (delta 0.0001)
        }
        
    }

}
