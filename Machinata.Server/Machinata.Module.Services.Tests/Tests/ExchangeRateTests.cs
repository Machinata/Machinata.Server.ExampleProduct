using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Machinata.Module.Services.Tests {


    [TestClass]
    public class ExchangeRateTests {

        private void _validateData(Machinata.Module.Services.Providers.ExchangeRateProvider provider, string currency) {
            var data = provider.DataForCurrency(currency);
            Debug.WriteLine(data.Currency);
            foreach(var rate in data.Rates) {
                Debug.WriteLine(rate.Key + " : " + rate.Value);
            }
            Assert.AreEqual(data.Currency,currency);
            Assert.IsTrue(data.Rates.Count > 0);
        }
        
        //[TestMethod]
        public void Services_ExchangeRate_Default_USD() {
            _validateData(Machinata.Module.Services.ExchangeRate.Default, "USD");
        }

        //[TestMethod]
        public void Services_ExchangeRate_Default_CHF() {
            _validateData(Machinata.Module.Services.ExchangeRate.Default, "CHF");
        }

        //[TestMethod]
        public void Services_ExchangeRate_Default_EUR() {
            _validateData(Machinata.Module.Services.ExchangeRate.Default, "EUR");
        }
        
        //[TestMethod]
        public void Services_ExchangeRate_Fixer_USD() {
            _validateData(new Machinata.Module.Services.Providers.FixerExchangeRateProvider(), "USD");
        }

        [TestMethod]
        public void Services_ExchangeRate_Fixer_CHF() {
            _validateData(new Machinata.Module.Services.Providers.FixerExchangeRateProvider(), "CHF");
        }

        //[TestMethod]
        public void Services_ExchangeRate_Fixer_EUR() {
            _validateData(new Machinata.Module.Services.Providers.FixerExchangeRateProvider(), "EUR");
        }
    }

}
