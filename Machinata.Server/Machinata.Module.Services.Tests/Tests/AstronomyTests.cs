using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Tests.Tests {


    
    [TestClass]
    public class AstronomyTests {
        
        //[TestMethod]
        // TODO: not working at the moment, seems to be off by 1 hour!
        public void Services_Astronmy_Sun_Sunrise() {
            var date = new DateTime(2018, 06, 11, 0, 0, 0, DateTimeKind.Utc);
            var latlon = "47.3768866,8.541694"; // Zurich
            var ret = Module.Services.Astronomy.Sun.CalculateSunriseUTC(date, latlon);
            Assert.AreEqual(ret.Date, date.Date);
            Assert.AreEqual(ret.Hour, 5);
            Assert.AreEqual(ret.Minute, 29, 10);
        }

        //[TestMethod]
        // TODO: not working at the moment, seems to be off by 1 hour!
        public void Services_Astronmy_Sun_Sunset() {
            var date = new DateTime(2018, 06, 11);
            var latlon = "47.3768866,8.541694"; // Zurich
            var ret = Module.Services.Astronomy.Sun.CalculateSunsetUTC(date, latlon);
            Assert.AreEqual(ret.Date, date.Date);
            Assert.AreEqual(ret.Hour, 20);
            Assert.AreEqual(ret.Minute, 30);
        }

    }
}
