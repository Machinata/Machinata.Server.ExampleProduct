using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Machinata.Module.Services.Tests {


    [TestClass]
    public class GeoIPTests {

        private void _validateData(
                Machinata.Module.Services.Providers.GeoIPProvider provider,
                string ip,
                string expectedCity
            ) {
            var data = provider.DataForIP(ip);
            Assert.AreEqual(data.Address.City,expectedCity);
        }

        // dont use credits in (CI)
        public void Services_GeoIP_Default_Zurich() {
            _validateData(Machinata.Module.Services.GeoIP.Default, "83.150.2.227", "Zurich");
        }

        // dont use credits in (CI)
        public void Services_GeoIP_FreeGeoIPNet_Zurich() {
            _validateData(new Machinata.Module.Services.Providers.FreeGeoIPNetGeoIPProvider(), "83.150.2.227", "Zurich");
        }
        
    }

}
