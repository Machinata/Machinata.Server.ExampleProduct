﻿using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Logic {




    public class TestLoader {




        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion


        public static IEnumerable<Test> LoadAllTests(ModelContext db) {
            var result = new List<Test>();
            var providerMethods = Machinata.Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(TestProviderAttribute));
            foreach (var method in providerMethods) {
                var ret = method.Invoke(null, new object[] { db });
                var tests = ret as IEnumerable<Test>;
                int count = 0;
               foreach(var test in tests) {
                    if (string.IsNullOrWhiteSpace(test.Id)) {
                        var info = method.DeclaringType.FullName + "." + method.Name + "-" + count;
                        throw new Exception($"Please define a Id for test: " + info);
                    }
                    //test.Id = method.DeclaringType.FullName + "-" + method.Name + "-" + count;
                    test.ContainingType = method.DeclaringType.FullName;
                    test.TestType = test.GetType().FullName;

                    result.Add(test);
                    count++;
                }
            }

            // Validate Unique Global ID
            var dinstinct = result.GroupBy(d => d.GlobalId);
            var duplicates = dinstinct.Where(d => d.Count() > 1).SelectMany(d => d.AsEnumerable());
            if (duplicates.Any()) {
                _logger.Error("Found duplicated Tests ID (+Category+Subcategory): ");
                foreach (var duplicate in duplicates) {
                    _logger.Error($"{duplicate.Name}: ID={duplicate.Id}, Category={duplicate.Category},  SubCategory={duplicate.SubCategory}, Class={duplicate.ContainingType} ");
                }
                throw new BackendException("testing-error", "Found duplicated Test IDs(+Category + Subcategory), check log for details");
            }

            return result;

        }
    }
}
