﻿using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Testing.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Module.Testing.Logic {
    public class ErrorHandler {

        public static ExceptionResult WrapException(HttpContext context, Exception e) {

            var result = new ExceptionResult();

            var errorLog = Core.EmailLogger.CompileFullErrorReport(context, null, e);

            result.StackTrace = errorLog;
            result.Type = e.GetType().ToString();
          

            return result;
        }



    }

    public class ExceptionResult {
        public string StackTrace { get; set; }
        public string Type { get; set; }

    }
}
