﻿using Machinata.Core.Builder;
using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Logic {




    public class TestExecution {




        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion

        public static void CompareAndAutoAccept(ModelContext db, User user, TestResult result, TestResult lastAccepted, Test test) {
            var diffResult = TestDiffer.Diff(db, result, lastAccepted);
            result.DiffText = diffResult.TextDiff;
            if (result.DataResult != null) {

                ContentFile contentFile = null;
                ContentFile contentFileImageB = null;

                // Diff Image
                try {
                    var fileName = Guid.NewGuid() + ".png";
                    contentFile = DataCenter.UploadFile(fileName, diffResult.ImageDiff, "VisualTests", null, user);
                    db.ContentFiles().Add(contentFile);
                } catch (Exception e) {
                    throw new BackendException("compare-error-upload", $"Failed to upload diff image", e);
                }


                // Image B
                try {
                    var imageB = File.ReadAllBytes(diffResult.ImageB);
                    var fileNameImageB = Path.GetFileName(diffResult.ImageB);
                    contentFileImageB = DataCenter.UploadFile(fileNameImageB, imageB, "VisualTests", null, user);
                    db.ContentFiles().Add(contentFileImageB);
                } catch (Exception e) {
                    throw new BackendException("compare-error-upload", $"Failed to upload image B", e);
                }


                db.SaveChanges();
                result.DiffData = contentFile.ContentURL;
                result.ComparisonData = contentFileImageB.ContentURL;

                result.Properties[TestResult.PROPERTIES_IMAGE_DIFFERENT_COLOR] = diffResult.DifferentColor;
                result.Properties[TestResult.PROPERTIES_IMAGE_DIFFERENT_PIXELS] = diffResult.DifferentPixels;
            }

            // Status
            result.ResultStatus = diffResult.IsDifferent ? ResultStatuses.Changed : ResultStatuses.Same;

            // AutoAccept Same
            if (diffResult.IsDifferent == false) {
                result.Accepted = true;
            }

            // AutoAccept Different if IgoreDiff
            if (diffResult.IsDifferent == true && test.IgnoreDiff == true) {
                result.Accepted = true;
            }

            result.Properties[TestResult.PROPERTIES_AUTO_COMPARED_VERSION] = lastAccepted.BuildVersion;
            result.Properties[TestResult.PROPERTIES_AUTO_COMPARED_BUILDGUID] = lastAccepted.BuildGUID;
            result.Properties[TestResult.PROPERTIES_AUTO_COMPARED_ENTITY] = lastAccepted.PublicId;
        }

        public static TestResult IntitializeTestResult(string currentBuildVersion, string currentBuildGUID, DateTime currentBuildDate, Test test) {
            var result = new TestResult();
            result.TestID = test.Id;
            result.Name = test.Name;
            result.Description = test.Description;
            result.BuildVersion = currentBuildVersion;
            result.BuildGUID = currentBuildGUID;
            result.BuildDate = currentBuildDate;
            result.ContainingClass = test.ContainingType;
            result.TestClass = test.TestType;
            result.Category = test.Category;
            result.SubCategory = test.SubCategory;
            result.IgnoreDiff = test.IgnoreDiff;
            result.Parameters = test.Parameters;

            var buildInfo = Core.Util.Build.GetProductBuildInfo();
            result.Properties[TestResult.PROPERTIES_BUILD_ARCH] = buildInfo.BuildArch;
            result.Properties[TestResult.PROPERTIES_BUILD_MACHINE] = buildInfo.BuildMachine;
            result.Properties[TestResult.PROPERTIES_BUILD_OS] = buildInfo.BuildOS;
            result.Properties[TestResult.PROPERTIES_BUILD_PRODUCT] = Core.Config.ProjectPackage;
            result.Properties[TestResult.PROPERTIES_BUILD_USER] = buildInfo.BuildUser;
          


            return result;
        }


        /// <summary>
        /// Sends email summary for the given test results
        /// </summary>
        /// <param name="db"></param>
        /// <param name="testResults"></param>
        public static void SendNotificationEmail(ModelContext db, IEnumerable<TestResult> testResults, string buildGUID,  string subject, bool ignoreAccepted = false) {


            var form = new FormBuilder(Forms.Frontend.LISTING);
            var link = "{server-url}/admin/testing/versions/version/" + buildGUID + "/result/{entity.public-id}";
            var template = new EmailTemplate(db, "default/testing-notification");
            template.Subject = subject;

            // Failed Tests
            {
                var tests = testResults.Where(t => t.Status == TestStatuses.Error).AsQueryable();
                template.InsertList("failed-tests", "Failed Tests", tests, form, link:link);
            }

            // Unaccpeted Tests
            {
                var tests = testResults.Where(t => t.Accepted == false && t.Status == TestStatuses.Success).AsQueryable();
                template.InsertList("unaccepted-tests", "Not Accepted Tests", tests, form, link: link);
            }

            // Accepted Tests
            if (ignoreAccepted == false){
                var tests = testResults.Where(t => t.Accepted == true && t.Status == TestStatuses.Success).AsQueryable();
                template.InsertList("accepted-tests", "Accepted Tests", tests, form, link: link);
            } else {
                template.InsertVariable("accepted-tests", string.Empty);
            }

            template.SendNotificationEmail();

        }

    }
}
