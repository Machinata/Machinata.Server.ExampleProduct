﻿using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Logic {




    public class TestDiffer {




        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion


        /// <summary>
        /// Diffs BodyResults and DataResults (image)
        /// </summary>
        /// <param name="testResultA"></param>
        /// <param name="testResultB"></param>
        /// <returns></returns>
        public static DiffResult Diff (ModelContext db, TestResult testResultA, TestResult testResultB) {

            if (testResultA == null || testResultB == null) {
                throw new BackendException("diff-error-init", "Please provide testResultA and testResultB for to diff");
            }

            var result = new DiffResult();

            if (testResultA.BodyResult != null && testResultB.BodyResult != null && testResultA.BodyResult != testResultB.BodyResult) {
                try {
                    result.TextDiff = Core.Util.Diff.GetHTMLDiff(testResultA.BodyResult, testResultB.BodyResult);
                } catch (Exception e) {
                    throw new BackendException("diff-error-html", $"Failed to diff the texts in BodyResult",e);
                }

            }

            if (testResultA.DataResult != null && testResultB.DataResult != null) {

                string contentPathA = null;
                string contentPathB = null;

                // Download file A
                try {
                    contentPathA = DataCenter.DownloadFile(db, testResultA.DataResult);
                }
                catch (Exception e) {
                    throw new BackendException("diff-error-download", $"Failed download file A (DataResult)", e);
                }

                // Download file B
                try {
                    contentPathB = DataCenter.DownloadFile(db, testResultB.DataResult);
                }
                catch (Exception e) {
                    throw new BackendException("diff-error-download", $"Failed download file B (DataResult)", e);
                }

                // Run DIFF
                try {
                    using (var ms = new MemoryStream()) {
                        var imageDiffResult = Core.Imaging.ImageAnalysis.WriteImageDiffToStream(contentPathA, contentPathB, ms);
                        result.DifferentColor = imageDiffResult.DifferentColor;
                        result.DifferentPixels = imageDiffResult.DifferentPixels;
                        result.ImageB = contentPathB;
                        result.ImageDiff = ms.ToArray();
                    }
                } catch (Exception e) {
                    throw new BackendException("diff-error-image", $"Failed to run the image differ", e);
                }

                // Dont delete these files: these are the actual data files no additionalo downloads
                //File.Delete(contentPathA);
                //File.Delete(contentPathB);

            }

            return result;

        }


      
    }

    public class DiffResult {
        public string TextDiff { get; set; }
        //  public Stream ImageDiffStream { get; set; }
        public byte[] ImageDiff { get; set; }
        public string ImageB { get; set; }
        public int? DifferentPixels { get; set; }
        public int? DifferentColor { get; set; }

        public bool IsImageDifferent {
            get {
                if (this.DifferentPixels != null && this.DifferentColor != null) {
                    return this.DifferentColor != 0 && this.DifferentPixels != 0;
                }
                return false;
            }
        }

        /// <summary>
        /// Is Different: either text or image
        /// </summary>
        public bool IsDifferent {
            get {
                if (this.TextDiff != null && this.TextDiff.Length > 0) {
                    return true;
                }
                if (this.IsImageDifferent) {
                    return true;
                }

                return false;
            }
        }
    }
}
