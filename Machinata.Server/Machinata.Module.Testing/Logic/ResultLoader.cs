﻿using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Testing.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Logic {
    public class ResultLoader {

        public static IEnumerable<RevisionResults> LoadTestResultsByRevision(ModelContext db) {
            var buildVersions = db.TestResults().GroupBy(tr => tr.BuildGUID);
            var results = new List<RevisionResults>();
            foreach (var buildVersion in buildVersions) {
                var result = new RevisionResults(db);
                var entity = buildVersion.First();
                result.BuildVersion = entity.BuildVersion;
                result.BuildGUID = entity.BuildGUID;
                result.BuildDate = entity.BuildDate;
                result.TestResults = buildVersion.AsEnumerable();
                results.Add(result);

            }
            return results.OrderByDescending(r => r.BuildDate);

        }

        public static RevisionResults LoadLatestTestResult(ModelContext db) {
            var testResults = LoadTestResultsByRevision(db);
            return testResults.FirstOrDefault();
        }


        //public static TestResult LoadTestResult(ModelContext db, string version, string id) {
        //    return db.TestResults().FirstOrDefault(r => r.TestID == id && r.BuildVersion == version);
        //}


        /// <summary>
        /// Gets the last accepted version of the test result 
        /// </summary>
        /// <param name="db"></param>
        /// <param name="currentVersionDate"></param>
        /// <param name="id"></param>
        /// <param name="category"></param>
        /// <param name="subCategory"></param>
        /// <returns></returns>
        public static TestResult GetLastAcceptedVersion(ModelContext db, DateTime currentVersionDate, string id, string category, string subCategory) {
            return db.TestResults().OrderByDescending(tr => tr.BuildDate).FirstOrDefault(r => r.Accepted && r.BuildDate < currentVersionDate && r.TestID == id && r.Category == category && r.SubCategory == subCategory);
        }

        public static IEnumerable<RevisionResults> GetPreviousVersions(ModelContext db, string buildGUID, int count = 1) {
            var versions = LoadTestResultsByRevision(db);
            var self = versions.FirstOrDefault(v => v.BuildGUID == buildGUID);
            if (self == null) {
                throw new BackendException("version-error", "Version not found: " + buildGUID);
            }
            var next = versions.Where(v => v.BuildDate < self.BuildDate).Take(count);

            return next;

        }

        public static RevisionResults LoadTestResultByRevision(ModelContext db, string buildGUID) {
            var versions = LoadTestResultsByRevision(db);
            var versionResults = versions.FirstOrDefault(v => v.BuildGUID == buildGUID);
         

            return versionResults;

        }

        public static TestingStatus GetTestingStatus(ModelContext db) {
            var status = new TestingStatus();
            status.BuildVersion = Core.Config.BuildVersion;
            status.BuildDate = Core.Config.BuildDate;
            status.TestDefinitions = TestLoader.LoadAllTests(db).Count();

            var buildInfo = Core.Util.Build.GetProductBuildInfo();

            var buildTest = ResultLoader.LoadLatestTestResult(db);

            if (buildTest != null) {
                status.TestsUpToDate = buildTest.BuildGUID ==buildInfo.BuildGUID && buildTest?.IsCompleted == true;
                status.LastTestBuildVersion = buildTest.BuildVersion;
                status.LastTestBuildDate = buildTest.BuildDate;
                status.LastTestAccepted = buildTest.IsAccepted;
                status.LastTestCompleted = buildTest.IsCompleted;
                status.LastTestCount = buildTest.TestResults.Count();
                status.MissingTests = buildTest.MissingTests;
                status.FailedTests = buildTest.FailedTests;


                // Success
                if (status.LastTestAccepted && status.TestsUpToDate) {
                    status.InfoText = "This build has passed all tests.";
                    status.InfoClass = "success";
                }
                // Running
                else if (status.TestsUpToDate == false || status.MissingTests > 0) {

                    var count = status.MissingTests == 0 ? buildTest.TestResults.Count() : status.MissingTests;

                    status.InfoText = $"This build is still being validated ({count} more to go).";
                    status.InfoClass = "warning";
                    status.DownloadWarning = "Please do not use this build in production until it passes all tests.";

 
                }
                // Error
                else if ( status.LastTestAccepted == false) {
                    status.InfoText = $"This build has failed some tests ({status.FailedTests} failed) and needs further validation. Please do not use this build in production until it passes all tests.";
                    status.InfoClass = "error";
                    status.DownloadWarning = "Please do not use this build in production until it passes all tests.";
                }

            } else {
                status.InfoText = $"This build is still being validated ({status.TestDefinitions} more to go).";
                status.InfoClass = "warning";
            }

           

            return status;
        }

    }


    public class RevisionResults : ModelObject, IPublishedModelObject {

        public RevisionResults(ModelContext db) {
            this.DB = db;
        }
        public ModelContext DB { get; set; }


        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public DateTime? BuildDate { get; set; }

        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.System.LISTING)]
        public string BuildVersion { get; set; }

        [FormBuilder(Forms.System.LISTING)]
        public IEnumerable<TestResult> TestResults { get; set; }

        [FormBuilder(Forms.Admin.LISTING)]
        public int? Count {
            get {
                return this.TestResults?.Count();
            }
        }

        /// <summary>
        /// Is the version accepted
        /// </summary>
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool IsAccepted {
            get {
                return this.IsCompleted && this.TestResults.All(t => t.Accepted);
            }
        }

        public bool Published {
            get {
                return IsAccepted;
            }
        }

        /// <summary>
        ///  If this is the current version we check if number of tests is equal to number of test results, otherwise we assume it was completed
        /// </summary>
        /// <returns></returns>
        public bool IsCompleted {
            get {

                if (this.BuildGUID == Core.Config.BuildGUID) {
                    return this.TestResults.Count() == TestLoader.LoadAllTests(this.DB).Count();
                }
                return true;
            }
        }

        public int MissingTests {
            get {
                return TestLoader.LoadAllTests(this.DB).Count() - this.TestResults.Count();
            }
        }

        public int FailedTests {
            get {
                return this.TestResults.Count(t => t.Accepted == false);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string BuildUser {
            get {
                // TODO CHECK IF ALL RESULTS ARE FROM THE SAME TEST EXECUTION!?
                return this.TestResults.FirstOrDefault()?.Properties[TestResult.PROPERTIES_BUILD_USER]?.ToString();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string BuildArch {
            get {
                // TODO CHECK IF ALL RESULTS ARE FROM THE SAME TEST EXECUTION!?
                return this.TestResults.FirstOrDefault()?.Properties[TestResult.PROPERTIES_BUILD_ARCH]?.ToString();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string BuildMachine {
            get {
                // TODO CHECK IF ALL RESULTS ARE FROM THE SAME TEST EXECUTION!?
                return this.TestResults.FirstOrDefault()?.Properties[TestResult.PROPERTIES_BUILD_MACHINE]?.ToString();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string BuildOS {
            get {
                // TODO CHECK IF ALL RESULTS ARE FROM THE SAME TEST EXECUTION!?
                return this.TestResults.FirstOrDefault()?.Properties[TestResult.PROPERTIES_BUILD_OS]?.ToString();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string BuildProduct {
            get {
                // TODO CHECK IF ALL RESULTS ARE FROM THE SAME TEST EXECUTION!?
                return this.TestResults.FirstOrDefault()?.Properties[TestResult.PROPERTIES_BUILD_PRODUCT]?.ToString();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.System.LISTING)]
        public string BuildGUID { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        public string Accepted {
            get {

                var count = this.TestResults.Count(t => t.Accepted);
                return ToStatsString(count);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string Error {
            get {

                var count = this.TestResults.Count(t => t.ResultStatus == ResultStatuses.Error);
                return ToStatsString(count);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string Changed {
            get {

                var count = this.TestResults.Count(t => t.ResultStatus == ResultStatuses.Changed);
                return ToStatsString(count);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string Same {
            get {

                var count = this.TestResults.Count(t => t.ResultStatus == ResultStatuses.Same);
                return ToStatsString(count);
            }
        }

        private string ToStatsString(int count) {
            var all = this.TestResults.Count();
            var percent =( count * 100 / all).ToString("0.##");
            return $"{count}/{all} - {percent}%";
        }
    }

    public class TestingStatus : ModelObject {

        [FormBuilder(Forms.API.VIEW)]
        public string BuildVersion { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public string BuildDate { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public bool TestsUpToDate { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public int TestDefinitions { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public string LastTestBuildVersion { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? LastTestBuildDate { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public bool LastTestAccepted { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public bool LastTestCompleted { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public int LastTestCount { get; set; }
        [FormBuilder(Forms.API.VIEW)]
        public string InfoText { get; internal set; }
        [FormBuilder(Forms.API.VIEW)]
        public string InfoClass { get; internal set; }
        [FormBuilder(Forms.API.VIEW)]
        public int MissingTests { get; internal set; }
        [FormBuilder(Forms.API.VIEW)]
        public int FailedTests { get; internal set; }
        [FormBuilder(Forms.API.VIEW)]
        public string DownloadWarning { get; internal set; }
    }
}
