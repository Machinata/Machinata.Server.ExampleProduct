using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Lifecycle;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Documentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Testing.Model;
using Machinata.Module.Testing.Logic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Testing.Handler {


    public class TestingAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("testing");
        }

        #endregion


        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "test",
                Path = "/admin/testing",
                Title = "{text.testing}",
                Sort = "500"
            });
        }

        #endregion



        [RequestHandler("/admin/testing/")]
        public void Default() {

            var entities = ResultLoader.LoadTestResultsByRevision(this.DB).AsQueryable();
            entities = this.Template.Paginate(entities, this, nameof(TestResult.BuildDate));

            //var a = "asdfasdfasdfasdfasdfasdfasdfasd";
            //var b = "asdfasdfasdfasdfasdfasdfasdfasd";

            //var result = Core.Util.Diff.GetHTMLDiff(a, b);
            // var result2 = Core.Util.Diff.GetHTMLDiff2(a, b);

            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/versions/version/{entity.build-guid}");

            // Navigation
            this.Navigation.Add("testing");
        }

        [RequestHandler("/admin/testing/versions/version/{buildGUID}")]
        public void Version(string buildGUID) {

            var nVersions = this.Params.Int("versions", 1);

            var revisionResults = ResultLoader.LoadTestResultsByRevision(this.DB).FirstOrDefault(r => r.BuildGUID == buildGUID);
            var groupedEntities = revisionResults.TestResults.AsQueryable().GroupBy(g => g.Category);
            var previous = ResultLoader.GetPreviousVersions(this.DB, buildGUID, nVersions);

            // Meta
            this.Template.InsertPropertyList(
             variableName: "form",
             entity: revisionResults,
             form: new FormBuilder(Forms.Admin.VIEW)
            );

            var buildVersionTemplates = new List<PageTemplate>();

            var groupResultTemplates = new List<PageTemplate>();

            foreach (var group in groupedEntities) {

                var groupTemplate = this.Template.LoadTemplate("version.test-result.group");
                groupTemplate.InsertVariable("group.name", group.Key);

                // Results
                var templates = new List<PageTemplate>();
                foreach (var result in group) {
                    var template = this.Template.LoadTemplate("version.test-result.group.item");
                    template.InsertVariables("entity", result);
                    InsertVariables(result, template);

                    var previewTemplates = new List<PageTemplate>();

                    // This Version
                    {
                        var thisVersionPreview = template.LoadTemplate("version.test-result.preview." + result.TestClassName);
                        InsertVariables(result, thisVersionPreview);
                        previewTemplates.Add(thisVersionPreview);
                    }

                    foreach (var previousVersion in previous) {

                        var prevsiouResult = previousVersion.TestResults.FirstOrDefault(tr => tr.GlobalId == result.GlobalId);

                        // Preview
                        var prevVersionTemplate = template.LoadTemplate("version.test-result.preview." + result.TestClassName);

                        InsertVariables(prevsiouResult, prevVersionTemplate);

                        // Buildversion, only the first one need for <th>
                        if (buildVersionTemplates.Count < previous.Count()) {
                            var versionTemplate = template.LoadTemplate("version.test-result.build-version.th");
                            versionTemplate.InsertVariable("build-version", previousVersion.BuildVersion);
                            buildVersionTemplates.Add(versionTemplate);
                        }


                        previewTemplates.Add(prevVersionTemplate);
                    }

                    template.InsertTemplates("results", previewTemplates);

                    templates.Add(template);
                }

                groupTemplate.InsertTemplates("group.results", templates);

                groupResultTemplates.Add(groupTemplate);


            }
            this.Template.InsertTemplates("groups", groupResultTemplates);

            this.Template.InsertTemplates("build-versions", buildVersionTemplates);
            this.Template.InsertVariable("entity.is-accepted", revisionResults.IsAccepted ? "true" : "false");
            this.Template.InsertVariables("entity", revisionResults);
            this.Template.InsertVariable("number-versions", nVersions);



            // Navigation
            this.Navigation.Add("testing");
            this.Navigation.Add("versions/version/" + buildGUID, "{text.build-version}: " + revisionResults.BuildVersion);
        }

        private static void InsertVariables(TestResult result, PageTemplate template) {
            var classes = new List<string>();

            if (result != null) {
                if (result.Accepted) {
                    classes.Add("accepted");
                }
                {
                    classes.Add("result-" + result.ResultStatus.ToString() );
                }
                {
                    classes.Add("status-" + result.Status.ToString());
                }
                template.InsertVariable("entity.classes", string.Join(" ", classes));
                template.InsertVariables("entity", result);
                template.InsertVariable("entity.has-image", "true");
            } else {
                template.InsertVariable("entity.status", "not available");
                template.InsertVariable("entity.has-image", "false");
                template.InsertVariable("entity.data-result", string.Empty);
                template.InsertVariable("entity.body-result-summary", string.Empty);
            }


        }


   

        [RequestHandler("/admin/testing/versions/version/{buildGUID}/result/{publicId}")]
        public void Result(string buildGUID, string publicId) {

            var entity = this.DB.TestResults().GetByPublicId(publicId);
         
            this.Template.InsertPropertyList(
                variableName: "form",
                entity: entity);

            this.Template.InsertPropertyList(
              variableName: "technical-form",
              form: new FormBuilder(Forms.System.VIEW),
              entity: entity);


            this.Template.InsertVariable("entity.is-accepted", entity.Accepted ? "true" : "false");
            this.Template.InsertVariable("entity.has-diff-data", entity.DiffData!=null ? "true" : "false");
            this.Template.InsertVariable("entity.has-error-log", entity.ErrorResult != null ? "true" : "false");
            this.Template.InsertVariable("entity.has-data", entity.DataResult != null ? "true" : "false");
            this.Template.InsertVariable("entity.has-diff-text", entity.DiffText != null ? "true" : "false");
            this.Template.InsertVariable("entity.has-text", entity.BodyResult != null ? "true" : "false");
            this.Template.InsertVariable("entity.has-comparison-data", entity.ComparisonData != null ? "true" : "false");
            this.Template.InsertVariable("entity.has-compared-result", string.IsNullOrEmpty( entity.Properties[TestResult.PROPERTIES_AUTO_COMPARED_ENTITY]?.ToString()) ? "false" : "true");
            this.Template.InsertVariable("entity.has-parameter-url", string.IsNullOrEmpty(entity.Parameters[TestResult.PARAMETERS_URL]?.ToString()) ? "false" : "true");

            // Diff Text
            if (entity.DiffText != null) {
                var template = this.Template.LoadTemplate("default.diff-text");
                this.Template.InsertTemplate("diff-text", template);
            } else {
                this.Template.InsertVariable("diff-text", string.Empty);
            }

            // Diff Text
            if (entity.DiffData != null) {
                var template = this.Template.LoadTemplate("default.diff-image");
                this.Template.InsertTemplate("diff-image", template);
            } else {
                this.Template.InsertVariable("diff-image", string.Empty);
            }

            // Vars
            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("testing");
            this.Navigation.Add("versions/version/" + buildGUID, "{text.build-version}: " + entity.BuildVersion);
            this.Navigation.Add("result/" + entity.PublicId, "Result: " + entity.Name);
        }


        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/edit")]
        public void ResultEdit(string version, string publicId) {

            var entity = this.DB.TestResults().GetByPublicId(publicId);

            FormBuilder form = GetEditForm();

            this.Template.InsertForm(
                variableName: "form",
                form: form,
                entity: entity,
                apiCall: $"/api/admin/testing/result/{publicId}/edit",
                onSuccess: "{page.navigation.prev-path}");


            // Navigation
            this.Navigation.Add("testing");
            this.Navigation.Add("versions/version/" + version, "{text.build-version}: " + entity.BuildVersion);
            this.Navigation.Add("result/" + entity.PublicId, "Result: " + entity.Name);
            this.Navigation.Add("edit");
        }

        public static FormBuilder GetEditForm() {
            var form = new FormBuilder(Forms.Admin.EDIT);
            if (Core.Config.IsTestEnvironment == true) {
                form.Include(nameof(TestResult.DataResult));
                // form.Include(nameof(TestResult.BodyResult));
            }

            return form;
        }

        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/download-text", null, null, Verbs.Any, ContentType.Binary)]
        public void ResultText(string version, string publicId) {
            var entity = this.DB.TestResults().GetByPublicId(publicId);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(entity.BodyResult);
            var filename = entity.Name + "-BodyResult.txt";
            this.SendBinary(bytes, filename);
        }

        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/text")]
        public void Text(string version, string publicId) {
            var entity = this.DB.TestResults().GetByPublicId(publicId);
            if (entity.BodyResult != null) {
                this.Template.InsertVariable("text", entity.BodyResult);
            } else {
                this.Template.InsertVariable("text", "No text data available");
            }
        }

        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/download-diff-text", null, null, Verbs.Any, ContentType.Binary)]
        public void DiffTextDownload(string version, string publicId) {
            var entity = this.DB.TestResults().GetByPublicId(publicId);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(entity.DiffText);
            var filename = entity.Name + "-DiffText.html";
            this.SendBinary(bytes, filename);
        }

        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/diff-text")]
        public void DiffText(string version, string publicId) {
            var entity = this.DB.TestResults().GetByPublicId(publicId);
            if (entity.DiffText != null) {
                this.Template.InsertVariableUnsafe("diff", entity.DiffText);
            } else {
                this.Template.InsertVariableUnsafe("diff", "No Diff data available");
            }
        }

        [RequestHandler("/admin/testing/versions/version/{version}/result/{publicId}/error-log")]
        public void ErrorLog(string version, string publicId) {
            var entity = this.DB.TestResults().GetByPublicId(publicId);
            if (entity.ErrorResult != null) {
                this.Template.InsertVariableXMLSafeWithLineBreaks("error-result", entity.ErrorResult);
            }
            else {
                this.Template.InsertVariable("error-result", "No error result available");
            }
        }

        [RequestHandler("/testing/status", AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.Text)]
        public void Version() {

            CheckAuthorization();

            this.StringOutput.Clear();
            var status = ResultLoader.GetTestingStatus(this.DB);


            var jobject = Core.JSON.Serialize(data: status.GetJSON(new FormBuilder(Forms.API.VIEW)), lowerDashed: true, enumToString: true, indendFormatting: true);

            this.StringOutput.Append(jobject);
        }

    

        private void CheckAuthorization() {
            // Require login if no auth secret provided. This used by the exporter, with authorizes via the auth secret.
            if (string.IsNullOrEmpty(Testing.Config.Config.TestingAuthSecret) == true || this.Params.String("auth-secret") != Config.Config.TestingAuthSecret) {
                this.RequireLogin();
            }
        }



    }
}
