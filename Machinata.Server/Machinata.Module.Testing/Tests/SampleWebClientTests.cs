﻿using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace Machinata.Product.CinnamonReportingWebsite.VisualTests {
    public class SampleWebClientTests {


       // [TestProvider]
        public static IEnumerable<Test> GetSampleWebClientTests(ModelContext db) {
            var tests = new List<Test>();
            tests.Add(new WebClientTest() {
                URL = "https://www.nerves.ch/strategy?debug=true&test-param=/asdf/asdf.json",
                Name = "Nerves.ch Desktop",
                Id = "Sample Test 1",
                Category = "WeblClient Tests"
                           
            });
            tests.Add(new WebClientTest() {
                URL = "https://nerves.brodie.office.nerves.ch",
                Name = "Nerves Local",
                Id = "Sample Test 1a",
                Category = "WeblClient Tests"
            });
            tests.Add(new WebClientTest() {
                URL = "https://sbb.ch",
                Name = "Sbb.ch Mobile",
                Id = "Sample Test 2",
                Category = "WeblClient Tests",
                IgnoreDiff = true
            });
            tests.Add(new WebClientTest() {
                URL = "https://xyz.123",
                Name = "Unreachable URL",
                Id = "Sample Test 3",
                IgnoreDiff = true,
                Category = "WeblClient Tests"
            });
            return tests;
        }

    }
}