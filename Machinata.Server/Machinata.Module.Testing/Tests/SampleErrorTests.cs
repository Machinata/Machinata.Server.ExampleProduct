﻿using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace Machinata.Product.CinnamonReportingWebsite.VisualTests {
    public class SampleErrorTests {


      //  [TestProvider]
        public static IEnumerable<Test> GetSampleWebClientTests(ModelContext db) {
            var tests = new List<Test>();
            tests.Add(new WebClientTest() {
                URL = "https://unknown.test",
                Name = "Invalid URL Error Test",
                Id = "Sample Test 1",
                Category = "Sample Error Tests"

            });

            tests.Add(new KnownExceptionTest()
            {
                Name = "Known Exception Error Test",
                Id = "Sample Test 2",
                Category = "Sample Error Tests"

            });

            return tests;
        }

        

    }

    public class KnownExceptionTest : Test {
        protected override TestExecutionResult Run(ModelContext db) {
            throw new BackendException("known-error", "This is an expected test error");
        }
    }
}