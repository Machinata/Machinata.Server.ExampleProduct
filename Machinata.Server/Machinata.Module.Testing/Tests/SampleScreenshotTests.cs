﻿using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace Machinata.Product.CinnamonReportingWebsite.VisualTests {
    public class SampleScreenshotTests {


       // [VisualTestProvider]
        public static IEnumerable<Test> GetSampleScreenshotTests(ModelContext db) {
            var tests = new List<Test>();
            tests.Add(new ScreenshotVisualTest() {
                URL = "https://www.nerves.ch/strategy?debug=true&test-param=/asdf/asdf.json",
                Name = "Nerves.ch Desktop",
                Id = "Sample Test 1",
             
                BrowserSize = ScreenshotVisualTest.Desktop
            });
            tests.Add(new ScreenshotVisualTest() {
                URL = "https://nerves.brodie.office.nerves.ch",
                Name = "Nerves Local",
                Id = "Sample Test 1a",
                BrowserSize = ScreenshotVisualTest.Mobile
            });
            tests.Add(new ScreenshotVisualTest() {
                URL = "https://sbb.ch",
                Name = "Sbb.ch Mobile",
                Id = "Sample Test 2",
                BrowserSize = ScreenshotVisualTest.Mobile
            });
            tests.Add(new ScreenshotVisualTest() {
                URL = "https://xyz.123",
                Name = "Unreachable URL",
                Id = "Sample Test 3",
                BrowserSize = ScreenshotVisualTest.Mobile
            });
            return tests;
        }

    }
}