﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Config {
    public class Config {

        public static string TestingAuthSecret = Core.Config.GetStringSetting("TestingAuthSecret");
    }
}
