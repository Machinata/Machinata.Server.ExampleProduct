﻿using Machinata.Core.Data;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Testing.Logic;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Tasks {
    public class TestExecutor : Machinata.Core.TaskManager.Task {
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        public override void Process() {

            Log("Loading tests...");

            var user = this.DB.Users().SystemUser();
            var buildInfo = Core.Util.Build.GetProductBuildInfo();
            var currentBuildVersion = Core.Config.BuildVersion;
            var currentBuildGUID = buildInfo.BuildGUID;
            var currentBuildDate = Core.Util.Time.ConvertToUTCTimezone(Core.Config.BuildTimestamp);

            Log("Current version: " + currentBuildVersion);
            Log("Current build date: " + currentBuildDate);

            var missingTests = new List<Test>();
            var completedTests = new List<TestResult>();
            var availableTests = Logic.TestLoader.LoadAllTests(this.DB);
            var buildTestResults = this.DB.TestResults().Where(tr => tr.BuildGUID == currentBuildGUID).ToList();


            // Find missing test for this version
            foreach (var availableTest in availableTests) {
                var result = buildTestResults.FirstOrDefault(tr => tr.TestID == availableTest.Id && tr.Category == availableTest.Category && tr.SubCategory == availableTest.SubCategory);
                if (result == null) {
                    missingTests.Add(availableTest);
                }
            }

            Log($"Found {missingTests.Count()} unexecuted tests...");
         
            // Process Test
            foreach (var test in missingTests) {
                ProcessTest(user, currentBuildVersion, currentBuildGUID, currentBuildDate, completedTests, test);
            }

            Log($"Sending Test Result Summary");

            // Notification
            if (completedTests.Any(ct => ct.Accepted == false)) {
                TestExecution.SendNotificationEmail(this.DB, completedTests, currentBuildVersion, "Test Result Summary: " + currentBuildVersion, ignoreAccepted: true);
            }

        }

      

        private void ProcessTest(User user, string currentBuildVersion, string currentBuildGUID, DateTime currentBuildDate, List<TestResult> completedTests,  Test test) {
            Log($"Running test {completedTests.Count}: {test.Name} - {test.Category} - {test.SubCategory} - {test.Id}");
            TestResult result = TestExecution.IntitializeTestResult(currentBuildVersion, currentBuildGUID, currentBuildDate, test);

            var startTime = DateTime.UtcNow;

            try {

                var executionResult = test.RunTest(this.DB);

                result.Status = TestStatuses.Success;
                result.DataResult = executionResult.DataResult;
                result.BodyResult = executionResult.BodyResult;

                // Diff & AutoAccept
                var lastAccepted = ResultLoader.GetLastAcceptedVersion(this.DB, currentBuildDate, result.TestID, result.Category, result.SubCategory);
                if (lastAccepted != null) {
                    TestExecution.CompareAndAutoAccept(this.DB, user, result, lastAccepted, test);
                }

              

            } catch (Exception e) {
                // TODO: Expected Exception? if we want an exception to happen

                var exceptionResult = ErrorHandler.WrapException(this.Context, e);
                result.ErrorResult = exceptionResult.StackTrace;
                result.Properties[TestResult.PROPERTIES_EXCEPTION_TYPE] = exceptionResult.Type;
                result.Status = TestStatuses.Error;
                result.ResultStatus = ResultStatuses.Error;
            } finally {
                var endTime = DateTime.UtcNow;
                result.Started = startTime;
                result.Finished = endTime;
                completedTests.Add(result);
                this.DB.TestResults().Add(result);
                this.DB.SaveChanges();
               
            }

        }

    }
}
