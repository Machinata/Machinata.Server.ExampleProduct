using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Net;
using Machinata.Core.Builder;
using Machinata.Core.Data;
using Machinata.Core.Model;
using Machinata.Module.Headless;

namespace Machinata.Module.Testing.Model {

    public class WebClientTest : Test {

      
        public string URL { get; set; }
        public string Method { get; set; } = "GET";
       

        protected override TestExecutionResult Run(ModelContext db) {

            var result = new TestExecutionResult();
            var url = this.URL;

            url = url.Replace("{server-url}", Core.Config.ServerURL);
            url = url.Replace("{public-url}", Core.Config.PublicURL);

            string content = "";

            var request = HttpWebRequest.Create(url);
            request.Method = this.Method;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                content = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }

            result.BodyResult = content;

            return result;
        }
    }

   
}