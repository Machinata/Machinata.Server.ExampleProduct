using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;

namespace Machinata.Module.Testing.Model {


    public abstract class Test {

        public Test() {
            this.Parameters = new Properties();
        }

        /// <summary>
        /// Logical Id for the test, has to be unique with Category and SubCategory
        /// </summary>
        public string Id { get; set; }
        public string Name { get; set; }
      
        /// <summary>
        /// Class of test where it was defined
        /// </summary>
        public string ContainingType { get; set; }

        /// <summary>
        /// Type of Test (e.g. VisualTest)
        /// </summary>
        public string TestType { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }

        public bool IgnoreDiff { get; set; }

        public string GlobalId {
            get {
                return GetGlobalId(this.Id, this.Category, this.SubCategory);
            }
        }

        public Properties Parameters { get; set; }

        public static string GetGlobalId(string id, string category, string subCategory) {
            return $"{id}-{category}-{subCategory}";
        }

        /// <summary>
        /// Implementation of the Run routine
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        protected abstract TestExecutionResult Run(ModelContext db);


        /// <summary>
        /// Calls the implementation of the Run routine in the concrete Tests and wraps exceptions in BackendException
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public TestExecutionResult RunTest(ModelContext db) {
            try {
                return this.Run(db);
            } catch (Exception e) {
                throw new BackendException("run-error", $"Failed to run the test", e);
            }
        }

    }

    

    public enum TestStatuses : short{
        Success = 100,
       //Failure = 1000,
        Error = 1100
    }

    public enum ResultStatuses : short{
       Unknown = 0,
       Same  =1000,
       Error = 2000, 
       Changed= 3000
    }

    public class TestExecutionResult {
        public string DataResult { get; set; }
        public string BodyResult { get; set; }
    }
}