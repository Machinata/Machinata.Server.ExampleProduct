using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Machinata.Core.Builder;
using Machinata.Core.Data;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Testing.Model {

    [Serializable()]
    [ModelClass()]
    [Table("TestingResults")]
    public partial class TestResult : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion


        #region Constants ////////////////////////////////////////////////////////////////////////


        public const string PROPERTIES_IMAGE_DIFFERENT_PIXELS = "DifferentPixels";
        public const string PROPERTIES_IMAGE_DIFFERENT_COLOR = "DifferentColor";
        public const string PROPERTIES_AUTO_COMPARED_VERSION = "AutoComparedVersion";
        public const string PROPERTIES_AUTO_COMPARED_BUILDGUID = "AutoComparedBuildGUID";
        public const string PROPERTIES_AUTO_COMPARED_ENTITY = "AutoComparedEntity";
        public const string PROPERTIES_BUILD_USER = "BuildUser";
        public const string PROPERTIES_BUILD_MACHINE = "BuildMachine";
        public const string PROPERTIES_BUILD_OS = "BuildOS";
        public const string PROPERTIES_BUILD_PRODUCT = "BuildProduct";
        public const string PROPERTIES_BUILD_ARCH = "BuildArch";
       // public const string PROPERTIES_EXCEPTION_MESSAGE = "ExceptionMessage";
        public const string PROPERTIES_EXCEPTION_TYPE = "ExceptionType";
        public const string PARAMETERS_URL = "URL";


        #endregion


        #region Constructors //////////////////////////////////////////////////////////////////////

        public TestResult() {
            this.Properties = new Properties();
            this.Parameters = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Name { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string BuildVersion { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.System.LISTING)]
        public string BuildGUID { get; set; }


        [Column]
        [FormBuilder(Forms.System.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string ContainingClass { get; set; }

        [Column]
        [FormBuilder(Forms.System.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string TestClass { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Category { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string SubCategory { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime? BuildDate { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string BodyResult { get; set; }

        [Column]
        [FormBuilder(Forms.System.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        public string ErrorResult { get; set; }


        [Column]
        [FormBuilder()]
        public Properties Properties { get; set; }

        /// <summary>
        /// Parameters the test used: e.g. URL for screenshot
        /// </summary>
        [Column]
        [FormBuilder()]
        public Properties Parameters { get; set; }

        [Column]
        [FormBuilder(Forms.System.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [DataType(DataType.ImageUrl)]
        public string DataResult { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime? Started { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public DateTime? Finished { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string TestID { get; set; }


        /// <summary>
        /// if executing succeeded or failed
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public TestStatuses Status { get; set; }


        /// <summary>
        /// Result of the test
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public ResultStatuses ResultStatus { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public bool Accepted { get; set; }

        [Column]
        [FormBuilder(Forms.System.VIEW)]
        //[FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool IgnoreDiff { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string DiffData { get; set; }

        /// <summary>
        /// The data the image was compared to
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ComparisonData { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string DiffText { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder]
        public string TestClassName {
            get {
                if (string.IsNullOrEmpty(this.TestClass)) {
                    return null;
                }
                var splits = this.TestClass.Split('.').ToList();
                return  splits.LastOrDefault();
            }
        }


        [FormBuilder]
        public string GlobalId {
            get {
                return Test.GetGlobalId(this.TestID, this.Category, this.SubCategory);
            }
        }


        [FormBuilder]
        public string BodyResultSummary {
            get {
                if (this.BodyResult != null) {
                    return Core.Util.String.CreateSummarizedText(this.BodyResult, 20, true, allowSpecialCharacters: true, breakAtWordIfPossible: true);
                }
                return null;
            }
        }

        [FormBuilder]
        public string DiffTextSummary {
            get {
                if (this.DiffText != null) {
                    return Core.Util.String.CreateSummarizedText(this.DiffText, 20, true, allowSpecialCharacters: true, breakAtWordIfPossible: true);
                }
                return null;
            }
        }


        [FormBuilder(Forms.System.VIEW)]
        public string ExceptionType {
            get
            {
                return this.Properties[TestResult.PROPERTIES_EXCEPTION_TYPE]?.ToString();
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnProjectSeed]
        private static void OnProjectSeed(ModelContext context, string dataset) {
          
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        [FormBuilder]
        public string ComparedResultId {
            get {
                return this.Properties[TestResult.PROPERTIES_AUTO_COMPARED_ENTITY]?.ToString();
            }
        }

        [FormBuilder]
        public string ComparedVersion {
            get {
                return this.Properties[TestResult.PROPERTIES_AUTO_COMPARED_VERSION]?.ToString();
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.System.VIEW)]
        public string ParameterURL {
            get {
                return this.Parameters[TestResult.PARAMETERS_URL]?.ToString();
            }
        }

        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////



        #endregion


        #region Private Variables /////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            if (this.DiffData != null) {
                ContentFile file = GetContentFile(db, this.DiffData);
                DataCenter.DeleteFile(db, file);
            }
            if (this.DataResult != null) {
                ContentFile file = GetContentFile(db, this.DataResult);
                DataCenter.DeleteFile(db, file);
            }
            if (this.ComparisonData != null) {
                ContentFile file = GetContentFile(db, this.ComparisonData);
                DataCenter.DeleteFile(db, file);
            }
        }

        public static ContentFile GetContentFile(ModelContext db, string contentURL) {
            // delete
            //  var fileName = $"/content/{fileCategory.ToLower()}/{PublicId}/{FullFileNameURISafe}";

            var splits = contentURL.Split('/');
            var contentId = splits.Skip(3).First();

            var contentFile = db.ContentFiles().GetByPublicId(contentId);
            return contentFile;
            //return db.ContentFiles().FirstOrDefault(c => c.FileCategory == fileCategory && c.Source == "VisualTests");
        }

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextTestResultsExtenions {
        

        /// <summary>
        /// Loads the themes from the db, this should only be used for LEGAY Themes
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static DbSet<TestResult> TestResults(this Core.Model.ModelContext context) {
            return context.Set<TestResult>();
        }
    }

    #endregion
}
