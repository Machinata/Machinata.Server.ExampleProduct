

/// <summary>
/// Machinata Dashboard JS Library
/// Library for displaying and managing dynamic presentations.
/// </summary>
Machinata.Presentation = {};

Machinata.Presentation.Config = {};

Machinata.Presentation.Config.createAPICall = "/api/csam/portal/cockpit/create-presentation";
Machinata.Presentation.Config.addScreenAPICall = "/api/csam/portal/cockpit/presentation/{public-id}/add-screen";
Machinata.Presentation.Config.deleteAPICall = "/api/csam/portal/cockpit/presentation/{public-id}/delete";
Machinata.Presentation.Config.saveAPICall = "/api/csam/portal/cockpit/presentation/{public-id}/save";
Machinata.Presentation.Config.listAPICall = "/api/csam/portal/cockpit/presentations";

/// <summary>
/// Initializes a presentation with its element. The selector must be a presentation node with all presentation items
/// (widgets) as its children.
/// </summary>
Machinata.Presentation.init = function (presentation) {


    // Initialize Packery
    var grid = presentation.packery({
        itemSelector: '.bb-presentation-editor-screen',
        gutter: 0,
        columnWidth: presentation.find('.bb-presentation-editor-column-width')[0],
        rowHeight: presentation.find('.bb-presentation-editor-row-height')[0],
    });

    // Make all items draggable
    var items = null;
    if (presentation.hasClass("option-draggable")) {
        items = grid.find('.bb-presentation-editor-screen').draggable({
            stop: function () {
                Machinata.debug("Machinata.Presentation: draggable.stop");
                Machinata.Presentation.updateLayout(presentation);
            }
        });
    } else {
        items = grid.find('.bb-presentation-editor-screen');
    }
    Machinata.Presentation.registerItems(presentation,items);

    // Bind drag events to Packery
    grid.packery('bindUIDraggableEvents', items);

    // Bind events
    grid.on('layoutComplete', function (event, items) {
        Machinata.debug("Machinata.Presentation: packery.layoutComplete: " + items.length);
    });
    grid.on('dragItemPositioned', function (event, draggedItem) {
        Machinata.debug("Machinata.Presentation: packery.dragItemPositioned: " + draggedItem.length);
    });
    grid.on('fitComplete', function (event, item) {
        Machinata.debug("Machinata.Presentation: packery.fitComplete: " + item.length);
    });
    grid.on('removeComplete', function (event, removedItems) {
        Machinata.debug("Machinata.Presentation: packery.removeComplete: " + removedItems.length);
    });
};

/// <summary>
/// Force the packery layout to update.
/// </summary>
Machinata.Presentation.updateLayout = function (presentation, save) {
    Machinata.debug("Machinata.Presentation: updateLayout");
    setTimeout(function () {
        // Update packery layout
        presentation.packery('layout');
        // Do API save
        if (save != false) {
            setTimeout(function () {
                Machinata.Presentation.save(presentation);
            }, 500);
        }
    }, 10);
};


Machinata.Presentation.removeItems = function (presentation, elems) {
    elems.each(function () {
        var elem = $(this);
        //elem.remove();
    });
    presentation.packery('remove', elems);
    Machinata.Presentation.updateLayout(presentation);
};


Machinata.Presentation.confirmRemoveItem = function (presentation, elem) {
    itemDescription = elem.attr("data-title");
    var title = "Remove item";
    var message = "Are you sure you want to remove '" + itemDescription + "' from " + presentation.attr("data-title") + "?";
    Machinata.confirmDialog(title, message)
        .okay(function () {
            Machinata.Presentation.removeItems(elem.data("presentation"), elem);
        })
        .show();
};


/// <summary>
/// Whenever a item is added to the presentation, you must call this method
/// to ensure it is properly initialized and registered with the underlying
/// system.
/// </summary>
Machinata.Presentation.registerItems = function (presentation,elems) {
    elems.each(function () {
        var elem = $(this);
        elem.data("presentation", presentation);
        // Get id
        elem.find(".tools .tool.remove").click(function () {
            Machinata.Presentation.confirmRemoveItem(presentation, elem);
        });
        elem.find(".tools .tool.fullscreen").click(function () {
            var screenNumber = parseInt(elem.attr("data-screen-number"));
            Machinata.Presentation.openFullscreen(presentation.attr("data-id"), screenNumber);
        });
    });
};

Machinata.Presentation.save = function (presentation) {
    //TODO: support saving on either API or cookie
    var call = presentation.attr("data-api-call");
    if (call == null) return; //TODO: save to browser cache
    Machinata.debug("Machinata.Presentation: saving...");
    // Create data package
    var data = {};
    var items = presentation.packery('getItemElements');
    for(var i = 0; i < items.length; i++) {
        var item = $(items[i]);
        var iid = item.attr("data-screen-id");
        item.attr("data-screen-number",(i+1));
        data["screen_" + iid + "_sort"] = i;
    }
    // Do API call
    Machinata.apiCall(call)
        .data(data)
        .success(function (message) {
            Machinata.debug("Machinata.Presentation: save complete!");
            // Reload?
            if (message.data["require-reload"] == true) {
                Machinata.reload();
            }
        })
        .send();
};



Machinata.Presentation.deletePresentationById = function (publicId, confirm) {
    if (confirm != false) {
        // Show confirmation
        Machinata.confirmDialog("Delete presentation", "Are you sure you want to delete this presentation?")
        .okay(function () {
            Machinata.Presentation.deletePresentationById(publicId,false);
        })
        .show();
    } else {
        // Delete via API call
        var call = Machinata.Presentation.Config.deleteAPICall.replace("{public-id}", publicId);
        Machinata.apiCall(call)
            .success(function (message) {
                Machinata.reload();
            })
            .send();
    }
};


Machinata.Presentation.createNewPresentation = function () {
    // New presentation!
    Machinata.inputDialog("New presentation", "Please enter the name of the new presentation.", "", "Presentation name")
        .input(function (val) {
            if (val == null || val == "") return;
            // Do API call for new presentation
            Machinata.apiCall(Machinata.Presentation.Config.createAPICall)
                .data({ title: val })
                .success(function (message) {
                    Machinata.goToPage(message.data["url"]);
                })
                .send();
        })
        .show();
};

Machinata.Presentation.addScreen = function (title, subtitle, source, type, content) {
    Machinata.debug("Machinata.Presentation.addScreen:");


    // Do API call
    Machinata.apiCall(Machinata.Presentation.Config.listAPICall)
        .success(function (message) {
            var opts = {};
            var presentationIdToPublicId = {};
            $.each(message.data, function (index, item) {
                opts[item["public-id"]] = item.title;
                presentationIdToPublicId[item["url"]] = item["public-id"];
            });
            opts["-"] = "-";
            opts["new"] = "Create new presentation";
            Machinata.optionsDialog('Add to presentation', 'Which presentation would you like to add ' + title + ' to?', opts)
                .okay(function (publicId, val) {
                    
                    if (publicId == "new") {
                        // New presentation!
                        Machinata.inputDialog("New presentation", "Please enter the name of the new presentation.", "", "Dashboard name")
                            .input(function (val) {
                                if (val == null || val == "") return;
                                // Do API call for new presentation
                                Machinata.apiCall(Machinata.Presentation.Config.createAPICall)
                                    .data({ title: val })
                                    .success(function (message) {
                                        Machinata.Presentation._addScreenViaAPICall(
                                            title, subtitle, source, type, content,
                                            message.data["public-id"],
                                            message.data["url"]
                                        );
                                    })
                                    .send();
                            })
                            .show();

                    } else {

                        // Existing presentation!
                        Machinata.Presentation._addScreenViaAPICall(
                            title, subtitle, source, type, content,
                            publicId,
                            "/portal/cockpit/presentation/" + publicId
                        );
                    }
                })
                .show();
        })
        .send();
};

Machinata.Presentation._addScreenViaAPICall = function (title, subtitle, source, type, content, publicId, successLink) {
    // Prepare data
    var data = {
        title: title,
        subtitle: subtitle,
        source: source,
        type: type,
        content: content,
    };
    // Do API call
    Machinata.apiCall("/api/csam/portal/cockpit/presentation/" + publicId + "/add-screen") //TODO config
        .data(data)
        .success(function (message) {
            Machinata.yesNoDialog('Screen added', 'Would you like to go your presentation now?')
                .okay(function () {
                    Machinata.goToPage(successLink);
                }).show();
        })
        .send();
};







/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Presentation.openFullscreen = function (publicId, initialScreen) {

    // Init
    if (initialScreen == null) initialScreen = 1;
    var currentScreen = initialScreen;
    var screens = null; // json data

    // Create fullscreen UI
    var fullscreenContainer = $('<div class="bb-presentation-fullscreen-container"><div class="ui-readprogress bb-presentation-readprogress"><div class="progress"></div></div><div class="ui-toolbar option-light bb-presentation-fullscreen-toolbar"><label class="title"></label><label class="subtitle"></label><div class="tools"><div class="tool exit-fullscreen icon-csam-fn-small-close" title="Exit Fullscreen"></div></div></div><iframe></iframe><div class="bb-reporting-screen-controls prev"><a class="icon icon-csam-fn-small-chevron-left"></a></div><div class="bb-reporting-screen-controls next"><a class="icon icon-csam-fn-small-chevron-right"></a></div></div>');
    // Cache selectors
    var fullscreenIframe = fullscreenContainer.find("iframe");
    var fullscreenToolbar = fullscreenContainer.find(".bb-presentation-fullscreen-toolbar");
    fullscreenToolbar.find(".title").text("Loading...");
    fullscreenToolbar.find(".subtitle").text();
    // Helper function
    function showScreen(screen) {
        currentScreen = screen;
        fullscreenIframe.attr("src", screens[screen - 1]["content"]);
        fullscreenToolbar.find(".subtitle").text(screen + " / " + screens.length + ", " + screens[screen - 1]["source"]);
        if (currentScreen == 1) {
            fullscreenContainer.find(".bb-reporting-screen-controls.prev").hide();
        } else {
            fullscreenContainer.find(".bb-reporting-screen-controls.prev").show();
        }
        if (currentScreen == screens.length) {
            fullscreenContainer.find(".bb-reporting-screen-controls.next").hide();
        } else {
            fullscreenContainer.find(".bb-reporting-screen-controls.next").show();
        }
    }
    // Events
    fullscreenToolbar.find(".tool.exit-fullscreen").click(function () {
        Machinata.Presentation.exitFullscreen();
    });
    fullscreenContainer.find(".bb-reporting-screen-controls.prev").click(function () {
        var newScreen = currentScreen - 1;
        if (newScreen < 1) return;
        showScreen(newScreen);
    });
    fullscreenContainer.find(".bb-reporting-screen-controls.next").click(function () {
        var newScreen = currentScreen + 1;
        if (newScreen > screens.length) return;
        showScreen(newScreen);
    });
    // Regiser UI in DOM
    $("body").append(fullscreenContainer);
    // Enter fullscreen
    fullscreenContainer.fullScreen(true);
    // Get presentation data
    Machinata.apiCall("/api/csam/portal/cockpit/presentation/" + publicId + "/screens") //TODO config
        .success(function (message) {
            screens = message.data.screens;
            fullscreenToolbar.find(".title").text(message.data.title);
            showScreen(initialScreen);
        })
        .send();
};


/// <summary>
/// Forces a exit of fullscreen mode
/// </summary>
Machinata.Presentation.exitFullscreen = function () {
    var fullscreenContainer = $(".bb-presentation-fullscreen-container");
    fullscreenContainer.fullScreen(false);
    fullscreenContainer.remove();
};