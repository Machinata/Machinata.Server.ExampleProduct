﻿using Machinata.Core.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Headless {
    public class Firefox {

        public static string CachePathChromium = Core.Config.CachePath + Core.Config.PathSep + "firefox";
        
        
        protected static void _setEnvVariables(IDictionary<string, string> environment, IDictionary<string, string> customEnv, IDictionary realEnv) {
            foreach (DictionaryEntry item in realEnv) {
                environment[item.Key.ToString()] = item.Value.ToString();
            }

            if (customEnv != null) {
                foreach (var item in customEnv) {
                    environment[item.Key] = item.Value;
                }
            }
        }


        // Via https://github.com/hardkoded/puppeteer-sharp/blob/master/lib/PuppeteerSharp/ChromiumLauncher.cs
        internal static readonly string[] _defaultArgs = {
            "--disable-background-networking",
            "--enable-features=NetworkService,NetworkServiceInProcess",
            "--disable-background-timer-throttling",
            "--disable-backgrounding-occluded-windows",
            "--disable-breakpad",
            "--disable-client-side-phishing-detection",
            "--disable-component-extensions-with-background-pages",
            "--disable-default-apps",
            "--disable-dev-shm-usage",
            "--disable-extensions",
            "--disable-features=TranslateUI",
            "--disable-hang-monitor",
            "--disable-ipc-flooding-protection",
            "--disable-popup-blocking",
            "--disable-prompt-on-repost",
            "--disable-renderer-backgrounding",
            "--disable-sync",
            "--force-color-profile=srgb",
            "--metrics-recording-only",
            "--no-first-run",
            "--enable-automation",
            "--password-store=basic",
            "--use-mock-keychain"
        };

        private static Object _getScreenshotLock = new Object();

        public static string GetScreenshot(string url, string format, string path, int width, int height) {
            string ret = null;
            //lock (_getScreenshotLock) {
                ret = _getScreenshot(url, format, path, width, height);
            //}
            return ret;
        }

        private static string _getScreenshot(string url, string format, string path, int width, int height) {
            // See https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options#User_Profile
            // See https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Headless_mode

            if (!System.IO.Directory.Exists(CachePathChromium)) {
                System.IO.Directory.CreateDirectory(CachePathChromium);
            }

            var file = Core.Util.Files.GetFileNameWithExtension(path);
            //url = "https://developer.mozilla.org";

            var process = new Process() {
                //EnableRaisingEvents = true
            };
            //process.StartInfo.Verb = "runas";
            process.StartInfo.FileName = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
            //process.StartInfo.Arguments += $" -CreateProfile \"headless {CachePathChromium}\"";
            //process.StartInfo.Arguments += $" --window-size={width},{height}";
            //process.StartInfo.Arguments += $"-P headless";
            //process.StartInfo.Arguments += $" -headless";
            //process.StartInfo.Arguments += $" --screenshot {path} {url}";
            //process.StartInfo.Arguments += $" --screenshot \"{url}\"";
            process.StartInfo.Arguments += $" --screenshot {file} {url}";
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.RedirectStandardError = true;
            process.StartInfo.UseShellExecute = false;
            _setEnvVariables(process.StartInfo.Environment, new Dictionary<string, string>(), Environment.GetEnvironmentVariables());
            process.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(path);
            
            
            process.Start();

            //string stdoutx = process.StandardOutput.ReadToEnd();
            //string stderrx = process.StandardError.ReadToEnd();
            string stderrx = "unknown";

            //process.WaitForExit(8000);
            process.WaitForExit();

            if (process.ExitCode != 0) {
                throw new BackendException("screenshot-error", "Returned exit code " + process.ExitCode + ": " + stderrx + "\nCommand: " + process.StartInfo.FileName + " " + process.StartInfo.Arguments);
            }

            Core.Util.Files.WaitForFile(path);

            return path;
        }

    }
}
