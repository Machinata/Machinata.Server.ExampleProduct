﻿using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Headless {

    public class Restpack {

        private static object _respackSingleConversionLimitLock = new object();

        public static string GetScreenshot(string url, string format, string path, int width, int height) {
            string ret = null;

            lock (_respackSingleConversionLimitLock) {
                ret = _getScreenshot(url, format, path, width, height);
            }

            return ret;
        }

        private static string _getScreenshot(string url, string format, string path, int width, int height) {

            // See https://restpack.io/screenshot/docs
            var apiKey = Core.Config.GetStringSetting("RestpackApiAccessKey");

            IRestClient client = new RestClient("https://restpack.io/api/screenshot/v6/capture");

            IRestRequest request = new RestRequest(Method.POST);
            request.AddHeader("x-access-token", apiKey);

            var postData = new {
                url = url,
                json = "false",
                mode = "fullpage", // fullpage or viewport
                wait = "network",
                //delay = 10000
            };
            request.AddJsonBody(postData);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful) {
                throw new BackendException("screenshot-error", "Could not generate screenshot: "+ response.ErrorMessage);
            } else {
                System.IO.File.WriteAllBytes(path, response.RawBytes);


                //JObject jsonData = JObject.Parse(response.Content);
                //Console.WriteLine(jsonData);
                /*
                {
                    "cached": "false",
                    "height": "768",
                    "image": "https://cdn.restpack.io/a/cache/pdf/18486d242a171e3252b7663faefd3110f4c1bc4e68cf9bd9db02f84d557ecb19",
                    "remote_status": "200",
                    "run_time": "1136",
                    "url": "https://www.google.com/",
                    "width": "1280"
                    }
                */
            }


            return path;
        }


    }
}
