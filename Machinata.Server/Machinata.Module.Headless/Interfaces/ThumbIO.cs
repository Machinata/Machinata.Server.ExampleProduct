﻿using Machinata.Core.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Headless {

    public class ThumbIO {

        public static string GetScreenshot(string url, string format, string path, int width, int height) {

            // See https://www.thum.io/documentation/api/url
            var apiKey = "10773-07ac67f632a97d2780db680f2283a84f";
            var downloadURL = $"https://image.thum.io/get/auth/{apiKey}/noanimate/viewportWidth/{width}/{url}";

            WebClient wc = new WebClient();
            wc.DownloadFile(downloadURL, path);


            return path;
        }


    }
}
