using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    public class ScreenshotStaticHandler : Core.Handler.Handler {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathScreenshots = CachePath + "-screenshots";
        public static TimeSpan DefaultCacheTime = new TimeSpan(30, 0, 0, 0); // 30 days


        private static Object _getScreenshotLock = new Object();

        [RequestHandler("/static/headless/screenshot", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void Screenshot() {

            this.ContentType = ContentType.StaticFile;

            string url = this.Params.String("url");
            bool download = this.Params.Bool("download", false);
            int width = this.Params.Int("width", 1280);
            int height = this.Params.Int("height", 720);
            string format = this.Params.String("format", "png");
            string awaitSelector = this.Params.String("await-selector", null);
            bool fullpage = this.Params.Bool("full-page", false);
            string urlHash = Core.Encryption.DefaultHasher.HashString(url);
            string awaitSelectorHash = awaitSelector != null ? Core.Encryption.DefaultHasher.HashString(awaitSelector) : "";
            string cachePath = CachePathScreenshots + Core.Config.PathSep + urlHash + "_" + width + "_" + height+ "_"+ awaitSelectorHash + (fullpage?"_fullpage":"") + "." + format;


            // Create the bundle as cached if needed
            bool forceCreate = Core.Config.GetBoolSetting("StaticHeadlessScreenshotCacheEnabled", true) == false;


            /*
            //lock (_getScreenshotLock) {

                if (forceCreate || !System.IO.File.Exists(cachePath)) {
                    // Make sure the dir exists
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(cachePath));

                    // Init
                    _logger.Info("Creating screenshot of:  " + url);


                    //Module.Headless.Firefox.GetScreenshot(url, format, cachePath, width, height);
                    Module.Headless.Chromium.GetScreenshot(url, format, cachePath, width, height);


            }
            //}*/

            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, () => {
                Module.Headless.NervesScreenshot.GetScreenshot(url, format, cachePath, width, height, awaitSelector, fullpage);
            }, forceCreate);


            if (download == true) {
                string filename = this.Params.String("filename", "Screenshot");
                SendFile(cachePath, DefaultCacheTime, filename);
            } else {
                SendFile(cachePath, DefaultCacheTime);
            }


        }


    }
}
