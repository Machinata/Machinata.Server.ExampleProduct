using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Machinata.Product.ExampleProduct.Model {

    public class ExampleProductSeed {


        [OnProjectSeed]
        private static void OnProjectSeed(ModelContext context, string dataset) {

            // DATASET_REQUIRED
            if (dataset == Core.Model.SeedDatasets.DATASET_REQUIRED || dataset == Core.Model.SeedDatasets.DATASET_ALL) {
                

            }

            // DATASET_DUMMY
            if (dataset == SeedDatasets.DATASET_DUMMY || dataset == Core.Model.SeedDatasets.DATASET_ALL) {
                

            }
        }
        

    }
}