using Machinata.Core.Handler;
using System.Collections.Generic;

using Machinata.Core.Model;


namespace Machinata.Product.ExampleProduct.Handler {

    public class DefaultHandler : Module.UIKit.Handler.UIKitPageHandler {

        [SitemapRoute]
        [MultiRouteRequestHandler("routes.root", AccessPolicy.PUBLIC_ARN)]
        public void RootDefault() {
            this.Template.InsertLayoutedContent("content", "/Start", this.Language);
        }


        [SitemapRoute]
        [MultiRouteRequestHandler("routes.demo-1", AccessPolicy.PUBLIC_ARN)]
        public void DemoPage1() {
            this.Template.InsertLayoutedContent("content", "/Demo 1", this.Language);
            this.Navigation.Add("{text.routes.demo}", "{text.sections.demo-1}");
        }

        [SitemapRoute]
        [MultiRouteRequestHandler("routes.demo-2", AccessPolicy.PUBLIC_ARN)]
        public void DemoPage2() {
            this.Template.InsertLayoutedContent("content", "/Demo 2", this.Language);
            this.Navigation.Add("{text.routes.demo-2}", "{text.sections.demo-2}");
        }

        /// <summary>
        /// Set the defaul theme for this handler
        /// </summary>
        /// <returns></returns>
        public override string DefaultTheme() {
            return "uikit-example-product";
        }


        public override void DefaultNavigation() {

            this.Navigation.Items.Clear();

            this.Meta.UseMetaTitleForPageTitle = true;
            this.Navigation.TitleSeparator = " / ";

            this.Meta.TitleTrailer = " / Example Machinata Product";

        }

        public override void Finish() {
            base.Finish();
        }

        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();


            // Menu
            var menu = CompileMenu();
            this.Template.InsertMenu("menu", menu, "menu");

            // User
            if (this.User != null) {
                this.Template.InsertVariables("user", this.User);
                this.Template.InsertVariable("user.initials", this.User.GetInitials());
            } else {
                this.Template.InsertVariable("user.initials", string.Empty);
            }

            // Page settings
            var pageSettings = new List<string>();
            pageSettings.Add("show-menu");
            if (this.User != null && this.User.IsAdminUser) pageSettings.Add("show-internal-notes");
            if (this.User != null) pageSettings.Add("user-logged-in");
            else pageSettings.Add("user-logged-out");
            this.Template.InsertVariable("page.settings", string.Join(" ", pageSettings));

            this.Template.DiscoverVariables();
        }



        public Core.Builder.MenuBuilder CompileMenu() {
            const int STANDARD_SUMMARY_LEN = 200;
            var menu = new Core.Builder.MenuBuilder();

            menu.Section("{text.sections.demo-1}", "demo-1", "{text.routes.demo-1}");
            menu.Section("{text.sections.demo-2}", "demo-2", "{text.routes.demo-2}");

            menu.AutoSelect(this);
            menu.AutoSummarize(STANDARD_SUMMARY_LEN);
            return menu;
        }

      

    }
}